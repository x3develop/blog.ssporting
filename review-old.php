<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ngoal | ทีเด็ด ทรรศนะ</title>
    <link rel="shortcut icon" type="images/x-icon" href="images/favicon.ico.png">
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/doce.css">
    <link rel="stylesheet" href="/css/livescore.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <link rel="stylesheet" href="/css/game-redesign.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="js/jquery/dist/jquery.min.js"></script>
    <script src="js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
</head>
<style type="text/css">
    .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6,
    .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
        /*border:solid 1px;*/
        /*min-height: 100px;*/
    }
</style>
<body style="background-color: #f1f1f1">

<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playBet.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playUser.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playReviewMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playChannelMatch.php";

$playBet = new playBet();
$playUser=new playUser();
$playMatch = new playMatch();
$playReviewMatch = new playReviewMatch();
$playChannelMatch = new playChannelMatch();
$match=$playMatch->getByReview(30);
//$topRanking=$playReviewMatch->topRanking(5);
$topRankingAll=$playUser->accuracyUserAll();
$yd=false;
?>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="container" style="padding-top: 70px;">
    <div class="col-sm-9">
        <?php if(!empty($match) and $match[0]->dateMatch==date("Y-m-d")){ ?>
        <div style="width: 100%;margin: 10px 0px 10px 0px;border-bottom: solid 2px;">Today</div>
        <?php } ?>
        <?php foreach ($match as $key=>$value){
        $matchResultsHome = $playMatch->getListMatchResults($value->team_home, 5);
        $matchResultsAway = $playMatch->getListMatchResults($value->team_away, 5);
        $userListHome = $playMatch->getUserBetByTeam($value->match_id,'home',5);
        $userListAway = $playMatch->getUserBetByTeam($value->match_id,'away',5);
        $percentBet = $playBet->percentBet($value->match_id);
        $matchPlayChannelMatch=$playChannelMatch->getChannel($value->match_7m_id);
        $reviewMatchByMatchId=$playReviewMatch->getReviewMatchByMatchId($value->match_id);
        $reviewMatch = array();
//        foreach ($reviewMatchByMatchId as $keyR => $valueR) {
//            $winRate = $playReviewMatch->getWinRate($valueR['user_review_id'], 30);
//            if ($winRate["total"] != 0) {
//                $reviewMatch[$valueR['user_review_id']] = intval(100 - ((($winRate["total"] - $winRate["win"]) / $winRate["total"]) * 100));
//            } else {
//                $reviewMatch[$valueR['user_review_id']] = 0;
//            }
//        }
        ?>
            <?php if(date("Y-m-d")!=$value->dateMatch and $yd==false){ $yd=true; ?>
                <div style="width: 100%;margin: 10px 0px 10px 0px;border-bottom: solid 2px;">Yesterday</div>
            <?php } ?>
        <div class="box-review">
            <div onclick="location.href='/games.php?mid=<?php echo $value->match_id; ?>'" class="box-review-top">
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="text-right">
                                        <div class="pull-right">
                                            <h4 class="pull-right" style="width: 100%;"><?=$value->teamHomeEn?></h4>
                                            <ul class="last-march-view <?=$value->match_id?>">
                                                <li class="btn-results-left" number="0"><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                                <?php if (!empty($matchResultsHome)) { ?>
                                                <?php foreach (array_reverse($matchResultsHome) as $keyResults => $valueResults) { ?>
                                                    <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                        <li class="li-results"><i title="<?= $valueResults->time_match ?>" style="color: #858585;" class="fa fa-circle" aria-hidden="true"></i></li>
                                                    <?php } elseif ($valueResults->team_home == $value->team_home && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                        <li class="i-results"><i title="<?= $valueResults->time_match ?>" style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i></li>
                                                    <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                        <li class="li-results"><i title="<?= $valueResults->time_match ?>" style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i></li>
                                                    <?php } else { ?>
                                                        <li class="li-results"><i title="<?= $valueResults->time_match ?>" style="color: #d70e47;" class="fa fa-circle" aria-hidden="true"></i></li>
                                                    <?php } ?>
                                                <?php }} ?>
                                                <li>Last <span class="total-results">5</span> Match</li>
                                            </ul>
                                            <ul class="player-vote-view">
                                                <li>+<?=count($userListHome)?></li>
                                                <?php foreach ($userListHome as $keyH => $valueH) { ?>
                                                        <li><img src="https://graph.facebook.com/v2.8/<?php echo $valueH->fb_uid; ?>/picture"></li>
                                                <?php } ?>
                                                <li>Player</li>
                                            </ul>
                                            <div class="progress-box pull-right">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <small><?=number_format($percentBet['pHome'],2)?>%</small>
                                                        </td>
                                                        <td>
                                                            <div class="progress">
                                                                <div class="progress-bar progress-bar-striped active"
                                                                     role="progressbar"
                                                                     aria-valuenow="<?=number_format($percentBet['pHome'],2)?>" aria-valuemin="0"
                                                                     aria-valuemax="100"
                                                                     style="width: <?=number_format($percentBet['pHome'],2)?>%">
                                                                    <span class="sr-only"><?=number_format($percentBet['pHome'],2)?>% Complete</span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="logo-team-view"><img src="<?=$value->teamHomePath?>"></div>
                                        <a href="games.php?mid=<?php echo $value->match_id; ?>"><img src="/images/vote-view.png"></a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <h3><?=$value->name?></h3>
                            <ul>
                                <li><?=(($value->home_end_time_score===null)?((!empty($value->home_water_bill))?$value->home_water_bill:'-'):$value->home_end_time_score) ?></li>
                                <li>
                            <span class="box-view-hdp" style="color: #0084c0;">
                                <?=((!empty($value->handicap))?$value->handicap:'-')?>
                            </span>
                                </li>
                                <li><?=(($value->away_end_time_score===null)?((!empty($value->away_water_bill))?$value->away_water_bill:'-'):$value->away_end_time_score)?></li>
                            </ul>
                            <p>
                                <i class="fa fa-clock-o" aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?> |
                                <i class="fa fa-calendar" aria-hidden="true"></i> <?= date("d/m/Y", (strtotime($value->time_match) - (60 * 60))) ?>
                            </p>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td class="text-center">
                                        <div class="logo-team-view"><img src="<?=$value->teamAwayPath?>"></div>
                                        <a href="games.php?mid=<?php echo $value->match_id; ?>"><img src="/images/vote-view.png"></a>
                                    </td>
                                    <td>
                                        <div class="pull-left text-left">
                                            <h4><?=$value->teamAwayEn?></h4>
                                            <ul class="last-march-view">
                                                <li>Last <span class="total-results">5</span> Match</li>
                                                <?php if (!empty($matchResultsAway)) { ?>
                                                    <?php foreach ($matchResultsAway as $keyResults => $valueResults) { ?>
                                                        <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                            <li class="li-results"><i title="<?= $valueResults->time_match ?>" style="color: #858585;" class="fa fa-circle" aria-hidden="true"></i></li>
                                                        <?php } elseif ($valueResults->team_home == $value->team_away && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                            <li class="li-results"><i title="<?= $valueResults->time_match ?>" style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i></li>
                                                        <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                            <li class="li-results"><i title="<?= $valueResults->time_match ?>" style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i></li>
                                                        <?php } else { ?>
                                                            <li class="li-results"><i title="<?= $valueResults->time_match ?>" style="color: #d70e47;" class="fa fa-circle" aria-hidden="true"></i></li>
                                                        <?php } ?>
                                                    <?php }} ?>
                                                <li class="btn-results-right" number="0"><i class="fa fa-caret-right" aria-hidden="true"></i></li>
                                            </ul>
                                            <ul class="player-vote-view">
                                                <li>Player</li>
                                                <?php foreach ($userListAway as $keyH => $valueH) { ?>
                                                        <li><img src="https://graph.facebook.com/v2.8/<?php echo $valueH->fb_uid; ?>/picture"></li>
                                                <?php } ?>
                                                <li>+<?=count($userListAway)?></li>
                                            </ul>
                                            <div class="progress-box pull-left">
                                                <table>
                                                    <tr>

                                                        <td>
                                                            <div class="progress">
                                                                <div class="progress-bar pull-left progress-bar-striped active"
                                                                     role="progressbar"
                                                                     aria-valuenow="<?=number_format($percentBet['pAway'],2)?>" aria-valuemin="0"
                                                                     aria-valuemax="100"
                                                                     style="width: <?=number_format($percentBet['pAway'],2)?>%">
                                                                    <span class="sr-only"><?=number_format($percentBet['pAway'],2)?>% Complete</span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <small><?=number_format($percentBet['pAway'],2)?>%</small>
                                                        </td>
                                                    </tr>
                                                </table>


                                            </div>
                                        </div>
                                    </td>

                                </tr>
                            </table>


                        </td>
                        <?php if($value->status!="fullTime"){ ?>
                        <td class="on-tv">
                            <h3></h3>
                            <?php if(!empty($matchPlayChannelMatch)){ ?>
                                <?php foreach ($matchPlayChannelMatch as $keyC=>$valueC){ ?>
                                    <div class="chanel-box"><img src="<?=$valueC->channel_path;?>"></div>
                                <?php } ?>
                            <?php } ?>
                        </td>
                        <?php } ?>
                    </tr>
                </table>
            </div>
            <?php if(!empty($reviewMatchByMatchId)){ ?>
            <div id="carousel-example-generic-<?php echo $key; ?>" class="carousel slide" data-interval="false"  data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php for ($i=0;$i<ceil(count($reviewMatchByMatchId)/4);$i++){ ?>
                    <div class="item <?=(($i==0)?'active':'')?>" data-toggle="modal" data-target="#showReviewGuru-<?php echo $value->match_id?>">
                        <div class="col-sm-6">
                            <?php if(!empty($reviewMatchByMatchId[($i*4)])){?>
                            <div class="item-review">
                                <ul>
                                    <li>
                                        <div class="item-review-img">
                                            <img src="<?= $reviewMatchByMatchId[($i*4)]['path'] ?>">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-review-info">
                                            <h5><?= $reviewMatchByMatchId[($i*4)]['name'] ?></h5>
                                            <?php if ($reviewMatchByMatchId[($i*4)]['team'] == "home") { ?>
                                                <img src="<?= $reviewMatchByMatchId[($i*4)]['homepath'] ?>">
                                                <span class="name-team-slide-top">@ <?= $reviewMatchByMatchId[($i*4)]['homename'] ?></span>
                                            <?php } elseif ($reviewMatchByMatchId[($i*4)]['team'] == "away") { ?>
                                                <img src="<?= $reviewMatchByMatchId[($i*4)]['awaypath'] ?>">
                                                <span class="name-team-slide-top">@ <?= $reviewMatchByMatchId[($i*4)]['awayname'] ?></span>
                                            <?php } ?>
                                        </div>
                                    </li>
                                    <li>
                                        <p class="item-review-text">
                                            <?= $reviewMatchByMatchId[($i*4)]['review_text'] ?>
                                        </p>
                                    </li>
                                    <li class="pull-right">
                                        <div class="item-review-rate text-center">
                                            <div class="circle-rate">
                                                <h2 class="user-review-<?php echo $reviewMatchByMatchId[($i*4)]['user_review_id']; ?>"></h2>
                                                <small>%</small>
                                            </div>
                                            <p>Win Rate</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <?php } ?>
                            <?php if(!empty($reviewMatchByMatchId[(($i*4)+1)])){?>
                            <div class="item-review">
                                <ul>
                                    <li>
                                        <div class="item-review-img">
                                            <img src="<?= $reviewMatchByMatchId[(($i*4)+1)]['path'] ?>">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-review-info">
                                            <h5><?= $reviewMatchByMatchId[(($i*4)+1)]['name'] ?></h5>
                                            <?php if ($reviewMatchByMatchId[(($i*4)+1)]['team'] == "home") { ?>
                                                <img src="<?= $reviewMatchByMatchId[($i*4)+1]['homepath'] ?>">
                                                <span class="name-team-slide-top">@ <?= $reviewMatchByMatchId[(($i*4)+1)]['homename'] ?></span>
                                            <?php } elseif ($reviewMatchByMatchId[(($i*4)+1)]['team'] == "away") { ?>
                                                <img src="<?= $reviewMatchByMatchId[(($i*4)+1)]['awaypath'] ?>">
                                                <span class="name-team-slide-top">@ <?= $reviewMatchByMatchId[(($i*4)+1)]['awayname'] ?></span>
                                            <?php } ?>
                                        </div>
                                    </li>
                                    <li>
                                        <p class="item-review-text">
                                            <?= $reviewMatchByMatchId[(($i*4)+1)]['review_text'] ?>
                                        </p>
                                    </li>
                                    <li class="pull-right">
                                        <div class="item-review-rate text-center">
                                            <div class="circle-rate">
                                                <h2 class="user-review-<?php echo $reviewMatchByMatchId[(($i*4)+1)]['user_review_id']; ?>"></h2>
                                                <small>%</small>
                                            </div>
                                            <p>Win Rate</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="col-sm-6">
                            <?php if(!empty($reviewMatchByMatchId[(($i*4)+2)])){?>
                            <div class="item-review">
                                <ul>
                                    <li>
                                        <div class="item-review-img">
                                            <img src="<?= $reviewMatchByMatchId[(($i*4)+2)]['path'] ?>">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-review-info">
                                            <h5><?= $reviewMatchByMatchId[(($i*4)+2)]['name'] ?></h5>
                                            <?php if ($reviewMatchByMatchId[(($i*4)+2)]['team'] == "home") { ?>
                                                <img src="<?= $reviewMatchByMatchId[($i*4)+2]['homepath'] ?>">
                                                <span class="name-team-slide-top">@ <?= $reviewMatchByMatchId[(($i*4)+2)]['homename'] ?></span>
                                            <?php } elseif ($reviewMatchByMatchId[(($i*4)+2)]['team'] == "away") { ?>
                                                <img src="<?= $reviewMatchByMatchId[(($i*4)+2)]['awaypath'] ?>">
                                                <span class="name-team-slide-top">@ <?= $reviewMatchByMatchId[(($i*4)+2)]['awayname'] ?></span>
                                            <?php } ?>
                                        </div>
                                    </li>
                                    <li>
                                        <p class="item-review-text">
                                            <?= $reviewMatchByMatchId[(($i*4)+2)]['review_text'] ?>
                                        </p>
                                    </li>
                                    <li class="pull-right">
                                        <div class="item-review-rate text-center">
                                            <div class="circle-rate">
                                                <h2 class="user-review-<?php echo $reviewMatchByMatchId[(($i*4)+2)]['user_review_id']?>"></h2>
                                                <small>%</small>
                                            </div>
                                            <p>Win Rate</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <?php } ?>
                            <?php if(!empty($reviewMatchByMatchId[(($i*4)+3)])){?>
                            <div class="item-review">
                                <ul>
                                    <li>
                                        <div class="item-review-img">
                                            <img src="<?= $reviewMatchByMatchId[(($i*4)+3)]['path'] ?>">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-review-info">
                                            <h5><?= $reviewMatchByMatchId[(($i*4)+3)]['name'] ?></h5>
                                            <?php if ($reviewMatchByMatchId[(($i*4)+3)]['team'] == "home") { ?>
                                                <img src="<?= $reviewMatchByMatchId[($i*4)+3]['homepath'] ?>">
                                                <span class="name-team-slide-top">@ <?= $reviewMatchByMatchId[(($i*4)+3)]['homename'] ?></span>
                                            <?php } elseif ($reviewMatchByMatchId[(($i*4)+3)]['team'] == "away") { ?>
                                                <img src="<?= $reviewMatchByMatchId[(($i*4)+3)]['awaypath'] ?>">
                                                <span class="name-team-slide-top">@ <?= $reviewMatchByMatchId[(($i*4)+3)]['awayname'] ?></span>
                                            <?php } ?>
                                        </div>
                                    </li>
                                    <li>
                                        <p class="item-review-text">
                                            <?= $reviewMatchByMatchId[(($i*4)+3)]['review_text'] ?>
                                        </p>
                                    </li>
                                    <li class="pull-right">
                                        <div class="item-review-rate text-center">
                                            <div class="circle-rate">
                                                <h2 class="user-review-<?php echo $reviewMatchByMatchId[(($i*4)+3)]['user_review_id']?>"></h2>
                                                <small>%</small>
                                            </div>
                                            <p>Win Rate</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php for ($i=0;$i<ceil(count($reviewMatchByMatchId)/4);$i++){ ?>
                    <li data-target="#carousel-example-generic-<?php echo $key ?>" data-slide-to="<?=$i?>" class="<?=(($i==0)?'active':'')?>"></li>
                    <?php } ?>
<!--                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>-->
                </ol>
            </div>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
    <div class="col-sm-3 sidebar-ranking-review">
        <div class="clearfix box-pro-view">
            <table>
                <tr>
                    <td><h3><i class="fa fa-trophy" aria-hidden="true"></i> Top Professional</h3></td>
                    <td class="text-right"><a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></td>
                </tr>
            </table>
            <table class="table" id="table-topProfessional">

            </table>
        </div>
        <div class="clearfix box-player-view">
            <table>
                <tr>
                    <td><h3><i class="fa fa-trophy" aria-hidden="true"></i> Top Players</h3></td>
                    <td class="text-right"><a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></td>
                </tr>
            </table>
            <table class="table">
                <?php foreach ($topRankingAll as $key=>$value) {?>
                <tr>
                    <td><?php echo ($key+1)?></td>
                    <td><img src="https://graph.facebook.com/v2.8/<?php echo $value["fb_uid"]; ?>/picture?type=large"></td>
                    <td><h4><a style="color:#000000;" href="/profile.php?user_id=<?php echo $value['id']; ?>"><?php echo $value['name']; ?></a></h4></td>
                    <td><?php echo number_format($value['accuracy'], 2); ?>%</td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
</div>
<?php foreach ($match as $key=>$value){ ?>
<div class="modal fade model-review-match" matchId="<?php echo $value->match_id;?>"  id="showReviewGuru-<?php echo $value->match_id;?>" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="modalBody-showReviewGuru-<?php echo $value->match_id;?>">
            <div class="modal-header" style="padding: 0px;border-bottom: transparent;">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a style="padding: 15px 20px;" href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
                    <li role="presentation"><a style="padding: 15px 20px;" href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
                    <li role="presentation"><a style="padding: 15px 20px;" href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                    <li role="presentation"><a style="padding: 15px 20px;" href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
                </ul>
            </div>
            <div class="modal-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">home</div>
                    <div role="tabpanel" class="tab-pane" id="profile">profile</div>
                    <div role="tabpanel" class="tab-pane" id="messages">messages</div>
                    <div role="tabpanel" class="tab-pane" id="settings">settings</div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<script>
    $(document).ready(function () {

        $.ajax({
            url: "/json/checkWinRateReview.json",
            method: "POST",
            dataType: "JSON"
        }).done(function (response) {
            $.each(response, function (key, val) {
                $(".user-review-" + val.id).html(parseInt(val.winRate) + "%");
                // console.log(key+" :: "+val)
            })
        });

        $('li.btn-results-left,li.btn-results-right').on('click', function (e) {
            var number=parseInt($(this).attr('number'));
            if(number==2){
                number=0;
            }else {
                number+=1;
            }
            $(this).parent('ul').find('.li-results').addClass('hide');
            $(this).parent('ul').find('[number='+number+']').removeClass('hide');
            $(this).parent('ul').find('span.total-results').html(((number*8)+8));
            $(this).attr('number',number);
        });

        $('.model-review-match').each(function (index) {
            var matchId=$(this).attr('matchId');
            $.get( "reviewMatch.php?id="+matchId, function( data ) {
                // console.log("#modalBody-showReviewGuru-"+matchId);
                $("#modalBody-showReviewGuru-"+matchId).html( data );
            });
        })


    });

    $.get( "topProfessional.php", function( data ) {
        $( "#table-topProfessional" ).html( data );
    });

</script>

<?php //include 'footer.php';
include 'view/index/footer.php';
?>

</body>
</html>
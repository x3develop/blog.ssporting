<?php
require('simple_html_dom.php');
require_once 'model/SitemapInfo.php';
require_once 'model/SitemapSearch.php';
function safe($value)
{
    return urlencode($value);
}

?>
<div class="container">
    <h1>ผลการค้นหา "<?php echo(isset($_GET['q']) ? $_GET['q'] : '-'); ?>" จาก Youtube</h1>
</div>
<div class="container-search-result">

    <div class="container">
        <div class="row">
            <?php
            $videoidIn="/search-result-play?info=wqJsZYibWcI";
            var_dump(strpos($videoidIn, '&list='));
            $sitemapInfoObject = new SitemapInfo();
            $sitemapSearchObject = new SitemapSearch();
            $input = str_replace(' ', '+', safe($_GET['q']));
            $myUrl = "https://www.youtube.com/results?search_query=" . $input;
            $html = file_get_html($myUrl);
            $videos = array();
            $mostViewed = array();
            $i = 1;
            // save SiteMapInfo
            $sitemapSearchObject->add($_GET['q']);
            foreach ($html->find('div.yt-lockup-content') as $video) {
                $title = $video->find('h3.yt-lockup-title a', 0)->plaintext;
                $title = str_replace('^', ' ', $title);
                $title = str_replace('|', ' ', $title);
                $url = $video->find('h3.yt-lockup-title a', 0)->href;
                $videoid = str_replace("/watch?v=", "", $url);
                $duration = $video->find('h3.yt-lockup-title span', 0);
                $views = $video->find('ul.yt-lockup-meta-info li', 1);
                // save SiteMapSearch
                $sitemapInfoObject->add($videoid);
                ?>
                <div class="col-sm-4 col-md-2">
                    <div class="thumbnail">
                        <div class="thumbnail-post full-width inline-box">
                            <div class="crop"
                                 style="background-image: url('https://i1.ytimg.com/vi/<?= $videoid; ?>/mqdefault.jpg')"></div>
                        </div>
                        <div class="caption">
                            <h3><?php echo $title ?></h3>
                            <p class="text-center">
                                <a target="_blank"
                                   href="/search-result-play?info=<?php echo $videoid; ?>"
                                   class="btn btn-sm btn-primary btn-play" role="button">Play</a>
                            </p>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
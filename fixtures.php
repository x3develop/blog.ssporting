<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="js/jquery-ui/jquery-ui.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/utils.js"></script>
    <script>
        function flagNotFound(image) {
            image.src = "images/countries/0.png";
        }
        function imgError(image) {
            image.onerror = "";
            image.src = "images/teams_clean/team_default_32x32.png";
            return true;
        }

    </script>
</head>
<body>
<style>
    .odds {
        color: #0091c8;
        font-weight: bold !important;
    }
    span.disabled {
        background-color: #cccccc;
    }
</style>
<?php //include 'header.php'; ?>
<!--index menu-->
<?php
include_once $_SERVER["DOCUMENT_ROOT"] . "/model/MatchM.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/utility/DataMap.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/utility/LogoManager.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/utility/MatchUtils.php";


$nowday = \Carbon\Carbon::now();
$nowday->setTime(0, 0, 0);
$day = $nowday->toDateString();
if (!empty($_REQUEST['date'])) {
    $day = $_REQUEST['date'];
}
$hourNow = $nowday->hour;
$matchm = new MatchM();
$logoman = new LogoManager();
$datamap = new DataMap();
$matchutils = new MatchUtils();
//$livematch = $matchm->getLiveFile();
$livematch = $matchm->getMatchPack($day);
#print_r($livematch[0]);exit;

$gameinfos = $matchm->getGameInfoPack($day);
//echo count($livematch['all']) . "\n";
//echo count($livematch['bet']) . "\n";
//echo count($livematch['live']) . "\n";
$selectday = \Carbon\Carbon::createFromFormat('Y-m-d', $day);
$selectday->setTime(0, 0, 0);
$yesterday = clone $selectday;
$yesterday->subDay(1);
$tomorow = clone $selectday;
$tomorow->addDay(1);
?>
<?php include $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu.php'; ?>
<nav class="navbar navbar-menu-v2">
    <div class="container">
        <div class="nav-menutop nav-menu-main">
            <ul class="nav nav-pills">
                <li><a href=""><strong><i class="fa fa-futbol-o" aria-hidden="true"></i> SPORTBALL</a></strong></li>
                <li><a href="">หน้าหลัก</a></li>
                <li class="active-dropdown">
                    <a href="">ไฮไลท์บอล</a>
                    <span class="bx-menu-dropdown">
                            <table>
                                <tr>
                                    <td><a href="/videos.php?category=premier league"><i class="fa fa-play-circle-o"
                                                                                         aria-hidden="true"></i>
                                            พรีเมียร์ลีก</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/videos.php?category=la liga"><i class="fa fa-play-circle-o"
                                                                                  aria-hidden="true"></i> ลาลีกา</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/videos.php?category=bundesliga"><i class="fa fa-play-circle-o"
                                                                                     aria-hidden="true"></i> บุนเดสลีกา</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/videos.php?category=serie a"><i class="fa fa-play-circle-o"
                                                                                  aria-hidden="true"></i> ซีเรียอา</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/videos.php?category=league 1"><i class="fa fa-play-circle-o"
                                                                                   aria-hidden="true"></i> ลีกเอิง</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/videos.php?category=thai"><i class="fa fa-play-circle-o"
                                                                               aria-hidden="true"></i> ไทยพรีเมียลีก</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/videos.php?category=ucl"><i class="fa fa-play-circle-o"
                                                                              aria-hidden="true"></i>
                                            ยูฟ่าแชมเปียนลีก</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/videos.php?category=europa"><i class="fa fa-play-circle-o"
                                                                                 aria-hidden="true"></i> ยูโรป้า</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/videos.php?category=thai"><i class="fa fa-play-circle-o"
                                                                               aria-hidden="true"></i> ฟุตบอลทีมชาติ</a>
                                    </td>
                                </tr>
                            </table>
                        </span>
                </li>
                <li class="active-dropdown">
                    <a href="">ข่าวบอลต่างประเทศ</a>
                     <span class="bx-menu-dropdown">
                            <table>
                                <tr>
                                    <td><a href="/news.php?type=premier league&Page=1"><i class="fa fa-newspaper-o"
                                                                                          aria-hidden="true"></i>
                                            ข่าวพรีเมียร์ลีก</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/news.php?type=la liga&Page=1"><i class="fa fa-newspaper-o"
                                                                                   aria-hidden="true"></i>
                                            ข่าวลาลีกา</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/news.php?type=bundesliga&Page=1"><i class="fa fa-newspaper-o"
                                                                                      aria-hidden="true"></i>
                                            ข่าวบุนเดสลีกา</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/news.php?type=serie a&Page=1"><i class="fa fa-newspaper-o"
                                                                                   aria-hidden="true"></i> ข่าวซีเรียอา</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/news.php?type=ucl&Page=1"><i class="fa fa-newspaper-o"
                                                                               aria-hidden="true"></i>
                                            ข่าวยูฟ่าแชมเปียนลีก</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/news.php?type=europa&Page=1"><i class="fa fa-newspaper-o"
                                                                                  aria-hidden="true"></i>
                                            ข่าวยูโรป้า</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/news.php?type=thai&Page=1"><i class="fa fa-newspaper-o"
                                                                                aria-hidden="true"></i>
                                            ข่าวฟุตบอลทีมชาติ</a></td>
                                </tr>
                            </table>
                        </span>
                </li>
                <li><a href="">ข่าวบอลไทย</a></li>
                <li><a href="">ข่าวกีฬาอื่นๆ</a></li>
            </ul>
        </div>
    </div>
</nav>


<div class="wrapper-news-articles">
    <div class="containter-fluid">
        <div class="bx-head-fixtures bgtop">
            <div class="hilight-cover-top"></div>
            <div class="articles-title">
                <div class="container">
                    <div class="head-title-top pull-left" style="margin-top: 135px;">
                        <div style="font-size: 4em;">Fixtures</div>
                    </div>
                    <div class="head-title-top pull-right">
                        <div>รายการแข่งขันฟุตบอล Livescore ทุกลีก</div>
                        <div>สถิติการแข่งขัน เกมทายผล</div>
                    </div>
                </div>
            </div>
            <!--            <div class="background-image_image3"></div>-->
        </div>
    </div>
    <input type="hidden" id="page-info" zid="<?php echo $livematch['zid']; ?>">

    <div class="container" style="position: relative;">
        <div class="refresh-loading hide" id="div-refresh-loading"><img src="images/loading.gif"></div>

        <div style="margin-top: -45px;">
            <!-- Nav tabs -->
            <ul class="nav-bar-fix nav nav-tabs" role="tablist" style="border-bottom: 0px;">
                <li role="presentation"
                    class="<?php echo ($nowday->diffInDays($selectday, false) >= 0) ? 'active' : ''; ?>"><a
                        href="#matches" aria-controls="home" role="tab"
                        data-toggle="tab">All Matches
                        (<label class="all-match-count"><?php echo $livematch['allmatchcount']; ?></label>)</a></li>
                <li role="presentation"><a href="#live" aria-controls="profile" role="tab"
                                           data-toggle="tab" style="color: #f7f10b;">
                        <div class="blinking">Live (<label
                                class="live-match-count"><?php echo $livematch['livecount']; ?>
                            </label>)
                        </div>
                    </a></li>
                <li role="presentation"><a href="#games" aria-controls="messages" role="tab" data-toggle="tab">Games
                        (<label class="bet-match-count"><?php echo $livematch['betcount']; ?></label>)</a></li>
                <li role="presentation"
                    class="<?php echo ($nowday->diffInDays($selectday, false) < 0) ? 'active' : ''; ?>"><a
                        href="#result" aria-controls="profile" role="tab"
                        data-toggle="tab">Result (<label
                            class="result-match-count"><?php echo $livematch['resultcount']; ?>
                        </label>)</a></li>
                <li role="presentation"
                    class=""><a
                        href="#select-noti" aria-controls="profile" role="tab"
                        data-toggle="tab"><i class="fa fa-bell-o"></i> Select (<label
                            class="result-match-count" id="select-noti-count">0
                        </label>)</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel"
                     class="tab-pane <?php echo ($nowday->diffInDays($selectday, false) >= 0) ? 'active' : ''; ?>"
                     id="matches">
                    <div class="wrapper-fixtures" style="margin-top: 15px;">
                        <div class="tab-filter mg-bt15">
                            <div class="pull-left">
                                <div class="times-content">Filter by League</div>
                                <div class="dropdown"
                                     style="float:left; margin-right: 10px; border-bottom: solid 1px #0091c8;">

                                    <select class="league-select">
                                        <option value="all" selected>All LEAGUE</option>
                                        <?php foreach ($livematch['leaguelist'] as $key => $l) { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $l; ?></option>
                                        <?php } ?>
                                    </select>

                                </div>
                                <span style="display: none;" class="label-reset">
<!--                                    Filter Reset -->
                                    Refresh
                                    <i class="fa fa-refresh"></i></span>
                            </div>
                            <div class="pull-right">
                                <div class="date-slider">
                                    <ul>
                                        <li class="match-date" date=""><a
                                                href="/fixtures.php?date=<?php echo $yesterday->toDateString(); ?>"><i
                                                    class="fa fa-chevron-circle-left"></i></a></li>
                                        <li class="match-date" date=""><a
                                                href="/fixtures.php?date=<?php echo $yesterday->toDateString(); ?>"><?php echo ($nowday->isSameDay($yesterday)) ? 'Today' : $yesterday->format('d M Y'); ?></a>
                                        </li>
                                        <li class="match-date" date=""><a style="color: #000000;"
                                                                          href="/fixtures.php?date=<?php echo $selectday->toDateString(); ?>"><?php echo ($nowday->isSameDay($selectday)) ? 'Today' : $selectday->format('d M Y'); ?></a>
                                        </li>
                                        <li class="match-date" date=""><a
                                                href="/fixtures.php?date=<?php echo $tomorow->toDateString(); ?>"><?php echo ($nowday->isSameDay($tomorow)) ? 'Today' : $tomorow->format('d M Y'); ?></a>
                                        </li>
                                        <li class="li-next" date=""><a
                                                href="/fixtures.php?date=<?php echo $tomorow->toDateString(); ?>"><i
                                                    class="fa fa-chevron-circle-right"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="box-results allgame-box">
                            <?php foreach ($livematch['allmatch'] as $leaguekey => $league) { ?>
                                <div class="league-box" lid="<?php echo $leaguekey; ?>">
                                <span>
                                    <img onerror="flagNotFound(this)"
                                         src="<?php echo $logoman->getCountiesPix($livematch['countrylist'][$leaguekey]); ?>">
                                    <label
                                        class="league-name"><?php echo $livematch['leaguelist'][$leaguekey]; ?></label>
                                </span>

                                    <div>
                                        <table class="table-fixtures table-striped">
                                            <?php foreach ($league as $key => $match) { ?>
                                                <?php $m = $datamap->mapByKey($livematch['keylist']['match'], $livematch['all'][$leaguekey][$match]); ?>
                                                <?php
                                                $mhdp = "";
                                                $hhdp = "";
                                                $ghdp = "";
                                                if (array_key_exists($m['mid'], $livematch["odds"]) && is_numeric($livematch["odds"][$m['mid']][2])) {
                                                    $mhdp = $livematch["odds"][$m['mid']][2];
                                                    $hhdp = $livematch["odds"][$m['mid']][3];
                                                    $ghdp = $livematch["odds"][$m['mid']][4];
                                                } ?>
                                                <tr class="live-matche"
                                                    mid="<?php echo $m['mid']; ?>">
                                                    <td>
                                                        <?php
                                                        $dt = \Carbon\Carbon::createFromFormat("Y-m-d H:i:m", $m['date']);
                                                        $dt->addHours(7);
                                                        ?>
                                                        <div
                                                            class="txt-blue showdate <?php echo ($m['sid'] > 1 && $m['sid'] < 5) ? 'nowshowing' : ''; ?>"
                                                            mid="<?php echo $m['mid']; ?>"
                                                            sid="<?php echo $m['sid']; ?>" c0="<?php echo $m['c0']; ?>"
                                                            c1="<?php echo $m['c1']; ?>" c2="<?php echo $m['c2']; ?>"
                                                            cx="<?php echo $m['cx']; ?>">
                                                            <?php
                                                            $minuteat = $dt->format("H:i");
                                                            if ($m['sid'] >= 5) {
                                                                $minuteat = "FT";
                                                            } else if ($m['sid'] == 3) {
                                                                $minuteat = "HT";
                                                            }
                                                            echo $minuteat;
                                                            ?>
                                                        </div>
                                                    </td>
                                                    <td><label
                                                            class="home-name <?php echo (is_numeric($mhdp) && floatval($mhdp) < 0) ? 'hdp-team' : ''; ?> <?php echo ((is_numeric($mhdp) && floatval($mhdp) == 0) && floatval($hhdp) < floatval($ghdp)) ? 'hdp-team' : ''; ?>"><?php echo $m['hn']; ?></label>
                                                        <img class="home-logo"
                                                             src="<?php echo $logoman->getLogo($m['hid'], 0); ?>">
                                                    </td>
                                                    <td>
                                                        <?php if (is_numeric($mhdp)) { ?>
                                                            <a href="/game.php?mid=<?php echo $m['mid'] ?>"
                                                               class="bx-label-hdp"><?php echo $livematch["odds"][$m['mid']][2]; ?></a>
                                                        <?php } else { ?>
                                                            <div class="bx-label-hdp">-</div>
                                                        <?php } ?>
                                                    </td>
                                                    <td><img class="guest-logo"
                                                             src="<?php echo $logoman->getLogo($m['gid'], 0); ?>">
                                                        <label
                                                            class="guest-name <?php echo (is_numeric($mhdp) && floatval($mhdp) > 0) ? 'hdp-team' : ''; ?> <?php echo ((is_numeric($mhdp) && floatval($mhdp) == 0) && floatval($hhdp) > floatval($ghdp)) ? 'hdp-team' : ''; ?>"><?php echo $m['gn']; ?></label>
                                                    </td>
                                                    <td>
                                                        <div class="txt-blue">
                                                            <a class="now-score" mid="<?php echo $m['mid']; ?>"
                                                               href="/statistics.php?mid=<?php echo $m['mid']; ?>"><?php echo str_replace('-', ' - ', $m['s1']); ?></a>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="live-channel">
                                                            <?php if (array_key_exists($m['mid'], $gameinfos["game_info"])) { ?>
                                                                <?php if (!empty($gameinfos["game_info"][$m['mid']][6])) { ?>
                                                                    <img src="/images/fox.png">
                                                                    <div class="popover-tooltip-events tooltip-hover">
                                                                        <strong> <?php echo $gameinfos["game_info"][$m['mid']][6]; ?> </strong>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <?php if (array_key_exists($m['mid'], $livematch["odds"]) && is_numeric($livematch["odds"][$m['mid']][2])) { ?>
                                                            <div class="label-reset">
                                                                <a class="game-play" target="_blank"
                                                                   href="<?php echo "/game.php?mid=" . $m['mid']; ?>">GAME</a>
                                                            </div>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <div class="set-noti" lid="<?php echo $leaguekey; ?>"
                                                             mid="<?php echo $m['mid']; ?>">
                                                            <i class="fa fa-bell-o white-bell"></i>
                                                            <i class="fa fa-bell blue-bell hide"></i>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a class="h2h" target="_blank"
                                                           href="<?php echo "/statistics.php?mid=" . $m['mid']; ?>"><img
                                                                src="/images/stats.png"></a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="live">
                    <div class="wrapper-fixtures" style="margin-top: 15px;">
                        <div class="tab-filter mg-bt15">
                            <div class="pull-left" style="width: 30%;">
                                <div class="times-content">Filter by League</div>
                                <div class="dropdown"
                                     style="float:left; margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                    <select class="league-select">
                                        <option value="all" selected>All LEAGUE</option>
                                        <?php foreach ($livematch['leaguelist'] as $key => $l) { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $l; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="pull-left" style="width: 40%;padding-left: 15%;"><span class="label-reset">Refresh<i class="fa fa-refresh"></i></span></div>
                            <div class="pull-right" style="width: 30%;">
                                <div class="date-slider">
                                    <ul>
                                        <li class="match-date" date=""><a
                                                href="/fixtures.php?date=<?php echo $yesterday->toDateString(); ?>"><i
                                                    class="fa fa-chevron-circle-left"></i></a></li>
                                        <li class="match-date" date=""><a
                                                href="/fixtures.php?date=<?php echo $yesterday->toDateString(); ?>"><?php echo ($nowday->isSameDay($yesterday)) ? 'Today' : $yesterday->format('d M Y'); ?></a>
                                        </li>
                                        <li class="match-date" date=""><a style="color: #000000;"
                                                                          href="/fixtures.php?date=<?php echo $selectday->toDateString(); ?>"><?php echo ($nowday->isSameDay($selectday)) ? 'Today' : $selectday->format('d M Y'); ?></a>
                                        </li>
                                        <li class="match-date" date=""><a
                                                href="/fixtures.php?date=<?php echo $tomorow->toDateString(); ?>"><?php echo ($nowday->isSameDay($tomorow)) ? 'Today' : $tomorow->format('d M Y'); ?></a>
                                        </li>
                                        <li class="li-next" date=""><a
                                                href="/fixtures.php?date=<?php echo $tomorow->toDateString(); ?>"><i
                                                    class="fa fa-chevron-circle-right"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>

                        <div class="box-results live-box">
                            <?php foreach ($livematch['live'] as $leaguekey => $league) { ?>
                                <div class="league-box" lid="<?php echo $leaguekey; ?>">
                                <span>
                                    <img onerror="flagNotFound(this)"
                                         src="<?php echo $logoman->getCountiesPix($livematch['countrylist'][$leaguekey]); ?>">
                                    <label
                                        class="league-name"><?php echo $livematch['leaguelist'][$leaguekey]; ?></label>
                                </span>

                                    <div>
                                        <table class="table-fixtures table-striped">
                                            <?php foreach ($league as $key => $match) { ?>
                                                <?php $m = $datamap->mapByKey($livematch['keylist']['match'], $livematch['all'][$leaguekey][$match]); ?>
                                                <?php
                                                $mhdp = "";
                                                $hhdp = "";
                                                $ghdp = "";
                                                if (array_key_exists($m['mid'], $livematch["odds"]) && is_numeric($livematch["odds"][$m['mid']][2])) {
                                                    $mhdp = $livematch["odds"][$m['mid']][2];
                                                    $hhdp = $livematch["odds"][$m['mid']][3];
                                                    $ghdp = $livematch["odds"][$m['mid']][4];
                                                } ?>
                                                <tr class="live-matche"
                                                    mid="<?php echo $m['mid']; ?>">
                                                    <td>
                                                        <?php
                                                        $dt = \Carbon\Carbon::createFromFormat("Y-m-d H:i:m", $m['date']);
                                                        $dt->addHours(7);
                                                        ?>
                                                        <div
                                                            class="txt-blue showdate <?php echo ($m['sid'] > 1 && $m['sid'] < 5) ? 'nowshowing' : ''; ?>"
                                                            mid="<?php echo $m['mid']; ?>"
                                                            sid="<?php echo $m['sid']; ?>" c0="<?php echo $m['c0']; ?>"
                                                            c1="<?php echo $m['c1']; ?>" c2="<?php echo $m['c2']; ?>"
                                                            cx="<?php echo $m['cx']; ?>">
                                                            <?php
                                                            $minuteat = $dt->format("H:i");
                                                            if ($m['sid'] >= 5) {
                                                                $minuteat = "FT";
                                                            } else if ($m['sid'] == 3) {
                                                                $minuteat = "HT";
                                                            }
                                                            echo $minuteat;
                                                            ?>
                                                        </div>
                                                    </td>
                                                    <td><label
                                                            class="home-name <?php echo (is_numeric($mhdp) && floatval($mhdp) < 0) ? 'hdp-team' : ''; ?> <?php echo ((is_numeric($mhdp) && floatval($mhdp) == 0) && floatval($hhdp) < floatval($ghdp)) ? 'hdp-team' : ''; ?>"><?php echo $m['hn']; ?></label>
                                                        <img class="home-logo"
                                                             src="<?php echo $logoman->getLogo($m['hid'], 0); ?>">
                                                    </td>
                                                    <td>
                                                        <?php if (array_key_exists($m['mid'], $livematch["odds"]) && is_numeric($livematch["odds"][$m['mid']][2])) { ?>
                                                            <a href="/game.php?mid=<?php echo $m['mid'] ?>"
                                                               class="bx-label-hdp"><?php echo $livematch["odds"][$m['mid']][2]; ?></a>
                                                            <?php
                                                            $betable[$m['lid']][] = $m['mid'];
                                                            ?>
                                                        <?php } else { ?>
                                                            <div class="bx-label-hdp">-</div>
                                                        <?php } ?>
                                                    </td>
                                                    <td><img class="guest-logo"
                                                             src="<?php echo $logoman->getLogo($m['gid'], 0); ?>">
                                                        <label
                                                            class="guest-name <?php echo (is_numeric($mhdp) && floatval($mhdp) > 0) ? 'hdp-team' : ''; ?> <?php echo ((is_numeric($mhdp) && floatval($mhdp) == 0) && floatval($hhdp) > floatval($ghdp)) ? 'hdp-team' : ''; ?>"><?php echo $m['gn']; ?></label>
                                                    </td>
                                                    <td>
                                                        <div class="txt-blue">
                                                            <a class="now-score" mid="<?php echo $m['mid']; ?>"
                                                               href="/statistics.php?mid=<?php echo $m['mid']; ?>"><?php echo str_replace('-', ' - ', $m['s1']); ?></a>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="live-channel">
                                                            <?php if (array_key_exists($m['mid'], $gameinfos["game_info"])) { ?>
                                                                <?php if (!empty($gameinfos["game_info"][$m['mid']][6])) { ?>
                                                                    <img src="/images/fox.png">
                                                                    <div class="popover-tooltip-events tooltip-hover">
                                                                        <strong> <?php echo $gameinfos["game_info"][$m['mid']][6]; ?> </strong>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <?php if (array_key_exists($m['mid'], $livematch["odds"]) && is_numeric($livematch["odds"][$m['mid']][2])) { ?>
                                                            <div class="label-reset">
                                                                <a class="game-play" target="_blank"
                                                                   href="
                                                        <?php echo "/game.php?mid=" . $m['mid']; ?>">GAME</a>
                                                            </div>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <div class="set-noti" lid="<?php echo $leaguekey; ?>"
                                                             mid="<?php echo $m['mid']; ?>">
                                                            <i class="fa fa-bell-o white-bell"></i>
                                                            <i class="fa fa-bell blue-bell hide"></i>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a class="h2h" target="_blank"
                                                           href="<?php echo "/statistics.php?mid=" . $m['mid']; ?>"><img
                                                                src="/images/stats.png"></a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="games">
                    <div class="wrapper-fixtures" style="margin-top: 15px;">
                        <div class="tab-filter mg-bt15">
                            <div class="pull-left">
                                <div class="times-content">Filter by League</div>
                                <div class="dropdown"
                                     style="float:left; margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                    <select class="league-select">
                                        <option value="all" selected>ALL LEAGUE</option>
                                        <?php foreach ($livematch['leaguelist'] as $key => $l) { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $l; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <span class="label-reset" style="display: none;">
<!--                                    Filter Reset -->
                                    Refresh
                                    <i class="fa fa-refresh"></i></span>
                            </div>
                            <div class="pull-right">
                                <div class="date-slider">
                                    <ul>
                                        <li class="match-date" date=""><a
                                                href="/fixtures.php?date=<?php echo $yesterday->toDateString(); ?>"><i
                                                    class="fa fa-chevron-circle-left"></i></a></li>
                                        <li class="match-date" date=""><a
                                                href="/fixtures.php?date=<?php echo $yesterday->toDateString(); ?>"><?php echo ($nowday->isSameDay($yesterday)) ? 'Today' : $yesterday->format('d M Y'); ?></a>
                                        </li>
                                        <li class="match-date" date=""><a style="color: #000000;"
                                                                          href="/fixtures.php?date=<?php echo $selectday->toDateString(); ?>"><?php echo ($nowday->isSameDay($selectday)) ? 'Today' : $selectday->format('d M Y'); ?></a>
                                        </li>
                                        <li class="match-date" date=""><a
                                                href="/fixtures.php?date=<?php echo $tomorow->toDateString(); ?>"><?php echo ($nowday->isSameDay($tomorow)) ? 'Today' : $tomorow->format('d M Y'); ?></a>
                                        </li>
                                        <li class="li-next" date=""><a
                                                href="/fixtures.php?date=<?php echo $tomorow->toDateString(); ?>"><i
                                                    class="fa fa-chevron-circle-right"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>

                        <div class="box-results bet-box">
                            <?php foreach ($livematch['bet'] as $leaguekey => $league) { ?>
                                <div class="league-box" lid="<?php echo $leaguekey; ?>">
                                <span>
                                    <img onerror="flagNotFound(this)"
                                         src="<?php echo $logoman->getCountiesPix($livematch['countrylist'][$leaguekey]); ?>">
                                    <label
                                        class="league-name"><?php echo $livematch['leaguelist'][$leaguekey]; ?></label>
                                </span>

                                    <div>
                                        <table class="table-fixtures table-striped">
                                            <?php foreach ($league as $key => $match) { ?>
                                                <?php $m = $datamap->mapByKey($livematch['keylist']['match'], $livematch['all'][$leaguekey][$match]); ?>
                                                <?php
                                                $mhdp = "";
                                                $hhdp = "";
                                                $ghdp = "";
                                                if (array_key_exists($m['mid'], $livematch["odds"]) && is_numeric($livematch["odds"][$m['mid']][2])) {
                                                    $mhdp = $livematch["odds"][$m['mid']][2];
                                                    $hhdp = $livematch["odds"][$m['mid']][3];
                                                    $ghdp = $livematch["odds"][$m['mid']][4];
                                                } ?>
                                                <tr class="live-matche"
                                                    mid="<?php echo $m['mid']; ?>">
                                                    <td>
                                                        <?php
                                                        $dt = \Carbon\Carbon::createFromFormat("Y-m-d H:i:m", $m['date']);
                                                        $dt->addHours(7);
                                                        ?>
                                                        <div
                                                            class="txt-blue showdate <?php echo ($m['sid'] > 1 && $m['sid'] < 5) ? 'nowshowing' : ''; ?>"
                                                            mid="<?php echo $m['mid']; ?>"
                                                            sid="<?php echo $m['sid']; ?>" c0="<?php echo $m['c0']; ?>"
                                                            c1="<?php echo $m['c1']; ?>" c2="<?php echo $m['c2']; ?>"
                                                            cx="<?php echo $m['cx']; ?>">
                                                            <?php
                                                            $minuteat = $dt->format("H:i");
                                                            if ($m['sid'] >= 5) {
                                                                $minuteat = "FT";
                                                            } else if ($m['sid'] == 3) {
                                                                $minuteat = "HT";
                                                            }
                                                            echo $minuteat;
                                                            ?>
                                                        </div>
                                                    </td>
                                                    <td><label
                                                            class="home-name <?php echo (is_numeric($mhdp) && floatval($mhdp) < 0) ? 'hdp-team' : ''; ?> <?php echo ((is_numeric($mhdp) && floatval($mhdp) == 0) && floatval($hhdp) < floatval($ghdp)) ? 'hdp-team' : ''; ?>"><?php echo $m['hn']; ?></label>
                                                        <img class="home-logo"
                                                             src="<?php echo $logoman->getLogo($m['hid'], 0); ?>">
                                                    </td>
                                                    <td>
                                                        <?php if (array_key_exists($m['mid'], $livematch["odds"]) && is_numeric($livematch["odds"][$m['mid']][2])) { ?>
                                                            <a href="/game.php?mid=<?php echo $m['mid'] ?>"
                                                               class="bx-label-hdp"><?php echo $livematch["odds"][$m['mid']][2]; ?></a>
                                                            <?php
                                                            $betable[$m['lid']][] = $m['mid'];
                                                            ?>
                                                        <?php } else { ?>
                                                            <div class="bx-label-hdp">-</div>
                                                        <?php } ?>
                                                    </td>
                                                    <td><img class="guest-logo"
                                                             src="<?php echo $logoman->getLogo($m['gid'], 0); ?>">
                                                        <label
                                                            class="guest-name <?php echo (is_numeric($mhdp) && floatval($mhdp) > 0) ? 'hdp-team' : ''; ?> <?php echo ((is_numeric($mhdp) && floatval($mhdp) == 0) && floatval($hhdp) > floatval($ghdp)) ? 'hdp-team' : ''; ?>"><?php echo $m['gn']; ?></label>
                                                    </td>
                                                    <td>
                                                        <div class="txt-blue">
                                                            <a href="/statistics.php?mid=<?php echo $m['mid']; ?>"><?php echo str_replace('-', ' - ', $m['s1']); ?></a>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="live-channel">
                                                            <?php if (array_key_exists($m['mid'], $gameinfos["game_info"])) { ?>
                                                                <?php if (!empty($gameinfos["game_info"][$m['mid']][6])) { ?>
                                                                    <img src="/images/fox.png">
                                                                    <div class="popover-tooltip-events tooltip-hover">
                                                                        <strong> <?php echo $gameinfos["game_info"][$m['mid']][6]; ?> </strong>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <?php if (array_key_exists($m['mid'], $livematch["odds"]) && is_numeric($livematch["odds"][$m['mid']][2])) { ?>
                                                            <div class="label-reset">
                                                                <a class="game-play" target="_blank"
                                                                   href="<?php echo "/game.php?mid=" . $m['mid']; ?>">GAME</a>
                                                            </div>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <div class="set-noti" lid="<?php echo $leaguekey; ?>"
                                                             mid="<?php echo $m['mid']; ?>">
                                                            <i class="fa fa-bell-o white-bell"></i>
                                                            <i class="fa fa-bell blue-bell hide"></i>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a class="h2h" target="_blank"
                                                           href="<?php echo "/statistics.php?mid=" . $m['mid']; ?>"><img
                                                                src="/images/stats.png"></a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div role="tabpanel"
                     class="tab-pane <?php echo ($nowday->diffInDays($selectday, false) < 0) ? 'active' : ''; ?>"
                     id="result">
                    <div class="wrapper-fixtures" style="margin-top: 15px;">
                        <div class="tab-filter mg-bt15">
                            <div class="pull-left">
                                <div class="times-content">Filter by League</div>
                                <div class="dropdown"
                                     style="float:left; margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                    <select class="league-select">
                                        <option value="all" selected>ALL LEAGUE</option>
                                        <?php foreach ($livematch['leaguelist'] as $key => $l) { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $l; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <span class="label-reset" style="display: none;">
<!--                                    Filter Reset-->
                                    Refresh
                                    <i class="fa fa-refresh"></i></span>
                            </div>
                            <div class="pull-right">
                                <div class="date-slider">
                                    <ul>
                                        <li class="match-date" date=""><a
                                                href="/fixtures.php?date=<?php echo $yesterday->toDateString(); ?>"><i
                                                    class="fa fa-chevron-circle-left"></i></a></li>
                                        <li class="match-date" date=""><a
                                                href="/fixtures.php?date=<?php echo $yesterday->toDateString(); ?>"><?php echo ($nowday->isSameDay($yesterday)) ? 'Today' : $yesterday->format('d M Y'); ?></a>
                                        </li>
                                        <li class="match-date" date=""><a style="color: #000000;"
                                                                          href="/fixtures.php?date=<?php echo $selectday->toDateString(); ?>"><?php echo ($nowday->isSameDay($selectday)) ? 'Today' : $selectday->format('d M Y'); ?></a>
                                        </li>
                                        <li class="match-date" date=""><a
                                                href="/fixtures.php?date=<?php echo $tomorow->toDateString(); ?>"><?php echo ($nowday->isSameDay($tomorow)) ? 'Today' : $tomorow->format('d M Y'); ?></a>
                                        </li>
                                        <li class="li-next" date=""><a
                                                href="/fixtures.php?date=<?php echo $tomorow->toDateString(); ?>"><i
                                                    class="fa fa-chevron-circle-right"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>

                        <div class="box-results result-box">
                            <?php foreach ($livematch['result'] as $leaguekey => $league) { ?>
                                <div class="league-box" lid="<?php echo $leaguekey; ?>">
                                <span>
                                    <img onerror="flagNotFound(this)"
                                         src="<?php echo $logoman->getCountiesPix($livematch['countrylist'][$leaguekey]); ?>">
                                    <label
                                        class="league-name"><?php echo $livematch['leaguelist'][$leaguekey]; ?></label>
                                </span>

                                    <div>
                                        <table class="table-fixtures table-striped">
                                            <?php foreach ($league as $key => $match) { ?>
                                                <?php $m = $datamap->mapByKey($livematch['keylist']['match'], $livematch['all'][$leaguekey][$match]); ?>
                                                <?php
                                                $mhdp = "";
                                                $hhdp = "";
                                                $ghdp = "";
                                                if (array_key_exists($m['mid'], $livematch["odds"]) && is_numeric($livematch["odds"][$m['mid']][2])) {
                                                    $mhdp = $livematch["odds"][$m['mid']][2];
                                                    $hhdp = $livematch["odds"][$m['mid']][3];
                                                    $ghdp = $livematch["odds"][$m['mid']][4];
                                                } ?>
                                                <tr class="live-matche"
                                                    mid="<?php echo $m['mid']; ?>">
                                                    <td>
                                                        <?php
                                                        $dt = \Carbon\Carbon::createFromFormat("Y-m-d H:i:m", $m['date']);
                                                        $dt->addHours(7);
                                                        ?>
                                                        <div
                                                            class="txt-blue showdate"><?php echo(($m['sid'] >= 5) ? 'FT' : $dt->format("H:i")) ?></div>
                                                    </td>
                                                    <td>
                                                        <label
                                                            class="home-name <?php echo (is_numeric($mhdp) && floatval($mhdp) < 0) ? 'hdp-team' : ''; ?> <?php echo ((is_numeric($mhdp) && floatval($mhdp) == 0) && floatval($hhdp) < floatval($ghdp)) ? 'hdp-team' : ''; ?>">
                                                            <div class="cards">
                                                                <?php for ($i = 0; $i < $m['hrci']; $i++) { ?>
                                                                    <img src="images/red-card.png">
                                                                <?php } ?>
                                                            </div>
                                                            <?php echo $m['hn']; ?>
                                                        </label>
                                                        <img class="home-logo"
                                                             src="<?php echo $logoman->getLogo($m['hid'], 0); ?>">
                                                    </td>
                                                    <td>
                                                        <div class="txt-blue text-center">
                                                            <a href="/statistics.php?mid=<?php echo $m['mid']; ?>" style="font-weight: bold;"><?php echo str_replace('-', ' - ', $m['s1']); ?></a>
                                                        </div>
                                                    </td>
                                                    <td><img class="guest-logo"
                                                             src="<?php echo $logoman->getLogo($m['gid'], 0); ?>">
                                                        <label
                                                            class="guest-name <?php echo (is_numeric($mhdp) && floatval($mhdp) > 0) ? 'hdp-team' : ''; ?> <?php echo ((is_numeric($mhdp) && floatval($mhdp) == 0) && floatval($hhdp) > floatval($ghdp)) ? 'hdp-team' : ''; ?>">
                                                            <?php echo $m['gn']; ?>
                                                            <div class="cards">
                                                                <?php for ($i = 0; $i < $m['grci']; $i++) { ?>
                                                                    <img src="images/red-card.png">
                                                                <?php } ?>
                                                            </div>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <?php if (array_key_exists($m['mid'], $livematch["odds"]) && is_numeric($livematch["odds"][$m['mid']][2])) { ?>
                                                            <a href="/game.php?mid=<?php echo $m['mid'] ?>"
                                                               class="bx-label-hdp"><?php echo $livematch["odds"][$m['mid']][2]; ?></a>
                                                            <?php
                                                            $betable[$m['lid']][] = $m['mid'];
                                                            ?>
                                                        <?php } else { ?>
                                                            <div class="bx-label-hdp">-</div>
                                                        <?php } ?>
                                                    </td>
                                                    <td style="font-size: 12px; color: #888;"><label class="<?php echo empty($m['s2'])?'hide':''; ?>"><?php echo 'HT('.str_replace('-', ' - ', $m['s2']).')'; ?></label></td>
                                                    <td>
                                                        <div class="live-channel">
                                                            <?php if (array_key_exists($m['mid'], $gameinfos["game_info"])) { ?>
                                                                <?php if (!empty($gameinfos["game_info"][$m['mid']][6])) { ?>
                                                                    <img src="/images/fox.png">
                                                                    <div class="popover-tooltip-events tooltip-hover">
                                                                        <strong> <?php echo $gameinfos["game_info"][$m['mid']][6]; ?> </strong>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <?php if (array_key_exists($m['mid'], $livematch["odds"]) && is_numeric($livematch["odds"][$m['mid']][2])) { ?>
                                                            <div class="label-reset">
                                                                <a class="game-play" target="_blank"
                                                                   href="<?php echo "/game.php?mid=" . $m['mid']; ?>">GAME</a>
                                                            </div>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <div class="ended-noti"><i class="fa fa-bell"></i></div>
                                                    </td>
                                                    <td>
                                                        <a class="h2h" target="_blank"
                                                           href="<?php echo "/statistics.php?mid=" . $m['mid']; ?>"><img
                                                                src="/images/stats.png"></a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="select-noti">
                    <div class="wrapper-fixtures" style="margin-top: 15px;">
                        <div class="tab-filter mg-bt15">
                            <div class="pull-left" style="width: 100%;padding-left: 50%;">
                                <span style="text-align: center;" class="label-reset">Refresh<i class="fa fa-refresh"></i></span>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                        <div class="box-results noti-box">
                            <div class="league-box" lid="0">
                                <span>
                                    <img onerror="flagNotFound(this)" src="/images/countries/59.png">
                                    <label class="league-name">National League North</label>
                                </span>

                                <div>
                                    <table class="table-fixtures table-striped">
                                        <tbody>
                                        <tr class="live-matche">
                                            <td>
                                                <div class="txt-blue showdate">FT</div>
                                            </td>
                                            <td><label class="home-name">
                                                    <div class="cards"><img src="images/red-card.png"> <img
                                                            src="images/red-card.png"></div>
                                                    Curzon Ashton</label>
                                                <img class="home-logo" src="images/teams_clean/team_default_32x32.png">
                                            </td>
                                            <td>
                                                <div class="bx-label-hdp">-</div>
                                            </td>
                                            <td><img class="guest-logo" src="images/teams_clean/team_default_32x32.png">
                                                <label class="guest-name  ">United of Manchester
                                                    <div class="cards"><img src="images/red-card.png"> <img
                                                            src="images/red-card.png"></div>
                                                </label>
                                            </td>
                                            <td>
                                                <div class="txt-blue">
                                                    <a href="/statistics.php?mid=1535945">1 - 2</a>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="live-channel"></div>
                                            </td>
                                            <td></td>
                                            <td>
                                                <div class="active-select-noti"><i class="fa fa-bell"></i></div>
                                            </td>
                                            <td>
                                                <a class="h2h" target="_blank" href="/statistics.php?mid=1535945"><img
                                                        src="/images/stats.png"></a>
                                            </td>
                                        </tr>
                                        <tr class="live-matche">
                                            <td>
                                                <div class="txt-blue showdate">FT</div>
                                            </td>
                                            <td><label class="home-name  ">Curzon Ashton</label>
                                                <img class="home-logo" src="images/teams_clean/team_default_32x32.png">
                                            </td>
                                            <td>
                                                <div class="bx-label-hdp">-</div>
                                            </td>
                                            <td><img class="guest-logo" src="images/teams_clean/team_default_32x32.png">
                                                <label class="guest-name  ">United of Manchester</label>
                                            </td>
                                            <td>
                                                <div class="txt-blue">
                                                    <a href="/statistics.php?mid=1535945">1 - 2</a>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="live-channel"></div>
                                            </td>
                                            <td></td>
                                            <td>
                                                <div class="active-select-noti"><i class="fa fa-bell"></i></div>
                                            </td>
                                            <td>
                                                <a class="h2h" target="_blank" href="/statistics.php?mid=1535945"><img
                                                        src="/images/stats.png"></a>
                                            </td>
                                        </tr>
                                        <tr class="live-matche">
                                            <td>
                                                <div class="txt-blue showdate">FT</div>
                                            </td>
                                            <td><label class="home-name  ">Curzon Ashton</label>
                                                <img class="home-logo" src="images/teams_clean/team_default_32x32.png">
                                            </td>
                                            <td>
                                                <div class="bx-label-hdp">-</div>
                                            </td>
                                            <td><img class="guest-logo" src="images/teams_clean/team_default_32x32.png">
                                                <label class="guest-name  ">United of Manchester</label>
                                            </td>
                                            <td>
                                                <div class="txt-blue">
                                                    <a href="/statistics.php?mid=1535945">1 - 2</a>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="live-channel"></div>
                                            </td>
                                            <td></td>
                                            <td>
                                                <div class="active-select-noti"><i class="fa fa-bell"></i></div>
                                            </td>
                                            <td>
                                                <a class="h2h" target="_blank" href="/statistics.php?mid=1535945"><img
                                                        src="/images/stats.png"></a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="text-center" style="margin-top: 25px;">
        <img src="images/11111.jpg" style="max-width: 100%;">
    </div>
</div>
<?php //include 'footer.php';
include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/footer.php';
?>
<!--<script src="js/fixtures.js"></script>-->
<script src="/js/live.js"></script>
<script>
    function blinker() {
        $('.blinking').fadeOut(500);
        $('.blinking').fadeIn(500);
    }
    setInterval(blinker, 1500);
</script>

</body>
</html>
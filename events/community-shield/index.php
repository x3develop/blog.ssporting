<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="กิจกรรมตามหาลูกสุดท้าย"/>
    <meta property="og:description" content="ทายว่าประตูสุดท้าย แมนซิตี้ VS เซลชี ยิงนาทีเท่าไหร่"/>
    <meta property="og:image" content="https://ngoal.com/events/community-shield/img/fb_ads.png"/>

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link href="/events/community-shield/css/select-item-style.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.css">
    <!--    <link rel="stylesheet" href="/css/reset.css">-->

    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.js"></script>

</head>
<?php
require_once __DIR__ . "/bootstrap.php";

$isMobile = (bool)preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet' .
    '|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]' .
    '|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', $_SERVER['HTTP_USER_AGENT']);

if ($isMobile) {
    header('Location: /events/community-shield/index-mobile.php');
}
?>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="container-select-item">
    <div class="header">
        <img src="/events/community-shield/img/title.png">
    </div>
    <div class="content-3col">
        <div class="box-col-1">
            <div class="box-col-1-hover">
                <a class="btn btn-lg btn-default" type="submit" href="/events/community-shield/play.php?reward=3">เลือก</a>
            </div>
            <div class="box-col-1-static">
                <img src="/events/community-shield/img/static01.jpg">
            </div>

        </div>
        <div class="box-col-2">
            <div class="box-col-1-hover">
                <a class="btn btn-lg btn-default" type="submit" href="/events/community-shield/play.php?reward=1">เลือก</a>
            </div>
            <div class="box-col-1-static">
                <img src="/events/community-shield/img/static02.jpg">
            </div>

        </div>
        <div class="box-col-3">
            <div class="box-col-1-hover">
                <a class="btn btn-lg btn-default" type="submit" href="/events/community-shield/play.php?reward=2">เลือก</a>
            </div>
            <div class="box-col-1-static">
                <img src="/events/community-shield/img/static03.jpg">
            </div>

        </div>
        <div style="clear: both"></div>
    </div>
    <div class="footer1">
        <u>กิจกรรมตามหาลูกสุดท้าย</u> รางวัลแบ่งเป็น 3 ประเภท รางวัลเงิดสด, สกิน ROV, สกิน PUBG
    </div>
</div>
</body>
</html>
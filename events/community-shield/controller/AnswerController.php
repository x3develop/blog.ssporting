<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 8/2/2018
 * Time: 08:27
 */

namespace controller;


use Carbon\Carbon;
use model\Answer;
use Cache\Adapter\Filesystem\FilesystemCachePool;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;

class AnswerController
{
    public function add($fb_id, $eid, $rid, $answer)
    {
        $econ = new EventController();
        $event = $econ->getEvent($eid);
//dd($fb_id);
        $ansercount = Answer::where("fbid", $fb_id)
            ->where("eid", $eid)
            ->get()
            ->count();
        if ($ansercount < $event->max_answer) {
            $ans = new Answer();
            $ans->eid = $eid;
            $ans->fbid = $fb_id;
            $ans->rid = $rid;
            $ans->answer = $answer;
            $ans->save();
        }

        return $this->get($fb_id, $eid);
    }

    public function get($fb_id, $eid)
    {

        $rs = Answer::where("fbid", $fb_id)
            ->where("eid", $eid)
            ->get();
        return $rs;
    }

}
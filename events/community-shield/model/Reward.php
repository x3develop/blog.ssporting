<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/26/2018
 * Time: 4:06 PM
 */

namespace model;
require_once __DIR__."/../bootstrap.php";

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    protected $table = "reward";
    protected $fillable=['type', 'desc','quantity','created_at', 'updated_at'];

    public function answer(){
        return $this->belongsTo(Answer::class,"rid","id");
    }

}
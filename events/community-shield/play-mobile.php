<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link href="/events/community-shield/css/community-shield-style-mobile.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.css">
    <!--    <link rel="stylesheet" href="/css/reset.css">-->

    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.js"></script>

</head>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="container-icc2018">
    <div class="container container-mobile">
        <div class="pull-left"><img src="/events/community-shield/img/logo.png"></div>
        <!--        <div class="pull-right text-right">-->
        <!--            <p style="margin: 0">Sun 29 Jul</p>-->
        <!--            <p>04.05</p>-->
        <!--        </div>-->
        <div style="clear: both;"></div>
        <div class="col-lg-12" style="margin: 15px 0;">
            <h4>กิจกรรมตามหาลูกสุดท้าย(รอบแก้ตัว)</h4>
        </div>


        <div class="row-logo">
            <div class="col-xs-6" style="background-color: #7ebbeb; padding: 20px 0;">
                <img src="/events/community-shield/img/Manchester_City.png">
            </div>
            <div class="col-xs-6" style="background-color: #034693; padding: 20px 0;">
                <img src="/events/community-shield/img/Chelsea_FC.png">
            </div>
        </div>

        <h3 style="font-size: 170%; line-height: 50px;">ทายว่า <u>ประตูสุดท้าย</u> ยิงนาทีไหน</h3>
        <div class="box-select-num">
            <form id="add-form">
                <div class="form-inline text-center">
                    <input type="hidden" id="event-id" value="1" />
                    <ul>
                        <li>เลือกนาที</li>
                        <li><input style="width: 85px;" type="number" min="0" max="99" required id="answer" class="form-control" placeholder="0-99"></li>
                        <li>
                            <div class="pull-left">นาที</div>
                        </li>
                    </ul>
                </div>
                <button type="submit" id="save-answer" class="btn btn-lg btn-default"><span class="glyphicon glyphicon-send"
                                                                           aria-hidden="true"></span> ส่งคำตอบ
                </button>
            </form>
        </div>

        <h5>***กรณีที่จบเสมอ 0-0 ให้ตอบนาทีที่ 0</h5>

    </div>
</div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://connect.facebook.net/en_US/sdk.js"></script>

<script type='text/javascript'>
    var hassession = <?php echo (isset($_SESSION['login'])) ? 1 : 0; ?>;
</script>

<script>
    FB.init({
        // appId      : '1630409860331473',
        appId: '188797391962899',
        // appId: '1761102130633089',
        cookie: true,  // enable cookies to allow the server to access
                       // the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.8
    });

</script>
<script src="/events/community-shield/js/cs.js?<?php echo date("Ymdhis") ?>"></script>
</body>
</html>
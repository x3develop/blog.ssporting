<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link href="/events/community-shield/css/community-shield-style.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.css">
    <!--    <link rel="stylesheet" href="/css/reset.css">-->

    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.js"></script>

</head>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="container-icc2018">
    <div class="box-select-num">
        <form id="add-form">
            <div class="form-inline">
                <input type="hidden" id="event-id" value="1"/>
                <label>เลือกนาที :</label>
                <input style="width: 100px;" type="number" min="0" max="99" id="answer" required class="form-control"
                       placeholder="0-99">
                <label>น.</label>
                <button id="save-answer" type="submit" class="btn btn-default"><span class="glyphicon glyphicon-send"
                                                                    aria-hidden="true"></span> ส่งคำตอบ
                </button>
            </div>
        </form>
    </div>
    <div class="bg-content-icc2018">
        <img src="/events/community-shield/img/content-icc.jpg">
    </div>
</div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://connect.facebook.net/en_US/sdk.js"></script>

<script type='text/javascript'>
    var hassession = <?php echo (isset($_SESSION['login'])) ? 1 : 0; ?>;
</script>

<script>
    FB.init({
        // appId      : '1630409860331473',
        appId: '188797391962899',
        // appId: '1761102130633089',
        cookie: true,  // enable cookies to allow the server to access
                       // the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.8
    });

</script>
<script src="/events/community-shield/js/cs.js?<?php echo date("Ymdhis") ?>"></script>
</body>
</html>
<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 7/26/2018
 * Time: 17:08
 */

require_once __DIR__ . "/../bootstrap.php";


$fb_id = $_REQUEST['fb_id'];
$answer = $_REQUEST['answer'];
$eid = (int)$_REQUEST['eid'];
$rid = (int)$_REQUEST['rid'];

//dd($fb_id);
$econ = new \controller\EventController();
$event = $econ->getEvent($eid);
//dd($event);
$dt = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $event->end);
$dt->subHours(7);
$nt = \Carbon\Carbon::now();

$answers = null;

if ($nt <= $dt) {
    $acon = new \controller\AnswerController();
    $answers = $acon->add($fb_id, $eid, $rid, $answer);

}

header("ContentType:application/json");
echo json_encode($answers);
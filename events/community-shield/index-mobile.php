<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link href="/events/community-shield/css/select-item-style.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.css">
    <!--    <link rel="stylesheet" href="/css/reset.css">-->

    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.js"></script>

</head>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="container-select-item container-select-item-mobile">
    <div class="header">
        <img src="/events/community-shield/img/title.png">
    </div>
    <div class="content-3col">
        <div class="box-col-1">
            <a href="/events/community-shield/play-mobile.php?reward=3">
                <img src="/events/community-shield/img/static01-mobile.jpg">
            </a>
        </div>
        <div class="box-col-2">
            <a href="/events/community-shield/play-mobile.php?reward=1">
                <img src="/events/community-shield/img/static02-mobile.jpg">
            </a>
        </div>
        <div class="box-col-3">
            <a href="/events/community-shield/play-mobile.php?reward=2">
                <img src="/events/community-shield/img/static03-mobile.jpg">
            </a>

        </div>
        <div style="clear: both"></div>
    </div>
    <div class="footer1">
        <u>กิจกรรมตามหาลูกสุดท้าย</u> รางวัลแบ่งเป็น 3 ประเภท รางวัลเงิดสด, สกิน ROV, สกิน PUBG
    </div>
</div>
</body>
</html>
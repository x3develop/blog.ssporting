<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 12/6/2561
 * Time: 16:31 น.
 */

require __DIR__ . "/vendor/autoload.php";

use Illuminate\Database\Capsule\Manager as Capsule;
$capsule = new Capsule;
//$capsule->addConnection([
//
//    "driver" => "mysql",
//    "host" =>"192.168.1.121",
//    "port" =>"3307",
//    "database" => "icc",
//    "username" => "dev",
//    "password" => "x3@x1234"
//
//]);
$capsule->addConnection([

    "driver" => "mysql",
    "host" => "localhost",
    "port" => "3306",
    "database" => "ng_events",
    "username" => "dev",
    "password" => "1234",
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',

]);
// $capsule->addConnection([

//     "driver" => "mysql",
//     "host" => "172.17.0.1",
//     "port" => "3306",
//     "database" => "ng_events",
//     "username" => "root",
//     "password" => "@x1234",
//     'charset' => 'utf8',
//     'collation' => 'utf8_unicode_ci',

// ]);
//Make this Capsule instance available globally.
$capsule->setAsGlobal();
// Setup the Eloquent ORM.
// $capsule->bootEloquent();
$capsule->bootEloquent();

<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 8/2/2018
 * Time: 08:05
 */

namespace controller;


use Cache\Adapter\Filesystem\FilesystemCachePool;
use Carbon\Carbon;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use model\Event;

class EventController
{

    public function getEvent($eid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache");
        $item = $pool->getItem("event" . $eid);
        $event = null;
        if (!$item->isHit()) {
            $event = Event::find($eid);
            $item->set($event);
            $item->expiresAfter(3600);
            $pool->save($item);
        } else {
            $event = $item->get();
        }
        return $event;
    }

}
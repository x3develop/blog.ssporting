<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/26/2018
 * Time: 4:06 PM
 */

namespace model;
require_once __DIR__ . "/../bootstrap.php";

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = "events";
    protected $fillable = ['name', 'desc', 'end', ',max_answer', 'created_at', 'updated_at'];

    public function answers()
    {
        return $this->hasMany(Answer::class, "eid", "id");
    }

}
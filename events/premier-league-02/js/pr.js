$(document).ready(function () {
    var fbid = "0";
    var reward = 0;
    var eventdir="/events/premier-league-02";
    // rewardCheck();
    checkLoginState();
    $("#add-form").on("submit", function (e) {
        e.preventDefault();
        if (fbid !== "0") {
            var answer = $("#answer").val();
            var eid = $("#event-id").val();

            $.ajax({
                url: "/events/manage/services/addanswer.php",
                method: "GET",
                data: { fb_id: fbid, answer: answer, eid: eid, rid: reward },
                dataType: "JSON"
            }).done(function (res) {
                console.log(res);
                if (res) {
                    swal("เข้าร่วมกิจกรรมเรียบร้อย", {
                        icon: "success",
                    });
                    $("#answer").attr("disabled", "disabled");
                    $("#save-answer").attr("disabled", "disabled");
                } else {
                    swal("หมดเวลาทายผลแล้ว");
                }
            })
        } else {
            swal("ต้องล็อคอินก่อนถึงเข้าร่วมกิจกรรมได้");
            // fbLogin();
            FBlogin();
        }

    });

    function checkLoginState() {
        FB.getLoginStatus(function (response) {
            console.log(response);
            if (response.status === 'connected' && isLogin()) {
                updateUser(response.authResponse.accessToken);

            } else {
                // $(".btn-sign-FB").removeClass("hide");
                if (hassession == 1) {
                    fbLogin();
                    // FBlogin();
                }
            }

        });
    }

    function updateUser(token) {
        FB.api('/me', function (response) {
            console.log(response);
            fbid = response.id;
            $.ajax({
                url: "/events/manage/services/updateuser.php",
                method: "POST",
                data: { fb_id: response.id, name: response.name, access_token: token },
                dataType: "JSON"
            }).done(function (res) {
                checkplay();
            });

        });
    }

    function fbLogin() {
        FB.login(function (response) {
            location.reload();
        });

    }

    function isLogin() {
        // console.log(fbid);
        if (hassession == 0) {
            return false;
        } else {
            return true;
        }
    }

    function checkplay() {
        var eid = $("#event-id").val();
        $.ajax({
            url: "/events/manage/services/getanswer.php",
            method: "GET",
            data: { fb_id: fbid, eid: eid },
            dataType: "JSON"
        }).done(function (res) {
            console.log(res);
            if (res[0]) {
                $("#answer").val(res[0].answer);
                $("#answer").attr("disabled", "disabled");
                $("#save-answer").attr("disabled", "disabled");
            }

        });
    }

    function getParam(param) {
        return new URLSearchParams(window.location.search).get(param);
    }

    function rewardCheck() {
        var rewardp = getParam("reward");
        if (rewardp === null) {
            console.log(rewardp);
            window.location = "/events/premier-league-01";
        } else {
            reward = rewardp;
        }
    }

});
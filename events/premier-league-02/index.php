<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="แจกฟรี!! เพียงทายผลประตูรวมถูกต้อง"/>
    <meta property="og:description" content="กิจกรรมทายผลประตูรวมคู่ เซลซี พบ แมนซิตี้ แจกเงินสด 1000 บาท"/>
    <meta property="og:image" content="https://ngoal.com/events/premier-league-02/img/shared.png"/>

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link href="/events/premier-league-02/css/premier-league-01-style.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.css">
    <!--    <link rel="stylesheet" href="/css/reset.css">-->

    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.js"></script>

</head>
<?php
header('Location: /events/premier-league-02/desktop-award.php');
// require_once __DIR__ . "/bootstrap.php";
// $isMobile = (bool) preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet' .
//     '|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]' .
//     '|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', $_SERVER['HTTP_USER_AGENT']);

// if ($isMobile) {
//     header('Location: /events/premier-league-02/mobile.php');
// }
?>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php';?>
<div class="container-icc2018">
    <div class="box-select-num">
        <form id="add-form">
        <div class="form-inline">
            <input type="hidden" id="event-id" value="3"/>
            <label>เลือกจำนวนประตู :</label>
            <input id="answer" style="width: 100px;" type="number" min=0 max=50 class="form-control" placeholder="0-50">
            <label>ประตู</label>
            <button id="save-answer" type="submit" class="btn btn-default"><span class="glyphicon glyphicon-send"
                                                                aria-hidden="true"></span> ส่งคำตอบ
            </button>
        </div>
        </form>
    </div>
    <div class="bg-content-icc2018">
        <img src="/events/premier-league-02/img/premier-league-01.jpg">
    </div>
</div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://connect.facebook.net/en_US/sdk.js"></script>

<script type='text/javascript'>
    var hassession = <?php echo (isset($_SESSION['login'])) ? 1 : 0; ?>;
</script>

<script>
    FB.init({   
        appId: '188797391962899',
        // appId: '1761102130633089',
        cookie: true,  // enable cookies to allow the server to access
                       // the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.8
    });

</script>
<script src="/events/premier-league-02/js/pr.js"></script>
</body>
</html>
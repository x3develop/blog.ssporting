<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/26/2018
 * Time: 4:06 PM
 */

namespace model;
require_once __DIR__."/../bootstrap.php";

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "user";
    protected $fillable=['name', 'fb_id','access_token','created_at', 'updated_at'];

    public function bets(){
        return $this->hasMany(Bet::class,"fb_id","fb_id");
    }

}
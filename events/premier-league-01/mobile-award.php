<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="กิจกรรมตามหาใบเหลือง-ใบแดง"/>
    <meta property="og:description" content="ทายผลรวมใบเหลือง-ใบแดง แจกเงินสด 2000 บาท"/>
    <meta property="og:image" content="https://ngoal.com/events/premier-league-01/img/post20.png"/>

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link href="/events/community-shield/css/community-shield-style-mobile.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.css">
    <!--    <link rel="stylesheet" href="/css/reset.css">-->

    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.js"></script>

</head>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="container-icc2018">
    <div class="container container-mobile" style="padding: 0;">
        <img src="/events/premier-league-01/img/Award-mobile.png">

    </div>
</div>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link href="/events/premier-league-01/css/premier-league-01-style-mobile.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.css">
    <!--    <link rel="stylesheet" href="/css/reset.css">-->

    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.js"></script>

</head>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="container-icc2018">
    <div class="container container-mobile">
        <div class="pull-left"><img src="/events/premier-league-01/img/logo.png"></div>
<!--        <div class="pull-right text-right">-->
<!--            <p style="margin: 0">Sun 29 Jul</p>-->
<!--            <p>04.05</p>-->
<!--        </div>-->
<div style="clear: both;"></div>
<div class="col-lg-12" style="margin: 15px 0;">
    <h4>กิจกรรม “ตามหาใบเหลือง-ใบแดง”</h4>
</div>


        <div class="row-logo">
            <div class="col-xs-6" style="background-color: #034693; padding: 20px 0;">
                <img src="/events/premier-league-01/img/Chelsea_FC.png">
            </div>
            <div class="col-xs-6" style="background-color: #cb000f; padding: 20px 0;">
                <img src="/events/premier-league-01/img/Liverpool_FC.png">
            </div>
        </div>
<div class="container">
    <h2>
    <img src="/events/premier-league-01/img/text01.png">
    </h2>
</div>
<!--        <h3 style="font-size: 170%; line-height: 50px;">ทายว่า <u>ประตูสุดท้าย</u> ยิงนาทีไหน</h3>-->
        <div class="box-select-num">
            <div class="form-inline text-center">


                    <ul>
                        <li>เลือกจำนวนใบ</li>
                        <li><input style="width: 85px;" type="text" class="form-control" placeholder="0-50"></li>
                        <li><div class="pull-left">ใบ</div></li>
                    </ul>



            </div>
            <button type="submit" class="btn btn-lg btn-default"><span class="glyphicon glyphicon-send"
                                                                       aria-hidden="true"></span> ส่งคำตอบ
            </button>
        </div>

        <h5>***กรณีที่ไม่มีใบเหลืองใบแดง ให้ตอบ 0</h5>

    </div>
</div>
</body>
</html>
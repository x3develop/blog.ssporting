<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/model/GalleryM.php';
$GalleryObject = new GalleryM();
if (isset($_GET['galleryId'])) {
    $dataVideo = array();
    $dataVideo = $GalleryObject->getMediaStoreByGalleryId($_GET['galleryId']);
    $dataRelatedVideos = $GalleryObject->getRelatedVideo($dataVideo[0]->tag, $dataVideo[0]->id, 4);
    $dataRelatedGallery = $GalleryObject->getMoreRecentedGallery($dataVideo[0]->tag, $dataVideo[0]->id, 4);
    $dataVideoMOthers = $GalleryObject->getOthersVideo(4);
} else {
    header("Location: /");
}
?>

<div class="wrapper-news-articles news-articles-list" video_id="<?php echo $dataVideo[0]->video_id; ?>" style="">

    <div class="wrap-content-cols-2">
        <div class="bg-video-sexy">
            <div class="box-large-videos-row">
                <div class="box-large-videos">
                    <div id="box-large-videos-autoPlay">
                        <?php if ($dataVideo[0]->typeVideo == "dailymotion") { ?>
                            <iframe class="iframe_<?php echo $dataVideo[0]->id; ?>" id="large-videos-autoPlay"
                                    data-autoplay="true"
                                    stop="<?php echo $dataVideo[0]->stopUrlIframe ?>"
                                    start="<?php echo $dataVideo[0]->urlIframe ?>"
                                    src="http://www.dailymotion.com/embed/video/<?php echo $dataVideo[0]->videokey ?>"
                                    frameborder="0"
                                    allowfullscreen></iframe>
                        <?php } elseif ($dataVideo[0]->typeVideo == "twitter") { ?>
                            <iframe class="iframe_<?php echo $dataVideo[0]->id; ?>" id="large-videos-autoPlay"
                                    data-autoplay="true"
                                    stop="<?php echo $dataVideo[0]->stopUrlIframe ?>"
                                    start="<?php echo $dataVideo[0]->urlIframe ?>"
                                    src="https://twitter.com/i/cards/tfw/v1/<?php echo $dataVideo[0]->videokey ?>"
                                    frameborder="0"
                                    allowfullscreen></iframe>
                        <?php } elseif ($dataVideo[0]->typeVideo == "facebook") { ?>
                            <iframe class="iframe_<?php echo $dataVideo[0]->id; ?>" id="large-videos-autoPlay"
                                    width="auto" height="530" data-autoplay="true"
                                    stop="<?php echo $dataVideo[0]->stopUrlIframe ?>"
                                    start="<?php echo $dataVideo[0]->urlIframe ?>"
                                    src="https://www.facebook.com/v2.3/plugins/video.php?allowfullscreen=true&container_width=800&href=<?php echo $dataVideo[0]->content ?>"
                                    frameborder="0"
                                    allowfullscreen></iframe>
                        <?php } elseif ($dataVideo[0]->typeVideo == "youtube") { ?>
                            <iframe class="iframe_<?php echo $dataVideo[0]->id; ?>" id="large-videos-autoPlay"
                                    data-autoplay="true"
                                    stop="<?php echo $dataVideo[0]->stopUrlIframe ?>"
                                    start="<?php echo $dataVideo[0]->urlIframe ?>"
                                    src="http://www.youtube.com/embed/<?php echo $dataVideo[0]->videokey ?>"
                                    frameborder="0"
                                    allowfullscreen></iframe>
                        <?php } elseif ($dataVideo[0]->typeVideo == "streamable") { ?>
                            <iframe class="iframe_<?php echo $dataVideo[0]->id; ?>" id="large-videos-autoPlay"
                                    loop="0" data-autoplay="true"
                                    stop="<?php echo $dataVideo[0]->stopUrlIframe ?>"
                                    start="<?php echo $dataVideo[0]->urlIframe ?>"
                                    src="<?php echo $dataVideo[0]->stopUrlIframe ?>"
                                    frameborder="0"
                                    allowfullscreen></iframe>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="load_video">
                <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
            </div>
        </div>
    </div>

    <div class="container-fluid">

        <div class="wrap-content-cols other-video-box">
            <div class="container">
                <div class="content-large-videos bx-other-video-head">
                    <div class="bx-video-type"><?php echo $dataVideo[0]->video_tag ?></div>

                    <div class="contents-row rows">
                        <div class="col-sm-9 bx-description-videos main_articles"
                             newsid="<?php echo $dataVideo[0]->video_id ?>"
                             id="main_articles_<?php echo $dataVideo[0]->video_id ?>">
                            <h1><?php echo $dataVideo[0]->title ?></h1>
                            <div class="times-content mg-bt15">
                                <span><?php echo date(' g:i A', strtotime($dataVideo[0]->create_datetime)) ?></span>
                                <span><?php echo date(' F j, Y', strtotime($dataVideo[0]->create_datetime)) ?></span>
                            </div>
                            <?php if (!empty($dataVideo[0]->desc)) {
                                echo $dataVideo[0]->desc ?>
                            <?php } else { ?>
                                &nbsp;
                            <?php } ?>


                            <!--                                <div class="bx-social">-->
                            <!--                                    --><?php //include 'view/social/bx-social.php'; ?>
                            <!--                                </div>-->
                        </div>
                        <div class="col-sm-3">
                            <img src="/images/kcap.jpg" style="width: 100%;">
                        </div>
                        <div class="col-sm-12 hide">
                            <div class="bx-publish-info">
                                <table>
                                    <tr>
                                        <td><?php echo date(' g:i A', strtotime($dataVideo[0]->create_datetime)) ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo date(' F j, Y', strtotime($dataVideo[0]->create_datetime)) ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>
                <div class="wrap-content-cols">
                    <div class="double-underline bx-tabs-related relate25">Related</div>
                    <div class="rows">
                        <div class="label-news-vertical label-vdo-vertical">Gallery</div>
                        <?php foreach ($dataRelatedGallery as $key => $value) { ?>
                            <div class="col-sm-3 related-news">
                                <a class="link_wrap" href="/sexy-football-picture?id=<?php echo $value->id ?>"></a>
                                <div class="box-videos box-related box-related-sexy">
                                    <div class="bx-type-gallery">
                                        <i class="fa fa-picture-o"></i>
                                    </div>
                                    <div class="crop" style="background-image: url(<?php echo $value->thumbnail ?>)"></div>
                                </div>
                                <div class="title-owl-carousel title-h-videos">
                                    <a href="/sexy-football-picture?id=<?php echo $value->id ?>"><?php echo $value->title ?></a>
                                </div>
                                <div class="times-content">
                                            <span class="timeMoreRecentedGallery"
                                                  date="<?php echo $value->create_datetime; ?>"></span></div>
                            </div>
                        <?php } ?>
                    </div>
                    <div style="clear: both;"></div>
                    <div class="rows">
                        <?php if (!empty($dataRelatedVideos)) { ?>
                            <div class="label-news-clip">Video</div>
                            <?php foreach ($dataRelatedVideos as $key => $value) { ?>
                                <div class="col-sm-3">
                                    <a href="/sexy-football-videos?galleryId=<?php echo $value->id ?>">
                                        <div class="box-videos box-related box-related-sexy">
                                            <div class="bx-type-gallery">
                                                <i class="fa fa-play"></i>
                                            </div>
                                            <div class="crop"
                                                 style="background-image: url(<?= $value->urlImg; ?>);"></div>
                                        </div>
                                        <div class="title-owl-carousel"><?php echo $value->title ?></div>
                                        <div class="times-content">
                                            <span class="timeMoreRecentedGallery"
                                                    date="<?php echo $value->create_datetime; ?>"></span></div>
                                    </a>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

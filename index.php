﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="keywords" content="บ้านผลบอล ผลบอล ไฮไลท์ฟุตบอล ตารางคะแนน, News Football,Highlight Football,Video Football,Football">
    <meta name="description" content="บ้านผลบอล เราคือเว็บทายผลบอลอันดับ 1 ของประเทศ รวบรวมข่าวฟุตบอล ไฮไลท์การแข่งขัน ผลการแข่งขันฟุตบอล พร้อมทั้งตารางคะแนนฟุตบอลลีกต่าง ๆ อย่างครบครัน">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <meta name="google-site-verification" content="gP_HBdXiKXSOt4_9ZS2bjeItupfdNZ9UyuJxlSainX8" />

    <link rel="shortcut icon" type="images/x-icon" href="images/favicon.ico.png">
    <title>บ้านผลบอล | เว็บทายผลบอลอันดับ 1 ของประเทศ ข่าวฟุตบอล ไฮไลท์การแข่งขัน</title>

    <link rel="stylesheet" href="css/style-new.css">
    <link rel="stylesheet" href="css/view-all.css">
    <!--    <link rel="stylesheet" href="css/style.css">-->
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/gallery.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/livescore.css">
    <link rel="stylesheet" href="/css/program_ball.css">
    <link rel="stylesheet" href="/css/football-symbol.css">
    <link rel="stylesheet" href="/css/game-redesign.css">
    <link rel="stylesheet" href="/css/game-redesign-V2.css">
    <link rel="stylesheet" href="/css/swiper.min.css">
    <link rel="stylesheet" href="/css/circle-progress-bar.css">
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <!--    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">-->

    <!--js-->

    <script src="js/jquery/dist/jquery.min.js"></script>
    <script src="js/jquery-ui/jquery-ui.min.js"></script>
    <!--    <script src="js/flogin/src/flogin.js"></script>-->
    <script src="/js/moment.js"></script>
    <script src="/js/moment-with-locales.min.js"></script>
    <script src="/js/moment-timezone.js"></script>
    <script src="css/bootstrap/js/bootstrap.min.js"></script>
    <script src="css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="js/sly/sly.js"></script>
    <script src="js/jquery-timeago/jquery.timeago.js"></script>
    <script src="nanoscrollbar/nanoscrollbar.js"></script>
    <script src="/js/swiper.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="/js/highlight_live.js"></script>
    <script>
        function imgError(image) {
            image.onerror = "";
            image.src = "http://api.ssporting.com/teams_clean/team_default_32x32.png";
            return true;
        }
    </script>
    <script src="js/utils.js"></script>
    <!--    <script src="js/timeview.js"></script>-->
    <script src="js/index_game.js"></script>

    <?php include 'view/index/googletagmanager.php'; ?>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</head>

<body>

    <style>
        div.twitter-video{
        max-height: none !important;
        max-width: none !important;
        min-width: none !important;
        min-height: none !important;
        width: 100% !important;
        height: 100% !important;
    }
</style>
    <!--<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>-->
    <?php
session_start();
?>
    <?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playComment.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/HomeM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/VideoM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playBet.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playReviewMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playLogIncome.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playUser.php";
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/GalleryM.php';
$playUser = new playUser();
$playBet = new playBet();
$playMatch = new playMatch();
$playComment = new playComment();
$VideoMObj = new VideoM();
$homem = new HomeM();
$playLogIncome = new playLogIncome();
$playReviewMatch = new playReviewMatch();
$GalleryObject = new GalleryM();
$match = array();
$match = $playMatch->getFirstMathStatusBet(30);
//$reviewMatch=$playReviewMatch->getReviewByMatch();
$matchResultsHome = array();
$matchResultsAway = array();
$betMatchById = array();
$videoHighlight = array();
$resultsReviewMatch = array();
if (isset($_SESSION['login'])) {
    //calculate Bet
    $playBet->calculateBet($_SESSION['login']['id']);
    if (!empty($_SESSION['login']['accuracy_at']) and $_SESSION['login']['accuracy_at'] !== date('Y-m-d')) {
        $playBet->calculateAccuracy($_SESSION['login']['id']);
    }
    if ((intval(date("H")) + 7) >= 12) {
        $playBet->exchangeStep($_SESSION['login']['id']);
    }
    foreach ($match as $key => $value) {
        $betMatchById[$value->id] = $playBet->getTeamMatchById($_SESSION['login']['id'], $value->id);
    }
}

//foreach ($match as $key=>$value){
//    $matchResultsHome[$value->id] = $playMatch->getListMatchResults($value->team_home, 20);
//    $matchResultsAway[$value->id] = $playMatch->getListMatchResults($value->team_away, 20);
//    $userListHome[$value->id] = $playMatch->getUserBet($value->id,'home',5);
//    $userListAway[$value->id] = $playMatch->getUserBet($value->id,'away',5);
//    $percentBet[$value->id] = $playBet->percentBet($value->id);
//    $reviewMatch[$value->id] = $playReviewMatch->getReviewMatchByMatchId($value->id);
//
//    $reviewMatchList = $playReviewMatch->getReviewMatchByMatchId($value->id);
//    foreach ($reviewMatchList as $key => $value) {
//        $winRate = $playReviewMatch->getWinRate($value['user_review_id'], 30);
//        if ($winRate["total"] != 0) {
//            $resultsReviewMatch[$value['user_review_id']] = intval(100 - ((($winRate["total"] - $winRate["win"]) / $winRate["total"]) * 100));
//        } else {
//            $resultsReviewMatch[$value['user_review_id']] = 0;
//        }
//    }
//}

//if(!empty($match[0])){
//    $userListHome=$playMatch->getUserBet($match[0]->id);
//}

//index menu
include 'view/index/menu-index.php';
//include 'view/index/menu.php';
//include 'view/index/livescore.php';
?>
    <!--<button id="btn-notifyMe" onclick="notifyMe('999999999999999999')">Notify me!</button>-->
    <?php
//england 43948,ger 44352,spain 45141,italy 45146,fr 43893,th 43105,cl 44042, eu 44094, wc 44465
//$GalleryObject = new GalleryM();
//$recommendnews = $homem->getRecommentNews();
//$popularnews = $homem->getMostPopularNews();
//$lastestnews = $homem->getLastestNews();

//$thainews = $homem->getNewsByCategory('thai', 8);
//$foreignNewsPlData = $homem->getNewsByCategory('premier league', 7);

?>

    <div class="container-fluid" id="main-home">
        <!--    game-->
        <?php
if (!empty($match)) {?>
        <div id="fade-in-index" class="box-fade">
            <div class="wrap-selected-team-play wrap-left-team">
                <p>คุณเลือกเล่นทีม</p>
                <img id="img-TeamHomePath" src="<?=$matchView->teamHomePath?>">
                <h1 id="h1-TeamHomeEn">
                    <?=$matchView->teamHomeEn?>
                </h1>
                <form id="bet-home-match">
                    <input name="bet[time_match]" class="form-control" type="hidden" value="<?=$matchView->time_match;?>">
                    <input name="bet[user_id]" class="form-control" type="hidden" value="<?=$_SESSION['login']['id'];?>">
                    <input name="bet[match_id]" class="form-control" type="hidden" value="<?=$matchView->id;?>">
                    <input name="bet[team]" type="hidden" value="home">
                    <!--                <input name="bet[use_bet_coin]" type="hidden" value="750">-->
                    <input name="bet[use_bet_gold]" type="hidden" value="0">
                    <input name="bet[use_bet_coupon]" type="hidden" value="1">
                    <input name="bet[status]" type="hidden" value="bet">

                    <div class="box-commented" style="margin: 20px 0px;">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8">
                            <div class="box-input-select">
                                <textarea name="bet[comment]" class="form-control" placeholder="แสดงความคิดเห็น..."></textarea>
                            </div>
                        </div>
                        <div class="col-sm-2"></div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="box-button-send">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-4">
                            <div class="box-input-select">
                                <img src="/images/coin/scoin100.png">
                                <select name="bet[use_bet_coin]">
                                    <option value="1200">1200</option>
                                    <option value="800">800</option>
                                    <option value="400">400</option>
                                    <option value="200">200</option>
                                </select>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="box-button">
                                <button type="submit" class="btn btn-info btn-lg btn-block">Confirm</button>
                            </div>
                        </div>
                        <div class="col-sm-2"></div>
                        <div class="clearfix"></div>
                    </div>
                </form>
                <p>เลือกจำนวน Coin ที่ใช้ในการทายผล</p>


                <div class="block-close-vote text-center">
                    <div class="box-button close-tab close-tab-right">
                        <button type="button" class="btn btn-default btn-lg btn-block" data-dismiss="modal">
                            ปิด
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div id="fade-in-right-index" class="box-fade">
            <!--        <div class="close-tab pull-left close-tab-left"><i class="fa fa-times" aria-hidden="true"></i></div>-->
            <div class="wrap-selected-team-play wrap-right-team">
                <p>คุณเลือกเล่นทีม</p>

                <img id="img-TeamAwayPath" src="<?=$matchView->teamAwayPath?>">
                <h1 id="h1-TeamAwayEn">
                    <?=$matchView->teamAwayEn?>
                </h1>
                <form id="bet-away-match">
                    <input name="bet[time_match]" class="form-control" type="hidden" value="<?=$matchView->time_match;?>">
                    <input name="bet[user_id]" class="form-control" type="hidden" value="<?=$_SESSION['login']['id'];?>">
                    <input name="bet[match_id]" class="form-control" type="hidden" value="<?=$matchView->id;?>">
                    <input name="bet[team]" type="hidden" value="away">
                    <!--                <input name="bet[use_bet_coin]" type="hidden" value="750">-->
                    <input name="bet[use_bet_gold]" type="hidden" value="0">
                    <input name="bet[use_bet_coupon]" type="hidden" value="1">
                    <input name="bet[status]" type="hidden" value="bet">
                    <div class="box-commented" style="margin: 20px 0px;">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8">
                            <div class="box-input-select">
                                <textarea name="bet[comment]" class="form-control" placeholder="แสดงความคิดเห็น..."></textarea>
                            </div>
                        </div>
                        <div class="col-sm-2"></div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="box-button-send">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-4">
                            <div class="box-input-select">
                                <img src="/images/coin/scoin100.png">
                                <select name="bet[use_bet_coin]">
                                    <option value="1200">1200</option>
                                    <option value="800">800</option>
                                    <option value="400">400</option>
                                    <option value="200">200</option>
                                </select>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="box-button">
                                <button type="submit" class="btn btn-info btn-lg btn-block">Confirm</button>
                            </div>
                        </div>
                        <div class="col-sm-2"></div>
                        <div class="clearfix"></div>
                    </div>
                </form>
                <p>เลือกจำนวน Coin ที่ใช้ในการทายผล</p>
                <div class="block-close-vote text-center">
                    <div class="box-button close-tab close-tab-left">
                        <button type="button" class="btn btn-default btn-lg btn-block" data-dismiss="modal">
                            ปิด
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <?php
if (file_exists($webroot . '/load/livescore-main-index.html')) {
    echo "<span style='display: none;'>/load/livescore-main-index.html</span>";
    include $_SERVER["DOCUMENT_ROOT"] . '/load/livescore-main-index.html';
} else {
    include $_SERVER["DOCUMENT_ROOT"] . '/view/index/livescore-main-index.php';
}?>
        <?php }?>
    </div>
    <!--    header news-->

<!--    --><?php //include 'include/ponball.php'?>

    <?php
if (file_exists($webroot . '/load/headerNews.html')) {
    echo "<span style='display: none;'>/load/headerNews.html</span>";
    include $_SERVER["DOCUMENT_ROOT"] . '/load/headerNews.html';
} else {
    include $_SERVER["DOCUMENT_ROOT"] . '/view/index/headerNews.php';
}?>

    <!--    Most Popular-->
    <?php
if (file_exists($webroot . '/load/mostPopular.html')) {
    echo "<span style='display: none;'>/load/mostPopular.html</span>";
    include $_SERVER["DOCUMENT_ROOT"] . '/load/mostPopular.html';
} else {
    include $_SERVER["DOCUMENT_ROOT"] . '/view/index/mostPopular.php';
}
?>
    <?php
//<!--    video main-->
//    if(file_exists($webroot.'/load/videoMain.html')){
//        echo "<span style='display: none;'>/load/videoMain.html</span>";
//        include $_SERVER["DOCUMENT_ROOT"] . '/load/videoMain.html';
//    }else {
//        $lastestvideo = $VideoMObj->getSlideVideos("general", 24);
//        $highlightvideo = $VideoMObj->getSlideVideos("highlight", 24);
//        include $_SERVER["DOCUMENT_ROOT"] . '/view/index/videoMain.php';
//    }
?>
    <div class="container-videoMain-index" id="container-videoMain-index"></div>
    <?php
//    <!--    ข่าวต่างประเทศ-->
//    if(file_exists($webroot.'/load/foreignNews.html')) {
//        echo "<span style='display: none;'>/load/foreignNews.html</span>";
//        include $_SERVER["DOCUMENT_ROOT"] . '/load/foreignNews.html';
//    }else{
//        include $_SERVER["DOCUMENT_ROOT"] . '/view/index/foreignNews.php';
//    }
?>
    <div class="container-foreignNews-index" id="container-foreignNews-index"></div>
    <?php
//    <!--    Sexy Football-->
//    if(file_exists($webroot.'/load/sexyFootball.html')) {
//        echo "<span style='display: none;'>/load/sexyFootball.html</span>";
//        include $_SERVER["DOCUMENT_ROOT"] . '/load/sexyFootball.html';
//    }else{
//        include $_SERVER["DOCUMENT_ROOT"] . '/view/index/sexyFootball.php';
//    }
?>
    <div class="container-sexyFootball-index container-fluid" id="container-sexyFootball-index"></div>

    <div style="width: 100%;text-align: center;" id="loading-index">
        <img style="height: 40px" src="/images/loading3.gif">
    </div>

    <?php //include 'footer.php';
include 'view/index/footer.php';
?>

    <script>
        var servicesIndex;
        $.ajax({
            url: "/services-index",
            method: "POST",
            dataType: "JSON"
        }).done(function(response) {
            servicesIndex = response;
            insertIndex();
        });

        $.ajax({
            url: "/json/checkWinRateReview.json",
            method: "POST",
            dataType: "JSON"
        }).done(function(response) {
            $.each(response, function(key, val) {
                // console.log(val.winRate+"::"+key);
                if(val.winRate==null){
                    $(".user-review-" + val.id).html("100%");
                }else {
                    $(".user-review-" + val.id).html(parseInt(val.winRate) + "%");
                }
                // console.log(key+" :: "+val)
            })
        });


        // $(window).scroll(function () {
        scrollWindow();
        // });

        function insertIndex() {
            $.each(servicesIndex, function(key, val) {
                // console.log(key);
                if (key.indexOf("user-match-") >= 0) {
                    $("#" + key).attr('src', val);
                } else if (key.indexOf("progress-match-") >= 0) {
                    $("span#" + key).text(parseFloat(val) + '%');
                    $("div#" + key).attr('aria-valuenow', parseFloat(val));
                    $("div#" + key).css('width', parseFloat(val) + '%');
                } else {
                    $("#" + key).html(val);
                }
            })
        }

        function scrollWindow() {
            var windowHeight = jQuery(window).height();
            var windowWidth = jQuery(window).width();
            var body = $('body').height();
            //        console.log("body: "+(body-windowHeight)+" scrollTop: "+scrollTop);
            //         if((body-windowHeight)>(scrollTop-100)){
            if ($('#container-videoMain-index').html() == "") {
                $.get("/load/videoMain.html", function(data) {
                    $('#container-videoMain-index').html(data).fadeIn('slow');
                    $.each($('#container-videoMain-index').find(".how-long"), function(k, v) {
                        if ($.isNumeric($(this).text()) == true) {
                            $(this).html(moment($(this).text(), "X").fromNow())
                        }
                    });

                    if ($('#container-foreignNews-index').html() == "") {
                        $.get("/load/foreignNews.html", function(data) {
                            $('#container-foreignNews-index').html(data).fadeIn('slow');
                            $.each($('#container-foreignNews-index').find(".how-long"), function(k, v) {
                                if ($.isNumeric($(this).text()) == true) {
                                    $(this).html(moment($(this).text(), "X").fromNow())
                                }
                            });

                            if ($('#container-sexyFootball-index').html() == "") {
                                $.get("/load/sexyFootball.html", function(data) {
                                    $('#container-sexyFootball-index').html(data).fadeIn('slow');
                                    $.each($('#container-sexyFootball-index').find(".how-long"),
                                        function(k, v) {
                                            if ($.isNumeric($(this).text()) == true) {
                                                $(this).html(moment($(this).text(), "X").fromNow());
                                            }
                                        });
                                    $('#loading-index').hide();
                                    insertIndex();
                                });
                            }
                        });
                    }

                });
            }
            //         }
        }

        function setIndexVideosAutoPlay(link, tag, title, desc, date, id) {
            $('#large-videos-autoPlay').attr("src", link);
            $("#box-video-main-index video").attr("autoplay", "");
            $("#box-video-main-index video")[0].load();
        }
        $(".nano").nanoScroller();
        refreshTime();

        function refreshTime() {
            $.each($(".how-long"), function(k, v) {
                if ($.isNumeric($(this).text()) == true) {
                    $(this).html(moment($(this).text(), "X").fromNow())
                }
            })
        }
    </script>
    <script src="/js/play/game.js?<?php echo date(" YmdHis")?>
        ">
    </script>
</body>

</html>
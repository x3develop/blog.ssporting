<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 6/26/2018
 * Time: 10:21 PM
 */
if (!isset($_SESSION)) {
    session_start();
}
$webroot = $_SERVER['DOCUMENT_ROOT'];
?>

<?php if (isset($_SESSION['login'])) { ?>
<ul>
    <?php if (isset($_SESSION['login'])) { ?>
        <li class="coin-topmenu"><a href=""><img src="/images/coin/sgold100.png">
                <span><?php echo number_format($_SESSION['login']['gold'], 2) ?></span></a>
        </li>
        <li class="coin-topmenu"><a href=""><img src="/images/coin/scoin100.png">
                <span><?php echo number_format($_SESSION['login']['coin'], 2) ?></span></a>
        </li>
    <?php } ?>
    <li class="<?= ((!isset($_SESSION['login'])) ? 'hide' : '') ?>" id="fb-online">
        <input id="id-user-bet" type="hidden" value="<?php echo $_SESSION['login']['id']?>">
        <a href="#" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true"
           aria-expanded="true">
            <img id="fb-picture"
                 src="<?= ((isset($_SESSION['login'])) ? 'https://graph.facebook.com/v2.8/' . $_SESSION['login']['fb_uid'] . '/picture' : '') ?>">
            <label id="fb-name"><?= ((isset($_SESSION['login'])) ? $_SESSION['login']['name'] : '') ?></label>
            <span class="caret"></span>
        </a>
        <div class="popover fade bottom in dropdown-menu dropdown-menu-profile" role="tooltip"
             aria-labelledby="dropdownMenu1" style="margin-top: 33px;">
            <div class="arrow"></div>
            <div class="popover-content">
                <form>
                    <ul style="color: #333;">
                        <li><a id="own-profile-link" href="/profile.php">
                                <i class="fa fa-user" aria-hidden="true"></i> Profile</a></li>
                        <li><a id="facebook-logoutbutton" onclick="FBlogout()">
                                <i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </li>
</ul>
<?php }else{ ?>
    <a class="fb-login-button" onclick="FBlogin();">
        <i class="fa fa-facebook" aria-hidden="true"></i>
        <span>Login</span>
    </a>
<?php } ?>

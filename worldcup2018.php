<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="shortcut icon" type="images/x-icon" href="images/favicon.ico.png">
    <title>FIFA World Cup 2018</title>

    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/worldcup2018.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">

</head>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="content-event">
    <div class="content-event-side-title">
        <div class="btn-sign-FB"></div>
    </div>
    <div class="content-fifa-world-cup">


<!--        Round of 16-->
        <div class="columns-fifa01">
            <div class="title-columns-fifa">
                <h2>Round of 16</h2>
                <small>Monday 25 June</small>
            </div>
            <div class="content-fifa-16">
                <div class="box-fifa-16">
                    <div class="check-box">
                        <img src="/images/check-true.png">
                    </div>
                    <div class="pull-left box-fifa-16-haft none-select">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-Spain.png">
                            </li>
                            <li class="name-fifa-16"><span>SPN</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>
                            <li class="name-fifa-16"><span>POR</span></li>
                            <li class="flag-fifa-16">
                                <img src="/images/Portugal-flag.png">
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="box-fifa-16">
                    <div class="check-box">
                        <img src="/images/check-false.png">
                    </div>
                    <div class="pull-left box-fifa-16-haft">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-Spain.png">
                            </li>
                            <li class="name-fifa-16"><span>SPN</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>
                            <li class="name-fifa-16"><span>POR</span></li>
                            <li class="flag-fifa-16">
                                <img src="/images/Portugal-flag.png">
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="box-fifa-16">
                    <div class="check-box">
                        <img src="/images/check-false.png">
                    </div>
                    <div class="pull-left box-fifa-16-haft">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-Spain.png">
                            </li>
                            <li class="name-fifa-16"><span>SPN</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>
                            <li class="name-fifa-16"><span>POR</span></li>
                            <li class="flag-fifa-16">
                                <img src="/images/Portugal-flag.png">
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="box-fifa-16">
                    <div class="check-box">
                        <img src="/images/check-false.png">
                    </div>
                    <div class="pull-left box-fifa-16-haft">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-Spain.png">
                            </li>
                            <li class="name-fifa-16"><span>SPN</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>
                            <li class="name-fifa-16"><span>POR</span></li>
                            <li class="flag-fifa-16">
                                <img src="/images/Portugal-flag.png">
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="box-fifa-16">
                    <div class="check-box">
                        <img src="/images/check-false.png">
                    </div>
                    <div class="pull-left box-fifa-16-haft">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-Spain.png">
                            </li>
                            <li class="name-fifa-16"><span>SPN</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>
                            <li class="name-fifa-16"><span>POR</span></li>
                            <li class="flag-fifa-16">
                                <img src="/images/Portugal-flag.png">
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="box-fifa-16">
                    <div class="check-box">
                        <img src="/images/check-false.png">
                    </div>
                    <div class="pull-left box-fifa-16-haft">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-Spain.png">
                            </li>
                            <li class="name-fifa-16"><span>SPN</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>
                            <li class="name-fifa-16"><span>POR</span></li>
                            <li class="flag-fifa-16">
                                <img src="/images/Portugal-flag.png">
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="box-fifa-16">
                    <div class="check-box">
                        <img src="/images/check-true.png">
                    </div>
                    <div class="pull-left box-fifa-16-haft none-select">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-Spain.png">
                            </li>
                            <li class="name-fifa-16"><span>SPN</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>
                            <li class="name-fifa-16"><span>POR</span></li>
                            <li class="flag-fifa-16">
                                <img src="/images/Portugal-flag.png">
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="box-fifa-16">
                    <div class="check-box">
                        <img src="/images/check-true.png">
                    </div>
                    <div class="pull-left box-fifa-16-haft none-select">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-Spain.png">
                            </li>
                            <li class="name-fifa-16"><span>SPN</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>
                            <li class="name-fifa-16"><span>POR</span></li>
                            <li class="flag-fifa-16">
                                <img src="/images/Portugal-flag.png">
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
            <div class="box-award">
                <img src="/images/mony_preview.png">
                <h3>เงินสด 2,000 บาท</h3>
            </div>
            <div class="btn-col">
                ยืนยันการเลือกทีม
            </div>
        </div>


<!--        Round of 8-->
        <div class="columns-fifa02">
            <div class="title-columns-fifa">
                <h2>Round of 8</h2>
                <small>Monday 25 June</small>
            </div>
            <div class="content-top-fifa-8">
                <div class="box-fifa-16 box-fifa-8">
                    <div class="check-box">
                        <img src="/images/check-true.png">
                    </div>
                    <div class="pull-left box-fifa-16-haft">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>
                            <li class="name-fifa-16"><span>???</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>
                            <li class="name-fifa-16"><span>???</span></li>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>

                        </ul>
                    </div>

                </div>
                <div class="box-fifa-16 box-fifa-8">
                    <div class="check-box">
                        <img src="/images/check-true.png">
                    </div>
                    <div class="pull-left box-fifa-16-haft">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>
                            <li class="name-fifa-16"><span>???</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>
                            <li class="name-fifa-16"><span>???</span></li>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>

                        </ul>
                    </div>

                </div>
            </div>
            <div class="content-bottom-fifa-8">
                <div class="box-fifa-16 box-fifa-8">
                    <div class="check-box">
                        <img src="/images/check-false.png">
                    </div>
                    <div class="pull-left box-fifa-16-haft">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>
                            <li class="name-fifa-16"><span>???</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>
                            <li class="name-fifa-16"><span>???</span></li>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>

                        </ul>
                    </div>

                </div>
                <div class="box-fifa-16 box-fifa-8">
                    <div class="check-box">
                        <img src="/images/check-true.png">
                    </div>
                    <div class="pull-left box-fifa-16-haft">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>
                            <li class="name-fifa-16"><span>???</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>
                            <li class="name-fifa-16"><span>???</span></li>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>

                        </ul>
                    </div>

                </div>
            </div>

            <div class="box-award">
                <img src="/images/shirt_preview.png">
                <h3>เสื้อลิขสิทธิ์แท้มูลค่า 5,000 บาท</h3>
            </div>
            <div class="btn-col">
                ยืนยันการเลือกทีม
            </div>
        </div>


<!--        Semi-final-->
        <div class="columns-fifa03">
            <div class="title-columns-fifa">
                <h2>Semi-final</h2>
                <small>Saturday 23 June</small>
            </div>
            <div class="content-top-fifa-8" style="margin-top: 80px;">
                <div class="box-fifa-16 box-fifa-8">
                    <div class="check-box">
                        <img src="/images/check-true.png">
                    </div>
                    <div class="pull-left box-fifa-16-haft">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>
                            <li class="name-fifa-16"><span>???</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>
                            <li class="name-fifa-16"><span>???</span></li>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>

                        </ul>
                    </div>

                </div>
            </div>
            <div class="content-top-fifa-8" style="margin-top: 113px;">
                <div class="box-fifa-16 box-fifa-8">
                    <div class="check-box">
                        <img src="/images/check-true.png">
                    </div>
                    <div class="pull-left box-fifa-16-haft">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>
                            <li class="name-fifa-16"><span>???</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>
                            <li class="name-fifa-16"><span>???</span></li>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>

                        </ul>
                    </div>

                </div>
            </div>
            <div class="box-award" style="margin-top: 25px;">
                <img src="/images/ps4-preview.png">
                <h3>PS4 มูลค่า 15,000 บาท</h3>
            </div>
            <div class="btn-col">
                ยืนยันการเลือกทีม
            </div>
        </div>


<!--        Final-->
        <div class="columns-fifa04">
            <div class="title-columns-fifa">
                <h2>Final</h2>
                <small>Thursday 14 June</small>
            </div>
            <div class="content-top-fifa-8" style="margin-top: 80px;">

                <div class="box-fifa-16 box-fifa-final" style="margin-top: 140px;">
                    <div class="check-box" style="margin-top: 34px;">
                        <img src="/images/check-true.png">
                    </div>
                    <div class="pull-left box-fifa-16-haft">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>
                            <li class="name-fifa-16"><span>???</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>


                            <li class="flag-fifa-16 text-right">
                                <img src="/images/Flag-default.jpg">
                            </li>
                            <li class="name-fifa-16 pull-right"><span>???</span></li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="box-award" style="margin-top: 75px;">
                <img src="/images/iphone_preview.png">
                <h3>IPhone X มูลค่า 35,000 บาท</h3>
            </div>
            <div class="btn-col">
                ยืนยันการเลือกทีม
            </div>
        </div>
    </div>
</div>


</body>
</html>
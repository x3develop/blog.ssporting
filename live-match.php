<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta http-equiv="Cache-control" content="public">
    <title>Ngoal | Live Score</title>
    <link rel="shortcut icon" type="images/x-icon" href="/images/favicon.ico.png">
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <!--    <link rel="stylesheet" href="css/style.css">-->
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/livescore.css">
    <link rel="stylesheet" href="/css/program_ball.css">
    <link rel="stylesheet" href="/css/game-redesign.css">
    <link rel="stylesheet" href="/css/circle-progress-bar.css">
    <link rel="stylesheet" href="/css/football-symbol.css">
    <!-- <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.carousel.css">
    <!-- <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css"> -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="/nanoscrollbar/nanoscrollbar.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <!-- <script src="/js/jquery/dist/jquery.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/js/readmore.js"></script>
    <!-- <script src="/css/bootstrap/js/bootstrap.min.js"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <!-- <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/owl.carousel.min.js"></script>
    <!-- <script src="/js/jquery-timeago/jquery.timeago.js"></script> -->
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="/js/highlight_live.js"></script>
    <!-- <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script> -->
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118679525-1"></script> -->
</head>
<?php
include_once __DIR__ . "/bootstrap.php";
use Carbon\Carbon;
use controller\HighlightController;
use controller\MatchController;

$hcon = new HighlightController();
$mcon = new MatchController();
$today = Carbon::now("UTC");
$today->setTimezone("Asia/Bangkok");
$nowday = Carbon::now("Asia/Bangkok");
$yesterday = Carbon::yesterday("Asia/Bangkok");
$tomorrow = Carbon::tomorrow("Asia/Bangkok");
$selectday = isset($_REQUEST['day']) ? $_REQUEST['day'] : $today->toDateString();

$matchlist = $mcon->getAllLSort($selectday);
// $hllist = $hcon->getHighlightAllLSort("2018-11-29");

// dd($hllist);

?>
<body data-spy="scroll" data-target=".navbar" data-offset="50">
<?php include $_SERVER['DOCUMENT_ROOT'] . '/view/index/menu-all.php'; ?>
<style>
    body {
        position: relative;
        background-color: #f1f2f5;
    }

    .affix {
        position: relative;
        top: 0;
        width: 100%;
        z-index: 9999 !important;
    }

    .navbar {
        margin-bottom: 0;
    }


</style>

<div class="side-select" data-spy="affix" data-offset-top="60">
    <h4 class="title-center">
        TOP LEAGUES
    </h4>
    <div class="navbar nav-pills nav-stacked">
        <ul class="nav navbar-nav">
            <?php foreach ($matchlist['order'] as $lid => $priority) { ?>
                <?php if ($priority <= 10) { ?>
                    <?php $data = $matchlist['data'][$lid]; ?>
                    <li><a href="#league-mark-<?php echo preg_replace('/\s+/', '', $data['league']->name); ?>">
                            <img src="<?php echo $data['league']->getLogo(); ?>">
                            <h5><?php echo $data['league']->name; ?></h5>
                            <span class="num-side"><?php echo count($data['matches']); ?> คู่</span>
                        </a>
                    </li>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>
    <div style="clear: both"></div>
    <h4 class="title-center">
        LEAGUES
    </h4>
    <div id="nav" class="navbar nav-pills nav-stacked">
        <ul class="nav navbar-nav">
            <?php foreach ($matchlist['order'] as $lid => $priority) { ?>
                <?php if ($priority > 10) { ?>
                    <?php $data = $matchlist['data'][$lid]; ?>
                    <li><a href="#league-mark-<?php echo preg_replace('/\s+/', '', $data['league']->name); ?>">
                            <img src="<?php echo $data['league']->getLogo(); ?>">
                            <h5><?php echo $data['league']->name; ?></h5>
                            <span class="num-side"><?php echo count($data['matches']); ?> คู่</span>
                        </a>
                    </li>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>

<div class="content-all-programe pull-right main" id="main">

    <div class="text-title" style="margin-top: 20px;">
        <div class="hamburger-live-match" id="hamburger" onclick="toggleSidenav();"></div>
        <h4>บอลวันนี้ โปรแกรมบอล ราคาบอล โปรแกรมฟุตบอล</h4>
        <div class="pull-right">
            <span id="hl-datemark" class="hide"></span>
            <div class="inline-box">Date :</div>
            <div class="date-review btn-group" role="group" style="margin-right: 20px;">
                <a class="btn btn-sm btn-default btn-review <?php echo ($selectday == $yesterday->toDateString()) ? "active" : ""; ?>"
                   href="/live-match.php?day=<?php echo $yesterday->toDateString(); ?>">Yesterday</a>
                <a class="btn btn-sm btn-default btn-review <?php echo ($selectday == $nowday->toDateString()) ? "active" : ""; ?>"
                   href="/live-match.php?day=<?php echo $nowday->toDateString(); ?>">Today</a>
                <a class="btn btn-sm btn-default btn-review <?php echo ($selectday == $tomorrow->toDateString()) ? "active" : ""; ?>"
                   href="/live-match.php?day=<?php echo $tomorrow->toDateString(); ?>">Tomorrow</a>
            </div>
            <div class="inline-box">Filter By :</div>
            <div class="dropdown pull-right">
                <button class="btn btn-default dropdown-toggle btn-sm" type="button" id="dropdownMenu1"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <a id="filter-mark">ALL</a>
                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li class="filter-option" option="all"><a href="#">All Match</a></li>
                    <li class="filter-option" option="live"><a href="#">Live Match</a></li>
                    <li class="filter-option" option="end"><a href="#">End Match</a></li>
                </ul>
            </div>
        </div>
    </div>
    <?php foreach ($matchlist['order'] as $lid => $priority) { ?>
        <?php $data = $matchlist['data'][$lid]; ?>
        <div class="content-program-soccer"
             id="league-mark-<?php echo preg_replace('/\s+/', '', $data['league']->name); ?>">
            <div class="topic">
                <div class="logo-league-program">
                    <img src="<?php echo $data['league']->getLogo(); ?>">
                </div>
                <div class="title-league-program">
                    <h3><?php echo $data['league']->name; ?></h3>
                    <h5>บ้านผลบอล โปรแกรมบอล ราคาบอล </h5>
                </div>
                <div class="dropdown pull-right">
                </div>

            </div>
            <div class="content">
                <div class="panel-group panel-group-row" id="accordion<?php echo $lid; ?>">
                    <!--full match-->
                    <?php foreach ($data['matches'] as $match) { ?>
                        <div class="panel panel-default livescore-panel color-tape " mid="<?php echo $match->id; ?>">
                            <div class="panel-heading">
                                <div mid="<?php echo $match->id; ?>" class="hdp-float wait-float">
                                    <ul>
                                        <li class="live-h-hdp"
                                            mid="<?php echo $match->id; ?>"><?php echo (empty($match->hdp)) ? "-" : number_format($match->hdp->home_water_bill, 2); ?></li>
                                        <li><h3 class="live-hdp"
                                                mid="<?php echo $match->id; ?>"><?php echo (empty($match->hdp)) ? "-" : $match->hdp->handicap; ?></h3>
                                        </li>
                                        <li class="live-a-hdp"
                                            mid="<?php echo $match->id; ?>"><?php echo (empty($match->hdp)) ? "-" : number_format($match->hdp->away_water_bill, 2); ?></li>
                                    </ul>
                                </div>
                                <div mid="<?php echo $match->id; ?>" class="hdp-float hide FT-float">
                                    <ul>
                                        <li class="live-h-score"
                                            mid="<?php echo $match->id; ?>"><?php echo $match->live_score_home; ?></li>
                                        <li><h3>FT</h3></li>
                                        <li class="live-a-score"
                                            mid="<?php echo $match->id; ?>"><?php echo $match->live_score_away; ?></li>
                                    </ul>
                                </div>
                                <div mid="<?php echo $match->id; ?>" class="hdp-float hide live-float">
                                    <ul>
                                        <li class="live-h-score"
                                            mid="<?php echo $match->id; ?>"><?php echo $match->live_score_home; ?></li>
                                        <li>
                                            <div class="progress minute-progress" mid="<?php echo $match->id; ?>"
                                                 data-percentage="40">
				                            <span class="progress-left">
                                                <span class="progress-bar"></span>
				                            </span>
                                                <span class="progress-right">
                                                <span class="progress-bar"></span>
                                            </span>
                                                <div class="progress-value">
                                                    <div class="live-minute" mid="<?php echo $match->id; ?>">45+</div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="live-a-score"
                                            mid="<?php echo $match->id; ?>"><?php echo $match->live_score_away; ?></li>
                                    </ul>
                                </div>
                                <div class="left-accordion-row pull-left">
                                    <div class="pull-right logo-team-ponball">
                                        <img src="<?php echo $match->teama->getLogo(); ?>">
                                    </div>
                                    <div class="pull-right detail-team-ponball">
                                        <div class="top-detail-team-ponball text-right">
                                            <h4><?php echo $match->teama->name_en; ?></h4>
                                        </div>
                                        <div class="bottom-detail-team-ponball">
                                            <div class="score-ct beton-homep" mid="<?php echo $match->id; ?>">0%</div>
                                            <div class="progress full-width">
                                                <div class="progress-bar pull-right beton-home"
                                                     mid="<?php echo $match->id; ?>" role="progressbar"
                                                     aria-valuenow="0"
                                                     aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-right">
                                        <ul class="status-lastMatch full-width">
                                            <li><p>Last Match</p></li>
                                            <?php foreach (array_reverse(explode(",", $match->lastresult_home)) as $rs) { ?>
                                                <?php
                                                $rsclass = "draw-status";
                                                switch ($rs) {
                                                    case "w":
                                                        $rsclass = "win-status";
                                                        break;
                                                    case "l":
                                                        $rsclass = "lose-status";
                                                        break;
                                                    default:
                                                        $rsclass = "draw-status";
                                                        break;
                                                }

                                                ?>
                                                <li>
                                                    <div class="<?php echo $rsclass; ?>"></div>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                        <ul class="status-lastMatch full-width odd">
                                            <li><p>ODD</p></li>
                                            <?php foreach (array_reverse(explode(",", $match->lastresult_odds_home)) as $rs) { ?>
                                                <?php
                                                $rsclass = "draw-status";
                                                switch ($rs) {
                                                    case "w":
                                                        $rsclass = "win-status";
                                                        break;
                                                    case "l":
                                                        $rsclass = "lose-status";
                                                        break;
                                                    default:
                                                        $rsclass = "draw-status";
                                                        break;
                                                }

                                                ?>
                                                <li>
                                                    <div class="<?php echo $rsclass; ?>"></div>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>

                                    <div class="pull-left content-col-left">
                                        <div class="at-minute ct-ft">
                                            <?php
                                            $stime = Carbon::createFromFormat("Y-m-d H:i:s", $match->time_match, "Asia/Singapore");
                                            $stime->tz = "Asia/Bangkok";
                                            ?>
                                            <span class="at-minute-mark label-ft" at="<?php echo $match->time_match; ?>"
                                                  mid="<?php echo $match->id; ?>" state="<?php echo $match->state; ?>"
                                                  ht="<?php echo $match->ht_start; ?>"><?php echo $stime->format("H:i"); ?></span>
                                        </div>
                                        <div class="ct-live-chanel hide">
                                            <img src="/images/channel/pptv.png">
                                        </div>
                                    </div>
                                </div>
                                <div class="right-accordion-row pull-right">
                                    <div class="pull-left logo-team-ponball">
                                        <img src="<?php echo $match->teamb->getLogo(); ?>">
                                    </div>
                                    <div class="pull-left detail-team-ponball">
                                        <div class="top-detail-team-ponball text-left">
                                            <h4><?php echo $match->teamb->name_en; ?></h4>
                                        </div>
                                        <div class="bottom-detail-team-ponball">
                                            <div class="progress full-width">
                                                <div class="progress-bar progress-bar-danger beton-away"
                                                     mid="<?php echo $match->id; ?>" role="progressbar"
                                                     aria-valuenow="0"
                                                     aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                </div>
                                            </div>
                                            <div class="score-ct beton-awayp" mid="<?php echo $match->id; ?>">0%</div>
                                        </div>
                                    </div>
                                    <div class="pull-left">
                                        <ul class="status-lastMatch full-width">
                                            <?php foreach (explode(",", $match->lastresult_away) as $rs) { ?>
                                                <?php
                                                $rsclass = "draw-status";
                                                switch ($rs) {
                                                    case "w":
                                                        $rsclass = "win-status";
                                                        break;
                                                    case "l":
                                                        $rsclass = "lose-status";
                                                        break;
                                                    default:
                                                        $rsclass = "draw-status";
                                                        break;
                                                }

                                                ?>
                                                <li>
                                                    <div class="<?php echo $rsclass; ?>"></div>
                                                </li>
                                            <?php } ?>
                                            <li><p>Last Match</p></li>
                                        </ul>
                                        <ul class="status-lastMatch full-width odd">
                                            <?php foreach (explode(",", $match->lastresult_odds_away) as $rs) { ?>
                                                <?php
                                                $rsclass = "draw-status";
                                                switch ($rs) {
                                                    case "w":
                                                        $rsclass = "win-status";
                                                        break;
                                                    case "l":
                                                        $rsclass = "lose-status";
                                                        break;
                                                    default:
                                                        $rsclass = "draw-status";
                                                        break;
                                                }

                                                ?>
                                                <li>
                                                    <div class="<?php echo $rsclass; ?>"></div>
                                                </li>
                                            <?php } ?>
                                            <li><p>ODD</p></li>
                                        </ul>
                                    </div>
                                    <div class="pull-right-section">
                                        <div class="intro-game">
                                            <a href="<?php echo "/match/{$match->id}.html"; ?>" data-toggle="tooltip"
                                               data-placement="top"
                                               title="เล่นเกมทายผล">
                                                <i class="fa fa-futbol-o" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                        <a data-toggle="collapse" class="review-dropdown collapsed"
                                           data-parent="#accordion<?php echo $lid; ?>"
                                           href="#<?php echo "collapse_endmatch" . $match->id; ?>"
                                           mid="<?php echo $match->id; ?>">
                                            <h4>Live score</h4>
                                        </a>
                                    </div>

                                </div>
                                <div style="clear: both"></div>
                            </div>
                            <div id="<?php echo "collapse_endmatch" . $match->id; ?>" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs nav-tabs-highlight" role="tablist">
                                        <li class="<?php echo ($match->state != 17) ? "active" : ""; ?>"
                                            role="presentation">
                                            <a href="#<?php echo "livescore-tab" . $match->id; ?>" role="tab"
                                               data-toggle="tab">Livescore</a></li>
                                        <li class="<?php echo ($match->state == 17) ? "active" : ""; ?>"
                                            role="presentation">
                                            <a href="#<?php echo "review" . $match->id; ?>" role="tab"
                                               data-toggle="tab">ทีเด็ด-ทรรศนะ</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel"
                                             class="tab-pane <?php echo ($match->state != 17) ? "active" : ""; ?>"
                                             id="<?php echo "livescore-tab" . $match->id; ?>">
                                            <div class="col-md-4">

                                                <div class="thumbnail">

                                                    <div class="header-event-match">
                                                        <ul>
                                                            <li><h3>EVENT MATCH</h3></li>
                                                            <li><span class="event-dp-button end-match"
                                                                      mid="<?php echo $match->id; ?>">End Match</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="score-event-match">

                                                        <div class="pull-right">
                                                            <img src="<?php echo $match->teama->getLogo(); ?>">
                                                            <p><?php echo $match->teama->name_en; ?></p>

                                                        </div>
                                                        <h4><span class="live-h-score"
                                                                  mid="<?php echo $match->id; ?>"><?php echo $match->live_score_home; ?></span>-<span
                                                                    class="live-a-score"
                                                                    mid="<?php echo $match->id; ?>"><?php echo $match->live_score_away; ?></span>
                                                        </h4>
                                                        <div class="pull-left text-right">
                                                            <p><?php echo $match->teamb->name_en; ?></p>
                                                            <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                        </div>

                                                    </div>
                                                    <div class="content-event-match" mid="<?php echo $match->id; ?>">
                                                        <?php $matchevent = $mcon->getEvent($match->id); ?>
                                                        <?php foreach ($matchevent as $event) { ?>
                                                            <?php if ($event->side == "home") { ?>
                                                                <div class="full-width line-content-all">
                                                                    <div class="col-home-live">
                                                                        <div class="box-football-icon">
                                                                            <div class="<?php echo $event->getEventImgClass(); ?>"></div>
                                                                        </div>
                                                                        <div class="min-live"><?php echo $event->at_minute . "'"; ?></div>
                                                                        <div class="score-live"><?php echo $event->score; ?></div>
                                                                        <?php if (empty($event->assist)) { ?>
                                                                            <div class="name-live"><?php echo $event->player; ?></div>
                                                                        <?php } else { ?>
                                                                            <?php if ($event->event == "substutitions") { ?>
                                                                                <div class="name-live">
                                                                                    <span class="substitute-player-out"></span>
                                                                                    <p><?php echo $event->player; ?></p>
                                                                                </div>
                                                                                <div class="name-live">
                                                                                    <span class="substitute-player-in"></span>
                                                                                    <p><?php echo $event->assist; ?></p>
                                                                                </div>
                                                                            <?php } else { ?>
                                                                                <div class="name-live"><?php echo $event->player . " (" . $event->assist . ")"; ?></div>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            <?php } else { ?>
                                                                <div class="full-width line-content-all">
                                                                    <div class="col-away-live pull-right">
                                                                        <?php if (empty($event->assist)) { ?>
                                                                            <div class="name-live"><?php echo $event->player; ?></div>
                                                                        <?php } else { ?>
                                                                            <?php if ($event->event == "substutitions") { ?>
                                                                                <div class="name-live">
                                                                                    <span class="substitute-player-in"></span>
                                                                                    <p><?php echo $event->assist; ?></p>
                                                                                </div>
                                                                                <div class="name-live">
                                                                                    <span class="substitute-player-out"></span>
                                                                                    <p><?php echo $event->player; ?></p>
                                                                                </div>
                                                                            <?php } else { ?>
                                                                                <div class="name-live"><?php echo "(" . $event->assist . ") " . $event->player; ?></div>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                        <div class="score-live"><?php echo $event->score; ?></div>
                                                                        <div class="min-live"><?php echo $event->at_minute . "'"; ?></div>
                                                                        <div class="box-football-icon">
                                                                            <div class="<?php echo $event->getEventImgClass(); ?>"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            <?php } ?>
                                                        <?php } ?>


                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 content-status-match">
                                                <div class="thumbnail">

                                                    <!-- Nav tabs -->
                                                    <ul class="nav nav-tabs" role="tablist">
                                                        <li role="presentation" class="active"><a
                                                                    href="#<?php echo "statistics-tab" . $match->id; ?>"
                                                                    role="tab"
                                                                    data-toggle="tab">Statistics</a>
                                                        </li>
                                                        <li role="presentation"><a
                                                                    href="#<?php echo "lineUp-tab" . $match->id; ?>"
                                                                    role="tab"
                                                                    data-toggle="tab">Line
                                                                Up</a>
                                                        </li>

                                                    </ul>

                                                    <!-- Tab panes -->
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane active"
                                                             id="<?php echo "statistics-tab" . $match->id; ?>">
                                                            <div class="table statistics-table">
                                                                <table>
                                                                    <thead>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="pull-right">
                                                                                <h4><?php echo $match->teama->name_en; ?></h4>
                                                                                <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                            </div>
                                                                        </td>
                                                                        <td><h3>VS</h3></td>
                                                                        <td>
                                                                            <div class="pull-left">
                                                                                <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                                <h4><?php echo $match->teamb->name_en; ?></h4>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="statistics-mark"
                                                                           mid="<?php echo $match->id; ?>">
                                                                    <?php $statlist = $mcon->getStat($match->id); ?>
                                                                    <?php foreach ($statlist as $stat) { ?>
                                                                        <?php $rmax = $stat->hval + $stat->aval; ?>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="num-progress <?php echo ($stat->hval > $stat->aval) ? "active" : ""; ?>"><?php echo $stat->hval; ?></div>
                                                                                <div class="progress">
                                                                                    <div class="progress-bar progress-bar-danger pull-right"
                                                                                         role="progressbar"
                                                                                         aria-valuenow="<?php echo $stat->hval; ?>"
                                                                                         aria-valuemin="0"
                                                                                         aria-valuemax="<?php echo $rmax; ?>"
                                                                                         style="width: <?php echo $stat->hval / $rmax * 100; ?>%;">
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td><?php echo $stat->type; ?></td>
                                                                            <td>
                                                                                <div class="progress">
                                                                                    <div class="progress-bar"
                                                                                         role="progressbar"
                                                                                         aria-valuenow="<?php echo $stat->aval; ?>"
                                                                                         aria-valuemin="0"
                                                                                         aria-valuemax="<?php echo $rmax; ?>"
                                                                                         style="width: <?php echo $stat->aval / $rmax * 100; ?>%;">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="num-progress <?php echo ($stat->hval < $stat->aval) ? "active" : ""; ?>"><?php echo $stat->aval; ?></div>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane"
                                                             id="<?php echo "lineUp-tab" . $match->id; ?>">
                                                            <div class="lineUp-table">
                                                                <table class="table">
                                                                    <tr>
                                                                        <td>
                                                                            <div class="pull-right full-width text-right">
                                                                                <span><?php echo $match->teama->name_en; ?></span>
                                                                                <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                            </div>
                                                                            <div class="pull-right full-width">
                                                                                <div class="content-lineUp-home">
                                                                                    <ul>
                                                                                        <?php
                                                                                        $lineups = $mcon->getLineups($match->id . "");
                                                                                        foreach ($lineups as $lineup) {
                                                                                            if ($lineup->side == "home") {
                                                                                                ?>
                                                                                                <li>
                                                                                                    <div class="num-player"><?php echo $lineup->number; ?></div>
                                                                                                    <div><?php echo $lineup->name; ?></div>
                                                                                                    <div><?php echo $lineup->position; ?></div>
                                                                                                </li>
                                                                                            <?php } ?>
                                                                                        <?php } ?>

                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="pull-left">
                                                                                <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                                <span><?php echo $match->teamb->name_en; ?></span>
                                                                            </div>
                                                                            <div class="pull-left full-width">
                                                                                <div class="content-lineUp-away">
                                                                                    <ul>
                                                                                        <?php
                                                                                        $lineups = $mcon->getLineups($match->id . "");
                                                                                        foreach ($lineups as $lineup) {
                                                                                            if ($lineup->side == "away") {
                                                                                                ?>
                                                                                                <li>
                                                                                                    <div class="num-player"><?php echo $lineup->number; ?></div>
                                                                                                    <div><?php echo $lineup->name; ?></div>
                                                                                                    <div><?php echo $lineup->position; ?></div>
                                                                                                </li>
                                                                                            <?php } ?>
                                                                                        <?php } ?>

                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>


                                                                </table>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                        <div role="tabpanel"
                                             class="tab-pane <?php echo ($match->state == 17) ? "active" : ""; ?>"
                                             id="<?php echo "review" . $match->id; ?>">
                                            <div class="home-panel-body pull-left">
                                                <?php foreach ($match->reviews as $review) { ?>
                                                    <?php if ($review->team == "home") { ?>
                                                        <div class="content-home-review full-width pull-right">
                                                            <div class="pull-right">
                                                                <div class="area-logoName">
                                                                    <div class="pull-right logo-review-user">
                                                                        <img src="<?php echo $review->owner->path; ?>">
                                                                    </div>
                                                                    <div class="pull-right">
                                                                        <h4><?php echo $review->owner->name; ?></h4>
                                                                        <ul>
                                                                            <li><?php echo "@" . strtolower(str_replace(" ", "", $match->teama->name_en)); ?></li>
                                                                            <li>
                                                                                <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="pull-right text-review-content text-right">
                                                                <p>
                                                                    <?php echo $review->review_text; ?>
                                                                </p>
                                                            </div>
                                                            <div class="pull-left ct-winrate">
                                                                <div>
                                                                    <h3><?php echo number_format($review->owner->accurate, 0) . "%"; ?></h3>
                                                                    <small>Win rate</small>
                                                                </div>
                                                                <div>
                                                                    <h3>30</h3>
                                                                    <small>Matches</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                            <div class="away-panel-body pull-right">

                                                <?php foreach ($match->reviews as $review) { ?>
                                                    <?php if ($review->team == "away") { ?>
                                                        <div class="content-away-review full-width pull-left">
                                                            <div class="pull-left">
                                                                <div class="area-logoName area-logoName-away">
                                                                    <div class="pull-left logo-review-user">
                                                                        <img src="<?php echo $review->owner->path; ?>">
                                                                    </div>
                                                                    <div class="pull-left">
                                                                        <h4><?php echo $review->owner->name; ?></h4>
                                                                        <ul>
                                                                            <li>
                                                                                <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                            </li>
                                                                            <li><?php echo "@" . strtolower(str_replace(" ", "", $match->teamb->name_en)); ?></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="pull-left text-review-content text-left">
                                                                <p>
                                                                    <?php echo $review->review_text; ?>
                                                                </p>
                                                            </div>
                                                            <div class="pull-right ct-winrate">
                                                                <div>
                                                                    <h3><?php echo number_format($review->owner->accurate, 0) . "%"; ?></h3>
                                                                    <small>Win rate</small>
                                                                </div>
                                                                <div>
                                                                    <h3>30</h3>
                                                                    <small>Matches</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                            <div class="full-width text-center">
                                                <a class="btn btn-default btn-sm"
                                                   href="/review-infomation.php?match_id=<?php echo $match->id; ?>"
                                                   role="button">อ่านทีเด็ดทรรศนะทั้งหมด</a>
                                            </div>
                                        </div>
                                        <div style="clear: both"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>


                </div>
            </div>
        </div>
    <?php } ?>

</div>


<script>

    function toggleSidenav() {
        document.body.classList.toggle('sidenav-active');
    }


    $("#nav ul li a[href^='#']").on('click', function (e) {

        // prevent default anchor click behavior
        e.preventDefault();

        // store hash
        var hash = this.hash;

        // animate
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 1000, function () {

            // when done, add hash to url
            // (default click behaviour)
            window.location.hash = hash;
        });

    });
</script>
</body>
</html>
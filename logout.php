<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 11/21/2016
 * Time: 10:50
 */
session_start();
if(isset($_SESSION['uid']) || isset($_SESSION['login']) || isset($_SESSION['menu']))
{
    unset($_SESSION['login']);
    unset($_SESSION['menu']);
    session_destroy();
    header("refresh: 0; url=".$_SERVER['HTTP_REFERER']);
}
?>
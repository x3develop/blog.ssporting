<?php
session_start();
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playBet.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playComment.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playReviewMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playLeague.php";
$playLeague = new playLeague();
$playMatch = new playMatch();
$playComment = new playComment();
$playReviewMatch = new playReviewMatch();
$playBet = new playBet();
$mid = ($_REQUEST["mid"]) ? $_REQUEST['mid'] : 0;
$match = $playMatch->getFirstMathStatusBet(30);

if (!empty($mid)) {
    $matchView = $playMatch->getFirstMathById($mid)[0];
} elseif (!empty($match[0]->id)) {
    $matchView = $playMatch->getFirstMathById($match[0]->id)[0];
}else{
    header('Location: /');
    exit();
}

$matchResultsHome = $playMatch->getListMatchResults($matchView->team_home, 6);
$matchResultsAway = $playMatch->getListMatchResults($matchView->team_away, 6);
$userListHome = $playMatch->getUserBet($matchView->id);
$percentBet = $playBet->percentBet($matchView->id);
//comment
$comment = $playMatch->getListComment($matchView->id);
$commentMini = array();
foreach ($comment as $key => $value) {
    if (!empty($value->countComment)) {
        $commentMini[$value->id] = $playComment->getMiniComment($matchView->id, $value->id);
    }
}

$commentHome = $playMatch->getListCommentByTeam($matchView->id, 'home');
$commentAway = $playMatch->getListCommentByTeam($matchView->id, 'away');
$heartList = $playComment->checkHeartByMatch($matchView->id);
$heartListByUser = $playComment->checkHeartByUser($matchView->id);
$reviewMatch = $playReviewMatch->getReviewAndUserMatchByMatchId($matchView->id);
$resultsReviewMatch = array();
foreach ($reviewMatch as $key => $value) {
    $resultsReviewMatch[$value['user_review_id']] = $playReviewMatch->getResultsReviewMatchByUserReview($value['user_review_id'], 14);
}

if (isset($_SESSION['login'])) {
    $matchBet = $playBet->getTeamMatchById($_SESSION['login']['id'], $mid);
}

//HEAD-TO-HEAD
$dataHeadToHead=$playMatch->getHeadToHead($matchView->team_home,$matchView->team_away);
$dataFormGuideHome=$playMatch->getFormGuide($matchView->team_home,50);
$dataFormGuideAway=$playMatch->getFormGuide($matchView->team_away,50);
$dataNextMatchHome=$playMatch->getNextMatch($matchView->team_home,$matchView->id);
$dataNextMatchAway=$playMatch->getNextMatch($matchView->team_away,$matchView->id);
$tableLeague = $playLeague->checkTeamByLeague($matchView->league_id);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/livescore.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/game-redesign.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="js/jquery/dist/jquery.min.js"></script>
    <script src="js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="nanoscrollbar/nanoscrollbar.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/timeview.js"></script>
    <script src="js/game.js"></script>
    <script src="js/sly/sly.js"></script>
    <?php include 'view/index/googletagmanager.php'; ?>
</head>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="content-livescore-main content-game-redesign">
    <div class="container">
        <div class="row">
            <div class="content-list-livescore">
                <div class="col-md-12">
                    <div class="col-md-3 active">
                        <div class="thumbnail content-team-slide">
                            <div class="pull-left">
                                <table>
                                    <tr>
                                        <td><img src="/images/logo-team/Chelsea.png"></td>
                                        <td>Chelsea</td>
                                    </tr>
                                    <tr>
                                        <td><img src="/images/logo-team/manchester.png"></td>
                                        <td class="active">manchester</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="pull-right">
                                <div class="time-live">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p>21:00</p>
                                </div>
                                <div class="HDP-live">HDP</div>
                                <div class="num-live">-025</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="thumbnail content-team-slide">
                            <div class="pull-left">
                                <table>
                                    <tr>
                                        <td><img src="/images/logo-team/Chelsea.png"></td>
                                        <td>Chelsea</td>
                                    </tr>
                                    <tr>
                                        <td><img src="/images/logo-team/manchester.png"></td>
                                        <td class="active">manchester</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="pull-right">
                                <div class="time-live">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p>21:00</p>
                                </div>
                                <div class="HDP-live">HDP</div>
                                <div class="num-live">-025</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="thumbnail content-team-slide">
                            <div class="pull-left">
                                <table>
                                    <tr>
                                        <td><img src="/images/logo-team/Chelsea.png"></td>
                                        <td>Chelsea</td>
                                    </tr>
                                    <tr>
                                        <td><img src="/images/logo-team/manchester.png"></td>
                                        <td class="active">manchester</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="pull-right">
                                <div class="time-live">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p>21:00</p>
                                </div>
                                <div class="HDP-live">HDP</div>
                                <div class="num-live">-025</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="thumbnail content-team-slide">
                            <div class="pull-left">
                                <table>
                                    <tr>
                                        <td><img src="/images/logo-team/Chelsea.png"></td>
                                        <td>Chelsea</td>
                                    </tr>
                                    <tr>
                                        <td><img src="/images/logo-team/manchester.png"></td>
                                        <td class="active">manchester</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="pull-right">
                                <div class="time-live">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p>21:00</p>
                                </div>
                                <div class="HDP-live">HDP</div>
                                <div class="num-live">-025</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="topic-league">
                <h3><img src="/images/logo-team/fa-cup-2017.png"> ENGLAND FA CUP</h3>
                <ul>
                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> 20:00</li>
                    <li>|</li>
                    <li><i class="fa fa-calendar" aria-hidden="true"></i> 21/07/2018</li>
                </ul>
            </div>
            <div class="content-select-team-vote">

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="pull-right">
                                    <div class="name-team-vote pull-left text-right">
                                        <h3>Chelsea</h3>
                                    </div>
                                    <div class="box-vote-team">
                                        <div class="logo-vote-team-home">
                                            <img src="/images/logo-team/Chelsea.png">
                                        </div>
                                        <div class="btn-vote-team-home">
                                            VOTE
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="pull-left">
                                    <div class="box-vote-team">
                                        <div class="logo-vote-team-away">
                                            <img src="/images/logo-team/manchester.png">
                                        </div>
                                        <div class="btn-vote-team-away">
                                            VOTE
                                        </div>
                                    </div>
                                    <div class="name-team-vote pull-left">
                                        <h3>Manchester UTD</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <ul class="content-HDP-game">
                <li class="box-last-match text-right">
                    <table>
                        <tr>
                            <td><i class="fa fa-caret-left" aria-hidden="true"></i></td>
                            <td><img src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td><img src="/images/icon-stat/betlist_result/lose_half.png"></td>
                            <td><img src="/images/icon-stat/betlist_result/draw.png"></td>
                            <td><img src="/images/icon-stat/betlist_result/win_half.png"></td>
                            <td><img src="/images/icon-stat/betlist_result/win.png"></td>
                            <td>Last 5 Match</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><img src="/images/2.jpg"></td>
                            <td><img src="/images/9.jpg"></td>
                            <td><img src="/images/p1.png"></td>
                            <td>Player</td>
                        </tr>
                    </table>
                </li>
                <li class="box-HDP-game text-center">
                    <table>
                        <tr>
                            <td class="text-right"><p>1.99</p></td>
                            <td><h2>[0.5]</h2></td>
                            <td class="text-left"><p>1.88</p></td>
                        </tr>
                    </table>
                </li>
                <li class="box-last-match text-left">
                    <table>
                        <tr>
                            <td>Last 5 Match</td>
                            <td><i class="fa fa-caret-left" aria-hidden="true"></i></td>
                            <td><img src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td><img src="/images/icon-stat/betlist_result/lose_half.png"></td>
                            <td><img src="/images/icon-stat/betlist_result/draw.png"></td>
                            <td><img src="/images/icon-stat/betlist_result/win_half.png"></td>
                            <td><img src="/images/icon-stat/betlist_result/win.png"></td>
                        </tr>
                        <tr>
                            <td>Player</td>
                            <td></td>
                            <td><img src="/images/2.jpg"></td>
                            <td><img src="/images/9.jpg"></td>
                            <td><img src="/images/p1.png"></td>
                            <td></td>
                            <td></td>

                        </tr>
                    </table>
                </li>
            </ul>
        </div>
    </div>
</div>

<!--ทรรศนะ-->
<div class="wrap-preview-game">
    <div class="container">
        <div class="title-label-rota">PREVIEW</div>
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <div class="col-sm-4 col-md-4">
                        <div class="thumbnail box-preview" data-toggle="modal" data-target="#showReviewGuru">
                            <div class="top-box-preview">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="box-guru-img pull-left">
                                                <img src="/images/sample-logo01.png">
                                            </div>
                                            <div class="pull-left">
                                                <h5>Thscore</h5>
                                                <div class="box-player-preview">
                                                    <img src="/images/logo-team/Chelsea.png">
                                                    <span>@ Chelsea</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-link"><i class="fa fa-angle-right"
                                                                                              aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div class="pull-right">
                                                <h3>50%</h3>
                                                <p>Win rate</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="button-box-preview">
                                <p>
                                    เซบีย่ายังมีลุ้นพื้นที่ยุโรปนัดนี้ต้องเน้นเต็มที่แม้ฟอร์มจะ
                                    แผ่วลงไปแต่โซเซียดาดเกมเยือนยังเล่นไม่แน่นอนนัด
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="thumbnail box-preview" data-toggle="modal" data-target="#showReviewPlayer">
                            <div class="top-box-preview">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="box-user-img pull-left">
                                                <img src="/images/p1.png">
                                            </div>
                                            <div class="pull-left">
                                                <h5>กระบี่มือหนึ่ง</h5>
                                                <div class="box-player-preview">
                                                    <img src="/images/logo-team/manchester.png">
                                                    <span>@ Manchester UTD</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-link"><i class="fa fa-angle-right"
                                                                                              aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div class="pull-right">
                                                <h3>50%</h3>
                                                <p>Win rate</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="button-box-preview">
                                <p>
                                    เซบีย่ายังมีลุ้นพื้นที่ยุโรปนัดนี้ต้องเน้นเต็มที่แม้ฟอร์มจะ
                                    แผ่วลงไปแต่โซเซียดาดเกมเยือนยังเล่นไม่แน่นอนนัด
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="thumbnail box-preview">
                            <div class="top-box-preview">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="box-guru-img pull-left">
                                                <img src="/images/sample-logo01.png">
                                            </div>
                                            <div class="pull-left">
                                                <h5>Thscore</h5>
                                                <div class="box-player-preview">
                                                    <img src="/images/logo-team/Chelsea.png">
                                                    <span>@ Chelsea</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-link"><i class="fa fa-angle-right"
                                                                                              aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div class="pull-right">
                                                <h3>50%</h3>
                                                <p>Win rate</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="button-box-preview">
                                <p>
                                    เซบีย่ายังมีลุ้นพื้นที่ยุโรปนัดนี้ต้องเน้นเต็มที่แม้ฟอร์มจะ
                                    แผ่วลงไปแต่โซเซียดาดเกมเยือนยังเล่นไม่แน่นอนนัด
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="thumbnail box-preview">
                            <div class="top-box-preview">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="box-guru-img pull-left">
                                                <img src="/images/sample-logo01.png">
                                            </div>
                                            <div class="pull-left">
                                                <h5>Thscore</h5>
                                                <div class="box-player-preview">
                                                    <img src="/images/logo-team/Chelsea.png">
                                                    <span>@ Chelsea</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-link"><i class="fa fa-angle-right"
                                                                                              aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div class="pull-right">
                                                <h3>50%</h3>
                                                <p>Win rate</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="button-box-preview">
                                <p>
                                    เซบีย่ายังมีลุ้นพื้นที่ยุโรปนัดนี้ต้องเน้นเต็มที่แม้ฟอร์มจะ
                                    แผ่วลงไปแต่โซเซียดาดเกมเยือนยังเล่นไม่แน่นอนนัด
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="thumbnail box-preview">
                            <div class="top-box-preview">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="box-user-img pull-left">
                                                <img src="/images/p1.png">
                                            </div>
                                            <div class="pull-left">
                                                <h5>กระบี่มือหนึ่ง</h5>
                                                <div class="box-player-preview">
                                                    <img src="/images/logo-team/manchester.png">
                                                    <span>@ Manchester UTD</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-link"><i class="fa fa-angle-right"
                                                                                              aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div class="pull-right">
                                                <h3>50%</h3>
                                                <p>Win rate</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="button-box-preview">
                                <p>
                                    เซบีย่ายังมีลุ้นพื้นที่ยุโรปนัดนี้ต้องเน้นเต็มที่แม้ฟอร์มจะ
                                    แผ่วลงไปแต่โซเซียดาดเกมเยือนยังเล่นไม่แน่นอนนัด
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="thumbnail box-preview">
                            <div class="top-box-preview">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="box-guru-img pull-left">
                                                <img src="/images/sample-logo01.png">
                                            </div>
                                            <div class="pull-left">
                                                <h5>Thscore</h5>
                                                <div class="box-player-preview">
                                                    <img src="/images/logo-team/Chelsea.png">
                                                    <span>@ Chelsea</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-link"><i class="fa fa-angle-right"
                                                                                              aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div class="pull-right">
                                                <h3>50%</h3>
                                                <p>Win rate</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="button-box-preview">
                                <p>
                                    เซบีย่ายังมีลุ้นพื้นที่ยุโรปนัดนี้ต้องเน้นเต็มที่แม้ฟอร์มจะ
                                    แผ่วลงไปแต่โซเซียดาดเกมเยือนยังเล่นไม่แน่นอนนัด
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="col-sm-4 col-md-4">
                        <div class="thumbnail box-preview">
                            <div class="top-box-preview">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="box-guru-img pull-left">
                                                <img src="/images/sample-logo01.png">
                                            </div>
                                            <div class="pull-left">
                                                <h5>Thscore</h5>
                                                <div class="box-player-preview">
                                                    <img src="/images/logo-team/Chelsea.png">
                                                    <span>@ Chelsea</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-link"><i class="fa fa-angle-right"
                                                                                              aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div class="pull-right">
                                                <h3>50%</h3>
                                                <p>Win rate</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="button-box-preview">
                                <p>
                                    เซบีย่ายังมีลุ้นพื้นที่ยุโรปนัดนี้ต้องเน้นเต็มที่แม้ฟอร์มจะ
                                    แผ่วลงไปแต่โซเซียดาดเกมเยือนยังเล่นไม่แน่นอนนัด
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="thumbnail box-preview">
                            <div class="top-box-preview">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="box-user-img pull-left">
                                                <img src="/images/p1.png">
                                            </div>
                                            <div class="pull-left">
                                                <h5>กระบี่มือหนึ่ง</h5>
                                                <div class="box-player-preview">
                                                    <img src="/images/logo-team/manchester.png">
                                                    <span>@ Manchester UTD</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-link"><i class="fa fa-angle-right"
                                                                                              aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div class="pull-right">
                                                <h3>50%</h3>
                                                <p>Win rate</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="button-box-preview">
                                <p>
                                    เซบีย่ายังมีลุ้นพื้นที่ยุโรปนัดนี้ต้องเน้นเต็มที่แม้ฟอร์มจะ
                                    แผ่วลงไปแต่โซเซียดาดเกมเยือนยังเล่นไม่แน่นอนนัด
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="thumbnail box-preview">
                            <div class="top-box-preview">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="box-guru-img pull-left">
                                                <img src="/images/sample-logo01.png">
                                            </div>
                                            <div class="pull-left">
                                                <h5>Thscore</h5>
                                                <div class="box-player-preview">
                                                    <img src="/images/logo-team/Chelsea.png">
                                                    <span>@ Chelsea</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-link"><i class="fa fa-angle-right"
                                                                                              aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div class="pull-right">
                                                <h3>50%</h3>
                                                <p>Win rate</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="button-box-preview">
                                <p>
                                    เซบีย่ายังมีลุ้นพื้นที่ยุโรปนัดนี้ต้องเน้นเต็มที่แม้ฟอร์มจะ
                                    แผ่วลงไปแต่โซเซียดาดเกมเยือนยังเล่นไม่แน่นอนนัด
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="thumbnail box-preview">
                            <div class="top-box-preview">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="box-guru-img pull-left">
                                                <img src="/images/sample-logo01.png">
                                            </div>
                                            <div class="pull-left">
                                                <h5>Thscore</h5>
                                                <div class="box-player-preview">
                                                    <img src="/images/logo-team/Chelsea.png">
                                                    <span>@ Chelsea</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-link"><i class="fa fa-angle-right"
                                                                                              aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div class="pull-right">
                                                <h3>50%</h3>
                                                <p>Win rate</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="button-box-preview">
                                <p>
                                    เซบีย่ายังมีลุ้นพื้นที่ยุโรปนัดนี้ต้องเน้นเต็มที่แม้ฟอร์มจะ
                                    แผ่วลงไปแต่โซเซียดาดเกมเยือนยังเล่นไม่แน่นอนนัด
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="thumbnail box-preview">
                            <div class="top-box-preview">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="box-user-img pull-left">
                                                <img src="/images/p1.png">
                                            </div>
                                            <div class="pull-left">
                                                <h5>กระบี่มือหนึ่ง</h5>
                                                <div class="box-player-preview">
                                                    <img src="/images/logo-team/manchester.png">
                                                    <span>@ Manchester UTD</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-link"><i class="fa fa-angle-right"
                                                                                              aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div class="pull-right">
                                                <h3>50%</h3>
                                                <p>Win rate</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="button-box-preview">
                                <p>
                                    เซบีย่ายังมีลุ้นพื้นที่ยุโรปนัดนี้ต้องเน้นเต็มที่แม้ฟอร์มจะ
                                    แผ่วลงไปแต่โซเซียดาดเกมเยือนยังเล่นไม่แน่นอนนัด
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="thumbnail box-preview">
                            <div class="top-box-preview">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="box-guru-img pull-left">
                                                <img src="/images/sample-logo01.png">
                                            </div>
                                            <div class="pull-left">
                                                <h5>Thscore</h5>
                                                <div class="box-player-preview">
                                                    <img src="/images/logo-team/Chelsea.png">
                                                    <span>@ Chelsea</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-link"><i class="fa fa-angle-right"
                                                                                              aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div class="pull-right">
                                                <h3>50%</h3>
                                                <p>Win rate</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="button-box-preview">
                                <p>
                                    เซบีย่ายังมีลุ้นพื้นที่ยุโรปนัดนี้ต้องเน้นเต็มที่แม้ฟอร์มจะ
                                    แผ่วลงไปแต่โซเซียดาดเกมเยือนยังเล่นไม่แน่นอนนัด
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Indicators -->
            <ol class="carousel-indicators" style="bottom: 0; z-index: 0;">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>
        </div>
        <div class="box-add-review" data-toggle="modal" data-target="#addReview">
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </div>
    </div>
</div>

<!--สถิคิ-->
<div class="topic-content-state" data-spy="affix" data-offset-top="600" data-offset-bottom="200">
    <ul>
        <li>
            <div class="pull-right">
                <h2>Chelsea</h2>
                <img src="/images/logo-team/Chelsea.png">
            </div>
        </li>
        <li>
            <div class="text-center">
                <h3>ENGLAND FA CUP</h3>
                <p>
                    20:00 | 21/07/2018
                </p>
            </div>
        </li>
        <li>
            <div class="pull-left">
                <img src="/images/logo-team/manchester.png">
                <h2>Manchester UTD</h2>
            </div>
        </li>
    </ul>
    <div class="arrow-down"></div>
</div>
<!--HEAD TO HEAD-->

<div class="content-state content-h2h col-lg-12">
    <div class="head-state">
        <h2>HEAD TO HEAD</h2>
    </div>
    <div class="col-sm-6">
        <div class="pull-right padding-top-80">
            <!--            กราฟวงกลม-->
            <div class="section-state-box text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Chatdata Odds/ Result</h4>
                            </td>
                            <td class="text-right">

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="col-md-6">
                        <table>
                            <tr>
                                <td>Odds</td>
                                <td>
                                    <img src="/images/chart-simple.png">
                                </td>
                                <td class="data-chart">
                                    <div class="clearfix">
                                        <h4>41.4</h4>
                                        <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                                    </div>
                                    <div class="clearfix">
                                        <h4>58.6</h4>
                                        <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                                    </div>
                                    <div class="clearfix">
                                        <h4>0</h4>
                                        <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table>
                            <tr>
                                <td>Result</td>
                                <td>
                                    <img src="/images/chart-simple.png">
                                </td>
                                <td class="data-chart">
                                    <div class="clearfix">
                                        <h4>41.4</h4>
                                        <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                                    </div>
                                    <div class="clearfix">
                                        <h4>58.6</h4>
                                        <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                                    </div>
                                    <div class="clearfix">
                                        <h4>0</h4>
                                        <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!--            ตาราง-->
            <div class="section-state-box text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><img src="/images/logo-team/Chelsea.png"> Chelsea</h4>
                            </td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-default dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Last Match(5) <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="box-table-result-state" style="padding: 0 15px;">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#h2hTotal" aria-controls="h2hTotal"
                                                                      role="tab" data-toggle="tab">Total</a></li>
                            <li role="presentation"><a href="#h2hHome" aria-controls="h2hHome" role="tab"
                                                       data-toggle="tab">Home</a></li>
                            <li role="presentation"><a href="#h2hAway" aria-controls="h2hAway" role="tab"
                                                       data-toggle="tab">Away</a></li>

                            <li class="pull-right">
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> All
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox2" value="option2"> ENG PR
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox3" value="option3"> ENG FAC
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox3" value="option3"> ENG LC
                                </label>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="h2hTotal">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center mini-col">HDP</th>
                                        <th class="text-center mini-col">Odds</th>
                                        <th class="text-center mini-col">Result</th>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Manchester UTD</span>
                                            <img src="/images/logo-team/manchester.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span class="name-team-state">Chelsea</span>
                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Manchester UTD</span>
                                            <img src="/images/logo-team/manchester.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span class="name-team-state">Chelsea</span>
                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Chelsea</span>
                                            <img src="/images/logo-team/Chelsea.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/manchester.png">
                                            <span class="name-team-state">Manchester UTD</span>

                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Manchester UTD</span>
                                            <img src="/images/logo-team/manchester.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span class="name-team-state">Chelsea</span>
                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Chelsea</span>
                                            <img src="/images/logo-team/Chelsea.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/manchester.png">
                                            <span class="name-team-state">Manchester UTD</span>

                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="h2hHome">...</div>
                            <div role="tabpanel" class="tab-pane" id="h2hAway">...</div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both"></div>
    </div>
    <div class="col-sm-6 margin-top" style="background-color: #f1f1f1;">
        <div class="pull-left padding-top-80">
            <!--            กราฟวงกลม-->
            <div class="section-state-box text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Chatdata Odds/ Result</h4>
                            </td>
                            <td class="text-right">

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="col-md-6">
                        <table>
                            <tr>
                                <td>Odds</td>
                                <td>
                                    <img src="/images/chart-simple.png">
                                </td>
                                <td class="data-chart">
                                    <div class="clearfix">
                                        <h4>41.4</h4>
                                        <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                                    </div>
                                    <div class="clearfix">
                                        <h4>58.6</h4>
                                        <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                                    </div>
                                    <div class="clearfix">
                                        <h4>0</h4>
                                        <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table>
                            <tr>
                                <td>Result</td>
                                <td>
                                    <img src="/images/chart-simple.png">
                                </td>
                                <td class="data-chart">
                                    <div class="clearfix">
                                        <h4>41.4</h4>
                                        <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                                    </div>
                                    <div class="clearfix">
                                        <h4>58.6</h4>
                                        <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                                    </div>
                                    <div class="clearfix">
                                        <h4>0</h4>
                                        <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!--            ตาราง-->
            <div class="section-state-box text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><img src="/images/logo-team/manchester.png"> Manchester UTD</h4>
                            </td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-default dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Last Match(5) <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="box-table-result-state" style="padding: 0 15px;">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#h2hTotal" aria-controls="h2hTotal"
                                                                      role="tab" data-toggle="tab">Total</a></li>
                            <li role="presentation"><a href="#h2hHome" aria-controls="h2hHome" role="tab"
                                                       data-toggle="tab">Home</a></li>
                            <li role="presentation"><a href="#h2hAway" aria-controls="h2hAway" role="tab"
                                                       data-toggle="tab">Away</a></li>

                            <li class="pull-right">
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> All
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox2" value="option2"> ENG PR
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox3" value="option3"> ENG FAC
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox3" value="option3"> ENG LC
                                </label>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="h2hTotal">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center mini-col">HDP</th>
                                        <th class="text-center mini-col">Odds</th>
                                        <th class="text-center mini-col">Result</th>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Manchester UTD</span>
                                            <img src="/images/logo-team/manchester.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span class="name-team-state">Chelsea</span>
                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Manchester UTD</span>
                                            <img src="/images/logo-team/manchester.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span class="name-team-state">Chelsea</span>
                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Chelsea</span>
                                            <img src="/images/logo-team/Chelsea.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/manchester.png">
                                            <span class="name-team-state">Manchester UTD</span>

                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Manchester UTD</span>
                                            <img src="/images/logo-team/manchester.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span class="name-team-state">Chelsea</span>
                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Chelsea</span>
                                            <img src="/images/logo-team/Chelsea.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/manchester.png">
                                            <span class="name-team-state">Manchester UTD</span>

                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="h2hHome">...</div>
                            <div role="tabpanel" class="tab-pane" id="h2hAway">...</div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both"></div>
    </div>
</div>

<!--Form Guide-->
<div class="content-state content-formGuide col-lg-12">
    <div class="head-state">
        <h2>FORM GUIDE</h2>
    </div>
    <div class="col-sm-6">
        <div class="pull-right padding-top-80">
            <!--            กราฟวงกลม-->
            <div class="section-state-box text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Chatdata Odds/ Result</h4>
                            </td>
                            <td class="text-right">

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="col-md-6">
                        <table>
                            <tr>
                                <td>Odds</td>
                                <td>
                                    <img src="/images/chart-simple.png">
                                </td>
                                <td class="data-chart">
                                    <div class="clearfix">
                                        <h4>41.4</h4>
                                        <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                                    </div>
                                    <div class="clearfix">
                                        <h4>58.6</h4>
                                        <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                                    </div>
                                    <div class="clearfix">
                                        <h4>0</h4>
                                        <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table>
                            <tr>
                                <td>Result</td>
                                <td>
                                    <img src="/images/chart-simple.png">
                                </td>
                                <td class="data-chart">
                                    <div class="clearfix">
                                        <h4>41.4</h4>
                                        <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                                    </div>
                                    <div class="clearfix">
                                        <h4>58.6</h4>
                                        <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                                    </div>
                                    <div class="clearfix">
                                        <h4>0</h4>
                                        <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!--            ตาราง-->
            <div class="section-state-box text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><img src="/images/logo-team/Chelsea.png"> Chelsea</h4>
                            </td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-default dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Last Match(5) <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="box-table-result-state" style="padding: 0 15px;">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#h2hTotal" aria-controls="h2hTotal"
                                                                      role="tab" data-toggle="tab">Total</a></li>
                            <li role="presentation"><a href="#h2hHome" aria-controls="h2hHome" role="tab"
                                                       data-toggle="tab">Home</a></li>
                            <li role="presentation"><a href="#h2hAway" aria-controls="h2hAway" role="tab"
                                                       data-toggle="tab">Away</a></li>

                            <li class="pull-right">
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> All
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox2" value="option2"> ENG PR
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox3" value="option3"> ENG FAC
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox3" value="option3"> ENG LC
                                </label>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="h2hTotal">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center mini-col">HDP</th>
                                        <th class="text-center mini-col">Odds</th>
                                        <th class="text-center mini-col">Result</th>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Manchester UTD</span>
                                            <img src="/images/logo-team/manchester.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span class="name-team-state">Chelsea</span>
                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Manchester UTD</span>
                                            <img src="/images/logo-team/manchester.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span class="name-team-state">Chelsea</span>
                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Chelsea</span>
                                            <img src="/images/logo-team/Chelsea.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/manchester.png">
                                            <span class="name-team-state">Manchester UTD</span>

                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Manchester UTD</span>
                                            <img src="/images/logo-team/manchester.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span class="name-team-state">Chelsea</span>
                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Chelsea</span>
                                            <img src="/images/logo-team/Chelsea.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/manchester.png">
                                            <span class="name-team-state">Manchester UTD</span>

                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="h2hHome">...</div>
                            <div role="tabpanel" class="tab-pane" id="h2hAway">...</div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both"></div>
    </div>
    <div class="col-sm-6 margin-top" style="background-color: #f1f1f1;">
        <div class="pull-left padding-top-80">
            <!--            กราฟวงกลม-->
            <div class="section-state-box text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Chatdata Odds/ Result</h4>
                            </td>
                            <td class="text-right">

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="col-md-6">
                        <table>
                            <tr>
                                <td>Odds</td>
                                <td>
                                    <img src="/images/chart-simple.png">
                                </td>
                                <td class="data-chart">
                                    <div class="clearfix">
                                        <h4>41.4</h4>
                                        <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                                    </div>
                                    <div class="clearfix">
                                        <h4>58.6</h4>
                                        <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                                    </div>
                                    <div class="clearfix">
                                        <h4>0</h4>
                                        <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table>
                            <tr>
                                <td>Result</td>
                                <td>
                                    <img src="/images/chart-simple.png">
                                </td>
                                <td class="data-chart">
                                    <div class="clearfix">
                                        <h4>41.4</h4>
                                        <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                                    </div>
                                    <div class="clearfix">
                                        <h4>58.6</h4>
                                        <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                                    </div>
                                    <div class="clearfix">
                                        <h4>0</h4>
                                        <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!--            ตาราง-->
            <div class="section-state-box text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><img src="/images/logo-team/manchester.png"> Manchester UTD</h4>
                            </td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-default dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Last Match(5) <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="box-table-result-state" style="padding: 0 15px;">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#h2hTotal" aria-controls="h2hTotal"
                                                                      role="tab" data-toggle="tab">Total</a></li>
                            <li role="presentation"><a href="#h2hHome" aria-controls="h2hHome" role="tab"
                                                       data-toggle="tab">Home</a></li>
                            <li role="presentation"><a href="#h2hAway" aria-controls="h2hAway" role="tab"
                                                       data-toggle="tab">Away</a></li>

                            <li class="pull-right">
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> All
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox2" value="option2"> ENG PR
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox3" value="option3"> ENG FAC
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox3" value="option3"> ENG LC
                                </label>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="h2hTotal">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center mini-col">HDP</th>
                                        <th class="text-center mini-col">Odds</th>
                                        <th class="text-center mini-col">Result</th>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Manchester UTD</span>
                                            <img src="/images/logo-team/manchester.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span class="name-team-state">Chelsea</span>
                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Manchester UTD</span>
                                            <img src="/images/logo-team/manchester.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span class="name-team-state">Chelsea</span>
                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Chelsea</span>
                                            <img src="/images/logo-team/Chelsea.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/manchester.png">
                                            <span class="name-team-state">Manchester UTD</span>

                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Manchester UTD</span>
                                            <img src="/images/logo-team/manchester.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span class="name-team-state">Chelsea</span>
                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018 | <i
                                                        class="fa fa-clock-o" aria-hidden="true"></i> 02:00</p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state">Chelsea</span>
                                            <img src="/images/logo-team/Chelsea.png">
                                        </td>
                                        <td>
                                            <div class="score-state">0-1</div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="/images/logo-team/manchester.png">
                                            <span class="name-team-state">Manchester UTD</span>

                                        </td>
                                        <td class="text-center">1.75</td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                        <td class="text-center"><img src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="h2hHome">...</div>
                            <div role="tabpanel" class="tab-pane" id="h2hAway">...</div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both"></div>
    </div>
</div>

<!--Next Match-->
<div class="content-state content-formGuide col-lg-12">
    <div class="head-state">
        <h2>Next Match</h2>
    </div>
    <div class="col-sm-6">
        <div class="pull-right padding-top-80">
            <!--            ตาราง-->
            <div class="section-state-box text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><img src="/images/logo-team/Chelsea.png"> Chelsea</h4>
                            </td>
                            <td class="text-right">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="box-table-result-state" style="padding: 0 15px;">
                        <table class="table table-striped">
                            <tr>
                                <td>
                                    <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                </td>
                                <td>
                                    <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018</p>
                                </td>
                                <td class="text-right">
                                    <span>Chelsea <img src="/images/logo-team/Chelsea.png"></span>
                                </td>
                                <td>
                                    <div class="score-state">
                                        02:00
                                    </div>
                                </td>
                                <td class="text-left">
                                    <span><img src="/images/logo-team/manchester.png"> Manchester UTD</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                </td>
                                <td>
                                    <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018</p>
                                </td>
                                <td class="text-right">
                                    <span>Manchester UTD<img src="/images/logo-team/manchester.png"> </span>

                                </td>
                                <td>
                                    <div class="score-state">
                                        02:00
                                    </div>
                                </td>
                                <td class="text-left">
                                    <span><img src="/images/logo-team/Chelsea.png">Chelsea </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                </td>
                                <td>
                                    <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018</p>
                                </td>
                                <td class="text-right">
                                    <span>Chelsea <img src="/images/logo-team/Chelsea.png"></span>
                                </td>
                                <td>
                                    <div class="score-state">
                                        02:00
                                    </div>
                                </td>
                                <td class="text-left">
                                    <span><img src="/images/logo-team/manchester.png"> Manchester UTD</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                </td>
                                <td>
                                    <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018</p>
                                </td>
                                <td class="text-right">
                                    <span>Manchester UTD<img src="/images/logo-team/manchester.png"> </span>

                                </td>
                                <td>
                                    <div class="score-state">
                                        02:00
                                    </div>
                                </td>
                                <td class="text-left">
                                    <span><img src="/images/logo-team/Chelsea.png">Chelsea </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                </td>
                                <td>
                                    <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018</p>
                                </td>
                                <td class="text-right">
                                    <span>Manchester UTD<img src="/images/logo-team/manchester.png"> </span>

                                </td>
                                <td>
                                    <div class="score-state">
                                        02:00
                                    </div>
                                </td>
                                <td class="text-left">
                                    <span><img src="/images/logo-team/Chelsea.png">Chelsea </span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both"></div>
    </div>
    <div class="col-sm-6 margin-top" style="background-color: #f1f1f1;">
        <div class="pull-left padding-top-80">
            <!--            ตาราง-->
            <div class="section-state-box text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><img src="/images/logo-team/manchester.png"> Manchester UTD</h4>
                            </td>
                            <td class="text-right">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="box-table-result-state" style="padding: 0 15px;">
                        <table class="table table-striped">
                            <tr>
                                <td>
                                    <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                </td>
                                <td>
                                    <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018</p>
                                </td>
                                <td class="text-right">
                                    <span>Chelsea <img src="/images/logo-team/Chelsea.png"></span>
                                </td>
                                <td>
                                    <div class="score-state">
                                        02:00
                                    </div>
                                </td>
                                <td class="text-left">
                                    <span><img src="/images/logo-team/manchester.png"> Manchester UTD</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                </td>
                                <td>
                                    <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018</p>
                                </td>
                                <td class="text-right">
                                    <span>Manchester UTD<img src="/images/logo-team/manchester.png"> </span>

                                </td>
                                <td>
                                    <div class="score-state">
                                        02:00
                                    </div>
                                </td>
                                <td class="text-left">
                                    <span><img src="/images/logo-team/Chelsea.png">Chelsea </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                </td>
                                <td>
                                    <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018</p>
                                </td>
                                <td class="text-right">
                                    <span>Chelsea <img src="/images/logo-team/Chelsea.png"></span>
                                </td>
                                <td>
                                    <div class="score-state">
                                        02:00
                                    </div>
                                </td>
                                <td class="text-left">
                                    <span><img src="/images/logo-team/manchester.png"> Manchester UTD</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                </td>
                                <td>
                                    <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018</p>
                                </td>
                                <td class="text-right">
                                    <span>Manchester UTD<img src="/images/logo-team/manchester.png"> </span>

                                </td>
                                <td>
                                    <div class="score-state">
                                        02:00
                                    </div>
                                </td>
                                <td class="text-left">
                                    <span><img src="/images/logo-team/Chelsea.png">Chelsea </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span><img src="/images/five-leagues/premier-league-logo.png"> Premier League</span>
                                </td>
                                <td>
                                    <p><i class="fa fa-calendar" aria-hidden="true"></i> 12/12/2018</p>
                                </td>
                                <td class="text-right">
                                    <span>Manchester UTD<img src="/images/logo-team/manchester.png"> </span>

                                </td>
                                <td>
                                    <div class="score-state">
                                        02:00
                                    </div>
                                </td>
                                <td class="text-left">
                                    <span><img src="/images/logo-team/Chelsea.png">Chelsea </span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both"></div>
    </div>
</div>

<!--Table-->
<div class="content-state content-formGuide col-lg-12">
    <div class="head-state">
        <h2>Table</h2>
    </div>
    <div class="col-sm-6">
        <div class="pull-right padding-top-80">
            <!--            ตาราง-->
            <div class="section-state-box text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><img src="/images/five-leagues/premier-league-logo.png"> Premier League</h4>
                            </td>
                            <td class="text-right">
                                <button type="button" class="btn btn-link"><strong>Show All</strong> <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span></button>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="box-table-result-state" style="padding: 0 15px;">
                        <table class="table table-state">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Team</th>
                                <th>P</th>
                                <th>W</th>
                                <th>D</th>
                                <th>L</th>
                                <th>GF</th>
                                <th>GA</th>
                                <th>GD</th>
                                <th>PTS</th>
                            </tr>
                            </thead>
                            <tr>
                                <td>2</td>
                                <td><img src="/images/logo-team/manchester.png">Manchester UTD</td>
                                <td>36</td>
                                <td>24</td>
                                <td>5</td>
                                <td>7</td>
                                <td>67</td>
                                <td>28</td>
                                <td>77</td>
                                <td>77</td>
                            </tr>
                            <tr  class="active">
                                <td>5</td>
                                <td><img src="/images/logo-team/Chelsea.png">Chelsea</td>
                                <td>37</td>
                                <td>21</td>
                                <td>7</td>
                                <td>9</td>
                                <td>30</td>
                                <td>32</td>
                                <td>70</td>
                                <td>70</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both"></div>
    </div>
    <div class="col-sm-6 margin-top" style="background-color: #f1f1f1;">
        <div class="pull-left padding-top-80">
            <!--            ตาราง-->
            <div class="section-state-box text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><img src="/images/five-leagues/premier-league-logo.png"> Premier League</h4>
                            </td>
                            <td class="text-right">
                                <button type="button" class="btn btn-link"><strong>Show All</strong> <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span></button>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="box-table-result-state" style="padding: 0 15px;">
                        <table class="table table-state">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Team</th>
                                <th>P</th>
                                <th>W</th>
                                <th>D</th>
                                <th>L</th>
                                <th>GF</th>
                                <th>GA</th>
                                <th>GD</th>
                                <th>PTS</th>
                            </tr>
                            </thead>
                            <tr class="active">
                                <td>2</td>
                                <td><img src="/images/logo-team/manchester.png">Manchester UTD</td>
                                <td>36</td>
                                <td>24</td>
                                <td>5</td>
                                <td>7</td>
                                <td>67</td>
                                <td>28</td>
                                <td>77</td>
                                <td>77</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td><img src="/images/logo-team/Chelsea.png">Chelsea</td>
                                <td>37</td>
                                <td>21</td>
                                <td>7</td>
                                <td>9</td>
                                <td>30</td>
                                <td>32</td>
                                <td>70</td>
                                <td>70</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both"></div>
    </div>
</div>

<div style="clear: both;"></div>

<!-- Modal Add -->
<div class="modal fade" id="addReview" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="width: 470px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มทรรศนะ</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ชือ</label>
                        <div class="col-sm-10">
                            <div class="clearfix">
                                <div class="box-guru-img pull-left">
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <h4>Thscore</h4>
                            </div>
                            <div class="clearfix">
                                <div class="box-user-img pull-left">
                                    <img src="/images/p1.png">
                                </div>
                                <h4>กระบี่มือหนึ่ง</h4>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ทีมที่เลือก</label>
                        <div class="col-sm-10">
                            <div class="box-player-preview">
                                <img src="/images/logo-team/manchester.png">
                                <span>@ Manchester UTD</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ทรรศนะ</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                <button type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่ม
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Preview Guru-->
<div class="modal fade" id="showReviewGuru" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">ทรรศนะทั้งหมด</h4>
            </div>
            <div class="modal-body">
                <div class="box-preview">
                    <div class="top-box-preview">
                        <table>
                            <tr>
                                <td>
                                    <div class="box-guru-img pull-left">
                                        <img src="/images/sample-logo01.png">
                                    </div>
                                    <div class="pull-left">
                                        <h5>Thscore</h5>
                                        <div class="box-player-preview">
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span>@ Chelsea</span>
                                        </div>
                                    </div>
                                    <div class="pull-left">
                                        <ul class="box-betlist">
                                            <li><img src="/images/icon-stat/betlist_result/lose.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/lose_half.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                        </ul>
                                    </div>
                                </td>
                                <td>
                                    <div class="pull-right">
                                        <h3>50%</h3>
                                        <p>Win rate</p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="button-box-preview">

                        <div class="well well-sm">
                            ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว
                            ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว
                        </div>
                    </div>
                </div>
                <div class="box-preview">
                    <div class="top-box-preview">
                        <table>
                            <tr>
                                <td>
                                    <div class="box-user-img pull-left">
                                        <img src="/images/p1.png">
                                    </div>
                                    <div class="pull-left">
                                        <h5>กระบี่มือหนึ่ง</h5>
                                        <div class="box-player-preview">
                                            <img src="/images/logo-team/manchester.png">
                                            <span>@ Manchester UTD</span>
                                        </div>
                                    </div>
                                    <div class="pull-left">
                                        <ul class="box-betlist">
                                            <li><img src="/images/icon-stat/betlist_result/lose.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/lose_half.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                        </ul>
                                    </div>
                                </td>
                                <td>
                                    <div class="pull-right">
                                        <h3>50%</h3>
                                        <p>Win rate</p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="button-box-preview">
                        <div class="well well-sm">
                            ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว
                            ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว
                        </div>
                    </div>
                </div>
                <div class="box-preview">
                    <div class="top-box-preview">
                        <table>
                            <tr>
                                <td>
                                    <div class="box-guru-img pull-left">
                                        <img src="/images/sample-logo01.png">
                                    </div>
                                    <div class="pull-left">
                                        <h5>Thscore</h5>
                                        <div class="box-player-preview">
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span>@ Chelsea</span>
                                        </div>
                                    </div>
                                    <div class="pull-left">
                                        <ul class="box-betlist">
                                            <li><img src="/images/icon-stat/betlist_result/lose.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/lose_half.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                        </ul>
                                    </div>
                                </td>
                                <td>
                                    <div class="pull-right">
                                        <h3>50%</h3>
                                        <p>Win rate</p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="button-box-preview">

                        <div class="well well-sm">
                            ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว
                            ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว
                        </div>
                    </div>
                </div>
                <div class="box-preview">
                    <div class="top-box-preview">
                        <table>
                            <tr>
                                <td>
                                    <div class="box-user-img pull-left">
                                        <img src="/images/p1.png">
                                    </div>
                                    <div class="pull-left">
                                        <h5>กระบี่มือหนึ่ง</h5>
                                        <div class="box-player-preview">
                                            <img src="/images/logo-team/manchester.png">
                                            <span>@ Manchester UTD</span>
                                        </div>
                                    </div>
                                    <div class="pull-left">
                                        <ul class="box-betlist">
                                            <li><img src="/images/icon-stat/betlist_result/lose.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/lose_half.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                        </ul>
                                    </div>
                                </td>
                                <td>
                                    <div class="pull-right">
                                        <h3>50%</h3>
                                        <p>Win rate</p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="button-box-preview">
                        <div class="well well-sm">
                            ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว
                            ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว
                        </div>
                    </div>
                </div>
                <div class="box-preview">
                    <div class="top-box-preview">
                        <table>
                            <tr>
                                <td>
                                    <div class="box-guru-img pull-left">
                                        <img src="/images/sample-logo01.png">
                                    </div>
                                    <div class="pull-left">
                                        <h5>Thscore</h5>
                                        <div class="box-player-preview">
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span>@ Chelsea</span>
                                        </div>
                                    </div>
                                    <div class="pull-left">
                                        <ul class="box-betlist">
                                            <li><img src="/images/icon-stat/betlist_result/lose.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/lose_half.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                        </ul>
                                    </div>
                                </td>
                                <td>
                                    <div class="pull-right">
                                        <h3>50%</h3>
                                        <p>Win rate</p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="button-box-preview">

                        <div class="well well-sm">
                            ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว
                            ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว
                        </div>
                    </div>
                </div>
                <div class="box-preview">
                    <div class="top-box-preview">
                        <table>
                            <tr>
                                <td>
                                    <div class="box-user-img pull-left">
                                        <img src="/images/p1.png">
                                    </div>
                                    <div class="pull-left">
                                        <h5>กระบี่มือหนึ่ง</h5>
                                        <div class="box-player-preview">
                                            <img src="/images/logo-team/manchester.png">
                                            <span>@ Manchester UTD</span>
                                        </div>
                                    </div>
                                    <div class="pull-left">
                                        <ul class="box-betlist">
                                            <li><img src="/images/icon-stat/betlist_result/lose.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/lose_half.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                            <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                        </ul>
                                    </div>
                                </td>
                                <td>
                                    <div class="pull-right">
                                        <h3>50%</h3>
                                        <p>Win rate</p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="button-box-preview">
                        <div class="well well-sm">
                            ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว
                            ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php //include 'footer.php';
include 'view/index/footer.php';
?>
</body>
</html>



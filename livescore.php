<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta http-equiv="Cache-control" content="public,max-age=604800">
    <title>ผลบอลสด
        Livescore <?php echo(!empty($x['fragment']) ? "ลีก" . str_replace("league-mark-", "", $x['fragment']) : "") ?> <?php echo(isset($_GET['day']) ? "ประจำวันที่" . $_GET['day'] : '') ?>
        ผลบอล บ้านผลบอล ngoal</title>
    <meta name="keywords"
          content="ผลบอลสด, บอลสด, Livescore,บ้านผลบอล,ผลบอลย้อนหลัง,โปรแกรมบอลล่วงหน้า,ตารางการแข่งขัน,ผลบอลวันนี้,ราคาบอลไหล,อัตราต่อรอง">
    <meta name="description"
          content="ดูผลบอลสด livescore <?php echo(!empty($x['fragment']) ? "ลีก" . str_replace("league-mark-", "", $x['fragment']) : "") ?> ประจำวันที่ <?php echo(isset($_GET['day']) ? "ประจำวันที่" . $_GET['day'] : '') ?> ผลบอลย้อนหลัง ผลบอลเมื่อคืน  โปรแกรมบอลล่วงหน้า อัตราต่อรอง ราคาบอลไหลที่รวดเร็ว ทุกคู่ทุกลีก รวมทั้งรายงานใบเหลืองใบแดง การทำประตู รายละเอียดครบครัน">

    <link rel="shortcut icon" type="images/x-icon" href="/images/favicon.ico.png">
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <!--    <link rel="stylesheet" href="css/style.css">-->
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/lineups-decktop.css">
    <link rel="stylesheet" href="/css/livescore.css">
    <link rel="stylesheet" href="/css/program_ball.css">
    <link rel="stylesheet" href="/css/game-redesign.css">
    <link rel="stylesheet" href="/css/circle-progress-bar.css">
    <link rel="stylesheet" href="/css/football-symbol.css">
    <!-- <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.carousel.css">
    <!-- <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css"> -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.theme.default.min.css">
    <!--    <link rel="stylesheet" href="/nanoscrollbar/nanoscrollbar.css">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.nanoscroller/0.8.7/css/nanoscroller.css">

    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <!-- <script src="/js/jquery/dist/jquery.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nanoscroller/0.8.7/javascripts/jquery.nanoscroller.js"></script>
    <script src="/js/readmore.js"></script>
    <!-- <script src="/css/bootstrap/js/bootstrap.min.js"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <!-- <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/owl.carousel.min.js"></script>
    <!-- <script src="/js/jquery-timeago/jquery.timeago.js"></script> -->
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="/js/highlight_live.js"></script>
    <!-- <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script> -->
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118679525-1"></script> -->
</head>
<?php
include_once __DIR__ . "/bootstrap.php";
use Carbon\Carbon;
use controller\HighlightController;
use controller\MatchController;

$hcon = new HighlightController();
$mcon = new MatchController();
$today = Carbon::now("UTC");
$today->setTimezone("Asia/Bangkok");
$nowday = Carbon::now("Asia/Bangkok");
$yesterday = Carbon::yesterday("Asia/Bangkok");
$tomorrow = Carbon::tomorrow("Asia/Bangkok");
$selectday = isset($_REQUEST['day']) ? $_REQUEST['day'] : $today->toDateString();

$matchlist = $mcon->getAllLSort($selectday);

$current = strtotime(date("Y-m-d"));
$date = strtotime($selectday);
$datediff = $date - $current;
$difference = floor($datediff / (60 * 60 * 24));
//var_dump($difference);
?>
<body style="background-color: #e5e5ea;">
<?php include $_SERVER['DOCUMENT_ROOT'] . '/view/index/menu-all.php'; ?>
<style>
    body {
        position: relative;
        background-color: #dce0e4;
    }

    .affix {
        position: relative;
        top: 0;
        width: 100%;
        z-index: 9999 !important;
    }

    .navbar {
        margin-bottom: 0;
    }


</style>



<div class="side-select" data-spy="affix" data-offset-top="60">
    <div class="nano">
        <div class="nano-content">
            <h4 class="title-center" style="margin-top: 0;">
                TOP LEAGUES
            </h4>
            <div class="navbar nav-pills nav-stacked">
                <ul class="nav navbar-nav">
                    <?php foreach ($matchlist['order'] as $lid => $priority) { ?>
                        <?php if ($priority <= 10) { ?>
                            <?php $data = $matchlist['data'][$lid]; ?>
                            <li>
                                <a href="#league-mark-<?php echo preg_replace('/\s+/', '', $data['league']->name); ?>">
                                    <img src="<?php echo $data['league']->getLogo(); ?>">
                                    <h5><?php echo $data['league']->name; ?></h5>
                                    <span class="num-side"><?php echo count($data['matches']); ?> คู่</span>
                                </a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
            <div style="clear: both"></div>
            <h4 class="title-center">
                LEAGUES
            </h4>
            <div id="nav" class="navbar nav-pills nav-stacked">
                <ul class="nav navbar-nav">
                    <?php foreach ($matchlist['order'] as $lid => $priority) { ?>
                        <?php if ($priority > 10) { ?>
                            <?php $data = $matchlist['data'][$lid]; ?>
                            <li>
                                <a href="#league-mark-<?php echo preg_replace('/\s+/', '', $data['league']->name); ?>">
                                    <img src="<?php echo $data['league']->getLogo(); ?>">
                                    <h5><?php echo $data['league']->name; ?></h5>
                                    <span class="num-side"><?php echo count($data['matches']); ?> คู่</span>
                                </a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>

        </div>
    </div>
</div>

<div class="content-all-programe pull-left main main2" id="main">

    <div class="content-all-programe-top inline-box full-width">
        <!--        <div class="hamburger-live-match" id="hamburger" onclick="toggleSidenav();"></div>-->
        <!--        <h4 style="font-size: 12px!important;">บอลวันนี้ โปรแกรมบอล ราคาบอล โปรแกรมฟุตบอล</h4>-->
        <div class="pull-left full-width inline-box">
            <span id="hl-datemark" class="hide"></span>
            <div class="date-review btn-group" role="group" style="margin-right: 20px;">
                <a class="btn btn-sm btn-default btn-review <?php echo ($selectday == $yesterday->toDateString()) ? "active" : ""; ?>"
                   href="/livescore?day=<?php echo $yesterday->toDateString(); ?>">Yesterday</a>
                <a class="btn btn-sm btn-default btn-review <?php echo ($selectday == $nowday->toDateString()) ? "active" : ""; ?>"
                   href="/livescore">Today</a>
                <a class="btn btn-sm btn-default btn-review <?php echo ($selectday == $tomorrow->toDateString()) ? "active" : ""; ?>"
                   href="/livescore?day=<?php echo $tomorrow->toDateString(); ?>">Tomorrow</a>
            </div>
            <div class="dropdown pull-right">
                <button class="btn btn-default dropdown-toggle btn-sm" type="button" id="dropdownMenu1"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <a id="filter-mark">ALL</a>
                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li class="filter-option" option="all"><a href="#">All Match</a></li>
                    <li class="filter-option"><a href="#">Game Match</a></li>
                    <li class="filter-option" option="live"><a href="#">Live Match</a></li>
                    <li class="filter-option" option="end"><a href="#">End Match</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="content-all-programe-all inline-box full-width">
        <div class="nano" data-spy="scroll" data-target=".navbar" data-offset="40">
            <div class="nano-content">
                <!--    Live Marth-->
                <div id="live-box">

                    <?php foreach ($matchlist['order'] as $lid => $priority) { ?>
                        <?php $data = $matchlist['data'][$lid]; ?>
                        <div class="content-program-soccer league-box container-live_all <?php echo (array_key_exists($lid, $matchlist['live_league'])) ? "" : "hide"; ?>"
                             lid="<?php echo $lid; ?>" id="liveleague<?php echo $lid; ?>">
                            <div class="topic topic-tab">
                                <div class="logo-league-program">
                                    <img src="<?php echo $data['league']->getLogo(); ?>">
                                </div>
                                <div class="title-league-program">
                                    <h3><?php echo $data['league']->name; ?></h3>
                                </div>
                                <div class="dropdown pull-right">
                                </div>

                            </div>
                            <div class="content">
                                <div class="panel-group panel-group-row matches-row league-matches-live"
                                     lid="<?php echo $lid; ?>"
                                     id="accordion<?php echo $lid; ?>">
                                    <?php foreach ($data['matches'] as $match) { ?>
                                        <?php if ($match->state == 1 || $match->state == 2 || $match->state == 3) { ?>
                                            <div class="panel panel-default livescore-panel color-tape livescore-live"
                                                 lid="<?php echo $lid; ?>" mid="<?php echo $match->id; ?>">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse"
                                                       class="review-dropdown full-width inline-box review-dropdown-left collapsed"
                                                       data-parent="#accordion<?php echo $lid; ?>"
                                                       href="#<?php echo "collapse_endmatch" . $match->id; ?>"
                                                       mid="<?php echo $match->id; ?>">
                                                        <div mid="<?php echo $match->id; ?>"
                                                             class="hdp-float hide wait-float">
                                                            <ul>
                                                                <li class="live-h-hdp"
                                                                    mid="<?php echo $match->id; ?>"><?php echo (empty($match->hdp)) ? "-" : number_format($match->hdp->home_water_bill, 2); ?></li>
                                                                <li><h3 class="live-hdp"
                                                                        mid="<?php echo $match->id; ?>"><?php echo (empty($match->hdp)) ? "-" : $match->hdp->handicap; ?></h3>
                                                                </li>
                                                                <li class="live-a-hdp"
                                                                    mid="<?php echo $match->id; ?>"><?php echo (empty($match->hdp)) ? "-" : number_format($match->hdp->away_water_bill, 2); ?></li>
                                                            </ul>
                                                        </div>
                                                        <div mid="<?php echo $match->id; ?>"
                                                             class="hdp-float hide FT-float">
                                                            <ul>
                                                                <li class="live-h-score"
                                                                    mid="<?php echo $match->id; ?>"><?php echo $match->live_score_home; ?></li>
                                                                <li><h3>FT</h3></li>
                                                                <li class="live-a-score"
                                                                    mid="<?php echo $match->id; ?>"><?php echo $match->live_score_away; ?></li>
                                                            </ul>
                                                        </div>
                                                        <div mid="<?php echo $match->id; ?>"
                                                             class="hdp-float live-float">
                                                            <ul>
                                                                <li class="live-h-score"
                                                                    mid="<?php echo $match->id; ?>"><?php echo $match->live_score_home; ?></li>
                                                                <li>
                                                                    <div class="minute-progress"
                                                                         mid="<?php echo $match->id; ?>"
                                                                         data-percentage="0">
                                                                        <!--				                            <span class="progress-left">-->
                                                                        <!--                                                <span class="progress-bar"></span>-->
                                                                        <!--				                            </span>-->
                                                                        <!--                                                                        <span class="progress-right">-->
                                                                        <!--                                                <span class="progress-bar"></span>-->
                                                                        <!--                                            </span>-->
                                                                        <div class="progress-value">
                                                                            <div class="live-minute"
                                                                                 mid="<?php echo $match->id; ?>">-
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="live-a-score"
                                                                    mid="<?php echo $match->id; ?>"><?php echo $match->live_score_away; ?></li>
                                                            </ul>
                                                        </div>
                                                        <div class="left-accordion-row pull-left">
                                                            <div class="pull-right logo-team-ponball">
                                                                <img src="<?php echo $match->teama->getLogo(); ?>">
                                                            </div>
                                                            <div class="pull-right detail-team-ponball">
                                                                <div class="top-detail-team-ponball text-right">
                                                                    <h4><?php echo $match->teama->name_en; ?></h4>
                                                                </div>
                                                                <div class="bottom-detail-team-ponball hide">
                                                                    <div class="score-ct beton-homep"
                                                                         mid="<?php echo $match->id; ?>">
                                                                        0%
                                                                    </div>
                                                                    <div class="progress full-width">
                                                                        <div class="progress-bar pull-right beton-home"
                                                                             mid="<?php echo $match->id; ?>"
                                                                             role="progressbar"
                                                                             aria-valuenow="0"
                                                                             aria-valuemin="0" aria-valuemax="100"
                                                                             style="width: 0%;">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="pull-right hide">
                                                                    <ul class="status-lastMatch full-width">
                                                                        <li><p>Last Match</p></li>
                                                                        <?php foreach (array_reverse(explode(",", $match->lastresult_home)) as $rs) { ?>
                                                                            <?php
                                                                            $rsclass = "draw-status";
                                                                            switch ($rs) {
                                                                                case "w":
                                                                                    $rsclass = "win-status";
                                                                                    break;
                                                                                case "l":
                                                                                    $rsclass = "lose-status";
                                                                                    break;
                                                                                default:
                                                                                    $rsclass = "draw-status";
                                                                                    break;
                                                                            }

                                                                            ?>
                                                                            <li>
                                                                                <div class="<?php echo $rsclass; ?>"></div>
                                                                            </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                    <ul class="status-lastMatch full-width odd">
                                                                        <li><p>ODD</p></li>
                                                                        <?php foreach (array_reverse(explode(",", $match->lastresult_odds_home)) as $rs) { ?>
                                                                            <?php
                                                                            $rsclass = "draw-status";
                                                                            switch ($rs) {
                                                                                case "w":
                                                                                    $rsclass = "win-status";
                                                                                    break;
                                                                                case "l":
                                                                                    $rsclass = "lose-status";
                                                                                    break;
                                                                                default:
                                                                                    $rsclass = "draw-status";
                                                                                    break;
                                                                            }

                                                                            ?>
                                                                            <li>
                                                                                <div class="<?php echo $rsclass; ?>"></div>
                                                                            </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                </div>
                                                            </div>


                                                            <div class="pull-left content-col-left">
                                                                <div class="at-minute ct-ft">
                                                                    <?php
                                                                    $stime = Carbon::createFromFormat("Y-m-d H:i:s", $match->time_match, "Asia/Singapore");
                                                                    $stime->tz = "Asia/Bangkok";
                                                                    ?>
                                                                    <span class="at-minute-mark label-live"
                                                                          at="<?php echo $match->time_match; ?>"
                                                                          mid="<?php echo $match->id; ?>"
                                                                          lid="<?php echo $lid; ?>"
                                                                          state="<?php echo $match->state; ?>"
                                                                          ht="<?php echo $match->ht_start; ?>">LIVE</span>
                                                                </div>
                                                                <div class="ct-live-chanel hide">
                                                                    <img src="/images/channel/pptv.png">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="right-accordion-row pull-right">
                                                            <div class="pull-left logo-team-ponball">
                                                                <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                            </div>
                                                            <div class="pull-left detail-team-ponball">
                                                                <div class="top-detail-team-ponball text-left">
                                                                    <h4><?php echo $match->teamb->name_en; ?></h4>
                                                                </div>
                                                                <div class="bottom-detail-team-ponball hide">
                                                                    <div class="progress full-width">
                                                                        <div class="progress-bar progress-bar-danger beton-away"
                                                                             mid="<?php echo $match->id; ?>"
                                                                             role="progressbar"
                                                                             aria-valuenow="0"
                                                                             aria-valuemin="0" aria-valuemax="100"
                                                                             style="width: 0%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="score-ct beton-awayp"
                                                                         mid="<?php echo $match->id; ?>">
                                                                        0%
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="pull-left hide">
                                                                <ul class="status-lastMatch full-width">
                                                                    <?php foreach (explode(",", $match->lastresult_away) as $rs) { ?>
                                                                        <?php
                                                                        $rsclass = "draw-status";
                                                                        switch ($rs) {
                                                                            case "w":
                                                                                $rsclass = "win-status";
                                                                                break;
                                                                            case "l":
                                                                                $rsclass = "lose-status";
                                                                                break;
                                                                            default:
                                                                                $rsclass = "draw-status";
                                                                                break;
                                                                        }

                                                                        ?>
                                                                        <li>
                                                                            <div class="<?php echo $rsclass; ?>"></div>
                                                                        </li>
                                                                    <?php } ?>
                                                                    <li><p>Last Match</p></li>
                                                                </ul>
                                                                <ul class="status-lastMatch full-width odd">
                                                                    <?php foreach (explode(",", $match->lastresult_odds_away) as $rs) { ?>
                                                                        <?php
                                                                        $rsclass = "draw-status";
                                                                        switch ($rs) {
                                                                            case "w":
                                                                                $rsclass = "win-status";
                                                                                break;
                                                                            case "l":
                                                                                $rsclass = "lose-status";
                                                                                break;
                                                                            default:
                                                                                $rsclass = "draw-status";
                                                                                break;
                                                                        }

                                                                        ?>
                                                                        <li>
                                                                            <div class="<?php echo $rsclass; ?>"></div>
                                                                        </li>
                                                                    <?php } ?>
                                                                    <li><p>ODD</p></li>
                                                                </ul>
                                                            </div>


                                                        </div>
                                                        <div class="pull-right-section">
                                                            <div class="intro-game">
                                                                <a href="<?php echo(($difference >= 1) ? "#" : "/games?mid={$match->id}"); ?>"
                                                                   data-toggle="tooltip"
                                                                   data-placement="top"
                                                                   title="เล่นเกมทายผล">
                                                                    <i class="fa fa-gamepad" aria-hidden="true"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div style="clear: both"></div>
                                                    </a>
                                                    <div style="clear: both"></div>
                                                </div>


                                                <div id="<?php echo "collapse_endmatch" . $match->id; ?>"
                                                     class="panel-collapse panel-collapse-side collapsed ">
                                                    <div class="panel-collapse-side-info">
                                                        <div class="nano">
                                                            <div class="nano-content">
                                                                <div class="bg-topic">
                                                                    <div class="topic">
                                                                        <div class="logo-league-program">
                                                                            <img src="<?php echo $data['league']->getLogo(); ?>">
                                                                        </div>
                                                                        <div class="title-league-program">
                                                                            <h3><?php echo $data['league']->name; ?></h3>
                                                                        </div>
                                                                        <div class="pull-right content-col-left">
                                                                            <div class="at-minute ct-ft">
                                                                                <?php
                                                                                $stime = Carbon::createFromFormat("Y-m-d H:i:s", $match->time_match, "Asia/Singapore");
                                                                                $stime->tz = "Asia/Bangkok";
                                                                                ?>
                                                                                <span class="at-minute-mark "
                                                                                      at="<?php echo $match->time_match; ?>"
                                                                                      mid="<?php echo $match->id; ?>"
                                                                                      lid="<?php echo $lid; ?>"
                                                                                      state="<?php echo $match->state; ?>"
                                                                                      ht="<?php echo $match->ht_start; ?>"><?php echo $stime->format("H:i"); ?></span>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="panel-heading full-width">
                                                                        <div class="review-dropdown"
                                                                             mid="<?php echo $match->id; ?>">
                                                                            <div mid="<?php echo $match->id; ?>"
                                                                                 class="hdp-float hide wait-float">
                                                                                <ul>
                                                                                    <li class="live-h-hdp"
                                                                                        mid="<?php echo $match->id; ?>"><?php echo (empty($match->hdp)) ? "-" : number_format($match->hdp->home_water_bill, 2); ?></li>
                                                                                    <li>
                                                                                        <h3 class="live-hdp"
                                                                                            mid="<?php echo $match->id; ?>"><?php echo (empty($match->hdp)) ? "-" : $match->hdp->handicap; ?></h3>
                                                                                    </li>
                                                                                    <li class="live-a-hdp"
                                                                                        mid="<?php echo $match->id; ?>"><?php echo (empty($match->hdp)) ? "-" : number_format($match->hdp->away_water_bill, 2); ?></li>
                                                                                </ul>
                                                                            </div>
                                                                            <div mid="<?php echo $match->id; ?>"
                                                                                 class="hdp-float hide FT-float">
                                                                                <ul>
                                                                                    <li class="live-h-score"
                                                                                        mid="<?php echo $match->id; ?>"><?php echo $match->live_score_home; ?></li>
                                                                                    <li><h3>FT</h3></li>
                                                                                    <li class="live-a-score"
                                                                                        mid="<?php echo $match->id; ?>"><?php echo $match->live_score_away; ?></li>
                                                                                </ul>
                                                                            </div>
                                                                            <div mid="<?php echo $match->id; ?>"
                                                                                 class="hdp-float live-float">
                                                                                <ul>
                                                                                    <li class="live-h-score"
                                                                                        mid="<?php echo $match->id; ?>"><?php echo $match->live_score_home; ?></li>
                                                                                    <li>
                                                                                        <div mid="<?php echo $match->id; ?>"
                                                                                             data-percentage="0">

                                                                                            <div class="live-minute"
                                                                                                 mid="<?php echo $match->id; ?>">
                                                                                                -
                                                                                            </div>

                                                                                        </div>
                                                                                    </li>
                                                                                    <li class="live-a-score"
                                                                                        mid="<?php echo $match->id; ?>"><?php echo $match->live_score_away; ?></li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="left-accordion-row pull-left">
                                                                                <div class="pull-right logo-team-ponball">
                                                                                    <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                                </div>
                                                                                <div class="pull-right detail-team-ponball">
                                                                                    <div class="top-detail-team-ponball text-right">
                                                                                        <h4><?php echo $match->teama->name_en; ?></h4>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <ul class="status-lastMatch full-width">
                                                                                            <li><p>Last Match</p></li>
                                                                                            <?php foreach (array_reverse(explode(",", $match->lastresult_home)) as $rs) { ?>
                                                                                                <?php
                                                                                                $rsclass = "draw-status";
                                                                                                switch ($rs) {
                                                                                                    case "w":
                                                                                                        $rsclass = "win-status";
                                                                                                        break;
                                                                                                    case "l":
                                                                                                        $rsclass = "lose-status";
                                                                                                        break;
                                                                                                    default:
                                                                                                        $rsclass = "draw-status";
                                                                                                        break;
                                                                                                }

                                                                                                ?>
                                                                                                <li>
                                                                                                    <div class="<?php echo $rsclass; ?>"></div>
                                                                                                </li>
                                                                                            <?php } ?>
                                                                                        </ul>
                                                                                        <ul class="status-lastMatch full-width odd">
                                                                                            <li><p>ODD</p></li>
                                                                                            <?php foreach (array_reverse(explode(",", $match->lastresult_odds_home)) as $rs) { ?>
                                                                                                <?php
                                                                                                $rsclass = "draw-status";
                                                                                                switch ($rs) {
                                                                                                    case "w":
                                                                                                        $rsclass = "win-status";
                                                                                                        break;
                                                                                                    case "l":
                                                                                                        $rsclass = "lose-status";
                                                                                                        break;
                                                                                                    default:
                                                                                                        $rsclass = "draw-status";
                                                                                                        break;
                                                                                                }

                                                                                                ?>
                                                                                                <li>
                                                                                                    <div class="<?php echo $rsclass; ?>"></div>
                                                                                                </li>
                                                                                            <?php } ?>
                                                                                        </ul>
                                                                                    </div>

                                                                                </div>
                                                                                <div style="clear: both"></div>
                                                                                <div class="full-width">

                                                                                    <div class="bottom-detail-team-ponball inline-box">
                                                                                        <div class="score-ct pull-left text-right beton-homep"
                                                                                             mid="<?php echo $match->id; ?>">
                                                                                            0%
                                                                                        </div>
                                                                                        <div class="progress pull-right">
                                                                                            <div class="progress-bar pull-right beton-home"
                                                                                                 mid="<?php echo $match->id; ?>"
                                                                                                 role="progressbar"
                                                                                                 aria-valuenow="0"
                                                                                                 aria-valuemin="0"
                                                                                                 aria-valuemax="100"
                                                                                                 style="width: 0%;">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="btn-vote-bet inline-box active">
                                                                                        <button type="button"
                                                                                                class="btn btn-xs btn-primary">Vote
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="right-accordion-row pull-right">
                                                                                <div class="logo-team-ponball">
                                                                                    <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                                </div>
                                                                                <div class="detail-team-ponball">
                                                                                    <div class="top-detail-team-ponball text-left">
                                                                                        <h4><?php echo $match->teamb->name_en; ?></h4>
                                                                                    </div>
                                                                                    <div class="pull-left">
                                                                                        <ul class="status-lastMatch full-width">
                                                                                            <?php foreach (explode(",", $match->lastresult_away) as $rs) { ?>
                                                                                                <?php
                                                                                                $rsclass = "draw-status";
                                                                                                switch ($rs) {
                                                                                                    case "w":
                                                                                                        $rsclass = "win-status";
                                                                                                        break;
                                                                                                    case "l":
                                                                                                        $rsclass = "lose-status";
                                                                                                        break;
                                                                                                    default:
                                                                                                        $rsclass = "draw-status";
                                                                                                        break;
                                                                                                }

                                                                                                ?>
                                                                                                <li>
                                                                                                    <div class="<?php echo $rsclass; ?>"></div>
                                                                                                </li>
                                                                                            <?php } ?>
                                                                                            <li><p>Last Match</p></li>
                                                                                        </ul>
                                                                                        <ul class="status-lastMatch full-width odd">
                                                                                            <?php foreach (explode(",", $match->lastresult_odds_away) as $rs) { ?>
                                                                                                <?php
                                                                                                $rsclass = "draw-status";
                                                                                                switch ($rs) {
                                                                                                    case "w":
                                                                                                        $rsclass = "win-status";
                                                                                                        break;
                                                                                                    case "l":
                                                                                                        $rsclass = "lose-status";
                                                                                                        break;
                                                                                                    default:
                                                                                                        $rsclass = "draw-status";
                                                                                                        break;
                                                                                                }

                                                                                                ?>
                                                                                                <li>
                                                                                                    <div class="<?php echo $rsclass; ?>"></div>
                                                                                                </li>
                                                                                            <?php } ?>
                                                                                            <li><p>ODD</p></li>
                                                                                        </ul>
                                                                                    </div>


                                                                                </div>
                                                                                <div class="full-width">
                                                                                    <div class="btn-vote-bet">
                                                                                        <button type="button"
                                                                                                class="btn btn-xs btn-success">Vote
                                                                                        </button>
                                                                                    </div>
                                                                                    <div class="bottom-detail-team-ponball inline-box pull-right">
                                                                                        <div class="progress pull-left">
                                                                                            <div class="progress-bar progress-bar-danger beton-away"
                                                                                                 mid="<?php echo $match->id; ?>"
                                                                                                 role="progressbar"
                                                                                                 aria-valuenow="0"
                                                                                                 aria-valuemin="0"
                                                                                                 aria-valuemax="100"
                                                                                                 style="width: 0%;">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="score-ct pull-right beton-awayp"
                                                                                             mid="<?php echo $match->id; ?>">
                                                                                            0%
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="clear: both"></div>
                                                                        </div>
                                                                        <div style="clear: both"></div>
                                                                    </div>
                                                                </div>

                                                                <div class="panel-body">
                                                                    <!-- Nav tabs -->
                                                                    <ul class="nav nav-tabs nav-tabs-highlight"
                                                                        role="tablist">

                                                                        <li class="<?php echo ($match->state != 17) ? "active" : ""; ?>"
                                                                            role="presentation">
                                                                            <a href="#<?php echo "livescore-tab" . $match->id; ?>"
                                                                               role="tab"
                                                                               data-toggle="tab">Detail</a>
                                                                        </li>
                                                                        <li role="presentation">
                                                                            <a href="#stat-live" role="tab"
                                                                               data-toggle="tab">Statistics</a>
                                                                        </li>
                                                                        <li role="presentation">
                                                                            <a href="#h2h-live" role="tab"
                                                                               data-toggle="tab">Head to
                                                                                Head</a>
                                                                        </li>
                                                                        <li role="presentation">
                                                                            <a href="#formguide-live" role="tab"
                                                                               data-toggle="tab">Form
                                                                                Guide</a>
                                                                        </li>
                                                                        <li role="presentation">
                                                                            <a href="#liveups_live" role="tab"
                                                                               data-toggle="tab">Liveup</a>
                                                                        </li>
                                                                        <li class="<?php echo ($match->state == 17) ? "active" : ""; ?>"
                                                                            role="presentation">
                                                                            <a href="#<?php echo "review" . $match->id; ?>"
                                                                               role="tab"
                                                                               data-toggle="tab">ทีเด็ด-ทรรศนะ</a>
                                                                        </li>
                                                                    </ul>

                                                                    <!-- Tab panes -->
                                                                    <div class="tab-content">
                                                                        <div role="tabpanel"
                                                                             class="tab-pane <?php echo ($match->state != 17) ? "active" : ""; ?>"
                                                                             id="<?php echo "livescore-tab" . $match->id; ?>">
                                                                            <div class="content-detail-livescore">
                                                                                <div class="full-width inline-box">
                                                                                    <div class="header-event-match hide">
                                                                                        <ul>
                                                                                            <li><h3>EVENT
                                                                                                    MATCH</h3>
                                                                                            </li>
                                                                                            <li><span class="event-dp-button end-match"
                                                                                                      mid="<?php echo $match->id; ?>">End Match</span>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="full-width inline-box">
                                                                                        <div class="score-event-match">

                                                                                            <div class="pull-right">
                                                                                                <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                                                <p><?php echo $match->teama->name_en; ?></p>

                                                                                            </div>
                                                                                            <h4><span class="live-h-score"
                                                                                                      mid="<?php echo $match->id; ?>"><?php echo $match->live_score_home; ?></span>-<span
                                                                                                        class="live-a-score"
                                                                                                        mid="<?php echo $match->id; ?>"><?php echo $match->live_score_away; ?></span>
                                                                                            </h4>
                                                                                            <div class="pull-left text-right">
                                                                                                <p><?php echo $match->teamb->name_en; ?></p>
                                                                                                <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                                            </div>

                                                                                        </div>
                                                                                        <div class="content-event-match"
                                                                                             mid="<?php echo $match->id; ?>">
                                                                                            <?php
                                                                                            //$matchevent = $mcon->getEvent($match->id);
                                                                                            $matchevent = [];
                                                                                            ?>
                                                                                            <?php foreach ($matchevent as $event) { ?>
                                                                                                <?php if ($event->side == "home") { ?>
                                                                                                    <div class="full-width line-content-all">
                                                                                                        <div class="col-home-live">
                                                                                                            <div class="box-football-icon">
                                                                                                                <div class="<?php echo $event->getEventImgClass(); ?>"></div>
                                                                                                            </div>
                                                                                                            <div class="min-live"><?php echo $event->at_minute . "'"; ?></div>
                                                                                                            <div class="score-live"><?php echo $event->score; ?></div>
                                                                                                            <?php if (empty($event->assist)) { ?>
                                                                                                                <div class="name-live"><?php echo $event->player; ?></div>
                                                                                                            <?php } else { ?>
                                                                                                                <?php if ($event->event == "substutitions") { ?>
                                                                                                                    <div class="name-live">
                                                                                                                        <span class="substitute-player-out"></span>
                                                                                                                        <p><?php echo $event->player; ?></p>
                                                                                                                    </div>
                                                                                                                    <div class="name-live">
                                                                                                                        <span class="substitute-player-in"></span>
                                                                                                                        <p><?php echo $event->assist; ?></p>
                                                                                                                    </div>
                                                                                                                <?php } else { ?>
                                                                                                                    <div class="name-live"><?php echo $event->player . " (" . $event->assist . ")"; ?></div>
                                                                                                                <?php } ?>
                                                                                                            <?php } ?>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                <?php } else { ?>
                                                                                                    <div class="full-width line-content-all">
                                                                                                        <div class="col-away-live pull-right">
                                                                                                            <?php if (empty($event->assist)) { ?>
                                                                                                                <div class="name-live"><?php echo $event->player; ?></div>
                                                                                                            <?php } else { ?>
                                                                                                                <?php if ($event->event == "substutitions") { ?>
                                                                                                                    <div class="name-live">
                                                                                                                        <span class="substitute-player-in"></span>
                                                                                                                        <p><?php echo $event->assist; ?></p>
                                                                                                                    </div>
                                                                                                                    <div class="name-live">
                                                                                                                        <span class="substitute-player-out"></span>
                                                                                                                        <p><?php echo $event->player; ?></p>
                                                                                                                    </div>
                                                                                                                <?php } else { ?>
                                                                                                                    <div class="name-live"><?php echo "(" . $event->assist . ") " . $event->player; ?></div>
                                                                                                                <?php } ?>
                                                                                                            <?php } ?>
                                                                                                            <div class="score-live"><?php echo $event->score; ?></div>
                                                                                                            <div class="min-live"><?php echo $event->at_minute . "'"; ?></div>
                                                                                                            <div class="box-football-icon">
                                                                                                                <div class="<?php echo $event->getEventImgClass(); ?>"></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                <?php } ?>
                                                                                            <?php } ?>


                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div role="tabpanel" class="tab-pane"
                                                                             id="stat-live">
                                                                            <div class="inline-box">
                                                                                <!-- Nav tabs -->
                                                                                <ul class="nav nav-tabs hide"
                                                                                    role="tablist">
                                                                                    <li role="presentation"
                                                                                        class="active">
                                                                                        <a
                                                                                                href="#<?php echo "statistics-tab" . $match->id; ?>"
                                                                                                role="tab"
                                                                                                data-toggle="tab">Statistics</a>
                                                                                    </li>
                                                                                    <li role="presentation">
                                                                                        <a
                                                                                                href="#<?php echo "lineUp-tab" . $match->id; ?>"
                                                                                                role="tab"
                                                                                                data-toggle="tab">Line
                                                                                            Up</a>
                                                                                    </li>

                                                                                </ul>
                                                                                <div class="full-width inline-box"
                                                                                     style="height: 320px;">
                                                                                    <div class="nano">
                                                                                        <div class="nano-content">
                                                                                            <!-- Tab panes -->
                                                                                            <div id="<?php echo "statistics-tab" . $match->id; ?>">
                                                                                                <div class="table statistics-table">
                                                                                                    <table>
                                                                                                        <thead>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <div class="pull-right">
                                                                                                                    <h4><?php echo $match->teama->name_en; ?></h4>
                                                                                                                    <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                                                                </div>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <h3>
                                                                                                                    VS</h3>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div class="pull-left">
                                                                                                                    <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                                                                    <h4><?php echo $match->teamb->name_en; ?></h4>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        </thead>
                                                                                                        <tbody class="statistics-mark"
                                                                                                               mid="<?php echo $match->id; ?>">
                                                                                                        <?php
                                                                                                        // $statlist = $mcon->getStat($match->id);
                                                                                                        $statlist = [];
                                                                                                        ?>
                                                                                                        <?php foreach ($statlist as $stat) { ?>
                                                                                                            <?php $rmax = $stat->hval + $stat->aval; ?>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <div class="num-progress <?php echo ($stat->hval > $stat->aval) ? "active" : ""; ?>"><?php echo $stat->hval; ?></div>
                                                                                                                    <div class="progress">
                                                                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                                                                             role="progressbar"
                                                                                                                             aria-valuenow="<?php echo $stat->hval; ?>"
                                                                                                                             aria-valuemin="0"
                                                                                                                             aria-valuemax="<?php echo $rmax; ?>"
                                                                                                                             style="width: <?php echo $stat->hval / $rmax * 100; ?>%;">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                                <td><?php echo $stat->type; ?></td>
                                                                                                                <td>
                                                                                                                    <div class="progress">
                                                                                                                        <div class="progress-bar"
                                                                                                                             role="progressbar"
                                                                                                                             aria-valuenow="<?php echo $stat->aval; ?>"
                                                                                                                             aria-valuemin="0"
                                                                                                                             aria-valuemax="<?php echo $rmax; ?>"
                                                                                                                             style="width: <?php echo $stat->aval / $rmax * 100; ?>%;">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="num-progress <?php echo ($stat->hval < $stat->aval) ? "active" : ""; ?>"><?php echo $stat->aval; ?></div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        <?php } ?>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>


                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div role="tabpanel" class="tab-pane"
                                                                             id="h2h-live">
                                                                            <div class="content-h2h-match">
                                                                                <div class="h2h-col inline-box full-width">
                                                                                    <div class="h2h-state">
                                                                                        <ul>
                                                                                            <li>
                                                                                                <img src="/images/logo-team/manchester.png">
                                                                                            </li>
                                                                                            <li>4 Win</li>
                                                                                            <li>9 Draw</li>
                                                                                            <li>1 Lost</li>
                                                                                            <li>
                                                                                                <img src="/images/logo-team/Chelsea.png">
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="box-match-list-title-last full-width">
                                                                                        <div class="bh-ctn text-center full-width">

                                                                                            <div class="form-inline-checkbox"
                                                                                                 data-toggle="buttons">
                                                                                                <label class="btn btn-xs btn-default active">
                                                                                                    <input type="checkbox"
                                                                                                           autocomplete="off"
                                                                                                           checked="">
                                                                                                    All
                                                                                                </label>
                                                                                                <label class="btn btn-xs btn-default">
                                                                                                    <input type="checkbox"
                                                                                                           autocomplete="off">
                                                                                                    Premier League
                                                                                                </label>
                                                                                                <label class="btn btn-xs btn-default">
                                                                                                    <input type="checkbox"
                                                                                                           autocomplete="off">
                                                                                                    FA Cup
                                                                                                </label>
                                                                                                <label class="btn btn-xs btn-default">
                                                                                                    <input type="checkbox"
                                                                                                           autocomplete="off">
                                                                                                    UEFA Champions
                                                                                                    League
                                                                                                </label>
                                                                                                <label class="btn btn-xs btn-default">
                                                                                                    <input type="checkbox"
                                                                                                           autocomplete="off">
                                                                                                    EFL Cup
                                                                                                </label>


                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div style="clear: both;"></div>

                                                                                    <div class="full-width inline-box">
                                                                                        <div class="box-table-result-state blurred-box">
                                                                                            <div class="full-width inline-box">
                                                                                                <div class="box-match-tournament">
                                                                                                    <div class="box-match-list-topic">
                                                                                                        <div class="pull-left">
                                                                                                            <img src="/images/logo-team/fa-cup-2017.png">
                                                                                                            <p>FA
                                                                                                                Cup</p>
                                                                                                        </div>
                                                                                                        <ul class="inline-box pull-right">
                                                                                                            <li>Odds
                                                                                                            </li>
                                                                                                            <li>
                                                                                                                Resualt
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                    <div class="box-match-list">
                                                                                                        <div class="text-center">
                                                                                                            <div class="date-match">
                                                                                                                15
                                                                                                                Apr 18
                                                                                                            </div>
                                                                                                            <div class="time-match">
                                                                                                                15:00
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div>
                                                                                                            <div class="home-name-match team-bet-color">
                                                                                                                <img src="/images/logo-team/Chelsea.png">
                                                                                                                <span>Chelsea</span>
                                                                                                            </div>
                                                                                                            <div class="away-name-match hignlight-team-win">
                                                                                                                <img src="/images/logo-team/manchester.png">
                                                                                                                <span>Manchester united</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="match-score text-center">
                                                                                                            <div class="home-score-match">
                                                                                                                0
                                                                                                            </div>
                                                                                                            <div class="away-score-match">
                                                                                                                2
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <div class="odds-match">
                                                                                                                <div class="win-status"></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <div class="result-match">
                                                                                                                <div class="lose-status"></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="box-match-list">

                                                                                                        <div class="text-center">
                                                                                                            <div class="date-match">
                                                                                                                15
                                                                                                                Apr 18
                                                                                                            </div>
                                                                                                            <div class="time-match">
                                                                                                                15:00
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div>
                                                                                                            <div class="home-name-match hignlight-team-win">
                                                                                                                <img src="/images/logo-team/Chelsea.png">
                                                                                                                <span>Chelsea</span>
                                                                                                            </div>
                                                                                                            <div class="away-name-match team-bet-color">
                                                                                                                <img src="/images/logo-team/manchester.png">
                                                                                                                <span>Manchester united</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="match-score text-center">
                                                                                                            <div class="home-score-match">
                                                                                                                0
                                                                                                            </div>
                                                                                                            <div class="away-score-match">
                                                                                                                2
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <div class="odds-match">
                                                                                                                <div class="draw-status"></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <div class="result-match">
                                                                                                                <div class="win-status"></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="box-match-tournament">
                                                                                                    <div class="box-match-list-topic">
                                                                                                        <div class="pull-left">
                                                                                                            <img src="/images/five-leagues/premier-league-logo.png">
                                                                                                            <p>Premier
                                                                                                                League</p>
                                                                                                        </div>
                                                                                                        <div class="pull-right">
                                                                                                            <ul>
                                                                                                                <li>
                                                                                                                    Odds
                                                                                                                </li>
                                                                                                                <li>
                                                                                                                    Resualt
                                                                                                                </li>
                                                                                                            </ul>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="box-match-list">
                                                                                                        <div class="text-center">
                                                                                                            <div class="date-match">
                                                                                                                15
                                                                                                                Apr 18
                                                                                                            </div>
                                                                                                            <div class="time-match">
                                                                                                                15:00
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div>
                                                                                                            <div class="home-name-match team-bet-color">
                                                                                                                <img src="/images/logo-team/Chelsea.png">
                                                                                                                <span>Chelsea</span>
                                                                                                            </div>
                                                                                                            <div class="away-name-match hignlight-team-win">
                                                                                                                <img src="/images/logo-team/manchester.png">
                                                                                                                <span>Manchester united</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="match-score text-center">
                                                                                                            <div class="home-score-match">
                                                                                                                0
                                                                                                            </div>
                                                                                                            <div class="away-score-match">
                                                                                                                2
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <div class="odds-match">
                                                                                                                <div class="win-status"></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <div class="result-match">
                                                                                                                <div class="lose-status"></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="box-match-list">

                                                                                                        <div class="text-center">
                                                                                                            <div class="date-match">
                                                                                                                15
                                                                                                                Apr 18
                                                                                                            </div>
                                                                                                            <div class="time-match">
                                                                                                                15:00
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div>
                                                                                                            <div class="home-name-match hignlight-team-win">
                                                                                                                <img src="/images/logo-team/Chelsea.png">
                                                                                                                <span>Chelsea</span>
                                                                                                            </div>
                                                                                                            <div class="away-name-match team-bet-color">
                                                                                                                <img src="/images/logo-team/manchester.png">
                                                                                                                <span>Manchester united</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="match-score text-center">
                                                                                                            <div class="home-score-match">
                                                                                                                0
                                                                                                            </div>
                                                                                                            <div class="away-score-match">
                                                                                                                2
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <div class="odds-match">
                                                                                                                <div class="draw-status"></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <div class="result-match">
                                                                                                                <div class="win-status"></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="box-match-tournament">
                                                                                                    <div class="box-match-list-topic">
                                                                                                        <div class="pull-left">
                                                                                                            <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                                                                                            <p>UEFA
                                                                                                                Champions
                                                                                                                League</p>
                                                                                                        </div>
                                                                                                        <div class="pull-right">
                                                                                                            <ul>
                                                                                                                <li>
                                                                                                                    Odds
                                                                                                                </li>
                                                                                                                <li>
                                                                                                                    Resualt
                                                                                                                </li>
                                                                                                            </ul>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="box-match-list">
                                                                                                        <div class="text-center">
                                                                                                            <div class="date-match">
                                                                                                                15
                                                                                                                Apr 18
                                                                                                            </div>
                                                                                                            <div class="time-match">
                                                                                                                15:00
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div>
                                                                                                            <div class="home-name-match team-bet-color">
                                                                                                                <img src="/images/logo-team/Chelsea.png">
                                                                                                                <span>Chelsea</span>
                                                                                                            </div>
                                                                                                            <div class="away-name-match hignlight-team-win">
                                                                                                                <img src="/images/logo-team/manchester.png">
                                                                                                                <span>Manchester united</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="match-score text-center">
                                                                                                            <div class="home-score-match">
                                                                                                                0
                                                                                                            </div>
                                                                                                            <div class="away-score-match">
                                                                                                                2
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <div class="odds-match">
                                                                                                                <div class="win-status"></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <div class="result-match">
                                                                                                                <div class="lose-status"></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="box-match-list">

                                                                                                        <div class="text-center">
                                                                                                            <div class="date-match">
                                                                                                                15
                                                                                                                Apr 18
                                                                                                            </div>
                                                                                                            <div class="time-match">
                                                                                                                15:00
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div>
                                                                                                            <div class="home-name-match hignlight-team-win">
                                                                                                                <img src="/images/logo-team/Chelsea.png">
                                                                                                                <span>Chelsea</span>
                                                                                                            </div>
                                                                                                            <div class="away-name-match team-bet-color">
                                                                                                                <img src="/images/logo-team/manchester.png">
                                                                                                                <span>Manchester united</span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="match-score text-center">
                                                                                                            <div class="home-score-match">
                                                                                                                0
                                                                                                            </div>
                                                                                                            <div class="away-score-match">
                                                                                                                2
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <div class="odds-match">
                                                                                                                <div class="draw-status"></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <div class="result-match">
                                                                                                                <div class="win-status"></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div role="tabpanel" class="tab-pane"
                                                                             id="formguide-live">
                                                                            <div class="content-statistics-match">
                                                                                <div class="fg-col full-width">
                                                                                    <div class="full-width inline-box">

                                                                                        <div class="bt-flex">
                                                                                            <div class="box-table-result-state">
                                                                                                <div class="box-match-list-title-last full-width">
                                                                                                    <div class="bh-ctn text-center bt-flex">
                                                                                                        <div class="pull-left">
                                                                                                            <img src="/images/logo-team/manchester.png">
                                                                                                        </div>
                                                                                                        <div class="pull-left form-inline-checkbox"
                                                                                                             data-toggle="buttons">
                                                                                                            <label class="btn btn-xs btn-default active">
                                                                                                                <input type="checkbox"
                                                                                                                       autocomplete="off"
                                                                                                                       checked="">
                                                                                                                All
                                                                                                            </label>
                                                                                                            <label class="btn btn-xs btn-default">
                                                                                                                <input type="checkbox"
                                                                                                                       autocomplete="off">
                                                                                                                Premier
                                                                                                                League
                                                                                                            </label>
                                                                                                            <label class="btn btn-xs btn-default">
                                                                                                                <input type="checkbox"
                                                                                                                       autocomplete="off">
                                                                                                                FA Cup
                                                                                                            </label>
                                                                                                            <label class="btn btn-xs btn-default">
                                                                                                                <input type="checkbox"
                                                                                                                       autocomplete="off">
                                                                                                                EFL Cup
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="full-width inline-box">
                                                                                                    <div class="box-match-tournament">
                                                                                                        <div class="box-match-list-topic">
                                                                                                            <div class="pull-left">
                                                                                                                <img src="/images/logo-team/fa-cup-2017.png">
                                                                                                                <p>FA
                                                                                                                    Cup</p>
                                                                                                            </div>
                                                                                                            <div class="pull-right">
                                                                                                                <ul>
                                                                                                                    <li>
                                                                                                                        O
                                                                                                                    </li>
                                                                                                                    <li>
                                                                                                                        R
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="box-match-list">

                                                                                                            <div class="text-center">
                                                                                                                <div class="date-match">
                                                                                                                    15
                                                                                                                    Apr
                                                                                                                    18
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div>
                                                                                                                <div class="home-name-match team-bet-color">
                                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                                    <span>Chelsea</span>
                                                                                                                </div>
                                                                                                                <div class="away-name-match hignlight-team-win">
                                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                                    <span>Manchester United</span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="match-score text-center">
                                                                                                                <div class="home-score-match">
                                                                                                                    0
                                                                                                                </div>
                                                                                                                <div class="away-score-match">
                                                                                                                    2
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="odds-match">
                                                                                                                    <div class="win-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="result-match">
                                                                                                                    <div class="lose-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="box-match-list">

                                                                                                            <div class="text-center">
                                                                                                                <div class="date-match">
                                                                                                                    15
                                                                                                                    Apr
                                                                                                                    18
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div>
                                                                                                                <div class="home-name-match team-bet-color">
                                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                                    <span>Chelsea</span>
                                                                                                                </div>
                                                                                                                <div class="away-name-match hignlight-team-win">
                                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                                    <span>Manchester United</span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="match-score text-center">
                                                                                                                <div class="home-score-match">
                                                                                                                    0
                                                                                                                </div>
                                                                                                                <div class="away-score-match">
                                                                                                                    2
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="odds-match">
                                                                                                                    <div class="win-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="result-match">
                                                                                                                    <div class="lose-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="box-match-tournament">
                                                                                                        <div class="box-match-list-topic">
                                                                                                            <div class="pull-left">
                                                                                                                <img src="/images/five-leagues/premier-league-logo.png">
                                                                                                                <p>
                                                                                                                    Premier
                                                                                                                    League</p>
                                                                                                            </div>
                                                                                                            <div class="pull-right">
                                                                                                                <ul>
                                                                                                                    <li>
                                                                                                                        O
                                                                                                                    </li>
                                                                                                                    <li>
                                                                                                                        R
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="box-match-list">
                                                                                                            <div class="text-center">
                                                                                                                <div class="date-match">
                                                                                                                    15
                                                                                                                    Apr
                                                                                                                    18
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div>
                                                                                                                <div class="home-name-match highlight-team-win">
                                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                                    <span>Chelsea</span>
                                                                                                                </div>
                                                                                                                <div class="away-name-match">
                                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                                    <span>Manchester United</span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="match-score text-center">
                                                                                                                <div class="home-score-match">
                                                                                                                    0
                                                                                                                </div>
                                                                                                                <div class="away-score-match">
                                                                                                                    2
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="odds-match">
                                                                                                                    <div class="win-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="result-match">
                                                                                                                    <div class="lose-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="box-match-list">

                                                                                                            <div class="text-center">
                                                                                                                <div class="date-match">
                                                                                                                    15
                                                                                                                    Apr
                                                                                                                    18
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div>
                                                                                                                <div class="home-name-match">
                                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                                    <span>Chelsea</span>
                                                                                                                </div>
                                                                                                                <div class="away-name-match highlight-team-win">
                                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                                    <span>Manchester United</span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="match-score text-center">
                                                                                                                <div class="home-score-match">
                                                                                                                    0
                                                                                                                </div>
                                                                                                                <div class="away-score-match">
                                                                                                                    2
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="odds-match">
                                                                                                                    <div class="win-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="result-match">
                                                                                                                    <div class="lose-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="box-match-tournament">
                                                                                                        <div class="box-match-list-topic">
                                                                                                            <div class="pull-left">
                                                                                                                <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                                                                                                <p>UEFA
                                                                                                                    Champions
                                                                                                                    League</p>
                                                                                                            </div>
                                                                                                            <div class="pull-right">
                                                                                                                <ul>
                                                                                                                    <li>
                                                                                                                        O
                                                                                                                    </li>
                                                                                                                    <li>
                                                                                                                        R
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="box-match-list">

                                                                                                            <div class="text-center">
                                                                                                                <div class="date-match">
                                                                                                                    15
                                                                                                                    Apr
                                                                                                                    18
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div>
                                                                                                                <div class="home-name-match">
                                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                                    <span>Chelsea</span>
                                                                                                                </div>
                                                                                                                <div class="away-name-match hignlight-team-win">
                                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                                    <span>Manchester United</span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="match-score text-center">
                                                                                                                <div class="home-score-match">
                                                                                                                    0
                                                                                                                </div>
                                                                                                                <div class="away-score-match">
                                                                                                                    2
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="odds-match">
                                                                                                                    <div class="win-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="result-match">
                                                                                                                    <div class="lose-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="box-match-list">

                                                                                                            <div class="text-center">
                                                                                                                <div class="date-match">
                                                                                                                    15
                                                                                                                    Apr
                                                                                                                    18
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div>
                                                                                                                <div class="home-name-match">
                                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                                    <span>Chelsea</span>
                                                                                                                </div>
                                                                                                                <div class="away-name-match highlight-team-win">
                                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                                    <span>Manchester United</span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="match-score text-center">
                                                                                                                <div class="home-score-match">
                                                                                                                    0
                                                                                                                </div>
                                                                                                                <div class="away-score-match">
                                                                                                                    2
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="odds-match">
                                                                                                                    <div class="win-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="result-match">
                                                                                                                    <div class="lose-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>
                                                                                            <div class="box-table-result-state">
                                                                                                <div class="box-match-list-title-last full-width">
                                                                                                    <div class="bh-ctn text-center bt-flex">
                                                                                                        <div class="pull-left">
                                                                                                            <img src="/images/logo-team/Chelsea.png">
                                                                                                        </div>
                                                                                                        <div class="pull-left form-inline-checkbox"
                                                                                                             data-toggle="buttons">
                                                                                                            <label class="btn btn-xs btn-default active">
                                                                                                                <input type="checkbox"
                                                                                                                       autocomplete="off"
                                                                                                                       checked="">
                                                                                                                All
                                                                                                            </label>
                                                                                                            <label class="btn btn-xs btn-default">
                                                                                                                <input type="checkbox"
                                                                                                                       autocomplete="off">
                                                                                                                Premier
                                                                                                                League
                                                                                                            </label>
                                                                                                            <label class="btn btn-xs btn-default">
                                                                                                                <input type="checkbox"
                                                                                                                       autocomplete="off">
                                                                                                                FA Cup
                                                                                                            </label>
                                                                                                            <label class="btn btn-xs btn-default">
                                                                                                                <input type="checkbox"
                                                                                                                       autocomplete="off">
                                                                                                                UEFA
                                                                                                                Champions
                                                                                                                League
                                                                                                            </label>
                                                                                                            <label class="btn btn-xs btn-default">
                                                                                                                <input type="checkbox"
                                                                                                                       autocomplete="off">
                                                                                                                EFL Cup
                                                                                                            </label>


                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="full-width inline-box">
                                                                                                    <div class="box-match-tournament">
                                                                                                        <div class="box-match-list-topic">
                                                                                                            <div class="pull-left">
                                                                                                                <img src="/images/logo-team/fa-cup-2017.png">
                                                                                                                <p>FA
                                                                                                                    Cup</p>
                                                                                                            </div>
                                                                                                            <div class="pull-right">
                                                                                                                <ul>
                                                                                                                    <li>
                                                                                                                        O
                                                                                                                    </li>
                                                                                                                    <li>
                                                                                                                        R
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="box-match-list">

                                                                                                            <div class="text-center">
                                                                                                                <div class="date-match">
                                                                                                                    15
                                                                                                                    Apr
                                                                                                                    18
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div>
                                                                                                                <div class="home-name-match team-bet-color">
                                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                                    <span>Chelsea</span>
                                                                                                                </div>
                                                                                                                <div class="away-name-match hignlight-team-win">
                                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                                    <span>Manchester United</span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="match-score text-center">
                                                                                                                <div class="home-score-match">
                                                                                                                    0
                                                                                                                </div>
                                                                                                                <div class="away-score-match">
                                                                                                                    2
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="odds-match">
                                                                                                                    <div class="win-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="result-match">
                                                                                                                    <div class="lose-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="box-match-list">

                                                                                                            <div class="text-center">
                                                                                                                <div class="date-match">
                                                                                                                    15
                                                                                                                    Apr
                                                                                                                    18
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div>
                                                                                                                <div class="home-name-match team-bet-color">
                                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                                    <span>Chelsea</span>
                                                                                                                </div>
                                                                                                                <div class="away-name-match hignlight-team-win">
                                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                                    <span>Manchester United</span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="match-score text-center">
                                                                                                                <div class="home-score-match">
                                                                                                                    0
                                                                                                                </div>
                                                                                                                <div class="away-score-match">
                                                                                                                    2
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="odds-match">
                                                                                                                    <div class="win-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="result-match">
                                                                                                                    <div class="lose-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="box-match-tournament">
                                                                                                        <div class="box-match-list-topic">
                                                                                                            <div class="pull-left">
                                                                                                                <img src="/images/five-leagues/premier-league-logo.png">
                                                                                                                <p>
                                                                                                                    Premier
                                                                                                                    League</p>
                                                                                                            </div>
                                                                                                            <div class="pull-right">
                                                                                                                <ul>
                                                                                                                    <li>
                                                                                                                        O
                                                                                                                    </li>
                                                                                                                    <li>
                                                                                                                        R
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="box-match-list">
                                                                                                            <div class="text-center">
                                                                                                                <div class="date-match">
                                                                                                                    15
                                                                                                                    Apr
                                                                                                                    18
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div>
                                                                                                                <div class="home-name-match highlight-team-win">
                                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                                    <span>Chelsea</span>
                                                                                                                </div>
                                                                                                                <div class="away-name-match">
                                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                                    <span>Manchester United</span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="match-score text-center">
                                                                                                                <div class="home-score-match">
                                                                                                                    0
                                                                                                                </div>
                                                                                                                <div class="away-score-match">
                                                                                                                    2
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="odds-match">
                                                                                                                    <div class="win-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="result-match">
                                                                                                                    <div class="lose-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="box-match-list">

                                                                                                            <div class="text-center">
                                                                                                                <div class="date-match">
                                                                                                                    15
                                                                                                                    Apr
                                                                                                                    18
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div>
                                                                                                                <div class="home-name-match">
                                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                                    <span>Chelsea</span>
                                                                                                                </div>
                                                                                                                <div class="away-name-match highlight-team-win">
                                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                                    <span>Manchester United</span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="match-score text-center">
                                                                                                                <div class="home-score-match">
                                                                                                                    0
                                                                                                                </div>
                                                                                                                <div class="away-score-match">
                                                                                                                    2
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="odds-match">
                                                                                                                    <div class="win-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="result-match">
                                                                                                                    <div class="lose-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="box-match-tournament">
                                                                                                        <div class="box-match-list-topic">
                                                                                                            <div class="pull-left">
                                                                                                                <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                                                                                                <p>UEFA
                                                                                                                    Champions
                                                                                                                    League</p>
                                                                                                            </div>
                                                                                                            <div class="pull-right">
                                                                                                                <ul>
                                                                                                                    <li>
                                                                                                                        O
                                                                                                                    </li>
                                                                                                                    <li>
                                                                                                                        R
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="box-match-list">

                                                                                                            <div class="text-center">
                                                                                                                <div class="date-match">
                                                                                                                    15
                                                                                                                    Apr
                                                                                                                    18
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div>
                                                                                                                <div class="home-name-match">
                                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                                    <span>Chelsea</span>
                                                                                                                </div>
                                                                                                                <div class="away-name-match hignlight-team-win">
                                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                                    <span>Manchester United</span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="match-score text-center">
                                                                                                                <div class="home-score-match">
                                                                                                                    0
                                                                                                                </div>
                                                                                                                <div class="away-score-match">
                                                                                                                    2
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="odds-match">
                                                                                                                    <div class="win-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="result-match">
                                                                                                                    <div class="lose-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="box-match-list">

                                                                                                            <div class="text-center">
                                                                                                                <div class="date-match">
                                                                                                                    15
                                                                                                                    Apr
                                                                                                                    18
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div>
                                                                                                                <div class="home-name-match">
                                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                                    <span>Chelsea</span>
                                                                                                                </div>
                                                                                                                <div class="away-name-match highlight-team-win">
                                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                                    <span>Manchester United</span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="match-score text-center">
                                                                                                                <div class="home-score-match">
                                                                                                                    0
                                                                                                                </div>
                                                                                                                <div class="away-score-match">
                                                                                                                    2
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="odds-match">
                                                                                                                    <div class="win-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <div class="result-match">
                                                                                                                    <div class="lose-status"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div role="tabpanel" class="tab-pane"
                                                                             id="liveups_live">
                                                                            <div id="<?php echo "lineUp-tab" . $match->id; ?>">
                                                                                <div class="lineUp-table">
                                                                                    <table class="table">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <div class="pull-right full-width text-right hide">
                                                                                                    <span><?php echo $match->teama->name_en; ?></span>
                                                                                                    <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                                                </div>
                                                                                                <div class="pull-right full-width">
                                                                                                    <div class="content-lineUp-home">
                                                                                                        <ul>
                                                                                                            <?php
                                                                                                            $lineups = $mcon->getLineups($match->id . "");
                                                                                                            foreach ($lineups as $lineup) {
                                                                                                                if ($lineup->side == "home") {
                                                                                                                    ?>
                                                                                                                    <li>
                                                                                                                        <div class="num-player"><?php echo $lineup->number; ?></div>
                                                                                                                        <div><?php echo $lineup->name; ?></div>
                                                                                                                        <div><?php echo $lineup->position; ?></div>
                                                                                                                    </li>
                                                                                                                <?php } ?>
                                                                                                            <?php } ?>

                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div class="pull-left hide">
                                                                                                    <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                                                    <span><?php echo $match->teamb->name_en; ?></span>
                                                                                                </div>
                                                                                                <div class="pull-left full-width">
                                                                                                    <div class="content-lineUp-away">
                                                                                                        <ul>
                                                                                                            <?php
                                                                                                            $lineups = $mcon->getLineups($match->id . "");
                                                                                                            foreach ($lineups as $lineup) {
                                                                                                                if ($lineup->side == "away") {
                                                                                                                    ?>
                                                                                                                    <li>
                                                                                                                        <div class="num-player"><?php echo $lineup->number; ?></div>
                                                                                                                        <div><?php echo $lineup->name; ?></div>
                                                                                                                        <div><?php echo $lineup->position; ?></div>
                                                                                                                    </li>
                                                                                                                <?php } ?>
                                                                                                            <?php } ?>

                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>


                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div role="tabpanel"
                                                                             class="tab-pane <?php echo ($match->state == 17) ? "active" : ""; ?>"
                                                                             id="<?php echo "review" . $match->id; ?>">
                                                                            <div class="home-panel-body pull-left">
                                                                                <?php foreach ($match->reviews as $review) { ?>
                                                                                    <?php if ($review->team == "home") { ?>
                                                                                        <div class="content-home-review full-width pull-right">
                                                                                            <div class="pull-right">
                                                                                                <div class="area-logoName">
                                                                                                    <div class="pull-right logo-review-user">
                                                                                                        <img src="<?php echo $review->owner->path; ?>">
                                                                                                    </div>
                                                                                                    <div class="pull-right">
                                                                                                        <h4><?php echo $review->owner->name; ?></h4>
                                                                                                        <ul>
                                                                                                            <li><?php echo "@" . strtolower(str_replace(" ", "", $match->teama->name_en)); ?></li>
                                                                                                            <li>
                                                                                                                <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="pull-right text-review-content text-right">
                                                                                                <p>
                                                                                                    <?php echo $review->review_text; ?>
                                                                                                </p>
                                                                                            </div>
                                                                                            <div class="pull-left ct-winrate">
                                                                                                <div>
                                                                                                    <h3><?php echo number_format($review->owner->accurate, 0) . "%"; ?></h3>
                                                                                                    <small>Win rate
                                                                                                    </small>
                                                                                                </div>
                                                                                                <div>
                                                                                                    <h3>30</h3>
                                                                                                    <small>Matches
                                                                                                    </small>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            </div>
                                                                            <div class="away-panel-body pull-right">

                                                                                <?php foreach ($match->reviews as $review) { ?>
                                                                                    <?php if ($review->team == "away") { ?>
                                                                                        <div class="content-away-review full-width pull-left">
                                                                                            <div class="pull-left">
                                                                                                <div class="area-logoName area-logoName-away">
                                                                                                    <div class="pull-left logo-review-user">
                                                                                                        <img src="<?php echo $review->owner->path; ?>">
                                                                                                    </div>
                                                                                                    <div class="pull-left">
                                                                                                        <h4><?php echo $review->owner->name; ?></h4>
                                                                                                        <ul>
                                                                                                            <li>
                                                                                                                <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                                                            </li>
                                                                                                            <li><?php echo "@" . strtolower(str_replace(" ", "", $match->teamb->name_en)); ?></li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="pull-left text-review-content text-left">
                                                                                                <p>
                                                                                                    <?php echo $review->review_text; ?>
                                                                                                </p>
                                                                                            </div>
                                                                                            <div class="pull-right ct-winrate">
                                                                                                <div>
                                                                                                    <h3><?php echo number_format($review->owner->accurate, 0) . "%"; ?></h3>
                                                                                                    <small>Win rate
                                                                                                    </small>
                                                                                                </div>
                                                                                                <div>
                                                                                                    <h3>30</h3>
                                                                                                    <small>Matches
                                                                                                    </small>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            </div>
                                                                            <!--                                                                            <div class="full-width text-center">-->
                                                                            <!--                                                                                <a class="btn btn-default btn-sm"-->
                                                                            <!--                                                                                   href="/review-infomation?match_id=-->
                                                                            <?php //echo $match->id; ?><!--"-->
                                                                            <!--                                                                                   role="button">อ่านทีเด็ดทรรศนะทั้งหมด</a>-->
                                                                            <!--                                                                            </div>-->
                                                                        </div>
                                                                        <div style="clear: both"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>


                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>


                <!--    Full Time-->
                <?php foreach ($matchlist['order'] as $lid => $priority) { ?>
                <?php $data = $matchlist['data'][$lid]; ?>
                <div class="content-program-soccer league-box" lid="<?php echo $lid; ?>"
                     id="league-mark-<?php echo preg_replace('/\s+/', '', $data['league']->name); ?>">
                    <div class="topic topic-tab">
                        <div class="logo-league-program">
                            <img src="<?php echo $data['league']->getLogo(); ?>">
                        </div>
                        <div class="title-league-program">
                            <h3><?php echo $data['league']->name; ?></h3>
                            <!--                    <h5>บ้านผลบอล โปรแกรมบอล ราคาบอล </h5>-->
                        </div>
                        <div class="dropdown pull-right">
                        </div>

                    </div>
                    <div class="content">
                        <div class="panel-group panel-group-row matches-row league-matches"
                             lid="<?php echo $lid; ?>"
                             id="accordion<?php echo $lid; ?>">
                            <?php foreach ($data['matches'] as $match) { ?>
                            <?php if ($match->state >= 4) { ?>
                            <div class="panel panel-default livescore-panel color-tape"
                                 lid="<?php echo $lid; ?>"
                                 mid="<?php echo $match->id; ?>">
                                <div class="panel-heading">
                                    <a data-toggle="collapse"
                                       class="review-dropdown full-width inline-box review-dropdown-left collapsed"
                                       data-parent="#accordion<?php echo $lid; ?>"
                                       href="#<?php echo "collapse_endmatch" . $match->id; ?>"
                                       mid="<?php echo $match->id; ?>">
                                        <div mid="<?php echo $match->id; ?>" class="hdp-float wait-float">
                                            <ul>
                                                <li class="live-h-hdp"
                                                    mid="<?php echo $match->id; ?>"><?php echo (empty($match->hdp)) ? "-" : number_format($match->hdp->home_water_bill, 2); ?></li>
                                                <li><h3 class="live-hdp"
                                                        mid="<?php echo $match->id; ?>"><?php echo (empty($match->hdp)) ? "-" : $match->hdp->handicap; ?></h3>
                                                </li>
                                                <li class="live-a-hdp"
                                                    mid="<?php echo $match->id; ?>"><?php echo (empty($match->hdp)) ? "-" : number_format($match->hdp->away_water_bill, 2); ?></li>
                                            </ul>
                                        </div>
                                        <div mid="<?php echo $match->id; ?>"
                                             class="hdp-float hide FT-float">
                                            <ul>
                                                <li class="live-h-score"
                                                    mid="<?php echo $match->id; ?>"><?php echo $match->live_score_home; ?></li>
                                                <li><h3>FT</h3></li>
                                                <li class="live-a-score"
                                                    mid="<?php echo $match->id; ?>"><?php echo $match->live_score_away; ?></li>
                                            </ul>
                                        </div>
                                        <div mid="<?php echo $match->id; ?>"
                                             class="hdp-float hide live-float">
                                            <ul>
                                                <li class="live-h-score"
                                                    mid="<?php echo $match->id; ?>"><?php echo $match->live_score_home; ?></li>
                                                <li>
                                                    <div class="progress minute-progress"
                                                         mid="<?php echo $match->id; ?>"
                                                         data-percentage="40">
				                            <span class="progress-left">
                                                <span class="progress-bar"></span>
				                            </span>
                                                        <span class="progress-right">
                                                <span class="progress-bar"></span>
                                            </span>
                                                        <div class="progress-value">
                                                            <div class="live-minute"
                                                                 mid="<?php echo $match->id; ?>">45+
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="live-a-score"
                                                    mid="<?php echo $match->id; ?>"><?php echo $match->live_score_away; ?></li>
                                            </ul>
                                        </div>
                                        <div class="left-accordion-row pull-left">
                                            <div class="pull-right logo-team-ponball">
                                                <img src="<?php echo $match->teama->getLogo(); ?>">
                                            </div>
                                            <div class="pull-right detail-team-ponball">
                                                <div class="top-detail-team-ponball text-right">
                                                    <div class="red-card red-card-2 hide"><span>x2</span>
                                                    </div>
                                                    <h4><?php echo $match->teama->name_en; ?></h4>
                                                </div>
                                                <div class="bottom-detail-team-ponball hide">
                                                    <div class="score-ct beton-homep"
                                                         mid="<?php echo $match->id; ?>">0%
                                                    </div>
                                                    <div class="progress full-width">
                                                        <div class="progress-bar pull-right beton-home"
                                                             mid="<?php echo $match->id; ?>"
                                                             role="progressbar"
                                                             aria-valuenow="0"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width: 0%;">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pull-right">
                                                    <ul class="status-lastMatch full-width hide">
                                                        <li><p>Last Match</p></li>
                                                        <?php foreach (array_reverse(explode(",", $match->lastresult_home)) as $rs) { ?>
                                                            <?php
                                                            $rsclass = "draw-status";
                                                            switch ($rs) {
                                                                case "w":
                                                                    $rsclass = "win-status";
                                                                    break;
                                                                case "l":
                                                                    $rsclass = "lose-status";
                                                                    break;
                                                                default:
                                                                    $rsclass = "draw-status";
                                                                    break;
                                                            }

                                                            ?>
                                                            <li>
                                                                <div class="<?php echo $rsclass; ?>"></div>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                    <ul class="status-lastMatch full-width odd hide">
                                                        <li><p>ODD</p></li>
                                                        <?php foreach (array_reverse(explode(",", $match->lastresult_odds_home)) as $rs) { ?>
                                                            <?php
                                                            $rsclass = "draw-status";
                                                            switch ($rs) {
                                                                case "w":
                                                                    $rsclass = "win-status";
                                                                    break;
                                                                case "l":
                                                                    $rsclass = "lose-status";
                                                                    break;
                                                                default:
                                                                    $rsclass = "draw-status";
                                                                    break;
                                                            }

                                                            ?>
                                                            <li>
                                                                <div class="<?php echo $rsclass; ?>"></div>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>


                                            <div class="pull-left content-col-left">
                                                <div class="at-minute ct-ft">
                                                    <?php
                                                    $stime = Carbon::createFromFormat("Y-m-d H:i:s", $match->time_match, "Asia/Singapore");
                                                    $stime->tz = "Asia/Bangkok";
                                                    ?>
                                                    <span class="at-minute-mark "
                                                          at="<?php echo $match->time_match; ?>"
                                                          mid="<?php echo $match->id; ?>"
                                                          lid="<?php echo $lid; ?>"
                                                          state="<?php echo $match->state; ?>"
                                                          ht="<?php echo $match->ht_start; ?>"><?php echo $stime->format("H:i"); ?></span>
                                                </div>
                                                <div class="ct-live-chanel hide">
                                                    <img src="/images/channel/pptv.png">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="right-accordion-row pull-right">
                                            <div class="logo-team-ponball">
                                                <img src="<?php echo $match->teamb->getLogo(); ?>">
                                            </div>
                                            <div class="detail-team-ponball">
                                                <div class="top-detail-team-ponball text-left">
                                                    <h4><?php echo $match->teamb->name_en; ?></h4>
                                                    <div class="red-card red-card-2 hide"><span>x2</span>
                                                    </div>
                                                </div>
                                                <div class="pull-left">
                                                    <ul class="status-lastMatch full-width hide">
                                                        <?php foreach (explode(",", $match->lastresult_away) as $rs) { ?>
                                                            <?php
                                                            $rsclass = "draw-status";
                                                            switch ($rs) {
                                                                case "w":
                                                                    $rsclass = "win-status";
                                                                    break;
                                                                case "l":
                                                                    $rsclass = "lose-status";
                                                                    break;
                                                                default:
                                                                    $rsclass = "draw-status";
                                                                    break;
                                                            }

                                                            ?>
                                                            <li>
                                                                <div class="<?php echo $rsclass; ?>"></div>
                                                            </li>
                                                        <?php } ?>
                                                        <li><p>Last Match</p></li>
                                                    </ul>
                                                    <ul class="status-lastMatch full-width odd hide">
                                                        <?php foreach (explode(",", $match->lastresult_odds_away) as $rs) { ?>
                                                            <?php
                                                            $rsclass = "draw-status";
                                                            switch ($rs) {
                                                                case "w":
                                                                    $rsclass = "win-status";
                                                                    break;
                                                                case "l":
                                                                    $rsclass = "lose-status";
                                                                    break;
                                                                default:
                                                                    $rsclass = "draw-status";
                                                                    break;
                                                            }

                                                            ?>
                                                            <li>
                                                                <div class="<?php echo $rsclass; ?>"></div>
                                                            </li>
                                                        <?php } ?>
                                                        <li><p>ODD</p></li>
                                                    </ul>
                                                </div>
                                                <div class="bottom-detail-team-ponball hide">
                                                    <div class="progress full-width">
                                                        <div class="progress-bar progress-bar-danger beton-away"
                                                             mid="<?php echo $match->id; ?>"
                                                             role="progressbar"
                                                             aria-valuenow="0"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width: 0%;">
                                                        </div>
                                                    </div>
                                                    <div class="score-ct beton-awayp"
                                                         mid="<?php echo $match->id; ?>">0%
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="pull-right-section">
                                            <div class="intro-game">
                                                <a href="<?php echo(($difference >= 1) ? "#" : "/games?mid={$match->id}"); ?>"
                                                   data-toggle="tooltip"
                                                   data-placement="top"
                                                   title="เล่นเกมทายผล">
                                                    <i class="fa fa-gamepad" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div style="clear: both"></div>
                                    </a>
                                    <div style="clear: both"></div>
                                </div>


                                <div id="<?php echo "collapse_endmatch" . $match->id; ?>"
                                     class="panel-collapse panel-collapse-side collapse">
                                    <div class="panel-collapse-side-info">
                                        <div class="nano">
                                            <div class="nano-content">

                                                <div class="bg-topic">
                                                    <div class="topic">
                                                        <div class="logo-league-program">
                                                            <img src="<?php echo $data['league']->getLogo(); ?>">
                                                        </div>
                                                        <div class="title-league-program">
                                                            <h3><?php echo $data['league']->name; ?></h3>
                                                        </div>
                                                        <div class="pull-right content-col-left">
                                                            <div class="at-minute ct-ft">
                                                                <?php
                                                                $stime = Carbon::createFromFormat("Y-m-d H:i:s", $match->time_match, "Asia/Singapore");
                                                                $stime->tz = "Asia/Bangkok";
                                                                ?>
                                                                <span class="at-minute-mark "
                                                                      at="<?php echo $match->time_match; ?>"
                                                                      mid="<?php echo $match->id; ?>"
                                                                      lid="<?php echo $lid; ?>"
                                                                      state="<?php echo $match->state; ?>"
                                                                      ht="<?php echo $match->ht_start; ?>"><?php echo $stime->format("H:i"); ?></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="panel-heading full-width">
                                                        <div class="review-dropdown">

                                                            <!--                                                                        HDP-->
                                                            <div mid="<?php echo $match->id; ?>"
                                                                 class="hdp-float wait-float">
                                                                <ul>
                                                                    <li class="live-h-hdp"
                                                                        mid="<?php echo $match->id; ?>"><?php echo (empty($match->hdp)) ? "-" : number_format($match->hdp->home_water_bill, 2); ?></li>
                                                                    <li><h3 class="live-hdp"
                                                                            mid="<?php echo $match->id; ?>"><?php echo (empty($match->hdp)) ? "-" : $match->hdp->handicap; ?></h3>
                                                                    </li>
                                                                    <li class="live-a-hdp"
                                                                        mid="<?php echo $match->id; ?>"><?php echo (empty($match->hdp)) ? "-" : number_format($match->hdp->away_water_bill, 2); ?></li>
                                                                </ul>
                                                            </div>

                                                            <!--                                                                        Full Time-->
                                                            <div mid="<?php echo $match->id; ?>"
                                                                 class="hdp-float hide FT-float">
                                                                <ul>
                                                                    <li class="live-h-score"
                                                                        mid="<?php echo $match->id; ?>"><?php echo $match->live_score_home; ?></li>
                                                                    <li><h3>FT</h3></li>
                                                                    <li class="live-a-score"
                                                                        mid="<?php echo $match->id; ?>"><?php echo $match->live_score_away; ?></li>
                                                                </ul>
                                                            </div>

                                                            <!--                                                                        Live Time-->
                                                            <div mid="<?php echo $match->id; ?>"
                                                                 class="hdp-float hide live-float">
                                                                <ul>
                                                                    <li class="live-h-score"
                                                                        mid="<?php echo $match->id; ?>"><?php echo $match->live_score_home; ?></li>
                                                                    <li>
                                                                        <div class="progress minute-progress"
                                                                             mid="<?php echo $match->id; ?>"
                                                                             data-percentage="40">
				                            <span class="progress-left">
                                                <span class="progress-bar"></span>
				                            </span>
                                                                            <span class="progress-right">
                                                <span class="progress-bar"></span>
                                            </span>
                                                                            <div class="progress-value">
                                                                                <div class="live-minute"
                                                                                     mid="<?php echo $match->id; ?>">
                                                                                    45+
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="live-a-score"
                                                                        mid="<?php echo $match->id; ?>"><?php echo $match->live_score_away; ?></li>
                                                                </ul>
                                                            </div>


                                                            <div class="left-accordion-row pull-left">
                                                                <div class="pull-right logo-team-ponball">
                                                                    <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                </div>
                                                                <div class="pull-right detail-team-ponball">
                                                                    <div class="top-detail-team-ponball text-right">
                                                                        <h4><?php echo $match->teama->name_en; ?></h4>
                                                                    </div>
                                                                    <div class="pull-right">
                                                                        <ul class="status-lastMatch full-width ">
                                                                            <li><p>Last Match</p></li>
                                                                            <?php foreach (array_reverse(explode(",", $match->lastresult_home)) as $rs) { ?>
                                                                                <?php
                                                                                $rsclass = "draw-status";
                                                                                switch ($rs) {
                                                                                    case "w":
                                                                                        $rsclass = "win-status";
                                                                                        break;
                                                                                    case "l":
                                                                                        $rsclass = "lose-status";
                                                                                        break;
                                                                                    default:
                                                                                        $rsclass = "draw-status";
                                                                                        break;
                                                                                }

                                                                                ?>
                                                                                <li>
                                                                                    <div class="<?php echo $rsclass; ?>"></div>
                                                                                </li>
                                                                            <?php } ?>
                                                                        </ul>
                                                                        <ul class="status-lastMatch full-width odd ">
                                                                            <li><p>ODD</p></li>
                                                                            <?php foreach (array_reverse(explode(",", $match->lastresult_odds_home)) as $rs) { ?>
                                                                                <?php
                                                                                $rsclass = "draw-status";
                                                                                switch ($rs) {
                                                                                    case "w":
                                                                                        $rsclass = "win-status";
                                                                                        break;
                                                                                    case "l":
                                                                                        $rsclass = "lose-status";
                                                                                        break;
                                                                                    default:
                                                                                        $rsclass = "draw-status";
                                                                                        break;
                                                                                }

                                                                                ?>
                                                                                <li>
                                                                                    <div class="<?php echo $rsclass; ?>"></div>
                                                                                </li>
                                                                            <?php } ?>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div style="clear: both"></div>
                                                                <div class="full-width">

                                                                    <div class="bottom-detail-team-ponball inline-box">
                                                                        <div class="score-ct pull-left text-right beton-homep"
                                                                             mid="<?php echo $match->id; ?>">
                                                                            0%
                                                                        </div>
                                                                        <div class="progress pull-right">
                                                                            <div class="progress-bar pull-right beton-home"
                                                                                 mid="<?php echo $match->id; ?>"
                                                                                 role="progressbar"
                                                                                 aria-valuenow="0"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 0%;">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="btn-vote-bet inline-box active">
                                                                        <button type="button"
                                                                                class="btn btn-xs btn-primary">Vote
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="right-accordion-row pull-right">
                                                                <div class="logo-team-ponball">
                                                                    <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                </div>
                                                                <div class="detail-team-ponball">
                                                                    <div class="top-detail-team-ponball text-left">
                                                                        <h4><?php echo $match->teamb->name_en; ?></h4>
                                                                    </div>
                                                                    <div class="pull-left">
                                                                        <ul class="status-lastMatch full-width">
                                                                            <?php foreach (explode(",", $match->lastresult_away) as $rs) { ?>
                                                                                <?php
                                                                                $rsclass = "draw-status";
                                                                                switch ($rs) {
                                                                                    case "w":
                                                                                        $rsclass = "win-status";
                                                                                        break;
                                                                                    case "l":
                                                                                        $rsclass = "lose-status";
                                                                                        break;
                                                                                    default:
                                                                                        $rsclass = "draw-status";
                                                                                        break;
                                                                                }

                                                                                ?>
                                                                                <li>
                                                                                    <div class="<?php echo $rsclass; ?>"></div>
                                                                                </li>
                                                                            <?php } ?>
                                                                            <li><p>Last Match</p></li>
                                                                        </ul>
                                                                        <ul class="status-lastMatch full-width odd ">
                                                                            <?php foreach (explode(",", $match->lastresult_odds_away) as $rs) { ?>
                                                                                <?php
                                                                                $rsclass = "draw-status";
                                                                                switch ($rs) {
                                                                                    case "w":
                                                                                        $rsclass = "win-status";
                                                                                        break;
                                                                                    case "l":
                                                                                        $rsclass = "lose-status";
                                                                                        break;
                                                                                    default:
                                                                                        $rsclass = "draw-status";
                                                                                        break;
                                                                                }

                                                                                ?>
                                                                                <li>
                                                                                    <div class="<?php echo $rsclass; ?>"></div>
                                                                                </li>
                                                                            <?php } ?>
                                                                            <li><p>ODD</p></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="full-width">
                                                                    <div class="btn-vote-bet">
                                                                        <button type="button"
                                                                                class="btn btn-xs btn-success">Vote
                                                                        </button>
                                                                    </div>
                                                                    <div class="bottom-detail-team-ponball inline-box pull-right">
                                                                        <div class="progress pull-left">
                                                                            <div class="progress-bar progress-bar-danger beton-away"
                                                                                 mid="<?php echo $match->id; ?>"
                                                                                 role="progressbar"
                                                                                 aria-valuenow="0"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 0%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="score-ct pull-right beton-awayp"
                                                                             mid="<?php echo $match->id; ?>">
                                                                            0%
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div style="clear: both"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-body">

                                                    <!-- Nav tabs -->
                                                    <ul class="nav nav-tabs nav-tabs-highlight"
                                                        role="tablist">
                                                        <li class="<?php echo ($match->state != 17) ? "active" : ""; ?>"
                                                            role="presentation">
                                                            <a href="#<?php echo "livescore-tab" . $match->id; ?>"
                                                               role="tab"
                                                               data-toggle="tab">Detail</a>
                                                        </li>
                                                        <li role="presentation">
                                                            <a href="#stat" role="tab"
                                                               data-toggle="tab">Statistics</a>
                                                        </li>
                                                        <li role="presentation">
                                                            <a href="#h2h" role="tab" data-toggle="tab">Head
                                                                to Head</a>
                                                        </li>
                                                        <li role="presentation">
                                                            <a href="#formguide" role="tab"
                                                               data-toggle="tab">Form
                                                                Guide</a>
                                                        </li>
                                                        <li role="presentation">
                                                            <a href="#liveup" role="tab"
                                                               data-toggle="tab">Liveup</a>
                                                        </li>

                                                        <li class="<?php echo ($match->state == 17) ? "active" : ""; ?>"
                                                            role="presentation">
                                                            <a href="#<?php echo "review" . $match->id; ?>"
                                                               role="tab"
                                                               data-toggle="tab">ทีเด็ด-ทรรศนะ</a></li>
                                                    </ul>

                                                    <!-- Tab panes -->
                                                    <div class="tab-content">
                                                        <div role="tabpanel"
                                                             class="tab-pane <?php echo ($match->state != 17) ? "active" : ""; ?>"
                                                             id="<?php echo "livescore-tab" . $match->id; ?>">
                                                            <div class="content-detail-livescore">
                                                                <div class="full-width inline-box">
                                                                    <div class="header-event-match hide">
                                                                        <ul>
                                                                            <li><h3>EVENT MATCH</h3>
                                                                            </li>
                                                                            <li><span class="event-dp-button end-match"
                                                                                      mid="<?php echo $match->id; ?>">End Match</span>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="full-width inline-box">
                                                                        <div class="score-event-match hide">

                                                                            <div class="pull-right text-right">
                                                                                <p><?php echo $match->teama->name_en; ?></p>
                                                                                <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                            </div>
                                                                            <h4><span class="live-h-score"
                                                                                      mid="<?php echo $match->id; ?>"><?php echo $match->live_score_home; ?></span>-<span
                                                                                        class="live-a-score"
                                                                                        mid="<?php echo $match->id; ?>"><?php echo $match->live_score_away; ?></span>
                                                                            </h4>
                                                                            <div class="pull-left text-left">
                                                                                <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                                <p><?php echo $match->teamb->name_en; ?></p>
                                                                            </div>

                                                                        </div>
                                                                        <div class="content-event-match"
                                                                             mid="<?php echo $match->id; ?>">
                                                                            <?php
                                                                            //$matchevent = $mcon->getEvent($match->id);
                                                                            $matchevent = [];
                                                                            ?>
                                                                            <?php foreach ($matchevent as $event) { ?>
                                                                                <?php if ($event->side == "home") { ?>
                                                                                    <div class="full-width line-content-all">
                                                                                        <div class="col-home-live">
                                                                                            <div class="box-football-icon">
                                                                                                <div class="<?php echo $event->getEventImgClass(); ?>"></div>
                                                                                            </div>
                                                                                            <div class="min-live"><?php echo $event->at_minute . "'"; ?></div>
                                                                                            <div class="score-live"><?php echo $event->score; ?></div>
                                                                                            <?php if (empty($event->assist)) { ?>
                                                                                                <div class="name-live"><?php echo $event->player; ?></div>
                                                                                            <?php } else { ?>
                                                                                                <?php if ($event->event == "substutitions") { ?>
                                                                                                    <div class="name-live">
                                                                                                        <span class="substitute-player-out"></span>
                                                                                                        <p><?php echo $event->player; ?></p>
                                                                                                    </div>
                                                                                                    <div class="name-live">
                                                                                                        <span class="substitute-player-in"></span>
                                                                                                        <p><?php echo $event->assist; ?></p>
                                                                                                    </div>
                                                                                                <?php } else { ?>
                                                                                                    <div class="name-live"><?php echo $event->player . " (" . $event->assist . ")"; ?></div>
                                                                                                <?php } ?>
                                                                                            <?php } ?>
                                                                                        </div>
                                                                                    </div>
                                                                                <?php } else { ?>
                                                                                    <div class="full-width line-content-all">
                                                                                        <div class="col-away-live pull-right">
                                                                                            <?php if (empty($event->assist)) { ?>
                                                                                                <div class="name-live"><?php echo $event->player; ?></div>
                                                                                            <?php } else { ?>
                                                                                                <?php if ($event->event == "substutitions") { ?>
                                                                                                    <div class="name-live">
                                                                                                        <span class="substitute-player-in"></span>
                                                                                                        <p><?php echo $event->assist; ?></p>
                                                                                                    </div>
                                                                                                    <div class="name-live">
                                                                                                        <span class="substitute-player-out"></span>
                                                                                                        <p><?php echo $event->player; ?></p>
                                                                                                    </div>
                                                                                                <?php } else { ?>
                                                                                                    <div class="name-live"><?php echo "(" . $event->assist . ") " . $event->player; ?></div>
                                                                                                <?php } ?>
                                                                                            <?php } ?>
                                                                                            <div class="score-live"><?php echo $event->score; ?></div>
                                                                                            <div class="min-live"><?php echo $event->at_minute . "'"; ?></div>
                                                                                            <div class="box-football-icon">
                                                                                                <div class="<?php echo $event->getEventImgClass(); ?>"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                <?php } ?>
                                                                            <?php } ?>


                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div style="clear:both;"></div>
                                                            </div>

                                                        </div>
                                                        <div role="tabpanel" class="tab-pane" id="stat">
                                                            <div class="full-width inline-box">
                                                                <!-- Nav tabs -->
                                                                <ul class="nav nav-tabs hide"
                                                                    role="tablist">
                                                                    <li role="presentation"
                                                                        class="active"><a
                                                                                href="#<?php echo "statistics-tab" . $match->id; ?>"
                                                                                role="tab"
                                                                                data-toggle="tab">Statistics</a>
                                                                    </li>
                                                                    <li role="presentation"><a
                                                                                href="#<?php echo "lineUp-tab" . $match->id; ?>"
                                                                                role="tab"
                                                                                data-toggle="tab">Line
                                                                            Up</a>
                                                                    </li>

                                                                </ul>

                                                                <div id="<?php echo "statistics-tab" . $match->id; ?>">
                                                                    <div class="table statistics-table">
                                                                        <table>
                                                                            <thead>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="pull-right">
                                                                                        <h4><?php echo $match->teama->name_en; ?></h4>
                                                                                        <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                                    </div>
                                                                                </td>
                                                                                <td><h3>VS</h3>
                                                                                </td>
                                                                                <td>
                                                                                    <div class="pull-left">
                                                                                        <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                                        <h4><?php echo $match->teamb->name_en; ?></h4>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody class="statistics-mark"
                                                                                   mid="<?php echo $match->id; ?>">
                                                                            <?php
                                                                            // $statlist = $mcon->getStat($match->id);
                                                                            $statlist = [];
                                                                            ?>
                                                                            <?php foreach ($statlist as $stat) { ?>
                                                                                <?php $rmax = $stat->hval + $stat->aval; ?>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="num-progress <?php echo ($stat->hval > $stat->aval) ? "active" : ""; ?>"><?php echo $stat->hval; ?></div>
                                                                                        <div class="progress">
                                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                                 role="progressbar"
                                                                                                 aria-valuenow="<?php echo $stat->hval; ?>"
                                                                                                 aria-valuemin="0"
                                                                                                 aria-valuemax="<?php echo $rmax; ?>"
                                                                                                 style="width: <?php echo $stat->hval / $rmax * 100; ?>%;">
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td><?php echo $stat->type; ?></td>
                                                                                    <td>
                                                                                        <div class="progress">
                                                                                            <div class="progress-bar"
                                                                                                 role="progressbar"
                                                                                                 aria-valuenow="<?php echo $stat->aval; ?>"
                                                                                                 aria-valuemin="0"
                                                                                                 aria-valuemax="<?php echo $rmax; ?>"
                                                                                                 style="width: <?php echo $stat->aval / $rmax * 100; ?>%;">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="num-progress <?php echo ($stat->hval < $stat->aval) ? "active" : ""; ?>"><?php echo $stat->aval; ?></div>
                                                                                    </td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane" id="h2h">
                                                            <div class="content-h2h-match">
                                                                <div class="h2h-col inline-box full-width">
                                                                    <div class="h2h-state">
                                                                        <ul>
                                                                            <li>
                                                                                <img src="/images/logo-team/manchester.png">
                                                                            </li>
                                                                            <li>4 Win</li>
                                                                            <li>9 Draw</li>
                                                                            <li>1 Lost</li>
                                                                            <li>
                                                                                <img src="/images/logo-team/Chelsea.png">
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="box-match-list-title-last full-width">
                                                                        <div class="bh-ctn text-center full-width">

                                                                            <div class="form-inline-checkbox"
                                                                                 data-toggle="buttons">
                                                                                <label class="btn btn-xs btn-default active">
                                                                                    <input type="checkbox"
                                                                                           autocomplete="off"
                                                                                           checked="">
                                                                                    All
                                                                                </label>
                                                                                <label class="btn btn-xs btn-default">
                                                                                    <input type="checkbox"
                                                                                           autocomplete="off">
                                                                                    Premier League
                                                                                </label>
                                                                                <label class="btn btn-xs btn-default">
                                                                                    <input type="checkbox"
                                                                                           autocomplete="off">
                                                                                    FA Cup
                                                                                </label>
                                                                                <label class="btn btn-xs btn-default">
                                                                                    <input type="checkbox"
                                                                                           autocomplete="off">
                                                                                    UEFA Champions
                                                                                    League
                                                                                </label>
                                                                                <label class="btn btn-xs btn-default">
                                                                                    <input type="checkbox"
                                                                                           autocomplete="off">
                                                                                    EFL Cup
                                                                                </label>


                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div style="clear: both;"></div>

                                                                    <div class="full-width inline-box">
                                                                        <div class="box-table-result-state blurred-box">
                                                                            <div class="full-width inline-box">
                                                                                <div class="box-match-tournament">
                                                                                    <div class="box-match-list-topic">
                                                                                        <div class="pull-left">
                                                                                            <img src="/images/logo-team/fa-cup-2017.png">
                                                                                            <p>FA
                                                                                                Cup</p>
                                                                                        </div>
                                                                                        <ul class="inline-box pull-right">
                                                                                            <li>Odds
                                                                                            </li>
                                                                                            <li>
                                                                                                Resualt
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="box-match-list">
                                                                                        <div class="text-center">
                                                                                            <div class="date-match">
                                                                                                15
                                                                                                Apr 18
                                                                                            </div>
                                                                                            <div class="time-match">
                                                                                                15:00
                                                                                            </div>
                                                                                        </div>
                                                                                        <div>
                                                                                            <div class="home-name-match team-bet-color">
                                                                                                <img src="/images/logo-team/Chelsea.png">
                                                                                                <span>Chelsea</span>
                                                                                            </div>
                                                                                            <div class="away-name-match hignlight-team-win">
                                                                                                <img src="/images/logo-team/manchester.png">
                                                                                                <span>Manchester united</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="match-score text-center">
                                                                                            <div class="home-score-match">
                                                                                                0
                                                                                            </div>
                                                                                            <div class="away-score-match">
                                                                                                2
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="text-center">
                                                                                            <div class="odds-match">
                                                                                                <div class="win-status"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="text-center">
                                                                                            <div class="result-match">
                                                                                                <div class="lose-status"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="box-match-list">

                                                                                        <div class="text-center">
                                                                                            <div class="date-match">
                                                                                                15
                                                                                                Apr 18
                                                                                            </div>
                                                                                            <div class="time-match">
                                                                                                15:00
                                                                                            </div>
                                                                                        </div>
                                                                                        <div>
                                                                                            <div class="home-name-match hignlight-team-win">
                                                                                                <img src="/images/logo-team/Chelsea.png">
                                                                                                <span>Chelsea</span>
                                                                                            </div>
                                                                                            <div class="away-name-match team-bet-color">
                                                                                                <img src="/images/logo-team/manchester.png">
                                                                                                <span>Manchester united</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="match-score text-center">
                                                                                            <div class="home-score-match">
                                                                                                0
                                                                                            </div>
                                                                                            <div class="away-score-match">
                                                                                                2
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="text-center">
                                                                                            <div class="odds-match">
                                                                                                <div class="draw-status"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="text-center">
                                                                                            <div class="result-match">
                                                                                                <div class="win-status"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="box-match-tournament">
                                                                                    <div class="box-match-list-topic">
                                                                                        <div class="pull-left">
                                                                                            <img src="/images/five-leagues/premier-league-logo.png">
                                                                                            <p>Premier
                                                                                                League</p>
                                                                                        </div>
                                                                                        <div class="pull-right">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    Odds
                                                                                                </li>
                                                                                                <li>
                                                                                                    Resualt
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="box-match-list">
                                                                                        <div class="text-center">
                                                                                            <div class="date-match">
                                                                                                15
                                                                                                Apr 18
                                                                                            </div>
                                                                                            <div class="time-match">
                                                                                                15:00
                                                                                            </div>
                                                                                        </div>
                                                                                        <div>
                                                                                            <div class="home-name-match team-bet-color">
                                                                                                <img src="/images/logo-team/Chelsea.png">
                                                                                                <span>Chelsea</span>
                                                                                            </div>
                                                                                            <div class="away-name-match hignlight-team-win">
                                                                                                <img src="/images/logo-team/manchester.png">
                                                                                                <span>Manchester united</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="match-score text-center">
                                                                                            <div class="home-score-match">
                                                                                                0
                                                                                            </div>
                                                                                            <div class="away-score-match">
                                                                                                2
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="text-center">
                                                                                            <div class="odds-match">
                                                                                                <div class="win-status"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="text-center">
                                                                                            <div class="result-match">
                                                                                                <div class="lose-status"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="box-match-list">

                                                                                        <div class="text-center">
                                                                                            <div class="date-match">
                                                                                                15
                                                                                                Apr 18
                                                                                            </div>
                                                                                            <div class="time-match">
                                                                                                15:00
                                                                                            </div>
                                                                                        </div>
                                                                                        <div>
                                                                                            <div class="home-name-match hignlight-team-win">
                                                                                                <img src="/images/logo-team/Chelsea.png">
                                                                                                <span>Chelsea</span>
                                                                                            </div>
                                                                                            <div class="away-name-match team-bet-color">
                                                                                                <img src="/images/logo-team/manchester.png">
                                                                                                <span>Manchester united</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="match-score text-center">
                                                                                            <div class="home-score-match">
                                                                                                0
                                                                                            </div>
                                                                                            <div class="away-score-match">
                                                                                                2
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="text-center">
                                                                                            <div class="odds-match">
                                                                                                <div class="draw-status"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="text-center">
                                                                                            <div class="result-match">
                                                                                                <div class="win-status"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="box-match-tournament">
                                                                                    <div class="box-match-list-topic">
                                                                                        <div class="pull-left">
                                                                                            <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                                                                            <p>UEFA
                                                                                                Champions
                                                                                                League</p>
                                                                                        </div>
                                                                                        <div class="pull-right">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    Odds
                                                                                                </li>
                                                                                                <li>
                                                                                                    Resualt
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="box-match-list">
                                                                                        <div class="text-center">
                                                                                            <div class="date-match">
                                                                                                15
                                                                                                Apr 18
                                                                                            </div>
                                                                                            <div class="time-match">
                                                                                                15:00
                                                                                            </div>
                                                                                        </div>
                                                                                        <div>
                                                                                            <div class="home-name-match team-bet-color">
                                                                                                <img src="/images/logo-team/Chelsea.png">
                                                                                                <span>Chelsea</span>
                                                                                            </div>
                                                                                            <div class="away-name-match hignlight-team-win">
                                                                                                <img src="/images/logo-team/manchester.png">
                                                                                                <span>Manchester united</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="match-score text-center">
                                                                                            <div class="home-score-match">
                                                                                                0
                                                                                            </div>
                                                                                            <div class="away-score-match">
                                                                                                2
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="text-center">
                                                                                            <div class="odds-match">
                                                                                                <div class="win-status"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="text-center">
                                                                                            <div class="result-match">
                                                                                                <div class="lose-status"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="box-match-list">

                                                                                        <div class="text-center">
                                                                                            <div class="date-match">
                                                                                                15
                                                                                                Apr 18
                                                                                            </div>
                                                                                            <div class="time-match">
                                                                                                15:00
                                                                                            </div>
                                                                                        </div>
                                                                                        <div>
                                                                                            <div class="home-name-match hignlight-team-win">
                                                                                                <img src="/images/logo-team/Chelsea.png">
                                                                                                <span>Chelsea</span>
                                                                                            </div>
                                                                                            <div class="away-name-match team-bet-color">
                                                                                                <img src="/images/logo-team/manchester.png">
                                                                                                <span>Manchester united</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="match-score text-center">
                                                                                            <div class="home-score-match">
                                                                                                0
                                                                                            </div>
                                                                                            <div class="away-score-match">
                                                                                                2
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="text-center">
                                                                                            <div class="odds-match">
                                                                                                <div class="draw-status"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="text-center">
                                                                                            <div class="result-match">
                                                                                                <div class="win-status"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane"
                                                             id="formguide">
                                                            <div class="content-statistics-match">
                                                                <div class="fg-col full-width">
                                                                    <div class="full-width inline-box">

                                                                        <div class="bt-flex">
                                                                            <div class="box-table-result-state">
                                                                                <div class="box-match-list-title-last full-width">
                                                                                    <div class="bh-ctn text-center bt-flex">
                                                                                        <div class="pull-left">
                                                                                            <img src="/images/logo-team/manchester.png">
                                                                                        </div>
                                                                                        <div class="pull-left form-inline-checkbox"
                                                                                             data-toggle="buttons">
                                                                                            <label class="btn btn-xs btn-default active">
                                                                                                <input type="checkbox"
                                                                                                       autocomplete="off"
                                                                                                       checked="">
                                                                                                All
                                                                                            </label>
                                                                                            <label class="btn btn-xs btn-default">
                                                                                                <input type="checkbox"
                                                                                                       autocomplete="off">
                                                                                                Premier
                                                                                                League
                                                                                            </label>
                                                                                            <label class="btn btn-xs btn-default">
                                                                                                <input type="checkbox"
                                                                                                       autocomplete="off">
                                                                                                FA Cup
                                                                                            </label>
                                                                                            <label class="btn btn-xs btn-default">
                                                                                                <input type="checkbox"
                                                                                                       autocomplete="off">
                                                                                                EFL Cup
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="full-width inline-box">
                                                                                    <div class="box-match-tournament">
                                                                                        <div class="box-match-list-topic">
                                                                                            <div class="pull-left">
                                                                                                <img src="/images/logo-team/fa-cup-2017.png">
                                                                                                <p>FA
                                                                                                    Cup</p>
                                                                                            </div>
                                                                                            <div class="pull-right">
                                                                                                <ul>
                                                                                                    <li>
                                                                                                        O
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        R
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="box-match-list">

                                                                                            <div class="text-center">
                                                                                                <div class="date-match">
                                                                                                    15
                                                                                                    Apr
                                                                                                    18
                                                                                                </div>
                                                                                            </div>
                                                                                            <div>
                                                                                                <div class="home-name-match team-bet-color">
                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                    <span>Chelsea</span>
                                                                                                </div>
                                                                                                <div class="away-name-match hignlight-team-win">
                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                    <span>Manchester United</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="match-score text-center">
                                                                                                <div class="home-score-match">
                                                                                                    0
                                                                                                </div>
                                                                                                <div class="away-score-match">
                                                                                                    2
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="odds-match">
                                                                                                    <div class="win-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="result-match">
                                                                                                    <div class="lose-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="box-match-list">

                                                                                            <div class="text-center">
                                                                                                <div class="date-match">
                                                                                                    15
                                                                                                    Apr
                                                                                                    18
                                                                                                </div>
                                                                                            </div>
                                                                                            <div>
                                                                                                <div class="home-name-match team-bet-color">
                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                    <span>Chelsea</span>
                                                                                                </div>
                                                                                                <div class="away-name-match hignlight-team-win">
                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                    <span>Manchester United</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="match-score text-center">
                                                                                                <div class="home-score-match">
                                                                                                    0
                                                                                                </div>
                                                                                                <div class="away-score-match">
                                                                                                    2
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="odds-match">
                                                                                                    <div class="win-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="result-match">
                                                                                                    <div class="lose-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="box-match-tournament">
                                                                                        <div class="box-match-list-topic">
                                                                                            <div class="pull-left">
                                                                                                <img src="/images/five-leagues/premier-league-logo.png">
                                                                                                <p>
                                                                                                    Premier
                                                                                                    League</p>
                                                                                            </div>
                                                                                            <div class="pull-right">
                                                                                                <ul>
                                                                                                    <li>
                                                                                                        O
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        R
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="box-match-list">
                                                                                            <div class="text-center">
                                                                                                <div class="date-match">
                                                                                                    15
                                                                                                    Apr
                                                                                                    18
                                                                                                </div>
                                                                                            </div>
                                                                                            <div>
                                                                                                <div class="home-name-match highlight-team-win">
                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                    <span>Chelsea</span>
                                                                                                </div>
                                                                                                <div class="away-name-match">
                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                    <span>Manchester United</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="match-score text-center">
                                                                                                <div class="home-score-match">
                                                                                                    0
                                                                                                </div>
                                                                                                <div class="away-score-match">
                                                                                                    2
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="odds-match">
                                                                                                    <div class="win-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="result-match">
                                                                                                    <div class="lose-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="box-match-list">

                                                                                            <div class="text-center">
                                                                                                <div class="date-match">
                                                                                                    15
                                                                                                    Apr
                                                                                                    18
                                                                                                </div>
                                                                                            </div>
                                                                                            <div>
                                                                                                <div class="home-name-match">
                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                    <span>Chelsea</span>
                                                                                                </div>
                                                                                                <div class="away-name-match highlight-team-win">
                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                    <span>Manchester United</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="match-score text-center">
                                                                                                <div class="home-score-match">
                                                                                                    0
                                                                                                </div>
                                                                                                <div class="away-score-match">
                                                                                                    2
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="odds-match">
                                                                                                    <div class="win-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="result-match">
                                                                                                    <div class="lose-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="box-match-tournament">
                                                                                        <div class="box-match-list-topic">
                                                                                            <div class="pull-left">
                                                                                                <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                                                                                <p>UEFA
                                                                                                    Champions
                                                                                                    League</p>
                                                                                            </div>
                                                                                            <div class="pull-right">
                                                                                                <ul>
                                                                                                    <li>
                                                                                                        O
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        R
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="box-match-list">

                                                                                            <div class="text-center">
                                                                                                <div class="date-match">
                                                                                                    15
                                                                                                    Apr
                                                                                                    18
                                                                                                </div>
                                                                                            </div>
                                                                                            <div>
                                                                                                <div class="home-name-match">
                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                    <span>Chelsea</span>
                                                                                                </div>
                                                                                                <div class="away-name-match hignlight-team-win">
                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                    <span>Manchester United</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="match-score text-center">
                                                                                                <div class="home-score-match">
                                                                                                    0
                                                                                                </div>
                                                                                                <div class="away-score-match">
                                                                                                    2
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="odds-match">
                                                                                                    <div class="win-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="result-match">
                                                                                                    <div class="lose-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="box-match-list">

                                                                                            <div class="text-center">
                                                                                                <div class="date-match">
                                                                                                    15
                                                                                                    Apr
                                                                                                    18
                                                                                                </div>
                                                                                            </div>
                                                                                            <div>
                                                                                                <div class="home-name-match">
                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                    <span>Chelsea</span>
                                                                                                </div>
                                                                                                <div class="away-name-match highlight-team-win">
                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                    <span>Manchester United</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="match-score text-center">
                                                                                                <div class="home-score-match">
                                                                                                    0
                                                                                                </div>
                                                                                                <div class="away-score-match">
                                                                                                    2
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="odds-match">
                                                                                                    <div class="win-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="result-match">
                                                                                                    <div class="lose-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="box-table-result-state">
                                                                                <div class="box-match-list-title-last full-width">
                                                                                    <div class="bh-ctn text-center bt-flex">
                                                                                        <div class="pull-left">
                                                                                            <img src="/images/logo-team/Chelsea.png">
                                                                                        </div>
                                                                                        <div class="pull-left form-inline-checkbox"
                                                                                             data-toggle="buttons">
                                                                                            <label class="btn btn-xs btn-default active">
                                                                                                <input type="checkbox"
                                                                                                       autocomplete="off"
                                                                                                       checked="">
                                                                                                All
                                                                                            </label>
                                                                                            <label class="btn btn-xs btn-default">
                                                                                                <input type="checkbox"
                                                                                                       autocomplete="off">
                                                                                                Premier
                                                                                                League
                                                                                            </label>
                                                                                            <label class="btn btn-xs btn-default">
                                                                                                <input type="checkbox"
                                                                                                       autocomplete="off">
                                                                                                FA Cup
                                                                                            </label>
                                                                                            <label class="btn btn-xs btn-default">
                                                                                                <input type="checkbox"
                                                                                                       autocomplete="off">
                                                                                                UEFA
                                                                                                Champions
                                                                                                League
                                                                                            </label>
                                                                                            <label class="btn btn-xs btn-default">
                                                                                                <input type="checkbox"
                                                                                                       autocomplete="off">
                                                                                                EFL Cup
                                                                                            </label>


                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="full-width inline-box">
                                                                                    <div class="box-match-tournament">
                                                                                        <div class="box-match-list-topic">
                                                                                            <div class="pull-left">
                                                                                                <img src="/images/logo-team/fa-cup-2017.png">
                                                                                                <p>FA
                                                                                                    Cup</p>
                                                                                            </div>
                                                                                            <div class="pull-right">
                                                                                                <ul>
                                                                                                    <li>
                                                                                                        O
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        R
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="box-match-list">

                                                                                            <div class="text-center">
                                                                                                <div class="date-match">
                                                                                                    15
                                                                                                    Apr
                                                                                                    18
                                                                                                </div>
                                                                                            </div>
                                                                                            <div>
                                                                                                <div class="home-name-match team-bet-color">
                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                    <span>Chelsea</span>
                                                                                                </div>
                                                                                                <div class="away-name-match hignlight-team-win">
                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                    <span>Manchester United</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="match-score text-center">
                                                                                                <div class="home-score-match">
                                                                                                    0
                                                                                                </div>
                                                                                                <div class="away-score-match">
                                                                                                    2
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="odds-match">
                                                                                                    <div class="win-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="result-match">
                                                                                                    <div class="lose-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="box-match-list">

                                                                                            <div class="text-center">
                                                                                                <div class="date-match">
                                                                                                    15
                                                                                                    Apr
                                                                                                    18
                                                                                                </div>
                                                                                            </div>
                                                                                            <div>
                                                                                                <div class="home-name-match team-bet-color">
                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                    <span>Chelsea</span>
                                                                                                </div>
                                                                                                <div class="away-name-match hignlight-team-win">
                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                    <span>Manchester United</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="match-score text-center">
                                                                                                <div class="home-score-match">
                                                                                                    0
                                                                                                </div>
                                                                                                <div class="away-score-match">
                                                                                                    2
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="odds-match">
                                                                                                    <div class="win-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="result-match">
                                                                                                    <div class="lose-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="box-match-tournament">
                                                                                        <div class="box-match-list-topic">
                                                                                            <div class="pull-left">
                                                                                                <img src="/images/five-leagues/premier-league-logo.png">
                                                                                                <p>
                                                                                                    Premier
                                                                                                    League</p>
                                                                                            </div>
                                                                                            <div class="pull-right">
                                                                                                <ul>
                                                                                                    <li>
                                                                                                        O
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        R
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="box-match-list">
                                                                                            <div class="text-center">
                                                                                                <div class="date-match">
                                                                                                    15
                                                                                                    Apr
                                                                                                    18
                                                                                                </div>
                                                                                            </div>
                                                                                            <div>
                                                                                                <div class="home-name-match highlight-team-win">
                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                    <span>Chelsea</span>
                                                                                                </div>
                                                                                                <div class="away-name-match">
                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                    <span>Manchester United</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="match-score text-center">
                                                                                                <div class="home-score-match">
                                                                                                    0
                                                                                                </div>
                                                                                                <div class="away-score-match">
                                                                                                    2
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="odds-match">
                                                                                                    <div class="win-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="result-match">
                                                                                                    <div class="lose-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="box-match-list">

                                                                                            <div class="text-center">
                                                                                                <div class="date-match">
                                                                                                    15
                                                                                                    Apr
                                                                                                    18
                                                                                                </div>
                                                                                            </div>
                                                                                            <div>
                                                                                                <div class="home-name-match">
                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                    <span>Chelsea</span>
                                                                                                </div>
                                                                                                <div class="away-name-match highlight-team-win">
                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                    <span>Manchester United</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="match-score text-center">
                                                                                                <div class="home-score-match">
                                                                                                    0
                                                                                                </div>
                                                                                                <div class="away-score-match">
                                                                                                    2
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="odds-match">
                                                                                                    <div class="win-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="result-match">
                                                                                                    <div class="lose-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="box-match-tournament">
                                                                                        <div class="box-match-list-topic">
                                                                                            <div class="pull-left">
                                                                                                <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                                                                                <p>UEFA
                                                                                                    Champions
                                                                                                    League</p>
                                                                                            </div>
                                                                                            <div class="pull-right">
                                                                                                <ul>
                                                                                                    <li>
                                                                                                        O
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        R
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="box-match-list">

                                                                                            <div class="text-center">
                                                                                                <div class="date-match">
                                                                                                    15
                                                                                                    Apr
                                                                                                    18
                                                                                                </div>
                                                                                            </div>
                                                                                            <div>
                                                                                                <div class="home-name-match">
                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                    <span>Chelsea</span>
                                                                                                </div>
                                                                                                <div class="away-name-match hignlight-team-win">
                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                    <span>Manchester United</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="match-score text-center">
                                                                                                <div class="home-score-match">
                                                                                                    0
                                                                                                </div>
                                                                                                <div class="away-score-match">
                                                                                                    2
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="odds-match">
                                                                                                    <div class="win-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="result-match">
                                                                                                    <div class="lose-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="box-match-list">

                                                                                            <div class="text-center">
                                                                                                <div class="date-match">
                                                                                                    15
                                                                                                    Apr
                                                                                                    18
                                                                                                </div>
                                                                                            </div>
                                                                                            <div>
                                                                                                <div class="home-name-match">
                                                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                                                    <span>Chelsea</span>
                                                                                                </div>
                                                                                                <div class="away-name-match highlight-team-win">
                                                                                                    <img src="/images/logo-team/manchester.png">
                                                                                                    <span>Manchester United</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="match-score text-center">
                                                                                                <div class="home-score-match">
                                                                                                    0
                                                                                                </div>
                                                                                                <div class="away-score-match">
                                                                                                    2
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="odds-match">
                                                                                                    <div class="win-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="text-center">
                                                                                                <div class="result-match">
                                                                                                    <div class="lose-status"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane"
                                                             id="liveup">
                                                            <div class="hide"
                                                                 id="<?php echo "lineUp-tab" . $match->id; ?>">
                                                                <div class="lineUp-table">
                                                                    <table class="table">
                                                                        <tr>
                                                                            <td>
                                                                                <div class="pull-right full-width text-right hide">
                                                                                    <span><?php echo $match->teama->name_en; ?></span>
                                                                                    <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                                </div>
                                                                                <div class="pull-right full-width">
                                                                                    <div class="content-lineUp-home">
                                                                                        <ul>
                                                                                            <?php
                                                                                            $lineups = $mcon->getLineups($match->id . "");
                                                                                            foreach ($lineups as $lineup) {
                                                                                                if ($lineup->side == "home") {
                                                                                                    ?>
                                                                                                    <li>
                                                                                                        <div class="num-player"><?php echo $lineup->number; ?></div>
                                                                                                        <div><?php echo $lineup->name; ?></div>
                                                                                                        <div><?php echo $lineup->position; ?></div>
                                                                                                    </li>
                                                                                                <?php } ?>
                                                                                            <?php } ?>

                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="pull-left hide">
                                                                                    <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                                    <span><?php echo $match->teamb->name_en; ?></span>
                                                                                </div>
                                                                                <div class="pull-left full-width">
                                                                                    <div class="content-lineUp-away">
                                                                                        <ul>
                                                                                            <?php
                                                                                            $lineups = $mcon->getLineups($match->id . "");
                                                                                            foreach ($lineups as $lineup) {
                                                                                                if ($lineup->side == "away") {
                                                                                                    ?>
                                                                                                    <li>
                                                                                                        <div class="num-player"><?php echo $lineup->number; ?></div>
                                                                                                        <div><?php echo $lineup->name; ?></div>
                                                                                                        <div><?php echo $lineup->position; ?></div>
                                                                                                    </li>
                                                                                                <?php } ?>
                                                                                            <?php } ?>

                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>


                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <?php include 'mockup/formations-linups.php' ?>
                                                        </div>
                                                        <div role="tabpanel"
                                                             class="tab-pane <?php echo ($match->state == 17) ? "active" : ""; ?>"
                                                             id="<?php echo "review" . $match->id; ?>">


                                                            <div class="content-review-simple">

                                                                <div class="content-vote-review full-width inline-box text-center">
                                                                    <div class="box-vote-win">

                                                                        <div class="title-center">
                                                                            โหวดผลการแข่งขัน
                                                                        </div>

                                                                        <div class="box-vote-win-btn inline-box full-width">

                                                                            <div class="btn-group full-width"
                                                                                 role="group"
                                                                                 aria-label="..."
                                                                                 style="width: 180px;">
                                                                                <button type="button"
                                                                                        class="btn btn-primary">
                                                                                    Home
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="btn btn-default">
                                                                                    Draw
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="btn btn-success">
                                                                                    Away
                                                                                </button>

                                                                            </div>
                                                                        </div>
                                                                        <div class="box-progress vote-win full-width inline-box text-center">
                                                                            <div class="progress">
                                                                                <div class="progress-bar progress-bar-primary"
                                                                                     style="width: 35%">
                                                                                    35%
                                                                                </div>
                                                                                <div class="progress-bar progress-bar-draw"
                                                                                     style="width: 20%">
                                                                                    20%
                                                                                </div>
                                                                                <div class="progress-bar progress-bar-success"
                                                                                     style="width: 45%">
                                                                                    45%
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="home-panel-body pull-left">
                                                                    <?php foreach ($match->reviews as $review) { ?>
                                                                        <?php if ($review->team == "home") { ?>
                                                                            <div class="content-home-review full-width pull-right">
                                                                                <div class="pull-left ct-winrate">
                                                                                    <div>
                                                                                        <h3><?php echo number_format($review->owner->accurate, 0) . "%"; ?></h3>
                                                                                        <small>Win
                                                                                            rate
                                                                                        </small>
                                                                                    </div>
                                                                                    <div>
                                                                                        <h3>30</h3>
                                                                                        <small>Matches
                                                                                        </small>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="pull-right">
                                                                                    <div class="area-logoName">
                                                                                        <div class="pull-right logo-review-user">
                                                                                            <img src="<?php echo $review->owner->path; ?>">
                                                                                        </div>
                                                                                        <div class="pull-right">
                                                                                            <h4><?php echo $review->owner->name; ?></h4>
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                                                </li>
                                                                                                <li><?php echo "@" . strtolower(str_replace(" ", "", $match->teama->name_en)); ?></li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="pull-right full-width text-review-content text-left">
                                                                                    <p>
                                                                                        <?php echo $review->review_text; ?>
                                                                                    </p>
                                                                                </div>

                                                                            </div>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="away-panel-body pull-right">

                                                                    <?php foreach ($match->reviews as $review) { ?>
                                                                        <?php if ($review->team == "away") { ?>
                                                                            <div class="content-away-review full-width pull-left">
                                                                                <div class="pull-right ct-winrate">
                                                                                    <div>
                                                                                        <h3><?php echo number_format($review->owner->accurate, 0) . "%"; ?></h3>
                                                                                        <small>Win
                                                                                            rate
                                                                                        </small>
                                                                                    </div>
                                                                                    <div>
                                                                                        <h3>30</h3>
                                                                                        <small>Matches
                                                                                        </small>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="pull-left">
                                                                                    <div class="area-logoName area-logoName-away">
                                                                                        <div class="pull-left logo-review-user">
                                                                                            <img src="<?php echo $review->owner->path; ?>">
                                                                                        </div>
                                                                                        <div class="pull-left">
                                                                                            <h4><?php echo $review->owner->name; ?></h4>
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                                                </li>
                                                                                                <li><?php echo "@" . strtolower(str_replace(" ", "", $match->teamb->name_en)); ?></li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="pull-left full-width text-review-content text-left">
                                                                                    <p>
                                                                                        <?php echo $review->review_text; ?>
                                                                                    </p>
                                                                                </div>

                                                                            </div>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </div>
                                                                <div style="clear: both;"></div>
                                                                <!--                                                                            <div class="full-width text-center">-->
                                                                <!--                                                                                <a class="btn btn-default btn-sm"-->
                                                                <!--                                                                                   href="/review-infomation?match_id=-->
                                                                <?php //echo $match->id; ?><!--"-->
                                                                <!--                                                                                   role="button">อ่านทีเด็ดทรรศนะทั้งหมด</a>-->
                                                                <!--                                                                            </div>-->
                                                            </div>
                                                        </div>

                                                        <div style="clear: both"></div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <?php } ?>
                                <?php } ?>


                            </div>
                        </div>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </div>

    </div>
</div>


<div class="area-banner-side-livescore">
    <div class="banner-side-livescore-fix" data-spy="affix" data-offset-top="60">
        <img src="/images/simple-ads120x600.jpg">
    </div>
</div>
<script>
    $(".nano").nanoScroller({scroll: 'top'});
    $(".nano-pane").css("display", "block");
    $(".nano-slider").css("display", "block");


    function toggleSidenav() {
        document.body.classList.toggle('sidenav-active');
    }


    $("#nav ul li a[href^='#']").on('click', function (e) {

        // prevent default anchor click behavior
        e.preventDefault();

        // store hash
        var hash = this.hash;

        // animate
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 1000, function () {

            // when done, add hash to url
            // (default click behaviour)
            window.location.hash = hash;
        });

    });
</script>
<script src="/js/live_move.js"></script>
</body>
</html>
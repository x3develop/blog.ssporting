
<div style="display:none;float: left;padding-top: 10px;width: 66px;padding-left: 5px;">
    <?php
    if(isset($_GET["newsid"])){
        require_once 'model/NewsM.php';
        $NewsObject = new NewsM();
        $dataNews = $NewsObject->getNewsById($_GET["newsid"]);
        if(!empty($dataNews)) {
            echo '<iframe
            src="https://platform.twitter.com/widgets/tweet_button.html?size=l
            &url=' . 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '
            &text=' . $dataNews[0]->titleTh . '"
            width="140"
            height="28"
            title="Twitter Tweet Button"
            style="border: 0; overflow: hidden;">
        </iframe>';
        }
    }elseif(isset($_GET["id"]) and $_SERVER['PHP_SELF']=="/articles-gallery.php"){
        require_once 'model/GalleryM.php';
        $GalleryObject=new GalleryM();
        $dataGallery=$GalleryObject->getByGalleryId($_GET['id']);
        echo '<iframe
            src="https://platform.twitter.com/widgets/tweet_button.html?size=l
            &url='.'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'
            &text='.$dataGallery[0]->title.'"
            width="140"
            height="28"
            title="Twitter Tweet Button"
            style="border: 0; overflow: hidden;">
        </iframe>';
    }elseif(isset($_GET["videoId"])){
        require_once 'model/VideoM.php';
        $VideoMObj = new VideoM();
        $dataVideo = $VideoMObj->getVideosById($_GET['videoId']);
            echo '<iframe
            src="https://platform.twitter.com/widgets/tweet_button.html?size=l
            &url='.'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'
            &text='.$dataVideo[0]->title.'"
            width="140"
            height="28"
            title="Twitter Tweet Button"
            style="border: 0; overflow: hidden;">
        </iframe>';
    }

    ?>
</div>

<div class="fb-share-button box-share" data-href="<?=(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"?>" data-layout="button" data-size="" data-mobile-iframe="true">
    <a class="fb-xfbml-parse-ignore" target="_blank" href="">
        แชร์
    </a>
</div>

<div class="box-share">
    <div class="g-plus" data-action="share" data-annotation="none" ></div>
</div>

<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<script>
    window.___gcfg = {
        lang: 'en-US',
        parsetags: 'onload'
    };
</script>
<script src="https://apis.google.com/js/platform.js" async defer></script>


<style>
    .navbar-menu-v2-fixed{
        position: fixed;
        z-index: 100;
        width: 100%;
        border-width:0px;
        /*top:0;*/
    }
    .container-fixed{
        height: 48px;
    }
</style>

<nav class="navbar navbar-menu-v2 navbar-menu-v2-fixed" id="navbarTopBar" style="">
    <div class="container container-fixed">
        <div class="nav-menutop nav-menu-main">
            <div class="col-sm-10">
                <ul class="nav nav-pills">
                    <li><a href="/"><img src="/images/logo/logo-ngoal.png" style="width: 80px; margin-top: -5px;"></a></li>
                    <li><a href="/">หน้าหลัก</a></li>
                    <li class="active-dropdown">
                        <a href="/highlight">วีดีโอ/ไฮไลท์บอล</a>
                        <span class="bx-menu-dropdown">
                                <table>
                                    <tr>
                                        <td><a href="/highlight?category=premier league"><i class="fa fa-play-circle-o" aria-hidden="true"></i> พรีเมียร์ลีก</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="/highlight?category=la liga"><i class="fa fa-play-circle-o" aria-hidden="true"></i> ลาลีกา</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="/highlight?category=bundesliga"><i class="fa fa-play-circle-o" aria-hidden="true"></i> บุนเดสลีกา</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="/highlight?category=serie a"><i class="fa fa-play-circle-o" aria-hidden="true"></i> ซีเรียอา</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="/highlight?category=league 1"><i class="fa fa-play-circle-o" aria-hidden="true"></i> ลีกเอิง</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="/highlight?category=thai"><i class="fa fa-play-circle-o" aria-hidden="true"></i> ไทยพรีเมียลีก</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="/highlight?category=ucl"><i class="fa fa-play-circle-o" aria-hidden="true"></i> ยูฟ่าแชมเปียนลีก</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="/highlight?category=europa"><i class="fa fa-play-circle-o" aria-hidden="true"></i> ยูโรป้า</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="/highlight?category=international"><i class="fa fa-play-circle-o" aria-hidden="true"></i> ฟุตบอลทีมชาติ</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="/highlight#videosOthersTab"><i class="fa fa-play-circle-o"
                                                                                     aria-hidden="true"></i> ไฮไลท์ Ngoal</a>
                                        </td>
                                    </tr>
                                </table>
                            </span>
                    </li>
                    <li class="active-dropdown">
                        <a href="/newshighlight?type=other&Page=1">ข่าวบอลต่างประเทศ</a>
                        <span class="bx-menu-dropdown">
                                <table>
                                    <tr>
                                        <td><a href="/newshighlight?type=premier league&Page=1"><i class="fa fa-newspaper-o" aria-hidden="true"></i> ข่าวพรีเมียร์ลีก</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="/news?type=la liga&Page=1"><i class="fa fa-newspaper-o" aria-hidden="true"></i> ข่าวลาลีกา</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="/news?type=bundesliga&Page=1"><i class="fa fa-newspaper-o" aria-hidden="true"></i> ข่าวบุนเดสลีกา</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="/news?type=serie a&Page=1"><i class="fa fa-newspaper-o" aria-hidden="true"></i> ข่าวซีเรียอา</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="/news?type=ucl&Page=1"><i class="fa fa-newspaper-o" aria-hidden="true"></i> ข่าวยูฟ่าแชมเปียนลีก</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="/news?type=europa&Page=1"><i class="fa fa-newspaper-o" aria-hidden="true"></i> ข่าวยูโรป้า</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="/news?type=thai&Page=1"><i class="fa fa-newspaper-o" aria-hidden="true"></i> ข่าวฟุตบอลทีมชาติ</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="/news?type=other league&Page=1"><i class="fa fa-newspaper-o" aria-hidden="true"></i> ข่าวฟุตบอลลีกอื่น</a></td>
                                    </tr>
                                </table>
                            </span>
                    </li>
                    <li><a href="/news?type=thai&Page=1">ข่าวบอลไทย</a></li>
                    <li><a href="/news?type=other&Page=1">ข่าวกีฬาอื่นๆ</a></li>
                </ul>
            </div>
            <div class="col-sm-2">
                <div class="section-login">
                    <a href="/games"><img style="height: 40px;" src="/images/banner-game.png"></a>
                </div>
            </div>
        </div>
    </div>
    <div style="background-color: #003a6c; width: 100%;padding: 0px 0px;">
        <div id="loadingBar" style="background: rgb(0, 188, 212); width: 0%;height: 2px; transition: transform .3s ease;"></div>
    </div>
</nav>


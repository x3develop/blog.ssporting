<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 11/24/2016
 * Time: 10:17
 */
//$foreignNewsObject=new news();
//england 43948,ger 44352,spain 45141,italy 45146,fr 43893,th 43105,cl 44042, eu 44094, wc 44465
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/HomeM.php";
$homem = new HomeM();
$foreignNewsPlData = $homem->getNewsByCategory('premier league', 7);
$thainews = $homem->getNewsByCategory('thai', 8);
$foreignNewsLiData = $homem->getNewsByCategory('la liga', 7);
$foreignNewsBlData = $homem->getNewsByCategory('bundesliga', 7);
$foreignNewsSaData = $homem->getNewsByCategory('serie a', 7);
$foreignNewsL1Data = $homem->getNewsByCategory('league 1', 7);
$foreignNewsUCLData = $homem->getNewsByCategory('ucl', 7);
$foreignNewsErpData = $homem->getNewsByCategory('europa', 7);
//
//$MatchTodayPl = $homem->getLeagueFixture(43948);
//$MatchTodayLl = $homem->getLeagueFixture(45141);//45141
//$MatchTodayBl = $homem->getLeagueFixture(44352);
//$MatchTodaySa = $homem->getLeagueFixture(45146);
//$MatchTodayL1 = $homem->getLeagueFixture(43893);
//$MatchTodayUCL = $homem->getLeagueFixture(44042);
//$MatchTodayErp = $homem->getLeagueFixture(44094);
//
//$rankPl = $homem->getTeamRank(43948);
//$rankLl = $homem->getTeamRank(45141);
//$rankBl = $homem->getTeamRank(44352);
//$rankSa = $homem->getTeamRank(45146);
//$rankL1 = $homem->getTeamRank(43893);
//$rankUCL = $homem->getTeamRank(44042);
//$rankErp = $homem->getTeamRank(44094);
//
//$topplayerPl = $homem->getGoalRanking(43948);
//$topplayerLl = $homem->getGoalRanking(45141);
//$topplayerBl = $homem->getGoalRanking(44352);
//$topplayerSa = $homem->getGoalRanking(45146);
//$topplayerL1 = $homem->getGoalRanking(43893);
//$topplayerUCL = $homem->getGoalRanking(44042);
//$topplayerErp = $homem->getGoalRanking(44094);
//var_dump($MatchTodayPl);
//exit;
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playLeague.php";
$playLeague = new playLeague();
//English Premier League
$league2 = $playLeague->checkTeamByLeague(1);
$goalScorer2 = $playLeague->checkGoalScorerByLeague(1);
$matchLast2 = $playLeague->checkMatchLastByLeague(1);

$league = $playLeague->checkTeamByLeague(590);
$goalScorer = $playLeague->checkGoalScorerByLeague(590);
$matchLast = $playLeague->checkMatchLastByLeague(590);

$leagueLi = $playLeague->checkTeamByLeague(18);
$goalScorerLi = $playLeague->checkGoalScorerByLeague(18);
$matchLastLi = $playLeague->checkMatchLastByLeague(18);

$leagueBl = $playLeague->checkTeamByLeague(44);
$goalScorerBl = $playLeague->checkGoalScorerByLeague(44);
$matchLastBl = $playLeague->checkMatchLastByLeague(44);

$leagueSa = $playLeague->checkTeamByLeague(27);
$goalScorerSa = $playLeague->checkGoalScorerByLeague(27);
$matchLastSa = $playLeague->checkMatchLastByLeague(27);

$leagueL1 = $playLeague->checkTeamByLeague(36);
$goalScorerL1 = $playLeague->checkGoalScorerByLeague(36);
$matchLastL1 = $playLeague->checkMatchLastByLeague(36);

//$leagueUCL = $playLeague->checkTeamByLeague(105);
$leagueUCL = $playLeague->checkTeamByChampionsLeague(105);
$goalScorerUCL = $playLeague->checkGoalScorerByLeague(105);
$matchLastUCL = $playLeague->checkMatchLastByLeague(105);

//$leagueErp = $playLeague->checkTeamByLeague(608);
$leagueErp = $playLeague->checkTeamByChampionsLeague(608);
$goalScorerErp = $playLeague->checkGoalScorerByLeague(608);
$matchLastErp = $playLeague->checkMatchLastByLeague(608);
?>
<style>
    ul.pages > li.active {
        background-color: rgba(0, 0, 0, 0.1);
    }
</style>
<div class="wrap-box-news-list">
    <div class="bt-backword slide-back hide">
        <div class="backwardNews"><i class="fa fa-chevron-left"></i></div>
    </div>
    <div class="bt-forward slide-next hide">
        <div class="forwardNews"><i class="fa fa-chevron-right"></i></div>
    </div>
    <div class="menu-foreignNews-index">
        <div class="container">
            <ul id="pages" class="nav nav-tabs" role="tablist">
                <li role="presentation" id="tab-pane-news-all" class="active">
                    <a aria-controls="home" role="tab" data-toggle="tab" class="backward tab-pane-news">
                        <div class="tab-h-content">
                            <img src="/images/five-leagues/UEFA-logo.png">
                            <span>ข่าวฟุตบอลต่างประเทศ 222</span>
                        </div>
                    </a>
                </li>
                <li role="presentation" id="tab-pane-news-thai">
                    <a aria-controls="profile" role="tab" data-toggle="tab" class="forward tab-pane-news">
                        <div class="tab-h-content">
                            <img src="/images/five-leagues/thai-logo.png">
                            <span>ข่าวฟุตบอลไทย</span>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!--        <ul id="pages"><li class="">1</li><li class="active">2</li><li class="">3</li><li>4</li><li>5</li><li>6</li></ul>-->
    <div class="content-foreignNews-index">


        <div class="container">
            <div id="div-foreignNews" style="width: 100%; overflow: hidden; height: 100%;">
                <ul id="ul-foreignNews" style="height: 1000px;overflow:hidden;margin:0;">
                    <li class="content-foreignNews-box">
                        <div class="wrap-box-news">
                            <div>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs nav-tabs-league" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#Bpl" aria-controls="home" role="tab" data-toggle="tab">
                                            <img src="/images/five-leagues/English-Premier-League.png">
                                            <span>Premier League</span></a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#Laliga" aria-controls="profile" role="tab" data-toggle="tab">
                                            <img src="/images/five-leagues/La-Liga-Logo-1.png"> <span>La Liga</span></a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#Bundesliga" aria-controls="messages" role="tab" data-toggle="tab">
                                            <img src="/images/five-leagues/logo_bl.gif">
                                            <span>Bundes Liga</span></a></li>
                                    <li role="presentation">
                                        <a href="#Seriea" aria-controls="settings" role="tab" data-toggle="tab">
                                            <img src="/images/five-leagues/seriea1.png"> <span>Serie A</span></a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#League1" aria-controls="settings" role="tab" data-toggle="tab">
                                            <img src="/images/five-leagues/Ligue-1-logo-france.png">
                                            <span>League 1</span></a></li>
                                    <li role="presentation">
                                        <a href="#UCL" aria-controls="settings" role="tab" data-toggle="tab">
                                            <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                            <span>UCL</span></a></li>
                                    <li role="presentation">
                                        <a href="#Europa" aria-controls="settings" role="tab" data-toggle="tab">
                                            <img src="/images/five-leagues/uropa.png"> <span>Europa</span></a></li>

                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane-league tab-pane active" id="Bpl">
                                        <div class="wrap-content-cols">


                                            <div class="col-md-12 wrap-box-results content-foreignNews-responsive">
                                                <div class="panel-group box-results" id="accordion" role="tablist"
                                                     aria-multiselectable="true">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab">
                                                            <h4 class="panel-title">
                                                                <a class="collapsed" data-toggle="collapse"
                                                                   href="#collapseExample" aria-expanded="false"
                                                                   aria-controls="collapseExample">
                                                                    ตารางคะแนน
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div class="panel-collapse collapse" id="collapseExample">
                                                            <div class="wrap-height">
                                                                <table class="table-point">
                                                                    <tr>
                                                                        <th></th>
                                                                        <th>Club</th>
                                                                        <th>Pl</th>
                                                                        <th>GD</th>
                                                                        <th>Pts</th>
                                                                    </tr>
                                                                    <?php if (!empty($league2)) { ?>
                                                                        <?php foreach ($league2 as $key => $rank) { ?>
                                                                            <tr>
                                                                                <td><?php echo $key + 1; ?></td>
                                                                                <td>
                                                                                    <img src="<?= ((!empty($rank["path"])) ? "" . $rank["path"] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                    <div class="name-cut"><?php echo $rank["name_en"]; ?></div>
                                                                                </td>
                                                                                <td><?php echo $rank["PI"]; ?></td>
                                                                                <td><?php echo $rank["GD"]; ?></td>
                                                                                <td><?php echo((!empty($rank["totalPts"]) ? $rank["totalPts"] : '0')); ?></td>
                                                                            </tr>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab">
                                                            <h4 class="panel-title">
                                                                <a class="collapsed" role="button"
                                                                   data-toggle="collapse"
                                                                   href="#collapseExample2" aria-expanded="false"
                                                                   aria-controls="collapseExample2">
                                                                    ดาวซัลโว
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div class="panel-collapse collapse" id="collapseExample2">
                                                            <div class="wrap-height">

                                                                <div class="bx-player-records">
                                                                    <!--                                                    <div class="h-records">Goals</div>-->
                                                                    <div class="bx-top-records">
                                                                        <?php if (!empty($goalScorer2)) { ?>
                                                                            <table>
                                                                                <?php if (!empty($goalScorer2)) { ?>
                                                                                    <tr>
                                                                                        <td></td>
                                                                                        <td class="text-right image-name-topScore">
                                                                                            <img src="<?= ((!empty($goalScorer2[0]['path'])) ? '' . $goalScorer2[0]['path'] : '/images/p1.png') ?>">
                                                                                        </td>
                                                                                        <td class="image-logo-topScore">
                                                                                            <img src="<?= $playLeague->checkPathTeamPlayer($goalScorer2[0]['id'], 1) ?>">
                                                                                        </td>

                                                                                        <td>
                                                                                            <div class="name-top-records"><?= $goalScorer2[0]['name']; ?></div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div class="txt-point"><?= $goalScorer2[0]['goal']; ?></div>
                                                                                        </td>
                                                                                    </tr>
                                                                                <?php } ?>
                                                                            </table>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <?php if (!empty($goalScorer2)) { ?>
                                                                        <table class="table-list-othertop">
                                                                            <?php foreach ($goalScorer2 as $key => $value) { ?>
                                                                                <?php if ($key != 0) { ?>
                                                                                    <tr>
                                                                                        <td><?php echo $key + 1; ?></td>
                                                                                        <td>
                                                                                            <img style="width: 25px;"
                                                                                                 src="<?= $playLeague->checkPathTeamPlayer($value['id'], 1) ?>">
                                                                                        </td>
                                                                                        <td>
                                                                                            <div class="name-list-othertop"><?php echo $value['name']; ?></div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <span><?php echo $value['goal']; ?></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </table>
                                                                    <?php } ?>
                                                                </div>


                                                                <div style="clear:both;"></div>


                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab">
                                                            <h4 class="panel-title">
                                                                <a class="collapsed" role="button"
                                                                   data-toggle="collapse"
                                                                   href="#collapseExample3" aria-expanded="false"
                                                                   aria-controls="collapseExample3">
                                                                    ตารางแข่งขัน
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div class="panel-collapse collapse" id="collapseExample3">
                                                            ...333333
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>


                                            <div class="wrap-box-results">
                                                <!--                                                ตารางคะแนน-->

                                                <div class="col-sm-4 col-md-4">


                                                    <div class="box-results">
                                                        <span class="title-box-results">ตารางคะแนน</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano  bx2">
                                                                <div class="content">
                                                                    <table class="table-point">
                                                                        <tr>
                                                                            <th></th>
                                                                            <th>Club</th>
                                                                            <th>Pl</th>
                                                                            <th>GD</th>
                                                                            <th>Pts</th>
                                                                        </tr>
                                                                        <?php if (!empty($league2)) { ?>
                                                                            <?php foreach ($league2 as $key => $rank) { ?>
                                                                                <tr>
                                                                                    <td><?php echo $key + 1; ?></td>
                                                                                    <td>
                                                                                        <img src="<?= ((!empty($rank["path"])) ? "" . $rank["path"] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                        <div class="name-cut"><?php echo $rank["name_en"]; ?></div>
                                                                                    </td>
                                                                                    <td><?php echo $rank["PI"]; ?></td>
                                                                                    <td><?php echo $rank["GD"]; ?></td>
                                                                                    <td><?php echo((!empty($rank["totalPts"]) ? $rank["totalPts"] : '0')); ?></td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                ดาวซัลโว-->
                                                <div class="col-sm-4 col-md-3">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ดาวซัลโว</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano bx2">
                                                                <div class="content">
                                                                    <div class="bx-player-records">
                                                                        <!--                                                    <div class="h-records">Goals</div>-->
                                                                        <div class="bx-top-records">
                                                                            <?php if (!empty($goalScorer2)) { ?>
                                                                                <table>
                                                                                    <?php if (!empty($goalScorer2)) { ?>
                                                                                        <tr>
                                                                                            <td></td>
                                                                                            <td class="text-right image-name-topScore">
                                                                                                <img src="<?= ((!empty($goalScorer2[0]['path'])) ? '' . $goalScorer2[0]['path'] : '/images/p1.png') ?>">
                                                                                            </td>
                                                                                            <td class="image-logo-topScore">
                                                                                                <img src="<?= $playLeague->checkPathTeamPlayer($goalScorer2[0]['id'], 1) ?>">
                                                                                            </td>

                                                                                            <td>
                                                                                                <div class="name-top-records"><?= $goalScorer2[0]['name']; ?></div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div class="txt-point"><?= $goalScorer2[0]['goal']; ?></div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                </table>
                                                                            <?php } ?>
                                                                        </div>
                                                                        <?php if (!empty($goalScorer2)) { ?>
                                                                            <table class="table-list-othertop">
                                                                                <?php foreach ($goalScorer2 as $key => $value) { ?>
                                                                                    <?php if ($key != 0) { ?>
                                                                                        <tr>
                                                                                            <td><?php echo $key + 1; ?></td>
                                                                                            <td>
                                                                                                <img style="width: 25px;"
                                                                                                     src="<?= $playLeague->checkPathTeamPlayer($value['id'], 1) ?>">
                                                                                            </td>
                                                                                            <td>
                                                                                                <div class="name-list-othertop"><?php echo $value['name']; ?></div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <span><?php echo $value['goal']; ?></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            </table>
                                                                        <?php } ?>
                                                                    </div>


                                                                    <div style="clear:both;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                ตารางแข่งขัน-->
                                                <div class="col-sm-4 col-md-5">
                                                    <div class="box-results box-results-tablet">
                                                        <span class="title-box-results">ตารางแข่งขัน</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano bx2">
                                                                <div class="content">
                                                                    <div>
                                                                        <?php if (!empty($matchLast2)) { ?>
                                                                            <?php $data = ""; ?>
                                                                            <?php foreach ($matchLast2 as $okey => $fixtures) { ?>
                                                                                <?php if ($data != date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60)))) { ?>
                                                                                    <?php if ($okey != 0) { ?>
                                                                                        </table>
                                                                                    <?php } ?>
                                                                                    <div class="tab-date-played">
                                                                                        <i class="fa fa-calendar"></i> <?php echo date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60))); ?>
                                                                                    </div>
                                                                                    <table class="table-live">
                                                                                    <?php $data = date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60))); ?>
                                                                                <?php } ?>
                                                                                <tr>
                                                                                    <td class="text-right">
                                                                                        <span><?php echo $fixtures['nameTeamHome']; ?></span>
                                                                                        <img src="<?= ((!empty($fixtures['pathTeamHome'])) ? "" . $fixtures['pathTeamHome'] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                    </td>
                                                                                    <td class="score-box-results">
                                                                                        <?php if ($fixtures['status'] == "fullTime") { ?>
                                                                                            <strong><?php echo $fixtures['home_end_time_score'] ?>
                                                                                                -<?php echo $fixtures['away_end_time_score'] ?></strong>
                                                                                        <?php } else { ?>
                                                                                            <strong><?php echo date('H:i', (strtotime($fixtures['time_match']) - (60 * 60))); ?></strong>
                                                                                        <?php } ?>
                                                                                    </td>
                                                                                    <td class="text-left">
                                                                                        <img src="<?= ((!empty($fixtures['pathTeamAway'])) ? "" . $fixtures['pathTeamAway'] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                        <span><?php echo $fixtures['nameTeamAway']; ?></span>
                                                                                    </td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                            </table>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style="clear:both;"></div>
                                            </div>
                                        </div>
                                        <div class="wrap-box-content-news">
                                            <div class="box-other-news">
                                                <div class="rows">
                                                    <?php for ($i = 0; $i < 3; $i++) { ?>
                                                        <?php if (array_key_exists($i, $foreignNewsPlData)) { ?>
                                                            <div class="col-sm-4"
                                                                 style="border: 0;margin-bottom: 0;">
                                                                <div class="thumbnail">
                                                                    <div class="tab-color"></div>
                                                                    <div class="times-content"><i
                                                                                class="fa fa-clock-o"></i>
                                                                        <time class="how-long"><?php echo $foreignNewsPlData[$i]->createDatetime; ?></time>
                                                                    </div>
                                                                    <div class="title-owl-carousel"><?php echo $foreignNewsPlData[$i]->titleTh; ?></div>
                                                                    <div class="box-videos img-other-news">
                                                                        <div class="hilight-cover"></div>
                                                                        <a href="/articles?newsid=<?php echo $foreignNewsPlData[$i]->newsid; ?>">
                                                                            <img src="<?php echo $foreignNewsPlData[$i]->imageLink; ?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <div style="clear: both;"></div>
                                                </div>
                                            </div>
                                            <div class="box-other-news">
                                                <div class="rows">
                                                    <?php for ($i = 3; $i < count($foreignNewsPlData); $i++) { ?>
                                                        <?php if (array_key_exists($i, $foreignNewsPlData)) { ?>
                                                            <div class="col-sm-3">
                                                                <div class="thumbnail">
                                                                    <div class="tab-color"></div>
                                                                    <div class="times-content"><i
                                                                                class="fa fa-clock-o"></i>
                                                                        <time
                                                                                class="how-long"><?php echo $foreignNewsPlData[$i]->createDatetime; ?></time>
                                                                    </div>
                                                                    <div
                                                                            class="title-owl-carousel"><?php echo $foreignNewsPlData[$i]->titleTh; ?></div>
                                                                    <div class="box-videos">
                                                                        <div class="hilight-cover"></div>
                                                                        <a href="/articles?newsid=<?php echo $foreignNewsPlData[$i]->newsid; ?>">
                                                                            <img src="<?php echo $foreignNewsPlData[$i]->imageLink; ?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" data-league="18" class="tab-pane-league tab-pane" id="Laliga">
                                        <div class="wrap-content-cols">
                                            <div class="wrap-box-results">
                                                <!--                                                ตารางคะแนน-->
                                                <div class="col-sm-4">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ตารางคะแนน</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano  bx2">
                                                                <div class="content">
                                                                    <table class="table-point">
                                                                        <tr>
                                                                            <th>Pos</th>
                                                                            <th>Club</th>
                                                                            <th>Pl</th>
                                                                            <th>GD</th>
                                                                            <th>Pts</th>
                                                                        </tr>
                                                                        <?php if (!empty($leagueLi)) { ?>
                                                                            <?php foreach ($leagueLi as $key => $rank) { ?>
                                                                                <tr>
                                                                                    <td><?php echo $key + 1; ?></td>
                                                                                    <td>
                                                                                        <img src="<?= ((!empty($rank["path"])) ? "" . $rank["path"] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                        <?php echo $rank["name_en"]; ?>
                                                                                    </td>
                                                                                    <td><?php echo $rank["PI"]; ?></td>
                                                                                    <td><?php echo $rank["GD"]; ?></td>
                                                                                    <td><?php echo((!empty($rank["totalPts"]) ? $rank["totalPts"] : '0')); ?></td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                ดาวซัลโว-->
                                                <div class="col-sm-3">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ดาวซัลโว</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano bx2">
                                                                <div class="content">
                                                                    <div class="bx-player-records">
                                                                        <!--                                                    <div class="h-records">Goals</div>-->
                                                                        <div class="bx-top-records">
                                                                            <?php if (!empty($goalScorerLi)) { ?>
                                                                                <table>
                                                                                    <?php if (!empty($goalScorerLi)) { ?>
                                                                                        <tr>
                                                                                            <td></td>
                                                                                            <td class="text-right image-name-topScore">
                                                                                                <img src="<?= ((!empty($goalScorerLi[0]['path'])) ? '' . $goalScorerLi[0]['path'] : '/images/p1.png') ?>">
                                                                                            </td>
                                                                                            <td class="image-logo-topScore">
                                                                                                <img src="<?= $playLeague->checkPathTeamPlayer($goalScorerLi[0]['id'], 18) ?>">
                                                                                            </td>

                                                                                            <td>
                                                                                                <div class="name-top-records"><?= $goalScorerLi[0]['name']; ?></div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div class="txt-point"><?= $goalScorerLi[0]['goal']; ?></div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                </table>
                                                                            <?php } ?>
                                                                        </div>
                                                                        <?php if (!empty($goalScorerLi)) { ?>
                                                                            <table class="table-list-othertop">
                                                                                <?php foreach ($goalScorerLi as $key => $value) { ?>
                                                                                    <?php if ($key != 0) { ?>
                                                                                        <tr>
                                                                                            <td><?php echo $key + 1; ?></td>
                                                                                            <td>
                                                                                                <img style="width: 25px;"
                                                                                                     src="<?= $playLeague->checkPathTeamPlayer($value['id'], 18) ?>">
                                                                                            </td>
                                                                                            <td>
                                                                                                <div class="name-list-othertop"><?php echo $value['name']; ?></div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <span><?php echo $value['goal']; ?></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            </table>
                                                                        <?php } ?>
                                                                    </div>


                                                                    <div style="clear:both;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                ตารางแข่งขัน-->
                                                <div class="col-sm-5">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ตารางแข่งขัน</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano bx2">
                                                                <div class="content">
                                                                    <div>
                                                                        <?php if (!empty($matchLastLi)) { ?>
                                                                            <?php $data = ""; ?>
                                                                            <?php foreach ($matchLastLi as $okey => $fixtures) { ?>
                                                                                <?php if ($data != date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60)))) { ?>
                                                                                    <?php if ($okey != 0) { ?>
                                                                                        </table>
                                                                                    <?php } ?>
                                                                                    <div class="tab-date-played">
                                                                                        <i class="fa fa-calendar"></i> <?php echo date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60))); ?>
                                                                                    </div>
                                                                                    <table class="table-live">
                                                                                    <?php $data = date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60))); ?>
                                                                                <?php } ?>
                                                                                <tr>
                                                                                    <td>
                                                                                        <?php echo $fixtures['nameTeamHome']; ?>
                                                                                        <img src="<?= ((!empty($fixtures['pathTeamHome'])) ? "" . $fixtures['pathTeamHome'] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                    </td>
                                                                                    <td style="width: 10%;">
                                                                                        <?php if ($fixtures['status'] == "fullTime") { ?>
                                                                                            <strong><?php echo $fixtures['home_end_time_score'] ?>
                                                                                                -<?php echo $fixtures['away_end_time_score'] ?></strong>
                                                                                        <?php } else { ?>
                                                                                            <strong><?php echo date('H:i', (strtotime($fixtures['time_match']) - (60 * 60))); ?></strong>
                                                                                        <?php } ?>
                                                                                    </td>
                                                                                    <td>
                                                                                        <img src="<?= ((!empty($fixtures['pathTeamAway'])) ? "" . $fixtures['pathTeamAway'] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                        <?php echo $fixtures['nameTeamAway']; ?>
                                                                                    </td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                            </table>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style="clear:both;"></div>
                                            </div>
                                        </div>
                                        <div class="wrap-box-content-news">
                                            <div class="box-other-news">
                                                <div class="rows">
                                                    <?php for ($i = 0; $i < 3; $i++) { ?>
                                                        <?php if (array_key_exists($i, $foreignNewsLiData)) { ?>
                                                            <div class="col-sm-4"
                                                                 style="border: 0px;margin-bottom: 0px;">
                                                                <div class="thumbnail">
                                                                    <div class="tab-color"></div>
                                                                    <div class="times-content"><i
                                                                                class="fa fa-clock-o"></i>
                                                                        <time class="how-long"><?php echo $foreignNewsLiData[$i]->createDatetime; ?></time>
                                                                    </div>
                                                                    <div class="title-owl-carousel"><?php echo $foreignNewsLiData[$i]->titleTh; ?></div>
                                                                    <div class="box-videos img-other-news">
                                                                        <div class="hilight-cover"></div>
                                                                        <a href="/articles?newsid=<?php echo $foreignNewsLiData[$i]->newsid; ?>">
                                                                            <img src="<?php echo $foreignNewsLiData[$i]->imageLink; ?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <div style="clear: both;"></div>
                                                </div>
                                            </div>
                                            <div class="box-other-news">
                                                <div class="rows">
                                                    <?php for ($i = 3; $i < count($foreignNewsLiData); $i++) { ?>
                                                        <?php if (array_key_exists($i, $foreignNewsLiData)) { ?>
                                                            <div class="col-sm-3">
                                                                <div class="thumbnail">
                                                                    <div class="tab-color"></div>
                                                                    <div class="times-content"><i
                                                                                class="fa fa-clock-o"></i>
                                                                        <time
                                                                                class="how-long"><?php echo $foreignNewsLiData[$i]->createDatetime; ?></time>
                                                                    </div>
                                                                    <div
                                                                            class="title-owl-carousel"><?php echo $foreignNewsLiData[$i]->titleTh; ?></div>
                                                                    <div class="box-videos">
                                                                        <div class="hilight-cover"></div>
                                                                        <a href="/articles?newsid=<?php echo $foreignNewsLiData[$i]->newsid; ?>">
                                                                            <img src="<?php echo $foreignNewsLiData[$i]->imageLink; ?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" data-league="44" class="tab-pane-league tab-pane"
                                         id="Bundesliga">
                                        <div class="wrap-content-cols">
                                            <div class="wrap-box-results">
                                                <!--                                                ตารางคะแนน-->
                                                <div class="col-sm-4">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ตารางคะแนน</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano  bx2">
                                                                <div class="content">
                                                                    <table class="table-point">
                                                                        <tr>
                                                                            <th>Pos</th>
                                                                            <th>Club</th>
                                                                            <th>Pl</th>
                                                                            <th>GD</th>
                                                                            <th>Pts</th>
                                                                        </tr>
                                                                        <?php if (!empty($leagueBl)) { ?>
                                                                            <?php foreach ($leagueBl as $key => $rank) { ?>
                                                                                <tr>
                                                                                    <td><?php echo $key + 1; ?></td>
                                                                                    <td>
                                                                                        <img src="<?= ((!empty($rank["path"])) ? "" . $rank["path"] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                        <?php echo $rank["name_en"]; ?>
                                                                                    </td>
                                                                                    <td><?php echo $rank["PI"]; ?></td>
                                                                                    <td><?php echo $rank["GD"]; ?></td>
                                                                                    <td><?php echo((!empty($rank["totalPts"]) ? $rank["totalPts"] : '0')); ?></td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                ดาวซัลโว-->
                                                <div class="col-sm-3">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ดาวซัลโว</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano bx2">
                                                                <div class="content">
                                                                    <div class="bx-player-records">
                                                                        <!--                                                    <div class="h-records">Goals</div>-->
                                                                        <div class="bx-top-records">
                                                                            <?php if (!empty($goalScorerBl)) { ?>
                                                                                <table>
                                                                                    <?php if (!empty($goalScorerBl)) { ?>
                                                                                        <tr>
                                                                                            <td></td>
                                                                                            <td class="text-right image-name-topScore">
                                                                                                <img src="<?= ((!empty($goalScorerBl[0]['path'])) ? '' . $goalScorerBl[0]['path'] : '/images/p1.png') ?>">
                                                                                            </td>
                                                                                            <td class="image-logo-topScore">
                                                                                                <img src="<?= $playLeague->checkPathTeamPlayer($goalScorerBl[0]['id'], 44) ?>">
                                                                                            </td>

                                                                                            <td>
                                                                                                <div class="name-top-records"><?= $goalScorerBl[0]['name']; ?></div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div class="txt-point"><?= $goalScorerBl[0]['goal']; ?></div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                </table>
                                                                            <?php } ?>
                                                                        </div>
                                                                        <?php if (!empty($goalScorerBl)) { ?>
                                                                            <table class="table-list-othertop">
                                                                                <?php foreach ($goalScorerBl as $key => $value) { ?>
                                                                                    <?php if ($key != 0) { ?>
                                                                                        <tr>
                                                                                            <td><?php echo $key + 1; ?></td>
                                                                                            <td>
                                                                                                <img style="width: 25px;"
                                                                                                     src="<?= $playLeague->checkPathTeamPlayer($value['id'], 44) ?>">
                                                                                            </td>
                                                                                            <td>
                                                                                                <div class="name-list-othertop"><?php echo $value['name']; ?></div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <span><?php echo $value['goal']; ?></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            </table>
                                                                        <?php } ?>
                                                                    </div>


                                                                    <div style="clear:both;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                ตารางแข่งขัน-->
                                                <div class="col-sm-5">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ตารางแข่งขัน</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano bx2">
                                                                <div class="content">
                                                                    <div>
                                                                        <?php if (!empty($matchLastBl)) { ?>
                                                                            <?php $data = ""; ?>
                                                                            <?php foreach ($matchLastBl as $okey => $fixtures) { ?>
                                                                                <?php if ($data != date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60)))) { ?>
                                                                                    <?php if ($okey != 0) { ?>
                                                                                        </table>
                                                                                    <?php } ?>
                                                                                    <div class="tab-date-played">
                                                                                        <i class="fa fa-calendar"></i> <?php echo date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60))); ?>
                                                                                    </div>
                                                                                    <table class="table-live">
                                                                                    <?php $data = date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60))); ?>
                                                                                <?php } ?>
                                                                                <tr>
                                                                                    <td>
                                                                                        <?php echo $fixtures['nameTeamHome']; ?>
                                                                                        <img src="<?= ((!empty($fixtures['pathTeamHome'])) ? "" . $fixtures['pathTeamHome'] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                    </td>
                                                                                    <td style="width: 10%;">
                                                                                        <?php if ($fixtures['status'] == "fullTime") { ?>
                                                                                            <strong><?php echo $fixtures['home_end_time_score'] ?>
                                                                                                -<?php echo $fixtures['away_end_time_score'] ?></strong>
                                                                                        <?php } else { ?>
                                                                                            <strong><?php echo date('H:i', (strtotime($fixtures['time_match']) - (60 * 60))); ?></strong>
                                                                                        <?php } ?>
                                                                                    </td>
                                                                                    <td>
                                                                                        <img src="<?= ((!empty($fixtures['pathTeamAway'])) ? "" . $fixtures['pathTeamAway'] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                        <?php echo $fixtures['nameTeamAway']; ?>
                                                                                    </td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                            </table>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style="clear:both;"></div>
                                            </div>
                                        </div>
                                        <div class="wrap-box-content-news">
                                            <div class="box-other-news">
                                                <div class="rows">
                                                    <?php for ($i = 0; $i < 3; $i++) { ?>
                                                        <?php if (array_key_exists($i, $foreignNewsBlData)) { ?>
                                                            <div class="col-sm-4"
                                                                 style="border: 0px;margin-bottom: 0px;">
                                                                <div class="thumbnail">
                                                                    <div class="tab-color"></div>
                                                                    <div class="times-content"><i
                                                                                class="fa fa-clock-o"></i>
                                                                        <time class="how-long"><?php echo $foreignNewsBlData[$i]->createDatetime; ?></time>
                                                                    </div>
                                                                    <div class="title-owl-carousel"><?php echo $foreignNewsBlData[$i]->titleTh; ?></div>
                                                                    <div class="box-videos img-other-news">
                                                                        <div class="hilight-cover"></div>
                                                                        <a href="/articles?newsid=<?php echo $foreignNewsBlData[$i]->newsid; ?>">
                                                                            <img src="<?php echo $foreignNewsBlData[$i]->imageLink; ?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <div style="clear: both;"></div>
                                                </div>
                                            </div>
                                            <div class="box-other-news">
                                                <div class="rows">
                                                    <?php for ($i = 3; $i < count($foreignNewsBlData); $i++) { ?>
                                                        <?php if (array_key_exists($i, $foreignNewsBlData)) { ?>
                                                            <div class="col-sm-3">
                                                                <div class="thumbnail">
                                                                    <div class="tab-color"></div>
                                                                    <div class="times-content"><i
                                                                                class="fa fa-clock-o"></i>
                                                                        <time
                                                                                class="how-long"><?php echo $foreignNewsBlData[$i]->createDatetime; ?></time>
                                                                    </div>
                                                                    <div
                                                                            class="title-owl-carousel"><?php echo $foreignNewsBlData[$i]->titleTh; ?></div>
                                                                    <div class="box-videos">
                                                                        <div class="hilight-cover"></div>
                                                                        <a href="/articles?newsid=<?php echo $foreignNewsBlData[$i]->newsid; ?>">
                                                                            <img src="<?php echo $foreignNewsBlData[$i]->imageLink; ?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" data-league="27" class="tab-pane-league tab-pane" id="Seriea">
                                        <div class="wrap-content-cols">
                                            <div class="wrap-box-results">
                                                <!--                                                ตารางคะแนน-->
                                                <div class="col-sm-4">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ตารางคะแนน</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano  bx2">
                                                                <div class="content">
                                                                    <table class="table-point">
                                                                        <tr>
                                                                            <th>Pos</th>
                                                                            <th>Club</th>
                                                                            <th>Pl</th>
                                                                            <th>GD</th>
                                                                            <th>Pts</th>
                                                                        </tr>
                                                                        <?php if (!empty($leagueSa)) { ?>
                                                                            <?php foreach ($leagueSa as $key => $rank) { ?>
                                                                                <tr>
                                                                                    <td><?php echo $key + 1; ?></td>
                                                                                    <td>
                                                                                        <img src="<?= ((!empty($rank["path"])) ? "" . $rank["path"] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                        <?php echo $rank["name_en"]; ?>
                                                                                    </td>
                                                                                    <td><?php echo $rank["PI"]; ?></td>
                                                                                    <td><?php echo $rank["GD"]; ?></td>
                                                                                    <td><?php echo((!empty($rank["totalPts"]) ? $rank["totalPts"] : '0')); ?></td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                ดาวซัลโว-->
                                                <div class="col-sm-3">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ดาวซัลโว</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano bx2">
                                                                <div class="content">
                                                                    <div class="bx-player-records">
                                                                        <!--                                                    <div class="h-records">Goals</div>-->
                                                                        <div class="bx-top-records">
                                                                            <?php if (!empty($goalScorerSa)) { ?>
                                                                                <table>
                                                                                    <?php if (!empty($goalScorerSa)) { ?>
                                                                                        <tr>
                                                                                            <td></td>
                                                                                            <td class="text-right image-name-topScore">
                                                                                                <img src="<?= ((!empty($goalScorerSa[0]['path'])) ? '' . $goalScorerSa[0]['path'] : '/images/p1.png') ?>">
                                                                                            </td>
                                                                                            <td class="image-logo-topScore">
                                                                                                <img src="<?= $playLeague->checkPathTeamPlayer($goalScorerSa[0]['id'], 27) ?>">
                                                                                            </td>

                                                                                            <td>
                                                                                                <div class="name-top-records"><?= $goalScorerSa[0]['name']; ?></div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div class="txt-point"><?= $goalScorerSa[0]['goal']; ?></div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                </table>
                                                                            <?php } ?>
                                                                        </div>
                                                                        <?php if (!empty($goalScorerSa)) { ?>
                                                                            <table class="table-list-othertop">
                                                                                <?php foreach ($goalScorerSa as $key => $value) { ?>
                                                                                    <?php if ($key != 0) { ?>
                                                                                        <tr>
                                                                                            <td><?php echo $key + 1; ?></td>
                                                                                            <td>
                                                                                                <img style="width: 25px;"
                                                                                                     src="<?= $playLeague->checkPathTeamPlayer($value['id'], 27) ?>">
                                                                                            </td>
                                                                                            <td>
                                                                                                <div class="name-list-othertop"><?php echo $value['name']; ?></div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <span><?php echo $value['goal']; ?></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            </table>
                                                                        <?php } ?>
                                                                    </div>


                                                                    <div style="clear:both;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                ตารางแข่งขัน-->
                                                <div class="col-sm-5">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ตารางแข่งขัน</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano bx2">
                                                                <div class="content">
                                                                    <div>
                                                                        <?php if (!empty($matchLastSa)) { ?>
                                                                            <?php $data = ""; ?>
                                                                            <?php foreach ($matchLastSa as $okey => $fixtures) { ?>
                                                                                <?php if ($data != date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60)))) { ?>
                                                                                    <?php if ($okey != 0) { ?>
                                                                                        </table>
                                                                                    <?php } ?>
                                                                                    <div class="tab-date-played">
                                                                                        <i class="fa fa-calendar"></i> <?php echo date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60))); ?>
                                                                                    </div>
                                                                                    <table class="table-live">
                                                                                    <?php $data = date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60))); ?>
                                                                                <?php } ?>
                                                                                <tr>
                                                                                    <td>
                                                                                        <?php echo $fixtures['nameTeamHome']; ?>
                                                                                        <img src="<?= ((!empty($fixtures['pathTeamHome'])) ? "" . $fixtures['pathTeamHome'] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                    </td>
                                                                                    <td style="width: 10%;">
                                                                                        <?php if ($fixtures['status'] == "fullTime") { ?>
                                                                                            <strong><?php echo $fixtures['home_end_time_score'] ?>
                                                                                                -<?php echo $fixtures['away_end_time_score'] ?></strong>
                                                                                        <?php } else { ?>
                                                                                            <strong><?php echo date('H:i', (strtotime($fixtures['time_match']) - (60 * 60))); ?></strong>
                                                                                        <?php } ?>
                                                                                    </td>
                                                                                    <td>
                                                                                        <img src="<?= ((!empty($fixtures['pathTeamAway'])) ? "" . $fixtures['pathTeamAway'] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                        <?php echo $fixtures['nameTeamAway']; ?>
                                                                                    </td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                            </table>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style="clear:both;"></div>
                                            </div>
                                        </div>
                                        <div class="wrap-box-content-news">
                                            <div class="box-other-news">
                                                <div class="rows">
                                                    <?php for ($i = 0; $i < 3; $i++) { ?>
                                                        <?php if (array_key_exists($i, $foreignNewsSaData)) { ?>
                                                            <div class="col-sm-4"
                                                                 style="border: 0px;margin-bottom: 0px;">
                                                                <div class="thumbnail">
                                                                    <div class="tab-color"></div>
                                                                    <div class="times-content"><i
                                                                                class="fa fa-clock-o"></i>
                                                                        <time class="how-long"><?php echo $foreignNewsSaData[$i]->createDatetime; ?></time>
                                                                    </div>
                                                                    <div class="title-owl-carousel"><?php echo $foreignNewsSaData[$i]->titleTh; ?></div>
                                                                    <div class="box-videos img-other-news">
                                                                        <div class="hilight-cover"></div>
                                                                        <a href="/articles?newsid=<?php echo $foreignNewsSaData[$i]->newsid; ?>">
                                                                            <img src="<?php echo $foreignNewsSaData[$i]->imageLink; ?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <div style="clear: both;"></div>
                                                </div>
                                            </div>
                                            <div class="box-other-news">
                                                <div class="rows">
                                                    <?php for ($i = 3; $i < count($foreignNewsSaData); $i++) { ?>
                                                        <?php if (array_key_exists($i, $foreignNewsSaData)) { ?>
                                                            <div class="col-sm-3">
                                                                <div class="thumbnail">
                                                                    <div class="tab-color"></div>
                                                                    <div class="times-content"><i
                                                                                class="fa fa-clock-o"></i>
                                                                        <time
                                                                                class="how-long"><?php echo $foreignNewsSaData[$i]->createDatetime; ?></time>
                                                                    </div>
                                                                    <div
                                                                            class="title-owl-carousel"><?php echo $foreignNewsSaData[$i]->titleTh; ?></div>
                                                                    <div class="box-videos">
                                                                        <div class="hilight-cover"></div>
                                                                        <a href="/articles?newsid=<?php echo $foreignNewsSaData[$i]->newsid; ?>">
                                                                            <img src="<?php echo $foreignNewsSaData[$i]->imageLink; ?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" data-league="36" class="tab-pane-league tab-pane" id="League1">
                                        <div class="wrap-content-cols">
                                            <div class="wrap-box-results">
                                                <!--                                                ตารางคะแนน-->
                                                <div class="col-sm-4">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ตารางคะแนน</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano  bx2">
                                                                <div class="content">
                                                                    <table class="table-point">
                                                                        <tr>
                                                                            <th>Pos</th>
                                                                            <th>Club</th>
                                                                            <th>Pl</th>
                                                                            <th>GD</th>
                                                                            <th>Pts</th>
                                                                        </tr>
                                                                        <?php if (!empty($leagueL1)) { ?>
                                                                            <?php foreach ($leagueL1 as $key => $rank) { ?>
                                                                                <tr>
                                                                                    <td><?php echo $key + 1; ?></td>
                                                                                    <td>
                                                                                        <img src="<?= ((!empty($rank["path"])) ? "" . $rank["path"] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                        <?php echo $rank["name_en"]; ?>
                                                                                    </td>
                                                                                    <td><?php echo $rank["PI"]; ?></td>
                                                                                    <td><?php echo $rank["GD"]; ?></td>
                                                                                    <td><?php echo((!empty($rank["totalPts"]) ? $rank["totalPts"] : '0')); ?></td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                ดาวซัลโว-->
                                                <div class="col-sm-3">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ดาวซัลโว</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano bx2">
                                                                <div class="content">
                                                                    <div class="bx-player-records">
                                                                        <!--                                                    <div class="h-records">Goals</div>-->
                                                                        <div class="bx-top-records">
                                                                            <?php if (!empty($goalScorerL1)) { ?>
                                                                                <table>
                                                                                    <?php if (!empty($goalScorerL1)) { ?>
                                                                                        <tr>
                                                                                            <td></td>
                                                                                            <td class="text-right image-name-topScore">
                                                                                                <img src="<?= ((!empty($goalScorerL1[0]['path'])) ? '' . $goalScorerL1[0]['path'] : '/images/p1.png') ?>">
                                                                                            </td>
                                                                                            <td class="image-logo-topScore">
                                                                                                <img src="<?= $playLeague->checkPathTeamPlayer($goalScorerL1[0]['id'], 36) ?>">
                                                                                            </td>

                                                                                            <td>
                                                                                                <div class="name-top-records"><?= $goalScorerL1[0]['name']; ?></div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div class="txt-point"><?= $goalScorerL1[0]['goal']; ?></div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                </table>
                                                                            <?php } ?>
                                                                        </div>
                                                                        <?php if (!empty($goalScorerL1)) { ?>
                                                                            <table class="table-list-othertop">
                                                                                <?php foreach ($goalScorerL1 as $key => $value) { ?>
                                                                                    <?php if ($key != 0) { ?>
                                                                                        <tr>
                                                                                            <td><?php echo $key + 1; ?></td>
                                                                                            <td>
                                                                                                <img style="width: 25px;"
                                                                                                     src="<?= $playLeague->checkPathTeamPlayer($value['id'], 36) ?>">
                                                                                            </td>
                                                                                            <td>
                                                                                                <div class="name-list-othertop"><?php echo $value['name']; ?></div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <span><?php echo $value['goal']; ?></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            </table>
                                                                        <?php } ?>
                                                                    </div>


                                                                    <div style="clear:both;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                ตารางแข่งขัน-->
                                                <div class="col-sm-5">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ตารางแข่งขัน</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano bx2">
                                                                <div class="content">
                                                                    <div>
                                                                        <?php if (!empty($matchLastL1)) { ?>
                                                                            <?php $data = ""; ?>
                                                                            <?php foreach ($matchLastL1 as $okey => $fixtures) { ?>
                                                                                <?php if ($data != date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60)))) { ?>
                                                                                    <?php if ($okey != 0) { ?>
                                                                                        </table>
                                                                                    <?php } ?>
                                                                                    <div class="tab-date-played">
                                                                                        <i class="fa fa-calendar"></i> <?php echo date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60))); ?>
                                                                                    </div>
                                                                                    <table class="table-live">
                                                                                    <?php $data = date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60))); ?>
                                                                                <?php } ?>
                                                                                <tr>
                                                                                    <td>
                                                                                        <?php echo $fixtures['nameTeamHome']; ?>
                                                                                        <img src="<?= ((!empty($fixtures['pathTeamHome'])) ? "" . $fixtures['pathTeamHome'] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                    </td>
                                                                                    <td style="width: 10%;">
                                                                                        <?php if ($fixtures['status'] == "fullTime") { ?>
                                                                                            <strong><?php echo $fixtures['home_end_time_score'] ?>
                                                                                                -<?php echo $fixtures['away_end_time_score'] ?></strong>
                                                                                        <?php } else { ?>
                                                                                            <strong><?php echo date('H:i', (strtotime($fixtures['time_match']) - (60 * 60))); ?></strong>
                                                                                        <?php } ?>
                                                                                    </td>
                                                                                    <td>
                                                                                        <img src="<?= ((!empty($fixtures['pathTeamAway'])) ? "" . $fixtures['pathTeamAway'] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                        <?php echo $fixtures['nameTeamAway']; ?>
                                                                                    </td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                            </table>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style="clear:both;"></div>
                                            </div>
                                        </div>
                                        <div class="wrap-box-content-news">
                                            <div class="box-other-news">
                                                <div class="rows">
                                                    <?php for ($i = 0; $i < 3; $i++) { ?>
                                                        <?php if (array_key_exists($i, $foreignNewsL1Data)) { ?>
                                                            <div class="col-sm-4"
                                                                 style="border: 0px;margin-bottom: 0px;">
                                                                <div class="thumbnail">
                                                                    <div class="tab-color"></div>
                                                                    <div class="times-content"><i
                                                                                class="fa fa-clock-o"></i>
                                                                        <time class="how-long"><?php echo $foreignNewsL1Data[$i]->createDatetime; ?></time>
                                                                    </div>
                                                                    <div class="title-owl-carousel"><?php echo $foreignNewsL1Data[$i]->titleTh; ?></div>
                                                                    <div class="box-videos img-other-news">
                                                                        <div class="hilight-cover"></div>
                                                                        <a href="/articles?newsid=<?php echo $foreignNewsL1Data[$i]->newsid; ?>">
                                                                            <img src="<?php echo $foreignNewsL1Data[$i]->imageLink; ?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <div style="clear: both;"></div>
                                                </div>
                                            </div>
                                            <div class="box-other-news">
                                                <div class="rows">
                                                    <?php for ($i = 3; $i < count($foreignNewsL1Data); $i++) { ?>
                                                        <?php if (array_key_exists($i, $foreignNewsL1Data)) { ?>
                                                            <div class="col-sm-3">
                                                                <div class="thumbnail">
                                                                    <div class="tab-color"></div>
                                                                    <div class="times-content"><i
                                                                                class="fa fa-clock-o"></i>
                                                                        <time
                                                                                class="how-long"><?php echo $foreignNewsL1Data[$i]->createDatetime; ?></time>
                                                                    </div>
                                                                    <div
                                                                            class="title-owl-carousel"><?php echo $foreignNewsL1Data[$i]->titleTh; ?></div>
                                                                    <div class="box-videos">
                                                                        <div class="hilight-cover"></div>
                                                                        <a href="/articles?newsid=<?php echo $foreignNewsL1Data[$i]->newsid; ?>">
                                                                            <img src="<?php echo $foreignNewsL1Data[$i]->imageLink; ?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" data-league="105" class="tab-pane-league tab-pane active" id="UCL">
                                        <div class="wrap-content-cols">
                                            <div class="wrap-box-results">
                                                <!--                                                ตารางคะแนน-->
                                                <div class="col-sm-4">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ตารางคะแนน</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano  bx2">
                                                                <div class="content">
                                                                    <?php if (!empty($leagueUCL)) { ?>
                                                                        <?php foreach ($leagueUCL as $key => $rank) { ?>
                                                                            <?php if(($key+1)%4==1 or $key==0){ ?>
                                                                                <table class="table table-bordered">
                                                                                <tr><td colspan="5" style="font-weight: bolder;">Group <?php echo $rank["groups"]; ?></td></tr>
                                                                                <tr>
                                                                                    <th style="padding: 2px;">Pos</th>
                                                                                    <th style="padding: 2px;">Club</th>
                                                                                    <th style="padding: 2px;">Pl</th>
                                                                                    <th style="padding: 2px;">GD</th>
                                                                                    <th style="padding: 2px;">Pts</th>
                                                                                </tr>
                                                                            <?php } ?>
                                                                            <tr>
                                                                                <td style="padding: 2px;"><?php echo $key + 1; ?></td>
                                                                                <td style="padding: 2px;">
                                                                                    <img src="<?= ((!empty($rank["path"])) ? "" . $rank["path"] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                    <?php echo $rank["name_en"]; ?>
                                                                                </td>
                                                                                <td style="padding: 2px;"><?php echo $rank["PI"]; ?></td>
                                                                                <td style="padding: 2px;"><?php echo $rank["GD"]; ?></td>
                                                                                <td style="padding: 2px;"><?php echo((!empty($rank["totalPts"]) ? $rank["totalPts"] : '0')); ?></td>
                                                                            </tr>
                                                                            <?php if((($key+1)%4==0) and $key!==0){?>
                                                                                </table>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                        </table>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                ดาวซัลโว-->
                                                <div class="col-sm-3">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ดาวซัลโว</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano bx2">
                                                                <div class="content">
                                                                    <div class="bx-player-records">
                                                                        <!--                                                    <div class="h-records">Goals</div>-->
                                                                        <div class="bx-top-records">
                                                                            <?php if (!empty($goalScorerUCL)) { ?>
                                                                                <table>
                                                                                    <?php if (!empty($goalScorerUCL)) { ?>
                                                                                        <tr>
                                                                                            <td></td>
                                                                                            <td class="text-right image-name-topScore">
                                                                                                <img src="<?= ((!empty($goalScorerUCL[0]['path'])) ? '' . $goalScorerUCL[0]['path'] : '/images/p1.png') ?>">
                                                                                            </td>
                                                                                            <td class="image-logo-topScore">
                                                                                                <img src="<?= $playLeague->checkPathTeamPlayer($goalScorerUCL[0]['id'], 105) ?>">
                                                                                            </td>

                                                                                            <td>
                                                                                                <div class="name-top-records"><?= $goalScorerUCL[0]['name']; ?></div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div class="txt-point"><?= $goalScorerUCL[0]['goal']; ?></div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                </table>
                                                                            <?php } ?>
                                                                        </div>
                                                                        <?php if (!empty($goalScorerUCL)) { ?>
                                                                            <table class="table-list-othertop">
                                                                                <?php foreach ($goalScorerUCL as $key => $value) { ?>
                                                                                    <?php if ($key != 0) { ?>
                                                                                        <tr>
                                                                                            <td><?php echo $key + 1; ?></td>
                                                                                            <td>
                                                                                                <img style="width: 25px;"
                                                                                                     src="<?= $playLeague->checkPathTeamPlayer($value['id'], 105) ?>">
                                                                                            </td>
                                                                                            <td>
                                                                                                <div class="name-list-othertop"><?php echo $value['name']; ?></div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <span><?php echo $value['goal']; ?></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            </table>
                                                                        <?php } ?>
                                                                    </div>


                                                                    <div style="clear:both;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                ตารางแข่งขัน-->
                                                <div class="col-sm-5">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ตารางแข่งขัน</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano bx2">
                                                                <div class="content">
                                                                    <div>
                                                                        <?php if (!empty($matchLastUCL)) { ?>
                                                                            <?php $data = ""; ?>
                                                                            <?php foreach ($matchLastUCL as $okey => $fixtures) { ?>
                                                                                <?php if ($data != date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60)))) { ?>
                                                                                    <?php if ($okey != 0) { ?>
                                                                                        </table>
                                                                                    <?php } ?>
                                                                                    <div class="tab-date-played">
                                                                                        <i class="fa fa-calendar"></i> <?php echo date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60))); ?>
                                                                                    </div>
                                                                                    <table class="table-live">
                                                                                    <?php $data = date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60))); ?>
                                                                                <?php } ?>
                                                                                <tr>
                                                                                    <td>
                                                                                        <?php echo $fixtures['nameTeamHome']; ?>
                                                                                        <img src="<?= ((!empty($fixtures['pathTeamHome'])) ? "" . $fixtures['pathTeamHome'] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                    </td>
                                                                                    <td style="width: 10%;">
                                                                                        <?php if ($fixtures['status'] == "fullTime") { ?>
                                                                                            <strong><?php echo $fixtures['home_end_time_score'] ?>
                                                                                                -<?php echo $fixtures['away_end_time_score'] ?></strong>
                                                                                        <?php } else { ?>
                                                                                            <strong><?php echo date('H:i', (strtotime($fixtures['time_match']) - (60 * 60))); ?></strong>
                                                                                        <?php } ?>
                                                                                    </td>
                                                                                    <td>
                                                                                        <img src="<?= ((!empty($fixtures['pathTeamAway'])) ? "" . $fixtures['pathTeamAway'] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                        <?php echo $fixtures['nameTeamAway']; ?>
                                                                                    </td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                            </table>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style="clear:both;"></div>
                                            </div>
                                        </div>
                                        <div class="wrap-box-content-news">
                                            <div class="box-other-news">
                                                <div class="rows">
                                                    <?php for ($i = 0; $i < 3; $i++) { ?>
                                                        <?php if (array_key_exists($i, $foreignNewsUCLData)) { ?>
                                                            <div class="col-sm-4"
                                                                 style="border: 0px;margin-bottom: 0px;">
                                                                <div class="thumbnail">
                                                                    <div class="tab-color"></div>
                                                                    <div class="times-content"><i
                                                                                class="fa fa-clock-o"></i>
                                                                        <time class="how-long"><?php echo $foreignNewsUCLData[$i]->createDatetime; ?></time>
                                                                    </div>
                                                                    <div class="title-owl-carousel"><?php echo $foreignNewsUCLData[$i]->titleTh; ?></div>
                                                                    <div class="box-videos img-other-news">
                                                                        <div class="hilight-cover"></div>
                                                                        <a href="/articles?newsid=<?php echo $foreignNewsUCLData[$i]->newsid; ?>">
                                                                            <img src="<?php echo $foreignNewsUCLData[$i]->imageLink; ?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <div style="clear: both;"></div>
                                                </div>
                                            </div>
                                            <div class="box-other-news">
                                                <div class="rows">
                                                    <?php for ($i = 3; $i < count($foreignNewsUCLData); $i++) { ?>
                                                        <?php if (array_key_exists($i, $foreignNewsUCLData)) { ?>
                                                            <div class="col-sm-3">
                                                                <div class="thumbnail">
                                                                    <div class="tab-color"></div>
                                                                    <div class="times-content"><i
                                                                                class="fa fa-clock-o"></i>
                                                                        <time
                                                                                class="how-long"><?php echo $foreignNewsUCLData[$i]->createDatetime; ?></time>
                                                                    </div>
                                                                    <div
                                                                            class="title-owl-carousel"><?php echo $foreignNewsUCLData[$i]->titleTh; ?></div>
                                                                    <div class="box-videos">
                                                                        <div class="hilight-cover"></div>
                                                                        <a href="/articles?newsid=<?php echo $foreignNewsUCLData[$i]->newsid; ?>">
                                                                            <img src="<?php echo $foreignNewsUCLData[$i]->imageLink; ?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" data-league="608" class="tab-pane-league tab-pane" id="Europa">
                                        <div class="wrap-content-cols">
                                            <div class="wrap-box-results">
                                                <!--                                                ตารางคะแนน-->
                                                <div class="col-sm-4">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ตารางคะแนน</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano  bx2">
                                                                <div class="content">
                                                                    <?php if (!empty($leagueErp)) { ?>
                                                                        <?php foreach ($leagueErp as $key => $rank) { ?>
                                                                            <?php if(($key+1)%4==1 or $key==0){ ?>
                                                                                <table class="table table-bordered">
                                                                                <tr><td colspan="5" style="font-weight: bolder;">Group <?php echo $rank["groups"]; ?></td></tr>
                                                                                <tr>
                                                                                    <th style="padding: 2px;">Pos</th>
                                                                                    <th style="padding: 2px;">Club</th>
                                                                                    <th style="padding: 2px;">Pl</th>
                                                                                    <th style="padding: 2px;">GD</th>
                                                                                    <th style="padding: 2px;">Pts</th>
                                                                                </tr>
                                                                            <?php } ?>
                                                                            <tr>
                                                                                <td style="padding: 2px;"><?php echo $key + 1; ?></td>
                                                                                <td style="padding: 2px;">
                                                                                    <img src="<?= ((!empty($rank["path"])) ? "" . $rank["path"] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                    <?php echo $rank["name_en"]; ?>
                                                                                </td>
                                                                                <td style="padding: 2px;"><?php echo $rank["PI"]; ?></td>
                                                                                <td style="padding: 2px;"><?php echo $rank["GD"]; ?></td>
                                                                                <td style="padding: 2px;"><?php echo((!empty($rank["totalPts"]) ? $rank["totalPts"] : '0')); ?></td>
                                                                            </tr>
                                                                            <?php if((($key+1)%4==0) and $key!==0){?>
                                                                                </table>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                        </table>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                ดาวซัลโว-->
                                                <div class="col-sm-3">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ดาวซัลโว</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano bx2">
                                                                <div class="content">
                                                                    <div class="bx-player-records">
                                                                        <!--                                                    <div class="h-records">Goals</div>-->
                                                                        <div class="bx-top-records">
                                                                            <?php if (!empty($goalScorerErp)) { ?>
                                                                                <table>
                                                                                    <?php if (!empty($goalScorerErp)) { ?>
                                                                                        <tr>
                                                                                            <td></td>
                                                                                            <td class="text-right image-name-topScore">
                                                                                                <img src="<?= ((!empty($goalScorerErp[0]['path'])) ? '' . $goalScorerErp[0]['path'] : '/images/p1.png') ?>">
                                                                                            </td>
                                                                                            <td class="image-logo-topScore">
                                                                                                <img src="<?= $playLeague->checkPathTeamPlayer($goalScorerErp[0]['id'], 608) ?>">
                                                                                            </td>

                                                                                            <td>
                                                                                                <div class="name-top-records"><?= $goalScorerErp[0]['name']; ?></div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div class="txt-point"><?= $goalScorerErp[0]['goal']; ?></div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                </table>
                                                                            <?php } ?>
                                                                        </div>
                                                                        <?php if (!empty($goalScorerErp)) { ?>
                                                                            <table class="table-list-othertop">
                                                                                <?php foreach ($goalScorerErp as $key => $value) { ?>
                                                                                    <?php if ($key != 0) { ?>
                                                                                        <tr>
                                                                                            <td><?php echo $key + 1; ?></td>
                                                                                            <td>
                                                                                                <img style="width: 25px;"
                                                                                                     src="<?= $playLeague->checkPathTeamPlayer($value['id'], 608) ?>">
                                                                                            </td>
                                                                                            <td>
                                                                                                <div class="name-list-othertop"><?php echo $value['name']; ?></div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <span><?php echo $value['goal']; ?></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            </table>
                                                                        <?php } ?>
                                                                    </div>


                                                                    <div style="clear:both;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                ตารางแข่งขัน-->
                                                <div class="col-sm-5">
                                                    <div class="box-results">
                                                        <span class="title-box-results">ตารางแข่งขัน</span>
                                                        <div class="wrap-height">
                                                            <div id="about" class="nano bx2">
                                                                <div class="content">
                                                                    <div>
                                                                        <?php if (!empty($matchLastErp)) { ?>
                                                                            <?php $data = ""; ?>
                                                                            <?php foreach ($matchLastErp as $okey => $fixtures) { ?>
                                                                                <?php if ($data != date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60)))) { ?>
                                                                                    <?php if ($okey != 0) { ?>
                                                                                        </table>
                                                                                    <?php } ?>
                                                                                    <div class="tab-date-played">
                                                                                        <i class="fa fa-calendar"></i> <?php echo date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60))); ?>
                                                                                    </div>
                                                                                    <table class="table-live">
                                                                                    <?php $data = date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60))); ?>
                                                                                <?php } ?>
                                                                                <tr>
                                                                                    <td>
                                                                                        <?php echo $fixtures['nameTeamHome']; ?>
                                                                                        <img src="<?= ((!empty($fixtures['pathTeamHome'])) ? "" . $fixtures['pathTeamHome'] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                    </td>
                                                                                    <td style="width: 10%;">
                                                                                        <?php if ($fixtures['status'] == "fullTime") { ?>
                                                                                            <strong><?php echo $fixtures['home_end_time_score'] ?>
                                                                                                -<?php echo $fixtures['away_end_time_score'] ?></strong>
                                                                                        <?php } else { ?>
                                                                                            <strong><?php echo date('H:i', (strtotime($fixtures['time_match']) - (60 * 60))); ?></strong>
                                                                                        <?php } ?>
                                                                                    </td>
                                                                                    <td>
                                                                                        <img src="<?= ((!empty($fixtures['pathTeamAway'])) ? "" . $fixtures['pathTeamAway'] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                                        <?php echo $fixtures['nameTeamAway']; ?>
                                                                                    </td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                            </table>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style="clear:both;"></div>
                                            </div>
                                        </div>
                                        <div class="wrap-box-content-news">
                                            <div class="box-other-news">
                                                <div class="rows">
                                                    <?php for ($i = 0; $i < 3; $i++) { ?>
                                                        <?php if (array_key_exists($i, $foreignNewsErpData)) { ?>
                                                            <div class="col-sm-4"
                                                                 style="border: 0px;margin-bottom: 0px;">
                                                                <div class="thumbnail">
                                                                    <div class="tab-color"></div>
                                                                    <div class="times-content"><i
                                                                                class="fa fa-clock-o"></i>
                                                                        <time class="how-long"><?php echo $foreignNewsErpData[$i]->createDatetime; ?></time>
                                                                    </div>
                                                                    <div class="title-owl-carousel"><?php echo $foreignNewsErpData[$i]->titleTh; ?></div>
                                                                    <div class="box-videos img-other-news">
                                                                        <div class="hilight-cover"></div>
                                                                        <a href="/articles?newsid=<?php echo $foreignNewsErpData[$i]->newsid; ?>">
                                                                            <img src="<?php echo $foreignNewsErpData[$i]->imageLink; ?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <div style="clear: both;"></div>
                                                </div>
                                            </div>
                                            <div class="box-other-news">
                                                <div class="rows">
                                                    <?php for ($i = 3; $i < count($foreignNewsErpData); $i++) { ?>
                                                        <?php if (array_key_exists($i, $foreignNewsErpData)) { ?>
                                                            <div class="col-sm-3">
                                                                <div class="thumbnail">
                                                                    <div class="tab-color"></div>
                                                                    <div class="times-content"><i
                                                                                class="fa fa-clock-o"></i>
                                                                        <time
                                                                                class="how-long"><?php echo $foreignNewsErpData[$i]->createDatetime; ?></time>
                                                                    </div>
                                                                    <div
                                                                            class="title-owl-carousel"><?php echo $foreignNewsErpData[$i]->titleTh; ?></div>
                                                                    <div class="box-videos">
                                                                        <div class="hilight-cover"></div>
                                                                        <a href="/articles?newsid=<?php echo $foreignNewsErpData[$i]->newsid; ?>">
                                                                            <img src="<?php echo $foreignNewsErpData[$i]->imageLink; ?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </li>

                    <!--                    ข่าวฟุตบอลไทย-->
                    <li class="content-foreignNews-box">
                        <div class="wrap-content-cols">

                            <div class="wrap-box-results">
                                <!--                                ตารางคะแนน-->
                                <div class="col-sm-4">
                                    <div class="box-results">
                                        <span class="title-box-results">ตารางคะแนน</span>
                                        <div class="wrap-height">
                                            <div id="about" class="nano  bx2">
                                                <div class="content">
                                                    <table class="table-point">
                                                        <tr>
                                                            <th></th>
                                                            <th>Club</th>
                                                            <th>Pl</th>
                                                            <th>GD</th>
                                                            <th>Pts</th>
                                                        </tr>
                                                        <?php if (!empty($league)) { ?>
                                                            <?php foreach ($league as $key => $rank) { ?>
                                                                <tr>
                                                                    <td><?php echo $key + 1; ?></td>
                                                                    <td>
                                                                        <img src="<?= ((!empty($rank["path"])) ? "" . $rank["path"] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                        <?php echo $rank["name_en"]; ?>
                                                                    </td>
                                                                    <td><?php echo $rank["PI"]; ?></td>
                                                                    <td><?php echo $rank["GD"]; ?></td>
                                                                    <td><?php echo((!empty($rank["totalPts"]) ? $rank["totalPts"] : '0')); ?></td>
                                                                </tr>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--                            ดาวซัลโว-->
                                <div class="col-sm-3">
                                    <div class="box-results">
                                        <span class="title-box-results">ดาวซัลโว</span>
                                        <div class="wrap-height">
                                            <div id="about" class="nano bx2">
                                                <div class="content">
                                                    <div class="bx-player-records">
                                                        <!--                                                    <div class="h-records">Goals</div>-->
                                                        <div class="bx-top-records">
                                                            <?php if (!empty($goalScorer)) { ?>
                                                                <table>
                                                                    <?php if (!empty($goalScorer)) { ?>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td class="text-right image-name-topScore">
                                                                                <img src="<?= ((!empty($goalScorer[0]['path'])) ? '' . $goalScorer[0]['path'] : '/images/p1.png') ?>">
                                                                            </td>
                                                                            <td class="image-logo-topScore"><img
                                                                                        src="<?= $playLeague->checkPathTeamPlayer($goalScorer[0]['id'], 590) ?>">
                                                                            </td>

                                                                            <td>
                                                                                <div class="name-top-records"><?= $goalScorer[0]['name']; ?></div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="txt-point"><?= $goalScorer[0]['goal']; ?></div>
                                                                            </td>

                                                                        </tr>
                                                                    <?php } ?>
                                                                </table>
                                                            <?php } ?>
                                                        </div>
                                                        <?php if (!empty($goalScorer)) { ?>
                                                            <table class="table-list-othertop">
                                                                <?php foreach ($goalScorer as $key => $value) { ?>
                                                                    <?php if ($key != 0) { ?>
                                                                        <tr>
                                                                            <td><?php echo $key + 1; ?></td>
                                                                            <td>
                                                                                <img src="<?= $playLeague->checkPathTeamPlayer($value['id'], 590) ?>">
                                                                            </td>
                                                                            <td>
                                                                                <div class="name-list-othertop"><?php echo $value['name']; ?></div>
                                                                            </td>
                                                                            <td>
                                                                                <span><?php echo $value['goal']; ?></span>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </table>
                                                        <?php } ?>
                                                    </div>


                                                    <div style="clear:both;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--                                ตารางแข่งขัน-->
                                <div class="col-sm-5">
                                    <div class="box-results">
                                        <span class="title-box-results">ตารางแข่งขัน</span>
                                        <div class="wrap-height">
                                            <div id="about" class="nano bx2">
                                                <div class="content">
                                                    <div>
                                                        <?php if (!empty($matchLast)) { ?>
                                                            <?php $data = ""; ?>
                                                            <?php foreach ($matchLast as $okey => $fixtures) { ?>
                                                                <?php if ($data != date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60)))) { ?>
                                                                    <?php if ($okey != 0) { ?>
                                                                        </table>
                                                                    <?php } ?>
                                                                    <div class="tab-date-played">
                                                                        <i class="fa fa-calendar"></i> <?php echo date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60))); ?>
                                                                    </div>
                                                                    <table class="table-live">
                                                                    <?php $data = date('d-M-Y', (strtotime($fixtures['time_match']) - (60 * 60))); ?>
                                                                <?php } ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php echo $fixtures['nameTeamHome']; ?>
                                                                        <img src="<?= ((!empty($fixtures['pathTeamHome'])) ? "" . $fixtures['pathTeamHome'] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                    </td>
                                                                    <td style="width: 10%;">
                                                                        <?php if ($fixtures['status'] == "fullTime") { ?>
                                                                            <strong><?php echo $fixtures['home_end_time_score'] ?>
                                                                                -<?php echo $fixtures['away_end_time_score'] ?></strong>
                                                                        <?php } else { ?>
                                                                            <strong><?php echo date('H:i', (strtotime($fixtures['time_match']) - (60 * 60))); ?></strong>
                                                                        <?php } ?>
                                                                    </td>
                                                                    <td>
                                                                        <img src="<?= ((!empty($fixtures['pathTeamAway'])) ? "" . $fixtures['pathTeamAway'] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                                        <?php echo $fixtures['nameTeamAway']; ?>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                            </table>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="clear:both;"></div>
                            </div>
                        </div>
                        <div style="clear:both;"></div>
                        <div class="wrap-box-news rows">
                            <div class="wrap-box-content-news">
                                <div class="box-thai-news">
                                    <div class="rows">
                                        <div class="bx-thai-news">
                                            <div class="col-sm-6">
                                                <?php for ($i = 0; $i < 2; $i++) { ?>
                                                    <?php if (array_key_exists($i, $thainews)) { ?>
                                                        <div class="bx-story-medium bx-story-bigs tahi-news"
                                                             newsid="<?php echo $thainews[$i]->newsid; ?>">
                                                            <div class="hilight-cover"></div>
                                                            <a href="/articles?newsid=<?php echo $thainews[$i]->newsid ?>"
                                                               class="crop"
                                                               style="background-image: url(<?php echo $thainews[$i]->imageLink; ?>)"></a>

                                                            <div class="bx-story-content">
                                                                <div class="times-content"><i
                                                                            class="fa fa-clock-o"></i> <span
                                                                            class="how-long"><?php echo $thainews[$i]->createDatetime; ?></span>
                                                                </div>
                                                                <a href="/articles?newsid=<?php echo $thainews[$i]->newsid ?>"
                                                                   class="h-content"><?php echo $thainews[$i]->titleTh; ?></a>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                            <div class="col-xs-3 col-sm-3">
                                                <?php for ($i = 2; $i < count($thainews); $i++) { ?>
                                                    <?php if (array_key_exists($i, $thainews) && $i % 2 == 0) { ?>
                                                        <div class="thai-news"
                                                             newsid="<?php echo $thainews[$i]->newsid; ?>">
                                                            <div class="bx-story-small">
                                                                <div class="hilight-cover"></div>
                                                                <a href="/articles?newsid=<?php echo $thainews[$i]->newsid ?>"
                                                                   class="crop"
                                                                   style="background-image: url(<?php echo $thainews[$i]->imageLink; ?>)"></a>

                                                                <div class="bx-story-content">
                                                                    <div class="times-content"><i
                                                                                class="fa fa-clock-o"></i> <span
                                                                                class="how-long"><?php echo $thainews[$i]->createDatetime; ?></span>
                                                                    </div>
                                                                    <a href="/articles?newsid=<?php echo $thainews[$i]->newsid ?>"
                                                                       class="h-content"><?php echo $thainews[$i]->titleTh; ?></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                            <div class="col-xs-3 col-sm-3">
                                                <?php for ($i = 2; $i < count($thainews); $i++) { ?>
                                                    <?php if (array_key_exists($i, $thainews) && $i % 2 != 0) { ?>
                                                        <div class="thai-news"
                                                             newsid="<?php echo $thainews[$i]->newsid; ?>">
                                                            <div class="bx-story-small">
                                                                <div class="hilight-cover"></div>
                                                                <a href="/articles?newsid=<?php echo $thainews[$i]->newsid ?>"
                                                                   class="crop"
                                                                   style="background-image: url(<?php echo $thainews[$i]->imageLink; ?>)"></a>

                                                                <div class="bx-story-content">
                                                                    <div class="times-content"><i
                                                                                class="fa fa-clock-o"></i> <span
                                                                                class="how-long"><?php echo $thainews[$i]->createDatetime; ?></span>
                                                                    </div>
                                                                    <a href="/articles?newsid=<?php echo $thainews[$i]->newsid ?>"
                                                                       class="h-content"><?php echo $thainews[$i]->titleTh; ?></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                        </div>


                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('click', 'a.backward,a.forward', function (e) {
        if ($(this).hasClass('backward')) {
            $('div.backwardNews').trigger('click');
        } else {
            $('div.forwardNews').trigger('click');
        }
        console.log("CLICK");
    });

        var $frame = $('.wrap-box-news-list');
        var $slidee = $frame.find('div#div-foreignNews').eq(0);
        var $wrap = $frame.parent();
        $slidee.sly({
            horizontal: 1,
            itemNav: 'basic',
            smart: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 0,
    //            scrollBar: $wrap.find('.scrollbar'),
            pagesBar: $wrap.find('#pages'),
    //    scrollBy: 1,
            speed: 2000,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,

            // Cycling
            cycleBy: 'items',
            cycleInterval: 8000,
            pauseOnHover: 1,

            // Buttons
            forward: $wrap.find('.btnforward'),
            backward: $wrap.find('.btnbackward'),
            prev: $wrap.find('.btnbackward'),
            next: $wrap.find('.btnforward'),
            prevPage: $wrap.find('div.backwardNews'),
            nextPage: $wrap.find('div.forwardNews')
        });

    $(document).ready(function () {
//        console.log('9999');
        $('ul#pages li').each(function (i) {
            if (i == 0) {
                $(this).addClass('tab-pane-news');
                $(this).html("<a class='tab-pane-news backward'><div class='tab-h-content'><img src='/images/five-leagues/UEFA-logo.png'><span>ข่าวฟุตบอลต่างประเทศ</span></div></a>")
            } else {
                $(this).addClass('tab-pane-news');
                $(this).html("<a class='tab-pane-news forward'><div class='tab-h-content'><img src='/images/five-leagues/thai-logo.png'><span>ข่าวฟุตบอลไทย</span></div></a>")
            }

        });
//        refreshTime();
//        loadTabPaneLeague();
    });

    function loadTabPaneLeague() {
        $('.tab-pane-league').each(function (index) {
            var box = $(this);
            if (index > 0) {
                $.ajax({
                    url: "/play/tabPanelLeague",
                    data: {id: $(this).attr('data-league')},
                    method: "GET",
                    dataType: "html"
                }).done(function (response) {
                    box.html(response);
                    refreshTimeById(box.attr('data-league'));
                })
            }
        })
    }
    function refreshTimeById(id) {
        console.log("refreshTimeById");
        $.each($(".how-long-" + id), function (k, v) {
            if ($.isNumeric($(this).text())) {
                $(this).html(moment($(this).text(), "X").fromNow())
            } else {
                $(this).html(moment($(this).text(), "YYYY-MM-DD HH:mm:ss").fromNow())
            }
        })
    }
    function refreshTime() {
        console.log("refreshTime");
        $.each($(".how-long"), function (k, v) {
            if ($.isNumeric($(this).text())) {
                $(this).html(moment($(this).text(), "X").fromNow())
            } else {
                $(this).html(moment($(this).text(), "YYYY-MM-DD HH:mm:ss").fromNow())
            }
        })
    }
</script>

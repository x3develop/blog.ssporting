<?php

?>

<div class="content-livescore-main">
    <div class="container">
        <div class="col-md-12">
            <div class="content-list-livescore">

                <div class="col-md-12">
                    <div class="col-md-3 active">
                        <div class="thumbnail content-team-slide">
                            <div class="pull-left">
                                <table>
                                    <tr>
                                        <td><img src="/images/logo-team/Chelsea.png"></td>
                                        <td>Chelsea</td>
                                    </tr>
                                    <tr>
                                        <td><img src="/images/logo-team/manchester.png"></td>
                                        <td class="active">manchester</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="pull-right">
                                <div class="time-live">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p>21:00</p>
                                </div>
                                <div class="HDP-live">HDP</div>
                                <div class="num-live">-025</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="thumbnail content-team-slide">
                            <div class="pull-left">
                                <table>
                                    <tr>
                                        <td><img src="/images/logo-team/Chelsea.png"></td>
                                        <td>Chelsea</td>
                                    </tr>
                                    <tr>
                                        <td><img src="/images/logo-team/manchester.png"></td>
                                        <td class="active">manchester</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="pull-right">
                                <div class="time-live">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p>21:00</p>
                                </div>
                                <div class="HDP-live">HDP</div>
                                <div class="num-live">-025</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="thumbnail content-team-slide">
                            <div class="pull-left">
                                <table>
                                    <tr>
                                        <td><img src="/images/logo-team/Chelsea.png"></td>
                                        <td>Chelsea</td>
                                    </tr>
                                    <tr>
                                        <td><img src="/images/logo-team/manchester.png"></td>
                                        <td class="active">manchester</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="pull-right">
                                <div class="time-live">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p>21:00</p>
                                </div>
                                <div class="HDP-live">HDP</div>
                                <div class="num-live">-025</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="thumbnail content-team-slide">
                            <div class="pull-left">
                                <table>
                                    <tr>
                                        <td><img src="/images/logo-team/Chelsea.png"></td>
                                        <td>Chelsea</td>
                                    </tr>
                                    <tr>
                                        <td><img src="/images/logo-team/manchester.png"></td>
                                        <td class="active">manchester</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="pull-right">
                                <div class="time-live">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p>21:00</p>
                                </div>
                                <div class="HDP-live">HDP</div>
                                <div class="num-live">-025</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="topic-league">
                <h3><img src="/images/logo-team/fa-cup-2017.png"> ENGLAND FA CUP</h3>
                <ul>
                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> 20:00</li>
                    <li>|</li>
                    <li><i class="fa fa-calendar" aria-hidden="true"></i> 21/07/2018</li>
                </ul>
            </div>
            <div class="content-select-team-vote">

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="pull-right">
                                    <div class="name-team-vote pull-left text-right">
                                        <h3>Chelsea</h3>
                                    </div>
                                    <div class="box-vote-team">
                                        <div class="logo-vote-team-home">
                                            <img src="/images/logo-team/Chelsea.png">
                                        </div>
                                        <div class="btn-vote-team-home">
                                            VOTE
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="pull-left">
                                    <div class="box-vote-team">
                                        <div class="logo-vote-team-away">
                                            <img src="/images/logo-team/manchester.png">
                                        </div>
                                        <div class="btn-vote-team-away">
                                            VOTE
                                        </div>
                                    </div>
                                    <div class="name-team-vote pull-left">
                                        <h3>Manchester UTD</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 content-HDP-vote">
            <ul>
                <li>1.99</li>
                <li class="text-blue">[0.5]</li>
                <li>1.88</li>
            </ul>
        </div>

    </div>

    <div class="col-md-12">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <!--                <ol class="carousel-indicators">-->
            <!--                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>-->
            <!--                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>-->
            <!--                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>-->
            <!--                </ol>-->

            <!-- Wrapper for slides -->
            <div class="container">
                <div class="carousel-inner" role="listbox">
                    <div class="item active">

                        <div class="col-md-6 box-comment-vote">
                            <a href="#">
                                <table>
                                    <tr>
                                        <td><img src="/images/sample-logo01.png"></td>
                                        <td>
                                            <span class="text-home-comment"><strong>@Bate borisov</strong></span>
                                            <span>
                                            ดาวรุ่งวัย 19 ปี เผย กองหน้าชาวบราซิล ขอบคุณที่
                                            แอสซิสต์ให้สร้างสถิติใหม่ไทยลีก พร้อมแอบแซวเป็น “โกลเด้นบอย”</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                            <a href="#">
                                <table>
                                    <tr>
                                        <td><img src="/images/sample-logo02.png"></td>
                                        <td>
                                            <span class="text-away-comment"><strong>@Bate borisov</strong></span>
                                            <span>
                                            ดาวรุ่งวัย 19 ปี เผย กองหน้าชาวบราซิล ขอบคุณที่
                                            แอสซิสต์ให้สร้างสถิติใหม่ไทยลีก พร้อมแอบแซวเป็น “โกลเด้นบอย”</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </div>

                        <div class="col-md-6 box-comment-vote">
                            <a href="#">
                                <table>
                                    <tr>
                                        <td><img src="/images/sample-logo01.png"></td>
                                        <td>
                                            <span class="text-home-comment"><strong>@Bate borisov</strong></span>
                                            <span>
                                            ดาวรุ่งวัย 19 ปี เผย กองหน้าชาวบราซิล ขอบคุณที่
                                            แอสซิสต์ให้สร้างสถิติใหม่ไทยลีก พร้อมแอบแซวเป็น “โกลเด้นบอย”</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                            <a href="#">
                                <table>
                                    <tr>
                                        <td><img src="/images/sample-logo02.png"></td>
                                        <td>
                                            <span class="text-away-comment"><strong>@Bate borisov</strong></span>
                                            <span>
                                            ดาวรุ่งวัย 19 ปี เผย กองหน้าชาวบราซิล ขอบคุณที่
                                            แอสซิสต์ให้สร้างสถิติใหม่ไทยลีก พร้อมแอบแซวเป็น “โกลเด้นบอย”</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </div>

                    </div>
                    <div class="item">

                        <div class="col-md-6 box-comment-vote">
                            <a href="#">
                                <table>
                                    <tr>
                                        <td><img src="/images/sample-logo01.png"></td>
                                        <td>
                                            <span class="text-home-comment"><strong>@Bate borisov</strong></span>
                                            <span>
                                            ดาวรุ่งวัย 19 ปี เผย กองหน้าชาวบราซิล ขอบคุณที่
                                            แอสซิสต์ให้สร้างสถิติใหม่ไทยลีก พร้อมแอบแซวเป็น “โกลเด้นบอย”</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                            <a href="#">
                                <table>
                                    <tr>
                                        <td><img src="/images/sample-logo02.png"></td>
                                        <td>
                                            <span class="text-away-comment"><strong>@Bate borisov</strong></span>
                                            <span>
                                            ดาวรุ่งวัย 19 ปี เผย กองหน้าชาวบราซิล ขอบคุณที่
                                            แอสซิสต์ให้สร้างสถิติใหม่ไทยลีก พร้อมแอบแซวเป็น “โกลเด้นบอย”</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </div>
                        <div class="col-md-6 box-comment-vote">
                            <a href="#">
                                <table>
                                    <tr>
                                        <td><img src="/images/sample-logo01.png"></td>
                                        <td>
                                            <span class="text-home-comment"><strong>@Bate borisov</strong></span>
                                            <span>
                                            ดาวรุ่งวัย 19 ปี เผย กองหน้าชาวบราซิล ขอบคุณที่
                                            แอสซิสต์ให้สร้างสถิติใหม่ไทยลีก พร้อมแอบแซวเป็น “โกลเด้นบอย”</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                            <a href="#">
                                <table>
                                    <tr>
                                        <td><img src="/images/sample-logo02.png"></td>
                                        <td>
                                            <span class="text-away-comment"><strong>@Bate borisov</strong></span>
                                            <span>
                                            ดาวรุ่งวัย 19 ปี เผย กองหน้าชาวบราซิล ขอบคุณที่
                                            แอสซิสต์ให้สร้างสถิติใหม่ไทยลีก พร้อมแอบแซวเป็น “โกลเด้นบอย”</span>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>

<!--mobile-->

<div class="content-livescore-main-mobile content-livescore-main">
    <div class="container">
        <div class="row">
            <div class="content-list-livescore content-list-livescore-mobile">
                <div class="col-md-12">
                    <ul>
                        <li class="active">
                            <div class="thumbnail content-team-slide">
                                <div class="pull-left">
                                    <table>
                                        <tr>
                                            <td>Chelsea</td>
                                        </tr>
                                        <tr>
                                            <td class="active">manchester</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="pull-right">
                                    <div class="time-live">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <p>21:00</p>
                                    </div>
                                    <div class="HDP-live">HDP</div>
                                    <div class="num-live">-025</div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="thumbnail content-team-slide">
                                <div class="pull-left">
                                    <table>
                                        <tr>
                                            <td>Chelsea</td>
                                        </tr>
                                        <tr>
                                            <td class="active">manchester</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="pull-right">
                                    <div class="time-live">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <p>21:00</p>
                                    </div>
                                    <div class="HDP-live">HDP</div>
                                    <div class="num-live">-025</div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="topic-league">
                    <span class="text-center">
                        <img src="/images/logo-team/fa-cup-2017.png">
                    </span>
                <h3>ENGLAND FA CUP</h3>
                <ul>
                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> 20:00</li>
                    <li>|</li>
                    <li><i class="fa fa-calendar" aria-hidden="true"></i> 21/07/2018</li>
                </ul>
            </div>
            <div class="content-select-team-vote content-select-team-vote-mobile">
                <div class="row">
                    <table>
                        <tr>
                            <td>
                                <ul class="text-center">
                                    <li><img src="/images/logo-team/Chelsea.png"></li>
                                    <li><strong>Chelsea FC</strong></li>
                                    <li>
                                        <button type="button" class="btn btn-primary btn-lg">Vote</button>
                                    </li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-center">
                                    <li><img src="/images/logo-team/manchester.png"></li>
                                    <li><strong>Manchester UTD</strong></li>
                                    <li>
                                        <button type="button" class="btn btn-primary btn-lg">Vote</button>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row content-HDP-vote">
            <ul>
                <li>1.99</li>
                <li class="text-blue">[0.5]</li>
                <li>1.88</li>
            </ul>
        </div>

    </div>

    <div class="col-md-12">
        <div id="carousel-mobile-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <!--                <ol class="carousel-indicators">-->
            <!--                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>-->
            <!--                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>-->
            <!--                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>-->
            <!--                </ol>-->

            <!-- Wrapper for slides -->

            <div class="container">
                <div class="carousel-inner" role="listbox">
                    <div class="col-md-12">
                        <div class="item active">
                            <div class="box-comment-vote">
                                <a href="#">
                                    <table>
                                        <tr>
                                            <td><img src="/images/sample-logo01.png"></td>
                                            <td>
                                                <span class="text-home-comment"><strong>@Bate borisov</strong></span>
                                                <span>
                                            ดาวรุ่งวัย 19 ปี เผย กองหน้าชาวบราซิล ขอบคุณที่
                                            แอสซิสต์ให้สร้างสถิติใหม่ไทยลีก พร้อมแอบแซวเป็น “โกลเด้นบอย”</span>
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                                <a href="#">
                                    <table>
                                        <tr>
                                            <td><img src="/images/sample-logo02.png"></td>
                                            <td>
                                                <span class="text-away-comment"><strong>@Bate borisov</strong></span>
                                                <span>
                                            ดาวรุ่งวัย 19 ปี เผย กองหน้าชาวบราซิล ขอบคุณที่
                                            แอสซิสต์ให้สร้างสถิติใหม่ไทยลีก พร้อมแอบแซวเป็น “โกลเด้นบอย”</span>
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="box-comment-vote">
                                <a href="#">
                                    <table>
                                        <tr>
                                            <td><img src="/images/sample-logo01.png"></td>
                                            <td>
                                                <span class="text-home-comment"><strong>@Bate borisov</strong></span>
                                                <span>
                                            ดาวรุ่งวัย 19 ปี เผย กองหน้าชาวบราซิล ขอบคุณที่
                                            แอสซิสต์ให้สร้างสถิติใหม่ไทยลีก พร้อมแอบแซวเป็น “โกลเด้นบอย”</span>
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                                <a href="#">
                                    <table>
                                        <tr>
                                            <td><img src="/images/sample-logo02.png"></td>
                                            <td>
                                                <span class="text-away-comment"><strong>@Bate borisov</strong></span>
                                                <span>
                                            ดาวรุ่งวัย 19 ปี เผย กองหน้าชาวบราซิล ขอบคุณที่
                                            แอสซิสต์ให้สร้างสถิติใหม่ไทยลีก พร้อมแอบแซวเป็น “โกลเด้นบอย”</span>
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-mobile-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-mobile-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>

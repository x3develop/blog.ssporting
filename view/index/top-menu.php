<div class="topmenu">
    <div class="container">
        <div class="pull-left contact-social">
            <ul>
                <li>
                    <a href="https://www.facebook.com/nGoalTH/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="/"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="https://twitter.com/nGoalTH"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                </li>
            </ul>
        </div>
        <div class="pull-right box-login-FB">
            <?php if (!isset($_SESSION['login'])) { ?>
                <a class="fb-login-button" onclick="FBlogin();">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                    <span>Login</span>
                </a>
            <?php } ?>
        </div>
        <div class="pull-right content-profile-menu">
            <ul>
                <?php if (isset($_SESSION['login'])) { ?>
                    <li class="coin-topmenu"><a href=""><img src="/images/coin/sgold100.png">
                            <span><?php echo number_format($_SESSION['login']['gold'], 2) ?></span></a>
                    </li>
                    <li class="coin-topmenu"><a href=""><img src="/images/coin/scoin100.png">
                            <span><?php echo number_format($_SESSION['login']['coin'], 2) ?></span></a>
                    </li>
                <?php } ?>
                <li class="<?= ((!isset($_SESSION['login'])) ? 'hide' : '') ?>" id="fb-online">
                    <a href="#" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="true">
                        <img id="fb-picture"
                             src="<?= ((isset($_SESSION['login'])) ? 'https://graph.facebook.com/v2.8/' . $_SESSION['login']['fb_uid'] . '/picture?type=small' : '') ?>">
                        <label id="fb-name"><?= ((isset($_SESSION['login'])) ? $_SESSION['login']['name'] : '') ?></label>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel" style="width: 150px;">
                        <li><a id="own-profile-link" href="/profile.php">
                                ตั้งค่าโปรไฟล์</a>
                        </li>
                        <li><a href="/rules.php">กติกาและเงื่อนไข</a></li>
                        <li><a href="/ranking.php">Ranking</a></li>
                        <li><a href="/help.php">วิธีการเล่นเกมทายผล</a></li>
                        <li><a id="facebook-logoutbutton" href="/logout.php">
                                ออกจากระบบ</a>
                        </li>
                    </ul>
                    <div class="popover fade bottom in dropdown-menu dropdown-menu-profile hide" role="tooltip"
                         aria-labelledby="dropdownMenu1" style="margin-top: 33px;">
                        <div class="arrow"></div>
                        <div class="popover-content">
                            <form>
                                <ul style="color: #333;">
                                    <li>
                                        <a id="own-profile-link" href="/profile.php">
                                            Setting Profile
                                        </a>
                                    </li>
                                    <li><a href="/rules.php">กติกาและเงื่อนไข</a></li>
                                    <li><a href="/ranking.php">Ranking</a></li>
                                    <li><a href="/help.php">วิธีการเล่นเกมทายผล</a></li>
                                    <li>
                                        <a id="facebook-logoutbutton" href="/logout.php">
                                             Logout
                                        </a>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                </li>
                <?php if (!isset($_SESSION['login'])) { ?>
                <?php } ?>
            </ul>
        </div>

    </div>
</div>
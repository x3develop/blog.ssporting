<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/24/2017
 * Time: 10:57
 */
?>
<?php if (count($recommendodds['match']) != 0) { ?>
    <div class="container">
    <div class="wrap-content-cols section-games">
        <div class="row">
            <div class="bg-full">
                <div class="hilight-bg"></div>
                <div class="container" style="z-index: 50;">
                    <div class="wrap-games rows">
                        <div class="col-sm-1">
                            <div class="label-vdo" style="color: #4a7fa7; font-size: 13rem; width: 41.25rem;">GAMES
                                ทายผลบอล
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <table class="table-games table-games2">

                                <tr>
                                    <td>
                                        <div class="bx-head-team">
                                            <div
                                                class="<?php echo (!empty($ownbet) && $ownbet['choose'] == 'home') ? '' : 'hilight-cover'; ?>"></div>
                                            <?php if (!empty($ownbet) && $ownbet['choose'] == 'home') { ?>
                                                <div class="vote-already">VOTE</div>
                                            <?php } ?>
                                            <div class="crop-team"></div>
                                            <div class="bx-logo-team"><img
                                                    src="<?php echo $logoman->getLogo($thismatch['hid'], 2); ?>"></div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="bx-head-team">
                                            <div
                                                class="<?php echo (!empty($ownbet) && $ownbet['choose'] == 'away') ? '' : 'hilight-cover'; ?>"></div>
                                            <?php if (!empty($ownbet) && $ownbet['choose'] == 'away') { ?>
                                                <div class="vote-already">VOTE</div>
                                            <?php } ?>
                                            <div class="crop-team imgTest2"></div>
                                            <div class="bx-logo-team"><img
                                                    src="<?php echo $logoman->getLogo($thismatch['gid'], 2); ?>"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="wrap-vote">
                                            <div class="bx-hdp">
                                                HDP
                                                <?php
                                                $mhdp = "";
                                                $hhdp = "";
                                                $ghdp = "";
                                                if (!empty($gamedata["odds"])) {
                                                    $mhdp = $gamedata["odds"][2];
                                                    $hhdp = $gamedata["odds"][3];
                                                    $ghdp = $gamedata["odds"][4];
                                                } ?>
                                                <ul>
                                                    <li id="hdp_home_val">
                                                        <?php
                                                        if (!empty($gamedata["odds"])) {
                                                            echo $gamedata["odds"][3];
                                                        }
                                                        ?>
                                                    </li>
                                                    <?php
                                                    $oddstype = "none";
                                                    if (!empty($gamedata["odds"])) {
                                                        $oddstype = $gamedata["odds"][1];
                                                    }
                                                    ?>
                                                    <li id="hdp_val" oddstype="<?php echo $oddstype; ?>" class="bg-info">
                                                        <?php
                                                        if (!empty($gamedata["odds"])) {
                                                            echo $gamedata["odds"][2];
                                                        }
                                                        ?>
                                                    </li>
                                                    <li id="hdp_away_val">
                                                        <?php
                                                        if (!empty($gamedata["odds"])) {
                                                            echo $gamedata["odds"][4];
                                                        }
                                                        ?>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="bx-division">
                                                <?php
                                                $showdate = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $thismatch['date']);
                                                $showdate->addHours(7);
                                                ?>
                                                <?php echo $gamedata['match'][38]; ?> <label
                                                    class="info"><?php echo $showdate->format("H:i"); ?></label>
                                            </div>
                                            <div class="bx-vote">
                                                <?php if (empty($ownbet)) { ?>
                                                    <h3>เลือกทีมทายผล</h3>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 50px;">
                                        <div
                                            class="name-team <?php echo (is_numeric($mhdp) && floatval($mhdp) < 0) ? 'hdp-team' : ''; ?> <?php echo ((is_numeric($mhdp) && floatval($mhdp) == 0) && floatval($hhdp) < floatval($ghdp)) ? 'hdp-team' : ''; ?>">
                                            <?php
                                            echo $thismatch['hn'];
                                            ?>
                                        </div>
                                        <?php if (empty($ownbet)) { ?>
                                            <div class="bt-play" side="home">Play Home</div>
                                        <?php } ?>
                                    </td>
                                    <td style="padding-top: 50px;">
                                        <div
                                            class="name-team <?php echo (is_numeric($mhdp) && floatval($mhdp) > 0) ? 'hdp-team' : ''; ?> <?php echo ((is_numeric($mhdp) && floatval($mhdp) == 0) && floatval($hhdp) > floatval($ghdp)) ? 'hdp-team' : ''; ?>">
                                            <?php
                                            echo $thismatch['gn'];
                                            ?>
                                        </div>
                                        <?php if (empty($ownbet)) { ?>
                                            <div class="bt-play" side="away">Play Away</div>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="0"
                                                 aria-valuemin="0" aria-valuemax="100"
                                                 style="width: <?php echo (count($betside['betlist']['home']) / (count($betside['betlist']['home']) + count($betside['betlist']['away']))) * 100; ?>%;">
                                                <span class="sr-only">60% Complete</span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 10px 0px;">
                                        <div style="position: relative;">
                                            <div class="bx-icon-vote" style="left: 94%; top: 0px;"><a
                                                    href="/statistics.php?mid=<?php echo $thismatch['mid']; ?>"><img
                                                        src="/images/icon-vote.png" style="width: 50px;"></a></div>
                                            <div>
                                            </div>
                                            <div class="bx-head">Results</div>

                                            <div class="bx-icon-stat pull-left">
                                                <?php $showrsconut = 0; ?>
                                                <?php foreach ($teamguide["guide"]["home"] as $guide) { ?>
                                                    <?php $g = $datamap->mapByKey($teamguide['keylist']['guide'], $guide); ?>
                                                    <?php if ($showrsconut < 6) { ?>
                                                        <img src="/images/icon-stat/result/<?php echo $g['result']; ?>.png">
                                                        <?php $showrsconut++; ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                            <div style="clear: both;"></div>
                                        </div>
                                        <div>
                                            <div class="bx-head">Odds</div>

                                            <div class="bx-icon-stat pull-left">
                                                <?php $showrsconut = 0; ?>
                                                <?php foreach ($teamguide["guide"]["home"] as $guide) { ?>
                                                    <?php $g = $datamap->mapByKey($teamguide['keylist']['guide'], $guide); ?>
                                                    <?php if ($showrsconut < 6 && $g['odds_result'] != -1) { ?>
                                                        <img
                                                            src="/images/icon-stat/oddsresult/<?php echo $g['odds_result']; ?>.png">
                                                        <?php $showrsconut++; ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <div class="bx-head pull-right">Results</div>

                                            <div class="bx-icon-stat pull-right"> <?php $showrsconut = 0; ?>
                                                <?php foreach ($teamguide["guide"]["away"] as $guide) { ?>
                                                    <?php $g = $datamap->mapByKey($teamguide['keylist']['guide'], $guide); ?>
                                                    <?php if ($showrsconut < 6) { ?>
                                                        <img src="/images/icon-stat/result/<?php echo $g['result']; ?>.png">
                                                        <?php $showrsconut++; ?>
                                                    <?php } ?>
                                                <?php } ?></div>
                                            <div style="clear: both;"></div>
                                        </div>
                                        <div>
                                            <div class="bx-head  pull-right">Odds</div>

                                            <div class="bx-icon-stat pull-right"><?php $showrsconut = 0; ?>
                                                <?php foreach ($teamguide["guide"]["away"] as $guide) { ?>
                                                    <?php $g = $datamap->mapByKey($teamguide['keylist']['guide'], $guide); ?>
                                                    <?php if ($showrsconut < 6 && $g['odds_result'] != -1) { ?>
                                                        <img
                                                            src="/images/icon-stat/oddsresult/<?php echo $g['odds_result']; ?>.png">
                                                        <?php $showrsconut++; ?>
                                                    <?php } ?>
                                                <?php } ?></div>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 10px; background-color: #03385f;">
                                        <div style="position:relative;">
                                            <div class="bx-icon-vote">VS</div>
                                        </div>
                                        <div class="pull-left earlybet-home-user">
                                            <?php foreach ($betside['betlist']['home'] as $key => $val) { ?>
                                                <?php if ($key < 10) { ?>
                                                    <?php $bet = $datamap->mapByKey($betside['keylist']['betlist'], $val) ?>
                                                    <div class="box-img-user"><img
                                                            src="<?php echo $logoman->getFbProfilePix($bet['fb_uid']); ?>"
                                                            data-pin-nopin="true"></div>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="pull-right"><label
                                                class="point-team"><?php echo count($betside['betlist']['home']); ?></label>
                                        </div>
                                    </td>
                                    <td style="padding: 10px; background-color: #03385f;">
                                        <div class="pull-left"><label
                                                class="point-team"><?php echo count($betside['betlist']['away']); ?></label>
                                        </div>
                                        <div class="pull-right earlybet-away-user">
                                            <?php foreach ($betside['betlist']['away'] as $key => $val) { ?>
                                                <?php if ($key < 10) { ?>
                                                    <?php $bet = $datamap->mapByKey($betside['keylist']['betlist'], $val) ?>
                                                    <div class="box-img-user"><img
                                                            src="<?php echo $logoman->getFbProfilePix($bet['fb_uid']); ?>"
                                                            data-pin-nopin="true"></div>
                                                <?php } ?>
                                            <?php } ?>

                                        </div>
                                    </td>
                                </tr>
                                <!--                                <tr style="border-top: solid 1px #0091c8;">-->
                                <!--                                    <td style="padding: 10px;">-->
                                <!--                                        <div class="pull-left">ความคิดเห็น</div>-->
                                <!--                                    </td>-->
                                <!--                                    <td style="padding: 10px;">-->
                                <!--                                        <div class="pull-right">ดูความคิดเห็นทั้งหมด</div>-->
                                <!--                                    </td>-->
                                <!--                                </tr>-->
                            </table>

                            <div class="bx-comments-games mg-bt15" style="background-color: #003f6e; box-shadow: none;">
                                <div class="rows">
                                    <div class="col-sm-6" style="border-right: solid 1px #03243c;">
                                        <div class="box-comments box-coment-index">
                                            <table id="cm-home-side" class="hide">
                                                <tr>
                                                    <td>
                                                        <div class="box-img-user"><img src=""></div>
                                                    </td>
                                                    <td>
                                                        <strong>Sukhumvit Set</strong> เสือคืนป่า! ฟัลเกายิงประตู UCL
                                                        รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี
                                                        <div class="times-content"><span>34 นาที</span> <span><i
                                                                    class="fa fa-heart"></i> 2</span> <span>ตอบกลับ</span>
                                                        </div>
                                                        <div class="bx-reply">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="box-img-user"><img src="">
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Sukhumvit Set</strong> เสือคืนป่า!
                                                                        ฟัลเกายิงประตู UCL รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี
                                                                        <div class="times-content">
                                                                            <span>34 นาที</span> <span><i
                                                                                    class="fa fa-heart"></i> 2</span> <span>ตอบกลับ</span>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="box-img-user"><img src="">
                                                                        </div>
                                                                    </td>
                                                                    <td><input class="form-control"
                                                                               placeholder="ตอบกลับความคิดเห็น"></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="box-img-user"><img src=""></div>
                                                    </td>
                                                    <td>
                                                        <strong>Sukhumvit Set</strong> เสือคืนป่า! ฟัลเกายิงประตู UCL
                                                        รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี
                                                        <div class="times-content"><span>34 นาที</span> <span><i
                                                                    class="fa fa-heart"></i> 2</span> <span>ตอบกลับ</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="box-comments box-comment-index">
                                            <table id="cm-away-side" class="hide">
                                                <tr class="fullsize-cm-original">
                                                    <td>
                                                        <div class="box-img-user"><img class="cm-profile-pix"
                                                                                       src=""></div>
                                                    </td>
                                                    <td>
                                                        <strong class="cm-name-user">Sukhumvit Set</strong>
                                                        <labal class="cm-msg"></labal>
                                                        <div class="times-content"><span
                                                                class="how-long pass-long">34 นาที</span> <span><i
                                                                    class="fa fa-heart like-comment"
                                                                    commentid="0"></i> <label
                                                                    class="like-count" commentid="0">2</label></span>
                                                            <span class="reply-to" parent="0">ตอบกลับ</span></div>
                                                        <div class="bx-reply">
                                                            <table class="reply-row" parent="0">
                                                                <tr class="fullsize-rp-original">
                                                                    <td>
                                                                        <div class="box-img-user"><img
                                                                                class="cm-profile-pix"
                                                                                src="">
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <strong class="cm-name-user">Sukhumvit Set</strong>
                                                                        <labal class="rp-msg"></labal>
                                                                        <div class="times-content"><span class="how-long pass-long">34 นาที</span> <span><i
                                                                                    class="fa fa-heart like-comment"
                                                                                    commentid="0"></i> <label
                                                                                    class="like-count"
                                                                                    commentid="0">2</label></span> <span
                                                                                class="reply-to" parent="0">ตอบกลับ</span>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr class="reply-smgbox-orginal hide" parent="0" user="ownner">
                                                                    <td>
                                                                        <div class="box-img-user"><img
                                                                                class="active-user-pix"
                                                                                src="">
                                                                        </div>
                                                                    </td>
                                                                    <td><input class="form-control reply-msg-val"
                                                                               placeholder="ตอบกลับความคิดเห็น" parent="0">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="box-img-user"><img src="/images/9.jpg"></div>
                                                    </td>
                                                    <td>
                                                        <strong>Sukhumvit Set</strong> เสือคืนป่า! ฟัลเกายิงประตู UCL
                                                        รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี
                                                        <div class="times-content"><span>34 นาที</span> <span><i
                                                                    class="fa fa-heart"></i> 2</span> <span>ตอบกลับ</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="comment-input-main">
                                            <input id="comment-inputbox" class="form-control" placeholder="แสดงความคิดเห็น" style="border: 0px; background-color: #03385f;">
                                            <button class="btn btn-primary pull-right" id="post-comment">Post</button>
                                        </div>
                                    </div>
                                    <div style="clear: both;"></div>
                                </div>
                            </div>

                            <div class="bx-raking mg-bt15">
                                <div>
                                    <div class="pull-left" style="color: #fff;"><strong>อันดับคะแนน</strong></div>
                                    <div class="pull-right"><a href="ranking.php" style="color: #ccc;">ดูอันดับทั้งหมด</a></div>
                                    <div style="clear: both;"></div>
                                </div>
                                <div class="bx-table-ranking table-rank-index" style="height: 250px;">
                                    <div class="col-sm-4">
                                        <strong>Top Ranking</strong>
                                        <table>
                                            <!--                                            <tr>-->
                                            <!--                                                <th colspan="2">อันดับ</th>-->
                                            <!--                                                <th>แต้ม</th>-->
                                            <!--                                            </tr>-->
                                            <?php $row = 0; ?>
                                            <?php foreach ($ranking['gp']['now'] as $key => $val) { ?>
                                                <?php $r = $datamap->mapByKey($ranking['keylist']['gp'], $val); ?>
                                                <?php if ($row < 5) { ?>
                                                    <tr>
                                                        <td><?php echo $r['rank']; ?></td>
                                                        <td>
                                                            <div class="box-img-user bx-radius"><img
                                                                    src="https://graph.facebook.com//v2.8/<?php echo $r['fb_uid']; ?>/picture?type=small">
                                                            </div>
                                                            <div class="txt-name-rank"><?php echo $r['display_name']; ?></div>
                                                        </td>
                                                        <td><?php echo number_format($r['overall_gp'], 2); ?></td>
                                                    </tr>
                                                    <?php $row++; ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </table>
                                    </div>
                                    <div class="col-sm-4">
                                        <strong>Top Sgold</strong>
                                        <table>
                                            <!--                                            <tr>-->
                                            <!--                                                <th colspan="2">อันดับ</th>-->
                                            <!--                                                <th>แต้ม</th>-->
                                            <!--                                            </tr>-->
                                            <?php $row = 0; ?>
                                            <?php foreach ($ranking['gold']['now'] as $key => $val) { ?>
                                                <?php $r = $datamap->mapByKey($ranking['keylist']['gold'], $val); ?>
                                                <?php if ($row < 5) { ?>
                                                    <tr>
                                                        <td><?php echo $r['rank']; ?></td>
                                                        <td>
                                                            <div class="box-img-user bx-radius"><img
                                                                    src="https://graph.facebook.com//v2.8/<?php echo $r['fb_uid']; ?>/picture?type=small">
                                                            </div>
                                                            <div class="txt-name-rank"><?php echo $r['display_name']; ?></div>
                                                        </td>
                                                        <td><?php echo number_format($r['overall_sgold'], 2); ?></td>
                                                    </tr>
                                                    <?php $row++; ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </table>
                                    </div>
                                    <div class="col-sm-4">
                                        <strong>Premier League</strong>
                                        <table>
                                            <!--                                            <tr>-->
                                            <!--                                                <th colspan="2">อันดับ</th>-->
                                            <!--                                                <th>แต้ม</th>-->
                                            <!--                                            </tr>-->
                                            <?php $row = 0; ?>
                                            <?php foreach ($ranking['league']['english_premier_league'] as $key => $val) { ?>
                                                <?php $r = $datamap->mapByKey($ranking['keylist']['league'], $val); ?>
                                                <?php if ($row < 5) { ?>
                                                    <tr>
                                                        <td><?php echo $r['current_rank']; ?></td>
                                                        <td>
                                                            <div class="box-img-user bx-radius"><img
                                                                    src="https://graph.facebook.com//v2.8/<?php echo $r['fb_uid']; ?>/picture?type=small">
                                                            </div>
                                                            <div class="txt-name-rank"><?php echo $r['display_name']; ?></div>
                                                        </td>
                                                        <td><?php echo number_format($r['indicator'], 2); ?></td>
                                                    </tr>
                                                    <?php $row++; ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </table>
                                    </div>
                                    <div style="clear: both;"></div>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="h-tabs-game">เลือกทีมทายผลบอลวันนี้</div>
                            <div class="table-game-all mg-bt15" style="background-color: #03385f;">
                                <div class="wrap-game-today wrap-game-today2">
                                    <div id="about" class="nano bx3">
                                        <div class="content">
                                            <?php foreach ($recommendodds['match'] as $key => $val) { ?>
                                                <?php $oddsmatch = $datamap->mapByKey($recommendodds['keylist']['match'], $val); ?>
                                                <?php
                                                $dt = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $oddsmatch['date']);
                                                $dt->addHours(7);
                                                ?>
                                                <?php
                                                $mhdp = "";
                                                $hhdp = "";
                                                $ghdp = "";
                                                if (is_numeric($oddsmatch["hdp"])) {
                                                    $mhdp = $oddsmatch["hdp"];
                                                    $hhdp = $oddsmatch["hdp_home"];
                                                    $ghdp = $oddsmatch["hdp_away"];
                                                } ?>
                                                <div
                                                    class="box-results lnktogame hover-games <?php echo ($key % 2 == 0) ? 'active' : ''; ?>"
                                                    mid="<?php echo $oddsmatch['mid']; ?>">
                                                    <table class="table-list-games table-team-index">
                                                        <tr>
                                                            <td><img
                                                                    src="<?php echo $logoman->getLogo($oddsmatch['hid'], 0); ?>">
                                                                <label
                                                                    class="<?php echo (is_numeric($mhdp) && floatval($mhdp) < 0) ? 'hdp-team' : ''; ?> <?php echo ((is_numeric($mhdp) && floatval($mhdp) == 0) && floatval($hhdp) < floatval($ghdp)) ? 'hdp-team' : ''; ?>"><?php echo $oddsmatch['hn']; ?></label>
                                                            </td>
                                                            <td class="text-right"><?php echo $dt->format("H:i"); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><img
                                                                    src="<?php echo $logoman->getLogo($oddsmatch['gid'], 0); ?>">
                                                                <label
                                                                    class="<?php echo (is_numeric($mhdp) && floatval($mhdp) > 0) ? 'hdp-team' : ''; ?> <?php echo ((is_numeric($mhdp) && floatval($mhdp) == 0) && floatval($hhdp) > floatval($ghdp)) ? 'hdp-team' : ''; ?>"><?php echo $oddsmatch['gn']; ?></label>
                                                            </td>
                                                            <td class="text-right"><label
                                                                    class="info"><?php echo $oddsmatch['hdp'] ?></label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="bx-advertise mg-bt15 box-ads-index">
                                <img src="/images/prize.png" style="width: 90%;">

                                <div class="content-ads">
                                    <div><strong style="color: #fff;">แจกรางวัลทุกเดือน</strong></div>
                                    <span style="color: #ccc;">เล่นเกมส์ทายผลบอลฟรี สะสมเพชรไว้แลกของรางวัลมากมายจากเว็บ</span>
                                </div>
                            </div>

                        </div>
                        <div style="clear: both;"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div style="clear: both;"></div>
    </div>
<?php } ?>

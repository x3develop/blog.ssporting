<?php
if (!isset($_SESSION)) {
    session_start();
}
?>
<input type="hidden" id="statusLogin" value="<?= ((isset($_SESSION['login']))?'true':'false')?>">
<nav class="navbar navbar-inverse navbar-top navbar-topbar">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#" style="padding-top: 12px !important;"><img src="/images/logo/logo-ngoal.png" style="width: 70px;"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" style="padding-right: 0px; padding-left: 0px;">
            <ul class="nav navbar-nav">
                <li><a href="/">หน้าหลัก</a></li>
                <li><a href="/highlight">ไฮไลท์บอล</a></li>
                <li><a href="/news?type=other&page=1">ข่าวบอลตางประเทศ</a></li>
                <li><a href="/news?type=thai&page=1">ข่าวบอลไทย</a></li>
<!--                <li><a href="/fixtures">livescore</a></li>-->
<!--                <li><a href="#">ตารางบอล</a></li>-->
<!--                <li><a href="#">วิเคราะห์บอล</a></li>-->
                <li><a href="/games?mid=<?php echo ((!empty($match))?$match[0]->id:'') ?>">เกมทายผลบอล</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if(isset($_SESSION['login'])){ ?>
                <li><a href=""><img src="/images/coin/sgold100.png" style="width: 20px;float: left; margin-right: 5px;"> <span style="font-size: 14px;"><?php echo number_format($_SESSION['login']['gold'],2)?></span></a> </li>
                <li><a href=""><img src="/images/coin/scoin100.png" style="width: 20px;float: left; margin-right: 5px;"> <span style="font-size: 14px;"><?php echo number_format($_SESSION['login']['coin'],2)?></span></a> </li>
                <?php } ?>
                <li class="<?=((!isset($_SESSION['login']))?'hide':'')?>" id="fb-online">
                    <a href="#" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <img id="fb-picture" style="width: 20px"
                             src="<?=((isset($_SESSION['login']))?'https://graph.facebook.com/v2.8/'.$_SESSION['login']['fb_uid'].'/picture?type=small':'')?>">
                        <label id="fb-name"><?=((isset($_SESSION['login']))?$_SESSION['login']['name']:'')?></label>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-profile" aria-labelledby="dropdownMenu1">
                        <li><a id="own-profile-link" href="/profile"><i class="fa fa-user" aria-hidden="true"></i> Profile</a></li>
                        <li><a id="facebook-logoutbutton" href="/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
                    </ul>
                </li>
<!--                <button id="loginbutton"><img  style="margin-top: 2px;" src="/images/icon/login-face180.png"></button>-->
<!--                <div class="login-facebook" id="facebook-loginbutton">-->
<!--                    <i><img src="/images/icon/bt-facebook.png" width="65"></i>-->
<!--                </div>-->
<!--                <li><a href="#">สมัครสมาชิก</a></li>-->
                <?php if(!isset($_SESSION['login'])){ ?>
                    <!--                                <div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>-->
                    <li id="login-button" style="padding: 8px;color: #003a6c;font-weight: bolder;">
                        <fb:login-button style="cursor: pointer" data-size="large" scope="public_profile,email" onlogin="checkLoginState();">เข้าสู่ระบบด้วย Facebook </fb:login-button>
                    </li>
                <?php } ?>
<!--                <li id="login-button" style="display: none;">-->
<!--                    <fb:login-button scope="public_profile,email" onlogin="checkLoginState();"></fb:login-button>-->
<!--                </li>-->
            </ul>
        </div>
        <!--/.navbar-collapse -->
    </div>
</nav>
<?php
    $dataDailyIncome=false;
    $dataNewUserIncome=false;
?>
<?php if(isset($_SESSION['login'])){
    if(empty($playLogIncome)) {
        require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playLogIncome.php";
        $playLogIncome=new playLogIncome();
    }
    $dataDailyIncome=$playLogIncome->checkDailyIncome($_SESSION['login']['id']);
    $dataNewUserIncome=$playLogIncome->checkNewUserIncome($_SESSION['login']['id']);
?>
<div id="model-login-daily" class="modal fade" style="" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="width: 70%;">
        <div class="modal-content" style="position: relative; background: none;">
            <img style="width: 100%;" src="/images/login-daily/login-daily-<?php echo ($_SESSION['login']['combo_login']%7);?>.png">
            <div style="position: absolute;bottom:0;right:0;z-index: 100;width: 20%;padding: 2% 5%;font-size: 18px;font-weight: bolder;color: #fff;background-color: #0e505e;">
                <a href="/play/dailyCoin" style="color: #fff;text-decoration: none;">รับ Coin</a>
            </div>
        </div>
    </div>
</div>

<div id="model-new-user" class="modal fade" style="" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="width: 50%;">
        <div class="modal-content" style="position: relative; background: none;">
            <img style="width: 100%;" src="/images/login-daily/new-user.png">
            <div style="position: absolute;bottom:0;right:0;z-index: 100;width: 20%;height: 20%;padding: 7% 5%;font-size: 18px;font-weight: bolder;color: #fff;">
                <a href="/play/newUserCoin?pathname=<?php echo $_SERVER['REQUEST_URI'];?>" style="color: #fff;text-decoration: none;">รับ Coin</a>
            </div>
        </div>
    </div>
</div>

<?php } ?>

<?php if($dataDailyIncome){ ?>
<script>
    $(document).ready(function () {
        $('div#model-login-daily').modal({
            backdrop: 'static',
            keyboard: false
        });
    })
</script>
<?php } ?>
<?php if($dataNewUserIncome){ ?>
<script>
    $(document).ready(function () {
        $('div#model-new-user').modal({
            backdrop: 'static',
            keyboard: false
        });
    })
</script>
<?php } ?>

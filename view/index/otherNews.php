<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/24/2017
 * Time: 10:53
 */
?>
<div class="container">
    <div class="wrap-box-news rows">
        <div class="tab-h-content">
            <div class="pull-left"><span>ข่าวกีฬาอื่นๆ</span></div>
            <a class="pull-right more" href="/news?type=other&Page=1">More...</a>

            <div style="clear: both;"></div>
        </div>

        <div class="wrap-box-content-news">
            <div class="box-other-news">
                <div class="rows">
                    <?php for ($i = 0; $i < 3; $i++) { ?>
                        <?php if (array_key_exists($i, $othernews)) { ?>
                            <div class="col-sm-4" newsid="<?php echo $othernews[$i]->newsid; ?>">
                                <div class="tab-color"></div>
                                <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                        class="how-long"><?php echo $othernews[$i]->createDatetime; ?></span></div>
                                <div class="title-owl-carousel"><?php echo $othernews[$i]->titleTh; ?>
                                </div>
                                <div class="box-videos img-other-news">
                                    <div class="hilight-cover"></div>
                                    <a href="/articles?newsid=<?php echo $othernews[$i]->newsid; ?>">
                                        <img src="<?php echo $othernews[$i]->imageLink; ?>">
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <div class="box-other-news">
                <div class="rows">
                    <?php for ($i = 3; $i < count($othernews); $i++) { ?>
                        <?php if (array_key_exists($i, $othernews)) { ?>
                            <div class="col-sm-3" newsid="<?php echo $othernews[$i]->newsid; ?>">
                                <div class="tab-color"></div>
                                <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                        class="how-long"><?php echo $othernews[$i]->createDatetime; ?></span></div>
                                <div class="title-owl-carousel"><?php echo $othernews[$i]->titleTh; ?>
                                </div>
                                <div class="box-videos">
                                    <div class="hilight-cover"></div>
                                    <a href="/articles?newsid=<?php echo $othernews[$i]->newsid; ?>">
                                        <img src="<?php echo $othernews[$i]->imageLink; ?>">
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <div style="clear: both;"></div>
        </div>
    </div>
</div>
<div style="clear:both;"></div>

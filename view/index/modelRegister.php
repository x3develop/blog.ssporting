<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 4/26/2018
 * Time: 17:57
 */
if (!isset($_SESSION)) {
    session_start();
}
?>

<div class="modal fade"  id="myModalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form id="formLogin" class="form-horizontal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Login</h4>
                </div>
                <div class="modal-body" style="padding: 0 5%;">
                    <div class="form-group" style="margin-top: 15px;">
                        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" name="login[username]" class="form-control" id="inputEmail3" placeholder="Username&Email">
                            <small id="errorUsername" class="form-text text-muted " style="color: #ff3f4d;"></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" name="login[passage]" class="form-control" id="inputPassword3" placeholder="Password">
                            <small id="errorPassword" class="form-text text-muted " style="color: #ff3f4d;"></small>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="text-align: center;">
<!--                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
<!--                    <fb:login-button style="cursor: pointer" data-size="large" scope="public_profile,email" onlogin="checkLoginState();">เข้าสู่ระบบด้วย Facebook </fb:login-button>-->
                    <?php if (!isset($_SESSION['login'])) { ?>
                        <a class="btn btn-default fb-login-button" onclick="FBlogin();">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                            <span>Login</span>
                        </a>
                    <?php } ?>
                    <button type="submit" class="btn btn-primary">Login</button>
                    <button type="button" data-toggle="modal" data-target="#myModalRegister" class="btn btn-success">Register</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade " style="" id="myModalRegister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form id="formRegister" class="form-horizontal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabelRegister">Register</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="register[id]" value="<?php echo (isset($_SESSION['login']['id'])?$_SESSION['login']['id']:'');?>">
                    <div class="form-group" style="margin-top: 15px;">
                        <label for="inputEmail3" class="col-sm-3 control-label">UserName</label>
                        <div class="col-sm-9">
                            <input value="<?php echo (isset($_SESSION['login']['username'])?$_SESSION['login']['username']:'');?>" required type="text" name="register[username]" class="form-control" id="inputEmail3" placeholder="UserName">
                            <small id="errorRegisterUsername" class="form-text text-muted " style="color: #ff3f4d;"></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-9">
                            <input required type="password" name="register[password]" class="form-control" id="inputPassword3" placeholder="Password">
                            <small id="errorRegisterPassword" class="form-text text-muted " style="color: #ff3f4d;"></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">ConfrimPassword</label>
                        <div class="col-sm-9">
                            <input required type="password" name="register[confrimpassword]" class="form-control" id="inputPassword3" placeholder="ConfrimPassword">
                            <small id="errorRegisterConfrimPassword" class="form-text text-muted " style="color: #ff3f4d;"></small>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 15px;">
                        <label for="inputEmail3" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">
                            <input value="<?php echo (isset($_SESSION['login']['name'])?$_SESSION['login']['name']:'');?>" required type="text" name="register[name]" class="form-control" id="inputEmail3" placeholder="Name">
                            <small id="errorRegisterName" class="form-text text-muted " style="color: #ff3f4d;"></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                            <input value="<?php echo (isset($_SESSION['login']['email'])?$_SESSION['login']['email']:'');?>" required type="email" name="register[email]" class="form-control" id="inputPassword3" placeholder="Email">
                            <small id="errorRegisterEmail" class="form-text text-muted " style="color: #ff3f4d;"></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">picture</label>
                        <div class="col-sm-9" style="">
                            <img id="register-picture" style="width: 100px;margin: 5px;" src="<?php echo (isset($_SESSION['login']['path'])?$_SESSION['login']['path']:'/images/avatar.png');?>">
                            <label id="labelUpload" for="inputUpload" class="btn btn-warning m-0">UpLoad</label>
                            <input type="hidden" value="<?php echo (isset($_SESSION['login']['path'])?$_SESSION['login']['path']:'');?>" name="register[path]" class="form-control" id="inputPassword3" placeholder="path">
                            <small id="errorRegisterPath" class="form-text text-muted " style="color: #ff3f4d;"></small>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="btn_edit" type="submit" class="btn btn-warning hide">Edit</button>
                    <button id="btn_register" type="submit" class="btn btn-primary">Register</button>
                </div>
            </div>
        </form>
    </div>
</div>
<form style="display: none;" id="formUpload" method="post" enctype="multipart/form-data">
    <input id="inputUpload" type="file" data-max-size="50000" name="Filedata[]" multiple="multiple" accept="image/*" ><br>
</form>

<script>
    $(document).on('click', "#labelUpload", function (e){
        $('input[name=id]').val($(this).attr('data-id'));
    });
    $(document).on('change', "#inputUpload", function (e){
        $("#formUpload").submit();
    });
    $(document).on('submit', "#formUpload", function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        $('input[type=file][data-max-size]').each(function(){
            if(typeof this.files[0] !== 'undefined'){
                var maxSize = parseInt($(this).attr('data-max-size'),10),size = this.files[0].size;
                console.log(maxSize+" :: "+size);
                if(maxSize > size){
                    $.ajax({
                        type: 'POST',
                        url: "/play/uploadFileRegister.php",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            response=jQuery.parseJSON(response);
                            $('img#register-picture').attr('src',response+'#'+Math.floor((Math.random() * 1000) + 1));
                            $('input[name="register[path]"]').val(response);
                            $('img#player-'+$('#formUpload').find('input[name=id]').val()).attr('src',response+'#'+Math.floor((Math.random() * 1000) + 1));
                        },
                        error: function (data) {
                            console.log("error");
                            console.log(data);
                        }
                    });
                }else{
                    alert("ไฟลมีขนาดใหญ่เกินไป");
                }
            }
        });
    });
    $(document).on('focusout','form#formRegister input',function () {
        if($(this).attr('name')=="register[username]"){
            var username=$(this).val();
            var id=$('form#formRegister').find('input[name="register[id]"]').val();
            $.ajax({
                url: "/play/checkUsername.php",
                data: {username:username,id:id},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                if (response['status']) {
                    $('#errorRegisterUsername').html("Username duplicate !!");
                }else{
                    $('#errorRegisterUsername').html("");
                }
            });
        }else if($(this).attr('name')=="register[email]"){
            var email=$(this).val();
            var id=$('form#formRegister').find('input[name="register[id]"]').val();
            if(isEmail(email)) {
                $.ajax({
                    url: "/play/checkEmail.php",
                    data: {email: email,id:id},
                    method: "GET",
                    dataType: "json"
                }).done(function (response) {
                    if (response['status']) {
                        $('#errorRegisterEmail').html("Email duplicate !!");
                    } else {
                        $('#errorRegisterEmail').html("");
                    }
                });
            }
        }else if($(this).attr('name')=="register[confrimpassword]") {
            var password=$('form#formRegister').find('input[name="register[password]"]').first().val();
            var cpassword=$('form#formRegister').find('input[name="register[confrimpassword]"]').first().val();
            if(cpassword!=="" && cpassword>=password && password!=cpassword){
                $('small#errorRegisterConfrimPassword').html("confirm password not math password");
            }else{
                $('small#errorRegisterConfrimPassword').html("");
            }
        }
    });
    $(document).on('keyup','form#formRegister input',function () {
        if($(this).attr('name')=="register[username]"){
            var username=$(this).val();
            var id=$('form#formRegister').find('input[name="register[id]"]').val();
            $.ajax({
                url: "/play/checkUsername.php",
                data: {username:username,id:id},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                if (response['status']) {
                    $('#errorRegisterUsername').html("Username duplicate !!");
                }else{
                    $('#errorRegisterUsername').html("");
                }
            });
        }else if($(this).attr('name')=="register[email]"){
            var email=$(this).val();
            var id=$('form#formRegister').find('input[name="register[id]"]').val();
            if(isEmail(email)) {
                $.ajax({
                    url: "/play/checkEmail.php",
                    data: {email: email,id:id},
                    method: "GET",
                    dataType: "json"
                }).done(function (response) {
                    if (response['status']) {
                        $('#errorRegisterEmail').html("Email duplicate !!");
                    } else {
                        $('#errorRegisterEmail').html("");
                    }
                });
            }
        }else if($(this).attr('name')=="register[confrimpassword]") {
            var password=$('form#formRegister').find('input[name="register[password]"]').first().val();
            var cpassword=$('form#formRegister').find('input[name="register[confrimpassword]"]').first().val();
            console.log(password+"::"+cpassword);
            if(cpassword!=="" && cpassword>=password && password!=cpassword){
                $('small#errorRegisterConfrimPassword').html("confirm password not math password");
            }else{
                $('small#errorRegisterConfrimPassword').html("");
            }
        }
    });

    $(document).on('submit', "#formRegister", function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        if($(this).find('input[name="register[username]"]').val()!=="") {
            var username=$('#formRegister').find('input[name="register[username]"]').val();
            var id=$('form#formRegister').find('input[name="register[id]"]').val();
            $.ajax({
                url: "/play/checkUsername.php",
                data: {username:username,id:id},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                if (response['status']) {
                    $('#errorRegisterUsername').html("Username duplicate !!");
                }else{
                    var email=$('#formRegister').find('input[name="register[email]"]').val();
                    var id=$('form#formRegister').find('input[name="register[id]"]').val();
                    if(isEmail(email)) {
                        $.ajax({
                            url: "/play/checkEmail.php",
                            data: {email: email,id:id},
                            method: "GET",
                            dataType: "json"
                        }).done(function (response) {
                            if (response['status']) {
                                $('#errorRegisterEmail').html("Email duplicate !!");
                            } else {
                                var password = $('form#formRegister').find('input[name="register[password]"]').first().val();
                                var cpassword = $('form#formRegister').find('input[name="register[confrimpassword]"]').first().val();
                                if (cpassword !== "" && cpassword >= password && password != cpassword) {
                                    $('small#errorRegisterConfrimPassword').html("confirm password not math password");
                                } else {
                                    if($(this).find('input[name="register[path]"]').val()=="") {
                                        $(this).find('small#errorRegisterPath').html("กรุณากรอกเลือกรูป");
                                    }else{
                                        $.ajax({
                                            type: 'POST',
                                            url: "/play/checkOutIn.php",
                                            data: formData,
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            success: function (response) {
                                                response = jQuery.parseJSON(response);
                                                if (response['status'] !== "success") {
                                                    $('#' + response['status']).html(response['message']);
                                                } else {
                                                    if(window.location.pathname=="/profile.php"){
                                                        window.location.reload();
                                                    }else {
                                                        $('#fb-gold').removeClass('hide');
                                                        $('#fb-coin').removeClass('hide');
                                                        $('#fb-online').removeClass('hide');
                                                        $('#login-button').addClass('hide');
                                                        $('#fb-gold-text').html(parseFloat(response['data']['gold']).toFixed(2));
                                                        $('#fb-coin-text').html(parseFloat(response['data']['coin']).toFixed(2));
                                                        $('#fb-name').html(response['data']['name']);
                                                        if (response['data']['fb_uid']) {
                                                            $('#fb-picture').attr("src", "https://graph.facebook.com/v2.8/" + response['data']['fb_uid'] + "/picture?type=small");
                                                        } else if (response['data']['path']) {
                                                            $('#fb-picture').attr("src", response['data']['path']);
                                                        } else {
                                                            $('#fb-picture').attr("src", "/images/avatar.png");
                                                        }
                                                        $('#myModalRegister').modal('hide');
                                                        $('#formLogin').modal('hide');
                                                        $('#model-new-user').modal({
                                                            backdrop: 'static',
                                                            keyboard: false
                                                        });
                                                    }
                                                }
                                            },
                                            error: function (data) {
                                                console.log("error");
                                                console.log(data);
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }
    });

    $(document).on('submit', "#formLogin", function (e) {
        e.preventDefault();

        if($(this).find('input[type="text"]').val()==""){
            $('small#errorPath').html("กรุณาเลือกรูป");
        }else{
            $('small#errorPath').html("");
        }

        if($(this).find('input[type="password"]').val()==""){
            $('small#errorPassword').html("กรุณากรอกข้อมูล Password");
        }else{
            $('small#errorPassword').html("");
        }
        if($(this).find('input[type="text"]').val()!=="" && $(this).find('input[type="password"]').val()!=="") {
            var formData = new FormData(this);
            $.ajax({
                type: 'POST',
                url: "/play/checkOutIn.php",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    response = jQuery.parseJSON(response);
                    if(response['status']!=="success"){
                        $('#'+response['status']).html(response['message']);
                    }else{
                        $('#fb-gold').removeClass('hide');
                        $('#fb-coin').removeClass('hide');
                        $('#fb-online').removeClass('hide');
                        $('#login-button').addClass('hide');
                        $('#fb-gold-text').html(parseFloat(response['data']['gold']).toFixed(2));
                        $('#fb-coin-text').html(parseFloat(response['data']['coin']).toFixed(2));
                        $('#fb-name').html(response['data']['name']);
                        if(response['data']['fb_uid']){
                            $('#fb-picture').attr("src","https://graph.facebook.com/v2.8/"+response['data']['fb_uid']+"/picture?type=small");
                        }else if(response['data']['path']){
                            $('#fb-picture').attr("src", response['data']['path']);
                        }else{
                            $('#fb-picture').attr("src","/images/avatar.png");
                        }
                        $('#myModalLogin').modal('hide');
                    }
                },
                error: function (data) {
                    console.log("error");
                    console.log(data);
                }
            });
        }
    });

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
</script>

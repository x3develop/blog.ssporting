<?php
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/SitemapSearch.php';
$siteMapSearchObject=new SitemapSearch();
$data=$siteMapSearchObject->getByLimit(25);
?>
<style type="text/css">
    nav.footer {
        /*background-color: #02335c;*/
        background: linear-gradient(to right, #02335c, #0091c8);
        background: -webkit-linear-gradient(to right, #02335c, #0091c8);
        background: -moz-linear-gradient(to right, #02335c, #0091c8);
        background: -o-linear-gradient(to right, #02335c, #0091c8);
        background: linear-gradient(to right, #02335c, #0091c8);
    }

    nav.footer .contact-social i {
        color: #02335c;
        background-color: #fff;
        height: 20px;
        line-height: 20px;
        text-align: center;
        width: 20px;
        border-radius: 50%;

    }
    .row-footer ul li,
    nav.footer ul li{
        margin: 10px 0;
        display: inline-block;
    }
    .link-footer li a:hover{
        color: #fff;
        text-decoration: underline;
    }

    .link-footer li a{
        padding: 0 10px;
        border-left:solid 1px #6b99c0;
    }

</style>
<nav class="footer">

    <div class="container container-keyword-search">
<!--        <div class="full-width">-->
<!--            <div class="text-title">-->
<!--                <h4>คำที่ถูกค้นหาล่าสุด 25 คำ</h4>-->
<!--            </div>-->
<!--        </div>-->
        <div class="full-width">
            <?php foreach ($data as $key=>$value){ ?>
                <a class="label label-primary" href="search?q=<?php echo $value->text;?>"><?php echo $value->text;?></a>
            <?php } ?>
        </div>
    </div>


    <div class="container">
        <div class="col-md-12 hide">
            <div class="contact-social text-center">
                <ul>
                    <li>
                        <a href="https://www.facebook.com/nGoalTH/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="https://twitter.com/nGoalTH"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </li>
                </ul>
            </div>
        </div>


        <div class="pull-right">

        </div>
        <div style="clear: both;"></div>
    </div>
</nav>
<div class="row-footer full-width">
    <div class="container">
        <div class="bt-sstatic1">
            <!--            <p>@Sport Ball Limited 2017</p>-->
            <!-- Histats.com  (div with counter) -->
            <div id="histats_counter"></div>
        </div>
        <div class="col-md-12 text-center">
            <ul class="link-footer">
                <li><a style="border: 0;" href="/news?type=premier league&Page=1">ข่าวพรีเมียร์ลีก</a></li>
                <li><a href="https://www.ngoal.com">บ้านผลบอล</a></li>
                <li><a href="https://www.ngoal.com/livescore">ผลบอล</a></li>
                <li><a href="/videos?category=premier league">พรีเมียร์ลีก</a></li>
                <li><a href="/news?type=thai&Page=1">ข่าวบอลไทย</a></li>
                <li><a href="/other-videos">HotHit Video</a></li>
                <li><a href="/games?mid=<?php echo((!empty($match)) ? $match[0]->id : '') ?>">เกมทายผลบอล</a>
                </li>
                <li><a href="/news?type=thai&Page=1">ข่าวฟุตบอลทีมชาติ</a></li>
                <li><a href="/news?type=ucl&Page=1">ข่าวยูฟ่าแชมเปียนลีก</a></li>

            </ul>
        </div>
    </div>
</div>

<!-- Histats.com  START  (aync)-->
<script type="text/javascript">var _Hasync= _Hasync|| [];
    _Hasync.push(['Histats.start', '1,4097584,4,603,110,40,00010101']);
    _Hasync.push(['Histats.fasi', '1']);
    _Hasync.push(['Histats.track_hits', '']);
    (function() {
        var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
        hs.src = ('//s10.histats.com/js15_as.js');
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
    })();</script>
<noscript>
    <a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?4097584&101" alt="html hit counter" border="0"></a>
</noscript>
<!-- Histats.com  END  -->
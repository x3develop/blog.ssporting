<?php
if (!isset($_SESSION)) {
    session_start();
}
?>
<script>
    $('#myAffix').affix({
        offset: {
            top: 100,
            bottom: function () {
                return (this.bottom = $('.footer').outerHeight(true))
            }
        }
    })

    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');

    });
</script>

<input type="hidden" id="statusLogin" value="<?= ((isset($_SESSION['login'])) ? 'true' : 'false') ?>">
<div class="content-menu-index" data-spy="affix" data-offset-top="60">
    <nav class="navbar navbar-topbar">

        <div class="navbar-header">
            <div class="pull-left">
                <a class="navbar-brand" href="#">
                    <img src="/images/logo/logo-ngoal.png">
                </a>
            </div>
            <div class="pull-right navbar-header-right">
                <ul>

                    <li>
                        <div class="box-login-FB">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                            <span>Login</span>
                        </div>
                    </li>
                    <li>
                        <div class="user-topmenu collapsed" data-toggle="collapse"
                             data-target="#userbar" aria-expanded="false" aria-controls="userbar">
                            <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                        </div>
                    </li>
                    <li>
                        <div class="collapsed collapse-menu-btn" data-toggle="collapse"
                             data-target="#navbar"
                             aria-expanded="false" aria-controls="navbar">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </div>
                    </li>
                </ul>

            </div>
        </div>
        <div id="userbar" class="userbar-collapse collapse">
            <div class="userbar-collapse-body">
                <ul>
                    <li class="user-detail text-center">
                        <img src="/images/9.jpg">
                        <span><strong>Wayne Rooney</strong></span>
                    </li>
                    <li class="text-center user-coin-box">
                        <table>
                            <tr>
                                <td><img src="/images/coin/sgold100.png"><span>2,668.00</span></td>
                            </tr>
                            <tr>
                                <td><img src="/images/coin/scoin100.png"><span>3,200.00</span></td>
                            </tr>
                        </table>
                    </li>
                    <li>
                        <button type="button" class="btn btn-primary btn-lg">Edit Profile</button>
                    </li>
                    <li>
                        <button type="button" class="btn btn-danger btn-lg">Log out</button>
                    </li>
                </ul>
            </div>
        </div>
        <!--        เมนูใน smartphone-->
        <div id="navbar" class="collapse collapse-menu">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <!--                หน้าหลัก-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="/">หน้าหลัก</a>
                        </h4>
                    </div>
                </div>
                <!--                วีดีโอ/ไฮไลท์บอล-->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                วีดีโอ/ไฮไลท์บอล
                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <ul>
                                <li><a href="/highlight?category=premier league">พรีเมียร์ลีก</a></li>
                                <li><a href="/highlight?category=la liga">ลาลีกา</a></li>
                                <li><a href="/highlight?category=bundesliga">บุนเดสลีกา</a></li>
                                <li><a href="/highlight?category=serie a">ซีเรียอา</a></li>
                                <li><a href="/highlight?category=league 1">ลีกเอิง</a></li>
                                <li><a href="/highlight?category=thai">ไทยพรีเมียลีก</a></li>
                                <li><a href="/highlight?category=ucl">ยูฟ่าแชมเปียนลีก</a></li>
                                <li><a href="/highlight?category=europa">ยูโรป้า</a></li>
                                <li><a href="/highlight?category=international">ฟุตบอลทีมชาติ</a></li>
                                <li><a href="/highlight#videosOthersTab">ไฮไลท์ Ngoal</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--                ข่าวบอลต่างประเทศ-->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                ข่าวบอลต่างประเทศ
                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingThree">
                        <div class="panel-body">
                            <ul>
                                <li><a href="/news?type=premier league&Page=1">ข่าวพรีเมียร์ลีก</a></li>
                                <li><a href="/news?type=la liga&Page=1">ข่าวลาลีกา</a></li>
                                <li><a href="/news?type=bundesliga&Page=1">ข่าวบุนเดสลีกา</a></li>
                                <li><a href="/news?type=serie a&Page=1">ข่าวซีเรียอา</a></li>
                                <li><a href="/news?type=ucl&Page=1">ข่าวยูฟ่าแชมเปียนลีก</a></li>
                                <li><a href="/news?type=europa&Page=1">ข่าวยูโรป้า</a></li>
                                <li><a href="/news?type=thai&Page=1">ข่าวฟุตบอลทีมชาติ</a></li>
                                <li><a href="/news?type=other league&Page=1">ข่าวฟุตบอลลีกอื่น</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--                ข่าวบอลไทย-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="/news?type=thai&Page=1">ข่าวบอลไทย</a>
                        </h4>
                    </div>
                </div>
                <!--                ข่าวกีฬาอื่นๆ-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="/news?type=other&Page=1">ข่าวกีฬาอื่นๆ</a>
                        </h4>
                    </div>
                </div>
                <!--                Sexy Football-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#">Sexy Football</a>
                        </h4>
                    </div>
                </div>
            </div>
        </div>


        <div class="content-wrapper-menu">
            <!--   แทบบนสุด-->
            <div class="topmenu">
                <div class="container">
                    <div class="pull-left contact-social">
                        <ul>
                            <li>
                                <a href="https://www.facebook.com/nGoalTH/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="https://twitter.com/nGoalTH"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="pull-right box-login-FB">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                        <span>Login</span>
                    </div>
                    <div class="pull-right content-profile-menu">
                        <ul>
                            <?php if (isset($_SESSION['login'])) { ?>
                                <li class="coin-topmenu"><a href=""><img src="/images/coin/sgold100.png">
                                        <span><?php echo number_format($_SESSION['login']['gold'], 2) ?></span></a>
                                </li>
                                <li class="coin-topmenu"><a href=""><img src="/images/coin/scoin100.png">
                                        <span><?php echo number_format($_SESSION['login']['coin'], 2) ?></span></a>
                                </li>
                            <?php } ?>
                            <li class="<?= ((!isset($_SESSION['login'])) ? 'hide' : '') ?>" id="fb-online">
                                <a href="#" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="true">
                                    <img id="fb-picture"
                                         src="<?= ((isset($_SESSION['login'])) ? 'https://graph.facebook.com/v2.8/' . $_SESSION['login']['fb_uid'] . '/picture?type=small' : '') ?>">
                                    <label id="fb-name"><?= ((isset($_SESSION['login'])) ? $_SESSION['login']['name'] : '') ?></label>
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-profile" aria-labelledby="dropdownMenu1">
                                    <li><a id="own-profile-link" href="/profile"><i class="fa fa-user"
                                                                                        aria-hidden="true"></i>
                                            Profile</a></li>
                                    <li><a id="facebook-logoutbutton" href="/logout"><i class="fa fa-sign-out"
                                                                                            aria-hidden="true"></i>
                                            Logout</a>
                                    </li>
                                </ul>
                            </li>
                            <!--                <button id="loginbutton"><img  style="margin-top: 2px;" src="/images/icon/login-face180.png"></button>-->
                            <!--                <div class="login-facebook" id="facebook-loginbutton">-->
                            <!--                    <i><img src="/images/icon/bt-facebook.png" width="65"></i>-->
                            <!--                </div>-->
                            <!--                <li><a href="#">สมัครสมาชิก</a></li>-->
                            <?php if (!isset($_SESSION['login'])) { ?>
                                <!--                                <div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>-->
                                <li id="login-button" style="padding: 8px;color: #003a6c;font-weight: bolder;">
                                    <fb:login-button style="cursor: pointer" data-size="large"
                                                     scope="public_profile,email"
                                                     onlogin="checkLoginState();">เข้าสู่ระบบด้วย Facebook
                                    </fb:login-button>
                                </li>
                            <?php } ?>
                            <!--                <li id="login-button" style="display: none;">-->
                            <!--                    <fb:login-button scope="public_profile,email" onlogin="checkLoginState();"></fb:login-button>-->
                            <!--                </li>-->
                        </ul>
                    </div>

                </div>
            </div>

            <!--เมนูแถบสีขาว-->
            <div class="bar-menu">
                <div class="container">
                    <div class="bx-headtop rows">
                        <div class="logo col-sm-4" style=""><img src="/images/logo/logo-ngoal.png"></div>
                        <div class="col-sm-8">
                            <div class="section-login" style="width: 100%;">
                                <a href="/highlight">
                                    <img style="height: 45px; margin-right: 5px;" src="/images/banner-hd_preview.png">
                                </a>
                                <a href="/games?mid=<?php echo((!empty($match)) ? $match[0]->id : '') ?>">
                                    <img style="height: 45px;" src="/images/banner-game.png">
                                </a>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>
            </div>

            <!--/.navbar-collapse -->

            <div class="main-menu-index navbar-top" style="padding-left: 0;">
                <div class="container">
                    <div class="pull-left logo-menu">
                        <a href="/"><img src="/images/logo/logo-ngoal-white.png"></a>
                    </div>
                    <div class="nav-menutop pull-left nav-menu-main">
                        <ul class="nav nav-pills">
                            <li><a href="/">หน้าหลัก</a></li>

                            <li class="active-dropdown">
                                <div class="hot-icon">
                                    <img src="/images/source.gif">
                                </div>
                                <a href="/highlight">วีดีโอ/ไฮไลท์บอล <i class="fa fa-chevron-down"
                                                                          aria-hidden="true"></i></a>
                                <div class="bx-menu-dropdown">

                                    <div class="container">
                                        <div class="col-md-2 list-content-big-menu">
                                            <h3>วีดีโอ/ไฮไลท์บอล</h3>
                                            <ul>
                                                <li>
                                                    <a href="/highlight">วีดีโอ/ไฮไลท์บอล</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li class="active">
                                                    <a href="/highlight?category=premier league">พรีเมียร์ลีก</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/highlight?category=la liga">ลาลีกา</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/highlight?category=bundesliga">บุนเดสลีกา</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/highlight?category=serie a">ซีเรียอา</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/highlight?category=league 1">ลีกเอิง</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/highlight?category=thai">ไทยพรีเมียลีก</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/highlight?category=ucl">ยูฟ่าแชมเปียนลีก</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/highlight?category=europa">ยูโรป้า</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/highlight?category=international">ฟุตบอลทีมชาติ</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="col-md-6 detail-content-big-menu">
                                            <div class="col-sm-12">
                                                <h3 class="pull-left"><i class="fa fa-youtube-play"
                                                                         aria-hidden="true"></i> ไฮไลท์พรีเมียร์ลีก</h3>
                                                <a class="pull-right"
                                                   href="/highlight?category=premier league">ทั้งหมด</a>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="thumbnail">
                                                            <div class="box-videos">
                                                                <div class="hilight-cover"></div>
                                                                <div class="icon-play"><i class="fa fa-play"></i></div>
                                                                <img src="http://api.ngoal.com/uploaded/video/2705/Image_2705.jpg">
                                                            </div>
                                                            <div class="caption">
                                                                <h3>[Highlights] Stoke City VS Manchester City,Premier
                                                                    League,12-03-2017-18</h3>
                                                                <p><a href="#" class="btn btn-primary btn-sm"
                                                                      role="button">Read
                                                                        More</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="thumbnail">
                                                            <div class="box-highlight">
                                                                <div class="hilight-cover"></div>
                                                                <div class="icon-play"><i class="fa fa-play"></i></div>
                                                                <img src="http://api.ngoal.com/uploaded/video/2684/Image_2684.jpg">
                                                            </div>
                                                            <div class="caption">
                                                                <h3>[Highlights] Inter VS SSC Napoli,Italy Calcio Serie
                                                                    A,11-03-2017-18 </h3>
                                                                <p><a href="#" class="btn btn-primary btn-sm"
                                                                      role="button">Read
                                                                        More</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 detail-content-big-menu">
                                            <div class="col-sm-12">
                                                <h3 class="pull-left">
                                                    <i class="fa fa-youtube-play" aria-hidden="true"></i> Video Ngoal
                                                </h3>
                                                <a class="pull-right" href="/highlight#videosOthersTab">ทั้งหมด</a>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="thumbnail">
                                                            <div class="box-videos" style="height: 185px;">
                                                                <div class="hilight-cover"></div>
                                                                <div class="icon-play"><i class="fa fa-play"></i></div>
                                                                <img src="https://cf-e2.streamablevideo.com/image/sofxs.jpg">
                                                            </div>
                                                            <div class="caption">
                                                                <h3>[Highlights] Inter VS SSC Napoli,Italy Calcio Serie
                                                                    A,11-03-2017-18 </h3>
                                                                <p><a href="#" class="btn btn-primary btn-sm"
                                                                      role="button">Read
                                                                        More</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="active-dropdown">
                                <a href="">ข่าวบอลต่างประเทศ <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                                <div class="bx-menu-dropdown">

                                    <div class="container">
                                        <div class="col-md-2 list-content-big-menu">
                                            <h3>ข่าวฟุตบอลต่างประเทศ</h3>
                                            <ul>
                                                <li>
                                                    <a href="/news?type=premier league&Page=1">ข่าวพรีเมียร์ลีก</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li class="active">
                                                    <a href="/news?type=la liga&Page=1">ข่าวลาลีกา</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/news?type=bundesliga&Page=1">ข่าวบุนเดสลีกา</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/news?type=serie a&Page=1">ข่าวซีเรียอา</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/news?type=ucl&Page=1">ข่าวยูฟ่าแชมเปียนลีก</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/news?type=europa&Page=1">ข่าวยูโรป้า</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/news?type=thai&Page=1">ข่าวฟุตบอลทีมชาติ</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/highlight?category=ucl">ยูฟ่าแชมเปียนลีก</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/news?type=other league&Page=1">ข่าวฟุตบอลลีกอื่น</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="col-md-10 detail-content-big-menu">
                                            <div class="col-sm-12">
                                                <h3 class="pull-left"><i class="fa fa-youtube-play"
                                                                         aria-hidden="true"></i> ข่าวลาลีกา</h3>
                                                <a class="pull-right"
                                                   href="/highlight?category=premier league">ทั้งหมด</a>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="thumbnail">
                                                            <div class="bx-story-small">
                                                                <div class="hilight-cover"></div>
                                                                <a href="/articles?newsid=32239" class="crop"
                                                                   style="background-image: url(http://api.ngoal.com/uploaded/news/32239/newsImage_32239.jpg)"></a>
                                                            </div>
                                                            <div class="caption">
                                                                <h3>ยอดเพิ่ม! “กิเลน-กว่าง” แฟนเยอะสุดไทยลีกวีก 4
                                                                    ทะลุหลักหมื่น</h3>
                                                                <p><a href="#" class="btn btn-primary btn-sm"
                                                                      role="button">Read
                                                                        More</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="thumbnail">
                                                            <div class="bx-story-small">
                                                                <div class="hilight-cover"></div>
                                                                <a href="/articles?newsid=32232" class="crop"
                                                                   style="background-image: url(http://api.ngoal.com/uploaded/news/32232/newsImage_32232.jpg)"></a>
                                                            </div>
                                                            <div class="caption">
                                                                <h3>รองฝูงข้าใครอย่าแตะ! ผีฆ่าไม่ตายแซงดับพาเลซทดเจ็บ
                                                                    3-2</h3>
                                                                <p><a href="#" class="btn btn-primary btn-sm"
                                                                      role="button">Read
                                                                        More</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="thumbnail">
                                                            <div class="bx-story-small">
                                                                <div class="hilight-cover"></div>
                                                                <a href="/articles?newsid=32253"
                                                                   class="crop imgTest3"
                                                                   style="background-image: url(http://api.ngoal.com/uploaded/news/32253/newsImage_32253.jpg)"></a>
                                                            </div>
                                                            <div class="caption">
                                                                <h3>เป็นงานผม! เด เคอา โพสท์ภาพเซฟสุดเอื้อมพร้อมแคปชัน
                                                                    'นี่คือแมนฯ ยู'</h3>
                                                                <p><a href="#" class="btn btn-primary btn-sm"
                                                                      role="button">Read
                                                                        More</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li><a href="/news?type=thai&Page=1">ข่าวบอลไทย</a></li>

                            <li><a href="/news?type=other&Page=1">ข่าวกีฬาอื่นๆ</a></li>

                            <li class="active-dropdown">
                                <a href="/gallery">Sexy Football <i class="fa fa-chevron-down"
                                                                        aria-hidden="true"></i></a>
                                <div class="bx-menu-dropdown">

                                    <div class="container">
                                        <div class="col-md-2 list-content-big-menu">
                                            <h3>Sexy Football</h3>
                                            <ul>
                                                <li>
                                                    <a href="/news?type=premier league&Page=1">Sexy Picpost</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li class="active">
                                                    <a href="/news?type=la liga&Page=1">Sexy Video</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>


                                            </ul>
                                        </div>
                                        <!--                                        Picpost-->
                                        <div class="hide col-md-10 detail-content-big-menu">
                                            <div class="col-sm-12">
                                                <h3 class="pull-left hide">
                                                    <i class="fa fa-youtube-play" aria-hidden="true"></i> Sexy Picpost
                                                </h3>
                                                <h3 class="pull-left">
                                                    <i class="fa fa-picture-o"></i> Sexy Picpost
                                                </h3>

                                                <a class="pull-right"
                                                   href="/highlight?category=premier league">ทั้งหมด</a>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="thumbnail">
                                                            <div class="wrap-box-gallery">
                                                                <div class="bx-type-gallery">
                                                                    <i class="fa fa-picture-o"></i>
                                                                </div>
                                                                <div class="bx-gallery">
                                                                    <a href="/sexy-football-picture?id=3024">
                                                                        <div class="crop"
                                                                             style="background-image: url('http://api.ngoal.com/uploaded/gallery/3024/galleryImage_3024_34938.jpg');"></div>
                                                                    </a>
                                                                </div>
                                                                <div class="bx-detail-gallery">
                                                                    Sexy pool
                                                                </div>
                                                            </div>
                                                            <div class="caption">
                                                                <p><a href="#" class="btn btn-primary btn-sm"
                                                                      role="button">Read
                                                                        More</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="thumbnail">
                                                            <div class="wrap-box-gallery">
                                                                <div class="bx-type-gallery">
                                                                    <i class="fa fa-picture-o"></i>
                                                                </div>
                                                                <div class="bx-gallery">
                                                                    <a href="/sexy-football-picture?id=3022">
                                                                        <div class="crop"
                                                                             style="background-image: url('http://api.ngoal.com/uploaded/gallery/3022/galleryImage_3022_34924.jpg');"></div>
                                                                    </a>
                                                                </div>
                                                                <div class="bx-detail-gallery">
                                                                    See Bra
                                                                </div>
                                                            </div>
                                                            <div class="caption">
                                                                <p><a href="#" class="btn btn-primary btn-sm"
                                                                      role="button">Read
                                                                        More</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="thumbnail">
                                                            <div class="wrap-box-gallery">
                                                                <div class="bx-type-gallery">
                                                                    <i class="fa fa-picture-o"></i>
                                                                </div>
                                                                <div class="bx-gallery">
                                                                    <a href="/sexy-football-picture?id=3020">
                                                                        <div class="crop"
                                                                             style="background-image: url('http://api.ngoal.com/uploaded/gallery/3020/galleryImage_3020_34896.jpg');"></div>
                                                                    </a>
                                                                </div>
                                                                <div class="bx-detail-gallery">
                                                                    Bunny Girl
                                                                </div>
                                                            </div>
                                                            <div class="caption">
                                                                <p><a href="#" class="btn btn-primary btn-sm"
                                                                      role="button">Read
                                                                        More</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--                                        video-->
                                        <div class="col-md-10 detail-content-big-menu">
                                            <div class="col-sm-12">
                                                <h3 class="pull-left">
                                                    <i class="fa fa-youtube-play" aria-hidden="true"></i> Sexy Video
                                                </h3>

                                                <a class="pull-right"
                                                   href="/highlight?category=premier league">ทั้งหมด</a>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="thumbnail">
                                                            <a href="/sexy-football-videos?galleryId=2984">
                                                                <div class="box-videos box-related"
                                                                     style="height: 180px;">
                                                                    <div class="crop"
                                                                         style="background-image: url(https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.0-9/27545508_1516931121745647_555683035447182741_n.jpg?oh=78a47c5564b6c9c334976bc29e40187e&amp;oe=5B1A95DE);"></div>
                                                                    <div class="icon-play"><i class="fa fa-play"></i>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <div class="caption">
                                                                <h3>
                                                                    ใส่รองเท้ายังเช็กซี่ขนาดนี้
                                                                </h3>
                                                                <p><a href="#" class="btn btn-primary btn-sm"
                                                                      role="button">Read
                                                                        More</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="thumbnail">
                                                            <a href="/sexy-football-videos?galleryId=2984">
                                                                <div class="box-videos box-related">
                                                                    <div class="crop"
                                                                         style="background-image: url(https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.0-9/27657890_1519708308134595_4053864252026917281_n.jpg?oh=69b66181536c6005484083ba60e38a43&amp;oe=5B1E2FC0);"></div>
                                                                    <div class="icon-play"><i class="fa fa-play"></i>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <div class="caption">
                                                                <h3>
                                                                    ท่าที่สาวๆใช้ออกกำลังกายกันประจำ
                                                                </h3>
                                                                <p><a href="#" class="btn btn-primary btn-sm"
                                                                      role="button">Read
                                                                        More</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="thumbnail">
                                                            <a href="/sexy-football-videos?galleryId=2984">
                                                                <div class="box-videos box-related"
                                                                     style="height: 180px;">
                                                                    <div class="crop"
                                                                         style="background-image: url(https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.0-9/27752505_1522525927852833_6469203164454989308_n.jpg?oh=a10f9f02d3c2707c7ff38358b1c22778&amp;oe=5ADA33C9);"></div>
                                                                    <div class="icon-play"><i class="fa fa-play"></i>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <div class="caption">
                                                                <h3>
                                                                    กระจกอยู่ห้องไม่สะอาดพอดี อยาดได้คนมาทำความสะอาด
                                                                </h3>
                                                                <p><a href="#" class="btn btn-primary btn-sm"
                                                                      role="button">Read
                                                                        More</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="active-dropdown">
                                <div class="hot-icon">
                                    <img src="/images/source.gif">
                                </div>
                                <a href="/games?mid=<?php echo((!empty($match)) ? $match[0]->id : '') ?>">เกมทายผลบอล
                                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                </a>
                                <div class="bx-menu-dropdown">

                                    <div class="container">
                                        <div class="col-md-2 list-content-big-menu">
                                            <h3>เกมทายผลบอล</h3>
                                            <ul>
                                                <li class="active">
                                                    <a href="/">เกมทายผลบอล</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/rules">กติกาและเงื่อนไข</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/ranking">Ranking</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <a href="/help">วิธีการเล่นเกมทายผล</a>
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </li>

                                            </ul>
                                        </div>

                                        <div class="col-md-6 detail-content-big-menu detail-march-content">
                                            <div class="col-sm-12">
                                                <h3 class="pull-left">
                                                    <i class="fa fa-gamepad" aria-hidden="true"></i> March Football
                                                </h3>
                                                <a class="pull-right"
                                                   href="/highlight?category=premier league">ทั้งหมด</a>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-sm-6" style="border-right: solid 1px rgba(255, 255, 255, 0.1);">
                                                        <h5>ENGLAND FA CUP</h5>
                                                        <ul class="topic">
                                                            <li><i class="fa fa-clock-o" aria-hidden="true"></i> 20:00
                                                            </li>
                                                            <li>|</li>
                                                            <li><i class="fa fa-calendar" aria-hidden="true"></i>
                                                                21/07/2018
                                                            </li>
                                                        </ul>

                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                    <h4>Chelsea</h4>
                                                                </td>
                                                                <td class="box-vs">VS</td>
                                                                <td>
                                                                    <img src="/images/logo-team/manchester.png">
                                                                    <h4>Chelsea</h4>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <p class="text-center">
                                                            <a href="#" class="btn btn-primary btn-sm">ทรรศนะคู่บอล</a>
                                                        </p>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h5>ENGLAND FA CUP</h5>
                                                        <ul class="topic">
                                                            <li><i class="fa fa-clock-o" aria-hidden="true"></i> 20:00
                                                            </li>
                                                            <li>|</li>
                                                            <li><i class="fa fa-calendar" aria-hidden="true"></i>
                                                                21/07/2018
                                                            </li>
                                                        </ul>

                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <img src="/images/logo-team/Chelsea.png">
                                                                    <h4>Chelsea</h4>
                                                                </td>
                                                                <td class="box-vs">VS</td>
                                                                <td>
                                                                    <img src="/images/logo-team/manchester.png">
                                                                    <h4>Chelsea</h4>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <p class="text-center">
                                                            <a href="#" class="btn btn-primary btn-sm">ทรรศนะคู่บอล</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 detail-content-big-menu">
                                            <div class="col-sm-12">
                                                <h3 class="pull-left">
                                                    <i class="fa fa-trophy" aria-hidden="true"></i> Ranking
                                                </h3>
                                                <a class="pull-right"
                                                   href="/highlight?category=premier league">ทั้งหมด</a>
                                            </div>
                                            <div class="col-sm-12 menu-ranking-box">
                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#all-time-ranking" aria-controls="home" role="tab" data-toggle="tab">All Time</a></li>
                                                    <li role="presentation"><a href="#now-ranking" aria-controls="profile" role="tab" data-toggle="tab">Now</a></li>
                                                    <li role="presentation"><a href="#Last-champ-ranking" aria-controls="messages" role="tab" data-toggle="tab">Last Champ</a></li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="all-time-ranking">
                                                        <table>
                                                            <tr>
                                                                <td>1</td>
                                                                <td><img src="/images/9.jpg"></td>
                                                                <td>Wittaya Dargon Wongpanya</td>
                                                                <td>77.08</td>
                                                            </tr>
                                                            <tr>
                                                                <td>2</td>
                                                                <td><img src="/images/9.jpg"></td>
                                                                <td>Wittaya Dargon Wongpanya</td>
                                                                <td>77.08</td>
                                                            </tr>
                                                            <tr>
                                                                <td>3</td>
                                                                <td><img src="/images/9.jpg"></td>
                                                                <td>Wittaya Dargon Wongpanya</td>
                                                                <td>77.08</td>
                                                            </tr>
                                                            <tr>
                                                                <td>4</td>
                                                                <td><img src="/images/9.jpg"></td>
                                                                <td>Wittaya Dargon Wongpanya</td>
                                                                <td>77.08</td>
                                                            </tr>
                                                            <tr>
                                                                <td>5</td>
                                                                <td><img src="/images/9.jpg"></td>
                                                                <td>Wittaya Dargon Wongpanya</td>
                                                                <td>77.08</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="now-ranking">...</div>
                                                    <div role="tabpanel" class="tab-pane" id="Last-champ-ranking">...</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

    </nav>

</div>
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button"
   title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span
            class="glyphicon glyphicon-chevron-up"></span></a>

<?php
$dataDailyIncome = false;
$dataNewUserIncome = false;
?>
<?php if (isset($_SESSION['login'])) {
    if (empty($playLogIncome)) {
        require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playLogIncome.php";
        $playLogIncome = new playLogIncome();
    }
    $dataDailyIncome = $playLogIncome->checkDailyIncome($_SESSION['login']['id']);
    $dataNewUserIncome = $playLogIncome->checkNewUserIncome($_SESSION['login']['id']);
    ?>
    <div id="model-login-daily" class="modal fade" style="" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 70%;">
            <div class="modal-content" style="position: relative; background: none;">
                <img style="width: 100%;"
                     src="/images/login-daily/login-daily-<?php echo($_SESSION['login']['combo_login'] % 7); ?>.png">
                <div style="position: absolute;bottom:0;right:0;z-index: 100;width: 20%;padding: 2% 5%;font-size: 18px;font-weight: bolder;color: #fff;background-color: #0e505e;">
                    <a href="/play/dailyCoin" style="color: #fff;text-decoration: none;">รับ Coin</a>
                </div>
            </div>
        </div>
    </div>

    <div id="model-new-user" class="modal fade" style="" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 50%;">
            <div class="modal-content" style="position: relative; background: none;">
                <img style="width: 100%;" src="/images/login-daily/new-user.png">
                <div style="position: absolute;bottom:0;right:0;z-index: 100;width: 20%;height: 20%;padding: 7% 5%;font-size: 18px;font-weight: bolder;color: #fff;">
                    <a href="/play/newUserCoin?pathname=<?php echo $_SERVER['REQUEST_URI'];?>" style="color: #fff;text-decoration: none;">รับ Coin</a>
                </div>
            </div>
        </div>
    </div>

<?php } ?>

<?php if ($dataDailyIncome) { ?>
    <script>
        $(document).ready(function () {
            $('div#model-login-daily').modal({
                backdrop: 'static',
                keyboard: false
            });
        })
    </script>
<?php } ?>
<?php if ($dataNewUserIncome) { ?>
    <script>
        $(document).ready(function () {
            $('div#model-new-user').modal({
                backdrop: 'static',
                keyboard: false
            });
        })
    </script>
<?php } ?>

<!--เมนูหลัก หน้าแรก-->
<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/HomeM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/VideoM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/GalleryM.php';
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playUser.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
$GalleryObject = new GalleryM();
$playMatch = new playMatch();
$playUser = new playUser();
$VideoMObj = new VideoM();
$homem = new HomeM();
$videoHighlight = array();
$videoHighlight['premier_league'] = $VideoMObj->getSlideVideosRandom('premier league', 2);
$videoHighlight['la_liga'] = $VideoMObj->getSlideVideosRandom('la liga', 2);
$videoHighlight['bundesliga'] = $VideoMObj->getSlideVideosRandom('bundesliga', 2);
$videoHighlight['serie_a'] = $VideoMObj->getSlideVideosRandom('serie a', 2);
$videoHighlight['league_1'] = $VideoMObj->getSlideVideosRandom('league 1', 2);
$videoHighlight['thai'] = $VideoMObj->getSlideVideosRandom('thai', 2);
$videoHighlight['ucl'] = $VideoMObj->getSlideVideosRandom('ucl', 2);
$videoHighlight['europa'] = $VideoMObj->getSlideVideosRandom('europa', 2);
$videoHighlight['international'] = $VideoMObj->getSlideVideosRandom('international', 2);
$lastVideo = $VideoMObj->getVideosByLastVideoGeneral();
$videoNews = array();
$videoNews['premier_league'] = $homem->getNewsByCategory('premier league', 3);
$videoNews['la_liga'] = $homem->getNewsByCategory('la liga', 3);
$videoNews['bundesliga'] = $homem->getNewsByCategory('bundesliga', 3);
$videoNews['serie_a'] = $homem->getNewsByCategory('serie a', 3);
$videoNews['league_1'] = $homem->getNewsByCategory('league 1', 3);
$videoNews['thai'] = $homem->getNewsByCategory('thai', 3);
$videoNews['ucl'] = $homem->getNewsByCategory('ucl', 3);
$videoNews['europa'] = $homem->getNewsByCategory('europa', 3);
$videoNews['international'] = $homem->getNewsByCategory('international', 3);
$videoNews['other'] = $homem->getNewsByCategory('other', 3);
$dataSexyPicture = $GalleryObject->getAllGalleryPicture(0, 3);
$dataSexyVideo = $GalleryObject->getMediaGalleryVideo(3);
$topRankingAll = $playUser->accuracyUserAll();
$topRankingNow = $playUser->accuracyUserNow();
$topLastChamp = $playUser->accuracyUserLastChamp();
$match = array();
$match = $playMatch->getFirstMathStatusBet(2);
?>

<div class="main-menu-index navbar-top" style="padding-left: 0;">
    <div class="container container-wide">
        <div class="pull-left logo-menu">
            <a class="link_wrap" href="/"></a>
            <img src="/images/logo/logo-ngoal-white.png">
        </div>
        <div class="nav-menutop nav-menutop-edit pull-left nav-menu-main">
            <ul class="nav nav-pills">
                <li class="none-dropdown"><a href="https://www.ngoal.com">หน้าหลัก</a></li>
                <li class="active-dropdown">
                    <!--                    <a href="/videos.php">-->
                    <a href="/highlight">
                        <div class="hot-icon">
                            <i class="fa fa-circle" aria-hidden="true"></i>
                        </div>
                        วีดีโอ/ไฮไลท์บอล
                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                        <div class="bx-menu-dropdown-arrow">
                            <i class="fa fa-caret-up" aria-hidden="true"></i>
                        </div>

                    </a>
                    <div class="bx-menu-dropdown">
                        <div class="container">
                            <div class="col-sm-2 list-content-big-menu">
                                <h3>วีดีโอ/ไฮไลท์บอล</h3>
                                <ul>
                                    <li><a href="/highlight">ไฮไลท์บอลฟุตบอลทั้งหมด</a></li>
                                    <li class="active">
                                        <a class="menu-highlight"
                                           data-category="premier_league">พรีเมียร์ลีก</a>
                                    </li>
                                    <li>
                                        <a class="menu-highlight"
                                           data-category="la_liga">ลาลีกา</a>
                                    </li>
                                    <li>
                                        <a class="menu-highlight"
                                           data-category="bundesliga">บุนเดสลีกา</a>
                                    </li>
                                    <li>
                                        <a style="cursor: pointer;" class="menu-highlight"
                                           data-category="serie_a">ซีเรียอา</a>
                                    </li>
                                    <li><a style="cursor: pointer;" class="menu-highlight"
                                           data-category="league_1">ลีกเอิง</a>
                                    </li>
                                    <li>
                                        <a style="cursor: pointer;" class="menu-highlight" data-category="thai">ไทยพรีเมียลีก</a>
                                    </li>
                                    <li>
                                        <a style="cursor: pointer;" class="menu-highlight"
                                           data-category="ucl">ยูฟ่าแชมเปียนลีก</a>
                                    </li>
                                    <li>
                                        <a style="cursor: pointer;" class="menu-highlight"
                                           data-category="europa">ยูโรป้า</a>
                                    </li>
                                    <li>
                                        <a style="cursor: pointer;" class="menu-highlight"
                                           data-category="international">ฟุตบอลทีมชาติ</a>
                                    </li>

                                </ul>
                            </div>
                            <?php if (!empty($videoHighlight)) { ?>
                                <?php foreach ($videoHighlight as $key => $value) { ?>
                                    <div class="div-highlight col-sm-6 detail-content-big-menu <?= (($key == "premier_league") ? '' : 'hide'); ?>"
                                         id="highlight-<?= $key ?>">
                                        <div class="col-sm-12 topic-content-big-menu">
                                            <h3>
                                                <i class="fa fa-youtube-play" aria-hidden="true"></i>
                                                <?php if ($key == "premier_league") { ?>
                                                    ไฮไลท์ พรีเมียร์ลีก
                                                <?php } elseif ($key == "la_liga") { ?>
                                                    ไฮไลท์ ลาลีกา
                                                <?php } elseif ($key == "bundesliga") { ?>
                                                    ไฮไลท์ บุนเดสลีกา
                                                <?php } elseif ($key == "serie_a") { ?>
                                                    ไฮไลท์ ซีเรียอา
                                                <?php } elseif ($key == "league_1") { ?>
                                                    ไฮไลท์ ลีกเอิง
                                                <?php } elseif ($key == "thai") { ?>
                                                    ไฮไลท์ ไทยพรีเมียลีก
                                                <?php } elseif ($key == "ucl") { ?>
                                                    ไฮไลท์ ยูฟ่าแชมเปียนลีก
                                                <?php } elseif ($key == "europa") { ?>
                                                    ไฮไลท์ ยูโรป้า
                                                <?php } elseif ($key == "international") { ?>
                                                    ไฮไลท์ ฟุตบอลทีมชาติ
                                                <?php } ?>
                                            </h3>
                                            <a class="top-readmore"
                                               href="/highlight?category=<?= str_replace('_', ' ', $key); ?>"><span>ทั้งหมด</span></a>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="thumbnail">
                                                        <a class="link_wrap"
                                                           href="/highlight?videoId=<?= $value[0]->video_id ?>"></a>
                                                        <div class="box-videos box-related box-related-sexy">
                                                            <div class="hilight-cover"></div>
                                                            <div class="icon-play">
                                                                <i class="fa fa-play"></i>
                                                            </div>
                                                            <div class="crop">
                                                                <img src="<?= $value[0]->thumbnail ?>">
                                                            </div>

                                                        </div>
                                                        <div class="caption">
                                                            <h3><?= $value[0]->title ?></h3>
                                                        </div>
                                                        <div class="btn btn-default btn-sm">
                                                            อ่านทั้งหมด
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">

                                                    <div class="thumbnail">
                                                        <a class="link_wrap"
                                                           href="/highlight?videoId=<?= $value[1]->video_id ?>"></a>
                                                        <div class="box-videos box-related box-related-sexy">
                                                            <div class="hilight-cover"></div>
                                                            <div class="icon-play">
                                                                <i class="fa fa-play"></i>
                                                            </div>
                                                            <div class="crop">
                                                                <img src="<?= $value[1]->thumbnail ?>">
                                                            </div>
                                                        </div>
                                                        <div class="caption">
                                                            <h3><?= $value[1]->title ?></h3>
                                                        </div>
                                                        <div class="btn btn-default btn-sm">
                                                            อ่านทั้งหมด
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <div class="col-sm-4 detail-content-big-menu">
                                <div class="col-sm-12 topic-content-big-menu">
                                    <h3>
                                        <i class="fa fa-fire" aria-hidden="true"></i> HotHit Video
                                    </h3>
                                    <a class="top-readmore" href="/hothit"><span>ทั้งหมด</span></a>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-12">


                                            <div class="thumbnail">
                                                <a class="link_wrap"
                                                   href="/hothit-videos?videoId=<?= $lastVideo[0]->video_id ?>"></a>
                                                <div class="box-videos" style="height: 204px; overflow: hidden;">
                                                    <!--                                                                    <div class="hilight-cover"></div>-->
                                                    <div class="icon-play">
                                                        <i class="fa fa-play"></i>
                                                    </div>
<!--                                                    <div class="cover-hothit"><img-->
<!--                                                                src="/images/All_.png"></div>-->
                                                    <div class="crop">
                                                        <img src="<?= $lastVideo[0]->urlImg ?>">
                                                    </div>

                                                </div>
                                                <div class="bx-detail-gallery">
                                                    <h5><?= $lastVideo[0]->title ?></h5>
                                                </div>
                                                <div class="btn btn-default btn-sm">
                                                    อ่านทั้งหมด
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="active-dropdown">
                    <a href="/news?type=other">
                        ข่าวบอลต่างประเทศ
                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                        <div class="bx-menu-dropdown-arrow">
                            <i class="fa fa-caret-up" aria-hidden="true"></i>
                        </div>
                    </a>
                    <div class="bx-menu-dropdown">
                        <div class="container">
                            <div class="col-sm-2 list-content-big-menu">
                                <h3>ข่าวฟุตบอลต่างประเทศ</h3>
                                <ul>
                                    <li class="active">
                                        <a style="cursor: pointer;" class="menu-videoNews"
                                           data-category="premier_league">ข่าวพรีเมียร์ลีก</a>
                                    </li>
                                    <li>
                                        <a style="cursor: pointer;" class="menu-videoNews"
                                           data-category="la_liga">ข่าวลาลีกา</a>
                                    </li>
                                    <li>
                                        <a style="cursor: pointer;" class="menu-videoNews"
                                           data-category="bundesliga">ข่าวบุนเดสลีกา</a>
                                    </li>
                                    <li>
                                        <a style="cursor: pointer;" class="menu-videoNews"
                                           data-category="serie_a">ข่าวซีเรียอา</a>
                                    </li>
                                    <li>
                                        <a style="cursor: pointer;" class="menu-videoNews"
                                           data-category="ucl">ข่าวยูฟ่าแชมเปียนลีก</a>
                                    </li>
                                    <li>
                                        <a style="cursor: pointer;" class="menu-videoNews"
                                           data-category="europa">ข่าวยูโรป้า</a>
                                    </li>
                                    <li>
                                        <a style="cursor: pointer;" class="menu-videoNews"
                                           data-category="thai">ข่าวไทยพรีเมียลีก</a>
                                    </li>
                                    <li>
                                        <a style="cursor: pointer;" class="menu-videoNews"
                                           data-category="ucl">ข่าวยูฟ่าแชมเปียนลีก</a>
                                    </li>
                                    <li>
                                        <a style="cursor: pointer;" class="menu-videoNews"
                                           data-category="ucl">ข่าวทีมชาติ</a>
                                    </li>
                                    <li>
                                        <a style="cursor: pointer;" class="menu-videoNews"
                                           data-category="other">ข่าวฟุตบอลลีกอื่น</a>
                                    </li>

                                </ul>
                            </div>
                            <?php foreach ($videoNews as $key => $value) { ?>
                                <div class="div-videoNews col-sm-10 detail-content-big-menu <?= (($key == "premier_league") ? '' : 'hide'); ?>"
                                     id="videoNews-<?= $key ?>">
                                    <div class="col-sm-12 topic-content-big-menu">
                                        <h3>
                                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                                            <?php if ($key == "premier_league") { ?>
                                                ข่าว พรีเมียร์ลีก
                                            <?php } elseif ($key == "la_liga") { ?>
                                                ข่าว ลาลีกา
                                            <?php } elseif ($key == "bundesliga") { ?>
                                                ข่าว บุนเดสลีกา
                                            <?php } elseif ($key == "serie_a") { ?>
                                                ข่าว ซีเรียอา
                                            <?php } elseif ($key == "league_1") { ?>
                                                ข่าว ลีกเอิง
                                            <?php } elseif ($key == "thai") { ?>
                                                ข่าว ไทยพรีเมียลีก
                                            <?php } elseif ($key == "ucl") { ?>
                                                ข่าว ยูฟ่าแชมเปียนลีก
                                            <?php } elseif ($key == "europa") { ?>
                                                ข่าว ยูโรป้า
                                            <?php } elseif ($key == "international") { ?>
                                                ข่าว ฟุตบอลทีมชาติ
                                            <?php } ?>
                                        </h3>
                                        <a class="top-readmore"
                                           href="/news?type=<?= str_replace('_', ' ', $key); ?>"><span>ดูทั้งหมด</span></a>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="thumbnail">
                                                    <a class="link_wrap"
                                                       href="/articles?newsid=<?= $value[0]->newsid ?>"></a>
                                                    <div class="wrap-box-gallery bx-gallery bx-gallery-submenu">
                                                        <div class="hilight-cover"></div>
                                                        <div class="crop"
                                                             style="background-image: url(<?= $value[0]->imageLink ?>)"></div>
                                                    </div>
                                                    <div class="caption">
                                                        <h3><?= $value[0]->titleTh ?></h3>
                                                    </div>
                                                    <div class="btn btn-default btn-sm">
                                                        อ่านทั้งหมด
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="thumbnail">
                                                    <a class="link_wrap"
                                                       href="/articles?newsid=<?= $value[1]->newsid ?>"></a>
                                                    <div class="wrap-box-gallery bx-gallery bx-gallery-submenu zoom">
                                                        <div class="hilight-cover"></div>
                                                        <div class="crop"
                                                             style="background-image: url(<?= $value[1]->imageLink ?>)"></div>
                                                    </div>
                                                    <div class="caption">
                                                        <h3><?= $value[1]->titleTh ?></h3>
                                                    </div>
                                                    <div class="btn btn-default btn-sm">
                                                        อ่านทั้งหมด
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="thumbnail">
                                                    <a class="link_wrap"
                                                       href="/articles?newsid=<?= $value[2]->newsid ?>"></a>
                                                    <div class="wrap-box-gallery bx-gallery bx-gallery-submenu zoom">
                                                        <div class="hilight-cover"></div>
                                                        <div class="crop"
                                                             style="background-image: url(<?= $value[2]->imageLink ?>)"></div>
                                                    </div>
                                                    <div class="caption">
                                                        <h3><?= $value[2]->titleTh ?></h3>
                                                        <div class="btn btn-default btn-sm">
                                                            อ่านทั้งหมด
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                </li>
                <li class="none-dropdown"><a href="/news?type=thai&Page=1">ข่าวบอลไทย</a></li>
                <!--                <li><a href="/news.php?type=other&Page=1">ข่าวกีฬาอื่นๆ</a></li>-->

                <!--                sexy-->
                <li class="active-dropdown">
                    <!--                    href="/gallery.php"-->
                    <a href="/sexy-football">
                        Sexy Football
                        <!--                        <i class="fa fa-chevron-down" aria-hidden="true"></i>-->
                        <!--                        <div class="bx-menu-dropdown-arrow">-->
                        <!--                            <i class="fa fa-caret-up" aria-hidden="true"></i>-->
                        <!--                        </div>-->
                    </a>
                    <div class="hide bx-menu-dropdown">
                        <div class="container">
                            <div class="col-sm-2 list-content-big-menu">
                                <h3>Sexy Football</h3>
                                <ul>
                                    <li>
                                        <a class="menu-sexy"
                                           data-category="picture">Sexy Picture</a>
                                    </li>
                                    <li class="active">
                                        <a class="menu-sexy" data-category="video">Sexy
                                            Video</a>
                                    </li>
                                </ul>
                            </div>
                            <!--                                        Picpost-->
                            <div class="div-sexy col-sm-10 detail-content-big-menu hide" id="sexy-picture">
                                <div class="col-sm-12 topic-content-big-menu">
                                    <h3>
                                        <i class="fa fa-picture-o"></i> Sexy Picture
                                    </h3>
                                    <a class="top-readmore"
                                       href="/sexy-football?type=picture"><span>ดูทั้งหมด</span></a>


                                </div>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <?php foreach ($dataSexyPicture as $key => $value) { ?>
                                            <div class="col-sm-4">
                                                <a class="link_wrap"
                                                   href="/sexy-football-picture?id=<?= $value->id ?>"></a>
                                                <div class="thumbnail">
                                                    <div class="wrap-box-gallery bx-gallery bx-gallery-submenu">
                                                        <div class="bx-type-gallery">
                                                            <i class="fa fa-picture-o"></i>
                                                        </div>
                                                        <div class="crop"
                                                             style="background-image: url(<?php echo $value->thumbnail ?>)"></div>
                                                    </div>
                                                    <div class="bx-detail-gallery">
                                                        <h5><?= $value->title ?></h5>
                                                    </div>
                                                    <div class="btn btn-default btn-sm">
                                                        อ่านทั้งหมด
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <!--                                        video-->
                            <div class="div-sexy col-sm-10 detail-content-big-menu" id="sexy-video">
                                <div class="col-sm-12 topic-content-big-menu">
                                    <h3>
                                        <i class="fa fa-youtube-play" aria-hidden="true"></i> Sexy Video
                                    </h3>
                                    <a class="top-readmore" href="/sexy-football?type=video"><span>ดูทั้งหมด</span></a>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <?php foreach ($dataSexyVideo as $key => $value) { ?>
                                            <div class="col-sm-4">
                                                <a class="link_wrap"
                                                   href="/sexy-football-videos?galleryId=<?= $value->id ?>"></a>
                                                <div class="thumbnail">
                                                    <div class="wrap-box-gallery bx-gallery bx-gallery-submenu">
                                                        <div class="bx-type-gallery">
                                                            <i class="fa fa-play"></i>
                                                        </div>
                                                        <div class="crop"
                                                             style="background-image: url(<?= $value->urlImg ?>)"></div>
                                                    </div>
                                                    <div class="bx-detail-gallery">
                                                        <h5><?= $value->title ?></h5>
                                                    </div>
                                                    <div class="btn btn-default btn-sm">
                                                        อ่านทั้งหมด
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="active-dropdown">
                    <!--                    <a href="/games.php?mid=-->
                    <?php //echo((!empty($match[0])) ? $match[0]->id : '') ?><!--">-->
                    <a href="/livescore">
                        <div class="hot-icon">
                            <i class="fa fa-circle" aria-hidden="true"></i>
                        </div>
                        เกมทายผลบอล
                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                        <div class="bx-menu-dropdown-arrow">
                            <i class="fa fa-caret-up" aria-hidden="true"></i>
                        </div>
                    </a>
                    <div class="bx-menu-dropdown bx-menu-dropdown-match-football">

                        <div class="container">
                            <div class="col-sm-2 list-content-big-menu">
                                <h3>เกมทายผลบอล</h3>
                                <ul>
                                    <li class="active">
                                        <a>เกมทายผลบอล</a>
                                    </li>
                                    <li>
                                        <a href="/rules">กติกาและเงื่อนไข</a>
                                    </li>
                                    <li>
                                        <a href="/ranking">Ranking</a>
                                    </li>
                                    <li>
                                        <a href="/help">วิธีการเล่นเกมทายผล</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-sm-6 detail-content-big-menu detail-march-content">
                                <div class="col-sm-12 topic-content-big-menu">
                                    <h3>
                                        <i class="fa fa-gamepad" aria-hidden="true"></i> Match Football
                                    </h3>
                                    <a class="top-readmore"
                                       href="/livescore"><span>ทั้งหมด</span></a>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <?php if (!empty($match[0])) { ?>
                                            <div class="col-sm-6"
                                                 style="border-right: solid 1px rgba(255, 255, 255, 0.1);">
                                                <h5><?= $match[0]->name ?></h5>
                                                <ul class="topic">
                                                    <li>
                                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        <?= date("H:i", (strtotime($match[0]->time_match) - (60 * 60))) ?>
                                                    </li>
                                                    <li>|</li>
                                                    <li><i class="fa fa-calendar" aria-hidden="true"></i>
                                                        <?= date("d/m/Y", (strtotime($match[0]->time_match) - (60 * 60))) ?>
                                                    </li>
                                                </ul>

                                                <table>
                                                    <tr>
                                                        <td>
                                                            <img src="<?= $match[0]->teamHomePath ?>">
                                                            <h4 class="<?= ((!empty($match[0]->handicap) && $match[0]->handicap < 0) ? 'active' : '') ?>"><?= $match[0]->teamHomeEn ?></h4>
                                                        </td>
                                                        <td class="box-vs">VS</td>
                                                        <td>
                                                            <img src="<?= $match[0]->teamAwayPath ?>">
                                                            <h4 class="<?= ((!empty($match[0]->handicap) && $match[0]->handicap > 0) ? 'active' : '') ?>"><?= $match[0]->teamAwayEn ?></h4>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <p class="text-center">
                                                    <a href="/games?mid=<?= $match[0]->id ?>"
                                                       class="btn btn-default btn-sm">ทายผลบอล</a>
                                                </p>
                                            </div>
                                        <?php } ?>
                                        <?php if (!empty($match[1])) { ?>
                                            <div class="col-sm-6">
                                                <h5><?= $match[1]->name ?></h5>
                                                <ul class="topic">
                                                    <li>
                                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        <?= date("H:i", (strtotime($match[1]->time_match) - (60 * 60))) ?>
                                                    </li>
                                                    <li>|</li>
                                                    <li><i class="fa fa-calendar" aria-hidden="true"></i>
                                                        <?= date("d/m/Y", (strtotime($match[1]->time_match) - (60 * 60))) ?>
                                                    </li>
                                                </ul>

                                                <table>
                                                    <tr>
                                                        <td>
                                                            <img src="<?= $match[1]->teamHomePath ?>">
                                                            <h4 class="<?= ((!empty($match[1]->handicap) && $match[1]->handicap < 0) ? 'active' : '') ?>"><?= $match[1]->teamHomeEn ?></h4>
                                                        </td>
                                                        <td class="box-vs">VS</td>
                                                        <td>
                                                            <img src="<?= $match[1]->teamAwayPath ?>">
                                                            <h4 class="<?= ((!empty($match[1]->handicap) && $match[1]->handicap > 0) ? 'active' : '') ?>"><?= $match[1]->teamAwayEn ?></h4>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <p class="text-center">
                                                    <a href="/games?mid=<?= $match[1]->id ?>"
                                                       class="btn btn-default btn-sm">ทายผลบอล</a>
                                                </p>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 detail-content-big-menu">
                                <div class="col-sm-12 topic-content-big-menu">
                                    <h3>
                                        <i class="fa fa-trophy" aria-hidden="true"></i> Ranking
                                    </h3>
                                    <a class="top-readmore"
                                       href="/ranking"><span>ดูทั้งหมด</span></a>
                                </div>
                                <div class="col-sm-12 menu-ranking-box">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#all-time-ranking"
                                                                                  aria-controls="home"
                                                                                  role="tab"
                                                                                  data-toggle="tab">All
                                                Time</a></li>
                                        <li role="presentation"><a href="#now-ranking"
                                                                   aria-controls="profile" role="tab"
                                                                   data-toggle="tab">Now</a></li>
                                        <li role="presentation"><a href="#Last-champ-ranking"
                                                                   aria-controls="messages" role="tab"
                                                                   data-toggle="tab">Last Champ</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="all-time-ranking">
                                            <table>
                                                <?php foreach ($topRankingAll as $key => $value) {
                                                    if ($key < 5) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $key + 1 ?></td>
                                                            <td>
                                                                <img src="https://graph.facebook.com/v2.8/<?php echo $value["fb_uid"]; ?>/picture?type=small">
                                                            </td>
                                                            <td><?php echo $value["name"]; ?></td>
                                                            <td><?php echo number_format($value["accuracy"], 2); ?></td>
                                                        </tr>
                                                    <?php }
                                                } ?>
                                            </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="now-ranking">
                                            <table>
                                                <?php foreach ($topRankingNow as $key => $value) {
                                                    if ($key < 5) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $key + 1 ?></td>
                                                            <td>
                                                                <img src="https://graph.facebook.com/v2.8/<?php echo $value["fb_uid"]; ?>/picture?type=small">
                                                            </td>
                                                            <td><?php echo $value["name"]; ?></td>
                                                            <td><?php echo number_format($value["accuracy"], 2); ?></td>
                                                        </tr>
                                                    <?php }
                                                } ?>
                                            </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="Last-champ-ranking">
                                            <table>
                                                <?php foreach ($topLastChamp as $key => $value) {
                                                    if ($key < 5) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $key + 1 ?></td>
                                                            <td>
                                                                <img src="https://graph.facebook.com/v2.8/<?php echo $value["fb_uid"]; ?>/picture?type=small">
                                                            </td>
                                                            <td><?php echo $value["name"]; ?></td>
                                                            <td><?php echo number_format($value["accuracy"], 2); ?></td>
                                                        </tr>
                                                    <?php }
                                                } ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="none-dropdown"><a href="/review">ทีเด็ด ทรรศนะ</a></li>
                <li class="none-dropdown"><a href="/livescore">ผลบอล</a></li>
                <li class="text-event"><a href="https://ngoal.com/events"><img src="/images/source.gif"> <span
                                class="blink"> แจกรางวัล ฟรี!</span></a></li>
                <li class="active-dropdown hide">
                    <a href="/worldcup">
                        <img style="height: 23px;" src="/images/World_Cup.gif">
                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                        <div class="bx-menu-dropdown-arrow">
                            <i class="fa fa-caret-up" aria-hidden="true"></i>
                        </div>
                        <div class="bx-menu-dropdown bx-menu-dropdown-content" style="">
                            <div class="">
                                <ul>
                                    <li>
                                        <a href="/worldcup">
                                            <img style="height: 23px;" src="/images/russia-2018-logo.png">
                                            <p>World Cup 2018</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/worldcup-pubg">
                                            <img style="height: 23px;" src="/worldcup-pubg/img/pubg-logo-style-one.png">
                                            <p>Pubg World Cup</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/worldcup-rov">
                                            <img style="height: 23px;" src="/images/logo_en.png">
                                            <p>ROV World Cup</p>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
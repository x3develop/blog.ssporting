<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/24/2017
 * Time: 10:55
 */
?>
<?php
require_once $_SERVER["DOCUMENT_ROOT"] ."/model/HomeM.php";
$homem = new HomeM();
$popularnews = $homem->getMostPopularNews();
$lastestnews = $homem->getLastestNews();
?>
<div class="container">
    <div class="row">
        <div class="wrap-box-most-popular col-sm-3 col-md-2">
            <h2>Most Popular</h2>
            <div class="nano bx1">
                <div class="content" style="right: 0!important;">
                    <?php for ($i = 0; $i < 7; $i++) { ?>
                        <?php if (array_key_exists($i, $popularnews)) { ?>
                            <div class="box-popular" newsid="<?php echo $popularnews[$i]->newsid; ?>">
                                <span class="box-popular-title"><?php echo $popularnews[$i]->titleTh; ?></span>
                                <a href="/articles?newsid=<?php echo $popularnews[$i]->newsid; ?>">
                                    <div class="times-content">
                                        view
                                        <span id="popularnews-<?php echo $popularnews[$i]->newsid; ?>"><?php echo $popularnews[$i]->readCount; ?></span>
                                    </div>
                                    <img src="<?php echo $popularnews[$i]->imageLink; ?>">
                                </a>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="wrap-lastnews col-sm-9 col-md-10">

                <?php for ($i = 0; $i < 2; $i++) { ?>
                    <?php if (array_key_exists($i, $lastestnews)) { ?>
                        <div class="col-xs-6 col-sm-6 lastest-news" newsid="<?php echo $lastestnews[$i]->newsid; ?>">
                            <div class="bx-story-medium">
                                <div class="hilight-cover"></div>
                                <a href="/articles?newsid=<?php echo $lastestnews[$i]->newsid; ?>"
                                   class="crop imgTest3"
                                   style="background-image: url(<?php echo $lastestnews[$i]->imageLink; ?>)"></a>

                                <div class="bx-story-content">
                                    <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                            class="how-long"><?php echo $lastestnews[$i]->createDatetime; ?></span>
                                    </div>
                                    <a href="/articles?newsid=<?php echo $lastestnews[$i]->newsid; ?>"
                                       class="h-content"><?php echo $lastestnews[$i]->titleTh; ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>

                <?php for ($i = 2; $i < count($lastestnews); $i++) { ?>
                    <?php if (array_key_exists($i, $lastestnews)) { ?>
                        <div class="col-xs-6 col-sm-4 lastest-news" newsid="<?php echo $lastestnews[$i]->newsid; ?>">
                            <div class="bx-story-medium">
                                <div class="hilight-cover"></div>
                                <a href="/articles?newsid=<?php echo $lastestnews[$i]->newsid; ?>" class="crop"
                                   style="background-image: url(<?php echo $lastestnews[$i]->imageLink; ?>)"></a>

                                <div class="bx-story-content">
                                    <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                            class="how-long"><?php echo $lastestnews[$i]->createDatetime; ?></span>
                                    </div>
                                    <a href="/articles?newsid=<?php echo $lastestnews[$i]->newsid; ?>"
                                       class="h-content"><?php echo $lastestnews[$i]->titleTh; ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>

        </div>
        <div style="clear: both;"></div>
    </div>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/24/2017
 * Time: 10:54
 */
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/GalleryM.php';
$GalleryObject = new GalleryM();
$dataGalleryAll = $GalleryObject->getAllGalleryPicture(0, 5);
$videoSexy = $GalleryObject->getMediaGalleryVideo(3);
?>
<div class="container">
    <div class="wrap-box-news">
        <div class="col-md-12">
            <div class="tab-h-content sexy-football-topic">
                <div class="pull-left"><span>Sexy Football</span></div>
                <a href="/sexy-football" class="pull-right more-btn">ทั้งหมด</a>
            </div>
        </div>
        <div class="mg">
            <?php foreach ($dataGalleryAll as $key => $value) { ?>
                <?php if ($key == 0) { ?>
                    <div class="col-sm-6">
                        <div class="wrap-box-gallery">
                            <div class="bx-type-gallery">
                                <?php if ($value->gall_type == 'picture') { ?>
                                    <i class="fa fa-picture-o"></i>
                                <?php } else if ($value->gall_type == 'video') { ?>
                                    <i class="fa fa-video-camera"></i>
                                <?php } ?>
                            </div>
                            <div class="bx-gallery-big">
                                <a class="link_wrap" href="/sexy-football-picture?id=<?php echo $value->id ?>"></a>
                                    <?php if ($value->gall_type == 'picture') { ?>
                                        <div class="crop"
                                             style="background-image: url('<?php echo $value->path ?>');"></div>
                                    <?php } else if ($value->gall_type == 'video') { ?>
                                        <div class="crop"
                                             style="background-image: url('<?php echo $value->thumbnail ?>');"></div>
                                    <?php } ?>

                                <div class="bx-gallery-big-detail">
                                    <div class="tab-datetime">
                                    <span class="pull-left">
                                        <i class="fa fa-clock-o"></i>
                                        <time class="how-long"><?php echo strtotime($value->update_at); ?></time>
                                        <?php if (!empty($value->createDatetime)) { ?>
                                            <?php echo date(' j/n/Y', strtotime($value->createDatetime)) ?>
                                        <?php } ?>
                                    </span>
                                        <span class="pull-right">
                                        view
                                        <span id="gallery-sexy-<?php echo $value->id ?>"><?php echo $value->view; ?></span>
                                        </span>
                                        <div style="clear: both;"></div>
                                    </div>
                                    <div class="bx-detail-gallery">
                                        <?php echo((!empty($value->title)) ? $value->title : "&nbsp;"); ?>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
            <div class="col-sm-6 col-md-6">
                <?php foreach ($dataGalleryAll as $key => $value) { ?>
                    <?php if ($key > 0) { ?>
                        <div class="col-sm-6 col-md-6">
                            <div class="wrap-box-gallery wrap-box-gallery-side">
                                <div class="bx-type-gallery">
                                    <?php if ($value->gall_type == 'picture') { ?>
                                        <i class="fa fa-picture-o"></i>
                                    <?php } else if ($value->gall_type == 'video') { ?>
                                        <i class="fa fa-video-camera"></i>
                                    <?php } ?>
                                </div>
                                <div class="bx-gallery bx-gallery-sexy-index">
                                    <a class="link_wrap" href="/sexy-football-picture?id=<?php echo $value->id ?>"> </a>
                                        <?php if ($value->gall_type == 'picture') { ?>
                                            <div class="crop"
                                                 style="background-image: url('<?php echo $value->path ?>');"></div>
                                        <?php } else if ($value->gall_type == 'video') { ?>
                                            <div class="crop"
                                                 style="background-image: url('<?php echo $value->thumbnail ?>');"></div>
                                        <?php } ?>


                                    <div class="bx-gallery-big-detail">
                                        <div class="bx-detail-gallery">
                                            <div class="tab-datetime">
                                    <span class="pull-left">
                                        <i class="fa fa-clock-o"></i>
                                            <time style="font-size: 10px;"
                                                  class="how-long"><?php echo strtotime($value->update_at); ?></time>
                                        <?php if (!empty($value->createDatetime)) { ?>
                                            <?php echo date(' j/n/Y', strtotime($value->createDatetime)) ?>
                                        <?php } ?>
                                    </span>
                                                <span class="pull-right">
                                        view
                                        <span id="gallery-sexy-<?php echo $value->id ?>"><?php echo $value->view; ?></span>
                                    </span>
                                                <div style="clear: both;"></div>
                                            </div>
                                            <div class="bx-detail-gallery">
                                                <?php echo((!empty($value->title)) ? $value->title : "&nbsp;"); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    <?php }
                } ?>
                <div style="clear:both;"></div>
            </div>
            <div style="clear: both;"></div>
        </div>
    </div>

    <div style="position: relative;">
        <div class="label-clip">CLIP</div>
        <div class="wrap-video-sexy">
            <?php foreach ($videoSexy as $key => $value) { ?>
                <!--                --><?php //var_dump($value); ?>
                <div class="col-sm-4">
                    <div>
                        <a href="/sexy-football-videos?galleryId=<?= $value->id ?>">
                            <div class="box-videos box-video-sexy box-video-sexy-index" style="">
                                <div class="crop" style="background-image: url(<?= $value->thumbnail; ?>);"></div>
                                <div class="icon-play"><i class="fa fa-play"></i></div>
                            </div>
                        </a>
                        <div class="bx-detail-gallery"><a style="color: #000000;"
                                                          href="/sexy-football-videos?galleryId=<?= $value->id ?>"><?= $value->title; ?></a>
                        </div>
                        <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                    class="how-long"><?= strtotime($value->update_at) ?></span></div>
                    </div>
                </div>
            <?php } ?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/24/2017
 * Time: 10:57
 */
?>
<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/HomeM.php";
$homem = new HomeM();
$recommendnews = $homem->getRecommentNews();
if (!empty($recommendnews)) {
    ?>
    <div class="container-headerNews" style="padding-top: 30px;">
        <div class="container">
            <div class="row">
                <?php if (array_key_exists(0, $recommendnews)) { ?>
                    <div class="col-sm-6 col-md-7 recommend-news"
                         recommend="<?php echo $recommendnews[0]->recommentNews; ?>"
                         newsid="<?php echo $recommendnews[0]->newsid; ?>">
                        <div class="bx-story-big">
                            <div class="hilight-cover"></div>
                            <a href="/articles?newsid=<?php echo $recommendnews[0]->newsid; ?>"
                               class="crop imgTest1"
                               style="background-image: url(<?php echo $recommendnews[0]->imageLink; ?>)"></a>

                            <div class="bx-story-content">
                                <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                            class="how-long"><?php echo $recommendnews[0]->createDatetime; ?></span>
                                </div>
                                <a href="/articles?newsid=<?php echo $recommendnews[0]->newsid; ?>"
                                   class="h-content"><?php echo $recommendnews[0]->titleTh; ?></a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-sm-6 col-md-5" style="padding: 0;">

                    <?php for ($i = 1; $i < count($recommendnews); $i++) { ?>
                        <?php if (array_key_exists($i, $recommendnews)) { ?>
                            <div class="col-xs-6 col-sm-6 recommend-news"
                                 recommend="<?php echo $recommendnews[$i]->recommentNews; ?>"
                                 newsid="<?php echo $recommendnews[$i]->newsid; ?>">
                                <div class="bx-story-small">
                                    <div class="hilight-cover"></div>
                                    <a href="/articles?newsid=<?php echo $recommendnews[$i]->newsid; ?>"
                                       class="crop"
                                       style="background-image: url(<?php echo $recommendnews[$i]->imageLink; ?>)"></a>

                                    <div class="bx-story-content">
                                        <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                                    class="how-long"><?php echo $recommendnews[$i]->createDatetime; ?></span>
                                        </div>
                                        <a href="/articles?newsid=<?php echo $recommendnews[$i]->newsid; ?>"
                                           class="h-content"><?php echo $recommendnews[$i]->titleTh; ?></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>

                <div style="clear: both;"></div>
            </div>
        </div>
    </div>
<?php } ?>
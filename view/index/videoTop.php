<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/24/2017
 * Time: 10:48
 */
?>
<div class="container" id="container-video-top">
    <div class="wrap-box-videos">
        <div id="owl-demo" class="owl-carousel owl-theme">
            <?php foreach ($lastestvideo as $key => $value) { ?>
                <?php if($value->videosource=="twitter"){ ?>
                    <a style="" id="box-twitter-video-top-main" href="/other-videos.php?videoId=<?php echo $value->video_id ?>">
                        <div class="lastest-video">
                            <div class="title-owl-carousel title-h-videos">
                                <?php echo $value->title; ?>
                            </div>
                            <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                    class="how-long"><?php echo $value->create_datetime ?></span></div>
                            <div class="box-videos">
                                <div class="hilight-cover"></div>
                                <div class="icon-play"><i class="fa fa-play-circle-o"></i></div>
                                <img src="<?php echo $value->urlImg; ?>">
                            </div>
                        </div>
                    </a>
                <?php }else{ ?>
                    <a style="" id="box-other-video-top-main" href="#large-videos-autoPlay"
                       onclick="setIndexVideosAutoPlay('<?php echo $value->urlIframe ?>','<?php echo $value->video_tag ?>','<?php echo $value->title ?>','','<?php echo $value->create_datetime ?>','<?php echo $value->video_id ?>')">
                        <div class="lastest-video">
                            <div class="title-owl-carousel title-h-videos">
                                <?php echo $value->title; ?>
                            </div>
                            <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                    class="how-long"><?php echo $value->create_datetime ?></span></div>
                            <div class="box-videos">
                                <div class="hilight-cover"></div>
                                <div class="icon-play"><i class="fa fa-play-circle-o"></i></div>
                                <img src="<?php echo $value->urlImg; ?>">
                            </div>
                        </div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
        <div style="clear: both;"></div>
    </div>
    <div style="clear: both;"></div>
</div>

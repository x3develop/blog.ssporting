<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/27/2017
 * Time: 17:04
 */
//foreach($commentMini as $key=>$value){
//    var_dump($key);
//    echo '<br>';
//}
//exit;
//$match=$playMatch->getFirstMathStatusBet(1);
?>
<style>
    .heart-active{
        color:#0091c8;
    }
</style>
<?php if(!empty($match)){?>
<div class="wrap-content-cols row section-games">
    <div id="fade-in-index" class="box-fade">
        <div class="close-tab pull-right close-tab-right"><i class="fa fa-times" aria-hidden="true"></i></div>
        <div class="wrap-selected-team-play wrap-left-team box-selected-index">
            <h1 id="h1-TeamHomeEn"><?=$match[0]->teamHomeEn?></h1>

            <p>คุณเลือกเล่นทีม</p>

            <div>
                <img id="img-TeamHomePath" src="<?=$match[0]->teamHomePath?>">
            </div>
            <form id="bet-home-match">
                <input name="bet[time_match]" class="form-control" type="hidden" value="<?= $match[0]->time_match; ?>">
                <input name="bet[user_id]" class="form-control" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
                <input name="bet[match_id]" class="form-control" type="hidden" value="<?= $match[0]->id; ?>">
                <input name="bet[team]" type="hidden" value="home">
<!--                <input name="bet[use_bet_coin]" type="hidden" value="750">-->
                <input name="bet[use_bet_gold]" type="hidden" value="0">
                <input name="bet[use_bet_coupon]" type="hidden" value="1">
                <input name="bet[status]" type="hidden" value="bet">

                <div class="box-commented" style="margin: 20px 0px;">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <div class="box-input-select">
                            <textarea name="bet[comment]" class="form-control" placeholder="แสดงความคิดเห็น..."></textarea>
                        </div>
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="clearfix"></div>
                </div>

                <div class="box-button-send">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-4">
                        <div class="box-input-select">
                            <img src="/images/coin/scoin100.png">
                            <select name="bet[use_bet_coin]">
                                <option value="1200">1200</option>
                                <option value="800">800</option>
                                <option value="400">400</option>
                                <option value="200">200</option>
                            </select>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box-button">
                            <button data-loading-text="Loading..." type="submit" class="btn btn-info btn-lg btn-block">Confirm</button>
                        </div>
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="clearfix"></div>
                </div>
            </form>
            <p>เลือกจำนวน Coin ที่ใช้ในการทายผล</p>
        </div>
    </div>
    <div id="fade-in-right-index" class="box-fade">
        <div class="close-tab pull-left close-tab-left"><i class="fa fa-times" aria-hidden="true"></i></div>
        <div class="wrap-selected-team-play wrap-right-team  box-selected-index">
            <h1 id="h1-TeamAwayEn"><?=$match[0]->teamAwayEn?></h1>

            <p>คุณเลือกเล่นทีม</p>

            <div>
                <img id="img-TeamAwayPath" src="<?=$match[0]->teamAwayPath?>">
            </div>
            <form id="bet-away-match">
                <input name="bet[time_match]" class="form-control" type="hidden" value="<?= $match[0]->time_match; ?>">
                <input name="bet[user_id]" class="form-control" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
                <input name="bet[match_id]" class="form-control" type="hidden" value="<?= $match[0]->id; ?>">
                <input name="bet[team]" type="hidden" value="away">
                <!--                <input name="bet[use_bet_coin]" type="hidden" value="750">-->
                <input name="bet[use_bet_gold]" type="hidden" value="0">
                <input name="bet[use_bet_coupon]" type="hidden" value="1">
                <input name="bet[status]" type="hidden" value="bet">

                <div class="box-commented" style="margin: 20px 0px;">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <div class="box-input-select">
                            <textarea name="bet[comment]" class="form-control" placeholder="แสดงความคิดเห็น..."></textarea>
                        </div>
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="clearfix"></div>
                </div>

                <div class="box-button-send">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-4">
                        <div class="box-input-select">
                            <img src="/images/coin/scoin100.png">
                            <select name="bet[use_bet_coin]">
                                <option value="1200">1200</option>
                                <option value="800">800</option>
                                <option value="400">400</option>
                                <option value="200">200</option>
                            </select>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box-button">
                            <button data-loading-text="Loading..." type="submit" class="btn btn-info btn-lg btn-block">Confirm</button>
                        </div>
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="clearfix"></div>
                </div>
            </form>
            <p>เลือกจำนวน Coin ที่ใช้ในการทายผล</p>
        </div>
    </div>
    <div class="row">
        <div class="bg-full">
            <div class="container" style="z-index: 50;">
                <div class="wrap-games rows" id="list-match-view">
                    <div class="col-sm-8 col-sm-match" id="col-sm-match-<?=$match[0]->id;?>">
                        <table class="table-games table-games2">
                            <tr>
                                <td>
                                    <div class="bx-head-team">
                                        <div class="hilight-cover"></div>
                                        <div class="vote-already vote-home"><?=((!empty($betMatchById['team']) and $betMatchById['team']=="home")?'VOTE':'')?></div>
                                        <div class="crop-team"></div>
                                        <div class="bx-logo-team"><img
                                                src="<?=$match[0]->teamHomePath?>"></div>
                                    </div>
                                </td>
                                <td>
                                    <div class="bx-head-team">
                                        <div class="hilight-cover"></div>
                                        <div class="vote-already vote-away"><?=((!empty($betMatchById['team']) and $betMatchById['team']=="away")?'VOTE':'')?></div>
                                        <div class="crop-team imgTest2"></div>
                                        <div class="bx-logo-team"><img
                                                src="<?=$match[0]->teamAwayPath?>"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="wrap-vote box-vote">
                                        <?php if(empty($betMatchById)){ ?>
                                        <div id="vote-left-<?=$match[0]->id;?>" class="vote-left">VOTE</div>
                                        <div id="vote-right-<?=$match[0]->id;?>" class="vote-right">VOTE</div>
                                        <?php } ?>
                                        <div class="bx-hdp box-hdp-vote">
                                            HDP
                                            <ul>
                                                <li style="padding: 0px;"><?=$match[0]->home_water_bill?></li>
                                                <li class="tb-hdp"><?=$match[0]->handicap?></li>
                                                <li style="padding: 0px;"><?=$match[0]->away_water_bill?></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="table-namteam">
                            <tr>
                                <td>
                                    <div class="name-teams"><?=$match[0]->teamHomeEn?></div>
                                </td>
                                <td>
                                    <div class="name-teams"><?=$match[0]->teamAwayEn?></div>
                                </td>
                            </tr>
                        </table>
                        <table class="table-ui-division">
                            <tr>
                                <td>
                                    <?php foreach ($matchResultsHome as $key=>$value){ ?>
                                    <?php if($value->scoreHome==$value->scoreAway){ ?>
                                        <img title="<?=$value->time_match?>" src="/images/icon-stat/result/1.png">
                                    <?php }elseif($value->team_home==$match[0]->team_home && $value->scoreHome>$value->scoreAway){?>
                                        <img title="<?=$value->time_match?>" src="/images/icon-stat/result/0.png">
                                    <?php }elseif($value->team_away==$match[0]->team_home && $value->scoreAway>$value->scoreHome){?>
                                        <img title="<?=$value->time_match?>" src="/images/icon-stat/result/0.png">
                                    <?php }else{ ?>
                                        <img title="<?=$value->time_match?>" src="/images/icon-stat/result/2.png">
                                    <?php }} ?>
                                </td>
                                <td>
                                    <span>
                                        <?=$match[0]->name?>
                                        <rt><?=date("j M H:i", (strtotime($match[0]->time_match)-(60*60)));?></rt>
                                    </span>
                                </td>
                                <td>
                                    <?php foreach ($matchResultsAway as $key=>$value){ ?>
                                        <?php if($value->scoreHome==$value->scoreAway){ ?>
                                            <img title="<?=$value->time_match?>" src="/images/icon-stat/result/1.png">
                                        <?php }elseif($value->team_home==$match[0]->team_away && $value->scoreHome>$value->scoreAway){?>
                                            <img title="<?=$value->time_match?>" src="/images/icon-stat/result/0.png">
                                        <?php }elseif($value->team_away==$match[0]->team_away && $value->scoreAway>$value->scoreHome){?>
                                            <img title="<?=$value->time_match?>" src="/images/icon-stat/result/0.png">
                                        <?php }else{ ?>
                                            <img title="<?=$value->time_match?>" src="/images/icon-stat/result/2.png">
                                        <?php }} ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="userBetMatchHome-<?=$match[0]->id;?>">
                                        <?php $betHomeCount=0;$betAwayCount=0;?>
                                        <?php foreach ($userListHome as $key=>$value){
                                            if ($key<8 &&$value->team=="home"){
                                                $betHomeCount++;?>
                                                <img src="https://graph.facebook.com/v2.8/<?php echo $value->fb_uid; ?>/picture">
                                            <?php }} ?>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <div id="userBetMatchAway-<?=$match[0]->id;?>">
                                        <?php foreach ($userListHome as $key=>$value){
                                            if ($key<8 && $value->team=="away"){
                                                $betAwayCount++;?>
                                                <img src="https://graph.facebook.com/v2.8/<?php echo $value->fb_uid; ?>/picture">
                                            <?php }} ?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="0"
                                             aria-valuemin="0" aria-valuemax="100"
                                             style="width: <?=(100+((($betHomeCount-($betAwayCount+$betHomeCount))/($betAwayCount+$betHomeCount))*100))?>%;">
                                            <span class="sr-only"><?=(100+((($betHomeCount-($betAwayCount+$betHomeCount))/($betAwayCount+$betHomeCount))*100))?>%</span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="bx-comments-games mg-bt15" style="background-color: #fff; box-shadow: none;">
                            <div class="rows">
                                <div class="col-sm-6" style="border-right: solid 1px #ccc;">
                                    <div class="box-comments" style="overflow-y: auto;height: 185px;">
                                        <table>
                                            <tbody id="box-comments-home-<?=$match[0]->id;?>">
                                            <?php if(isset($_SESSION['login'])){ ?>
                                                <tr>
                                                    <td>
                                                        <div class="box-img-user">
                                                            <img src="https://graph.facebook.com/v2.8/<?=$_SESSION['login']['fb_uid'] ?>/picture">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <form id="form-comment-home-<?=$match[0]->id;?>" class="form-comment-home">
                                                            <input type="hidden" name="comment_match[team]" value="home">
                                                            <input type="hidden" name="comment_match[match_id]" value="<?=$match[0]->id;?>">
                                                            <input class="form-control" name="comment_match[comment]" placeholder="ตอบกลับความคิดเห็น">
                                                        </form>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <?php foreach ($commentHome as $key=>$value){
                                                if($value->team=="home"){ ?>
                                            <tr id="box-comment-<?=$value->id?>">
                                                <td>
                                                    <div class="box-img-user"><img src="<?=((!empty($value->fb_uid))?"https://graph.facebook.com/v2.8/" .$value->fb_uid . "/picture":((!empty($value->path)) ? $value->path : '/images/avatar.png'))?>"></div>
                                                </td>
                                                <td>
                                                    <strong><?= ((!empty($value->name))?$value->name:$value->username);?></strong>
                                                    <?= $value->comment;?>
                                                    <div class="times-content"><span class="comment-updated_at" data-updated_at="<?= $value->updated_at ?>"><?= $value->updated_at ?></span>
                                                        <?php if(isset($_SESSION['login'])){ ?>
                                                        <span style="cursor: pointer;">
                                                            <?php if(!empty($heartListByUser[$_SESSION['login']['fb_uid']."-".$value->id])){?>
                                                                <i id="commemt-heart-<?=$value->id?>" onclick="removeHeart(<?=$heartListByUser[$_SESSION['login']['fb_uid']."-".$value->id][0]?>)" class="fa fa-heart heart-active"></i>
                                                                <span class="heart-count" id="commemt-comment_match-<?=$value->id?>"><?=((!empty($heartList[$value->id]))?$heartList[$value->id][0]:'')?>
                                                            <?php }else{ ?>
                                                                <i id="commemt-heart-<?=$value->id?>" onclick="addHeart(<?=$value->id?>,<?=$match[0]->id;?>)" class="fa fa-heart"></i>
                                                                <span class="heart-count" id="commemt-comment_match-<?=$value->id?>"><?=((!empty($heartList[$value->id]))?$heartList[$value->id][0]:'')?>
                                                            <?php } ?>
                                                        </span>
                                                        <?php } ?>
                                                        <?php if(!empty($value->countComment)){ ?>
                                                            <span class="show-bx-reply"><?=$value->countComment.' ข้อความตอบกลับ'?></span>
                                                        <?php }else{ ?>
                                                            <span class="show-bx-reply">ตอบกลับ</span>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="bx-reply hide">
                                                        <table>
                                                        <tbody id="comment-bx_reply-<?=$value->id?>">
                                                        <?php if(!empty($commentMini[$value->id])){ ?>
                                                            <?php foreach($commentMini[$value->id] as $kMini=>$vMini){?>
                                                            <tr id="box-comment-<?=$vMini['id']?>">
                                                                <td>
                                                                    <div class="box-img-user">
                                                                        <img src="https://graph.facebook.com/v2.8/<?=$vMini['fb_uid'] ?>/picture">
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <strong id="Minicomment-name"><?= ((!empty($vMini['name']))?$vMini['name']:$vMini['username']);?></strong>
                                                                    <span id="Minicomment-comment"><?= $vMini['comment'];?></span>
                                                                    <div class="times-content"><span class="comment-updated_at" data-updated_at="<?= $vMini['updated_at'] ?>"><?= $vMini['updated_at'] ?></span>
                                                                        <?php if(isset($_SESSION['login'])){ ?>
                                                                            <span style="cursor: pointer;">
                                                                        <?php if(!empty($heartListByUser[$_SESSION['login']['fb_uid']."-".$vMini['id']])){?>
                                                                            <i id="Minicommemt-heart-<?=$vMini['id']?>" onclick="removeHeart(<?=$heartListByUser[$_SESSION['login']['fb_uid']."-".$vMini['id']][0]?>)" class="fa fa-heart heart-active"></i>
                                                                                <span class="heart-count" id="Minicommemt-comment_match-<?=$vMini['id']?>"><?=$vMini['countHeart']?>
                                                                        <?php }else{ ?>
                                                                            <i id="Minicommemt-heart-<?=$vMini['id']?>" onclick="addHeart(<?=$vMini['id']?>,<?=$match[0]->id;?>)" class="fa fa-heart"></i>
                                                                                <span class="heart-count" id="Minicommemt-comment_match-<?=$vMini['id']?>"><?=$vMini['countHeart']?>
                                                                        <?php } ?>
                                                                            </span>
                                                                        <?php } ?>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        <?php }} ?>
                                                            <tr>
                                                                <td>
                                                                    <div class="box-img-user">
                                                                        <img src="https://graph.facebook.com/v2.8/<?=$_SESSION['login']['fb_uid'] ?>/picture">
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <form class="form-comment form-comment-<?=$value->id?>">
                                                                        <input type="hidden" name="comment_match[team]" value="home">
                                                                        <input type="hidden" name="comment_match[comment_match_id]" value="<?=$value->id?>">
                                                                        <input type="hidden" name="comment_match[match_id]" value="<?=$match[0]->id;?>">
                                                                        <input name="comment_match[comment]" class="form-control" placeholder="ตอบกลับความคิดเห็น">
                                                                    </form>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php }} ?>
                                            <tr id="main-miniComments" class="hide">
                                                <td>
                                                    <div class="box-img-user">
                                                        <img id="comment-profile" src="">
                                                    </div>
                                                </td>
                                                <td>
                                                    <strong id="comment-name"></strong>
                                                    <span id="comment-comment"></span>
                                                    <div class="times-content">
                                                        <span class="comment-updated_at"></span>
                                                        <span>
                                                            <i id="commemt-heart-" class="fa fa-heart"></i>
                                                            <span id="commemt-comment_match-" class="heart-count"></span>
                                                        </span>
                                                        <!--                                                        <span>ตอบกลับ</span>-->
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr id="main-Comments" class="hide">
                                                <td>
                                                    <div class="box-img-user"><img id="comment-profile" src=""></div>
                                                </td>
                                                <td>
                                                    <strong id="comment-name"></strong>
                                                    <span id="comment-comment"></span>
                                                    <div class="times-content">
                                                        <span class="comment-updated_at"></span>
                                                        <span>
                                                            <i class="fa fa-heart"></i>
                                                            <span class="heart-count"></span>
                                                        </span>
                                                        <span class="show-bx-reply">ตอบกลับ</span>
                                                    </div>
                                                    <div class="bx-reply hide">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <div class="box-img-user">
                                                                        <img src="https://graph.facebook.com/v2.8/<?=$_SESSION['login']['fb_uid'] ?>/picture">
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <form id="form-comment">
                                                                        <input type="hidden" name="comment_match[team]" value="home">
                                                                        <input type="hidden" name="comment_match[comment_match_id]">
                                                                        <input type="hidden" name="comment_match[match_id]">
                                                                        <input class="form-control" placeholder="ตอบกลับความคิดเห็น">
                                                                    </form>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="box-comments" style="overflow-y: auto;height: 185px;">
                                        <table>
                                            <tbody id="box-comments-away-<?=$match[0]->id;?>">
                                            <?php if(isset($_SESSION['login'])){ ?>
                                                <tr>
                                                    <td>
                                                        <div class="box-img-user">
                                                            <img src="https://graph.facebook.com/v2.8/<?=$_SESSION['login']['fb_uid'] ?>/picture">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <form id="form-comment-away-<?=$match[0]->id;?>" class="form-comment-away">
                                                            <input type="hidden" name="comment_match[team]" value="away">
                                                            <input type="hidden" name="comment_match[match_id]" value="<?=$match[0]->id;?>">
                                                            <input class="form-control" name="comment_match[comment]" placeholder="ตอบกลับความคิดเห็น">
                                                        </form>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <?php foreach ($commentAway as $key=>$value){
                                            if($value->team=="away"){ ?>
                                            <tr id="box-comment-<?=$value->id?>">
                                                <td>
                                                    <div class="box-img-user"><img src="<?=((!empty($value->fb_uid))?"https://graph.facebook.com/v2.8/" .$value->fb_uid . "/picture":((!empty($value->path)) ? $value->path : '/images/avatar.png'))?>"></div>
                                                </td>
                                                <td>
                                                    <strong><?= ((!empty($value->name))?$value->name:$value->username);?></strong>
                                                    <?= $value->comment;?>
                                                    <div class="times-content"><span class="comment-updated_at" data-updated_at="<?= $value->updated_at ?>"><?= $value->updated_at ?></span>
                                                        <?php if(isset($_SESSION['login'])){ ?>
                                                            <span style="cursor: pointer;">
                                                            <?php if(!empty($heartListByUser[$_SESSION['login']['fb_uid']."-".$value->id])){?>
                                                                <i id="commemt-heart-<?=$value->id?>" onclick="removeHeart(<?=$heartListByUser[$_SESSION['login']['fb_uid']."-".$value->id][0]?>)" class="fa fa-heart heart-active"></i>
                                                                <span class="heart-count" id="commemt-comment_match-<?=$value->id?>"><?=((!empty($heartList[$value->id]))?$heartList[$value->id][0]:'')?>
                                                            <?php }else{ ?>
                                                                <i id="commemt-heart-<?=$value->id?>" onclick="addHeart(<?=$value->id?>,<?=$match[0]->id;?>)" class="fa fa-heart"></i>
                                                                <span class="heart-count" id="commemt-comment_match-<?=$value->id?>"><?=((!empty($heartList[$value->id]))?$heartList[$value->id][0]:'')?>
                                                            <?php } ?>
                                                            </span>
                                                        <?php } ?>
                                                        <?php if(!empty($value->countComment)){ ?>
                                                            <span class="show-bx-reply"><?=$value->countComment.' ข้อความตอบกลับ'?></span>
                                                        <?php }else{ ?>
                                                            <span class="show-bx-reply">ตอบกลับ</span>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="bx-reply hide">
                                                        <table>
                                                            <tbody id="comment-bx_reply-<?=$value->id?>">
                                                            <?php if(!empty($commentMini[$value->id])){ ?>
                                                                <?php foreach($commentMini[$value->id] as $kMini=>$vMini){?>
                                                                    <tr id="box-comment-<?=$vMini['id']?>">
                                                                        <td>
                                                                            <div class="box-img-user">
                                                                                <img src="https://graph.facebook.com/v2.8/<?=$vMini['fb_uid'] ?>/picture">
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <strong id="Minicomment-name"><?= ((!empty($vMini['name']))?$vMini['name']:$vMini['username']);?></strong>
                                                                            <span id="Minicomment-comment"><?= $vMini['comment'];?></span>
                                                                            <div class="times-content"><span class="comment-updated_at" data-updated_at="<?= $vMini['updated_at'] ?>"><?= $vMini['updated_at'] ?></span>
                                                                                <?php if(isset($_SESSION['login'])){ ?>
                                                                                <span style="cursor: pointer;">
                                                                                <?php if(!empty($heartListByUser[$_SESSION['login']['fb_uid']."-".$vMini['id']])){?>
                                                                                    <i id="Minicommemt-heart-<?=$vMini['id']?>" onclick="removeHeart(<?=$heartListByUser[$_SESSION['login']['fb_uid']."-".$vMini['id']][0]?>)" class="fa fa-heart heart-active"></i>
                                                                                <span class="heart-count" id="Minicommemt-comment_match-<?=$vMini['id']?>"><?=$vMini['countHeart']?>
                                                                                    <?php }else{ ?>
                                                                                    <i id="Minicommemt-heart-<?=$vMini['id']?>" onclick="addHeart(<?=$vMini['id']?>,<?=$match[0]->id;?>)" class="fa fa-heart"></i>
                                                                                <span class="heart-count" id="Minicommemt-comment_match-<?=$vMini['id']?>"><?=$vMini['countHeart']?>
                                                                                    <?php } ?>
                                                                            </span>
                                                                                    <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                <?php }} ?>
                                                            <tr>
                                                                <td>
                                                                    <div class="box-img-user">
                                                                        <img src="https://graph.facebook.com/v2.8/<?=$_SESSION['login']['fb_uid'] ?>/picture">
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <form class="form-comment form-comment-<?=$value->id?>">
                                                                        <input type="hidden" name="comment_match[team]" value="away">
                                                                        <input type="hidden" name="comment_match[comment_match_id]" value="<?=$value->id?>">
                                                                        <input type="hidden" name="comment_match[match_id]" value="<?=$match[0]->id;?>">
                                                                        <input name="comment_match[comment]" class="form-control" placeholder="ตอบกลับความคิดเห็น">
                                                                    </form>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php }} ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12 hide">
                                    <div class="comment-input-main">
                                        <input id="comment-inputbox" class="form-control"
                                               placeholder="แสดงความคิดเห็น"
                                               style="border: 0px; background-color: #03385f;">
                                        <button class="btn btn-primary pull-right" id="post-comment">Post</button>
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="h-tabs-game">เกมทายผลบอล</div>
                        <div class="table-game-all mg-bt15" style="background-color: #03385f;">
                            <div class="wrap-game-today wrap-game-today2">
                                <div id="about" class="nano bx4">
                                    <div class="content">
                                    <?php foreach ($match as $key=>$value){ ?>
                                        <div data-timeMatch="<?=$value->time_match?>" data-matchId="<?=$value->id?>" id="list-match-all-<?=$value->id?>" class="list-match-all box-results bottom-line <?=(($key==0)?'active-playing':'')?>">
                                            <table class="table-list-games table-team-index">
                                                <tr>
                                                    <td><img src="<?=$value->teamHomePath?>">
                                                        <label class="<?=((!empty($value->handicap) && $value->handicap<0)?'team-hdp':'')?>"><?=$value->teamHomeEn?></label>
                                                    </td>
                                                    <td class="text-right"><label class="<?=(($value->status=="fullTime")?'':'info')?>"><?=(($value->status=="fullTime")?((!empty($value->home_over_time_score))?$value->home_end_time_score."(".$value->home_over_time_score.")":$value->home_end_time_score):date("H:i", (strtotime($value->time_match)-(60*60))))?></label></td>
                                                </tr>
                                                <tr>
                                                    <td><img src="<?=$value->teamAwayPath?>">
                                                        <label class="<?=((!empty($value->handicap) && $value->handicap>0)?'team-hdp':'')?>"><?=$value->teamAwayEn?></label>
                                                    </td>
                                                    <td class="text-right"><label><?=(($value->status=="fullTime")?((!empty($value->away_over_time_score))?$value->away_end_time_score."(".$value->away_over_time_score.")":$value->away_end_time_score):$value->handicap)?></label></td>
                                                </tr>
                                            </table>
                                        </div>
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="label-game"
                             style="color: #333;font-size: 12rem;width: 4.80rem;font-weight: bold;">Games
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<div style="clear: both;"></div>
<script src="/js/play/game.js"></script>
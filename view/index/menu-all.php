<?php
if (!isset($_SESSION)) {
    session_start();
}
$webroot = $_SERVER['DOCUMENT_ROOT'];
?>
<link rel="stylesheet" href="/css/sidebar-menu.css">
<script src="/js/newFB.js"></script>
<script>
    $('#myAffix').affix({
        offset: {
            top: 100,
            bottom: function () {
                return (this.bottom = $('.footer').outerHeight(true))
            }
        }
    })

    $(document).ready(function () {
        var trigger = $('.hamburger'),
            overlay = $('.overlay'),
            isClosed = false;

        trigger.click(function () {
            hamburger_cross();
        });

        function hamburger_cross() {

            if (isClosed == true) {
                overlay.hide();
                trigger.removeClass('is-open');
                trigger.addClass('is-closed');
                isClosed = false;
            } else {
                overlay.show();
                trigger.removeClass('is-closed');
                trigger.addClass('is-open');
                isClosed = true;
            }
        }

        $('[data-toggle="offcanvas"]').click(function () {
            $('#wrapper').toggleClass('toggled');
        });
    });
</script>
<input type="hidden" id="statusLogin" value="<?= ((isset($_SESSION['login'])) ? 'true' : 'false') ?>">
<div class="content-menu-index content-menu-all" data-spy="affix" data-offset-top="60">
    <nav class="navbar navbar-topbar">
        <div class="navbar-header">
            <div class="pull-left">
                <a class="navbar-brand" href="#">
                    <img src="/images/logo/logo-ngoal.png">
                </a>
            </div>
            <div class="pull-right navbar-header-right">
                <ul>

                    <li>
                        <div class="box-login-FB">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                            <span>Login</span>
                        </div>
                    </li>
                    <li>
                        <div class="user-topmenu collapsed" data-toggle="collapse"
                             data-target="#userbar" aria-expanded="false" aria-controls="userbar">
                            <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                        </div>
                    </li>
                    <li>
                        <div class="collapsed collapse-menu-btn" data-toggle="collapse"
                             data-target="#navbar"
                             aria-expanded="false" aria-controls="navbar">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </div>
                    </li>
                </ul>

            </div>
        </div>
        <div id="userbar" class="userbar-collapse collapse">
            <div class="userbar-collapse-body">
                <ul>
                    <li class="user-detail text-center">
                        <img src="/images/9.jpg">
                        <span><strong>Wayne Rooney</strong></span>
                    </li>
                    <li class="text-center user-coin-box">
                        <table>
                            <tr>
                                <td><img src="/images/coin/sgold100.png"><span>2,668.00</span></td>
                            </tr>
                            <tr>
                                <td><img src="/images/coin/scoin100.png"><span>3,200.00</span></td>
                            </tr>
                        </table>
                    </li>
                    <li>
                        <button type="button" class="btn btn-primary btn-lg">Edit Profile</button>
                    </li>
                    <li>
                        <button type="button" class="btn btn-danger btn-lg">Log out</button>
                    </li>
                </ul>
            </div>
        </div>
        <!--        เมนูใน smartphone-->
        <div id="navbar" class="collapse collapse-menu">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <!--                หน้าหลัก-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="/">หน้าหลัก</a>
                        </h4>
                    </div>
                </div>
                <!--                วีดีโอ/ไฮไลท์บอล-->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                วีดีโอ/ไฮไลท์บอล
                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <ul>
                                <li><a href="/highlight?category=premier league">พรีเมียร์ลีก</a></li>
                                <li><a href="/highlight?category=la liga">ลาลีกา</a></li>
                                <li><a href="/highlight?category=bundesliga">บุนเดสลีกา</a></li>
                                <li><a href="/highlight?category=serie a">ซีเรียอา</a></li>
                                <li><a href="/highlight?category=league 1">ลีกเอิง</a></li>
                                <li><a href="/highlight?category=thai">ไทยพรีเมียลีก</a></li>
                                <li><a href="/highlight?category=ucl">ยูฟ่าแชมเปียนลีก</a></li>
                                <li><a href="/highlight?category=europa">ยูโรป้า</a></li>
                                <li><a href="/highlight?category=international">ฟุตบอลทีมชาติ</a></li>
                                <li><a href="/highlight#highlightOthersTab">HotHit Video</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--                ข่าวบอลต่างประเทศ-->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                ข่าวบอลต่างประเทศ
                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingThree">
                        <div class="panel-body">
                            <ul>
                                <li><a href="/news?type=premier league&Page=1">ข่าวพรีเมียร์ลีก</a></li>
                                <li><a href="/news?type=la liga&Page=1">ข่าวลาลีกา</a></li>
                                <li><a href="/news?type=bundesliga&Page=1">ข่าวบุนเดสลีกา</a></li>
                                <li><a href="/news?type=serie a&Page=1">ข่าวซีเรียอา</a></li>
                                <li><a href="/news?type=ucl&Page=1">ข่าวยูฟ่าแชมเปียนลีก</a></li>
                                <li><a href="/news?type=europa&Page=1">ข่าวยูโรป้า</a></li>
                                <li><a href="/news?type=thai&Page=1">ข่าวฟุตบอลทีมชาติ</a></li>
                                <li><a href="/news?type=other league&Page=1">ข่าวฟุตบอลลีกอื่น</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--                ข่าวบอลไทย-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="/news?type=thai&Page=1">ข่าวบอลไทย</a>
                        </h4>
                    </div>
                </div>
                <!--                ข่าวกีฬาอื่นๆ-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="/news?type=other&Page=1">ข่าวกีฬาอื่นๆ</a>
                        </h4>
                    </div>
                </div>
                <!--                Sexy Football-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#">Sexy Football</a>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-wrapper-menu">
            <!--   แทบบนสุด-->
            <div class="topmenu">
                <div class="container">
                    <div class="pull-left contact-social">
                        <ul>
                            <li>
                                <a href="https://www.facebook.com/nGoalTH/"><i class="fa fa-facebook"
                                                                               aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="https://twitter.com/nGoalTH"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="pull-right box-login-FB">
                        <?php if (!isset($_SESSION['login'])) { ?>
                            <a class="fb-login-button" onclick="FBlogin();">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                <span>Login</span>
                            </a>
                        <?php } ?>
                    </div>
                    <div class="pull-right content-profile-menu">
                        <ul>
                            <?php if (isset($_SESSION['login'])) { ?>
                                <li class="coin-topmenu"><a href=""><img src="/images/coin/sgold100.png">
                                        <span><?php echo number_format($_SESSION['login']['gold'], 2) ?></span></a>
                                </li>
                                <li class="coin-topmenu"><a href=""><img src="/images/coin/scoin100.png">
                                        <span><?php echo number_format($_SESSION['login']['coin'], 2) ?></span></a>
                                </li>
                            <?php } ?>
                            <li class="pull-right <?= ((!isset($_SESSION['login'])) ? 'hide' : '') ?>" id="fb-online">
                                <a href="#" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="true">
                                    <img id="fb-picture"
                                         src="<?= ((isset($_SESSION['login'])) ? 'https://graph.facebook.com/v2.8/' . $_SESSION['login']['fb_uid'] . '/picture?type=small' : '') ?>">
                                    <label id="fb-name"><?= ((isset($_SESSION['login'])) ? $_SESSION['login']['name'] : '') ?></label>
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="dLabel" style="width: 150px;">
                                    <li><a id="own-profile-link" href="/profile">
                                            ตั้งค่าโปรไฟล์</a>
                                    </li>
                                    <li><a href="/rules">กติกาและเงื่อนไข</a></li>
                                    <li><a href="/ranking">Ranking</a></li>
                                    <li><a href="/help">วิธีการเล่นเกมทายผล</a></li>
                                    <li><a id="facebook-logoutbutton" href="/logout">
                                            ออกจากระบบ</a>
                                    </li>
                                </ul>
                                <!--                                <div class="popover fade bottom in dropdown-menu dropdown-menu-profile" role="tooltip"-->
                                <!--                                     aria-labelledby="dropdownMenu1" style="margin-top: 33px;">-->
                                <!--                                    <div class="arrow"></div>-->
                                <!--                                    <div class="popover-content">-->
                                <!--                                        <form>-->
                                <!--                                            <ul style="color: #333;">-->
                                <!--                                                -->
                                <!--                                            </ul>-->
                                <!--                                        </form>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                            </li>
                            <?php if (!isset($_SESSION['login'])) { ?>
                            <?php } ?>
                        </ul>
                    </div>

                </div>
            </div>


            <!--/.navbar-collapse -->
            <?php
            if (file_exists($webroot . '/load/main-menu-index.html')) {
                echo "<span style='display: none;'>/load/main-menu-index.html</span>";
                include $_SERVER["DOCUMENT_ROOT"] . '/load/main-menu-index.html';
            } else { ?>
                <div class="main-menu-index navbar-top full-width" style="padding-left: 0;">
                    <div class="container container-wide">
                        <div class="pull-left logo-menu">
                            <a class="link_wrap" href="/"></a>
                            <img src="/images/logo/logo-ngoal-white.png">
                        </div>
                        <div class="nav-menutop nav-menutop-edit pull-left nav-menu-main">
                            <ul class="nav nav-pills">
                                <li><a href="/">หน้าหลัก</a></li>
                                <li class="active-dropdown">
                                    <!--                                    href="/highlight.php"-->
                                    <a>
                                        <div class="hot-icon">
                                            <i class="fa fa-circle" aria-hidden="true"></i>
                                        </div>
                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                        <div class="bx-menu-dropdown-arrow">
                                            <i class="fa fa-caret-up" aria-hidden="true"></i>
                                        </div>
                                        <div class="bx-menu-dropdown">
                                            <div class="container">
                                                <div class="col-md-2 list-content-big-menu">
                                                    <h3>วีดีโอ/ไฮไลท์บอล</h3>
                                                    <ul>
                                                        <li>
                                                            <!--                                                    href="/highlight"-->
                                                            <a href="/highlight">วีดีโอ/ไฮไลท์บอล</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li class="active">
                                                            <!--                                                    href="/highlight?category=premier league"-->
                                                            <a style="cursor: pointer;" class="menu-highlight"
                                                               data-category="premier_league">พรีเมียร์ลีก</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li>
                                                            <!--                                                    href="/highlight?category=la liga"-->
                                                            <a style="cursor: pointer;" class="menu-highlight"
                                                               data-category="la_liga">ลาลีกา</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li>
                                                            <!--                                                    href="/highlight?category=bundesliga"-->
                                                            <a style="cursor: pointer;" class="menu-highlight"
                                                               data-category="bundesliga">บุนเดสลีกา</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li>
                                                            <!--                                                    href="/highlight?category=serie a"-->
                                                            <a style="cursor: pointer;" class="menu-highlight"
                                                               data-category="serie_a">ซีเรียอา</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li>
                                                            <!--                                                    href="/highlight?category=league 1"-->
                                                            <a style="cursor: pointer;" class="menu-highlight"
                                                               data-category="league_1">ลีกเอิง</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <!--                                                <li>-->
                                                        <!--                                                    href="/highlight?category=thai"-->
                                                        <!--                                                    <a style="cursor: pointer;" class="menu-highlight" data-category="thai">ไทยพรีเมียลีก</a>-->
                                                        <!--                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>-->
                                                        <!--                                                </li>-->
                                                        <li>
                                                            <!--                                                    href="/highlight?category=ucl"-->
                                                            <a style="cursor: pointer;" class="menu-highlight"
                                                               data-category="ucl">ยูฟ่าแชมเปียนลีก</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li>
                                                            <!--                                                    href="/highlight?category=europa"-->
                                                            <a style="cursor: pointer;" class="menu-highlight"
                                                               data-category="europa">ยูโรป้า</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li>
                                                            <!--                                                    href="/highlight?category=international"-->
                                                            <a style="cursor: pointer;" class="menu-highlight"
                                                               data-category="international">ฟุตบอลทีมชาติ</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>

                                                    </ul>
                                                </div>
                                                <?php if (!empty($videoHighlight)) { ?>
                                                    <?php foreach ($videoHighlight as $key => $value) { ?>
                                                        <div class="div-highlight col-md-6 detail-content-big-menu <?= (($key == "premier_league") ? '' : 'hide'); ?>"
                                                             id="highlight-<?= $key ?>">
                                                            <div class="col-sm-12">
                                                                <h3 class="pull-left">
                                                                    <i class="fa fa-youtube-play"
                                                                       aria-hidden="true"></i>
                                                                    <?php if ($key == "premier_league") { ?>
                                                                        ไฮไลท์ พรีเมียร์ลีก
                                                                    <?php } elseif ($key == "la_liga") { ?>
                                                                        ไฮไลท์ ลาลีกา
                                                                    <?php } elseif ($key == "bundesliga") { ?>
                                                                        ไฮไลท์ บุนเดสลีกา
                                                                    <?php } elseif ($key == "serie_a") { ?>
                                                                        ไฮไลท์ ซีเรียอา
                                                                    <?php } elseif ($key == "league_1") { ?>
                                                                        ไฮไลท์ ลีกเอิง
                                                                    <?php } elseif ($key == "thai") { ?>
                                                                        ไฮไลท์ ไทยพรีเมียลีก
                                                                    <?php } elseif ($key == "ucl") { ?>
                                                                        ไฮไลท์ ยูฟ่าแชมเปียนลีก
                                                                    <?php } elseif ($key == "europa") { ?>
                                                                        ไฮไลท์ ยูโรป้า
                                                                    <?php } elseif ($key == "international") { ?>
                                                                        ไฮไลท์ ฟุตบอลทีมชาติ
                                                                    <?php } ?>
                                                                </h3>
                                                                <a class="pull-right"
                                                                   href="/highlight?category=<?= str_replace('_', ' ', $key); ?>">ทั้งหมด</a>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="row">
                                                                    <div class="col-sm-6">

                                                                        <div class="thumbnail">

                                                                            <div class="box-videos">
                                                                                <a href="/highlight?videoId=<?= $value[0]->video_id ?>">
                                                                                    <div class="hilight-cover"></div>
                                                                                    <div class="icon-play">
                                                                                        <i class="fa fa-play"></i>
                                                                                    </div>
                                                                                    <img src="<?= $value[0]->thumbnail ?>">
                                                                                </a>
                                                                            </div>
                                                                            <div class="caption">
                                                                                <a href="/highlight?videoId=<?= $value[0]->video_id ?>">
                                                                                    <h3><?= $value[0]->title ?></h3></a>
                                                                                <p>
                                                                                    <a href="/highlight?videoId=<?= $value[0]->video_id ?>"
                                                                                       class="btn btn-primary btn-sm"
                                                                                       role="button">
                                                                                        อ่านทั้งหมด
                                                                                    </a>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <a href="/highlight?videoId=<?= $value[1]->video_id ?>">
                                                                            <div class="thumbnail">
                                                                                <div class="box-videos">
                                                                                    <a href="/highlight?videoId=<?= $value[1]->video_id ?>">
                                                                                        <div class="hilight-cover"></div>
                                                                                        <div class="icon-play">
                                                                                            <i class="fa fa-play"></i>
                                                                                        </div>
                                                                                        <img src="<?= $value[1]->thumbnail ?>">
                                                                                    </a>
                                                                                </div>
                                                                                <div class="caption">
                                                                                    <a href="/highlight?videoId=<?= $value[1]->video_id ?>">
                                                                                        <h3><?= $value[1]->title ?></h3>
                                                                                    </a>
                                                                                    <p>
                                                                                        <a href="/highlight?videoId=<?= $value[1]->video_id ?>"
                                                                                           class="btn btn-primary btn-sm"
                                                                                           role="button">
                                                                                            อ่านทั้งหมด
                                                                                        </a>
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                                <div class="col-md-4 detail-content-big-menu">
                                                    <div class="col-sm-12">
                                                        <h3 class="pull-left">
                                                            <i class="fa fa-fire" aria-hidden="true"></i> HotHit Video
                                                        </h3>
                                                        <a class="pull-right" href="/hothit-videos">ทั้งหมด</a>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="thumbnail">
                                                                    <div class="box-videos" style="height: 204px;">
                                                                        <a href="/hothit-videos?videoId=<?= $lastVideo[0]->video_id ?>">
                                                                            <!--                                                                    <div class="hilight-cover"></div>-->
                                                                            <div class="icon-play">
                                                                                <i class="fa fa-play"></i>
                                                                            </div>
                                                                            <div class="cover-hothit"><img
                                                                                        src="/images/All_.png"></div>
                                                                            <img src="<?= $lastVideo[0]->urlImg ?>">
                                                                        </a>
                                                                    </div>
                                                                    <div class="caption">
                                                                        <a href="/hothit-videos?videoId=<?= $lastVideo[0]->video_id ?>">
                                                                            <h3><?= $lastVideo[0]->title ?></h3></a>
                                                                        <p>
                                                                            <a href="/hothit-videos?videoId=<?= $lastVideo[0]->video_id ?>"
                                                                               class="btn btn-primary btn-sm"
                                                                               role="button">
                                                                                อ่านทั้งหมด
                                                                            </a>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear: both;"></div>
                                    </a>

                                </li>
                                <li class="active-dropdown">
                                    <a>
                                        ข่าวบอลต่างประเทศ
                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                        <div class="bx-menu-dropdown-arrow">
                                            <i class="fa fa-caret-up" aria-hidden="true"></i>
                                        </div>
                                        <div class="bx-menu-dropdown">
                                            <div class="container">
                                                <div class="col-md-2 list-content-big-menu">
                                                    <h3>ข่าวฟุตบอลต่างประเทศ</h3>
                                                    <ul>
                                                        <li class="active">
                                                            <!--                                                    href="/news?type=premier league&Page=1"-->
                                                            <a style="cursor: pointer;" class="menu-videoNews"
                                                               data-category="premier_league">ข่าวพรีเมียร์ลีก</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li>
                                                            <!--                                                    href="/news?type=la liga&Page=1"-->
                                                            <a style="cursor: pointer;" class="menu-videoNews"
                                                               data-category="la_liga">ข่าวลาลีกา</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li>
                                                            <!--                                                    href="/news?type=bundesliga&Page=1"-->
                                                            <a style="cursor: pointer;" class="menu-videoNews"
                                                               data-category="bundesliga">ข่าวบุนเดสลีกา</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li>
                                                            <!--                                                    href="/news?type=serie a&Page=1"-->
                                                            <a style="cursor: pointer;" class="menu-videoNews"
                                                               data-category="serie_a">ข่าวซีเรียอา</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li>
                                                            <!--                                                    href="/news?type=ucl&Page=1"-->
                                                            <a style="cursor: pointer;" class="menu-videoNews"
                                                               data-category="ucl">ข่าวยูฟ่าแชมเปียนลีก</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li>
                                                            <!--                                                    href="/news?type=europa&Page=1"-->
                                                            <a style="cursor: pointer;" class="menu-videoNews"
                                                               data-category="europa">ข่าวยูโรป้า</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li>
                                                            <!--                                                    href="/news?type=thai&Page=1"-->
                                                            <a style="cursor: pointer;" class="menu-videoNews"
                                                               data-category="thai">ข่าวฟุตบอลทีมชาติ</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li>
                                                            <!--                                                    href="/videos?category=ucl"-->
                                                            <a style="cursor: pointer;" class="menu-videoNews"
                                                               data-category="ucl">ยูฟ่าแชมเปียนลีก</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li>
                                                            <!--                                                    href="/news?type=other league&Page=1"-->
                                                            <a style="cursor: pointer;" class="menu-videoNews"
                                                               data-category="other">ข่าวฟุตบอลลีกอื่น</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>

                                                    </ul>
                                                </div>
                                                <?php foreach ($videoNews as $key => $value) { ?>
                                                    <div class="div-videoNews col-md-10 detail-content-big-menu <?= (($key == "premier_league") ? '' : 'hide'); ?>"
                                                         id="videoNews-<?= $key ?>">
                                                        <div class="col-sm-12">
                                                            <h3 class="pull-left">
                                                                <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                                                                <?php if ($key == "premier_league") { ?>
                                                                    ข่าว พรีเมียร์ลีก
                                                                <?php } elseif ($key == "la_liga") { ?>
                                                                    ข่าว ลาลีกา
                                                                <?php } elseif ($key == "bundesliga") { ?>
                                                                    ข่าว บุนเดสลีกา
                                                                <?php } elseif ($key == "serie_a") { ?>
                                                                    ข่าว ซีเรียอา
                                                                <?php } elseif ($key == "league_1") { ?>
                                                                    ข่าว ลีกเอิง
                                                                <?php } elseif ($key == "thai") { ?>
                                                                    ข่าว ไทยพรีเมียลีก
                                                                <?php } elseif ($key == "ucl") { ?>
                                                                    ข่าว ยูฟ่าแชมเปียนลีก
                                                                <?php } elseif ($key == "europa") { ?>
                                                                    ข่าว ยูโรป้า
                                                                <?php } elseif ($key == "international") { ?>
                                                                    ข่าว ฟุตบอลทีมชาติ
                                                                <?php } ?>
                                                            </h3>
                                                            <a class="pull-right"
                                                               href="/news?type=<?= str_replace('_', ' ', $key); ?>">ทั้งหมด</a>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <div class="thumbnail">
                                                                        <div class="bx-story-small">
                                                                            <a href="/articles?newsid=<?= $value[0]->newsid ?>">
                                                                                <div class="hilight-cover"></div>
                                                                                <div class="crop"
                                                                                     style="background-image: url(<?= $value[0]->imageLink ?>)">
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                        <div class="caption">
                                                                            <a href="/articles?newsid=<?= $value[0]->newsid ?>">
                                                                                <h3><?= $value[0]->titleTh ?></h3></a>
                                                                            <p>
                                                                                <a href="/articles?newsid=<?= $value[0]->newsid ?>"
                                                                                   class="btn btn-primary btn-sm"
                                                                                   role="button">
                                                                                    อ่านทั้งหมด
                                                                                </a>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="thumbnail">
                                                                        <div class="bx-story-small">
                                                                            <a href="/articles?newsid=<?= $value[1]->newsid ?>">
                                                                                <div class="hilight-cover"></div>
                                                                                <div class="crop"
                                                                                     style="background-image: url(<?= $value[1]->imageLink ?>)">
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                        <div class="caption">
                                                                            <a href="/articles?newsid=<?= $value[1]->newsid ?>">
                                                                                <h3><?= $value[1]->titleTh ?></h3></a>
                                                                            <p>
                                                                                <a href="/articles?newsid=<?= $value[1]->newsid ?>"
                                                                                   class="btn btn-primary btn-sm"
                                                                                   role="button">
                                                                                    อ่านทั้งหมด
                                                                                </a>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="thumbnail">
                                                                        <div class="bx-story-small">
                                                                            <a href="/articles?newsid=<?= $value[2]->newsid ?>">
                                                                                <div class="hilight-cover"></div>
                                                                                <div class="crop"
                                                                                     style="background-image: url(<?= $value[2]->imageLink ?>)"></div>
                                                                            </a>
                                                                        </div>
                                                                        <div class="caption">
                                                                            <a href="/articles?newsid=<?= $value[2]->newsid ?>">
                                                                                <h3><?= $value[2]->titleTh ?></h3></a>
                                                                            <p>
                                                                                <a href="/articles?newsid=<?= $value[2]->newsid ?>"
                                                                                   class="btn btn-primary btn-sm"
                                                                                   role="button">
                                                                                    อ่านทั้งหมด
                                                                                </a>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </a>

                                </li>
                                <li><a href="/news?type=thai&Page=1">ข่าวบอลไทย</a></li>
                                <li><a href="/news?type=other&Page=1">ข่าวกีฬาอื่นๆ</a></li>
                                <li class="active-dropdown">
                                    <!--                                    href="/gallery"-->
                                    <a>Sexy Football
                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                        <div class="bx-menu-dropdown-arrow">
                                            <i class="fa fa-caret-up" aria-hidden="true"></i>
                                        </div>
                                        <div class="bx-menu-dropdown">
                                            <div class="container">
                                                <div class="col-md-2 list-content-big-menu">
                                                    <h3>Sexy Football</h3>
                                                    <ul>
                                                        <li>
                                                            <a class="menu-sexy"
                                                               data-category="picture">Sexy Picture</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li class="active">
                                                            <a class="menu-sexy" data-category="video">Sexy
                                                                Video</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!--                                        Picpost-->
                                                <div class="div-sexy col-md-10 detail-content-big-menu hide"
                                                     id="sexy-picture">
                                                    <div class="col-sm-12">
                                                        <h3 class="pull-left">
                                                            <i class="fa fa-picture-o"></i> Sexy Picture
                                                        </h3>
                                                        <a class="pull-right" href="/gallery">ทั้งหมด</a>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <?php foreach ($dataSexyPicture as $key => $value) { ?>
                                                                <div class="col-sm-4">
                                                                    <div class="thumbnail">
                                                                        <div class="wrap-box-gallery">
                                                                            <div class="bx-type-gallery">
                                                                                <a href="/sexy-football-picture?id=<?= $value->id ?>">
                                                                                    <i class="fa fa-picture-o"></i>
                                                                                </a>
                                                                            </div>
                                                                            <div class="bx-gallery">
                                                                                <a href="/sexy-football-picture?id=<?= $value->id ?>">
                                                                                    <div class="crop"
                                                                                         style="background-image: url(<?= $value->thumbnail ?>);"></div>
                                                                                </a>
                                                                            </div>
                                                                            <div class="bx-detail-gallery">
                                                                                <a href="/sexy-football-picture?id=<?= $value->id ?>"><?= $value->title ?></a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="caption">
                                                                            <p>
                                                                                <a href="/sexy-football-picture?id=<?= $value->id ?>"
                                                                                   class="btn btn-primary btn-sm"
                                                                                   role="button">
                                                                                    อ่านทั้งหมด
                                                                                </a>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        video-->
                                                <div class="div-sexy col-md-10 detail-content-big-menu" id="sexy-video">
                                                    <div class="col-sm-12">
                                                        <h3 class="pull-left">
                                                            <i class="fa fa-youtube-play" aria-hidden="true"></i> Sexy
                                                            Video
                                                        </h3>
                                                        <a class="pull-right" href="/hothit-videos-sexy">ทั้งหมด</a>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <?php foreach ($dataSexyVideo as $key => $value) { ?>
                                                                <div class="col-sm-4">
                                                                    <div class="thumbnail">
                                                                        <div class="box-videos"
                                                                             style="height: 168px;">
                                                                            <a href="/sexy-football-videos?galleryId=<?= $value->id ?>">
                                                                                <div class="crop"
                                                                                     style="background-image: url(<?= $value->urlImg ?>);"></div>
                                                                                <div class="icon-play">
                                                                                    <i class="fa fa-play"></i>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                        <div class="caption">
                                                                            <a href="/sexy-football-videos?galleryId=<?= $value->id ?>">
                                                                                <h3><?= $value->title ?></h3></a>
                                                                            <p>
                                                                                <a href="/hothit-videos-sexy?galleryId=<?= $value->id ?>"
                                                                                   class="btn btn-primary btn-sm"
                                                                                   role="button">
                                                                                    อ่านทั้งหมด
                                                                                </a>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>

                                </li>
                                <li class="active-dropdown">
                                    <a href="/games?mid=<?php echo((!empty($match[0])) ? $match[0]->id : '') ?>">
                                        <div class="hot-icon">
                                            <i class="fa fa-circle" aria-hidden="true"></i>
                                        </div>
                                        เกมทายผลบอล
                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                        <div class="bx-menu-dropdown-arrow">
                                            <i class="fa fa-caret-up" aria-hidden="true"></i>
                                        </div>
                                        <div class="bx-menu-dropdown">

                                            <div class="container">
                                                <div class="col-md-2 list-content-big-menu">
                                                    <h3>เกมทายผลบอล</h3>
                                                    <ul>
                                                        <li class="active">
                                                            <!--                                                    href="/"-->
                                                            <a>เกมทายผลบอล</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li>
                                                            <!--                                                    href="/rules"-->
                                                            <a href="/rules">กติกาและเงื่อนไข</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li>
                                                            <!--                                                    href="/ranking"-->
                                                            <a href="/ranking">Ranking</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>
                                                        <li>
                                                            <a href="/help">วิธีการเล่นเกมทายผล</a>
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        </li>

                                                    </ul>
                                                </div>

                                                <div class="col-md-6 detail-content-big-menu detail-march-content">
                                                    <div class="col-sm-12">
                                                        <h3 class="pull-left">
                                                            <i class="fa fa-gamepad" aria-hidden="true"></i> March
                                                            Football
                                                        </h3>
                                                        <a class="pull-right"
                                                           href="/games">ทั้งหมด</a>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <?php if (!empty($match[0])) { ?>
                                                                <div class="col-sm-6"
                                                                     style="border-right: solid 1px rgba(255, 255, 255, 0.1);">
                                                                    <h5><?= $match[0]->name ?></h5>
                                                                    <ul class="topic">
                                                                        <li>
                                                                            <i class="fa fa-clock-o"
                                                                               aria-hidden="true"></i>
                                                                            <?= date("H:i", (strtotime($match[0]->time_match) - (60 * 60))) ?>
                                                                        </li>
                                                                        <li>|</li>
                                                                        <li><i class="fa fa-calendar"
                                                                               aria-hidden="true"></i>
                                                                            <?= date("d/m/Y", (strtotime($match[0]->time_match) - (60 * 60))) ?>
                                                                        </li>
                                                                    </ul>

                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <img src="<?= $match[0]->teamHomePath ?>">
                                                                                <h4 class="<?= ((!empty($match[0]->handicap) && $match[0]->handicap < 0) ? 'active' : '') ?>"><?= $match[0]->teamHomeEn ?></h4>
                                                                            </td>
                                                                            <td class="box-vs">VS</td>
                                                                            <td>
                                                                                <img src="<?= $match[0]->teamAwayPath ?>">
                                                                                <h4 class="<?= ((!empty($match[0]->handicap) && $match[0]->handicap > 0) ? 'active' : '') ?>"><?= $match[0]->teamAwayEn ?></h4>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <p class="text-center">
                                                                        <a href="/games?mid=<?= $match[0]->id ?>"
                                                                           class="btn btn-primary btn-sm">ทายผลบอล</a>
                                                                    </p>
                                                                </div>
                                                            <?php } ?>
                                                            <?php if (!empty($match[1])) { ?>
                                                                <div class="col-sm-6">
                                                                    <h5><?= $match[1]->name ?></h5>
                                                                    <ul class="topic">
                                                                        <li>
                                                                            <i class="fa fa-clock-o"
                                                                               aria-hidden="true"></i>
                                                                            <?= date("H:i", (strtotime($match[1]->time_match) - (60 * 60))) ?>
                                                                        </li>
                                                                        <li>|</li>
                                                                        <li><i class="fa fa-calendar"
                                                                               aria-hidden="true"></i>
                                                                            <?= date("d/m/Y", (strtotime($match[1]->time_match) - (60 * 60))) ?>
                                                                        </li>
                                                                    </ul>

                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <img src="<?= $match[1]->teamHomePath ?>">
                                                                                <h4 class="<?= ((!empty($match[1]->handicap) && $match[1]->handicap < 0) ? 'active' : '') ?>"><?= $match[1]->teamHomeEn ?></h4>
                                                                            </td>
                                                                            <td class="box-vs">VS</td>
                                                                            <td>
                                                                                <img src="<?= $match[1]->teamAwayPath ?>">
                                                                                <h4 class="<?= ((!empty($match[1]->handicap) && $match[1]->handicap > 0) ? 'active' : '') ?>"><?= $match[1]->teamAwayEn ?></h4>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <p class="text-center">
                                                                        <a href="/games?mid=<?= $match[1]->id ?>"
                                                                           class="btn btn-primary btn-sm">ทายผลบอล</a>
                                                                    </p>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 detail-content-big-menu">
                                                    <div class="col-sm-12">
                                                        <h3 class="pull-left">
                                                            <i class="fa fa-trophy" aria-hidden="true"></i> Ranking
                                                        </h3>
                                                        <a class="pull-right"
                                                           href="/ranking">ทั้งหมด</a>
                                                    </div>
                                                    <div class="col-sm-12 menu-ranking-box">
                                                        <!-- Nav tabs -->
                                                        <ul class="nav nav-tabs" role="tablist">
                                                            <li role="presentation" class="active"><a
                                                                        href="#all-time-ranking"
                                                                        aria-controls="home"
                                                                        role="tab"
                                                                        data-toggle="tab">All
                                                                    Time</a></li>
                                                            <li role="presentation"><a href="#now-ranking"
                                                                                       aria-controls="profile"
                                                                                       role="tab"
                                                                                       data-toggle="tab">Now</a></li>
                                                            <li role="presentation"><a href="#Last-champ-ranking"
                                                                                       aria-controls="messages"
                                                                                       role="tab"
                                                                                       data-toggle="tab">Last Champ</a>
                                                            </li>
                                                        </ul>

                                                        <!-- Tab panes -->
                                                        <div class="tab-content">
                                                            <div role="tabpanel" class="tab-pane active"
                                                                 id="all-time-ranking">
                                                                <table>
                                                                    <?php foreach ($topRankingAll as $key => $value) {
                                                                        if ($key < 5) {
                                                                            ?>
                                                                            <tr>
                                                                                <td><?= $key + 1 ?></td>
                                                                                <td>
                                                                                    <img src="https://graph.facebook.com/v2.8/<?php echo $value["fb_uid"]; ?>/picture?type=small">
                                                                                </td>
                                                                                <td><?php echo $value["name"]; ?></td>
                                                                                <td><?php echo number_format($value["accuracy"], 2); ?></td>
                                                                            </tr>
                                                                        <?php }
                                                                    } ?>
                                                                </table>
                                                            </div>
                                                            <div role="tabpanel" class="tab-pane" id="now-ranking">
                                                                <table>
                                                                    <?php foreach ($topRankingNow as $key => $value) {
                                                                        if ($key < 5) {
                                                                            ?>
                                                                            <tr>
                                                                                <td><?= $key + 1 ?></td>
                                                                                <td>
                                                                                    <img src="https://graph.facebook.com/v2.8/<?php echo $value["fb_uid"]; ?>/picture?type=small">
                                                                                </td>
                                                                                <td><?php echo $value["name"]; ?></td>
                                                                                <td><?php echo number_format($value["accuracy"], 2); ?></td>
                                                                            </tr>
                                                                        <?php }
                                                                    } ?>
                                                                </table>
                                                            </div>
                                                            <div role="tabpanel" class="tab-pane"
                                                                 id="Last-champ-ranking">
                                                                <table>
                                                                    <?php foreach ($topLastChamp as $key => $value) {
                                                                        if ($key < 5) {
                                                                            ?>
                                                                            <tr>
                                                                                <td><?= $key + 1 ?></td>
                                                                                <td>
                                                                                    <img src="https://graph.facebook.com/v2.8/<?php echo $value["fb_uid"]; ?>/picture?type=small">
                                                                                </td>
                                                                                <td><?php echo $value["name"]; ?></td>
                                                                                <td><?php echo number_format($value["accuracy"], 2); ?></td>
                                                                            </tr>
                                                                        <?php }
                                                                    } ?>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="bt-banner-game"></div>
                </div>
            <?php } ?>

        </div>
        <div style="clear: both"></div>
        <div style="background-color: #003a6c; width: 100%;padding: 0px 0px;">
            <div id="loadingBar"
                 style="background: rgb(0, 188, 212); width: 0%;height: 2px; transition: transform .3s ease;"></div>
        </div>
    </nav>

</div>
<div id="wrapper">


    <div class="overlay"></div>

    <!-- Sidebar -->
    <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
        <div class="text-center"></div>
        <ul class="nav sidebar-nav">
            <li><a href="/"><img style="height: 25px;" src="/images/logo/logo-ngoal-white.png"></a></li>
            <li><a href="/highlight">วิดีโอ/ไฮไลท์บอล</a></li>
            <li class="collapse-side-menu">
                <div class="panel">
                    <div class="panel-heading" role="tab" id="headingMenu">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseMenu"
                               aria-expanded="true" aria-controls="collapseMenu">
                                ข่าวบอลต่างประเทศ
                            </a>
                        </h4>
                    </div>
                    <div id="collapseMenu" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingMenu">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <a href="/news?type=premier%20league">ข่าวพรีเมียร์ลีก</a>
                            </li>
                            <li class="list-group-item">
                                <a href="/news?type=la%20liga">ข่าวลาลีกา</a>
                            </li>
                            <li class="list-group-item">
                                <a href="/news?type=bundesliga">ข่าวบุนเดสลีกา</a>
                            </li>
                            <li class="list-group-item">
                                <a href="/news?type=serie%20a">ข่าวซีเรียอา</a>
                            </li>
                            <li class="list-group-item">
                                <a href="/news?type=ucl">ข่าวยูฟ่าแชมเปียนลีก</a>
                            </li>
                            <li class="list-group-item">
                                <a href="/news?type=europa">ข่าวยูโรป้า</a>
                            </li>
                            <li class="list-group-item">
                                <a href="/news?type=thai">ข่าวไทยพรีเมียลีก</a>
                            </li>
                            <li class="list-group-item">
                                <a href="/news?type=other">ข่าวฟุตบอลลีกอื่น</a>
                            </li>

                        </ul>
                    </div>
                </div>
            </li>
            <li><a href="/news?type=thai&Page=1">ข่าวบอลไทย</a></li>
            <!--            <li><a href="/news?type=other&Page=1">ข่าวกีฬาอื่นๆ</a></li>-->
            <li><a href="/hothit-videos">Clip HotHit</a></li>
            <li><a href="/sexy-football">Sexy Football</a></li>
            <li><a href="/games">เกมทายผล</a></li>
            <li><a href="/rules">กติกาและเงื่อนไข</a></li>
            <li><a href="/ranking">Ranking</a></li>
            <li><a href="/help">วิธีการเล่นเกมทายผล</a></li>
            <li><a href="/review">ทีเด็ด ทรรศนะ</a></li>
            <li class="text-event"><a href="https://ngoal.com/events"><img src="/images/source.gif"> <span
                            class="blink"> แจกรางวัล ฟรี!</span></a></li>
        </ul>
    </nav>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div class="box-content-wrapper" data-spy="affix" data-offset-top="60" id="page-content-wrapper">
        <a href="/"><img src="/images/logo/logo-ngoal-white.png"></a>
        <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
            <span class="hamb-top"></span>
            <span class="hamb-middle"></span>
            <span class="hamb-bottom"></span>
        </button>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<?php
$dataDailyIncome = false;
$dataNewUserIncome = false;
?>
<?php if (isset($_SESSION['login'])) {
    if (empty($playLogIncome)) {
        require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playLogIncome.php";
        $playLogIncome = new playLogIncome();
    }
    $dataDailyIncome = $playLogIncome->checkDailyIncome($_SESSION['login']['id']);
    $dataNewUserIncome = $playLogIncome->checkNewUserIncome($_SESSION['login']['id']);
    ?>
    <div id="model-login-daily" class="modal fade" style="" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 70%;">
            <div class="modal-content" style="position: relative; background: none;">
                <img style="width: 100%;"
                     src="/images/login-daily/login-daily-<?php echo($_SESSION['login']['combo_login'] % 7); ?>-1.png">
                <div style="position: absolute;bottom:0;right:0;z-index: 100;width: 20%;padding: 2% 5%;font-size: 18px;font-weight: bolder;color: #fff;background-color: #0e505e;">
                    <a href="/play/dailyCoin?pathname=<?php echo $_SERVER['REQUEST_URI']; ?>"
                       style="color: #fff;text-decoration: none;">รับ Coin</a>
                </div>
            </div>
        </div>
    </div>

    <div id="model-new-user" class="modal fade" style="" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 50%;">
            <div class="modal-content" style="position: relative; background: none;">
                <img style="width: 100%;" src="/images/login-daily/new-user-1.png">
                <div style="position: absolute;bottom:0;right:0;z-index: 100;width: 20%;height: 20%;padding: 7% 5%;font-size: 18px;font-weight: bolder;color: #fff;">
                    <a href="/play/newUserCoin?pathname=<?php echo $_SERVER['REQUEST_URI']; ?>"
                       style="color: #fff;text-decoration: none;">รับ Coin</a>
                </div>
            </div>
        </div>
    </div>

<?php } ?>
<?php if ($dataDailyIncome) { ?>
    <script>
        $(document).ready(function () {
            $('div#model-login-daily').modal({
                backdrop: 'static',
                keyboard: false
            });
        })
    </script>
<?php } ?>
<?php if ($dataNewUserIncome) { ?>
    <script>
        $(document).ready(function () {
            $('div#model-new-user').modal({
                backdrop: 'static',
                keyboard: false
            });
        })
    </script>
<?php } ?>

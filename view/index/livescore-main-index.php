<?php
require_once $_SERVER["DOCUMENT_ROOT"] ."/model/playBet.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playReviewMatch.php";
$playBet=new playBet();
$playMatch = new playMatch();
$playReviewMatch=new playReviewMatch();
$match=$playMatch->getFirstMathStatusBet(30);
$reviewMatch=$playReviewMatch->getReviewByMatch();
$webroot=$_SERVER['DOCUMENT_ROOT'];
foreach ($match as $key=>$value){
    $matchResultsHome[$value->id] = $playMatch->getListMatchResults($value->team_home, 5);
    $matchResultsAway[$value->id] = $playMatch->getListMatchResults($value->team_away, 5);
//    $userListHome[$value->id] = $playMatch->getUserBet($value->id,'home',5);
//    $userListAway[$value->id] = $playMatch->getUserBet($value->id,'away',5);
//    $percentBet[$value->id] = $playBet->percentBet($value->id);
//    $reviewMatch[$value->id] = $playReviewMatch->getReviewMatchByMatchId($value->id);
//
//    $reviewMatchList = $playReviewMatch->getReviewMatchByMatchId($value->id);
//    foreach ($reviewMatchList as $key => $value) {
//        $winRate = $playReviewMatch->getWinRate($value['user_review_id'], 30);
//        if ($winRate["total"] != 0) {
//            $resultsReviewMatch[$value['user_review_id']] = intval(100 - ((($winRate["total"] - $winRate["win"]) / $winRate["total"]) * 100));
//        } else {
//            $resultsReviewMatch[$value['user_review_id']] = 0;
//        }
//    }
}

//review match
$reviewCharTextLength = 250;
?>
<style>
    h3 > a.text-blue {
        text-decoration: underline !important;
    }
</style>
<div class="row">
    <div class="content-livescore-main content-game-redesign"
         style="margin-top: -20px; padding-bottom: 0;">
        <div class="container" id="sly-container">
            <div class="row">
                <div class="content-list-livescore">
                    <div class="col-md-12" id="liveScoreHead" style="width: 100%;overflow: hidden;height: 104px;">
                        <ul class="" id="ul-liveScoreHead" style="margin: 0">
                            <?php if (!empty($match)) {
                                foreach ($match as $key => $value) { ?>
                                    <li data-timeMatch="<?= $value->time_match ?>" data-matchId="<?= $value->id ?>"
                                        class="match-slide" style="float: left;width: 280px;">
                                        <div class="col-md-12" style="">
                                            <div class="thumbnail content-team-slide">
                                                <div class="pull-left">
                                                    <div class="content-team-slide-title">
                                                        <img src="<?= $value->teamHomePath ?>">
                                                        <div class="<?= ((!empty($value->handicap) && $value->handicap < 0) ? 'active' : '') ?>">
                                                            <div class="name-team-slide-top"><?= $value->teamHomeEn ?></div>
                                                        </div>
                                                    </div>

                                                    <div class="content-team-slide-title">
                                                        <img src="<?= $value->teamAwayPath ?>">
                                                        <div class="<?= ((!empty($value->handicap) && $value->handicap > 0) ? 'active' : '') ?>">
                                                            <div class="name-team-slide-top"><?= $value->teamAwayEn ?></div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="pull-right">
                                                    <div class="time-live">
                                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        <p><?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?></p>
                                                    </div>
                                                    <div class="HDP-live">HDP</div>
                                                    <div class="num-live"><?= $value->handicap ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php }
                            } ?>
                        </ul>
                    </div>
                </div>
            </div>

            <?php if (!empty($match)) { ?>
                <?php foreach ($match as $key => $value) { ?>
                    <div class="match-team <?php echo(($key == 0) ? '' : 'hide') ?>"
                         id="match-team-<?php echo $value->id ?>">
                        <div class="col-md-12">
                            <!--                title-->
                            <div class="topic-league">
                                <h3><img src="<?php echo $value->path ?>">
                                    <a href="/games?mid=<?php echo $value->id ?>"><?php echo $value->name ?></a>
                                </h3>
                                <ul>
                                    <li><i class="fa fa-clock-o"
                                           aria-hidden="true"></i> <?php echo date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                    </li>
                                    <li>|</li>
                                    <li><i class="fa fa-calendar"
                                           aria-hidden="true"></i> <?php echo date("d/m/Y", (strtotime($value->time_match) - (60 * 60))) ?>
                                    </li>
                                </ul>
                            </div>
                            <!--                Vote-->
                            <div class="content-select-team-vote">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-xs-6 col-sm-6">
                                            <div class="content-team-vote-home">
                                                <div class="box-vote-team">
                                                    <div class="logo-vote-team-home">
                                                        <a path="<?php echo $webroot."/images/team/".$value->team_home."/".$value->team_home."_256x256.png";?>" href="/games?mid=<?php echo $value->id ?>"><img
                                                                    src="<?php echo (file_exists($webroot."/images/team/".$value->team_home."/".$value->team_home."_256x256.png")?"/images/team/".$value->team_home."/".$value->team_home."_256x256.png":$value->teamHomePath); ?>"></a>
                                                    </div>
                                                    <div class="btn-vote-team-home text-right">
                                                        <?php if ($betMatchById[$value->id]) { ?>
                                                            <?php if ($betMatchById[$value->id] == "home") { ?>
                                                                <a>VOTE</a>
                                                                <div class="bg-vote-team-home"></div>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <a class="a-vote-team-home">VOTE</a>
                                                            <div class="bg-vote-team-home"></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="name-team-vote">
                                                    <h3 class="">
                                                        <a style="color: #333;"
                                                           class="<?php echo(($value->handicap < 0) ? 'text-blue' : '') ?>"
                                                           href="/games?mid=<?php echo $value->id ?>"><?php echo $value->teamHomeEn ?></a>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6">
                                            <div class="content-team-vote-away">
                                                <div class="box-vote-team">
                                                    <div class="logo-vote-team-away">
                                                        <a href="/games?mid=<?= $value->id ?>"><img
                                                                    src="<?php echo (file_exists($webroot."/images/team/".$value->team_away."/".$value->team_away."_256x256.png")?"/images/team/".$value->team_away."/".$value->team_away."_256x256.png":$value->teamAwayPath); ?>"></a>
                                                    </div>
                                                    <div class="btn-vote-team-away">
                                                        <?php if ($betMatchById[$value->id]) { ?>
                                                            <?php if ($betMatchById[$value->id] == "away") { ?>
                                                                <a>VOTE</a>
                                                                <div class="bg-vote-team-away"></div>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <a class="a-vote-team-away">VOTE</a>
                                                            <div class="bg-vote-team-away"></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="name-team-vote">
                                                    <h3 class="">
                                                        <a style="color: #333;"
                                                           class="<?php echo(($value->handicap > 0) ? 'text-blue' : '') ?>"
                                                           href="/games?mid=<?= $value->id ?>"><?= $value->teamAwayEn ?></a>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--                HDP-->
                            <div class="row text-center" id="content-hdp-<?php echo $value->id; ?>">
                                <ul class="content-HDP-game">
                                    <li class="box-last-match text-right">
                                        <table>
                                            <tbody>
                                            <tr class="tr-btn-results-left">
                                                <td class="btn-results-left"><i class="fa fa-caret-left" aria-hidden="true"></i></td>
                                                <?php if (!empty($matchResultsHome[$value->id])) { ?>
                                                    <?php foreach (array_reverse($matchResultsHome[$value->id]) as $keyResults => $valueResults) { ?>
                                                        <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                            <td class="li-results">
                                                                <img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/draw.png">
                                                            </td>
                                                        <?php } elseif ($valueResults->team_home == $value->team_home && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                            <td class="li-results">
                                                                <img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/win.png">
                                                            </td>
                                                        <?php } elseif ($valueResults->team_away == $value->team_home && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                            <td class="li-results">
                                                                <img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/win.png">
                                                            </td>
                                                        <?php } else { ?>
                                                            <td class="li-results">
                                                                <img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/lose.png">
                                                            </td>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                                <td>Last <span class="total-results">5</span> Match</td>
                                            </tr>
                                            <tr class="text-right">
                                                <td></td>
                                                <?php if (!empty($userListHome[$value->id])) { ?>
                                                    <td>
                                                        <img id="user-match-home-<?php echo $value->id?>-4" src="<?php echo((!empty($userListHome[$value->id][4])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[$value->id][4]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <img id="user-match-home-<?php echo $value->id?>-3" src="<?php echo((!empty($userListHome[$value->id][3])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[$value->id][3]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <img id="user-match-home-<?php echo $value->id?>-2" src="<?php echo((!empty($userListHome[$value->id][2])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[$value->id][2]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <img id="user-match-home-<?php echo $value->id?>-1" src="<?php echo((!empty($userListHome[$value->id][1])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[$value->id][1]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <img id="user-match-home-<?php echo $value->id?>-0" src="<?php echo((!empty($userListHome[$value->id][0])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[$value->id][0]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                <?php } else { ?>
                                                    <td><img id="user-match-home-<?php echo $value->id?>-4" src="/images/avatar.png"></td>
                                                    <td><img id="user-match-home-<?php echo $value->id?>-3" src="/images/avatar.png"></td>
                                                    <td><img id="user-match-home-<?php echo $value->id?>-2" src="/images/avatar.png"></td>
                                                    <td><img id="user-match-home-<?php echo $value->id?>-1" src="/images/avatar.png"></td>
                                                    <td><img id="user-match-home-<?php echo $value->id?>-0" src="/images/avatar.png"></td>
                                                <?php } ?>
                                                <td>Player</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="text-left"><span id="progress-match-home-<?php echo $value->id?>"><?= number_format($percentBet[$value->id]['pHome'], 2) ?>%</span></td>
                                                <td>
                                                    <div class="progress pull-right">
                                                        <div id="progress-match-home-<?php echo $value->id?>" class="progress-bar pull-right" role="progressbar"
                                                             aria-valuenow="<?= number_format($percentBet[$value->id]['pHome'], 2) ?>"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width: <?= number_format($percentBet[$value->id]['pHome'], 2) ?>%;">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                    <li class="box-HDP-game text-center">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="text-right">
                                                    <p><?= ((!empty($value->home_water_bill)) ? number_format($value->home_water_bill,2) : '-'); ?></p>
                                                </td>
                                                <td>
                                                    <a href="/games?mid=<?php echo $value->id; ?>">
                                                        <h2>[<?= ((!empty($value->handicap)) ? $value->handicap : '-'); ?>]</h2>
                                                    </a>
                                                </td>
                                                <td class="text-left">
                                                    <p><?= ((!empty($value->away_water_bill)) ? number_format($value->away_water_bill,2) : '-'); ?></p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                    <li class="box-last-match text-left">
                                        <table>
                                            <tbody>
                                            <tr class="tr-btn-results-right">
                                                <td>Last <span class="total-results">5</span> Match</td>
                                                <?php if (!empty($matchResultsAway[$value->id])) { ?>
                                                    <?php foreach ($matchResultsAway[$value->id] as $keyResults => $valueResults) { ?>
                                                        <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                            <td class="li-results">
                                                                <img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/draw.png">
                                                            </td>
                                                        <?php } elseif ($valueResults->team_home == $value->team_away && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                            <td class="li-results">
                                                                <img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/win.png">
                                                            </td>
                                                        <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                            <td class="li-results">
                                                                <img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/win.png">
                                                            </td>
                                                        <?php } else { ?>
                                                            <td class="li-results">
                                                                <img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/lose.png"
                                                            </td>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                                <td class="btn-results-right"><i class="fa fa-caret-right" aria-hidden="true"></i></td>
                                            </tr>
                                            <tr>
                                                <td>Player</td>
                                                <?php if (!empty($userListAway[$value->id])) { ?>
                                                    <td>
                                                        <img id="user-match-away-<?php echo $value->id?>-4" src="<?php echo((!empty($userListAway[$value->id][0])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[$value->id][0]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <img id="user-match-away-<?php echo $value->id?>-3" src="<?php echo((!empty($userListAway[$value->id][1])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[$value->id][1]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <img id="user-match-away-<?php echo $value->id?>-2" src="<?php echo((!empty($userListAway[$value->id][2])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[$value->id][2]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <img id="user-match-away-<?php echo $value->id?>-1" src="<?php echo((!empty($userListAway[$value->id][3])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[$value->id][3]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <img id="user-match-away-<?php echo $value->id?>-0" src="<?php echo((!empty($userListAway[$value->id][4])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[$value->id][4]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                <?php } else { ?>
                                                    <td><img id="user-match-away-<?php echo $value->id?>-4" src="/images/avatar.png"></td>
                                                    <td><img id="user-match-away-<?php echo $value->id?>-3" src="/images/avatar.png"></td>
                                                    <td><img id="user-match-away-<?php echo $value->id?>-2" src="/images/avatar.png"></td>
                                                    <td><img id="user-match-away-<?php echo $value->id?>-1" src="/images/avatar.png"></td>
                                                    <td><img id="user-match-away-<?php echo $value->id?>-0" src="/images/avatar.png"></td>
                                                <?php } ?>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <div class="progress" style="">
                                                        <div id="progress-match-away-<?php echo $value->id?>" class="progress-bar pull-left" role="progressbar"
                                                             aria-valuenow="<?= number_format($percentBet[$value->id]['pAway'], 2) ?>"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width: <?= number_format($percentBet[$value->id]['pAway'], 2) ?>%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><span id="progress-match-away-<?php echo $value->id?>"><?= number_format($percentBet[$value->id]['pAway'], 2) ?>%</span></td>
                                            </tr>
                                            </tbody>
                                        </table>


                                    </li>
                                </ul>
                                <ul class="content-HDP-game content-HDP-game-responsive">
                                    <li class="box-HDP-game text-center">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="text-right">
                                                    <p><?= ((!empty($value->home_water_bill)) ? $value->home_water_bill : '-'); ?></p>
                                                </td>
                                                <td><h2>[<?= ((!empty($value->handicap)) ? $value->handicap : '-'); ?>
                                                        ]</h2></td>
                                                <td class="text-left">
                                                    <p><?= ((!empty($value->away_water_bill)) ? $value->away_water_bill : '-'); ?></p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                    <li class="box-last-match text-center" style="border-right: solid 1px #092740;">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td colspan="6">Last 5 Match</td>
                                            </tr>
                                            <tr class="tr-btn-results-left">
                                                <td class="btn-results-left" number="3"><i class="fa fa-caret-left"
                                                                                           aria-hidden="true"></i></td>
                                                <?php if (!empty($matchResultsHome[$value->id])) { ?>
                                                    <?php foreach (array_reverse($matchResultsHome[$value->id]) as $keyResults => $valueResults) { ?>
                                                        <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                            <td scoreAway="<?= $valueResults->scoreAway ?>"
                                                                scoreHome="<?= $valueResults->scoreHome ?>"
                                                                number="<?= floor($keyResults / 5); ?>"
                                                                class="<?= ((floor($keyResults / 5) == 3) ? '' : 'hide') ?> li-results">
                                                                <img title="<?= $valueResults->time_match ?>"
                                                                     src="/images/icon-stat/betlist_result/draw.png">
                                                            </td>
                                                        <?php } elseif ($valueResults->team_home == $value->team_home && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                            <td team_homeR="<?php echo $valueResults->team_home ?>"
                                                                team_home="<?php echo $value->team_home; ?>"
                                                                scoreAway="<?= $valueResults->scoreAway ?>"
                                                                scoreHome="<?= $valueResults->scoreHome ?>"
                                                                number="<?= floor($keyResults / 5); ?>"
                                                                class="<?= ((floor($keyResults / 5) == 3) ? '' : 'hide') ?> li-results">
                                                                <img title="<?= $valueResults->time_match ?>"
                                                                     src="/images/icon-stat/betlist_result/win.png">
                                                            </td>
                                                        <?php } elseif ($valueResults->team_away == $value->team_home && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                            <td team_homeR="<?php echo $valueResults->team_home ?>"
                                                                team_home="<?php echo $value->team_home; ?>"
                                                                scoreAway="<?= $valueResults->scoreAway ?>"
                                                                scoreHome="<?= $valueResults->scoreHome ?>"
                                                                number="<?= floor($keyResults / 5); ?>"
                                                                class="<?= ((floor($keyResults / 5) == 3) ? '' : 'hide') ?> li-results">
                                                                <img title="<?= $valueResults->time_match ?>"
                                                                     src="/images/icon-stat/betlist_result/win.png">
                                                            </td>
                                                        <?php } else { ?>
                                                            <td team_homeR="<?php echo $valueResults->team_home ?>"
                                                                team_home="<?php echo $value->team_home; ?>"
                                                                scoreAway="<?= $valueResults->scoreAway ?>"
                                                                scoreHome="<?= $valueResults->scoreHome ?>"
                                                                number="<?= floor($keyResults / 5); ?>"
                                                                class="<?= ((floor($keyResults / 5) == 3) ? '' : 'hide') ?> li-results">
                                                                <img title="<?= $valueResults->time_match ?>"
                                                                     src="/images/icon-stat/betlist_result/lose.png">
                                                            </td>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>

                                            </tr>

                                            <tr>
                                                <td colspan="6">Player</td>
                                            </tr>
                                            <tr class="text-center">
                                                <td></td>
                                                <?php if (!empty($userListHome[$value->id])) { ?>
                                                    <td>
                                                        <img src="<?php echo((!empty($userListHome[$value->id][4])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[$value->id][4]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <img src="<?php echo((!empty($userListHome[$value->id][3])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[$value->id][3]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <img src="<?php echo((!empty($userListHome[$value->id][2])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[$value->id][2]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <img src="<?php echo((!empty($userListHome[$value->id][1])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[$value->id][1]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <img src="<?php echo((!empty($userListHome[$value->id][0])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[$value->id][0]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                <?php } else { ?>
                                                    <td><img src="/images/avatar.png"></td>
                                                    <td><img src="/images/avatar.png"></td>
                                                    <td><img src="/images/avatar.png"></td>
                                                    <td><img src="/images/avatar.png"></td>
                                                    <td><img src="/images/avatar.png"></td>
                                                <?php } ?>

                                            </tr>
                                            </tbody>
                                        </table>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="text-left">
                                                    <span><?= number_format($percentBet[$value->id]['pHome'], 2) ?>
                                                        %</span></td>
                                                <td>
                                                    <div class="progress pull-right">
                                                        <div class="progress-bar pull-right" role="progressbar"
                                                             aria-valuenow="<?= number_format($percentBet[$value->id]['pHome'], 2) ?>"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width: <?= number_format($percentBet[$value->id]['pHome'], 2) ?>%;">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                    <li class="box-last-match text-center">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td colspan="6">Last 5 Match</td>
                                            </tr>
                                            <tr class="tr-btn-results-right">
                                                <?php if (!empty($matchResultsAway[$value->id])) { ?>
                                                    <?php foreach ($matchResultsAway[$value->id] as $keyResults => $valueResults) { ?>
                                                        <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                            <td scoreAway="<?= $valueResults->scoreAway ?>"
                                                                scoreHome="<?= $valueResults->scoreHome ?>"
                                                                number="<?= floor($keyResults / 5); ?>"
                                                                class="<?= ((floor($keyResults / 5) == 0) ? '' : 'hide') ?> li-results">
                                                                <img title="<?= $valueResults->time_match ?>"
                                                                     src="/images/icon-stat/betlist_result/draw.png">
                                                            </td>
                                                        <?php } elseif ($valueResults->team_home == $value->team_away && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                            <td scoreAway="<?= $valueResults->scoreAway ?>"
                                                                scoreHome="<?= $valueResults->scoreHome ?>"
                                                                number="<?= floor($keyResults / 5); ?>"
                                                                class="<?= ((floor($keyResults / 5) == 0) ? '' : 'hide') ?> li-results">
                                                                <img title="<?= $valueResults->time_match ?>"
                                                                     src="/images/icon-stat/betlist_result/win.png">
                                                            </td>
                                                        <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                            <td scoreAway="<?= $valueResults->scoreAway ?>"
                                                                scoreHome="<?= $valueResults->scoreHome ?>"
                                                                number="<?= floor($keyResults / 5); ?>"
                                                                class="<?= ((floor($keyResults / 5) == 0) ? '' : 'hide') ?> li-results">
                                                                <img title="<?= $valueResults->time_match ?>"
                                                                     src="/images/icon-stat/betlist_result/win.png">
                                                            </td>
                                                        <?php } else { ?>
                                                            <td scoreAway="<?= $valueResults->scoreAway ?>"
                                                                scoreHome="<?= $valueResults->scoreHome ?>"
                                                                number="<?= floor($keyResults / 5); ?>"
                                                                class="<?= ((floor($keyResults / 5) == 0) ? '' : 'hide') ?> li-results">
                                                                <img title="<?= $valueResults->time_match ?>"
                                                                     src="/images/icon-stat/betlist_result/lose.png"
                                                            </td>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                                <td class="btn-results-right" number="0"><i class="fa fa-caret-right"
                                                                                            aria-hidden="true"></i></td>
                                            </tr>
                                            <tr>
                                                <td colspan="6">Player</td>
                                            </tr>
                                            <tr>
                                                <?php if (!empty($userListAway[$value->id])) { ?>
                                                    <td>
                                                        <img src="<?php echo((!empty($userListAway[$value->id][0])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[$value->id][0]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <img src="<?php echo((!empty($userListAway[$value->id][1])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[$value->id][1]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <img src="<?php echo((!empty($userListAway[$value->id][2])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[$value->id][2]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <img src="<?php echo((!empty($userListAway[$value->id][3])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[$value->id][3]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <img src="<?php echo((!empty($userListAway[$value->id][4])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[$value->id][4]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                                    </td>
                                                <?php } else { ?>
                                                    <td><img src="/images/avatar.png"></td>
                                                    <td><img src="/images/avatar.png"></td>
                                                    <td><img src="/images/avatar.png"></td>
                                                    <td><img src="/images/avatar.png"></td>
                                                    <td><img src="/images/avatar.png"></td>
                                                <?php } ?>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <div class="progress" style="">
                                                        <div class="progress-bar pull-left" role="progressbar"
                                                             aria-valuenow="<?= number_format($percentBet[$value->id]['pAway'], 2) ?>"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width: <?= number_format($percentBet[$value->id]['pAway'], 2) ?>%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><span><?= number_format($percentBet[$value->id]['pAway'], 2) ?>
                                                        %</span></td>
                                            </tr>
                                            </tbody>
                                        </table>


                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <div class="wrap-preview-game" style="border-bottom: transparent;" >
            <div class="container">
                <div class="title-label-rota" style="margin-top: 109px;">REVIEW</div>
                <?php if (!empty($reviewMatch)) {
                    foreach ($reviewMatch as $key => $value) {
                        $count = 0;
                        if (!empty($value)) { ?>
                            <div class="reviewMatch <?= (($count == 0) ? 'hide' : '') ?>"
                                 id="reviewMatch-<?= $key ?>">
                                <div id="carousel-example-generic-<?= $key ?>" class="carousel slide"
                                     data-ride="carousel">
                                    <div class="container">
                                        <div class="carousel-inner" role="listbox">
                                            <?php for ($i = 0; $i < ceil((count($value) / 6)); $i++) { ?>
                                                <div class="item <?= (($i == 0) ? 'active' : '') ?>">
                                                    <div class="col-sm-4 col-md-4">
                                                        <?php if (!empty($value[(($i * 6) + 0)])) { ?>
                                                            <a href="/review" class="thumbnail box-preview">
                                                                <div class="top-box-preview">
                                                                    <table>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="<?php echo ((!empty($value[(($i * 6) + 0)]['user_review_id']))?'box-guru-img':'box-user-img');?>  pull-left">
                                                                                    <img src="<?= ((!empty($value[(($i * 6) + 0)]['path']))?$value[(($i * 6) + 0)]['path']:'https://graph.facebook.com/v2.8/' . $value[(($i * 6) + 0)]['fb_uid'] . '/picture') ?>">
                                                                                </div>
                                                                                <div class="pull-left">
                                                                                    <h5><?= $value[(($i * 6) + 0)]['name'] ?></h5>
                                                                                    <div class="box-player-preview">
                                                                                        <?php if ($value[(($i * 6) + 0)]['team'] == "home") { ?>
                                                                                            <img src="<?= $value[(($i * 6) + 0)]['homepath'] ?>">
                                                                                            <span class="name-team-slide-top">@ <?= $value[(($i * 6) + 0)]['homename'] ?></span>
                                                                                        <?php } elseif ($value[(($i * 6) + 0)]['team'] == "away") { ?>
                                                                                            <img src="<?= $value[(($i * 6) + 0)]['awaypath'] ?>">
                                                                                            <span class="name-team-slide-top">@ <?= $value[(($i * 6) + 0)]['awayname'] ?></span>
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="pull-right">
                                                                                    <button type="button"
                                                                                            class="btn btn-link">
                                                                                        <i class="fa fa-angle-right"
                                                                                           aria-hidden="true"></i>
                                                                                    </button>
                                                                                </div>
                                                                                <div class="pull-right">
                                                                                    <h3 class="user-review-<?php echo $value[(($i * 6) + 0)]['user_review_id']?>">
                                                                                        <?php echo ((!empty($value[(($i * 6) + 0)]['accuracy']))?number_format($value[(($i * 6) + 0)]['accuracy']):'');?>
                                                                                        %</h3>
                                                                                    <p>Win rate</p>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="button-box-preview"
                                                                     style="height: 40px;overflow: hidden;">
                                                                    <?= $value[(($i * 6) + 0)]['review_text'] ?>
                                                                </div>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if (!empty($value[(($i * 6) + 1)])) { ?>
                                                            <a href="/review" class="thumbnail box-preview">
                                                                <div class="top-box-preview">
                                                                    <table>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="<?php echo ((!empty($value[(($i * 6) + 1)]['user_review_id']))?'box-guru-img':'box-user-img');?> pull-left">
                                                                                    <img src="<?= ((!empty($value[(($i * 6) + 1)]['path']))?$value[(($i * 6) + 1)]['path']:'https://graph.facebook.com/v2.8/' . $value[(($i * 6) + 1)]['fb_uid'] . '/picture') ?>">
                                                                                </div>
                                                                                <div class="pull-left">
                                                                                    <h5><?= $value[(($i * 6) + 1)]['name'] ?></h5>
                                                                                    <div class="box-player-preview">
                                                                                        <?php if ($value[(($i * 6) + 1)]['team'] == "home") { ?>
                                                                                            <img src="<?= $value[(($i * 6) + 1)]['homepath'] ?>">
                                                                                            <span class="name-team-slide-top">@ <?= $value[(($i * 6) + 1)]['homename'] ?></span>
                                                                                        <?php } elseif ($value[(($i * 6) + 1)]['team'] == "away") { ?>
                                                                                            <img src="<?= $value[(($i * 6) + 1)]['awaypath'] ?>">
                                                                                            <span class="name-team-slide-top">@ <?= $value[(($i * 6) + 1)]['awayname'] ?></span>
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="pull-right">
                                                                                    <button type="button"
                                                                                            class="btn btn-link">
                                                                                        <i class="fa fa-angle-right"
                                                                                           aria-hidden="true"></i>
                                                                                    </button>
                                                                                </div>
                                                                                <div class="pull-right">
                                                                                    <h3 class="user-review-<?php echo $value[(($i * 6) + 1)]['user_review_id']?>">
                                                                                        <?php echo ((!empty($value[(($i * 6) + 1)]['accuracy']))?number_format($value[(($i * 6) + 1)]['accuracy']):'');?>
                                                                                        %</h3>
                                                                                    <p>Win rate</p>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="button-box-preview"
                                                                     style="height: 40px;overflow: hidden;">
                                                                    <?= $value[(($i * 6) + 1)]['review_text'] ?>
                                                                </div>
                                                            </a>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-sm-4 col-md-4">
                                                        <?php if (!empty($value[(($i * 6) + 2)])) { ?>
                                                            <a href="/review" class="thumbnail box-preview">
                                                                <div class="top-box-preview">
                                                                    <table>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="<?php echo ((!empty($value[(($i * 6) + 2)]['user_review_id']))?'box-guru-img':'box-user-img');?> pull-left">
                                                                                    <img src="<?= ((!empty($value[(($i * 6) + 2)]['path']))?$value[(($i * 6) + 2)]['path']:'https://graph.facebook.com/v2.8/' . $value[(($i * 6) + 2)]['fb_uid'] . '/picture') ?>">
                                                                                </div>
                                                                                <div class="pull-left">
                                                                                    <h5><?= $value[(($i * 6) + 2)]['name'] ?></h5>
                                                                                    <div class="box-player-preview">
                                                                                        <?php if ($value[(($i * 6) + 2)]['team'] == "home") { ?>
                                                                                            <img src="<?= $value[(($i * 6) + 2)]['homepath'] ?>">
                                                                                            <span class="name-team-slide-top">@ <?= $value[(($i * 6) + 2)]['homename'] ?></span>
                                                                                        <?php } elseif ($value[(($i * 6) + 2)]['team'] == "away") { ?>
                                                                                            <img src="<?= $value[(($i * 6) + 2)]['awaypath'] ?>">
                                                                                            <span class="name-team-slide-top">@ <?= $value[(($i * 6) + 2)]['awayname'] ?></span>
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="pull-right">
                                                                                    <button type="button"
                                                                                            class="btn btn-link">
                                                                                        <i class="fa fa-angle-right"
                                                                                           aria-hidden="true"></i>
                                                                                    </button>
                                                                                </div>
                                                                                <div class="pull-right">
                                                                                    <h3 class="user-review-<?php echo $value[(($i * 6) + 2)]['user_review_id']?>">
                                                                                        <?php echo ((!empty($value[(($i * 6) + 2)]['accuracy']))?number($value[(($i * 6) + 2)]['accuracy']):'');?>
                                                                                        %</h3>
                                                                                    <p>Win rate</p>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="button-box-preview"
                                                                     style="height: 40px;overflow: hidden;">
                                                                    <?= $value[(($i * 6) + 2)]['review_text'] ?>
                                                                </div>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if (!empty($value[(($i * 6) + 3)])) { ?>
                                                            <a href="/review" class="thumbnail box-preview">
                                                                <div class="top-box-preview">
                                                                    <table>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="<?php echo ((!empty($value[(($i * 6) + 3)]['user_review_id']))?'box-guru-img':'box-user-img');?> pull-left">
                                                                                    <img src="<?= ((!empty($value[(($i * 6) + 3)]['path']))?$value[(($i * 6) + 3)]['path']:'https://graph.facebook.com/v2.8/' . $value[(($i * 6) + 3)]['fb_uid'] . '/picture') ?>">
                                                                                </div>
                                                                                <div class="pull-left">
                                                                                    <h5><?= $value[(($i * 6) + 3)]['name'] ?></h5>
                                                                                    <div class="box-player-preview">
                                                                                        <?php if ($value[(($i * 6) + 3)]['team'] == "home") { ?>
                                                                                            <img src="<?= $value[(($i * 6) + 3)]['homepath'] ?>">
                                                                                            <span class="name-team-slide-top">@ <?= $value[(($i * 6) + 3)]['homename'] ?></span>
                                                                                        <?php } elseif ($value[(($i * 6) + 3)]['team'] == "away") { ?>
                                                                                            <img src="<?= $value[(($i * 6) + 3)]['awaypath'] ?>">
                                                                                            <span class="name-team-slide-top">@ <?= $value[(($i * 6) + 3)]['awayname'] ?></span>
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="pull-right">
                                                                                    <button type="button"
                                                                                            class="btn btn-link">
                                                                                        <i class="fa fa-angle-right"
                                                                                           aria-hidden="true"></i>
                                                                                    </button>
                                                                                </div>
                                                                                <div class="pull-right">
                                                                                    <h3 class="user-review-<?php echo $value[(($i * 6) + 3)]['user_review_id']?>">
                                                                                        <?php echo ((!empty($value[(($i * 6) + 3)]['accuracy']))?number($value[(($i * 6) + 3)]['accuracy']):'');?>
                                                                                        %</h3>
                                                                                    <p>Win rate</p>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="button-box-preview"
                                                                     style="height: 40px;overflow: hidden;">
                                                                    <?= $value[(($i * 6) + 3)]['review_text'] ?>
                                                                </div>
                                                            </a>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-sm-4 col-md-4">
                                                        <?php if (!empty($value[(($i * 6) + 4)])) { ?>
                                                            <a href="/review" class="thumbnail box-preview">
                                                                <div class="top-box-preview">
                                                                    <table>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="<?php echo ((!empty($value[(($i * 6) + 4)]['user_review_id']))?'box-guru-img':'box-user-img');?> pull-left">
                                                                                    <img src="<?= ((!empty($value[(($i * 6) + 4)]['path']))?$value[(($i * 6) + 4)]['path']:'https://graph.facebook.com/v2.8/' . $value[(($i * 6) + 4)]['fb_uid'] . '/picture') ?>">
                                                                                </div>
                                                                                <div class="pull-left">
                                                                                    <h5><?= $value[(($i * 6) + 4)]['name'] ?></h5>
                                                                                    <div class="box-player-preview">
                                                                                        <?php if ($value[(($i * 6) + 4)]['team'] == "home") { ?>
                                                                                            <img src="<?= $value[(($i * 6) + 4)]['homepath'] ?>">
                                                                                            <span class="name-team-slide-top">@ <?= $value[(($i * 6) + 4)]['homename'] ?></span>
                                                                                        <?php } elseif ($value[(($i * 6) + 4)]['team'] == "away") { ?>
                                                                                            <img src="<?= $value[(($i * 6) + 4)]['awaypath'] ?>">
                                                                                            <span class="name-team-slide-top">@ <?= $value[(($i * 6) + 4)]['awayname'] ?></span>
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="pull-right">
                                                                                    <button type="button"
                                                                                            class="btn btn-link">
                                                                                        <i class="fa fa-angle-right"
                                                                                           aria-hidden="true"></i>
                                                                                    </button>
                                                                                </div>
                                                                                <div class="pull-right">
                                                                                    <h3 class="user-review-<?php echo $value[(($i * 6) + 4)]['user_review_id']?>">
                                                                                        <?php echo ((!empty($value[(($i * 6) + 4)]['accuracy']))?number_format($value[(($i * 6) + 4)]['accuracy']):'');?>
                                                                                        %</h3>
                                                                                    <p>Win rate</p>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="button-box-preview"
                                                                     style="height: 40px;overflow: hidden;">
                                                                    <?= $value[(($i * 6) + 4)]['review_text'] ?>
                                                                </div>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if (!empty($value[(($i * 6) + 5)])) { ?>
                                                            <a href="/review" class="thumbnail box-preview">
                                                                <div class="top-box-preview">
                                                                    <table>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="<?php echo ((!empty($value[(($i * 6) + 5)]['user_review_id']))?'box-guru-img':'box-user-img');?> pull-left">
                                                                                    <img src="<?= ((!empty($value[(($i * 6) + 5)]['path']))?$value[(($i * 6) + 5)]['path']:'https://graph.facebook.com/v2.8/' . $value[(($i * 6) + 5)]['fb_uid'] . '/picture') ?>">
                                                                                </div>
                                                                                <div class="pull-left">
                                                                                    <h5><?= $value[(($i * 6) + 5)]['name'] ?></h5>
                                                                                    <div class="box-player-preview">
                                                                                        <?php if ($value[(($i * 6) + 5)]['team'] == "home") { ?>
                                                                                            <img src="<?= $value[(($i * 6) + 5)]['homepath'] ?>">
                                                                                            <span class="name-team-slide-top">@ <?= $value[(($i * 6) + 5)]['homename'] ?></span>
                                                                                        <?php } elseif ($value[(($i * 6) + 5)]['team'] == "away") { ?>
                                                                                            <img src="<?= $value[(($i * 6) + 5)]['awaypath'] ?>">
                                                                                            <span class="name-team-slide-top">@ <?= $value[(($i * 6) + 5)]['awayname'] ?></span>
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="pull-right">
                                                                                    <button type="button"
                                                                                            class="btn btn-link">
                                                                                        <i class="fa fa-angle-right"
                                                                                           aria-hidden="true"></i>
                                                                                    </button>
                                                                                </div>
                                                                                <div class="pull-right">
                                                                                    <h3 class="user-review-<?php echo $value[(($i * 6) + 5)]['user_review_id']?>">
                                                                                        <?php echo ((!empty($value[(($i * 6) + 5)]['accuracy']))?number_format($value[(($i * 6) + 5)]['accuracy']):'-');?>
                                                                                        %</h3>
                                                                                    <p>Win rate</p>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="button-box-preview"
                                                                     style="height: 40px;overflow: hidden;">
                                                                    <?= $value[(($i * 6) + 5)]['review_text'] ?>
                                                                </div>
                                                            </a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <!-- Controls -->
                                    <!--                                    <a class="left carousel-control" href="#carousel-example-generic---><?//= $key ?><!--"-->
                                    <!--                                       role="button"-->
                                    <!--                                       data-slide="prev">-->
                                    <!--                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>-->
                                    <!--                                        <span class="sr-only">Previous</span>-->
                                    <!--                                    </a>-->
                                    <!--                                    <a class="right carousel-control" href="#carousel-example-generic---><?//= $key ?><!--"-->
                                    <!--                                       role="button"-->
                                    <!--                                       data-slide="next">-->
                                    <!--                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>-->
                                    <!--                                        <span class="sr-only">Next</span>-->
                                    <!--                                    </a>-->
                                </div>
                            </div>
                            <?php $count++;
                        }
                    }
                } ?>
            </div>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {

        // $('td.btn-results-left').on('click', function (e) {
        //     var number = parseInt($(this).attr('number'));
        //     if (number == 0) {
        //         number = 3;
        //     } else {
        //         number -= 1;
        //     }
        //     // console.log($(this).parent('tr').attr('class')+" :: "+number);
        //     $(this).parent('tr').find('.li-results').addClass('hide');
        //     $(this).parent('tr').find('[number=' + number + ']').removeClass('hide');
        //     $(this).parent('tr').find('span.total-results').html(20 - ((number * 5)));
        //     $(this).attr('number', number);
        // });
        //
        // $('td.btn-results-right').on('click', function (e) {
        //     var number = parseInt($(this).attr('number'));
        //     if (number == 3) {
        //         number = 0;
        //     } else {
        //         number += 1;
        //     }
        //     // console.log($(this).parent('tr').attr('class')+" :: "+number);
        //     $(this).parent('tr').find('.li-results').addClass('hide');
        //     $(this).parent('tr').find('[number=' + number + ']').removeClass('hide');
        //     $(this).parent('tr').find('span.total-results').html(((number * 5) + 5));
        //     $(this).attr('number', number);
        // });


        $("#liveScoreHead").find('li.match-slide').each(function (index) {
            $(this).css('width', ($('#liveScoreHead').width() / 4));
        });


        $(window).resize(function () {
            $("#liveScoreHead").find('li.match-slide').each(function (index) {
                $(this).css('width', ($('#liveScoreHead').width() / 4));
            });
        });
        var $frame = $('#sly-container');
        var $slidee = $frame.children('#liveScoreHead').eq(0);
        var $wrap = $frame.parent();
        var frame = new Sly('#sly-container', {
            slidee: $frame.find('#ul-liveScoreHead'),
            horizontal: 1,
            itemNav: 'basic',
            smart: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 0,
//            scrollBar: $wrap.find('.scrollbar'),
            scrollBy: 1,
            speed: 2000,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,

            // Cycling
            cycleBy: 'items',
            cycleInterval: 6000,
            pauseOnHover: 1,

            // Buttons
            forward: $wrap.find('.btnforward'),
            backward: $wrap.find('.btnbackward'),
            prev: $wrap.find('.btnbackward'),
            next: $wrap.find('.btnforward'),
            prevPage: $wrap.find('.backward'),
            nextPage: $wrap.find('.forward'),

//            draggedClass:  'dragged', // Class for dragged elements (like SLIDEE or scrollbar handle).
//            activeClass:   'active',  // Class for active items and pages.
//            disabledClass: 'disabled' // Class for disabled navigation elements.
        });

        frame.on('active', function (eventName) {
            $('div.match-team').addClass('hide');
            $('div#match-team-' + $('ul#ul-liveScoreHead>li.active').attr('data-matchId')).removeClass('hide');
            $('div.reviewMatch').addClass('hide');
            $('div#reviewMatch-' + $('ul#ul-liveScoreHead>li.active').attr('data-matchId')).removeClass('hide');


//            console.log("change:: " + $('ul#ul-liveScoreHead>li.active').attr('data-matchId'));
        });

//        $(document).on('mouseover','#fade-in-index,#fade-in-right-index',function () {
//            console.log('mouseover');
//            frame.pause();
//        });

//        $( ".content-livescore-main").mouseover(function() {
//            console.log('mouseover');
//                frame.pause();
//            }).mouseout(function() {
//            console.log('mouseout');
//                frame.resume();
//            });

        $(".content-livescore-main").hover(
            function () {
                frame.pause();
            }, function () {
                frame.resume();
            }
        );

// Initiate Sly instance
        frame.init();

//        var $frame = $('.wrap-box-livescore');
//        var $slidee = $frame.children('#liveScoreHead').eq(0);
//        var $wrap = $frame.parent();
//        $slidee.sly({
//            horizontal: 1,
//            itemNav: 'basic',
//            smart: 1,
//            activateOn: 'click',
//            mouseDragging: 1,
//            touchDragging: 1,
//            releaseSwing: 1,
//            startAt: 0,
////            scrollBar: $wrap.find('.scrollbar'),
//            scrollBy: 1,
//            speed: 2000,
//            elasticBounds: 1,
//            easing: 'easeOutExpo',
//            dragHandle: 1,
//            dynamicHandle: 1,
//            clickBar: 1,
//
//            // Cycling
//            cycleBy: 'items',
//            cycleInterval: 6000,
//            pauseOnHover: 1,
//
//            // Buttons
//            forward: $wrap.find('.btnforward'),
//            backward: $wrap.find('.btnbackward'),
//            prev: $wrap.find('.btnbackward'),
//            next: $wrap.find('.btnforward'),
//            prevPage: $wrap.find('.backward'),
//            nextPage: $wrap.find('.forward'),
//
//            draggedClass:  'dragged', // Class for dragged elements (like SLIDEE or scrollbar handle).
//            activeClass:   'active',  // Class for active items and pages.
//            disabledClass: 'disabled' // Class for disabled navigation elements.
//        });
    });
    //    $slidee
    // Register a callback that will be executed only once
    //    $slidee.on('load', function (eventName) {
    //        console.log("load");
    ////        console.log(eventName); // 'load'
    //    });
</script>
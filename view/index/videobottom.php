<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/24/2017
 * Time: 10:50
 */
?>
<div class="container" id="container-video-bottom">
    <div class="wrap-box-videos">
        <div id="owl-demo2"class="owl-carousel owl-theme">
            <?php foreach ($highlightvideo as $key => $value) { ?>
                <?php if ($key > 0) { ?>
                    <?php if($value->videosource=="twitter"){ ?>
                        <a id="box-twitter-video-bottom-main" href="/other-videos.php?videoId=<?php echo $value->video_id ?>">
                            <div class="lastest-video">
                                <div class="title-owl-carousel title-h-videos">
                                    <?php echo $value->title; ?>
                                </div>
                                <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                            class="how-long"><?php echo $value->create_datetime ?></span></div>
                                <div class="box-videos">
                                    <div class="hilight-cover"></div>
                                    <div class="icon-play"><i class="fa fa-play-circle-o"></i></div>
                                    <img src="<?php echo $value->urlImg; ?>">
                                </div>
                            </div>
                        </a>
                    <?php }else{ ?>
                        <a id="box-other-video-bottom-main" href="#large-videos-autoPlay"
                           onclick="setIndexVideosAutoPlay('<?php echo $value->urlIframe ?>','<?php echo $value->video_tag ?>','<?php echo $value->title ?>','','<?php echo $value->create_datetime ?>','<?php echo $value->video_id ?>')">
                            <div class="lastest-video">
                                <div class="title-owl-carousel title-h-videos"><?php echo $value->title; ?></div>
                                <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                            class="how-long"><?php echo $value->create_datetime ?></span></div>
                                <div class="box-videos">
                                    <div class="hilight-cover"></div>
                                    <div class="icon-play"><i class="fa fa-play-circle-o"></i></div>
                                    <img src="<?php echo $value->urlImg ?>">
                                </div>
                            </div>
                        </a>
                    <?php } ?>
                <?php }} ?>
        </div>
    </div>
    <div style="clear: both;"></div>
</div>

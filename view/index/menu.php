<?php
if (!isset($_SESSION)) {
    session_start();
}
?>
<input type="hidden" id="statusLogin" value="<?= ((isset($_SESSION['login']))?'true':'false')?>">
<nav class="navbar navbar-inverse navbar-top navbar-topbar">
    <div class="content-wrapper-menu">
        <!--   แทบบนสุด-->
        <div class="topmenu">
            <div class="container">
                <div class="pull-left contact-social">
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/nGoalTH/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="https://twitter.com/nGoalTH"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="pull-right box-login-FB">
                    <?php if (!isset($_SESSION['login'])) { ?>
                    <a onclick="checkLoginState()">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                        <span>Login</span>
                    </a>
                    <?php } ?>
                </div>
                <div class="pull-right content-profile-menu">
                    <ul>
                        <?php if (isset($_SESSION['login'])) { ?>
                            <li class="coin-topmenu"><a href=""><img src="/images/coin/sgold100.png">
                                    <span><?php echo number_format($_SESSION['login']['gold'], 2) ?></span></a>
                            </li>
                            <li class="coin-topmenu"><a href=""><img src="/images/coin/scoin100.png">
                                    <span><?php echo number_format($_SESSION['login']['coin'], 2) ?></span></a>
                            </li>
                        <?php } ?>
                        <li class="<?= ((!isset($_SESSION['login'])) ? 'hide' : '') ?>" id="fb-online">
                            <a href="#" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="true">
                                <img id="fb-picture"
                                     src="<?= ((isset($_SESSION['login'])) ? 'https://graph.facebook.com/v2.8/' . $_SESSION['login']['fb_uid'] . '/picture?type=small' : '') ?>">
                                <label id="fb-name"><?= ((isset($_SESSION['login'])) ? $_SESSION['login']['name'] : '') ?></label>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-profile" aria-labelledby="dropdownMenu1">
                                <li><a id="own-profile-link" href="/profile"><i class="fa fa-user" aria-hidden="true"></i>Profile</a></li>
                                <li><a id="facebook-logoutbutton" href="/logout"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a></li>
                            </ul>
                        </li>
                        <?php if (!isset($_SESSION['login'])) { ?>
<!--                            <li id="login-button" style="padding: 8px;color: #003a6c;font-weight: bolder;">-->
<!--                                <fb:login-button style="cursor: pointer" data-size="large"-->
<!--                                                 scope="public_profile,email"-->
<!--                                                 onlogin="checkLoginState();">เข้าสู่ระบบด้วย Facebook-->
<!--                                </fb:login-button>-->
<!--                            </li>-->
                        <?php } ?>
                    </ul>
                </div>

            </div>
        </div>

        <!--เมนูแถบสีขาว-->
        <div class="bar-menu">
            <div class="container">
                <div class="bx-headtop rows">
                    <div class="logo col-sm-4" style=""><img src="/images/logo/logo-ngoal.png"></div>
                    <div class="col-sm-8">
                        <div class="section-login" style="width: 100%;">
                            <a href="/highlight">
                                <img style="height: 45px; margin-right: 5px;" src="/images/banner-hd_preview.png">
                            </a>
                            <a href="/games?mid=<?php echo((!empty($match)) ? $match[0]->id : '') ?>">
                                <img style="height: 45px;" src="/images/banner-game.png">
                            </a>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>
        </div>

        <!--/.navbar-collapse -->
        <div class="main-menu-index navbar-top" style="padding-left: 0;">
            <div class="container">
<!--                <div class="pull-left logo-menu">-->
<!--                    <a href="/"><img src="/images/logo/logo-ngoal-white.png"></a>-->
<!--                </div>-->
                <div class="nav-menutop pull-left nav-menu-main">
                    <ul class="nav nav-pills">
                        <li><a href="/">หน้าหลัก</a></li>
                        <li class="active-dropdown">
                            <div class="hot-icon">
                                <img src="/images/source.gif">
                            </div>
                            <a href="/videos">วีดีโอ/ไฮไลท์บอล <i class="fa fa-chevron-down"
                                                                      aria-hidden="true"></i></a>
                            <span class="bx-menu-dropdown">
                            <table>
                                <tr>
                                    <td><a href="/highlight?category=premier league"><i class="fa fa-play-circle-o"
                                                                                         aria-hidden="true"></i>
                                            พรีเมียร์ลีก</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight?category=la liga"><i class="fa fa-play-circle-o"
                                                                                  aria-hidden="true"></i> ลาลีกา</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight?category=bundesliga"><i class="fa fa-play-circle-o"
                                                                                     aria-hidden="true"></i> บุนเดสลีกา</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight?category=serie a"><i class="fa fa-play-circle-o"
                                                                                  aria-hidden="true"></i> ซีเรียอา</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight?category=league 1"><i class="fa fa-play-circle-o"
                                                                                   aria-hidden="true"></i> ลีกเอิง</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight?category=thai"><i class="fa fa-play-circle-o"
                                                                               aria-hidden="true"></i> ไทยพรีเมียลีก</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight?category=ucl"><i class="fa fa-play-circle-o"
                                                                              aria-hidden="true"></i>
                                            ยูฟ่าแชมเปียนลีก</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight?category=europa"><i class="fa fa-play-circle-o"
                                                                                 aria-hidden="true"></i> ยูโรป้า</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight?category=international"><i class="fa fa-play-circle-o"
                                                                                        aria-hidden="true"></i>
                                            ฟุตบอลทีมชาติ</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight#videosOthersTab"><i class="fa fa-play-circle-o"
                                                                                 aria-hidden="true"></i> ไฮไลท์
                                            Ngoal</a>
                                    </td>
                                </tr>
                            </table>
                        </span>
                        </li>
                        <li class="active-dropdown">
                            <a href="">ข่าวบอลต่างประเทศ <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                            <span class="bx-menu-dropdown">
                            <table>
                                <tr>
                                    <td><a href="/news?type=premier league&Page=1"><i class="fa fa-newspaper-o"
                                                                                          aria-hidden="true"></i>
                                            ข่าวพรีเมียร์ลีก</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/news?type=la liga&Page=1"><i class="fa fa-newspaper-o"
                                                                                   aria-hidden="true"></i>
                                            ข่าวลาลีกา</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/news?type=bundesliga&Page=1"><i class="fa fa-newspaper-o"
                                                                                      aria-hidden="true"></i>
                                            ข่าวบุนเดสลีกา</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/news?type=serie a&Page=1"><i class="fa fa-newspaper-o"
                                                                                   aria-hidden="true"></i> ข่าวซีเรียอา</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/news?type=ucl&Page=1"><i class="fa fa-newspaper-o"
                                                                               aria-hidden="true"></i>
                                            ข่าวยูฟ่าแชมเปียนลีก</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/news?type=europa&Page=1"><i class="fa fa-newspaper-o"
                                                                                  aria-hidden="true"></i>
                                            ข่าวยูโรป้า</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/news?type=thai&Page=1"><i class="fa fa-newspaper-o"
                                                                                aria-hidden="true"></i>
                                            ข่าวฟุตบอลทีมชาติ</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/news?type=other league&Page=1"><i class="fa fa-newspaper-o"
                                                                                        aria-hidden="true"></i>
                                            ข่าวฟุตบอลลีกอื่น</a></td>
                                </tr>
                            </table>
                        </span>
                        </li>
                        <li>
                            <a href="/news?type=thai&Page=1">ข่าวบอลไทย</a>
                        </li>
                        <li><a href="/news?type=other&Page=1">ข่าวกีฬาอื่นๆ</a></li>
                        <li><a href="/gallery">Sexy Football</a></li>
                        <li class="active-dropdown">
                            <div class="hot-icon">
                                <img src="/images/source.gif">
                            </div>
                            <a href="/games?mid=<?php echo((!empty($match)) ? $match[0]->id : '') ?>">เกมทายผลบอล <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                            <span class="bx-menu-dropdown">
                            <table>
                                <tr>
                                    <td><a href="/rules">กติกาและเงื่อนไข</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/ranking">Ranking</a></td>
                                </tr>
                                <!--                                <tr>-->
                                <!--                                    <td><a href="/">สมัครเล่นเกม</a></td>-->
                                <!--                                </tr>-->
                                <!--                                <tr>-->
                                <!--                                    <td><a href="/">บัญชีผู้เล่น</a></td>-->
                                <!--                                </tr>-->

                            </table>
                        </span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</nav>

<div class="modal fade"  id="myModalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form id="formLogin" class="form-horizontal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Login</h4>
            </div>
            <div class="modal-body" style="padding: 0 5%;">
                <div class="form-group" style="margin-top: 15px;">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="text" name="login[username]" class="form-control" id="inputEmail3" placeholder="Username&Email">
                        <small id="errorUsername" class="form-text text-muted " style="color: #ff3f4d;"></small>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" name="login[passage]" class="form-control" id="inputPassword3" placeholder="Password">
                        <small id="errorPassword" class="form-text text-muted " style="color: #ff3f4d;"></small>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="text-align: center;">
<!--                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                <fb:login-button style="cursor: pointer" data-size="large" scope="public_profile,email" onlogin="checkLoginState();">เข้าสู่ระบบด้วย Facebook </fb:login-button>
                <button type="submit" class="btn btn-primary">Login</button>
                <button type="button" data-toggle="modal" data-target="#myModalRegister" class="btn btn-success">Register</button>
            </div>
        </div>
        </form>
    </div>
</div>

<div class="modal fade " style="" id="myModalRegister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form id="formRegister" class="form-horizontal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabelRegister">Register</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="register[id]" value="<?php echo (isset($_SESSION['login']['id'])?$_SESSION['login']['id']:'');?>">
                <div class="form-group" style="margin-top: 15px;">
                    <label for="inputEmail3" class="col-sm-3 control-label">UserName</label>
                    <div class="col-sm-9">
                        <input value="<?php echo (isset($_SESSION['login']['username'])?$_SESSION['login']['username']:'');?>" required type="text" name="register[username]" class="form-control" id="inputEmail3" placeholder="UserName">
                        <small id="errorRegisterUsername" class="form-text text-muted " style="color: #ff3f4d;"></small>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 control-label">Password</label>
                    <div class="col-sm-9">
                        <input required type="password" name="register[password]" class="form-control" id="inputPassword3" placeholder="Password">
                        <small id="errorRegisterPassword" class="form-text text-muted " style="color: #ff3f4d;"></small>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 control-label">ConfrimPassword</label>
                    <div class="col-sm-9">
                        <input required type="password" name="register[confrimpassword]" class="form-control" id="inputPassword3" placeholder="ConfrimPassword">
                        <small id="errorRegisterConfrimPassword" class="form-text text-muted " style="color: #ff3f4d;"></small>
                    </div>
                </div>
                <div class="form-group" style="margin-top: 15px;">
                    <label for="inputEmail3" class="col-sm-3 control-label">Name</label>
                    <div class="col-sm-9">
                        <input value="<?php echo (isset($_SESSION['login']['name'])?$_SESSION['login']['name']:'');?>" required type="text" name="register[name]" class="form-control" id="inputEmail3" placeholder="Name">
                        <small id="errorRegisterName" class="form-text text-muted " style="color: #ff3f4d;"></small>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input value="<?php echo (isset($_SESSION['login']['email'])?$_SESSION['login']['email']:'');?>" required type="email" name="register[email]" class="form-control" id="inputPassword3" placeholder="Email">
                        <small id="errorRegisterEmail" class="form-text text-muted " style="color: #ff3f4d;"></small>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 control-label">picture</label>
                    <div class="col-sm-9" style="">
                        <img id="register-picture" style="width: 100px;margin: 5px;" src="<?php echo (isset($_SESSION['login']['path'])?$_SESSION['login']['path']:'/images/avatar.png');?>">
                        <label id="labelUpload" for="inputUpload" class="btn btn-warning m-0">UpLoad</label>
                        <input type="hidden" value="<?php echo (isset($_SESSION['login']['path'])?$_SESSION['login']['path']:'');?>" name="register[path]" class="form-control" id="inputPassword3" placeholder="path">
                        <small id="errorRegisterPath" class="form-text text-muted " style="color: #ff3f4d;"></small>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="btn_edit" type="submit" class="btn btn-warning hide">Edit</button>
                <button id="btn_register" type="submit" class="btn btn-primary">Register</button>
            </div>
        </div>
        </form>
    </div>
</div>

<form style="display: none;" id="formUpload" method="post" enctype="multipart/form-data">
    <input id="inputUpload" type="file" data-max-size="50000" name="Filedata[]" multiple="multiple" accept="image/*" ><br>
</form>

<?php
    $dataDailyIncome=false;
    $dataNewUserIncome=false;
?>
<?php if(isset($_SESSION['login'])){
    if(empty($playLogIncome)) {
        require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playLogIncome.php";
        $playLogIncome=new playLogIncome();
    }
    $dataDailyIncome=$playLogIncome->checkDailyIncome($_SESSION['login']['id']);
    $dataNewUserIncome=$playLogIncome->checkNewUserIncome($_SESSION['login']['id']);
?>
<div id="model-login-daily" class="modal fade" style="" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="width: 70%;">
        <div class="modal-content" style="position: relative; background: none;">
            <img style="width: 100%;" src="/images/login-daily/login-daily-<?php echo ($_SESSION['login']['combo_login']%7);?>.png">
            <div style="position: absolute;bottom:0;right:0;z-index: 100;width: 20%;padding: 2% 5%;font-size: 18px;font-weight: bolder;color: #fff;background-color: #0e505e;">
                <a href="/play/dailyCoin?pathname=<?php echo $_SERVER['REQUEST_URI'];?>" style="color: #fff;text-decoration: none;">รับ Coin</a>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div id="model-new-user" class="modal fade" style="" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="width: 50%;">
        <div class="modal-content" style="position: relative; background: none;">
            <img style="width: 100%;" src="/images/login-daily/new-user.png">
            <div style="position: absolute;bottom:0;right:0;z-index: 100;width: 20%;height: 20%;padding: 7% 5%;font-size: 18px;font-weight: bolder;color: #fff;">
                <a href="/play/newUserCoin?pathname=<?php echo $_SERVER['REQUEST_URI'];?>" style="color: #fff;text-decoration: none;">รับ Coin</a>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on('click', "#labelUpload", function (e){
        $('input[name=id]').val($(this).attr('data-id'));
    });
    $(document).on('change', "#inputUpload", function (e){
        $("#formUpload").submit();
    });
    $(document).on('submit', "#formUpload", function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        $('input[type=file][data-max-size]').each(function(){
            if(typeof this.files[0] !== 'undefined'){
                var maxSize = parseInt($(this).attr('data-max-size'),10),size = this.files[0].size;
                console.log(maxSize+" :: "+size);
                if(maxSize > size){
                    $.ajax({
                        type: 'POST',
                        url: "/play/uploadFileRegister",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            response=jQuery.parseJSON(response);
                            $('img#register-picture').attr('src',response+'#'+Math.floor((Math.random() * 1000) + 1));
                            $('input[name="register[path]"]').val(response);
                            $('img#player-'+$('#formUpload').find('input[name=id]').val()).attr('src',response+'#'+Math.floor((Math.random() * 1000) + 1));
                        },
                        error: function (data) {
                            console.log("error");
                            console.log(data);
                        }
                    });
                }else{
                    alert("ไฟลมีขนาดใหญ่เกินไป");
                }
            }
        });
    });
    $(document).on('focusout','form#formRegister input',function () {
        if($(this).attr('name')=="register[username]"){
            var username=$(this).val();
            var id=$('form#formRegister').find('input[name="register[id]"]').val();
            $.ajax({
                url: "/play/checkUsername",
                data: {username:username,id:id},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                if (response['status']) {
                    $('#errorRegisterUsername').html("Username duplicate !!");
                }else{
                    $('#errorRegisterUsername').html("");
                }
            });
        }else if($(this).attr('name')=="register[email]"){
            var email=$(this).val();
            var id=$('form#formRegister').find('input[name="register[id]"]').val();
            if(isEmail(email)) {
                $.ajax({
                    url: "/play/checkEmail",
                    data: {email: email,id:id},
                    method: "GET",
                    dataType: "json"
                }).done(function (response) {
                    if (response['status']) {
                        $('#errorRegisterEmail').html("Email duplicate !!");
                    } else {
                        $('#errorRegisterEmail').html("");
                    }
                });
            }
        }else if($(this).attr('name')=="register[confrimpassword]") {
            var password=$('form#formRegister').find('input[name="register[password]"]').first().val();
            var cpassword=$('form#formRegister').find('input[name="register[confrimpassword]"]').first().val();
            if(cpassword!=="" && cpassword>=password && password!=cpassword){
                $('small#errorRegisterConfrimPassword').html("confirm password not math password");
            }else{
                $('small#errorRegisterConfrimPassword').html("");
            }
        }
    });
    $(document).on('keyup','form#formRegister input',function () {
        if($(this).attr('name')=="register[username]"){
            var username=$(this).val();
            var id=$('form#formRegister').find('input[name="register[id]"]').val();
            $.ajax({
                url: "/play/checkUsername",
                data: {username:username,id:id},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                if (response['status']) {
                    $('#errorRegisterUsername').html("Username duplicate !!");
                }else{
                    $('#errorRegisterUsername').html("");
                }
            });
        }else if($(this).attr('name')=="register[email]"){
            var email=$(this).val();
            var id=$('form#formRegister').find('input[name="register[id]"]').val();
            if(isEmail(email)) {
                $.ajax({
                    url: "/play/checkEmail",
                    data: {email: email,id:id},
                    method: "GET",
                    dataType: "json"
                }).done(function (response) {
                    if (response['status']) {
                        $('#errorRegisterEmail').html("Email duplicate !!");
                    } else {
                        $('#errorRegisterEmail').html("");
                    }
                });
            }
        }else if($(this).attr('name')=="register[confrimpassword]") {
            var password=$('form#formRegister').find('input[name="register[password]"]').first().val();
            var cpassword=$('form#formRegister').find('input[name="register[confrimpassword]"]').first().val();
            console.log(password+"::"+cpassword);
            if(cpassword!=="" && cpassword>=password && password!=cpassword){
                $('small#errorRegisterConfrimPassword').html("confirm password not math password");
            }else{
                $('small#errorRegisterConfrimPassword').html("");
            }
        }
    });

    $(document).on('submit', "#formRegister", function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        if($(this).find('input[name="register[username]"]').val()!=="") {
            var username=$('#formRegister').find('input[name="register[username]"]').val();
            var id=$('form#formRegister').find('input[name="register[id]"]').val();
            $.ajax({
                url: "/play/checkUsername",
                data: {username:username,id:id},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                if (response['status']) {
                    $('#errorRegisterUsername').html("Username duplicate !!");
                }else{
                    var email=$('#formRegister').find('input[name="register[email]"]').val();
                    var id=$('form#formRegister').find('input[name="register[id]"]').val();
                    if(isEmail(email)) {
                        $.ajax({
                            url: "/play/checkEmail",
                            data: {email: email,id:id},
                            method: "GET",
                            dataType: "json"
                        }).done(function (response) {
                            if (response['status']) {
                                $('#errorRegisterEmail').html("Email duplicate !!");
                            } else {
                                var password = $('form#formRegister').find('input[name="register[password]"]').first().val();
                                var cpassword = $('form#formRegister').find('input[name="register[confrimpassword]"]').first().val();
                                if (cpassword !== "" && cpassword >= password && password != cpassword) {
                                    $('small#errorRegisterConfrimPassword').html("confirm password not math password");
                                } else {
                                    if($(this).find('input[name="register[path]"]').val()=="") {
                                        $(this).find('small#errorRegisterPath').html("กรุณากรอกเลือกรูป");
                                    }else{
                                        $.ajax({
                                            type: 'POST',
                                            url: "/play/checkOutIn",
                                            data: formData,
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            success: function (response) {
                                                response = jQuery.parseJSON(response);
                                                if (response['status'] !== "success") {
                                                    $('#' + response['status']).html(response['message']);
                                                } else {
                                                    if(window.location.pathname=="/profile"){
                                                        window.location.reload();
                                                    }else {
                                                        $('#fb-gold').removeClass('hide');
                                                        $('#fb-coin').removeClass('hide');
                                                        $('#fb-online').removeClass('hide');
                                                        $('#login-button').addClass('hide');
                                                        $('#fb-gold-text').html(parseFloat(response['data']['gold']).toFixed(2));
                                                        $('#fb-coin-text').html(parseFloat(response['data']['coin']).toFixed(2));
                                                        $('#fb-name').html(response['data']['name']);
                                                        if (response['data']['fb_uid']) {
                                                            $('#fb-picture').attr("src", "https://graph.facebook.com/v2.8/" + response['data']['fb_uid'] + "/picture?type=small");
                                                        } else if (response['data']['path']) {
                                                            $('#fb-picture').attr("src", response['data']['path']);
                                                        } else {
                                                            $('#fb-picture').attr("src", "/images/avatar.png");
                                                        }
                                                        $('#myModalRegister').modal('hide');
                                                        $('#formLogin').modal('hide');
                                                        $('#model-new-user').modal({
                                                            backdrop: 'static',
                                                            keyboard: false
                                                        });
                                                    }
                                                }
                                            },
                                            error: function (data) {
                                                console.log("error");
                                                console.log(data);
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }
    });

    $(document).on('submit', "#formLogin", function (e) {
        e.preventDefault();

        if($(this).find('input[type="text"]').val()==""){
            $('small#errorPath').html("กรุณาเลือกรูป");
        }else{
            $('small#errorPath').html("");
        }

        if($(this).find('input[type="password"]').val()==""){
            $('small#errorPassword').html("กรุณากรอกข้อมูล Password");
        }else{
            $('small#errorPassword').html("");
        }
        if($(this).find('input[type="text"]').val()!=="" && $(this).find('input[type="password"]').val()!=="") {
            var formData = new FormData(this);
            $.ajax({
                type: 'POST',
                url: "/play/checkOutIn",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    response = jQuery.parseJSON(response);
                    if(response['status']!=="success"){
                        $('#'+response['status']).html(response['message']);
                    }else{
                        $('#fb-gold').removeClass('hide');
                        $('#fb-coin').removeClass('hide');
                        $('#fb-online').removeClass('hide');
                        $('#login-button').addClass('hide');
                        $('#fb-gold-text').html(parseFloat(response['data']['gold']).toFixed(2));
                        $('#fb-coin-text').html(parseFloat(response['data']['coin']).toFixed(2));
                        $('#fb-name').html(response['data']['name']);
                        if(response['data']['fb_uid']){
                            $('#fb-picture').attr("src","https://graph.facebook.com/v2.8/"+response['data']['fb_uid']+"/picture?type=small");
                        }else if(response['data']['path']){
                            $('#fb-picture').attr("src", response['data']['path']);
                        }else{
                            $('#fb-picture').attr("src","/images/avatar.png");
                        }
                        $('#myModalLogin').modal('hide');
                    }
                },
                error: function (data) {
                    console.log("error");
                    console.log(data);
                }
            });
        }
    });

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
</script>

<?php if($dataDailyIncome){ ?>
<script>
    $(document).ready(function () {
        $('div#model-login-daily').modal({
            backdrop: 'static',
            keyboard: false
        });
    })
</script>
<?php } ?>
<?php if($dataNewUserIncome){ ?>
<script>
    $(document).ready(function () {
        $('div#model-new-user').modal({
            backdrop: 'static',
            keyboard: false
        });
    })
</script>
<?php } ?>

<?php
//include '../../model/liveLeague.php';
//$liveLeagueObj = new liveLeague();
//$dataLeagueAndMatchToDay = $liveLeagueObj->getLeagueAndMatchToDay(10);
//var_dump($dataLeagueAndMatchToDay);
//var_dump(!empty($dataLeagueAndMatchToDay['All']));
//exit;
?>
<!--<style>-->
<!--    li.active {-->
<!--        background-color: rgba(0, 0, 0, 0.1);-->
<!--    }-->
<!--</style>-->

<!--<script>-->
<!--    var symbolAll = [];-->
<!--    $.get('/services/getBoxSearch', function (data) {-->
<!--        symbolAll = data.list;-->
<!--        $('#searchNewsVideo').typeahead(-->
<!--            {-->
<!--                source: data.list-->
<!--            }-->
<!--        );-->
<!--    }, 'json');-->
<!---->
<!--    $(document).on('keyup', "#searchNewsVideo", function (e) {-->
<!--        var obj = _.filter(symbolAll, function (obj) {-->
<!--            return obj['name'].search(search) !== -1-->
<!--        });-->
<!---->
<!--        if (obj.length > 0 && $(this).val() != "") {-->
<!--            $.each(obj, function (keyV, valueV) {-->
<!---->
<!--            });-->
<!--        }-->
<!--    });-->
<!--    $(document).on('click', '.match-slide', function (e) {-->
<!--        var mid = $(e.currentTarget).attr('mid');-->
<!--        window.location = 'games.php?mid=' + mid;-->
<!--    })-->
<!--</script>-->

<div class="nav-second-menu">
    <div class="row bg-sub-head">
        <div class="container">
            <div class="bx-headtop rows">
                <div class="logo col-sm-4" style=""><img src="/images/logo/logo-ngoal.png"></div>
                <div class="col-sm-6 text-center" style="position: relative;">
                    <!--                    <input class="form-control" id="searchNewsVideo" placeholder="ค้นหา ข่าว ไฮไลท์ etc.">-->
                    <ul style="position: absolute;z-index: 100;display: none;">
                        <li style="width: 100%;float: left;background-color: #cccccc;">
                            <div style="width: 100%;height: 80px;padding: 1%;">
                                <div style="width: 20%;height: 100%;float: left;">
<!--                                    <img style="width: 100%;" src="http://api.ssporting.com/uploaded/video/1105/Image_1105.jpg">-->
                                </div>
                                <div style="padding:1%;width: 80%;height: 100%;float: left;text-align: left;">ไฮไลท์เต็ม
                                    เอฟเวอร์ตัน -vs- ลิเวอร์พูล
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-2">
                    <div class="section-login">
                        <a href="/games?mid=<?php echo ((!empty($match))?$match[0]->id:'') ?>"><img style="height: 45px;" src="/images/banner-game.png"></a>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>
<!--    section new vision layout-->
    <div class="row bg-vision-head">
        <div class="container">
            <?php if(!empty($match)){?>
            <div class="col-sm-12 wrap-box-livescore">
                <div class="bt-backword slide-back">
                    <div class="backward"><i class="fa fa-chevron-left"></i></div>
                </div>
                <div id="liveScoreHead" style="width: 100%;overflow: hidden;height: 190px">
                    <ul class="" style="margin: 0px">
                        <?php foreach ($match as $key=>$value){ ?>
                            <?php if($key%2==0){  ?>
                        <li class="match-slide" style="float: left;">
                            <div class="box-livescore" style="width: 580px;">
                                <div class="col-sm-12 col-box-vision">
                                    <div class="box-white">
                                        <div class="col-sm-6 bx-half-left">
                                            <table>
                                                <tr>
                                                    <td><img src="<?=$value->teamHomePath?>"> </td>
                                                    <td class="<?=((!empty($value->handicap) && $value->handicap<0)?'team-hdp':'')?>"><label><?=$value->teamHomeEn?></label> </td>
                                                    <td><span><?=date("H:i", (strtotime($value->time_match)-(60*60)))?></span></td>
                                                </tr>
                                                <tr>
                                                    <td><img src="<?=$value->teamAwayPath?>"> </td>
                                                    <td class="<?=((!empty($value->handicap) && $value->handicap>0)?'team-hdp':'')?>"><label><?=$value->teamAwayEn?></label></td>
                                                    <td><span><?=$value->handicap?></span></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-sm-6 bx-half-right">
                                            <div><strong>ทรรศนะก่อนเกมส์</strong>
                                                <a href="/games?mid=<?=$value->id?>" class="bt-predict pull-right">ทายผล</a>
                                            </div>
                                            <?php if(!empty($value->userReviewHomeText) && !empty($value->reviewHomeText)){ ?>
                                                <div class="box-vision">
                                                    <div>@<?=$value->userReviewHomeText?></div>
                                                    <div class="content-vision"><?=$value->reviewHomeText?></div>
                                                </div>
                                            <?php }elseif (!empty($value->userReviewAwayText) && !empty($value->reviewAwayText)){ ?>
                                                <div class="box-vision">
                                                    <div>@<?=$value->userReviewAwayText?></div>
                                                    <div class="content-vision"><?=$value->reviewAwayText?></div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                               <?php }else{ ?>
                                <div style="clear: both;"></div>
                                <div class="col-sm-12 col-box-vision">
                                    <div class="box-white">
                                        <div class="col-sm-6 bx-half-left">
                                            <table>
                                                <tr>
                                                    <td><img src="<?=$value->teamHomePath?>"> </td>
                                                    <td class="<?=((!empty($value->handicap) && $value->handicap<0)?'team-hdp':'')?>"><label><?=$value->teamHomeEn?></label></td>
                                                    <td><span><?=date("H:i", (strtotime($value->time_match)-(60*60)))?></span></td>
                                                </tr>
                                                <tr>
                                                    <td><img src="<?=$value->teamAwayPath?>"> </td>
                                                    <td class="<?=((!empty($value->handicap) && $value->handicap>0)?'team-hdp':'')?>"><label><?=$value->teamAwayEn?></label></td>
                                                    <td><span><?=$value->handicap?></span></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-sm-6 bx-half-right">
                                            <div>
                                                <strong>ทรรศนะก่อนเกมส์</strong>
                                                <a href="/games?mid=<?=$value->id?>" class="bt-predict pull-right">ทายผล</a>
                                            </div>
                                            <?php if(!empty($value->userReviewHomeText) && !empty($value->reviewHomeText)){ ?>
                                                <div class="box-vision">
                                                    <div>@<?=$value->userReviewHomeText?></div>
                                                    <div class="content-vision"><?=$value->reviewHomeText?></div>
                                                </div>
                                            <?php }elseif (!empty($value->userReviewAwayText) && !empty($value->reviewAwayText)){ ?>
                                                <div class="box-vision">
                                                    <div>@<?=$value->userReviewAwayText?></div>
                                                    <div class="content-vision"><?=$value->reviewAwayText?></div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </li>
                                <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
                <div class="bt-forward slide-next">
                    <div class="forward"><i class="fa fa-chevron-right"></i></div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>

    <div class="row">
        <div class="container">
            <div class="col-sm-12">
                <div class="nav-menutop nav-menu-main">
                    <ul class="nav nav-pills">
                        <li><a href="/">หน้าหลัก</a></li>
                        <li class="active-dropdown">
                            <a href="/highlight">วีดีโอ/ไฮไลท์บอล</a>
                            <span class="bx-menu-dropdown">
                            <table>
                                <tr>
                                    <td><a href="/highlight?category=premier league"><i class="fa fa-play-circle-o"
                                                                                         aria-hidden="true"></i>
                                            พรีเมียร์ลีก</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight?category=la liga"><i class="fa fa-play-circle-o"
                                                                                  aria-hidden="true"></i> ลาลีกา</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight?category=bundesliga"><i class="fa fa-play-circle-o"
                                                                                     aria-hidden="true"></i> บุนเดสลีกา</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight?category=serie a"><i class="fa fa-play-circle-o"
                                                                                  aria-hidden="true"></i> ซีเรียอา</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight?category=league 1"><i class="fa fa-play-circle-o"
                                                                                   aria-hidden="true"></i> ลีกเอิง</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight?category=thai"><i class="fa fa-play-circle-o"
                                                                               aria-hidden="true"></i> ไทยพรีเมียลีก</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight?category=ucl"><i class="fa fa-play-circle-o"
                                                                              aria-hidden="true"></i>
                                            ยูฟ่าแชมเปียนลีก</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight?category=europa"><i class="fa fa-play-circle-o"
                                                                                 aria-hidden="true"></i> ยูโรป้า</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight?category=international"><i class="fa fa-play-circle-o"
                                                                                        aria-hidden="true"></i>
                                            ฟุตบอลทีมชาติ</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/highlight#videosOthersTab"><i class="fa fa-play-circle-o"
                                                                                 aria-hidden="true"></i> ไฮไลท์
                                            Ngoal</a>
                                    </td>
                                </tr>
                            </table>
                        </span>
                        </li>
                        <li class="active-dropdown">
                            <a href="">ข่าวบอลต่างประเทศ</a>
                            <span class="bx-menu-dropdown">
                            <table>
                                <tr>
                                    <td><a href="/news?type=premier league&Page=1"><i class="fa fa-newspaper-o"
                                                                                          aria-hidden="true"></i>
                                            ข่าวพรีเมียร์ลีก</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/news?type=la liga&Page=1"><i class="fa fa-newspaper-o"
                                                                                   aria-hidden="true"></i>
                                            ข่าวลาลีกา</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/news?type=bundesliga&Page=1"><i class="fa fa-newspaper-o"
                                                                                      aria-hidden="true"></i>
                                            ข่าวบุนเดสลีกา</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/news?type=serie a&Page=1"><i class="fa fa-newspaper-o"
                                                                                   aria-hidden="true"></i> ข่าวซีเรียอา</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="/news?type=ucl&Page=1"><i class="fa fa-newspaper-o"
                                                                               aria-hidden="true"></i>
                                            ข่าวยูฟ่าแชมเปียนลีก</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/news?type=europa&Page=1"><i class="fa fa-newspaper-o"
                                                                                  aria-hidden="true"></i>
                                            ข่าวยูโรป้า</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/news?type=thai&Page=1"><i class="fa fa-newspaper-o"
                                                                                aria-hidden="true"></i>
                                            ข่าวฟุตบอลทีมชาติ</a></td>
                                </tr>
                                <tr>
                                    <td><a href="/news?type=other league&Page=1"><i class="fa fa-newspaper-o"
                                                                                        aria-hidden="true"></i>
                                            ข่าวฟุตบอลลีกอื่น</a></td>
                                </tr>
                            </table>
                        </span>
                        </li>
                        <li>
                            <a href="/news?type=thai&Page=1">ข่าวบอลไทย</a>
                        </li>
                        <li><a href="/news?type=other&Page=1">ข่าวกีฬาอื่นๆ</a></li>
                    </ul>
                </div>
                <div class="sport-bar">
                    <marquee>
                        <?php echo $homem->getHotNews(); ?>
                    </marquee>
                </div>
            </div>
        </div>
    </div>

</div>

<script>

    $(document).ready(function () {

        var $frame = $('.wrap-box-livescore');
        var $slidee = $frame.children('#liveScoreHead').eq(0);
        var $wrap = $frame.parent();
        $slidee.sly({
            horizontal: 1,
            itemNav: 'basic',
            smart: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 0,
//            scrollBar: $wrap.find('.scrollbar'),
            scrollBy: 1,
            speed: 2000,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,

            // Cycling
            cycleBy: 'items',
            cycleInterval: 6000,
            pauseOnHover: 1,

            // Buttons
            forward: $wrap.find('.btnforward'),
            backward: $wrap.find('.btnbackward'),
            prev: $wrap.find('.btnbackward'),
            next: $wrap.find('.btnforward'),
            prevPage: $wrap.find('.backward'),
            nextPage: $wrap.find('.forward')
        });

//        var options = {
//            horizontal: 1,
//            itemNav: 'basic',
//            smart: 1,
//            activateOn: 'click',
//            mouseDragging: 1,
//            touchDragging: 1,
//            releaseSwing: 1,
//            startAt: 0,
//            scrollBy: 1,
//            activatePageOn: 'click',
//            speed: 300,
//            elasticBounds: 1,
//            easing: 'easeOutExpo',
//            dragHandle: 1,
//            dynamicHandle: 1,
//            clickBar: 1,
//        };
//        $('#liveScoreHead').sly(options);
    })
</script>
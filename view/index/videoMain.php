<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/24/2017
 * Time: 10:49
 */
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/VideoM.php';
$webroot = $_SERVER['DOCUMENT_ROOT'];
$VideoMObj = new VideoM();
//$highlightvideo = $VideoMObj->getSlideVideos("general", 1);
$lastestvideo = $VideoMObj->getSlideVideos("general", 24);
$highlightvideo = $VideoMObj->getSlideVideos("highlight", 24);
?>
<!--<div class="row">-->
<!--    <div class="video-cover-index">-->
<!--        <div class="container">-->
<!--            <span><img src="/images/background-text.png"></span>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<div class="container main-content-video" id="container-video-main">
    <div class="rows">
        <div class="col-lg-1">
            <div class="label-vdo">Videos</div>
        </div>
        <div class="col-lg-11">
            <div class="box-video" id="box-video-main-index">
                <?php foreach ($highlightvideo as $key => $value) { ?>
                    <?php if ($key == 0) { ?>
                        <?php if ($value->videosource == "dailymotion") { ?>
                            <iframe id="large-videos-autoPlay" width="100%" height="600" data-autoplay="true"
                                    src="http://www.dailymotion.com/embed/video/<?php echo $value->videokey ?>"
                                    frameborder="0"
                                    allowfullscreen></iframe>
                        <?php } elseif ($value->videosource == "twitter") { ?>
                            <blockquote class="twitter-video" data-lang="en">
                                <a href="<?php echo $value->urlIframe ?>"></a>
                            </blockquote>
                            <!--                                <iframe id="large-videos-autoPlay" width="100%" height="600" data-autoplay="true"-->
                            <!--                                        src="https://twitter.com/i/cards/tfw/v1/--><?php //echo $value->videokey ?><!--"-->
                            <!--                                        frameborder="0"-->
                            <!--                                        allowfullscreen></iframe>-->
                        <?php } elseif ($value->videosource == "facebook") { ?>
                            <iframe id="large-videos-autoPlay" width="100%" height="600" data-autoplay="true"
                                    src="https://www.facebook.com/v2.3/plugins/video.php?allowfullscreen=true&container_width=800&href=<?php echo $value->content ?>"
                                    frameborder="0"
                                    allowfullscreen></iframe>
                        <?php } elseif ($value->videosource == "youtube") { ?>
                            <iframe id="large-videos-autoPlay" width="100%" height="600" data-autoplay="true"
                                    src="https://www.youtube.com/embed/<?php echo $value->videokey ?>"
                                    frameborder="0"
                                    allowfullscreen></iframe>
                        <?php } elseif ($value->videosource == "streamable") { ?>
                            <?php if (!empty($value->pathFile)) { ?>
                                <video style="width: 100%;" class="video-player-tag" controls autoplay>
                                    <source id="large-videos-autoPlay" class="mp4-source" src="<?= $value->pathFile ?>">
                                </video>
                            <?php } else { ?>
                                <iframe id="large-videos-autoPlay" loop="0" width="100%" height="600"
                                        data-autoplay="true"
                                        src="https://streamable.com/s/<?php echo $value->videokey ?>"
                                        frameborder="0"
                                        allowfullscreen></iframe>
                            <?php } ?>
                        <?php } else { ?>
                            <div style="color: #FFFFFF">False</div>
                            <?php
                        }
                    }
                } ?>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>
<div id="container-video-top">
    <div class="wrap-box-videos index-box-videos-highlight index-box-videos-hothit">
        <div class="container">
            <div class="container-listhight">
                <div class="label-highlight-index"><a href="/highlight">HIGHLIGHT</a></div>
                <div class="content-index-box-videos-highlight">
                    <div id="owl-demo" class="owl-carousel owl-theme">
                        <?php foreach ($highlightvideo as $key => $value) { ?>
                            <!--                    --><?php //if ($key > 0) { ?>
                            <?php if ($value->videosource == "twitter") { ?>
                                <a style="" id="box-twitter-video-top-main"
                                   href="/hothit-videos?videoId=<?php echo $value->video_id ?>">
                                    <div class="lastest-video">
                                        <div class="box-videos box-videos-highlight">
                                            <!--                                        <div class="hilight-cover"></div>-->
                                            <div class="icon-play"><i class="fa fa-play"></i></div>
                                            <img src="<?php echo $value->urlImg; ?>">
                                        </div>
                                        <div class="title-owl-carousel title-h-videos">
                                            <?php echo $value->title; ?>
                                        </div>
                                        <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                                    class="how-long"><?php echo strtotime('+5 hours', strtotime($value->create_datetime)) ?></span>
                                        </div>
                                    </div>
                                </a>
                            <?php } else { ?>
                                <div style="" id="box-other-video-top-main">
                                    <div class="lastest-video">
                                        <div class="box-videos box-videos-highlight">
                                            <!--                                        <div class="hilight-cover"></div>-->
                                            <div onclick="setIndexVideosAutoPlay('<?php echo $value->urlIframe ?>','<?php echo $value->video_tag ?>','<?php echo $value->title ?>','','<?php echo $value->create_datetime ?>','<?php echo $value->video_id ?>')"
                                                 class="icon-play"><i class="fa fa-play"></i></div>
                                            <?php if (file_exists($webroot . "/images/video/" . $value->video_id . "/" . $value->video_id . '-index-video.jpg')) { ?>
                                                <img src="<?php echo "/images/video/" . $value->video_id . "/" . $value->video_id . '-index-video.jpg'; ?>">
                                            <?php } else { ?>
                                                <img src="<?php echo $value->urlImg; ?>">
                                            <?php } ?>
                                        </div>
                                        <div class="title-owl-carousel title-h-videos">
                                            <a href="/highlight?videoId=<?= $value->video_id ?>"><?php echo $value->title; ?></a>
                                        </div>
                                        <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                                    class="how-long"><?php echo strtotime('+5 hours', strtotime($value->create_datetime)) ?></span>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <!--                    --><?php //} ?>
                        <?php } ?>
                    </div>
                </div>
                <div style="clear: both"></div>
            </div>
        </div>
    </div>
</div>
<div style="clear: both"></div>
<div id="container-video-bottom">
    <div class="wrap-box-videos index-box-videos-highlight index-box-videos-hothit">
        <div class="container">
            <div class="container-listhight">
                <div class="label-highlight-index">
                    <a href="/hothit">HOT HIT</a>
                </div>
                <div class="content-index-box-videos-highlight">
                    <div id="owl-demo2" class="owl-carousel owl-theme">
                        <?php foreach ($lastestvideo as $key => $value) { ?>
                            <?php if ($value->videosource == "twitter") { ?>
                                <a id="box-twitter-video-bottom-main"
                                   href="/hothit-videos?videoId=<?php echo $value->video_id ?>">
                                    <div class="lastest-video">
                                        <div class="box-videos box-videos-highlight">
                                            <!--                                    <div class="hilight-cover"></div>-->
                                            <div class="icon-play"><i class="fa fa-play"></i></div>
<!--                                            <div class="cover-hothit"><img src="/images/All_.png"></div>-->
                                            <img src="<?php echo $value->urlImg; ?>">
                                        </div>
                                        <div class="title-owl-carousel title-h-videos">
                                            <?php echo $value->title; ?>
                                        </div>
                                        <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                                    class="how-long"><?php echo strtotime('+5 hours', strtotime($value->create_datetime)) ?></span>
                                        </div>
                                    </div>
                                </a>
                            <?php } else { ?>
                                <a id="box-other-video-bottom-main"
                                   href="/hothit-videos?videoId=<?php echo $value->video_id ?>">
                                    <div class="lastest-video">

                                        <div class="box-videos box-videos-highlight">
                                            <!--                                    <div class="hilight-cover"></div>-->
                                            <div class="icon-play"><i class="fa fa-play"></i></div>
<!--                                            <div class="cover-hothit"><img src="/images/All_.png"></div>-->
                                            <?php if (file_exists($webroot . "/images/video/" . $value->video_id . "/" . $value->video_id . '-index-video.jpg')) { ?>
                                                <img src="<?php echo "/images/video/" . $value->video_id . "/" . $value->video_id . '-index-video.jpg'; ?>">
                                            <?php } else { ?>
                                                <img src="<?php echo $value->urlImg; ?>">
                                            <?php } ?>
                                        </div>
                                        <div class="title-owl-carousel title-h-videos"><?php echo $value->title; ?></div>
                                        <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                                    class="how-long"><?php echo strtotime('+5 hours', strtotime($value->create_datetime)) ?></span>
                                        </div>
                                    </div>
                                </a>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <div style="clear: both"></div>
            </div>
        </div>
    </div>
    <div style="clear: both;"></div>
</div>
<script>
    $(document).ready(function () {
        $("#owl-demo").owlCarousel({
            loop: true,
            items: 6,
            navigation: true, // Show next and prev buttons
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            margin: 25,
            responsiveClass: true,
            responsive: {
                350: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 3,
                    nav: true
                },
                1000: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        });
        $("#owl-demo2").owlCarousel({
            loop: true,
            items: 6,
            navigation: true, // Show next and prev buttons
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            margin: 25,
            responsiveClass: true,
            responsive: {
                350: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 3,
                    nav: true
                },
                1000: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        });

        $('.owl-nav').find('.owl-prev').html("<i class='fa fa-chevron-left'>");
        $('.owl-nav').find('.owl-next').html("<i class='fa fa-chevron-right'>")

    });
</script>
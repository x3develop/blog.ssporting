<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
<!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="js/jquery/dist/jquery.min.js"></script>
    <script src="js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="nanoscrollbar/nanoscrollbar.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/timeview.js"></script>
    <script src="js/game.js"></script>
    <script src="js/sly/sly.js"></script>
</head>

<body>

<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/30/2017
 * Time: 16:53
 */
session_start();
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playComment.php";
require_once $_SERVER["DOCUMENT_ROOT"] ."/model/playBet.php";
$playBet=new playBet();
$playMatch = new playMatch();
$playComment=new playComment();
$mid = ($_REQUEST["mid"]) ? $_REQUEST['mid'] : 0;
//$match=$playMatch->getFirstMathById($mid);
$match=array();
$match=$playMatch->getFirstMathStatusBet(30);
$matchResults=array();
$matchView=array();
$betMatchById=array();
if(empty($mid) && !empty($match[0])){
    $matchView=$match[0];
    $matchResults=$playMatch->getListMatchResultsBy($matchView->team_home,$matchView->team_away,6);
//    $matchResultsAway=$playMatch->getListMatchResults($matchView->team_away,$matchView->team_home,6);
    if(isset($_SESSION['login'])){
        $betMatchById=$playBet->getBetMatchById($_SESSION['login']['id'],$matchView->id);
    }
    $comment=$playMatch->getListComment($matchView->id);
    $commentMini=array();
    foreach ($comment as $key=>$value){
        if(!empty($value->countComment)){
            $commentMini[$value->id]=$playComment->getMiniComment($matchView->id,$value->id);
        }
    }
    $heartList=$playComment->checkHeartByMatch($matchView->id);
    $heartListByUser=$playComment->checkHeartByUser($matchView->id);
}else if(!empty($mid)){
    $matchView=$playMatch->getFirstMathById($mid)[0];
    if(isset($_SESSION['login'])){
        $betMatchById=$playBet->getBetMatchById($_SESSION['login']['id'],$matchView->id);
    }
    $comment=$playMatch->getListComment($matchView->id);
    $commentMini=array();
    foreach ($comment as $key=>$value){
        if(!empty($value->countComment)){
            $commentMini[$value->id]=$playComment->getMiniComment($matchView->id,$value->id);
        }
    }
    $heartList=$playComment->checkHeartByMatch($matchView->id);
    $heartListByUser=$playComment->checkHeartByUser($matchView->id);
}

?>
<?php //include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu.php'; ?>
<!--<nav class="navbar navbar-menu-v2">-->
<!--    --><?php //include $_SERVER['DOCUMENT_ROOT'] . '/view/menu/navbar-menu-v2.php'; ?>
<!--</nav>-->
<style>
    .match-slide{
        width: 235px;
    }
    .box-sc-score{
        background-color: #FFFFFF;
    }
    .active>div>div.box-sc-score{
        background-color: #f3f3f3;
    }
    #fade-in-index.show, #fade-in-right-index.show{
        width: 42% !important;
    }
</style>
<div class="box-header" style="background-color: #fff;">
    <div class="container">
        <div class="section-box-rows rows wrap-box-livescore-game">
            <div class="bt-backword" style="top: 32px;">
                <div class="backward disabled"><i class="fa fa-chevron-left"></i></div>
            </div>
            <div id="liveScoreHead" style="width: 100%;overflow: hidden;">
                <ul class="" style="margin: 0px">
                    <?php foreach ($match as $key=>$value){ ?>
                    <li data-key="<?php echo $key;?>" class="match-slide <?php echo ((!empty($mid) && $mid==$value->id)?'active':'')?>" style="float: left;cursor: pointer;">
                        <div class="col-sm-12" onclick="window.location.href='/games.php?mid=<?=$value->id?>'">
                            <div class="box-sc-score bx-half-left">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td><img src="<?=$value->teamHomePath?>"></td>
                                        <td><label><?=$value->teamHomeEn?></label></td>
                                        <td><span class="info"><?= (((!empty($value->status) and $value->status=='bet'))?$value->handicap:$value->home_end_time_score);?></span></td>
                                    </tr>
                                    <tr>
                                        <td><img src="<?=$value->teamAwayPath?>"></td>
                                        <td><label><?=$value->teamAwayEn?></label></td>
                                        <td><span><?= (((!empty($value->status) and $value->status=='bet'))?date('H:i', (strtotime($value->time_match)-(60*60))):$value->away_end_time_score);?></span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="clearfix"></div>
            <div class="bt-forward" style="top: 32px;">
                <div class="forward"><i class="fa fa-chevron-right"></i></div>
            </div>
        </div>
    </div>
</div>

<div id="fade-in-index" class="box-fade">
    <!--        <div class="close-tab pull-right close-tab-right"><i class="fa fa-times" aria-hidden="true"></i></div>-->
    <div class="wrap-selected-team-play wrap-left-team">
        <h1 id="h1-TeamHomeEn"><?=$matchView->teamHomeEn?></h1>

        <p>คุณเลือกเล่นทีม</p>

        <div>
            <img id="img-TeamHomePath" src="<?=$matchView->teamHomePath?>">
        </div>
        <form id="bet-away-match">
            <input name="bet[time_match]" class="form-control" type="hidden" value="<?= $matchView->time_match; ?>">
            <input name="bet[user_id]" class="form-control" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
            <input name="bet[match_id]" class="form-control" type="hidden" value="<?= $matchView->id; ?>">
            <input name="bet[team]" type="hidden" value="home">
            <!--                <input name="bet[use_bet_coin]" type="hidden" value="750">-->
            <input name="bet[use_bet_gold]" type="hidden" value="0">
            <input name="bet[use_bet_coupon]" type="hidden" value="1">
            <input name="bet[status]" type="hidden" value="bet">

            <div class="box-commented" style="margin: 20px 0px;">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="box-input-select">
                        <textarea name="bet[comment]" class="form-control" placeholder="แสดงความคิดเห็น..."></textarea>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>

            <div class="box-button-send">
                <div class="col-sm-2"></div>
                <div class="col-sm-4">
                    <div class="box-input-select">
                        <img src="/images/coin/scoin100.png">
                        <select name="bet[use_bet_coin]">
                            <option value="1200">1200</option>
                            <option value="800">800</option>
                            <option value="400">400</option>
                            <option value="200">200</option>
                        </select>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box-button">
                        <button type="submit" class="btn btn-info btn-lg btn-block">Confirm</button>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
        </form>
        <p>เลือกจำนวน Coin ที่ใช้ในการทายผล</p>
    </div>
</div>
<div id="fade-in-right-index" class="box-fade">
    <!--        <div class="close-tab pull-left close-tab-left"><i class="fa fa-times" aria-hidden="true"></i></div>-->
    <div class="wrap-selected-team-play wrap-right-team">
        <h1 id="h1-TeamAwayEn"><?=$matchView->teamAwayEn?></h1>

        <p>คุณเลือกเล่นทีม</p>

        <div>
            <img id="img-TeamAwayPath" src="<?=$matchView->teamAwayPath?>">
        </div>
        <form id="bet-away-match">
            <input name="bet[time_match]" class="form-control" type="hidden" value="<?= $matchView->time_match; ?>">
            <input name="bet[user_id]" class="form-control" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
            <input name="bet[match_id]" class="form-control" type="hidden" value="<?= $matchView->id; ?>">
            <input name="bet[team]" type="hidden" value="away">
            <!--                <input name="bet[use_bet_coin]" type="hidden" value="750">-->
            <input name="bet[use_bet_gold]" type="hidden" value="0">
            <input name="bet[use_bet_coupon]" type="hidden" value="1">
            <input name="bet[status]" type="hidden" value="bet">
            <div class="box-commented" style="margin: 20px 0px;">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="box-input-select">
                        <textarea name="bet[comment]" class="form-control" placeholder="แสดงความคิดเห็น..."></textarea>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>

            <div class="box-button-send">
                <div class="col-sm-2"></div>
                <div class="col-sm-4">
                    <div class="box-input-select">
                        <img src="/images/coin/scoin100.png">
                        <select name="bet[use_bet_coin]">
                            <option value="1200">1200</option>
                            <option value="800">800</option>
                            <option value="400">400</option>
                            <option value="200">200</option>
                        </select>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box-button">
                        <button type="submit"  class="btn btn-info btn-lg btn-block">Confirm</button>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
        </form>
        <p>เลือกจำนวน Coin ที่ใช้ในการทายผล</p>
    </div>
</div>

<div class="section-games-header">
    <div class="bg-hilight-games">
        <div class="col-sm-2" style="color: #FFFFFF;font-size: 56px;text-align: right;"><?=((!empty($betMatchById['team']) and $betMatchById['team']=="home")?'VOTE':'')?></div>
        <div class="col-sm-8" style="color: white;">
            <div class="row text-center">
                <div class="col-sm-4">
                    <div class="text-center">
                        <img src="<?=$matchView->teamHomePath?>"></div>
                    <div class="name-team-game"><?=$matchView->teamHomeEn?></div>
                </div>
                <div class="col-sm-4">
                    <div class="text-center" style="height: 100px;">
                        <div class="wrap-vote-center">
                            <?php if(empty($betMatchById) && isset($_SESSION['login'])){ ?>){ ?>
                            <div class="vote-left">
                                VOTE
                            </div>
                            <div class="vote-right">
                                VOTE
                            </div>
                            <?php } ?>
                            <div class="bx-hdp box-hdp-vote">
                                HDP
                                <ul>
                                    <li><?=$matchView->home_water_bill?></li>
                                    <li class="tb-hdp"><?=$matchView->handicap?></li>
                                    <li><?=$matchView->away_water_bill?></li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div>
                        <h2><?= date('j M H:i', (strtotime($matchView->time_match)-(60*60)))?></h2>
<!--                        <div>8KM Stadium - Baku 24/22/2016</div>-->
                        <div><?=$matchView->name?></div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="text-center"><img
                            src="<?=$matchView->teamAwayPath?>"></div>
                    <div class="name-team-game"><?=$matchView->teamAwayEn?></div>
                </div>
            </div>
        </div>
        <div class="col-sm-2" style="color: #FFFFFF;font-size: 56px;text-align: left;"><?=((!empty($betMatchById['team']) and $betMatchById['team']=="away")?'VOTE':'')?></div>
    </div>
</div>
<div class="container">
    <div class="row rows-tabs">
        <div class="tabs-topMenu">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-tabs-top" role="tablist">
                <li role="presentation" class="active"><a href="#tab-comments" aria-controls="home" role="tab"
                                                          data-toggle="tab">
                        <div class="box-menu-tabs">
                            <table>
                                <tr>
                                    <td><span>ความคิดเห็น</span></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-comments-o"></i></td>
                                </tr>
                            </table>
                        </div>
                    </a></li>
                <li role="presentation" class="disabled"><a href="#tab-statistics" aria-controls="profile" role="tab" data-toggle="tab">
                        <div class="box-menu-tabs">
                            <table>
                                <tr>
                                    <td><span>สถิติการแข่งขัน</span></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-pie-chart"></i></td>
                                </tr>
                            </table>
                        </div>
                    </a></li>
                <li role="presentation" class="disabled"><a href="#tab-ranking" aria-controls="messages" role="tab" data-toggle="tab">
                        <div class="box-menu-tabs">
                            <table>
                                <tr>
                                    <td><span>การจัดอันดับ</span></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-list"></i></td>
                                </tr>
                            </table>
                        </div>
                    </a></li>
                <li role="presentation"><a href="#tab-rule" aria-controls="settings" role="tab" data-toggle="tab">
                        <div class="box-menu-tabs">
                            <table>
                                <tr>
                                    <td><span>กติกาการเล่นเกมส์</span></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-bullhorn"></i></td>
                                </tr>
                            </table>
                        </div>
                    </a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab-comments">
                    <div class="bx-comments-games mg-bt15"
                         style="background-color: #fff; box-shadow: none;margin: 15px 0px 30px 0px;">
                        <div class="rows">
                            <div class="col-sm-6" style="border-right: solid 1px #ccc;">
                                <div class="box-head-team pull-right">HOME</div>
                                <div class="box-comments">
                                    <table>
                                        <tbody id="box-comments-home-<?=$matchView->id;?>">
                                        <tr id="main-miniComments" class="hide">
                                            <td>
                                                <div class="box-img-user">
                                                    <img id="comment-profile" src="">
                                                </div>
                                            </td>
                                            <td>
                                                <strong id="comment-name"></strong>
                                                <span id="comment-comment"></span>
                                                <div class="times-content">
                                                    <span class="comment-updated_at"></span>
                                                    <span><i id="commemt-heart-" class="fa fa-heart"></i><span id="commemt-comment_match-" class="heart-count"></span></span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr id="main-Comments" class="hide">
                                            <td>
                                                <div class="box-img-user"><img id="comment-profile" src=""></div>
                                            </td>
                                            <td>
                                                <strong id="comment-name"></strong>
                                                <span id="comment-comment"></span>
                                                <div class="times-content">
                                                    <span class="comment-updated_at"></span>
                                                    <span><i class="fa fa-heart"></i><span class="heart-count"></span></span>
                                                    <span class="show-bx-reply">ตอบกลับ</span>
                                                </div>
                                                <div class="bx-reply hide">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <div class="box-img-user">
                                                                    <img src="https://graph.facebook.com/v2.8/<?=$_SESSION['login']['fb_uid'] ?>/picture">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <form id="form-comment">
                                                                    <input type="hidden" name="comment_match[team]" value="home">
                                                                    <input type="hidden" name="comment_match[comment_match_id]" value="<?=$value->id?>">
                                                                    <input type="hidden" name="comment_match[match_id]" value="<?=$matchView->id;?>">
                                                                    <input class="form-control" placeholder="ตอบกลับความคิดเห็น">
                                                                </form>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php foreach ($comment as $key=>$value){
                                            if($value->team=="home"){ ?>
                                                <tr id="box-comment-<?=$value->id?>">
                                                    <td>
                                                        <div class="box-img-user"><img src="<?=((!empty($value->fb_uid))?"https://graph.facebook.com/v2.8/" .$value->fb_uid . "/picture":((!empty($value->path)) ? $value->path : '/images/avatar.png'))?>"></div>
                                                    </td>
                                                    <td>
                                                        <strong><?= ((!empty($value->name))?$value->name:$value->username);?></strong>
                                                        <?= $value->comment;?>
                                                        <div class="times-content"><span class="comment-updated_at" data-updated_at="<?= $value->updated_at ?>"><?= $value->updated_at ?></span>
                                                            <?php if(isset($_SESSION['login'])){ ?>
                                                            <span style="cursor: pointer;">
                                                            <?php if(!empty($heartListByUser[$_SESSION['login']['fb_uid']."-".$value->id])){?>
                                                                <i id="commemt-heart-<?=$value->id?>" onclick="removeHeart(<?=$heartListByUser[$_SESSION['login']['fb_uid']."-".$value->id][0]?>)" class="fa fa-heart heart-active"></i>
                                                                <span class="heart-count" id="commemt-comment_match-<?=$value->id?>"><?=((!empty($heartList[$value->id]))?$heartList[$value->id][0]:'')?>
                                                                    <?php }else{ ?>
                                                                    <i id="commemt-heart-<?=$value->id?>" onclick="addHeart(<?=$value->id?>,<?=$matchView->id;?>)" class="fa fa-heart"></i>
                                                                <span class="heart-count" id="commemt-comment_match-<?=$value->id?>"><?=((!empty($heartList[$value->id]))?$heartList[$value->id][0]:'')?>
                                                                    <?php } ?>
                                                        </span>
                                                                    <?php } ?>
                                                                    <?php if(!empty($value->countComment)){ ?>
                                                                        <span class="show-bx-reply"><?=$value->countComment.' ข้อความตอบกลับ'?></span>
                                                                    <?php }else{ ?>
                                                                        <span class="show-bx-reply">ตอบกลับ</span>
                                                                    <?php } ?>
                                                        </div>
                                                        <div class="bx-reply hide">
                                                            <table>
                                                                <tbody id="comment-bx_reply-<?=$value->id?>">
                                                                <?php if(!empty($commentMini[$value->id])){ ?>
                                                                    <?php foreach($commentMini[$value->id] as $kMini=>$vMini){?>
                                                                        <tr id="box-comment-<?=$vMini['id']?>">
                                                                            <td>
                                                                                <div class="box-img-user">
                                                                                    <img src="https://graph.facebook.com/v2.8/<?=$vMini['fb_uid'] ?>/picture">
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <strong id="Minicomment-name"><?= ((!empty($vMini['name']))?$vMini['name']:$vMini['username']);?></strong>
                                                                                <span id="Minicomment-comment"><?= $vMini['comment'];?></span>
                                                                                <div class="times-content"><span class="comment-updated_at" data-updated_at="<?= $vMini['updated_at'] ?>"><?= $vMini['updated_at'] ?></span>
                                                                                    <?php if(isset($_SESSION['login'])){ ?>
                                                                                    <span style="cursor: pointer;">
                                                                        <?php if(!empty($heartListByUser[$_SESSION['login']['fb_uid']."-".$vMini['id']])){?>
                                                                                        <i id="Minicommemt-heart-<?=$vMini['id']?>" onclick="removeHeart(<?=$heartListByUser[$_SESSION['login']['fb_uid']."-".$vMini['id']][0]?>)" class="fa fa-heart heart-active"></i>
                                                                                <span class="heart-count" id="Minicommemt-comment_match-<?=$vMini['id']?>"><?=$vMini['countHeart']?>
                                                                                    <?php }else{ ?>
                                                                                    <i id="Minicommemt-heart-<?=$vMini['id']?>" onclick="addHeart(<?=$vMini['id']?>,<?=$matchView->id;?>)" class="fa fa-heart"></i>
                                                                                <span class="heart-count" id="Minicommemt-comment_match-<?=$vMini['id']?>"><?=$vMini['countHeart']?>
                                                                                    <?php } ?>
                                                                            </span>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    <?php }} ?>
                                                                <tr>
                                                                    <td>
                                                                        <div class="box-img-user">
                                                                            <img src="https://graph.facebook.com/v2.8/<?=$_SESSION['login']['fb_uid'] ?>/picture">
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <form class="form-comment form-comment-<?=$value->id?>">
                                                                            <input type="hidden" name="comment_match[team]" value="home">
                                                                            <input type="hidden" name="comment_match[comment_match_id]" value="<?=$value->id?>">
                                                                            <input type="hidden" name="comment_match[match_id]" value="<?=$matchView->id;?>">
                                                                            <input name="comment_match[comment]" class="form-control" placeholder="ตอบกลับความคิดเห็น">
                                                                        </form>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php }} ?>
                                        <?php if(isset($_SESSION['login'])){ ?>
                                            <tr>
                                                <td>
                                                    <div class="box-img-user">
                                                        <img src="https://graph.facebook.com/v2.8/<?=$_SESSION['login']['fb_uid'] ?>/picture">
                                                    </div>
                                                </td>
                                                <td>
                                                    <form id="form-comment-home-<?=$matchView->id;?>" class="form-comment-home">
                                                        <input type="hidden" name="comment_match[team]" value="home">
                                                        <input type="hidden" name="comment_match[match_id]" value="<?=$matchView->id;?>">
                                                        <input class="form-control" name="comment_match[comment]" placeholder="ตอบกลับความคิดเห็น">
                                                    </form>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="box-head-team pull-left">AWAY</div>
                                <div class="box-comments">
                                    <table>
                                        <tbody id="box-comments-away-<?=$matchView->id;?>">
                                        <?php foreach ($comment as $key=>$value){
                                            if($value->team=="away"){ ?>
                                                <tr id="box-comment-<?=$value->id?>">
                                                    <td>
                                                        <div class="box-img-user"><img src="<?=((!empty($value->fb_uid))?"https://graph.facebook.com/v2.8/" .$value->fb_uid . "/picture":((!empty($value->path)) ? $value->path : '/images/avatar.png'))?>"></div>
                                                    </td>
                                                    <td>
                                                        <strong><?= ((!empty($value->name))?$value->name:$value->username);?></strong>
                                                        <?= $value->comment;?>
                                                        <div class="times-content"><span class="comment-updated_at" data-updated_at="<?= $value->updated_at ?>"><?= $value->updated_at ?></span>
                                                            <?php if(isset($_SESSION['login'])){ ?>
                                                            <span style="cursor: pointer;">
                                                            <?php if(!empty($heartListByUser[$_SESSION['login']['fb_uid']."-".$value->id])){?>
                                                                <i id="commemt-heart-<?=$value->id?>" onclick="removeHeart(<?=$heartListByUser[$_SESSION['login']['fb_uid']."-".$value->id][0]?>)" class="fa fa-heart heart-active"></i>
                                                                <span class="heart-count" id="commemt-comment_match-<?=$value->id?>"><?=((!empty($heartList[$value->id]))?$heartList[$value->id][0]:'')?>
                                                                    <?php }else{ ?>
                                                                    <i id="commemt-heart-<?=$value->id?>" onclick="addHeart(<?=$value->id?>,<?=$matchView->id;?>)" class="fa fa-heart"></i>
                                                                <span class="heart-count" id="commemt-comment_match-<?=$value->id?>"><?=((!empty($heartList[$value->id]))?$heartList[$value->id][0]:'')?>
                                                                    <?php } ?>
                                                            </span>
                                                                    <?php } ?>
                                                                    <?php if(!empty($value->countComment)){ ?>
                                                                        <span class="show-bx-reply"><?=$value->countComment.' ข้อความตอบกลับ'?></span>
                                                                    <?php }else{ ?>
                                                                        <span class="show-bx-reply">ตอบกลับ</span>
                                                                    <?php } ?>
                                                        </div>
                                                        <div class="bx-reply hide">
                                                            <table>
                                                                <tbody id="comment-bx_reply-<?=$value->id?>">
                                                                <?php if(!empty($commentMini[$value->id])){ ?>
                                                                    <?php foreach($commentMini[$value->id] as $kMini=>$vMini){?>
                                                                        <tr id="box-comment-<?=$vMini['id']?>">
                                                                            <td>
                                                                                <div class="box-img-user">
                                                                                    <img src="https://graph.facebook.com/v2.8/<?=$vMini['fb_uid'] ?>/picture">
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <strong id="Minicomment-name"><?= ((!empty($vMini['name']))?$vMini['name']:$vMini['username']);?></strong>
                                                                                <span id="Minicomment-comment"><?= $vMini['comment'];?></span>
                                                                                <div class="times-content"><span class="comment-updated_at" data-updated_at="<?= $vMini['updated_at'] ?>"><?= $vMini['updated_at'] ?></span>
                                                                                    <?php if(isset($_SESSION['login'])){ ?>
                                                                                    <span style="cursor: pointer;">
                                                                                <?php if(!empty($heartListByUser[$_SESSION['login']['fb_uid']."-".$vMini['id']])){?>
                                                                                        <i id="Minicommemt-heart-<?=$vMini['id']?>" onclick="removeHeart(<?=$heartListByUser[$_SESSION['login']['fb_uid']."-".$vMini['id']][0]?>)" class="fa fa-heart heart-active"></i>
                                                                                <span class="heart-count" id="Minicommemt-comment_match-<?=$vMini['id']?>"><?=$vMini['countHeart']?>
                                                                                    <?php }else{ ?>
                                                                                    <i id="Minicommemt-heart-<?=$vMini['id']?>" onclick="addHeart(<?=$vMini['id']?>,<?=$matchView->id;?>)" class="fa fa-heart"></i>
                                                                                <span class="heart-count" id="Minicommemt-comment_match-<?=$vMini['id']?>"><?=$vMini['countHeart']?>
                                                                                    <?php } ?>
                                                                            </span>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    <?php }} ?>
                                                                <tr>
                                                                    <td>
                                                                        <div class="box-img-user">
                                                                            <img src="https://graph.facebook.com/v2.8/<?=$_SESSION['login']['fb_uid'] ?>/picture">
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <form class="form-comment form-comment-<?=$value->id?>">
                                                                            <input type="hidden" name="comment_match[team]" value="away">
                                                                            <input type="hidden" name="comment_match[comment_match_id]" value="<?=$value->id?>">
                                                                            <input type="hidden" name="comment_match[match_id]" value="<?=$matchView->id;?>">
                                                                            <input name="comment_match[comment]" class="form-control" placeholder="ตอบกลับความคิดเห็น">
                                                                        </form>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php }} ?>
                                        <?php if(isset($_SESSION['login'])){ ?>
                                            <tr>
                                                <td>
                                                    <div class="box-img-user">
                                                        <img src="https://graph.facebook.com/v2.8/<?=$_SESSION['login']['fb_uid'] ?>/picture">
                                                    </div>
                                                </td>
                                                <td>
                                                    <form id="form-comment-away-<?=$matchView->id;?>" class="form-comment-away">
                                                        <input type="hidden" name="comment_match[team]" value="away">
                                                        <input type="hidden" name="comment_match[match_id]" value="<?=$matchView->id;?>">
                                                        <input class="form-control" name="comment_match[comment]" placeholder="ตอบกลับความคิดเห็น">
                                                    </form>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane " id="tab-statistics">
                    <div class="h2h-tabs-team1 h2h-tabs">
                        <div class="tab-head-title text-center">
                            <strong>Head-To-Head</strong>
                        </div>
                        <div class="tab-filter">
                            <div class="text-center">
                                <div style="margin-top: 15px;">
                                    <div class="tabs-selected">
                                        <div class="btn-group btn-selected">
                                            <button team="home" class="btnSelectTeam btn btn-default active">
                                                Real Madrid
                                            </button>
                                            <button team="away" class="btnSelectTeam btn btn-default">
                                                Hoffenhiem
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <div class="dropdown"
                                     style="float:left; margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                    <a data-target="#" href="" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        All Match <label style="font-size: 18px;">10</label>
                                        <span class="caret"></span>
                                    </a>
                                    <ul id="h2h-h-select-limit" class="dropdown-menu" aria-labelledby="dLabel">
                                        <li><a href="">Premier League</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="bx-h2h">
                            <div class="col-sm-6">
                                <div class="pull-right text-right">
                                    <div><img src="/images/graph/circle-graph.jpg" style="height: 180px;"></div>
                                </div>
                                <div class="pull-right">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-winrate">50%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-loserate">30%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class="bx-percent h2hh-oresult-drawrate">20%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>

                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="pull-left text-left">
                                    <div><img src="/images/graph/circle-graph2.jpg" style="height: 180px;"></div>
                                </div>
                                <div class="pull-left">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-result-winrate">30%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-result-loserate">30%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class="bx-percent h2hh-result-drawrate">20%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                    </div>
                    <div class="mg-bt15">
                        <table class="table-h2h table-striped">
                            <thead>
                            <tr>
                                <th>League</th>
                                <th>Date</th>
                                <th colspan="3">
                                    <div class="tab-filter text-center">
                                        <div class="dropdown"
                                             style="display:inline-block;margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                            <a data-target="#" href="http://example.com" data-toggle="dropdown"
                                               role="button" aria-haspopup="true" aria-expanded="true">
                                                <div class="times-content">Filter by Home & Away</div>
                                                <span class="h2h-currentside-view">All Match</span>
                                                <span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                <li><a class="h2h-viewside">Total</a>
                                                </li>
                                                <li><a class="h2h-viewside">Home</a>
                                                </li>
                                                <li><a class="h2h-viewside">Away</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div style="clear:both;"></div>
                                    </div>
                                </th>
                                <th>HDP</th>
                                <th>Odds</th>
                                <th>Result</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?=count($matchResults) ?>
                            <?php foreach ($matchResults as $key=>$value) { ?>
                            <tr class="match-<?=(($matchView->teamHomeEn))?>">
                                <td><?=$value->name?></td>
                                <td><?=date('j F Y',strtotime($value->time_match))?></td>
                                <td class="text-right"><span><?=$value->name_home?></span>
                                    <img src="<?=((!empty($value->path_home))?"".$value->path_home:'/images/teams_clean/team_default_32x32.png') ; ?>">
                                </td>
                                <td class="text-center">
                                    <div class="bx-label-hdp bg-active-sc"><?=$value->scoreHome?> - <?=$value->scoreAway?></div>
                                </td>
                                <td class="text-left">
                                    <img src="<?=((!empty($value->path_away))?"".$value->path_away:'/images/teams_clean/team_default_32x32.png') ; ?>">
                                    <span><?=$value->name_away?></span></td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="wrap mg-bt15">
                        <div class="tab-head-title text-center mg-bt15">
                            <strong>Table League</strong>
                        </div>
                        <div class="tab-filter">
                            <div class="pull-left h-league">
                                <img src="/images/five-leagues/premier-league-logo.png"> Premier League
                            </div>
                            <div class="pull-right">
                                <div class="times-content">Filter by League Or Match</div>
                                <div class="dropdown"
                                     style="float:left; margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                    <a id="dLabel" data-target="#" href="" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        All Match
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li><a class="rank-select-view" view="all">League</a></li>
                                        <li><a class="rank-select-view" view="match">Match</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <table class="table-stat table-striped" style="margin-top: 5px;">
                            <thead>
                            <tr>
                                <th>Position</th>
                                <th>Club</th>
                                <th>Played</th>
                                <th>Win</th>
                                <th>Draw</th>
                                <th>Lost</th>
                                <th>GF</th>
                                <th>GA</th>
                                <th>GD</th>
                                <th>Points</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1 <span class="fa-dot"><i class="fa fa-circle"></i> </span></td>
                                <td><img src="/images/logo-team/Chelsea.png"> Chelsea</td>
                                <td>13</td>
                                <td>10</td>
                                <td>1</td>
                                <td>2</td>
                                <td>19</td>
                                <td>12</td>
                                <td>+19</td>
                                <td>31</td>
                            </tr>
                            <tr>
                                <td>6 <span class="fa-dot"><i class="fa fa-circle"></i> </span></td>
                                <td><img src="/images/logo-team/manchester.png"> Manchester United</td>
                                <td>13</td>
                                <td>10</td>
                                <td>1</td>
                                <td>2</td>
                                <td>19</td>
                                <td>12</td>
                                <td>+19</td>
                                <td>31</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="tab-ranking">
                    <div class="row row-ranking" style="margin-top: 20px;">

                        <div class="col-sm-4 mg-bt15">
                            <div class="bx-white-border mg-bt15">
                                <div class="tab-h-game h-rank">
                                    <img src="/images/trophy/300150.png"> Top Ranking
                                </div>
                                <div class="wrap-rank">
                                    <div>
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs nav-3sec" role="tablist">
                                            <li role="presentation" class="active"><a href="#all-rank"
                                                                                      aria-controls="home"
                                                                                      role="tab" data-toggle="tab"
                                                                                      aria-expanded="false">All Time</a>
                                            </li>
                                            <li role="presentation" class=""><a href="#now-rank" aria-controls="profile"
                                                                                role="tab" data-toggle="tab"
                                                                                aria-expanded="false">Now</a></li>
                                            <li role="presentation"><a href="#last-rank"
                                                                       aria-controls="messages"
                                                                       role="tab" data-toggle="tab"
                                                                       aria-expanded="true">Last
                                                    Champ</a></li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="all-rank">
                                                <div class="box-ranking-top">
                                                    <!--                                        <div id="about" class="nano bx2">-->
                                                    <!--                                            <div class="content">-->
                                                    <table id="gp-alltime-tab" class="table-striped">
                                                        <tbody>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <!--                                                <th>P</th>-->
                                                            <th>PTA</th>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span>1</span>
                                                            </td>
                                                            <td>
                                                                <div class="img-user-ranking"><img
                                                                        src="https://graph.facebook.com/v2.8/1821234304804765/picture?type=normal">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="username-ranking-top"><a
                                                                        style="color:#000000;"
                                                                        href="/profile.php?fbuid=1821234304804765">กางเกงในไม่ใส่
                                                                        มีอันรัยอ่ะป่าว โย่วๆ</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="txt-3em">
                                                                    <strong>10.60</strong>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span>2</span>
                                                            </td>
                                                            <td>
                                                                <div class="img-user-ranking"><img
                                                                        src="https://graph.facebook.com/v2.8/1821234304804765/picture?type=normal">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="username-ranking-top"><a
                                                                        style="color:#000000;"
                                                                        href="/profile.php?fbuid=1821234304804765">กางเกงในไม่ใส่
                                                                        มีอันรัยอ่ะป่าว โย่วๆ</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="txt-3em">
                                                                    <strong>10.60</strong>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                                    <!--                                            </div>-->
                                                    <!--                                        </div>-->
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="now-rank">
                                                <div class="box-ranking-top">
                                                    <!--                                        <div id="about" class="nano bx2">-->
                                                    <!--                                            <div class="content">-->
                                                    <table id="gp-now-tab" class="table-striped">
                                                        <tbody>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <!--                                                <th>P</th>-->
                                                            <th>PTA</th>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                                    <!--                                            </div>-->
                                                    <!--                                        </div>-->
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="last-rank">
                                                <div class="box-ranking-top">
                                                    <!--                                        <div id="about" class="nano bx2">-->
                                                    <!--                                            <div class="content">-->
                                                    <table id="gp-period-tab" class="table-striped">
                                                        <tbody>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <!--                                                <th>P</th>-->
                                                            <th>PTA</th>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span>1</span>
                                                            </td>
                                                            <td>
                                                                <div class="img-user-ranking"><img
                                                                        src="https://graph.facebook.com/v2.8/1821234304804765/picture?type=normal">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="username-ranking-top"><a
                                                                        style="color:#000000;"
                                                                        href="/profile.php?fbuid=1821234304804765">กางเกงในไม่ใส่
                                                                        มีอันรัยอ่ะป่าว โย่วๆ</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="txt-3em">
                                                                    <strong>10.60</strong>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                                    <!--                                            </div>-->
                                                    <!--                                        </div>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 mg-bt15">
                            <div class="bx-white-border mg-bt15">
                                <div class="tab-h-game h-rank">
                                    <img src="/images/trophy/000150.png"> Top Sgold
                                </div>
                                <div class="wrap-rank">
                                    <div>
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs nav-half nav-3sec" role="tablist">
                                            <li role="presentation" class="active"><a href="#now-rank-sgold"
                                                                                      aria-controls="profile" role="tab"
                                                                                      data-toggle="tab"
                                                                                      aria-expanded="false">All
                                                    Time</a>
                                            </li>
                                            <li role="presentation"><a href="#last-rank-sgold"
                                                                       aria-controls="messages"
                                                                       role="tab" data-toggle="tab"
                                                                       aria-expanded="true">Now</a></li>
                                            <li role="presentation"><a href="#last-champ-sgold" aria-controls="messages"
                                                                       role="tab" data-toggle="tab">Last Champ</a></li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="now-rank-sgold">
                                                <div class="box-ranking-top">
                                                    <!--                                        <div id="about" class="nano bx2">-->
                                                    <!--                                            <div class="content">-->
                                                    <table id="gold-alltime-tab" class="table-striped">
                                                        <tbody>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <!--                                                <th>P</th>-->
                                                            <th>GOLD</th>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span>1</span>
                                                            </td>
                                                            <td>
                                                                <div class="img-user-ranking"><img
                                                                        src="https://graph.facebook.com/v2.8/107295366497394/picture?type=normal">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="username-ranking-top"><a
                                                                        style="color:#000000;"
                                                                        href="/profile.php?fbuid=107295366497394">Aphichat
                                                                        Sangkum</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="txt-3em">
                                                                    <strong>-20.00</strong>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span>2</span>
                                                            </td>
                                                            <td>
                                                                <div class="img-user-ranking"><img
                                                                        src="https://graph.facebook.com/v2.8/107295366497394/picture?type=normal">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="username-ranking-top"><a
                                                                        style="color:#000000;"
                                                                        href="/profile.php?fbuid=107295366497394">Aphichat
                                                                        Sangkum</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="txt-3em">
                                                                    <strong>-20.00</strong>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                                    <!--                                            </div>-->
                                                    <!--                                        </div>-->
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="last-rank-sgold">
                                                <div class="box-ranking-top">
                                                    <!--                                        <div id="about" class="nano bx2">-->
                                                    <!--                                            <div class="content">-->
                                                    <table id="gold-now-tab" class="table-striped">
                                                        <tbody>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <!--                                                <th>P</th>-->
                                                            <th>GOLD</th>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                                    <!--                                            </div>-->
                                                    <!--                                        </div>-->
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="last-champ-sgold">
                                                <div class="box-ranking-top">
                                                    <!--                                        <div id="about" class="nano bx2">-->
                                                    <!--                                            <div class="content">-->
                                                    <table id="gold-period-tab" class="table-striped">
                                                        <tbody>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <!--                                                <th>P</th>-->
                                                            <th>GOLD</th>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span>1</span>
                                                            </td>
                                                            <td>
                                                                <div class="img-user-ranking"><img
                                                                        src="https://graph.facebook.com/v2.8/1821234304804765/picture?type=normal">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="username-ranking-top"><a
                                                                        style="color:#000000;"
                                                                        href="/profile.php?fbuid=1821234304804765">กางเกงในไม่ใส่
                                                                        มีอันรัยอ่ะป่าว โย่วๆ</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="txt-3em">
                                                                    <strong>530.00</strong>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 mg-bt15">
                            <div class="bx-white-border mg-bt15">
                                <div class="tab-h-game h-rank">
                                    <img src="/images/trophy/ranking-ufa.png"> Top 5 League
                                </div>
                                <div class="wrap-rank">
                                    <div>
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs nav-5leage" role="tablist">
                                            <li role="presentation" class="active"><a href="#premier-tab"
                                                                                      aria-controls="profile" role="tab"
                                                                                      data-toggle="tab"
                                                                                      aria-expanded="false"><img
                                                        src="/images/five-leagues/English-Premier-League.png"
                                                        style="width: 31px;"></a>
                                            </li>
                                            <li role="presentation" class=""><a href="#laliga-tab"
                                                                                aria-controls="messages" role="tab"
                                                                                data-toggle="tab" aria-expanded="false"><img
                                                        src="/images/five-leagues/La-Liga-Logo-1.png"
                                                        style="width: 30px;"></a></li>
                                            <li role="presentation"><a href="#uefa-tab" aria-controls="messages"
                                                                       role="tab" data-toggle="tab"><img
                                                        src="/images/five-leagues/UEFA-Champions-League-1.png"
                                                        style="width: 32px;"></a>
                                            </li>
                                            <li role="presentation"><a href="#uropa-tab"
                                                                       aria-controls="messages"
                                                                       role="tab" data-toggle="tab"
                                                                       aria-expanded="true"><img
                                                        src="/images/five-leagues/uropa.png" style="width: 31px;"></a>
                                            </li>
                                            <li role="presentation"><a href="#tpl-tab" aria-controls="messages"
                                                                       role="tab" data-toggle="tab"><img
                                                        src="/images/five-leagues/tpl.png" style="width: 31px;"></a>
                                            </li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="premier-tab">
                                                <div class="box-ranking-top">
                                                    <table id="englist-premierleague-tab" class="table-striped">
                                                        <tbody>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>PTA</th>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span>1</span>
                                                            </td>
                                                            <td>
                                                                <div class="img-user-ranking"><img
                                                                        src="https://graph.facebook.com/v2.8/107295366497394/picture?type=normal">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="username-ranking-top"><a
                                                                        style="color:#000000;"
                                                                        href="/profile.php?fbuid=107295366497394">Aphichat
                                                                        Sangkum</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="txt-3em">
                                                                    <strong>-20.00</strong>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span>2</span>
                                                            </td>
                                                            <td>
                                                                <div class="img-user-ranking"><img
                                                                        src="https://graph.facebook.com/v2.8/107295366497394/picture?type=normal">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="username-ranking-top"><a
                                                                        style="color:#000000;"
                                                                        href="/profile.php?fbuid=107295366497394">Aphichat
                                                                        Sangkum</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="txt-3em">
                                                                    <strong>-20.00</strong>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="laliga-tab">
                                                <div class="box-ranking-top">
                                                    <table id="spain-laliga-tab" class="table-striped">
                                                        <tbody>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>PTA</th>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span>1</span>
                                                            </td>
                                                            <td>
                                                                <div class="img-user-ranking"><img
                                                                        src="https://graph.facebook.com/v2.8/107295366497394/picture?type=normal">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="username-ranking-top"><a
                                                                        style="color:#000000;"
                                                                        href="/profile.php?fbuid=107295366497394">Aphichat
                                                                        Sangkum</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="txt-3em">
                                                                    <strong>-20.00</strong>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="uefa-tab">
                                                <div class="box-ranking-top">
                                                    <table id="euro-uefa-tab" class="table-striped">
                                                        <tbody>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>PTA</th>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="uropa-tab">
                                                <div class="box-ranking-top">
                                                    <table id="euro-uropa-tab" class="table-striped">
                                                        <tbody>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>PTA</th>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="tpl-tab">
                                                <div class="box-ranking-top">
                                                    <table id="thai-premierleague-tap" class="table-striped">
                                                        <tbody>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>PTA</th>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="tab-rule">
                    <div class="bx-advertise mg-bt15" style="border-top: 0px;">
                        <div class="content-ads">
                            <div><strong>กติกาการเล่นเกมส์</strong></div>
                            <span>
                               <ul>
                                   <li>- ต้องเล่นเกมบอลเต็งก่อนจึงจะเล่นบอลสเต็ปได้</li>
                                   <li>- เลือกสเต็ปให้ครบ 5 คู่/วัน</li>
                                   <li>- **สเต็ปแตก ผลงานทั้ง 5 ตัว จะต้องเป็น W (Wตัวใหญ่หรือชนะเต็ม) เท่านั้น/li>
                                   <li>- **ไม่สามารถแก้ไขเปลี่ยนทีมที่เลือกเล่นในแต่ละวันได้
                                       ควรตรวจสอบให้มั่นใจก่อนจะกดบันทึก
                                   </li>
                                   <li>- หากผู้รับรางวัลมีบัญชีธนาคารไม่ตรงกับทางเว็บ (กสิกรไทย) จะหักค่าโอนปลายทาง</li>
                               </ul>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>


<script>

    $('a[data-toggle="tab"]').on('click', function(){
        if ($(this).parent('li').hasClass('disabled')) {
            return false;
        };
    });

    $(document).ready(function () {
        var startAt=$("#liveScoreHead").find("li.active").first().attr('data-key');
        var $frame = $('.wrap-box-livescore-game');
        var $slidee = $frame.children('#liveScoreHead').eq(0);
        var $wrap = $frame.parent();
        $slidee.sly({
            horizontal: 3,
            itemNav: 'basic',
            smart: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: startAt,
//            scrollBar: $wrap.find('.scrollbar'),
            scrollBy: 1,
            speed: 2000,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,

            // Cycling
//            cycleBy: 'items',
//            cycleInterval: 6000,
//            pauseOnHover: 1,

            // Buttons
//            forward: $wrap.find('.btnforward'),
//            backward: $wrap.find('.btnbackward'),
            prev: $wrap.find('.backward'),
            next: $wrap.find('.forward'),
//            prevPage: $wrap.find('.backward'),
//            nextPage: $wrap.find('.forward')
        });


//        $('.vote-left').on('click', function () {
//            $('#fade-in').toggleClass('show');
//            $(".wrap-left-team").toggleClass('show');
//        });
//
//        $('.vote-right').on('click', function () {
//            $('#fade-in-right').toggleClass('show');
//            $(".wrap-right-team").toggleClass('show');
//        });

    });
</script>
<script src="/js/play/game.js"></script>
<?php
require_once 'model/VideoM.php';
$VideoMObj = new VideoM();
$dataVideoRecentVideo=$VideoMObj->getRelatedVideo('',6);
$dataVideoAll = $VideoMObj->getAllVideo(0, 6, 'general');
//$dataVideoHighlight=$VideoMObj->getRelatedVideoByType('highlight','',6);
?>

<!--            Recent-->
<div class="swiper-container swiper2 swiper-highlight swiper-container-recent">
    <div class="text-title">
        <h4>Recent Video</h4>
    </div>
    <div class="swiper-wrapper">
        <?php foreach ($dataVideoRecentVideo as $key=>$value){ ?>
        <div class="swiper-slide box-recent-thumbnail">
            <a href="/hothit-videos?videoId=<?php echo $value->video_id ?>"></a>
            <div class="highlight-thumbnail">
                <img src="<?php echo $value->urlImg ?>">
            </div>
            <div class="caption">
                <h4><?php echo $value->title ?></h4>
                <p><?php echo $value->create_datetime ?></p>
            </div>
        </div>
        <?php } ?>
    </div>
    <!-- Add Arrows -->
    <div class="swiper-button-next swiper-button-next-recent"></div>
    <div class="swiper-button-prev swiper-button-prev-recent"></div>
</div>

<!--            ไฮไลท์คู่บอล-->
<div class="swiper-container swiper3 swiper-highlight swiper-container-highlight">
    <div class="text-title">
        <h4>HotHit Video</h4>
    </div>
    <div class="swiper-wrapper">
        <?php foreach ($dataVideoAll as $key=>$value){ ?>
        <div class="swiper-slide box-recent-thumbnail">
            <a href="/hothit-videos?videoId=<?php echo $value->video_id ?>"></a>
            <div class="highlight-thumbnail">
                <img src="<?php echo((!empty($value->thumbnail)) ? $value->thumbnail : '/images/image-not-found.png') ?>">
            </div>
            <div class="caption">
                <h4><?php echo $value->title; ?></h4>
                <p><?php echo $value->create_datetime ?></p>
            </div>
        </div>
        <?php } ?>
    </div>
    <!-- Add Arrows -->
    <div class="swiper-button-next swiper-button-next-highlight"></div>
    <div class="swiper-button-prev swiper-button-prev-highlight"></div>
</div>


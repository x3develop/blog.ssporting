<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/mobile/css/all-style.css" rel="stylesheet">
    <link href="/mobile/css/article.css" rel="stylesheet">
    <link href="/mobile/css/bootstrap.css" rel="stylesheet">
    <link href="/mobile/css/docs.css" rel="stylesheet">
    <link href="/mobile/css/game.css" rel="stylesheet">
    <link href="/mobile/css/highlight.css" rel="stylesheet">
    <link href="/mobile/css/index.css" rel="stylesheet">
    <link href="/mobile/css/live-football.css" rel="stylesheet">
    <link href="/mobile/css/news.css" rel="stylesheet">
    <link href="/mobile/css/profile.css" rel="stylesheet">
    <link href="/mobile/css/ranking.css" rel="stylesheet">
    <link href="/mobile/css/review.css" rel="stylesheet">
    <link href="/mobile/css/livescore-mobile.css" rel="stylesheet">
    <link href="/mobile/css/football-symbol-mobile.css" rel="stylesheet">
    <link href="/mobile/css/swiper.min.css" rel="stylesheet">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script src="https://connect.facebook.net/en_US/sdk.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<!--    <script src="/mobile/js/grid-layout.js"></script>-->

    <script src="/mobile/js/util.js"></script>
    <script src="/mobile/js/login.js"></script>
    <script src="/mobile/js/sexy.js"></script>
</head>
<body>

<?php include("menu-mobile.php"); ?>
<style>

    .content-nav-footer ul li.active-nf-sexy .nf-sexy:before,
    .content-nav-footer ul li.active-nf-sexy span {
        color: rgba(255, 255, 255, 1);
    }


</style>
<div class="all-sexy pull-left">

    <div class="tab-top">
        <div class="container">
            <h2>Sexey Football</h2>
        </div>
        <div class="container">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="sexy-tab active" gtype="all"><a href="#AllSexy" data-toggle="tab">ทั้งหมด</a></li>
                <li class="sexy-tab" gtype="picture"><a href="#PicSexy" data-toggle="tab">รูป</a></li>
                <li class="sexy-tab" gtype="video"><a href="#VdoSexy" data-toggle="tab">วิดีโอ</a></li>
            </ul>
        </div>

    </div>


    <div class="tab-content" style="position: relative">
        <div class="tab-pane fade in active" id="AllSexy">
            <div class="layout flexbin" id="grid_container">


            </div>
        </div>
        <div class="tab-pane fade" id="PicSexy">
            <div id="spic-container" class="layout flexbin">

            </div>
        </div>
        <div class="tab-pane fade" id="VdoSexy">
            <div id="svideo-container" class="layout flexbin">

            </div>
        </div>
    </div>


</div>

<?php include("footer.php"); ?>

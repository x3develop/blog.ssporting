<div class="text-center full-width inline-box hide" id="loading-img" style="margin-top: 20px;">
    <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
</div>

<div id="footer-mark" class="content-nav-footer">
    <ul class="bt-flex">
        <li class="active-nf-livescore">
            <a href="/mobile">
                <div class="nf-livescore"></div>
                <span>Live Score</span>
            </a>
        </li>
        <li class="active-nf-highlight">
            <a href="/mobile/highlight-football.php">
                <div class="nf-highlight"></div>
                <span>Highlight</span>
            </a>
        </li>
        <li class="active-nf-news">
            <a href="/mobile/news.php">
                <div class="nf-news"></div>
                <span>Read</span>
            </a>
        </li>
        <li class="active-nf-sexy">
            <a href="/mobile/sexy-football.php">
                <div class="nf-sexy"></div>
                <span>Sexy</span>
            </a>
        </li>
        <li class="active-nf-profile">
            <a href="/mobile/profile.php">
                <div class="nf-profile"></div>
                <span>Profile</span>
            </a>
        </li>
    </ul>
</div>

<!-- Modal Vote -->
<div class="modal fade modal-vote" id="myModalHome" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-content-home">

            <div class="modal-body">
                <h3><i class="fa fa-check-circle-o" aria-hidden="true"></i> คุณเลือกทีม</h3>
                <img class="home-logo-mark" src="">
                <h2 id="bet-modal-hname" class="home-name-mark">Manchester United</h2>


                <h6>เลือกจำนวน Coin ที่ใช้ในการทายผล</h6>
                <div class="dropdown">
                    <button class="btn btn-default btn-lg dropdown-toggle" type="button" id="dropdownMenu1"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <img src="/images/coin/scoin100.png"> <span class="bet-coin">1200</span>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="#" class="select-coin" value="1200">1,200</a></li>
                        <li><a href="#" class="select-coin" value="800">800</a></li>
                        <li><a href="#" class="select-coin" value="400">400</a></li>
                        <li><a href="#" class="select-coin" value="200">200</a></li>
                    </ul>
                </div>
                <div class="full-width inline-box">
                    <textarea id="home-comment-box" class="form-control" placeholder="เขียนทรรศนะ" rows="3"
                              maxlength="250"></textarea>
                </div>
                <div class="btn-group" role="group" aria-label="...">
                    <button type="button" class="btn btn-lg btn-danger bet-save" side="home">ตกลง</button>
                    <button type="button" class="btn btn-lg btn-default" data-dismiss="modal">ปิด</button>
                </div>
                <div class="full-width inline-box">
                    <img class="bet-waiting-img hide" src="/images/loading.gif"></img>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-vote fade" id="myModalAway" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-content-away">

            <div class="modal-body">
                <h3><i class="fa fa-check-circle-o" aria-hidden="true"></i> คุณเลือกทีม</h3>
                <img class="away-logo-mark" src="">
                <h2 id="bet-modal-aname" class="away-name-mark">Chelsea FC</h2>


                <h6>เลือกจำนวน Coin ที่ใช้ในการทายผล</h6>
                <div class="dropdown">
                    <button class="btn btn-default btn-lg dropdown-toggle" type="button" id="dropdownMenu1"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <img src="/images/coin/scoin100.png"> <span class="bet-coin">1200</span>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="#" class="select-coin" value="1200">1,200</a></li>
                        <li><a href="#" class="select-coin" value="800">800</a></li>
                        <li><a href="#" class="select-coin" value="400">400</a></li>
                        <li><a href="#" class="select-coin" value="200">200</a></li>
                    </ul>
                </div>
                <div class="full-width inline-box">
                    <textarea id="away-comment-box" class="form-control" placeholder="เขียนทรรศนะ" rows="3"
                              maxlength="250"></textarea>
                </div>
                <div class="btn-group" role="group" aria-label="...">
                    <button type="button" class="btn btn-lg btn-primary bet-save" side="away">ตกลง</button>
                    <button type="button" class="btn btn-lg btn-default" data-dismiss="modal">ปิด</button>
                </div>
                <div class="full-width inline-box">
                    <img class="bet-waiting-img hide" src="/images/loading.gif"></img>
                </div>
            </div>
        </div>
    </div>

</div>


<!--<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>-->
<!--<script type="text/javascript" src="/mobile/js/grid-layout.js"></script>-->
<!--<script type="text/javascript" src="/mobile/js/bootstrap.js"></script>-->
<script type="text/javascript" src="/mobile/js/swiper.min.js"></script>

<!--isNumeric-->
<script type="text/javascript">
    refreshTime();

    function refreshTime() {
        $.each($(".how-long"), function (k, v) {
            if ($.isNumeric($(this).text())) {
                $(this).html(moment($(this).text(), "X").fromNow())
            } else {
                $(this).html(moment($(this).text(), "YYYY-MM-DD HH:mm:ss").fromNow())
            }
        })

    }
</script>

<!--swiper-->
<script type="text/javascript">
    (function ($) {
        var swiper = new Swiper('.swiper-container', {
            pagination: {
                el: '.swiper-pagination',
                dynamicBullets: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    })(jQuery);
</script>

<!--scroll-->
<script type="text/javascript">
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#scroll').fadeIn();
            } else {
                $('#scroll').fadeOut();
            }
        });
        $('#scroll').click(function () {
            $("html, body").animate({scrollTop: 0}, 600);
            return false;
        });
    });
</script>

<!--navbar-scroll-->
<!--<script type="text/javascript">-->
<!--    var prevScrollpos = window.pageYOffset;-->
<!--    window.onscroll = function () {-->
<!--        var currentScrollPos = window.pageYOffset;-->
<!--        if (prevScrollpos > currentScrollPos) {-->
<!--            document.getElementById("navbar-scroll").style.top = "0";-->
<!--        } else {-->
<!--            document.getElementById("navbar-scroll").style.top = "-80px";-->
<!--        }-->
<!--        prevScrollpos = currentScrollPos;-->
<!--    }-->
<!---->
<!--</script>-->


<!--selectTeams-->
<script type="text/javascript">
    $("#selectTeams span").click(function () {
        // Remove active if this does not have active class
        if (!($(this).closest("span").hasClass("active"))) {
            $(this).closest("#selectTeams").find("span.active").removeClass('active');
        }
        $(this).closest("span").addClass('active');
    });

    $("#cat-news span").click(function () {
        // Remove active if this does not have active class
        if (!($(this).closest("span").hasClass("active"))) {
            $(this).closest("#cat-news").find("span.active").removeClass('active');
        }
        $(this).closest("span").addClass('active');
    });

    $("#list_game div.thumbnail").click(function () {
        // Remove active if this does not have active class
        if (!($(this).closest("span").hasClass("active"))) {
            $(this).closest("#list_game").find("div.thumbnail.active").removeClass('active');
        }
        $(this).closest("div").addClass('active');
    });
</script>

<!--openNav-->
<script type="text/javascript">
    function openNav() {
        document.getElementById("mySidenav").style.width = "100%";
        document.getElementById("main-review").style.marginRight = "250px";
        document.getElementById("main-review").style.right = "0";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main-review").style.marginRight = "0";
        document.getElementById("main-review").style.right = "0";
    }
</script>

<script type="text/javascript">
    /* When the user scrolls down, hide the navbar. When the user scrolls up, show the navbar */
    // var prevScrollpos = window.pageYOffset;
    // window.onscroll = function() {
    //     var currentScrollPos = window.pageYOffset;
    //     if (prevScrollpos > currentScrollPos) {
    //         document.getElementById("footer-mark").style.bottom = "0";
    //         document.getElementById("navbar-scroll").style.top = "0";
    //     } else {
    //         document.getElementById("footer-mark").style.bottom = "-80px";
    //         document.getElementById("navbar-scroll").style.top = "-80px";
    //     }
    //     prevScrollpos = currentScrollPos;
    // }
</script>

</body>
</html>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Cache-control" content="public,max-age=604800">
    <link href="/mobile/css/all-style.css" rel="stylesheet">
    <link href="/mobile/css/article.css" rel="stylesheet">
    <link href="/mobile/css/bootstrap.css" rel="stylesheet">
    <link href="/mobile/css/docs.css" rel="stylesheet">
    <link href="/mobile/css/game.css" rel="stylesheet">
    <link href="/mobile/css/highlight.css" rel="stylesheet">
    <link href="/mobile/css/index.css" rel="stylesheet">
    <link href="/mobile/css/news.css" rel="stylesheet">
    <link href="/mobile/css/profile.css" rel="stylesheet">
    <link href="/mobile/css/ranking.css" rel="stylesheet">
    <link href="/mobile/css/review.css" rel="stylesheet">
    <link href="/mobile/css/swiper.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone.min.js"></script>
    <script src="/js/newFB.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/mobile/js/news.js"></script>
</head>
<?php
include_once __DIR__ . "/../bootstrap.php";
use Carbon\Carbon;

$ncon = new \controller\NewsController();
$nlist = $ncon->getByCat("premier league", 0);
?>
<body>
<a href="#" id="scroll" style="display: none; bottom: 100px;"><span></span></a>

<style>

    .content-nav-footer ul li.active-nf-news .nf-news:before,
    .content-nav-footer ul li.active-nf-news span {
        color: rgba(255, 255, 255, 1);
    }
</style>
<?php include "menu-mobile.php"; ?>
<div class="tab-top tab-top-article">
    <div class="container title">
        <img id="league-news-logo" src="/images/five-leagues/premier-league-logo.png">
        <h2 id="league-news-name">English Premier League</h2>
        <button class="btn btn-link pull-right collapsed" type="button" data-toggle="collapse"
                data-target="#collapseCatNews" aria-expanded="false" aria-controls="collapseCatNews">
        </button>
    </div>

    <div>
        <div class="collapse" id="collapseCatNews">
            <div id="cat-news" class="container-cat-news well well-sm">
                <span class="league-news" lname="thai">
                    <img src="/images/five-leagues/thai-logo.png">
                 <p>บอลไทย</p>
                </span>
                <span class="league-news active" lname="premier league">
                    <img src="/images/five-leagues/premier-league-logo.png">
                    <p id="league-news-name">พรีเมียร์ลีก อังกฤษ</p>
                </span>
                <span class="league-news" lname="la liga">
                    <img src="/images/five-leagues/La-Liga-Logo-1.png">
                 <p id="league-news-name">ลาลีกา สเปน</p>
                </span>
                <span class="league-news" lname="league 1">
                    <img src="/images/five-leagues/Ligue-1-logo-france.png">
                 <p>ลีก เอิง ฝรั่งเศส</p>
                </span>
                <span class="league-news" lname="bundesliga">
                    <img src="/images/five-leagues/logo_bl.gif">
                 <p>บุนเดสลีกา เยอรมัน</p>
                </span>
                <span class="league-news" lname="serie a">
                    <img src="/images/five-leagues/seriea1.png">
                 <p>กัลโช่ เซเรีย อา อิตาลี</p>
                </span>
                <span class="league-news" lname="ucl">
                    <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                     <p>ยูฟ่า แชมเปี้ยนส์ลีก</p>
                </span>
                <span class="league-news" lname="europa">
                    <img src="/images/five-leagues/uropa.png">
                     <p>ยูโรปา ลีก</p>
                </span>
            </div>
        </div>
    </div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="tab-select tab-select-news active" attab="news"><a href="#news" data-toggle="tab">ข่าว</a></li>
        <li class="tab-select tab-select-table" attab="standing"><a href="#tableStand" data-toggle="tab">ตารางคะแนน</a>
        </li>
        <li class="tab-select tab-select-fixture" attab="fixture"><a href="#tablematch" data-toggle="tab">แมตช์</a>
        </li>
        <li class="tab-select tab-select-topscore" attab="player"><a href="#topplayer" data-toggle="tab">ดาวซัลโว</a>
        </li>
    </ul>


</div>
<div class="tab-content tab-content-article">
    <div class="tab-pane fade in active" id="news">
        <div class="highlight-news">
            <a class="link_wrap" href="/mobile/news-info?id=<?php echo $nlist[0]->newsId; ?>"></a>
            <div class="highlight-news-thumbnail">
                <img class="hl-img" src="https://www.ngoal.com/<?php echo $nlist[0]->imageLink; ?>">
            </div>
            <div class="highlight-news-topic">
                <h1 class="hl-headline"><?php echo $nlist[0]->titleTh; ?></h1>
            </div>
        </div>

        <!--        Full News-->
        <div class="content-full-news">
            <div class="col-sm-12">
                <div class="text-title">
                    <h4>ข่าวที่น่าสนใจ</h4>
                </div>
            </div>
            <div class="col-sm-12">
                <a class="link_wrap" href="#"></a>
                <div class="thumbnail-full-news">
                    <div class="crop-full-news" style="background-image: url('/images/1.jpg')"></div>
                </div>
                <h2>OFFICIAL: ทุบสถิติโอเวน! สาลิกาซิว'อัลมิรอน'แนวรุกปารากวัย</h2>
            </div>
            <div class="col-sm-12">
                <a class="link_wrap" href="#"></a>
                <div class="thumbnail-full-news">
                    <div class="crop-full-news" style="background-image: url('/images/2.jpg')"></div>
                </div>
                <h2>OFFICIAL: ทุบสถิติโอเวน! สาลิกาซิว'อัลมิรอน'แนวรุกปารากวัย</h2>
            </div>
            <div class="col-sm-12">
                <a class="link_wrap" href="#"></a>
                <div class="thumbnail-full-news">
                    <div class="crop-full-news" style="background-image: url('/images/3.jpg')"></div>
                </div>
                <h2>OFFICIAL: ทุบสถิติโอเวน! สาลิกาซิว'อัลมิรอน'แนวรุกปารากวัย</h2>
            </div>
            <div class="col-sm-12">
                <a class="link_wrap" href="#"></a>
                <div class="thumbnail-full-news">
                    <div class="crop-full-news" style="background-image: url('/images/4.jpg')"></div>
                </div>
                <h2>OFFICIAL: ทุบสถิติโอเวน! สาลิกาซิว'อัลมิรอน'แนวรุกปารากวัย</h2>
            </div>
        </div>


        <!--        picpost-->
        <div class="content-picpost">

            <div class="picpost-v1">
                <div class="text-title">
                    <h4><a href="#">Sexy Post</a></h4>
                </div>
                <div class="content-picpost-box">
                    <a class="link_wrap" href="#"></a>
                    <div class="item-content-picpost">
                        <div class="crop-picpost" style="background-image: url('/images/gallery/1.jpg')"></div>
                    </div>
                    <p>กิ๊บซี่ วนิดา สาวไซส์มินิ ที่ความเซ็กซี่มีล้นเหลือ!!</p>
                </div>
                <div class="content-picpost-box">
                    <a class="link_wrap" href="#"></a>
                    <div class="item-content-picpost">
                        <div class="crop-picpost" style="background-image: url('/images/gallery/2.jpg')"></div>
                    </div>
                    <p>กระแต อาร์สยาม เข็ดแล้วเต้นโคฟเวอร์ BLACKPINK คนติดูเหมือนสก๊อย</p>
                </div>
                <div class="content-picpost-box">
                    <a class="link_wrap" href="#"></a>
                    <div class="item-content-picpost">
                        <div class="crop-picpost" style="background-image: url('/images/gallery/3.jpg')"></div>
                    </div>
                    <p>ปุ๊กลุก ฝนทิพย์ แซ่บ ในชุดว่ายน้ำแหวกหลัง</p>
                </div>
                <div class="content-picpost-box">
                    <a class="link_wrap" href="#"></a>
                    <div class="item-content-picpost">
                        <div class="crop-picpost" style="background-image: url('/images/gallery/4.jpg')"></div>
                    </div>
                    <p>เปิดภาพล่าสุด มะนาว เจ้าบ่าวหนีงานแต่ง สวย แซ่บ เป๊ะ ขึ้นสิบเท่า</p>
                </div>
            </div>
            <div class="picpost-v2">
                <div class="text-title">
                    <h4><a href="#">Sexy Post</a></h4>
                </div>
                <!-- Swiper -->
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="content-picpost-box thumbnail">
                                <a class="link_wrap" href="#"></a>
                                <div class="item-content-picpost">
                                    <div class="crop-picpost"
                                         style="background-image: url('/images/gallery/1.jpg')"></div>
                                </div>
                                <p>กิ๊บซี่ วนิดา สาวไซส์มินิ ที่ความเซ็กซี่มีล้นเหลือ!!</p>
                            </div>
                            <div class="content-picpost-box thumbnail">
                                <a class="link_wrap" href="#"></a>
                                <div class="item-content-picpost">
                                    <div class="crop-picpost"
                                         style="background-image: url('/images/gallery/2.jpg')"></div>
                                </div>
                                <p>กระแต อาร์สยาม เข็ดแล้วเต้นโคฟเวอร์ BLACKPINK คนติดูเหมือนสก๊อย</p>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="content-picpost-box thumbnail">
                                <a class="link_wrap" href="#"></a>
                                <div class="item-content-picpost">
                                    <div class="crop-picpost"
                                         style="background-image: url('/images/gallery/3.jpg')"></div>
                                </div>
                                <p>ปุ๊กลุก ฝนทิพย์ แซ่บ ในชุดว่ายน้ำแหวกหลัง</p>
                            </div>
                            <div class="content-picpost-box thumbnail">
                                <a class="link_wrap" href="#"></a>
                                <div class="item-content-picpost">
                                    <div class="crop-picpost"
                                         style="background-image: url('/images/gallery/4.jpg')"></div>
                                </div>
                                <p>เปิดภาพล่าสุด มะนาว เจ้าบ่าวหนีงานแต่ง สวย แซ่บ เป๊ะ ขึ้นสิบเท่า</p>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="content-picpost-box thumbnail">
                                <a class="link_wrap" href="#"></a>
                                <div class="item-content-picpost">
                                    <div class="crop-picpost"
                                         style="background-image: url('/images/gallery/1.jpg')"></div>
                                </div>
                                <p>กิ๊บซี่ วนิดา สาวไซส์มินิ ที่ความเซ็กซี่มีล้นเหลือ!!</p>
                            </div>
                            <div class="content-picpost-box thumbnail">
                                <a class="link_wrap" href="#"></a>
                                <div class="item-content-picpost">
                                    <div class="crop-picpost"
                                         style="background-image: url('/images/gallery/2.jpg')"></div>
                                </div>
                                <p>กระแต อาร์สยาม เข็ดแล้วเต้นโคฟเวอร์ BLACKPINK คนติดูเหมือนสก๊อย</p>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="content-picpost-box thumbnail">
                                <a class="link_wrap" href="#"></a>
                                <div class="item-content-picpost">
                                    <div class="crop-picpost"
                                         style="background-image: url('/images/gallery/3.jpg')"></div>
                                </div>
                                <p>ปุ๊กลุก ฝนทิพย์ แซ่บ ในชุดว่ายน้ำแหวกหลัง</p>
                            </div>
                            <div class="content-picpost-box thumbnail">
                                <a class="link_wrap" href="#"></a>
                                <div class="item-content-picpost">
                                    <div class="crop-picpost"
                                         style="background-image: url('/images/gallery/4.jpg')"></div>
                                </div>
                                <p>เปิดภาพล่าสุด มะนาว เจ้าบ่าวหนีงานแต่ง สวย แซ่บ เป๊ะ ขึ้นสิบเท่า</p>
                            </div>
                        </div>
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                </div>


            </div>
        </div>


        <!--    Highlight-->

        <div class="content-simple-highlight">
            <div class="highlight-v1">
                <div class="text-title">
                    <h4>Highlight Football</h4>
                </div>
                <div class="box-hothit-item-new highlight-item">
                    <a class="link_wrap" href="#"></a>
                    <div class="box-hothit-thumbnail">
                        <img src="/images/luys5_first_1.jpg">
                    </div>
                    <div class="box-hothit-title">
                        <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                    </div>

                </div>
                <div class="box-hothit-item-new highlight-item">
                    <div class="box-hothit-thumbnail">
                        <img src="/images/luys5_first_1.jpg">
                    </div>
                    <div class="box-hothit-title">
                        <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                    </div>

                </div>
            </div>
            <div class="highlight-v2">
                <div class="text-title">
                    <h4>Highlight Football</h4>
                </div>
                <!-- Swiper -->
                <div class="swiper-container swiper-highlight">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="content-higlight-box-thumbnail thumbnail">
                                <div class="content-higlight-box">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                            </div>
                            <div class="content-higlight-box-thumbnail thumbnail">
                                <div class="content-higlight-box">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="content-higlight-box-thumbnail thumbnail">
                                <div class="content-higlight-box">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                            </div>
                            <div class="content-higlight-box-thumbnail thumbnail">
                                <div class="content-higlight-box">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                            </div>
                        </div>
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>


        <!--        NEW-->
        <div class="content-recent-col-2" id="news-main-box">
            <?php foreach ($nlist as $news) { ?>
                <?php
                $posttime = Carbon::createFromFormat("Y-m-d H:i:s", $news->created_at, "UTC");
                $posttime->setTimezone("Asia/Bangkok");
                ?>
                <div class="content-item-recent-row-2">
                    <a class="link_wrap" href="/mobile/news-info?id=<?php echo $news->newsId; ?>"></a>
                    <div class="item-recent-col-2">
                        <div class="media-thumbnail"><img src="https://www.ngoal.com/<?php echo $news->imageLink; ?>">
                        </div>
                        <div class="bx-2">
                            <h4 class="media-heading"><?php echo $news->titleTh; ?></h4>
                            <div class="time-box box-inline hide post-at" at="<?php echo $posttime ?>"></div>
                            <div class="vertical-line-between box-inline hide"></div>
                            <div class="view-box box-inline hide"><?php echo $news->readCount; ?></div>
                        </div>
                    </div>
                </div>
            <?php } ?>


        </div>


    </div>
    <div class="tab-pane fade" id="tableStand">
        <div class="container">
            <span class="pull-left hide">Season 18/19</span>
            <div class="text-right hide">
                <div class="btn-group" role="group" aria-label="...">
                    <button type="button" class="btn btn-default btn-xs active">ทั้งหมด</button>
                    <button type="button" class="btn btn-default btn-xs">บ้าน</button>
                    <button type="button" class="btn btn-default btn-xs">เยือน</button>
                </div>
            </div>
            <div style="clear: both;"></div>
            <div class="content-table-standing" id="standing-box">
                <table class="standing-group-table">
                    <thead>
                    <tr>
                        <th class="group-title"></th>
                    </tr>
                    <tr>
                        <th>#</th>
                        <th>Team</th>
                        <th>P</th>
                        <th>GD</th>
                        <th>PTS</th>
                    </tr>
                    </thead>
                    <tbody class="standing-order">
                    <tr class="std-row">
                        <td class="std-pos">1</td>
                        <td>
                            <img src="/images/teams_clean/team_default_32x32.png">
                            <p class="std-tname"></p>
                        </td>
                        <td class="std-p"></td>
                        <td class="std-gd"></td>
                        <td class="std-pts"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="tablematch">
        <div class="container" id="fixture-box">
            <div class="box-match-tournament content-news-match">
                <div class="row">
                    <div class="box-match-list-topic">
                        <div class="pull-left">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            <p class="fix-date"></p>
                        </div>
                    </div>
                </div>
                <div class="row fix-match">
                    <div class="box-match-list">
                        <div class="time-match text-center fix-time">15:00</div>
                        <div>
                            <div class="home-name-match"><img class="fix-hlogo"
                                                              src="/images/teams_clean/team_default_32x32.png"><label
                                        class="fix-hname">Chelsea</label></div>
                            <div class="away-name-match"><img class="fix-alogo"
                                                              src="/images/teams_clean/team_default_32x32.png"><label
                                        class="fix-aname">Manchester united</label></div>
                        </div>
                        <div>
                            <div class="home-score-match">0</div>
                            <div class="away-score-match">0</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="tab-pane fade" id="topplayer">
        <div class="table-topscore">
            <table id="tp-list">
                <tr class="tp-rows">
                    <td class="tp-rank">1</td>
                    <td><img class="tp-img" src="/images/player/image_4.jpg"></td>
                    <td>
                        <span class="tp-name">Eden Hazard</span>
                        <img class="tp-tlogo" src="/images/logo-team/Chelsea.png">
                        <p class="tp-tname">Chelsea</p>
                    </td>
                    <td><img src="/images/football.png"><label class="tp-goal">23</label></td>
                </tr>
            </table>
        </div>
    </div>
</div>


<!-- Initialize Swiper -->
<script>
    var swiper = new Swiper('.swiper-container', {

        slidesPerColumn: 3,
        spaceBetween: 3,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });
</script>
<script>
    var swiper = new Swiper('.swiper-highlight', {

        slidesPerColumn: 3,
        spaceBetween: 3,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });
</script>

<?php include "footer.php"; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/mobile/css/all-style.css" rel="stylesheet">
    <link href="/mobile/css/article.css" rel="stylesheet">
    <link href="/mobile/css/bootstrap.css" rel="stylesheet">
    <link href="/mobile/css/docs.css" rel="stylesheet">
    <link href="/mobile/css/game.css" rel="stylesheet">
    <link href="/mobile/css/highlight.css" rel="stylesheet">
    <link href="/mobile/css/index.css" rel="stylesheet">
    <link href="/mobile/css/live-football.css" rel="stylesheet">
    <link href="/mobile/css/news.css" rel="stylesheet">
    <link href="/mobile/css/profile.css" rel="stylesheet">
    <link href="/mobile/css/ranking.css" rel="stylesheet">
    <link href="/mobile/css/review.css" rel="stylesheet">
    <link href="/mobile/css/livescore-mobile.css" rel="stylesheet">
    <link href="/mobile/css/football-symbol-mobile.css" rel="stylesheet">
    <link href="/mobile/css/swiper.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/mobile/css/font-awesome-4.7.0/css/font-awesome.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/js/moment.js"></script>
    <script src="/js/moment-with-locales.min.js"></script>
    <script src="/js/newFB.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<?php include("menu-mobile.php"); ?>

<div class="container container-carousel-hothits">
    <div class="row">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <a class="link_wrap" href="/mobile/hothits-video-info.php"></a>
                    <div class="swiper-slide-thumbnal">
                        <img src="/images/cliphot/simple01_.jpg">
                    </div>
                    <div class="swiper-slide-caption">
                        <p>ไฮไลท์ กระชับมิตร Atalanta VS Chiasso</p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <a class="link_wrap" href="/mobile/hothits-video-info.php"></a>
                    <div class="swiper-slide-thumbnal">
                        <img src="/images/cliphot/simple02_.jpg">
                    </div>
                    <div class="swiper-slide-caption">
                        <p>ไฮไลท์ กระชับมิตร Atalanta VS Chiasso</p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <a class="link_wrap" href="/mobile/hothits-video-info.php"></a>
                    <div class="swiper-slide-thumbnal">
                        <img src="/images/cliphot/simple03_.jpg">
                    </div>
                    <div class="swiper-slide-caption">
                        <p>ไฮไลท์ กระชับมิตร Atalanta VS Chiasso</p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <a class="link_wrap" href="/mobile/hothits-video-info.php"></a>
                    <div class="swiper-slide-thumbnal">
                        <img src="/images/cliphot/simple04_.jpg">
                    </div>
                    <div class="swiper-slide-caption">
                        <p>ไฮไลท์ กระชับมิตร Atalanta VS Chiasso</p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <a class="link_wrap" href="/mobile/hothits-video-info.php"></a>
                    <div class="swiper-slide-thumbnal">
                        <img src="/images/cliphot/simple05_.jpg">
                    </div>
                    <div class="swiper-slide-caption">
                        <p>ไฮไลท์ กระชับมิตร Atalanta VS Chiasso</p>
                    </div>
                </div>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
        </div>
    </div>

</div>


<!--    Recent-->
<div class="container">
    <div class="content-recent">
        <div class="text-title">
            <h4>Recent</h4>
        </div>

        <div class="content-item-recent">
            <a class="link_wrap" href="/mobile/hothits-video-info.php"></a>
            <div class="item-recent">
                <div><img src="/images/cliphot/simple01_.jpg"></div>
                <div class="bx-2">
                    <h4 class="media-heading">ไฮไลท์ กระชับมิตร Atalanta VS Chiasso</h4>
                    <div class="time-box box-inline">4 hours ago</div>
                    <div class="vertical-line-between box-inline"></div>
                    <div class="view-box box-inline">20</div>
                </div>
            </div>
        </div>
        <div class="content-item-recent">
            <a class="link_wrap" href="/mobile/hothits-video-info.php"></a>
            <div class="item-recent">
                <div><img src="/images/cliphot/simple02_.jpg"></div>
                <div class="bx-2">
                    <h4 class="media-heading">ไฮไลท์ กระชับมิตร Atalanta VS Chiasso</h4>
                    <div class="time-box box-inline">4 hours ago</div>
                    <div class="vertical-line-between box-inline"></div>
                    <div class="view-box box-inline">20</div>
                </div>
            </div>
        </div>
        <div class="content-item-recent">
            <a class="link_wrap" href="/mobile/hothits-video-info.php"></a>
            <div class="item-recent">
                <div><img src="/images/cliphot/simple03_.jpg"></div>
                <div class="bx-2">
                    <h4 class="media-heading">ไฮไลท์ กระชับมิตร Atalanta VS Chiasso</h4>
                    <div class="time-box box-inline">4 hours ago</div>
                    <div class="vertical-line-between box-inline"></div>
                    <div class="view-box box-inline">20</div>
                </div>
            </div>
        </div>
        <div class="content-item-recent">
            <a class="link_wrap" href="/mobile/hothits-video-info.php"></a>
            <div class="item-recent">
                <div><img src="/images/cliphot/simple04_.jpg"></div>
                <div class="bx-2">
                    <h4 class="media-heading">ไฮไลท์ กระชับมิตร Atalanta VS Chiasso</h4>
                    <div class="time-box box-inline">4 hours ago</div>
                    <div class="vertical-line-between box-inline"></div>
                    <div class="view-box box-inline">20</div>
                </div>
            </div>
        </div>
        <div class="content-item-recent">
            <a class="link_wrap" href="/mobile/hothits-video-info.php"></a>
            <div class="item-recent">
                <div><img src="/images/cliphot/simple05_.jpg"></div>
                <div class="bx-2">
                    <h4 class="media-heading">ไฮไลท์ กระชับมิตร Atalanta VS Chiasso</h4>
                    <div class="time-box box-inline">4 hours ago</div>
                    <div class="vertical-line-between box-inline"></div>
                    <div class="view-box box-inline">20</div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include("footer.php"); ?>
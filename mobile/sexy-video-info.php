<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/mobile/css/all-style.css" rel="stylesheet">
    <link href="/mobile/css/article.css" rel="stylesheet">
    <link href="/mobile/css/bootstrap.css" rel="stylesheet">
    <link href="/mobile/css/docs.css" rel="stylesheet">
    <link href="/mobile/css/game.css" rel="stylesheet">
    <link href="/mobile/css/highlight.css" rel="stylesheet">
    <link href="/mobile/css/index.css" rel="stylesheet">
    <link href="/mobile/css/live-football.css" rel="stylesheet">
    <link href="/mobile/css/news.css" rel="stylesheet">
    <link href="/mobile/css/profile.css" rel="stylesheet">
    <link href="/mobile/css/ranking.css" rel="stylesheet">
    <link href="/mobile/css/review.css" rel="stylesheet">
    <link href="/mobile/css/livescore-mobile.css" rel="stylesheet">
    <link href="/mobile/css/football-symbol-mobile.css" rel="stylesheet">
    <link href="/mobile/css/swiper.min.css" rel="stylesheet">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script src="https://connect.facebook.net/en_US/sdk.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="/mobile/js/login.js"></script>
    <script src="/mobile/js/util.js"></script>

    <script>
        var fbplayer = "https://www.facebook.com/v2.3/plugins/video.php?allowfullscreen=false&autoplay=false&href=";
        var embedlink = "https://www.youtube.com/embed/";
        $(document).ready(function () {
            var frame = $('#player-frame');
            var oldlink = $(frame).attr('src');
            if ($(frame).attr('vtype') == 'youtube') {
                var vid = getYoutubeId(oldlink);
                $(frame).attr('src', embedlink + vid);
            } else if ($(frame).attr('vtype') == 'facebook') {
                $(frame).attr('src', fbplayer + oldlink);
            }
        });
    </script>
</head>
<?php
require_once __DIR__ . '/../bootstrap.php';
$gid = $_REQUEST['gid'];
if (!isset($gid)) {
    header("Location: /mobile/sexy-football");
}

$mcon = new \controller\MediaController();
$gall = $mcon->getMedia($gid);

$host = 'https://ngoal.com';
?>
<body>
<?php include("menu-mobile.php"); ?>

<div class="container-video">
    <div class="container">
        <div class="row">
            <div class="feature-news">
                <iframe id="player-frame" vtype="<?php echo $gall->cover->typeVideo; ?>" width="560" height="315"
                        src="<?php echo $gall->cover->path; ?>"
                        frameborder="0"
                        allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="container container-title-news-info">
        <h2><?php echo $gall->title; ?></h2>
    </div>
    <div class="container">
        <div class="time-box box-inline">4 hours ago</div>
        <div class="vertical-line-between box-inline"></div>
        <div class="view-box box-inline"><?php echo $gall->view; ?></div>
        <div class="container-share-news box-inline pull-right">
            <span class="ft-share-box">Share</span>
            <span class="gp-share-box">Share</span>
        </div>
    </div>

    <!--    Recent-->
    <div class="container">
        <div class="content-recent">
            <div class="text-title">
                <a href="/mobile/sexy-football.php">
                    <h4>Recent</h4>
                    <p>More</p>
                </a>
            </div>
            <div class="content-item-recent">
                <a href="/mobile/sexy-picpost-info.php"></a>
                <div class="item-recent item-recent-sexy">
                    <div><img src="/images/gallery/1.jpg"></div>
                    <div class="bx-2">
                        <h4 class="media-heading">Airpod Shake Test by Cup E</h4>
                        <div class="time-box box-inline">4 hours ago</div>
                        <div class="vertical-line-between box-inline"></div>
                        <div class="view-box box-inline">20</div>
                    </div>
                </div>
            </div>
            <div class="content-item-recent">
                <a href="/mobile/sexy-picpost-info.php"></a>
                <div class="item-recent item-recent-sexy">
                    <div><img src="/images/gallery/2.jpg"></div>
                    <div class="bx-2">
                        <h4 class="media-heading">Video Cup E Magazine #109 Jiw</h4>
                        <div class="time-box box-inline">4 hours ago</div>
                        <div class="vertical-line-between box-inline"></div>
                        <div class="view-box box-inline">20</div>
                    </div>

                </div>
            </div>
            <div class="content-item-recent">
                <a href="/mobile/sexy-picpost-info.php"></a>
                <div class="item-recent item-recent-sexy">
                    <div><img src="/images/gallery/3.jpg"></div>
                    <div class="bx-2">
                        <h4 class="media-heading">SHOWPOW {EP.1} l จิ๋ว 600 CC.l 18++ l Central plaza korat l Dadad
                            Market</h4>
                        <div class="time-box box-inline">4 hours ago</div>
                        <div class="vertical-line-between box-inline"></div>
                        <div class="view-box box-inline">20</div>
                    </div>
                </div>
            </div>
            <div class="content-item-recent">
                <a href="/mobile/sexy-picpost-info.php"></a>
                <div class="item-recent item-recent-sexy">
                    <div><img src="/images/gallery/4.jpg"></div>
                    <div class="bx-2">
                        <h4 class="media-heading">Cup E Magazine #115</h4>
                        <div class="time-box box-inline">4 hours ago</div>
                        <div class="vertical-line-between box-inline"></div>
                        <div class="view-box box-inline">20</div>
                    </div>
                </div>
            </div>
            <div class="content-item-recent">
                <a href="/mobile/sexy-picpost-info.php"></a>
                <div class="item-recent item-recent-sexy">
                    <div><img src="/images/gallery/5.jpg"></div>
                    <div class="bx-2">
                        <h4 class="media-heading">Airpod Shake Test by Cup E</h4>
                        <div class="time-box box-inline">4 hours ago</div>
                        <div class="vertical-line-between box-inline"></div>
                        <div class="view-box box-inline">20</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include("footer.php"); ?>

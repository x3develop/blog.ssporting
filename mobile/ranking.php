<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/mobile/css/all-style.css" rel="stylesheet">
    <link href="/mobile/css/article.css" rel="stylesheet">
    <link href="/mobile/css/bootstrap.css" rel="stylesheet">
    <link href="/mobile/css/docs.css" rel="stylesheet">
    <link href="/mobile/css/game.css" rel="stylesheet">
    <link href="/mobile/css/highlight.css" rel="stylesheet">
    <link href="/mobile/css/index.css" rel="stylesheet">
    <link href="/mobile/css/live-football.css" rel="stylesheet">
    <link href="/mobile/css/news.css" rel="stylesheet">
    <link href="/mobile/css/profile.css" rel="stylesheet">
    <link href="/mobile/css/ranking.css" rel="stylesheet">
    <link href="/mobile/css/review.css" rel="stylesheet">
    <link href="/mobile/css/livescore-mobile.css" rel="stylesheet">
    <link href="/mobile/css/football-symbol-mobile.css" rel="stylesheet">
    <link href="/mobile/css/swiper.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/mobile/css/font-awesome-4.7.0/css/font-awesome.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/js/moment.js"></script>
    <script src="/js/moment-with-locales.min.js"></script>
    <script src="/js/newFB.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<?php include("menu-mobile.php"); ?>

<!--Top Ranking-->
<div class="container">
    <div class="text-title">
        <a class="link_wrap" href="ranking-all.php"></a>
            <h4><img src="/images/trophy/300150.png">Top Ranking</h4>
            <p>ทั้งหมด</p>
    </div>
    <div class="container-top-ranking">

        <!-- Nav tabs -->
<!--        nav-pills-->
        <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="active"><a href="#allTime_topRanking"  role="tab" data-toggle="tab">All Time</a></li>
            <li role="presentation"><a href="#now_topRanking" role="tab" data-toggle="tab">Now</a></li>
            <li role="presentation"><a href="#lastChamp_topRanking" aria-controls="messages" role="tab" data-toggle="tab">Last Champ</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="allTime_topRanking">
                <table class="table table-striped">
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>PTA</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td><img src="/images/p1.png"></td>
                        <td><p>Rittirong Nimcharone</p></td>
                        <td>100.00</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td><img src="/images/bg-tophead.jpg"></td>
                        <td><p>esdakron Bounlong</p></td>
                        <td>95.00</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td><img src="/images/9.jpg"></td>
                        <td><p>Pataradanai Suanti</p></td>
                        <td>72.00</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td><img src="/images/2.jpg"></td>
                        <td><p>พัชรี ชูชาติ</p></td>
                        <td>63.00</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td><img src="/images/6.jpg"></td>
                        <td><p>Natchanon Karkingphai</p></td>
                        <td>60.00</td>
                    </tr>

                </table>
                <div class="text-center">
                    <a class="btn btn-default" href="ranking-all.php" role="button">ดูทั้งหมด</a>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="now_topRanking">
                <table class="table table-striped">
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>PTA</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td><img src="/images/p1.png"></td>
                        <td><p>Rittirong Nimcharone</p></td>
                        <td>100.00</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td><img src="/images/bg-tophead.jpg"></td>
                        <td><p>esdakron Bounlong</p></td>
                        <td>95.00</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td><img src="/images/9.jpg"></td>
                        <td><p>Pataradanai Suanti</p></td>
                        <td>72.00</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td><img src="/images/2.jpg"></td>
                        <td><p>พัชรี ชูชาติ</p></td>
                        <td>63.00</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td><img src="/images/6.jpg"></td>
                        <td><p>Natchanon Karkingphai</p></td>
                        <td>60.00</td>
                    </tr>

                </table>
                <div class="text-center">
                    <a class="btn btn-default" href="ranking-all.php" role="button">ดูทั้งหมด</a>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="lastChamp_topRanking">
                <table class="table table-striped">
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>PTA</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td><img src="/images/p1.png"></td>
                        <td><p>Rittirong Nimcharone</p></td>
                        <td>100.00</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td><img src="/images/bg-tophead.jpg"></td>
                        <td><p>esdakron Bounlong</p></td>
                        <td>95.00</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td><img src="/images/9.jpg"></td>
                        <td><p>Pataradanai Suanti</p></td>
                        <td>72.00</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td><img src="/images/2.jpg"></td>
                        <td><p>พัชรี ชูชาติ</p></td>
                        <td>63.00</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td><img src="/images/6.jpg"></td>
                        <td><p>Natchanon Karkingphai</p></td>
                        <td>60.00</td>
                    </tr>

                </table>
                <div class="text-center">
                    <a class="btn btn-default" href="ranking-all.php" role="button">ดูทั้งหมด</a>
                </div>
            </div>
        </div>

    </div>
</div>

<!--Top Sgold-->
<div class="container" style="margin: 20px 0;">
    <div class="text-title">
        <a class="link_wrap" href="ranking-all.php"></a>
            <h4><img src="/images/trophy/000150.png">Top Sgold</h4>
            <p>ทั้งหมด</p>
    </div>
    <div class="container-top-ranking">

        <!-- Nav tabs -->
        <!--        nav-pills-->
        <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="active"><a href="#allTime_topRanking"  role="tab" data-toggle="tab">All Time</a></li>
            <li role="presentation"><a href="#now_topRanking" role="tab" data-toggle="tab">Now</a></li>
            <li role="presentation"><a href="#lastChamp_topRanking" aria-controls="messages" role="tab" data-toggle="tab">Last Champ</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="allTime_topRanking">
                <table class="table table-striped">
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>PTA</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td><img src="/images/p1.png"></td>
                        <td><p>Rittirong Nimcharone</p></td>
                        <td>3,687.00</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td><img src="/images/bg-tophead.jpg"></td>
                        <td><p>esdakron Bounlong</p></td>
                        <td>3,011.00</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td><img src="/images/9.jpg"></td>
                        <td><p>Pataradanai Suanti</p></td>
                        <td>3,011.00</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td><img src="/images/2.jpg"></td>
                        <td><p>พัชรี ชูชาติ</p></td>
                        <td>2,608.00</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td><img src="/images/6.jpg"></td>
                        <td><p>Natchanon Karkingphai</p></td>
                        <td>2,448.00</td>
                    </tr>
                </table>
                <div class="text-center">
                    <a class="btn btn-default" href="ranking-all.php" role="button">ดูทั้งหมด</a>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="now_topRanking">
                <table class="table table-striped">
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>PTA</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td><img src="/images/p1.png"></td>
                        <td><p>Rittirong Nimcharone</p></td>
                        <td>3,687.00</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td><img src="/images/bg-tophead.jpg"></td>
                        <td><p>esdakron Bounlong</p></td>
                        <td>3,011.00</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td><img src="/images/9.jpg"></td>
                        <td><p>Pataradanai Suanti</p></td>
                        <td>3,011.00</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td><img src="/images/2.jpg"></td>
                        <td><p>พัชรี ชูชาติ</p></td>
                        <td>2,608.00</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td><img src="/images/6.jpg"></td>
                        <td><p>Natchanon Karkingphai</p></td>
                        <td>2,448.00</td>
                    </tr>
                </table>
                <div class="text-center">
                    <a class="btn btn-default" href="ranking-all.php" role="button">ดูทั้งหมด</a>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="lastChamp_topRanking">
                <table class="table table-striped">
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>PTA</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td><img src="/images/p1.png"></td>
                        <td><p>Rittirong Nimcharone</p></td>
                        <td>3,687.00</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td><img src="/images/bg-tophead.jpg"></td>
                        <td><p>esdakron Bounlong</p></td>
                        <td>3,011.00</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td><img src="/images/9.jpg"></td>
                        <td><p>Pataradanai Suanti</p></td>
                        <td>3,011.00</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td><img src="/images/2.jpg"></td>
                        <td><p>พัชรี ชูชาติ</p></td>
                        <td>2,608.00</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td><img src="/images/6.jpg"></td>
                        <td><p>Natchanon Karkingphai</p></td>
                        <td>2,448.00</td>
                    </tr>
                </table>
                <div class="text-center">
                    <a class="btn btn-default" href="ranking-all.php" role="button">ดูทั้งหมด</a>
                </div>
            </div>
        </div>

    </div>
</div>

<!--Top ทรรศนะ-->
<div class="container" style="margin-bottom: 20px;">
    <div class="text-title">
        <a class="link_wrap" href="ranking-all.php"></a>
            <h4><img src="/images/logo/fav-ngoal.png">Top ทรรศนะ</h4>
            <p>ทั้งหมด</p>
    </div>
    <div class="container-top-ranking">

        <!-- Nav tabs -->
        <!--        nav-pills-->
        <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="active"><a href="#Top_Professional"  role="tab" data-toggle="tab">Top Professional</a></li>
            <li role="presentation"><a href="#Top_Players" role="tab" data-toggle="tab">Top Players</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="Top_Professional">
                <table class="table table-striped">
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>PTA</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td><img src="/images/p1.png"></td>
                        <td><p>Rittirong Nimcharone</p></td>
                        <td>60.00%</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td><img src="/images/bg-tophead.jpg"></td>
                        <td><p>esdakron Bounlong</p></td>
                        <td>52.00%</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td><img src="/images/9.jpg"></td>
                        <td><p>Pataradanai Suanti</p></td>
                        <td>50.00%</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td><img src="/images/2.jpg"></td>
                        <td><p>พัชรี ชูชาติ</p></td>
                        <td>47.00%</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td><img src="/images/6.jpg"></td>
                        <td><p>Natchanon Karkingphai</p></td>
                        <td>40.00%</td>
                    </tr>
                </table>
                <div class="text-center">
                    <a class="btn btn-default" href="ranking-all.php" role="button">ดูทั้งหมด</a>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="Top_Players">
                <table class="table table-striped">
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>PTA</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td><img src="/images/p1.png"></td>
                        <td><p>Rittirong Nimcharone</p></td>
                        <td>60.00%</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td><img src="/images/bg-tophead.jpg"></td>
                        <td><p>esdakron Bounlong</p></td>
                        <td>52.00%</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td><img src="/images/9.jpg"></td>
                        <td><p>Pataradanai Suanti</p></td>
                        <td>50.00%</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td><img src="/images/2.jpg"></td>
                        <td><p>พัชรี ชูชาติ</p></td>
                        <td>47.00%</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td><img src="/images/6.jpg"></td>
                        <td><p>Natchanon Karkingphai</p></td>
                        <td>40.00%</td>
                    </tr>
                </table>
                <div class="text-center">
                    <a class="btn btn-default" href="ranking-all.php" role="button">ดูทั้งหมด</a>
                </div>
            </div>
        </div>

    </div>
</div>

<?php include("footer.php"); ?>
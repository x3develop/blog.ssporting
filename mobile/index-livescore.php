<style>
    body {
        background-color: #fff !important;
    }

    .active-nf-livescore .nf-livescore:before,
    .active-nf-livescore span {
        color: #fff !important;
    }
</style>


<?php
include_once __DIR__ . "/../bootstrap.php";

use Carbon\Carbon;

$mcon = new \controller\MatchController();
$today = Carbon::now("UTC");
$today->setTimezone("Asia/Bangkok");
$yesterday = $today->copy();
$yesterday->subDay();
$mlist = $mcon->getAllLSortMobile($today->toDateString());
$ylist = $mcon->getAllLSortMobile($yesterday->toDateString());
$rsicon = ["w" => "win-status", "d" => "draw-status", "l" => "lose-status"];
?>


<div class="container-livescore">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs nav-liveScore" role="tablist" data-spy="affix" data-offset-top="80">
        <li role="presentation"><a href="#Result" aria-controls="home" role="tab" data-toggle="tab">Result</a></li>
        <li role="presentation" class="active"><a href="#Today_live" aria-controls="profile" role="tab"
                                                  data-toggle="tab">Today <span>(15)</span></a></li>
        <li role="presentation"><a class="tab-live-top" href="#Tap_live" aria-controls="messages" role="tab"
                                   data-toggle="tab">Live <span>(5)</span></a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content tab-content-livescore">
        <div role="tabpanel" class="tab-pane" id="Result">
            <div class="content-result-livescore">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-tabs-result bt-flex" role="tablist">
                    <li role="presentation" class="active"><a href="#result-today" aria-controls="home" role="tab"
                                                              data-toggle="tab">Today</a></li>
                    <li role="presentation"><a href="#result-yesterday" aria-controls="profile" role="tab"
                                               data-toggle="tab">Yesterday</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="result-today">
                        <div class="container">
                            <!--                FT-->
                            <div class="inline-box full-width">
                                <div class="main-live-box full-width inline-box ">
                                    <div class="title-league inline-box full-width">
                                        <img src="/images/five-leagues/premier-league-logo.png">
                                        <h5>Premier league</h5>
                                    </div>
                                    <div class="panel-group inline-box full-width" id="accordion" role="tablist"
                                         aria-multiselectable="true">
                                        <div class="panel row-live-box">
                                            <div class="panel-heading" role="tab" id="headingOne-2">
                                                <a class="collapsed" role="button" data-toggle="collapse"
                                                   data-parent="#accordion"
                                                   href="#collapseOne-2" aria-expanded="true"
                                                   aria-controls="collapseOne-2">
                                                    <div class="end-time">FT</div>
                                                    <div class="home-side-team">
                                                        <div class="pull-right">

                                                            <h5>Arsenal</h5>
                                                            <div class="red-card"></div>
                                                            <img src="/images/logo-team/Arsenal.png">

                                                        </div>
                                                    </div>
                                                    <div class="center-row score-live-ft">
                                                        <ul>
                                                            <li>1</li>
                                                            <li>-</li>
                                                            <li>0</li>
                                                        </ul>
                                                    </div>
                                                    <div class="away-side-team">
                                                        <div class="pull-left">
                                                            <img src="/images/logo-team/Bournemouth.png">
                                                            <div class="red-card"></div>
                                                            <div class="red-card"></div>
                                                            <h5>Bournemouth</h5>

                                                        </div>
                                                    </div>
                                                </a>

                                            </div>
                                            <div id="collapseOne-2" class="panel-collapse collapse " role="tabpanel"
                                                 aria-labelledby="headingOne-2">
                                                <?php include "../mockup/mobile/livescore-information-tab-ft.php"; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php foreach ($mlist['order'] as $lid => $priority) { ?>
                                <div class="main-live-box full-width inline-box league-end-banner hide"
                                     lid="<?php echo $lid; ?>">
                                    <div class="title-league inline-box full-width">
                                        <img src="<?php echo $mlist["data"][$lid]["league"]->getLogo(); ?>">
                                        <h5><?php echo $mlist["data"][$lid]["league"]->name; ?></h5>
                                    </div>
                                    <?php foreach ($mlist['data'][$lid]['matches'] as $key => $match) { ?>
                                        <?php if ($match->state == 4 || $match->state == 13 || $match->state == 15) { ?>
                                            <?php
                                            $mdt = Carbon::createFromFormat("Y-m-d H:i:s", $match->time_match, "Asia/Singapore");
                                            $mdt->setTimezone("Asia/Bangkok");
                                            ?>
                                            <div class="panel-group inline-box full-width match-box" lid="<?php echo $lid; ?>"
                                                 mid="<?php echo $match->id; ?>"
                                                 id="accordion-<?php echo $match->id; ?>" role="tablist"
                                                 aria-multiselectable="true">
                                                <div class="panel row-live-box" mid="<?php echo $match->id; ?>">
                                                    <div class="panel-heading match-expand" mid="<?php echo $match->id; ?>"
                                                         role="tab"
                                                         id="headingOne-<?php echo $match->id; ?>">
                                                        <a class="collapse-toggle collapsed" role="button"
                                                           mid="<?php echo $match->id; ?>"
                                                           data-toggle="collapse"
                                                           data-parent="#accordion-<?php echo $match->id; ?>"
                                                           href="#collapseOne-<?php echo $match->id; ?>" aria-expanded="true"
                                                           aria-controls="collapseOne-<?php echo $match->id; ?>">
                                                            <div class="time-match-default at-minute-mark"
                                                                 mid="<?php echo $match->id; ?>"
                                                                 state="<?php echo $match->state; ?>"
                                                                 at="<?php echo $match->time_match; ?>"
                                                                 ht="<?php echo $match->ht_start; ?>"><?php echo $mdt->format("H:i") ?></div>
                                                            <div class="home-side-team">
                                                                <div class="pull-right">
                                                                    <h5 class="team-home-name" mid="<?php echo $match->id; ?>"
                                                                        tid="<?php echo $match->team_home; ?>"><?php echo $match->teama->name_en; ?></h5>
                                                                    <img id="main-home-logo-<?php echo $match->id; ?>"
                                                                         src="<?php echo $match->teama->getLogo(); ?>">
                                                                </div>
                                                            </div>
                                                            <div class="center-row row-hdp" mid="<?php echo $match->id; ?>">
                                                                <div class="title-center">
                                                                    HDP
                                                                </div>
                                                                <strong class="live-hdp"
                                                                        mid="<?php echo $match->id; ?>" hdp="<?php echo empty($match->hdp) ? "-" : $match->hdp->handicap; ?>" home="<?php echo empty($match->hdp) ? "-" : $match->hdp->home_water_bill; ?>" away="<?php echo empty($match->hdp) ? "-" : $match->hdp->away_water_bill; ?>"><?php echo empty($match->hdp) ? "-" : $match->hdp->handicap; ?></strong>
                                                            </div>
                                                            <div class="center-row main-score score-live hide"
                                                                 mid="<?php echo $match->id; ?>">
                                                                <ul>
                                                                    <li class="live-h-score"
                                                                        mid="<?php echo $match->id; ?>"><?php echo $match->live_score_home ?></li>
                                                                    <li>-</li>
                                                                    <li class="live-a-score"
                                                                        mid="<?php echo $match->id; ?>"><?php echo $match->live_score_away ?></li>
                                                                </ul>
                                                            </div>
                                                            <div class="away-side-team">
                                                                <div class="pull-left">
                                                                    <img id="main-away-logo-<?php echo $match->id; ?>"
                                                                         src="<?php echo $match->teamb->getLogo(); ?>">
                                                                    <h5 class="team-away-name" mid="<?php echo $match->id; ?>"
                                                                        tid="<?php echo $match->team_away; ?>"><?php echo $match->teamb->name_en; ?></h5>
                                                                </div>
                                                            </div>
                                                        </a>

                                                    </div>
                                                    <div id="collapseOne-<?php echo $match->id; ?>"
                                                         class="panel-collapse collapse"
                                                         mid="<?php echo $match->id; ?>" role="tabpanel"
                                                         aria-labelledby="headingOne-<?php echo $match->id; ?>">

                                                        <div class="panel-body">
                                                            <div class="panel-body-status-match inline-box full-width bt-flex">
                                                                <div class="home-status-match text-right">
                                                                    <div class="standing-view inline-box">
                                                                        <ul class="standing-view_last5">
                                                                            <li>Last 5 match</li>
                                                                            <?php foreach (array_reverse(explode(",", $match->lastresult_home)) as $result) { ?>
                                                                                <?php if (array_key_exists($result, $rsicon)) { ?>
                                                                                    <li class="<?php echo $rsicon[$result]; ?>"></li>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </ul>
                                                                        <ul class="standing-view_odd">
                                                                            <li>Odds</li>
                                                                            <?php foreach (array_reverse(explode(",", $match->lastresult_odds_home)) as $result) { ?>
                                                                                <?php if (array_key_exists($result, $rsicon)) { ?>
                                                                                    <li class="<?php echo $rsicon[$result]; ?>"></li>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="standing-view-logo inline-box">
                                                                        <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                        <span></span>
                                                                    </div>
                                                                </div>
                                                                <div class="away-status-match text-left">
                                                                    <div class="standing-view-logo inline-box ">
                                                                        <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                        <span></span>
                                                                    </div>
                                                                    <div class="standing-view inline-box">
                                                                        <ul class="standing-view_last5">
                                                                            <?php foreach (explode(",", $match->lastresult_away) as $result) { ?>
                                                                                <?php if (array_key_exists($result, $rsicon)) { ?>
                                                                                    <li class="<?php echo $rsicon[$result]; ?>"></li>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                            <li>Last 5 match</li>
                                                                        </ul>
                                                                        <ul class="standing-view_odd">
                                                                            <?php foreach (explode(",", $match->lastresult_odds_away) as $result) { ?>
                                                                                <?php if (array_key_exists($result, $rsicon)) { ?>
                                                                                    <li class="<?php echo $rsicon[$result]; ?>"></li>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                            <li>Odds</li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Nav tabs -->
                                                            <ul class="nav nav-tabs-event mtab-select" role="tablist">
                                                                <li role="presentation" class="mtab-header-de view-tab"
                                                                    tag="detail"
                                                                    mid="<?php echo $match->id; ?>"><a
                                                                            href="#detail-tab-<?php echo $match->id; ?>"
                                                                            role="tab"
                                                                            data-toggle="tab">Detail</a></li>
                                                                <li role="presentation" class="mtab-header-ls view-tab"
                                                                    tag="livestat"
                                                                    mid="<?php echo $match->id; ?>"><a
                                                                            href="#statistics-tab-<?php echo $match->id; ?>"
                                                                            role="tab"
                                                                            data-toggle="tab">Live Stats</a></li>
                                                                <li role="presentation" class="mtab-header-h2h view-tab active"
                                                                    tag="h2h"
                                                                    mid="<?php echo $match->id; ?>"><a
                                                                            href="#h2h-tab-<?php echo $match->id; ?>" role="tab"
                                                                            data-toggle="tab">H2H</a></li>
                                                                <li role="presentation" class="mtab-header-st view-tab"
                                                                    tag="formguide"
                                                                    mid="<?php echo $match->id; ?>"><a
                                                                            href="#fg-tab-<?php echo $match->id; ?>" role="tab"
                                                                            data-toggle="tab">Statistics</a></li>
                                                                <?php if ($match->home_formation != "-" && $match->away_formation != "-") { ?>
                                                                    <li role="presentation" class="mtab-header-lu view-tab"
                                                                        tag="lineups"
                                                                        mid="<?php echo $match->id; ?>"><a
                                                                                href="#lineups-tab-<?php echo $match->id; ?>"
                                                                                role="tab"
                                                                                data-toggle="tab">Lineups</a></li>
                                                                <?php } ?>
                                                                <li role="presentation" class="mtab-header-re view-tab"
                                                                    tag="review"
                                                                    mid="<?php echo $match->id; ?>"><a
                                                                            href="#review-tab-<?php echo $match->id; ?>"
                                                                            role="tab"
                                                                            data-toggle="tab">Review</a></li>
                                                            </ul>

                                                            <!-- Tab panes -->
                                                            <div class="tab-content">
                                                                <div role="tabpanel" class="tab-pane mtab-body-de"
                                                                     id="detail-tab-<?php echo $match->id; ?>">
                                                                    <div class="content-event-match"
                                                                         mid="<?php echo $match->id; ?>"></div>

                                                                </div>
                                                                <div role="tabpanel" class="tab-pane mtab-body-ls"
                                                                     id="statistics-tab-<?php echo $match->id; ?>">
                                                                    <div class="full-width inline-box box-statistics statistics-mark"
                                                                         mid="<?php echo $match->id; ?>">
                                                                    </div>
                                                                </div>
                                                                <div role="tabpanel" class="tab-pane mtab-body-h2h active"
                                                                     id="h2h-tab-<?php echo $match->id; ?>">
                                                                    <div class="box-h2h inline-box full-width">

                                                                        <ul class="nav nav-tabs nav-tabs-h2h" role="tablist">
                                                                            <li class="nav-tabs-team">
                                                                                <div class="pull-right">
                                                                                    <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                                </div>
                                                                                <div class="pull-right text-right">
                                                                                    <h4><?php echo $match->teama->name_en; ?></h4>
                                                                                </div>
                                                                            </li>
                                                                            <li class="nav-tabs-team">
                                                                                <div class="pull-left">
                                                                                    <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                                </div>
                                                                                <div class="pull-left text-left">
                                                                                    <h4><?php echo $match->teamb->name_en; ?></h4>
                                                                                </div>
                                                                            </li>
                                                                        </ul>

                                                                        <div class="data-h2h full-width inline-box text-center">

                                                                            <ul>
                                                                                <li>
                                                                            <span class="h2h-win-summary"
                                                                                  mid="<?php echo $match->id ?>">0</span>
                                                                                    <small>Win</small>
                                                                                </li>
                                                                                <li>
                                                                            <span class="h2h-draw-summary"
                                                                                  mid="<?php echo $match->id ?>">0</span>
                                                                                    <small>Draw</small>
                                                                                </li>
                                                                                <li>
                                                                            <span class="h2h-lose-summary"
                                                                                  mid="<?php echo $match->id ?>">0</span>
                                                                                    <small>Lose</small>
                                                                                </li>
                                                                            </ul>
                                                                        </div>

                                                                        <div class="tab-content">
                                                                            <div role="tabpanel"
                                                                                 class="tab-pane active h2h-view-box"
                                                                                 mid="<?php echo $match->id; ?>">

                                                                            </div>
                                                                        </div>


                                                                    </div>

                                                                </div>
                                                                <div role="tabpanel" class="tab-pane mtab-body-st"
                                                                     id="fg-tab-<?php echo $match->id; ?>">
                                                                    <div class="box-h2h box-statistics-mobile inline-box full-width">
                                                                        <ul class="nav nav-tabs" role="tablist">
                                                                            <li role="presentation"
                                                                                class="nav-tabs-team active">
                                                                                <a href="#statistic-home-<?php echo $match->id; ?>"
                                                                                   role="tab" data-toggle="tab">
                                                                                    <div class="pull-right">
                                                                                        <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                                    </div>
                                                                                    <div class="pull-right text-right">
                                                                                        <h4><?php echo $match->teama->name_en; ?></h4>
                                                                                        <ul>
                                                                                            <li>
                                                                                        <span class="fg-home-win"
                                                                                              mid="<?php echo $match->id; ?>">0</span>
                                                                                                <small>Win</small>
                                                                                            </li>
                                                                                            <li>
                                                                                        <span class="fg-home-draw"
                                                                                              mid="<?php echo $match->id; ?>">0</span>
                                                                                                <small>Draw</small>
                                                                                            </li>
                                                                                            <li>
                                                                                        <span class="fg-home-lose"
                                                                                              mid="<?php echo $match->id; ?>">0</span>
                                                                                                <small>Lose</small>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>

                                                                                </a>
                                                                            </li>
                                                                            <li class="nav-tabs-team" role="presentation">
                                                                                <a href="#statistic-away-<?php echo $match->id; ?>"
                                                                                   role="tab" data-toggle="tab">
                                                                                    <div class="pull-left">
                                                                                        <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                                    </div>
                                                                                    <div class="pull-left text-left">
                                                                                        <h4><?php echo $match->teamb->name_en; ?></h4>
                                                                                        <ul>
                                                                                            <li>
                                                                                        <span class="fg-away-win"
                                                                                              mid="<?php echo $match->id; ?>">0</span>
                                                                                                <small>Win</small>
                                                                                            </li>
                                                                                            <li>
                                                                                        <span class="fg-away-draw"
                                                                                              mid="<?php echo $match->id; ?>">0</span>
                                                                                                <small>Draw</small>
                                                                                            </li>
                                                                                            <li>
                                                                                        <span class="fg-away-lose"
                                                                                              mid="<?php echo $match->id; ?>">0</span>
                                                                                                <small>Lose</small>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                        </ul>

                                                                        <!-- Tab panes -->
                                                                        <div class="tab-content">
                                                                            <div role="tabpanel"
                                                                                 class="tab-pane active st-home-view-box"
                                                                                 mid="<?php echo $match->id; ?>"
                                                                                 id="statistic-home-<?php echo $match->id; ?>">

                                                                            </div>
                                                                            <div role="tabpanel"
                                                                                 class="tab-pane fade st-away-view-box"
                                                                                 mid="<?php echo $match->id; ?>"
                                                                                 id="statistic-away-<?php echo $match->id; ?>">

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div role="tabpanel" class="tab-pane mtab-body-lu"
                                                                     id="lineups-tab-<?php echo $match->id; ?>">
                                                                    <div id="lineups-box-<?php echo $match->id; ?>"
                                                                         class="box-lineup-m inline-box full-width">

                                                                    </div>

                                                                </div>
                                                                <div role="tabpanel"
                                                                     class="tab-pane tab-pane-review mtab-body-re"
                                                                     id="review-tab-<?php echo $match->id; ?>">
                                                                    <div class="content-vote-review container">
                                                                        <?php if (!empty($match->hdp)) { ?>
                                                                            <ul class="nav-review full-width inline-box">
                                                                                <li class="nav-review-home bet-modal" side="home" mid="<?php echo $match->id; ?>"
                                                                                    data-toggle="modal"
                                                                                    data-target="#myModalHome">
                                                                                    <a href="javascript:void(0)">
                                                                                        <div class="full-width text-center">
                                                                                            <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                                        </div>
                                                                                        <div class="full-width text-center">
                                                                                            <button type="button"  class="btn btn-primary bet-btn" side="home" mid="<?php echo $match->id; ?>">
                                                                                                <?php echo number_format($match->hdp->home_water_bill, 2); ?>
                                                                                            </button>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="HDP-box-center">
                                                                                        <div class="time-box match-time"
                                                                                             at="<?php echo $match->time_match ?>"><?php echo $mdt->format("H:i"); ?></div>
                                                                                        <strong>HDP</strong>
                                                                                        <h2 class="live-hdp-play"
                                                                                            mid="<?php echo $match->id; ?>"><?php echo $match->hdp->handicap; ?></h2>
                                                                                        <div class="box-water-text hide">
                                                                                            <div>1.9</div>
                                                                                            <div>1.69</div>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                                <li class="nav-review-away bet-modal" side="away" mid="<?php echo $match->id; ?>" data-toggle="modal"
                                                                                    data-target="#myModalAway">
                                                                                    <a href="javascript:void(0)">
                                                                                        <div class="full-width text-center">
                                                                                            <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                                        </div>
                                                                                        <div class="full-width text-center">
                                                                                            <button type="button" class="btn btn-success bet-btn" side="away" mid="<?php echo $match->id; ?>">
                                                                                                <?php echo number_format($match->hdp->away_water_bill, 2); ?>
                                                                                            </button>
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        <?php } else { ?>
                                                                            <div class="box-vote-win full-width inline-box text-center">
                                                                                <div class="title-center">
                                                                                    โหวดผลการแข่งขัน
                                                                                </div>
                                                                                <div class="box-vote-win-btn inline-box">
                                                                                    <div class="btn-group full-width"
                                                                                         role="group"
                                                                                         aria-label="...">
                                                                                        <button type="button"
                                                                                                class="btn btn-primary vote-match"
                                                                                                mid="<?php echo $match->id; ?>"
                                                                                                side="home">
                                                                                            Home
                                                                                        </button>
                                                                                        <button type="button"
                                                                                                class="btn btn-default vote-match"
                                                                                                mid="<?php echo $match->id; ?>"
                                                                                                side="draw">
                                                                                            Draw
                                                                                        </button>
                                                                                        <button type="button"
                                                                                                class="btn btn-success vote-match"
                                                                                                mid="<?php echo $match->id; ?>"
                                                                                                side="away">
                                                                                            Away
                                                                                        </button>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="box-progress vote-win full-width inline-box text-center match-vote-bar hide"
                                                                                 mid="<?php echo $match->id; ?>">
                                                                                <div class="progress">
                                                                                    <div class="progress-bar progress-bar-primary vote-bar-home"
                                                                                         mid="<?php echo $match->id; ?>">
                                                                                        0%
                                                                                    </div>
                                                                                    <div class="progress-bar progress-bar-draw vote-bar-draw"
                                                                                         mid="<?php echo $match->id; ?>">
                                                                                        0%
                                                                                    </div>
                                                                                    <div class="progress-bar progress-bar-success vote-bar-away"
                                                                                         mid="<?php echo $match->id; ?>">
                                                                                        0%
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="box-review-simple inline-box">
                                                                        <a href="review-info?mid=<?php echo $match->id; ?>"
                                                                           class="link_wrap"></a>
                                                                        <div class="review-list"
                                                                             mid="<?php echo $match->id; ?>">

                                                                        </div>
                                                                        <div class="inline-box full-width text-center read-review-more"
                                                                             mid="<?php echo $match->id; ?>">
                                                                            <a class="btn btn-primary btn-sm" href="#"
                                                                               role="button">ดูทรรศนะทั้งหมด</a>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="result-yesterday">
                        <div class="container">

                        </div>
                    </div>
                </div>

            </div>

        </div>


        <div role="tabpanel" class="tab-pane active" id="Today_live">
            <div class="container">
                <div class="topic-livescore bt-flex full-width">
                    <h2>
                        <small>12/12/2019</small>
                    </h2>
                    <div class="form-group">
                        <div class="pull-right">
                            <span>Odd</span>
                            <div class="checkbox checbox-switch switch-primary">
                                <label>
                                    <input type="checkbox" name="" checked=""/>
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <!--                Live Score-->
                <div class="status-live inline-box full-width">
                    <div class="main-live-box full-width inline-box">
                        <div class="title-league inline-box full-width">
                            <img src="/images/five-leagues/premier-league-logo.png">
                            <h5>Premier league2</h5>
                        </div>
                        <div class="panel-group inline-box full-width" id="accordion" role="tablist"
                             aria-multiselectable="true">
                            <div class="panel row-live-box">
                                <div class="panel-heading" role="tab" id="headingOne-1">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseOne-1" aria-expanded="true" aria-controls="collapseOne-1">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <h5>Arsenal</h5>
                                                <div class="red-card"></div>
                                                <img src="/images/logo-team/Arsenal.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>0</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Bournemouth.png">
                                                <h5>Bournemouth</h5>
                                            </div>
                                        </div>
                                    </a>

                                </div>
                                <div id="collapseOne-1" class="panel-collapse collapse " role="tabpanel"
                                     aria-labelledby="headingOne-1">
                                    <?php include "../mockup/mobile/livescore-information-tab.php"; ?>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading" role="tab" id="headingOne-2">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseOne-2" aria-expanded="true" aria-controls="collapseOne-2">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">

                                                <h5>Arsenal</h5>
                                                <div class="red-card"></div>
                                                <img src="/images/logo-team/Arsenal.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>0</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Bournemouth.png">
                                                <h5>Bournemouth</h5>
                                            </div>
                                        </div>
                                    </a>

                                </div>
                                <div id="collapseOne-2" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingOne-2">

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php foreach ($mlist['order'] as $lid => $priority) { ?>
                        <div class="main-live-box full-width inline-box league-live-banner hide"
                             lid="<?php echo $lid; ?>">
                            <div class="title-league inline-box full-width">
                                <img src="<?php echo $mlist["data"][$lid]["league"]->getLogo(); ?>">
                                <h5><?php echo $mlist["data"][$lid]["league"]->name; ?></h5>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <!--                None Live-->
                <div class="inline-box full-width">
                    <?php foreach ($mlist['order'] as $lid => $priority) { ?>
                        <div class="main-live-box full-width inline-box league-wait-banner" lid="<?php echo $lid; ?>">
                            <div class="title-league inline-box full-width">
                                <img class="league-logo" src="<?php echo $mlist['data'][$lid]['league']->getLogo(); ?>">
                                <h5><?php echo $mlist['data'][$lid]['league']->name; ?></h5>
                            </div>
                            <?php foreach ($mlist['data'][$lid]['matches'] as $key => $match) { ?>
                                <?php if ($match->state != 4 && $match->state != 13 && $match->state != 15) { ?>
                                    <?php
                                    $mdt = Carbon::createFromFormat("Y-m-d H:i:s", $match->time_match, "Asia/Singapore");
                                    $mdt->setTimezone("Asia/Bangkok");
                                    ?>
                                    <div class="panel-group inline-box full-width match-box" lid="<?php echo $lid; ?>"
                                         mid="<?php echo $match->id; ?>"
                                         id="accordion-<?php echo $match->id; ?>" role="tablist"
                                         aria-multiselectable="true">
                                        <div class="panel row-live-box" mid="<?php echo $match->id; ?>">
                                            <div class="panel-heading match-expand" mid="<?php echo $match->id; ?>"
                                                 role="tab"
                                                 id="headingOne-<?php echo $match->id; ?>">
                                                <a class="collapse-toggle collapsed" role="button"
                                                   mid="<?php echo $match->id; ?>"
                                                   data-toggle="collapse"
                                                   data-parent="#accordion-<?php echo $match->id; ?>"
                                                   href="#collapseOne-<?php echo $match->id; ?>" aria-expanded="true"
                                                   aria-controls="collapseOne-<?php echo $match->id; ?>">
                                                    <div class="time-match-default at-minute-mark"
                                                         mid="<?php echo $match->id; ?>"
                                                         state="<?php echo $match->state; ?>"
                                                         at="<?php echo $match->time_match; ?>"
                                                         ht="<?php echo $match->ht_start; ?>"><?php echo $mdt->format("H:i") ?></div>
                                                    <div class="home-side-team">
                                                        <div class="pull-right">
                                                            <h5 class="team-home-name" mid="<?php echo $match->id; ?>"
                                                                tid="<?php echo $match->team_home; ?>"><?php echo $match->teama->name_en; ?></h5>
                                                            <img id="main-home-logo-<?php echo $match->id; ?>"
                                                                 src="<?php echo $match->teama->getLogo(); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="center-row row-hdp" mid="<?php echo $match->id; ?>">
                                                        <div class="title-center">
                                                            HDP
                                                        </div>
                                                        <strong class="live-hdp"
                                                                mid="<?php echo $match->id; ?>" hdp="<?php echo empty($match->hdp) ? "-" : $match->hdp->handicap; ?>" home="<?php echo empty($match->hdp) ? "-" : $match->hdp->home_water_bill; ?>" away="<?php echo empty($match->hdp) ? "-" : $match->hdp->away_water_bill; ?>"><?php echo empty($match->hdp) ? "-" : $match->hdp->handicap; ?></strong>
                                                    </div>
                                                    <div class="center-row main-score score-live hide"
                                                         mid="<?php echo $match->id; ?>">
                                                        <ul>
                                                            <li class="live-h-score"
                                                                mid="<?php echo $match->id; ?>"><?php echo $match->live_score_home ?></li>
                                                            <li>-</li>
                                                            <li class="live-a-score"
                                                                mid="<?php echo $match->id; ?>"><?php echo $match->live_score_away ?></li>
                                                        </ul>
                                                    </div>
                                                    <div class="away-side-team">
                                                        <div class="pull-left">
                                                            <img id="main-away-logo-<?php echo $match->id; ?>"
                                                                 src="<?php echo $match->teamb->getLogo(); ?>">
                                                            <h5 class="team-away-name" mid="<?php echo $match->id; ?>"
                                                                tid="<?php echo $match->team_away; ?>"><?php echo $match->teamb->name_en; ?></h5>
                                                        </div>
                                                    </div>
                                                </a>

                                            </div>
                                            <div id="collapseOne-<?php echo $match->id; ?>"
                                                 class="panel-collapse collapse"
                                                 mid="<?php echo $match->id; ?>" role="tabpanel"
                                                 aria-labelledby="headingOne-<?php echo $match->id; ?>">

                                                <div class="panel-body">
                                                    <div class="panel-body-status-match inline-box full-width bt-flex">
                                                        <div class="home-status-match text-right">
                                                            <div class="standing-view inline-box">
                                                                <ul class="standing-view_last5">
                                                                    <li>Last 5 match</li>
                                                                    <?php foreach (array_reverse(explode(",", $match->lastresult_home)) as $result) { ?>
                                                                        <?php if (array_key_exists($result, $rsicon)) { ?>
                                                                            <li class="<?php echo $rsicon[$result]; ?>"></li>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </ul>
                                                                <ul class="standing-view_odd">
                                                                    <li>Odds</li>
                                                                    <?php foreach (array_reverse(explode(",", $match->lastresult_odds_home)) as $result) { ?>
                                                                        <?php if (array_key_exists($result, $rsicon)) { ?>
                                                                            <li class="<?php echo $rsicon[$result]; ?>"></li>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </ul>
                                                            </div>
                                                            <div class="standing-view-logo inline-box">
                                                                <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                        <div class="away-status-match text-left">
                                                            <div class="standing-view-logo inline-box ">
                                                                <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                <span></span>
                                                            </div>
                                                            <div class="standing-view inline-box">
                                                                <ul class="standing-view_last5">
                                                                    <?php foreach (explode(",", $match->lastresult_away) as $result) { ?>
                                                                        <?php if (array_key_exists($result, $rsicon)) { ?>
                                                                            <li class="<?php echo $rsicon[$result]; ?>"></li>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                    <li>Last 5 match</li>
                                                                </ul>
                                                                <ul class="standing-view_odd">
                                                                    <?php foreach (explode(",", $match->lastresult_odds_away) as $result) { ?>
                                                                        <?php if (array_key_exists($result, $rsicon)) { ?>
                                                                            <li class="<?php echo $rsicon[$result]; ?>"></li>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                    <li>Odds</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Nav tabs -->
                                                    <ul class="nav nav-tabs-event mtab-select" role="tablist">
                                                        <li role="presentation" class="mtab-header-de view-tab"
                                                            tag="detail"
                                                            mid="<?php echo $match->id; ?>"><a
                                                                    href="#detail-tab-<?php echo $match->id; ?>"
                                                                    role="tab"
                                                                    data-toggle="tab">Detail</a></li>
                                                        <li role="presentation" class="mtab-header-ls view-tab"
                                                            tag="livestat"
                                                            mid="<?php echo $match->id; ?>"><a
                                                                    href="#statistics-tab-<?php echo $match->id; ?>"
                                                                    role="tab"
                                                                    data-toggle="tab">Live Stats</a></li>
                                                        <li role="presentation" class="mtab-header-h2h view-tab active"
                                                            tag="h2h"
                                                            mid="<?php echo $match->id; ?>"><a
                                                                    href="#h2h-tab-<?php echo $match->id; ?>" role="tab"
                                                                    data-toggle="tab">H2H</a></li>
                                                        <li role="presentation" class="mtab-header-st view-tab"
                                                            tag="formguide"
                                                            mid="<?php echo $match->id; ?>"><a
                                                                    href="#fg-tab-<?php echo $match->id; ?>" role="tab"
                                                                    data-toggle="tab">Statistics</a></li>
                                                        <?php if ($match->home_formation != "-" && $match->away_formation != "-") { ?>
                                                            <li role="presentation" class="mtab-header-lu view-tab"
                                                                tag="lineups"
                                                                mid="<?php echo $match->id; ?>"><a
                                                                        href="#lineups-tab-<?php echo $match->id; ?>"
                                                                        role="tab"
                                                                        data-toggle="tab">Lineups</a></li>
                                                        <?php } ?>
                                                        <li role="presentation" class="mtab-header-re view-tab"
                                                            tag="review"
                                                            mid="<?php echo $match->id; ?>"><a
                                                                    href="#review-tab-<?php echo $match->id; ?>"
                                                                    role="tab"
                                                                    data-toggle="tab">Review</a></li>
                                                    </ul>

                                                    <!-- Tab panes -->
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane mtab-body-de"
                                                             id="detail-tab-<?php echo $match->id; ?>">
                                                            <div class="content-event-match"
                                                                 mid="<?php echo $match->id; ?>"></div>

                                                        </div>
                                                        <div role="tabpanel" class="tab-pane mtab-body-ls"
                                                             id="statistics-tab-<?php echo $match->id; ?>">
                                                            <div class="full-width inline-box box-statistics statistics-mark"
                                                                 mid="<?php echo $match->id; ?>">
                                                            </div>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane mtab-body-h2h active"
                                                             id="h2h-tab-<?php echo $match->id; ?>">
                                                            <div class="box-h2h inline-box full-width">

                                                                <ul class="nav nav-tabs nav-tabs-h2h" role="tablist">
                                                                    <li class="nav-tabs-team">
                                                                        <div class="pull-right">
                                                                            <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                        </div>
                                                                        <div class="pull-right text-right">
                                                                            <h4><?php echo $match->teama->name_en; ?></h4>
                                                                        </div>
                                                                    </li>
                                                                    <li class="nav-tabs-team">
                                                                        <div class="pull-left">
                                                                            <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                        </div>
                                                                        <div class="pull-left text-left">
                                                                            <h4><?php echo $match->teamb->name_en; ?></h4>
                                                                        </div>
                                                                    </li>
                                                                </ul>

                                                                <div class="data-h2h full-width inline-box text-center">

                                                                    <ul>
                                                                        <li>
                                                                            <span class="h2h-win-summary"
                                                                                  mid="<?php echo $match->id ?>">0</span>
                                                                            <small>Win</small>
                                                                        </li>
                                                                        <li>
                                                                            <span class="h2h-draw-summary"
                                                                                  mid="<?php echo $match->id ?>">0</span>
                                                                            <small>Draw</small>
                                                                        </li>
                                                                        <li>
                                                                            <span class="h2h-lose-summary"
                                                                                  mid="<?php echo $match->id ?>">0</span>
                                                                            <small>Lose</small>
                                                                        </li>
                                                                    </ul>
                                                                </div>

                                                                <div class="tab-content">
                                                                    <div role="tabpanel"
                                                                         class="tab-pane active h2h-view-box"
                                                                         mid="<?php echo $match->id; ?>">

                                                                    </div>
                                                                </div>


                                                            </div>

                                                        </div>
                                                        <div role="tabpanel" class="tab-pane mtab-body-st"
                                                             id="fg-tab-<?php echo $match->id; ?>">
                                                            <div class="box-h2h box-statistics-mobile inline-box full-width">
                                                                <ul class="nav nav-tabs" role="tablist">
                                                                    <li role="presentation"
                                                                        class="nav-tabs-team active">
                                                                        <a href="#statistic-home-<?php echo $match->id; ?>"
                                                                           role="tab" data-toggle="tab">
                                                                            <div class="pull-right">
                                                                                <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                            </div>
                                                                            <div class="pull-right text-right">
                                                                                <h4><?php echo $match->teama->name_en; ?></h4>
                                                                                <ul>
                                                                                    <li>
                                                                                        <span class="fg-home-win"
                                                                                              mid="<?php echo $match->id; ?>">0</span>
                                                                                        <small>Win</small>
                                                                                    </li>
                                                                                    <li>
                                                                                        <span class="fg-home-draw"
                                                                                              mid="<?php echo $match->id; ?>">0</span>
                                                                                        <small>Draw</small>
                                                                                    </li>
                                                                                    <li>
                                                                                        <span class="fg-home-lose"
                                                                                              mid="<?php echo $match->id; ?>">0</span>
                                                                                        <small>Lose</small>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>

                                                                        </a>
                                                                    </li>
                                                                    <li class="nav-tabs-team" role="presentation">
                                                                        <a href="#statistic-away-<?php echo $match->id; ?>"
                                                                           role="tab" data-toggle="tab">
                                                                            <div class="pull-left">
                                                                                <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                            </div>
                                                                            <div class="pull-left text-left">
                                                                                <h4><?php echo $match->teamb->name_en; ?></h4>
                                                                                <ul>
                                                                                    <li>
                                                                                        <span class="fg-away-win"
                                                                                              mid="<?php echo $match->id; ?>">0</span>
                                                                                        <small>Win</small>
                                                                                    </li>
                                                                                    <li>
                                                                                        <span class="fg-away-draw"
                                                                                              mid="<?php echo $match->id; ?>">0</span>
                                                                                        <small>Draw</small>
                                                                                    </li>
                                                                                    <li>
                                                                                        <span class="fg-away-lose"
                                                                                              mid="<?php echo $match->id; ?>">0</span>
                                                                                        <small>Lose</small>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                </ul>

                                                                <!-- Tab panes -->
                                                                <div class="tab-content">
                                                                    <div role="tabpanel"
                                                                         class="tab-pane active st-home-view-box"
                                                                         mid="<?php echo $match->id; ?>"
                                                                         id="statistic-home-<?php echo $match->id; ?>">

                                                                    </div>
                                                                    <div role="tabpanel"
                                                                         class="tab-pane fade st-away-view-box"
                                                                         mid="<?php echo $match->id; ?>"
                                                                         id="statistic-away-<?php echo $match->id; ?>">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane mtab-body-lu"
                                                             id="lineups-tab-<?php echo $match->id; ?>">
                                                            <div id="lineups-box-<?php echo $match->id; ?>"
                                                                 class="box-lineup-m inline-box full-width">

                                                            </div>

                                                        </div>
                                                        <div role="tabpanel"
                                                             class="tab-pane tab-pane-review mtab-body-re"
                                                             id="review-tab-<?php echo $match->id; ?>">
                                                            <div class="content-vote-review container">
                                                                <?php if (!empty($match->hdp)) { ?>
                                                                    <ul class="nav-review full-width inline-box">
                                                                        <li class="nav-review-home bet-modal" side="home" mid="<?php echo $match->id; ?>"
                                                                            data-toggle="modal"
                                                                            data-target="#myModalHome">
                                                                            <a href="javascript:void(0)">
                                                                                <div class="full-width text-center">
                                                                                    <img src="<?php echo $match->teama->getLogo(); ?>">
                                                                                </div>
                                                                                <div class="full-width text-center">
                                                                                    <button type="button"  class="btn btn-primary bet-btn" side="home" mid="<?php echo $match->id; ?>">
                                                                                        <?php echo number_format($match->hdp->home_water_bill, 2); ?>
                                                                                    </button>
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <div class="HDP-box-center">
                                                                                <div class="time-box match-time"
                                                                                     at="<?php echo $match->time_match ?>"><?php echo $mdt->format("H:i"); ?></div>
                                                                                <strong>HDP</strong>
                                                                                <h2 class="live-hdp-play"
                                                                                    mid="<?php echo $match->id; ?>"><?php echo $match->hdp->handicap; ?></h2>
                                                                                <div class="box-water-text hide">
                                                                                    <div>1.9</div>
                                                                                    <div>1.69</div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="nav-review-away bet-modal" side="away" mid="<?php echo $match->id; ?>" data-toggle="modal"
                                                                            data-target="#myModalAway">
                                                                            <a href="javascript:void(0)">
                                                                                <div class="full-width text-center">
                                                                                    <img src="<?php echo $match->teamb->getLogo(); ?>">
                                                                                </div>
                                                                                <div class="full-width text-center">
                                                                                    <button type="button" class="btn btn-success bet-btn" side="away" mid="<?php echo $match->id; ?>">
                                                                                        <?php echo number_format($match->hdp->away_water_bill, 2); ?>
                                                                                    </button>
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                <?php } else { ?>
                                                                    <div class="box-vote-win full-width inline-box text-center">
                                                                        <div class="title-center">
                                                                            โหวดผลการแข่งขัน
                                                                        </div>
                                                                        <div class="box-vote-win-btn inline-box">
                                                                            <div class="btn-group full-width"
                                                                                 role="group"
                                                                                 aria-label="...">
                                                                                <button type="button"
                                                                                        class="btn btn-primary vote-match"
                                                                                        mid="<?php echo $match->id; ?>"
                                                                                        side="home">
                                                                                    Home
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="btn btn-default vote-match"
                                                                                        mid="<?php echo $match->id; ?>"
                                                                                        side="draw">
                                                                                    Draw
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="btn btn-success vote-match"
                                                                                        mid="<?php echo $match->id; ?>"
                                                                                        side="away">
                                                                                    Away
                                                                                </button>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-progress vote-win full-width inline-box text-center match-vote-bar hide"
                                                                         mid="<?php echo $match->id; ?>">
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-primary vote-bar-home"
                                                                                 mid="<?php echo $match->id; ?>">
                                                                                0%
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-draw vote-bar-draw"
                                                                                 mid="<?php echo $match->id; ?>">
                                                                                0%
                                                                            </div>
                                                                            <div class="progress-bar progress-bar-success vote-bar-away"
                                                                                 mid="<?php echo $match->id; ?>">
                                                                                0%
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="box-review-simple inline-box">
                                                                <a href="review-info?mid=<?php echo $match->id; ?>"
                                                                   class="link_wrap"></a>
                                                                <div class="review-list"
                                                                     mid="<?php echo $match->id; ?>">

                                                                </div>
                                                                <div class="inline-box full-width text-center read-review-more"
                                                                     mid="<?php echo $match->id; ?>">
                                                                    <a class="btn btn-primary btn-sm" href="#"
                                                                       role="button">ดูทรรศนะทั้งหมด</a>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="Tap_live">
            <div class="container">
                <div class="topic-livescore bt-flex full-width">
                    <h2>Live Score
                        <small>12/12/2019</small>
                    </h2>
                    <div class="form-group">
                        <div class="pull-right">
                            <span>Odd</span>
                            <div class="checkbox checbox-switch switch-primary">
                                <label>
                                    <input type="checkbox" name="" checked=""/>
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <!--                Live Score-->
                <div class="status-live inline-box inline-box">
                    <div class="main-live-box full-width inline-box ">
                        <div class="title-league pull-left">
                            <img src="/images/five-leagues/premier-league-logo.png">
                            <h5>Premier league</h5>
                        </div>
                        <div class="inline-box full-width">
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">

                                                <h5>Arsenal</h5>
                                                <div class="red-card"></div>
                                                <img src="/images/logo-team/Arsenal.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>0</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Bournemouth.png">
                                                <h5>Bournemouth</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">

                                                <h5>Brighton</h5>
                                                <div class="red-card"></div>
                                                <div class="red-card"></div>
                                                <img src="/images/logo-team/Brighton.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>3</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Burnley.png">
                                                <h5>Burnley</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">32'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <h5>Chelsea</h5>
                                                <img src="/images/logo-team/Chelsea.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>0</li>
                                                <li>-</li>
                                                <li>3</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Everton.png">
                                                <div class="red-card"></div>
                                                <h5>Everton</h5>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">45+3'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <h5>Liverpool</h5>
                                                <img src="/images/logo-team/Liverpool.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>3</li>
                                                <li>-</li>
                                                <li>1</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/manchester.png">
                                                <div class="red-card"></div>
                                                <div class="red-card"></div>
                                                <h5>manchester</h5>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="main-live-box full-width inline-box ">
                        <div class="title-league pull-left">
                            <img src="/images/five-leagues/La-Liga-Logo-1.png">
                            <h5>La Liga</h5>
                        </div>
                        <div class="inline-box full-width">
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">

                                                <h5>Arsenal</h5>
                                                <div class="red-card"></div>
                                                <img src="/images/logo-team/Arsenal.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>0</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Bournemouth.png">
                                                <h5>Bournemouth</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">

                                                <h5>Brighton</h5>
                                                <div class="red-card"></div>
                                                <div class="red-card"></div>
                                                <img src="/images/logo-team/Brighton.png">

                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>3</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Burnley.png">
                                                <h5>Burnley</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    // $('#accordion').on('shown.bs.collapse', function () {
    //
    //     var panel = $(this).find('.in');
    //
    //     $('html, body').animate({
    //         scrollTop: panel.offset().top
    //     }, 1000);
    //
    // });
</script>
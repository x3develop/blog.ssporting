<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/mobile/css/all-style.css" rel="stylesheet">
    <link href="/mobile/css/article.css" rel="stylesheet">
    <link href="/mobile/css/bootstrap.css" rel="stylesheet">
    <link href="/mobile/css/docs.css" rel="stylesheet">
    <link href="/mobile/css/game.css" rel="stylesheet">
    <link href="/mobile/css/highlight.css" rel="stylesheet">
    <link href="/mobile/css/index.css" rel="stylesheet">
    <link href="/mobile/css/live-football.css" rel="stylesheet">
    <link href="/mobile/css/news.css" rel="stylesheet">
    <link href="/mobile/css/profile.css" rel="stylesheet">
    <link href="/mobile/css/ranking.css" rel="stylesheet">
    <link href="/mobile/css/review.css" rel="stylesheet">
    <link href="/mobile/css/livescore-mobile.css" rel="stylesheet">
    <link href="/mobile/css/football-symbol-mobile.css" rel="stylesheet">
    <link href="/mobile/css/swiper.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/mobile/css/font-awesome-4.7.0/css/font-awesome.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/js/moment.js"></script>
    <script src="/js/moment-with-locales.min.js"></script>
    <script src="/js/newFB.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
</head>
<?php
require_once __DIR__.'/../bootstrap.php';
$gid = $_REQUEST['gid'];
if (!isset($gid)) {
    header("Location: /mobile/sexy-football");
}

$mcon = new \controller\MediaController();
$gall = $mcon->getMedia($gid);

$host='https://ngoal.com';
?>
<body>
<?php include("menu-mobile.php"); ?>

<div class="container-picpost">
    <div class="container">
        <div class="row">
            <div class="feature-news" style="background-image:url(<?php echo $host.$gall->cover->thumbnail; ?>);"></div>
        </div>
    </div>
    <div class="container container-title-news-info">
        <h2><?php echo $gall->title ?></h2>
    </div>
    <div class="container">
        <div class="time-box box-inline">4 hours ago</div>
        <div class="vertical-line-between box-inline"></div>
        <div class="view-box box-inline"><?php echo $gall->view; ?></div>
        <div class="container-share-news box-inline pull-right">
            <span class="ft-share-box">Share</span>
            <span class="gp-share-box">Share</span>
        </div>
    </div>

    <div class="container container-gallery-all">
        <?php foreach ($gall->media as $media){ ?>
        <figure>
            <img src="<?php echo $host.$media->path; ?>">
        </figure>
        <?php } ?>

    </div>
</div>
<?php include("footer.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/mobile/css/all-style.css" rel="stylesheet">
    <link href="/mobile/css/article.css" rel="stylesheet">
    <link href="/mobile/css/bootstrap.css" rel="stylesheet">
    <link href="/mobile/css/docs.css" rel="stylesheet">
    <link href="/mobile/css/game.css" rel="stylesheet">
    <link href="/mobile/css/highlight.css" rel="stylesheet">
    <link href="/mobile/css/index.css" rel="stylesheet">
    <link href="/mobile/css/live-football.css" rel="stylesheet">
    <link href="/mobile/css/news.css" rel="stylesheet">
    <link href="/mobile/css/profile.css" rel="stylesheet">
    <link href="/mobile/css/ranking.css" rel="stylesheet">
    <link href="/mobile/css/review.css" rel="stylesheet">
    <link href="/mobile/css/livescore-mobile.css" rel="stylesheet">
    <link href="/mobile/css/football-symbol-mobile.css" rel="stylesheet">
    <link href="/mobile/css/swiper.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/mobile/css/font-awesome-4.7.0/css/font-awesome.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/js/moment.js"></script>
    <script src="/js/moment-with-locales.min.js"></script>
    <script src="/js/newFB.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<style>
    .active-nf-highlight .nf-highlight:before,
    .content-nav-footer ul li.active-nf-highlight span {
        color: rgba(255, 255, 255, 1) !important;
    }
</style>
<?php include("menu-mobile.php"); ?>
<div class="container-title-highlight full-width inline-box">
    <div class="btn-group hide pull-left">
        <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
            <span><img src="/images/five-leagues/premier-league-logo.png"></span>
            <span class="name-btn-group"> Premier League</span>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li>
                <a href="#"><img src="/images/five-leagues/all.png"> All</a>
            </li>
            <li>
                <a href="#"><img src="/images/five-leagues/premier-league-logo.png"> Premier League</a>
            </li>
            <li>
                <a href="#"><img src="/images/five-leagues/La-Liga-Logo-1.png"> La Liga</a>
            </li>
            <li>
                <a href="#"><img src="/images/five-leagues/Ligue-1-logo-france.png"> Ligue 1</a>
            </li>
            <li>
                <a href="#"><img src="/images/five-leagues/logo_bl.gif"> Bundesliga</a>
            </li>
            <li>
                <a href="#"><img src="/images/five-leagues/seriea1.png"> Serie A</a>
            </li>
        </ul>
    </div>
    <div class="pull-right hide">
        <div id="selectTeams" class="box-selectTeams">
            <span class="active"><img src="/images/logo-team/manchester.png"></span>
            <span><img src="/images/logo-team/Chelsea.png"></span>
            <span><img src="/images/logo-team/manchester.png"></span>
            <span><img src="/images/logo-team/Chelsea.png"></span>
            <span><img src="/images/logo-team/manchester.png"></span>
            <span><img src="/images/logo-team/Chelsea.png"></span>
            <span><img src="/images/logo-team/manchester.png"></span>
            <span><img src="/images/logo-team/Chelsea.png"></span>
            <span><img src="/images/logo-team/manchester.png"></span>
            <span><img src="/images/logo-team/Chelsea.png"></span>

        </div>
        <div class="shadow-label"></div>
    </div>
    <div class="pull-left topic-highlight">
        <a class="btn btn-link collapsed" role="button" data-toggle="collapse" href="#collapseLeague" aria-expanded="false"
           aria-controls="collapseLeague">
            <span><img src="/images/five-leagues/premier-league-logo.png"></span>
            <span class="name-btn-group"> Premier League</span>
        </a>
    </div>
    <div class="pull-right topic-select-team">
        <a class="btn btn-default collapsed" role="button" data-toggle="collapse" data-target="#collapseTeam"
                aria-expanded="false" aria-controls="collapseTeam">
            เลือกทีม
        </a>
    </div>
</div>
<div class="collapse collapse-league" id="collapseLeague">
    <div class="well">
       <ul>
           <li class="active">
               <span><img src="/images/five-leagues/premier-league-logo.png"></span>
               <span class="name-btn-group">Premier League</span>
           </li>
           <li>
               <span><img src="/images/five-leagues/La-Liga-Logo-1.png"></span>
               <span class="name-btn-group">La Liga</span>
           </li>
           <li>
               <span><img src="/images/five-leagues/Ligue-1-logo-france.png"></span>
               <span class="name-btn-group">Ligue 1</span>
           </li>
           <li>
               <span><img src="/images/five-leagues/logo_bl.gif"></span>
               <span class="name-btn-group">Bundesliga</span>
           </li>
           <li>
               <span><img src="/images/five-leagues/seriea1.png"></span>
               <span class="name-btn-group">Seriea1 A</span>
           </li>
           <li>
               <span><img src="/images/five-leagues/UEFA-Champions-League-1.png"></span>
               <span class="name-btn-group">UEFA</span>
           </li>
           <li>
               <span><img src="/images/five-leagues/uropa.png"></span>
               <span class="name-btn-group">uropa</span>
           </li>
           <li>
               <span><img src="/images/five-leagues/T1_Logo2.png"></span>
               <span class="name-btn-group">Thai L</span>
           </li>
           <li>
               <span><img src="/images/five-leagues/uefa_nations_league.png"></span>
               <span class="name-btn-group">uefa nations league</span>
           </li>
       </ul>
    </div>
</div>
<div class="collapse collapse-team" id="collapseTeam">
    <div class="well">
        <ul>
            <li class="active">
                <span><img src="/images/logo-team/Arsenal.png"></span>
                <span>Arsenal</span>
            </li>
            <li>
                <span><img src="/images/logo-team/Bournemouth.png"></span>
                <span>Bournemouth</span>
            </li>
            <li>
                <span><img src="/images/logo-team/Brighton.png"></span>
                <span>Brighton</span>
            </li>
            <li>
                <span><img src="/images/logo-team/Burnley.png"></span>
                <span>Burnley</span>
            </li>
            <li>
                <span><img src="/images/logo-team/Cardiff.png"></span>
                <span>Cardiff</span>
            </li>
            <li>
                <span><img src="/images/logo-team/Chelsea.png"></span>
                <span>Chelsea</span>
            </li>
            <li>
                <span><img src="/images/logo-team/Crystal-Palace.png"></span>
                <span>Crystal Palace</span>
            </li>
            <li>
                <span><img src="/images/logo-team/Everton.png"></span>
                <span>Everton</span>
            </li>
            <li>
                <span><img src="/images/logo-team/Fulham.png"></span>
                <span>Fulham</span>
            </li>
            <li>
                <span><img src="/images/logo-team/Huddersfield.png"></span>
                <span>Huddersfield</span>
            </li>
            <li>
                <span><img src="/images/logo-team/Leicester-City.png"></span>
                <span>Leicester City</span>
            </li>
            <li>
                <span><img src="/images/logo-team/Liverpool.png"></span>
                <span>Liverpool</span>
            </li>
        </ul>
    </div>
</div>
<div class="container container-main-highlight">
    <div class="row">
        <div class="videoWrapper">
            <iframe src="https://www.youtube.com/embed/eOHEzfTdRdQ" frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
        </div>
    </div>
</div>

<div class="container container-title-news-info">
    <h1>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h1>
    <div class="content-news-info">
        <div class="time-box box-inline">18 hours ago</div>
        <div class="view-box box-inline">29</div>
    </div>
</div>
<div class="container">
    <div class="container-share-news text-center full-width">
        <span class="ft-share-box">Share</span>
        <span class="gp-share-box">Share</span>
    </div>
</div>

<div class="container container-list-highlight">

    <!-- Nav tabs -->
    <ul class="nav" role="tablist">
        <li role="presentation" class="list-highlight-all active">
            <a href="#highlight-football" role="tab" data-toggle="tab">
                ไฮไลท์ยิงประตู <span class="badge">42</span>
            </a>
        </li>
        <li role="presentation" class="list-highlight-full">
            <a href="#fullmatch-football" role="tab" data-toggle="tab">
                ไฮไลท์เต็ม <span class="badge">42</span>
            </a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="highlight-football">
            <div class="box-hothit-item highlight-item active">
                <div class="box-hothit-thumbnail">
                    <img src="/images/luys5_first_1.jpg">
                </div>
                <div class="box-hothit-title">
                    <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                    <p>June 25, 2018</p>
                    <span class="playing-highlight">กำลังเล่น</span>
                </div>

            </div>
            <div class="box-hothit-item highlight-item">
                <div class="box-hothit-thumbnail">
                    <img src="/images/luys5_first_1.jpg">
                </div>
                <div class="box-hothit-title">
                    <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                    <p>June 25, 2018</p>
                    <span class="playing-highlight">กำลังเล่น</span>
                </div>

            </div>
            <div class="box-hothit-item highlight-item">
                <div class="box-hothit-thumbnail">
                    <img src="/images/luys5_first_1.jpg">
                </div>
                <div class="box-hothit-title">
                    <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                    <p>June 25, 2018</p>
                    <span class="playing-highlight">กำลังเล่น</span>
                </div>

            </div>
            <div class="box-hothit-item highlight-item">
                <div class="box-hothit-thumbnail">
                    <img src="/images/luys5_first_1.jpg">
                </div>
                <div class="box-hothit-title">
                    <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                    <p>June 25, 2018</p>
                    <span class="playing-highlight">กำลังเล่น</span>
                </div>

            </div>
            <div class="box-hothit-item highlight-item">
                <div class="box-hothit-thumbnail">
                    <img src="/images/luys5_first_1.jpg">
                </div>
                <div class="box-hothit-title">
                    <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                    <p>June 25, 2018</p>
                    <span class="playing-highlight">กำลังเล่น</span>
                </div>

            </div>
            <div class="box-hothit-item highlight-item">
                <div class="box-hothit-thumbnail">
                    <img src="/images/luys5_first_1.jpg">
                </div>
                <div class="box-hothit-title">
                    <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                    <p>June 25, 2018</p>
                    <span class="playing-highlight">กำลังเล่น</span>
                </div>

            </div>
        </div>
        <div role="tabpanel" class="tab-pane tab-pane-full-match fade" id="fullmatch-football">
            <div class="box-hothit-item highlight-item active">
                <div class="box-hothit-thumbnail">
                    <img src="/images/ca10r_first_1.jpg">
                </div>
                <div class="box-hothit-title">
                    <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                    <p>June 25, 2018</p>
                    <span class="playing-highlight">กำลังเล่น</span>
                </div>

            </div>
            <div class="box-hothit-item highlight-item">
                <div class="box-hothit-thumbnail">
                    <img src="/images/ca10r_first_1.jpg">
                </div>
                <div class="box-hothit-title">
                    <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                    <p>June 25, 2018</p>
                    <span class="playing-highlight">กำลังเล่น</span>
                </div>

            </div>
            <div class="box-hothit-item highlight-item">
                <div class="box-hothit-thumbnail">
                    <img src="/images/ca10r_first_1.jpg">
                </div>
                <div class="box-hothit-title">
                    <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                    <p>June 25, 2018</p>
                    <span class="playing-highlight">กำลังเล่น</span>
                </div>

            </div>
            <div class="box-hothit-item highlight-item">
                <div class="box-hothit-thumbnail">
                    <img src="/images/ca10r_first_1.jpg">
                </div>
                <div class="box-hothit-title">
                    <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                    <p>June 25, 2018</p>
                    <span class="playing-highlight">กำลังเล่น</span>
                </div>

            </div>
            <div class="box-hothit-item highlight-item">
                <div class="box-hothit-thumbnail">
                    <img src="/images/ca10r_first_1.jpg">
                </div>
                <div class="box-hothit-title">
                    <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                    <p>June 25, 2018</p>
                    <span class="playing-highlight">กำลังเล่น</span>
                </div>

            </div>
            <div class="box-hothit-item highlight-item">
                <div class="box-hothit-thumbnail">
                    <img src="/images/ca10r_first_1.jpg">
                </div>
                <div class="box-hothit-title">
                    <h3>[Highlights] France VS Croatia,FIFA World Cup 2018,15-07-2018</h3>
                    <p>June 25, 2018</p>
                    <span class="playing-highlight">กำลังเล่น</span>
                </div>

            </div>
        </div>
    </div>

</div>

<?php include("footer.php"); ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/mobile/css/all-style.css" rel="stylesheet">
    <link href="/mobile/css/article.css" rel="stylesheet">
    <link href="/mobile/css/bootstrap.css" rel="stylesheet">
    <link href="/mobile/css/docs.css" rel="stylesheet">
    <link href="/mobile/css/game.css" rel="stylesheet">
    <link href="/mobile/css/highlight.css" rel="stylesheet">
    <link href="/mobile/css/index.css" rel="stylesheet">
    <link href="/mobile/css/live-football.css" rel="stylesheet">
    <link href="/mobile/css/news.css" rel="stylesheet">
    <link href="/mobile/css/profile.css" rel="stylesheet">
    <link href="/mobile/css/ranking.css" rel="stylesheet">
    <link href="/mobile/css/review.css" rel="stylesheet">
    <link href="/mobile/css/livescore-mobile.css" rel="stylesheet">
    <link href="/mobile/css/football-symbol-mobile.css" rel="stylesheet">
    <link href="/mobile/css/swiper.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/mobile/css/font-awesome-4.7.0/css/font-awesome.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/js/moment.js"></script>
    <script src="/js/moment-with-locales.min.js"></script>
    <script src="/js/newFB.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
</head>
<body class="container-profile-mobile profile-professional" style="background-color: white!important">
<?php include("menu-mobile.php"); ?>
<style>

    .content-nav-footer ul li.active-nf-profile .nf-profile:before,
    .content-nav-footer ul li.active-nf-profile span {
        color: rgba(255, 255, 255, 1);
    }
</style>
<div class="profile-topic">

    <div class="full-width">
        <div class="box-image-profile full-width text-center">
            <img class="image-profile" src="/images/profess-player/sample-logo05.png">
        </div>

        <div class="full-width text-center">
            <h2>Siam Sport</h2>
            <ul class="padding-default margin-default">
                <li><p>professional</p></li>
                <li>LV : 99</li>
            </ul>
        </div>
    </div>

    <div class="box-rate-profile full-width" id="affix3" data-spy="affix" data-offset-top="60">
        <div class="bx-winrate inline-box pull-left">
            <div class="title-rate">
                <h3>78%</h3>
                <p>Win rate</p>
            </div>
        </div>
        <div class="container-graph-play inline-box pull-right">
            <div class="detail-graph-play">
                <table>
                    <tr>
                        <td>
                            <ul>
                                <li><h3>41</h3></li>
                                <li>
                                    <div class="win-status"> Win</div>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul>
                                <li><h3>58</h3></li>
                                <li>
                                    <div class="draw-status"> Draw</div>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul>
                                <li><h3>0</h3></li>
                                <li>
                                    <div class="lose-status"> Lose</div>
                                </li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div style="clear: both;"></div>
</div>

<!--คู่บอล-->

<div class="container-march-playing">
    <div class="container container-playing-pro">
        <div class="mini-title text-center" style="margin-top: 0;">
            <h3 class="padding-bottom">กำลังเล่น</h3>
            <p>17 FEB 2018</p>
        </div>
        <div class="row-player">
            <ul>
                <li class="text-right">
                    <div>Newcastle</div>
                    <img src="/images/logo-team/Newcastle.png">
                </li>
                <li class="text-center">VS</li>
                <li class="text-left">
                    <img src="/images/logo-team/mancity.png">
                    <div class="match-select">Manchester City</div>
                </li>
            </ul>
            <ul>
                <li class="text-right">
                    <div class="match-select">WestHam</div>
                    <img src="/images/logo-team/WestHam.png">
                </li>
                <li class="text-center">VS</li>
                <li class="text-left">
                    <img src="/images/logo-team/Liverpool.png">
                    <div>Liverpool</div>
                </li>
            </ul>
            <ul>
                <li class="text-right">
                    <div class="match-select">Everton</div>
                    <img src="/images/logo-team/Everton.png">
                </li>
                <li class="text-center">VS</li>
                <li class="text-left">
                    <img src="/images/logo-team/Cardiff.png">
                    <div>Cardiff</div>
                </li>
            </ul>
            <ul>
                <li class="text-right">
                    <div class="match-select">Watford</div>
                    <img src="/images/logo-team/Watford.png">
                </li>
                <li class="text-center">VS</li>
                <li class="text-left">
                    <img src="/images/logo-team/Chelsea.png">
                    <div>Chelsea</div>
                </li>
            </ul>
            <ul>
                <li class="text-right">
                    <div>Brighton & Hove Albion</div>
                    <img src="/images/logo-team/Brighton.png">
                </li>
                <li class="text-center">VS</li>
                <li class="text-left">
                    <img src="/images/logo-team/manchester.png">
                    <div class="match-select">Manchester United</div>
                </li>
            </ul>
        </div>

    </div>
</div>

<!--คู่บอล-->

<div class="container-march-playing">
    <div class="container">
        <div class="mini-title" style="margin-top: 0;">
            <h3 class="padding-bottom">History</h3>
        </div>
        <div class="container-event-match">
            <div class="content-event-match">
                <div class="topic-event-match">
                    <div>17 FEB 2018</div>
                    <div>HDP</div>
                </div>
                <div class="box-event-match">
                    <div>
                        <ul>
                            <li>Newcastle</li>
                            <li class="match-select">Manchester City</li>
                        </ul>
                    </div>
                    <div>
                        <ul>
                            <li>0</li>
                            <li>1</li>
                        </ul>
                    </div>
                    <div>
                        1.75
                    </div>
                    <div>
                        <div class="win-status"></div>
                    </div>
                </div>
                <div class="box-event-match">
                    <div>
                        <ul>
                            <li>Newcastle</li>
                            <li class="match-select">Manchester City</li>
                        </ul>
                    </div>
                    <div>
                        <ul>
                            <li>0</li>
                            <li>1</li>
                        </ul>
                    </div>
                    <div>
                        1.75
                    </div>
                    <div>
                        <div class="lose-status"></div>
                    </div>
                </div>
                <div class="box-event-match">
                    <div>
                        <ul>
                            <li>Newcastle</li>
                            <li class="match-select">Manchester City</li>
                        </ul>
                    </div>
                    <div>
                        <ul>
                            <li>0</li>
                            <li>1</li>
                        </ul>
                    </div>
                    <div>
                        1.75
                    </div>
                    <div>
                        <div class="win-status"></div>
                    </div>
                </div>
            </div>
            <div class="content-event-match">
                <div class="topic-event-match">
                    <div>10 FEB 2018</div>
                    <div>HDP</div>
                </div>
                <div class="box-event-match">
                    <div>
                        <ul>
                            <li>Newcastle</li>
                            <li class="match-select">Manchester City</li>
                        </ul>
                    </div>
                    <div>
                        <ul>
                            <li>0</li>
                            <li>1</li>
                        </ul>
                    </div>
                    <div>
                        1.75
                    </div>
                    <div>
                        <div class="draw-status"></div>
                    </div>
                </div>
                <div class="box-event-match">
                    <div>
                        <ul>
                            <li>Newcastle</li>
                            <li class="match-select">Manchester City</li>
                        </ul>
                    </div>
                    <div>
                        <ul>
                            <li>0</li>
                            <li>1</li>
                        </ul>
                    </div>
                    <div>
                        1.75
                    </div>
                    <div>
                        <div class="lose-status"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--คู่บอล-->
<div class="container">
    <div class="mini-title" style="margin-top: 0;">
        <h3>History</h3>
    </div>
    <div class="well well-sm text-center">
        <p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ยังไม่เล่นเกมทายผล</p>
        <button type="button" class="btn btn-default">ทายผลบอล</button>
    </div>
</div>


<?php include("footer.php"); ?>

<?php include("header.php"); ?>

<?php include("menu-mobile.php"); ?>

<div class="container-live-football">
    <div class="container-live-football-top">
        <div class="container">
            <div class="text-title">
                <h4><i class="fa fa-circle" aria-hidden="true"></i> ดูบอลสด</h4>
            </div>
        </div>
    </div>
    <div class="container-tab-nav" style="margin: 0;">
        <!-- Nav tabs -->
        <div class="container-live-football-top">
            <div class="container">
                <ul class="nav" role="tablist">
                    <li role="presentation" class="active"><a href="#living-football" role="tab" data-toggle="tab">
                            <i class="fa fa-video-camera" aria-hidden="true"></i> ถ่ายทอดสด
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#list-live-football" role="tab" data-toggle="tab">
                            <i class="fa fa-list" aria-hidden="true"></i> รายการ
                        </a>
                    </li>
                </ul>
            </div>
        </div>


        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="living-football">
                <div class="content-live-online">
                    <div class="videoWrapper">
                        <iframe id="large-videos-autoPlay" data-autoplay="true"
                                src="https://streamable.com/s/ca10r?autoplay=1" frameborder="0" allow="autoplay"
                                __idm_frm__="753" __idm_id__="679662593"></iframe>
                    </div>
                </div>

                <div class="container-fluid title-live-football">

                    <ul class="topic">
                        <li>
                            <img src="/images/five-leagues/premier-league-logo.png">
                        </li>
                        <li>
                            <p>Premier League</p>
                            <div>English</div>
                        </li>
                    </ul>
                    <div class="box-match-live">
                        <div class="text-center">
                            <p class="live-match-status">Live</p>
                            <p class="date-match">15 Apr 18</p>
                            <p class="time-match">15:00</p>
                        </div>
                        <div>
                            <ul>
                                <li><img src="/images/logo-team/Chelsea.png">Chelsea</li>
                                <li><img src="/images/logo-team/manchester.png">Manchester united</li>
                            </ul>
                        </div>
                    </div>

                </div>

                <div class="container container-share-news">
                    <span class="ft-share-box">Share</span>
                    <span class="gp-share-box">Share</span>
                </div>


                <div class="container">
                    <div class="mini-title">
                        <h3>ลิงค์ดูบอลสด</h3>
                    </div>
                    <ul>
                        <li><a href="#">Baadooballhd (คอมและมือถือ)</a></li>
                        <li><a href="#">Ballsodz (คอมและมือถือ)</a></li>
                        <li><a href="#">TVsod (คอมและมือถือ)</a></li>
                        <li><a href="#">Ball7m (คอมและมือถือ)</a></li>
                    </ul>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="list-live-football">
                <div class="container">
                    <div class="container-list-live">
                        <h5><i class="fa fa-calendar" aria-hidden="true"></i> 8 jun 2018</h5>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseOne"
                                           aria-expanded="true" aria-controls="collapseOne">
                                            <table class="table">
                                                <tr>
                                                    <td>18:00</td>
                                                    <td>
                                                        <img src="/images/five-leagues/seriea1.png">
                                                    </td>
                                                    <td>
                                                        <ul>
                                                            <li>Serie A</li>
                                                            <li><img src="/images/logo-team/Chelsea.png">Chelsea</li>
                                                            <li><img src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            </table>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <ul>
                                            <li><a href="#">Baadooballhd (คอมและมือถือ)</a></li>
                                            <li><a href="#">Ballsodz (คอมและมือถือ)</a></li>
                                            <li><a href="#">TVsod (คอมและมือถือ)</a></li>
                                            <li><a href="#">Ball7m (คอมและมือถือ)</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseTwo"
                                           aria-expanded="true" aria-controls="collapseTwo">
                                            <table class="table">
                                                <tr>
                                                    <td>18:00</td>
                                                    <td>
                                                        <img src="/images/five-leagues/premier-league-logo.png">
                                                    </td>
                                                    <td>
                                                        <ul>
                                                            <li>Premier League</li>
                                                            <li><img src="/images/logo-team/Chelsea.png">Chelsea</li>
                                                            <li><img src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            </table>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <ul>
                                            <li><a href="#">Baadooballhd (คอมและมือถือ)</a></li>
                                            <li><a href="#">Ballsodz (คอมและมือถือ)</a></li>
                                            <li><a href="#">TVsod (คอมและมือถือ)</a></li>
                                            <li><a href="#">Ball7m (คอมและมือถือ)</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="container-list-live">
                        <h5><i class="fa fa-calendar" aria-hidden="true"></i> 7 jun 2018</h5>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseThree"
                                           aria-expanded="true" aria-controls="collapseThree">
                                            <table class="table">
                                                <tr>
                                                    <td>18:00</td>
                                                    <td>
                                                        <img src="/images/five-leagues/seriea1.png">
                                                    </td>
                                                    <td>
                                                        <ul>
                                                            <li>Serie A</li>
                                                            <li><img src="/images/logo-team/Chelsea.png">Chelsea</li>
                                                            <li><img src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            </table>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <ul>
                                            <li><a href="#">Baadooballhd (คอมและมือถือ)</a></li>
                                            <li><a href="#">Ballsodz (คอมและมือถือ)</a></li>
                                            <li><a href="#">TVsod (คอมและมือถือ)</a></li>
                                            <li><a href="#">Ball7m (คอมและมือถือ)</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include("footer.php"); ?>



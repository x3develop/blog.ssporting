<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Cache-control" content="public,max-age=604800">
    <link href="/mobile/css/all-style.css" rel="stylesheet">
    <link href="/mobile/css/index.css" rel="stylesheet">
    <link href="/mobile/css/game.css" rel="stylesheet">
    <link href="/mobile/css/live-football.css" rel="stylesheet">
    <link href="/mobile/css/review.css" rel="stylesheet">
    <link href="/mobile/css/livescore-mobile.css" rel="stylesheet">
    <link href="/mobile/css/football-symbol-mobile.css" rel="stylesheet">
    <link href="/mobile/css/swiper.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/mobile/css/font-awesome-4.7.0/css/font-awesome.css">


    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <!-- Latest compiled and minified JavaScript -->
    <!-- <script src="/js/jquery/dist/jquery.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/js/readmore.js"></script>
    <!-- <script src="/css/bootstrap/js/bootstrap.min.js"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/owl.carousel.min.js"></script>
    <!-- <script src="/js/jquery-timeago/jquery.timeago.js"></script> -->
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <style>
        body {
            background-color: #f1f1f1 !important;
        }

        .active-nf-livescore .nf-livescore:before,
        .active-nf-livescore span{
            color: #fff!important;
        }
    </style>
</head>
<body>


<div class="container-livescore" style="padding-bottom: 80px;">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs nav-liveScore" role="tablist" data-spy="affix" data-offset-top="60">
        <li role="presentation"><a href="#Result" aria-controls="home" role="tab" data-toggle="tab">Result</a></li>
        <li role="presentation" class="active"><a href="#Today_live" aria-controls="profile" role="tab"
                                                  data-toggle="tab">Today <span>(15)</span></a></li>
        <li role="presentation"><a class="tab-live-top" href="#Tap_live" aria-controls="messages" role="tab"
                                   data-toggle="tab">Live <span>(2)</span></a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content tab-content-livescore">
        <div role="tabpanel" class="tab-pane" id="Result">
            <div class="content-result-livescore">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs bt-flex" role="tablist">
                    <li role="presentation" class="active"><a href="#result-today" aria-controls="home" role="tab" data-toggle="tab">Today</a></li>
                    <li role="presentation"><a href="#result-yesterday" aria-controls="profile" role="tab" data-toggle="tab">Yesterday</a></li>
                 </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="result-today">
                        <div class="container">
                            <!--                Live Score-->
                            <div class="inline-box inline-box">
                                <div class="main-live-box full-width inline-box ">
                                    <div class="title-league pull-left">
                                        <img src="/images/five-leagues/premier-league-logo.png">
                                        <h5>Premier league</h5>
                                    </div>
                                    <div class="inline-box full-width">
                                        <div class="panel row-live-box">
                                            <div class="panel-heading">
                                                <a href="#">
                                                    <div class="end-time">FT</div>
                                                    <div class="home-side-team">
                                                        <div class="pull-right">
                                                            <div class="red-card"></div>
                                                            <h5>Arsenal</h5>
                                                            <img src="/images/logo-team/Arsenal.png">
                                                        </div>
                                                    </div>
                                                    <div class="center-row score-live-ft">
                                                        <ul>
                                                            <li>1</li>
                                                            <li>-</li>
                                                            <li>0</li>
                                                        </ul>
                                                    </div>
                                                    <div class="away-side-team">
                                                        <div class="pull-left">
                                                            <img src="/images/logo-team/Bournemouth.png">
                                                            <h5>Bournemouth</h5>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="panel row-live-box">
                                            <div class="panel-heading">
                                                <a href="#">
                                                    <div class="end-time">FT</div>
                                                    <div class="home-side-team">
                                                        <div class="pull-right">
                                                            <div class="red-card"></div>
                                                            <div class="red-card"></div>
                                                            <h5>Brighton</h5>
                                                            <img src="/images/logo-team/Brighton.png">
                                                        </div>
                                                    </div>
                                                    <div class="center-row score-live-ft">
                                                        <ul>
                                                            <li>1</li>
                                                            <li>-</li>
                                                            <li>3</li>
                                                        </ul>
                                                    </div>
                                                    <div class="away-side-team">
                                                        <div class="pull-left">
                                                            <img src="/images/logo-team/Burnley.png">
                                                            <h5>Burnley</h5>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="panel row-live-box">
                                            <div class="panel-heading">
                                                <a href="#">
                                                    <div class="end-time">FT</div>
                                                    <div class="home-side-team">
                                                        <div class="pull-right">
                                                            <h5>Chelsea</h5>
                                                            <img src="/images/logo-team/Chelsea.png">
                                                        </div>
                                                    </div>
                                                    <div class="center-row score-live-ft">
                                                        <ul>
                                                            <li>0</li>
                                                            <li>-</li>
                                                            <li>3</li>
                                                        </ul>
                                                    </div>
                                                    <div class="away-side-team">
                                                        <div class="pull-left">
                                                            <img src="/images/logo-team/Everton.png">
                                                            <h5>Everton</h5>
                                                            <div class="red-card"></div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="panel row-live-box">
                                            <div class="panel-heading">
                                                <a href="#">
                                                    <div class="end-time">FT</div>
                                                    <div class="home-side-team">
                                                        <div class="pull-right">
                                                            <h5>Liverpool</h5>
                                                            <img src="/images/logo-team/Liverpool.png">
                                                        </div>
                                                    </div>
                                                    <div class="center-row score-live-ft">
                                                        <ul>
                                                            <li>3</li>
                                                            <li>-</li>
                                                            <li>1</li>
                                                        </ul>
                                                    </div>
                                                    <div class="away-side-team">
                                                        <div class="pull-left">
                                                            <img src="/images/logo-team/manchester.png">
                                                            <h5>manchester</h5>
                                                            <div class="red-card"></div>
                                                            <div class="red-card"></div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="main-live-box full-width inline-box ">
                                    <div class="title-league pull-left">
                                        <img src="/images/five-leagues/La-Liga-Logo-1.png">
                                        <h5>La Liga</h5>
                                    </div>
                                    <div class="inline-box full-width">
                                        <div class="panel row-live-box">
                                            <div class="panel-heading">
                                                <a href="#">
                                                    <div class="end-time">FT</div>
                                                    <div class="home-side-team">
                                                        <div class="pull-right">
                                                            <div class="red-card"></div>
                                                            <h5>Arsenal</h5>
                                                            <img src="/images/logo-team/Arsenal.png">
                                                        </div>
                                                    </div>
                                                    <div class="center-row score-live-ft">
                                                        <ul>
                                                            <li>1</li>
                                                            <li>-</li>
                                                            <li>0</li>
                                                        </ul>
                                                    </div>
                                                    <div class="away-side-team">
                                                        <div class="pull-left">
                                                            <img src="/images/logo-team/Bournemouth.png">
                                                            <h5>Bournemouth</h5>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="panel row-live-box">
                                            <div class="panel-heading">
                                                <a href="#">
                                                    <div class="end-time">FT</div>
                                                    <div class="home-side-team">
                                                        <div class="pull-right">
                                                            <div class="red-card"></div>
                                                            <div class="red-card"></div>
                                                            <h5>Brighton</h5>
                                                            <img src="/images/logo-team/Brighton.png">
                                                        </div>
                                                    </div>
                                                    <div class="center-row score-live-ft">
                                                        <ul>
                                                            <li>1</li>
                                                            <li>-</li>
                                                            <li>3</li>
                                                        </ul>
                                                    </div>
                                                    <div class="away-side-team">
                                                        <div class="pull-left">
                                                            <img src="/images/logo-team/Burnley.png">
                                                            <h5>Burnley</h5>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="result-yesterday">Yesterday</div>
                </div>

            </div>

        </div>
        <div role="tabpanel" class="tab-pane active" id="Today_live">
            <div class="container">
                <div class="topic-livescore bt-flex full-width">
                    <h2>Live Score
                        <small>12/12/2019</small>
                    </h2>
                    <div class="form-group">
                        <div class="pull-right">
                            <span>Odd</span>
                            <div class="checkbox checbox-switch switch-primary">
                                <label>
                                    <input type="checkbox" name="" checked=""/>
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

<!--                Live Score-->
                <div class="status-live inline-box inline-box">
                    <div class="main-live-box full-width inline-box ">
                        <div class="title-league pull-left">
                            <img src="/images/five-leagues/premier-league-logo.png">
                            <h5>Premier league</h5>
                        </div>
                        <div class="inline-box full-width">
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <div class="red-card"></div>
                                                <h5>Arsenal</h5>
                                                <img src="/images/logo-team/Arsenal.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>0</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Bournemouth.png">
                                                <h5>Bournemouth</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <div class="red-card"></div>
                                                <div class="red-card"></div>
                                                <h5>Brighton</h5>
                                                <img src="/images/logo-team/Brighton.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>3</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Burnley.png">
                                                <h5>Burnley</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">32'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <h5>Chelsea</h5>
                                                <img src="/images/logo-team/Chelsea.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>0</li>
                                                <li>-</li>
                                                <li>3</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Everton.png">
                                                <h5>Everton</h5>
                                                <div class="red-card"></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">45+3'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <h5>Liverpool</h5>
                                                <img src="/images/logo-team/Liverpool.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>3</li>
                                                <li>-</li>
                                                <li>1</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/manchester.png">
                                                <h5>manchester</h5>
                                                <div class="red-card"></div>
                                                <div class="red-card"></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="main-live-box full-width inline-box ">
                        <div class="title-league pull-left">
                            <img src="/images/five-leagues/La-Liga-Logo-1.png">
                            <h5>La Liga</h5>
                        </div>
                        <div class="inline-box full-width">
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <div class="red-card"></div>
                                                <h5>Arsenal</h5>
                                                <img src="/images/logo-team/Arsenal.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>0</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Bournemouth.png">
                                                <h5>Bournemouth</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <div class="red-card"></div>
                                                <div class="red-card"></div>
                                                <h5>Brighton</h5>
                                                <img src="/images/logo-team/Brighton.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>3</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Burnley.png">
                                                <h5>Burnley</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

<!--                None Live-->
                <div class="inline-box full-width">
                    <div class="main-live-box full-width inline-box ">
                        <div class="title-league inline-box full-width">
                            <img src="/images/five-leagues/premier-league-logo.png">
                            <h5>Premier league</h5>
                        </div>
                        <div class="panel-group inline-box full-width" id="accordion" role="tablist"
                             aria-multiselectable="true">
                            <div class="panel row-live-box">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <div class="time-match-default">19:45</div>
                                        <div class="home-side-team text-right">
                                            <h5>Arsenal</h5>
                                            <img src="/images/logo-team/Arsenal.png">

                                        </div>
                                        <div class="center-row row-hdp">
                                            <div class="title-center">
                                                HDP
                                            </div>
                                            <strong>2.50</strong>
                                        </div>
                                        <div class="away-side-team text-left">
                                            <img src="/images/logo-team/Bournemouth.png">
                                            <h5>Bournemouth</h5>

                                        </div>
                                    </a>

                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <!--                                        last 5 match-->
                                        <div class="panel-body-status-match inline-box full-width bt-flex">
                                            <div class="home-status-match text-right">
                                                <div class="standing-view inline-box">
                                                    <ul class="standing-view_last5">
                                                        <li>Last 5 match</li>
                                                        <li class="win-status"></li>
                                                        <li class="win-status"></li>
                                                        <li class="win-status"></li>
                                                        <li class="win-status"></li>
                                                        <li class="win-status"></li>
                                                    </ul>
                                                    <ul class="standing-view_odd">
                                                        <li>Odds</li>
                                                        <li class="win-status"></li>
                                                        <li class="win-status"></li>
                                                        <li class="win-status"></li>
                                                        <li class="win-status"></li>
                                                        <li class="win-status"></li>
                                                    </ul>
                                                </div>
                                                <div class="standing-view-logo inline-box">
                                                    <img src="/images/logo-team/Arsenal.png">
                                                    <span>(5)</span>
                                                </div>
                                            </div>
                                            <div class="away-status-match text-left">
                                                <div class="standing-view-logo inline-box ">
                                                    <img src="/images/logo-team/Bournemouth.png">
                                                    <span>(12)</span>
                                                </div>
                                                <div class="standing-view inline-box">
                                                    <ul class="standing-view_last5">
                                                        <li class="win-status"></li>
                                                        <li class="win-status"></li>
                                                        <li class="win-status"></li>
                                                        <li class="win-status"></li>
                                                        <li class="win-status"></li>
                                                        <li>Last 5 match</li>
                                                    </ul>
                                                    <ul class="standing-view_odd">
                                                        <li class="win-status"></li>
                                                        <li class="win-status"></li>
                                                        <li class="win-status"></li>
                                                        <li class="win-status"></li>
                                                        <li class="win-status"></li>
                                                        <li>Odds</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!--                                       h2h-->
                                        <div class="box-h2h inline-box full-width">
                                            <div class="title-center margin-center">
                                                <h6>Head To Head</h6>
                                            </div>
                                            <div class="progress full-width inline-box">
                                                <div class="progress-bar progress-bar-home" style="width: 60%">
                                                    2
                                                </div>
                                                <div class="progress-bar progress-bar-draw" style="width: 10%">
                                                    1
                                                </div>
                                                <div class="progress-bar progress-bar-way" style="width: 30%">
                                                    1
                                                </div>
                                            </div>
                                        </div>
                                        <!--                                        Review-->
                                        <div class="box-review-simple inline-box">
                                            <a href="#" class="link_wrap"></a>
                                            <div class="row-review-simple bt-flex">

                                                <div class="rrs-profile">
                                                    <img src="/images/2.jpg">
                                                </div>
                                                <div class="rrs-team">
                                                    <span>@arsenalarsenalarsenalarsenal</span>
                                                </div>
                                                <div class="rrs-review-simple">
                                                        <span>สองทีมนี้ผลงานต่างกันราวฟ้ากับเหว ทางเจ้าบ้านไปในทางที่ดี ฮอนก้า
                                                        เกมนี้ได้เปรียบอย่างมาก มีทั้งความมั่นใจในมุกๆเกมที่ลงสนาม
                                                        พร้อมเปิดบ้านซัดประตูคู่แข่ง ด้วยฟอร์มการเล่น 5 นัดหลัง ชนะ 3 เสมอ 1
                                                        แพ้ 1 เก็บได้ถึง 10 คะแนนเลยแค่ไม่กี่เกม แต่ทาง พีเอส เคมี่ กลับกัน
                                                        คว้าสองแต้มจาก 5 นัดหลัง เสมอ 2 แพ้ 3
                                                        </span>
                                                </div>


                                            </div>
                                            <div class="row-review-simple bt-flex">

                                                <div class="rrs-profile">
                                                    <img src="/images/profess-player/sample-logo04.png">
                                                </div>
                                                <div class="rrs-team">
                                                    <span>@bournemouth</span>
                                                </div>
                                                <div class="rrs-review-simple">
                                                    <span>มาดริด กำลังโชว์ฟอร์มเยี่ยม ก่อนมินิเบรค จึงกลับประเดิมเลก 2 ด้วยความฮึกเหิม นอกจากนั้น "ราชันชุดขาว" ทะยอยได้ตัวหลักคืนทีมเกือบครบ แถมฟิตเต็มร้อยทุกราย เชื่อว่าแชมป์ยุโรป 3 สมัยโฟกัสที่ 3 แต้มแน่นอน กอปรกับบียาร์เรอัล ไม่ได้อยู่ในช่วงเวลาทีดีนัก ล่าสุดรั้งท้ายตาราง ถึงแม้จะเปิดเกมสู้แบบหลังพิงฝา แต่มาเจอกับมาดริด ที่เต็มไปด้วยแข้งเกรดเอ บวกกับฟอร์มเริ่มเข้าฝัก ก็คงรอดยากสำหรับเจ้าบ้าน นอกจากนั้นปูมหลังเก่าๆ ฝั่งมาดริด บุกชนะบียาร์เรอัล ได้บ่อยครั้งอีกด้วย ดังนั้นราคาแค่ 0.5 ยังไงก็ต้องวางบอลต่อ
                                                        </span>
                                                </div>


                                            </div>
                                            <div class="row-review-simple bt-flex">

                                                <div class="rrs-profile">
                                                    <img src="/images/2.jpg">
                                                </div>
                                                <div class="rrs-team">
                                                    <span>@arsenalarsenalarsenalarsenal</span>
                                                </div>
                                                <div class="rrs-review-simple">
                                                        <span>สองทีมนี้ผลงานต่างกันราวฟ้ากับเหว ทางเจ้าบ้านไปในทางที่ดี ฮอนก้า
                                                        เกมนี้ได้เปรียบอย่างมาก มีทั้งความมั่นใจในมุกๆเกมที่ลงสนาม
                                                        พร้อมเปิดบ้านซัดประตูคู่แข่ง ด้วยฟอร์มการเล่น 5 นัดหลัง ชนะ 3 เสมอ 1
                                                        แพ้ 1 เก็บได้ถึง 10 คะแนนเลยแค่ไม่กี่เกม แต่ทาง พีเอส เคมี่ กลับกัน
                                                        คว้าสองแต้มจาก 5 นัดหลัง เสมอ 2 แพ้ 3
                                                        </span>
                                                </div>


                                            </div>
                                            <div class="row-review-simple bt-flex">

                                                <div class="rrs-profile">
                                                    <img src="/images/2.jpg">
                                                </div>
                                                <div class="rrs-team">
                                                    <span>@arsenalarsenalarsenalarsenal</span>
                                                </div>
                                                <div class="rrs-review-simple">
                                                        <span>สองทีมนี้ผลงานต่างกันราวฟ้ากับเหว ทางเจ้าบ้านไปในทางที่ดี ฮอนก้า
                                                        เกมนี้ได้เปรียบอย่างมาก มีทั้งความมั่นใจในมุกๆเกมที่ลงสนาม
                                                        พร้อมเปิดบ้านซัดประตูคู่แข่ง ด้วยฟอร์มการเล่น 5 นัดหลัง ชนะ 3 เสมอ 1
                                                        แพ้ 1 เก็บได้ถึง 10 คะแนนเลยแค่ไม่กี่เกม แต่ทาง พีเอส เคมี่ กลับกัน
                                                        คว้าสองแต้มจาก 5 นัดหลัง เสมอ 2 แพ้ 3
                                                        </span>
                                                </div>


                                            </div>

                                            <div class="inline-box full-width text-center">
                                                <a class="btn btn-default btn-sm" href="#"
                                                   role="button">ดูทรรศนะทั้งหมด</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="panel row-live-box ">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <a class="collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                                       aria-controls="collapseTwo">
                                        <div class="time-match-default">22:00</div>
                                        <div class="home-side-team text-right">
                                            <h5>Arsenal</h5>
                                            <img src="/images/logo-team/Arsenal.png">

                                        </div>
                                        <div class="center-row row-hdp">
                                            <div class="title-center">
                                                HDP
                                            </div>
                                            <strong>2.50</strong>
                                        </div>
                                        <div class="away-side-team text-left">
                                            <img src="/images/logo-team/Bournemouth.png">
                                            <h5>Bournemouth</h5>

                                        </div>
                                    </a>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                        richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                        brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,
                                        sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                                        shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson
                                        cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                                        Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                                        you probably haven't heard of them accusamus labore sustainable VHS.
                                    </div>
                                </div>
                            </div>
                            <div class="panel row-live-box ">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <a class="collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseThree" aria-expanded="false"
                                       aria-controls="collapseThree">
                                        <div class="time-match-default">22:00</div>
                                        <div class="home-side-team text-right">
                                            <h5>Arsenal</h5>
                                            <img src="/images/logo-team/Arsenal.png">

                                        </div>
                                        <div class="center-row row-hdp">
                                            <div class="title-center">
                                                HDP
                                            </div>
                                            <strong>2.50</strong>
                                        </div>
                                        <div class="away-side-team text-left">
                                            <img src="/images/logo-team/Bournemouth.png">
                                            <h5>Bournemouth</h5>

                                        </div>
                                    </a>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                        richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                        brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,
                                        sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                                        shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson
                                        cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                                        Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                                        you probably haven't heard of them accusamus labore sustainable VHS.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="Tap_live">
            <div class="container">
                <div class="topic-livescore bt-flex full-width">
                    <h2>Live Score
                        <small>12/12/2019</small>
                    </h2>
                    <div class="form-group">
                        <div class="pull-right">
                            <span>Odd</span>
                            <div class="checkbox checbox-switch switch-primary">
                                <label>
                                    <input type="checkbox" name="" checked=""/>
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <!--                Live Score-->
                <div class="status-live inline-box inline-box">
                    <div class="main-live-box full-width inline-box ">
                        <div class="title-league pull-left">
                            <img src="/images/five-leagues/premier-league-logo.png">
                            <h5>Premier league</h5>
                        </div>
                        <div class="inline-box full-width">
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <div class="red-card"></div>
                                                <h5>Arsenal</h5>
                                                <img src="/images/logo-team/Arsenal.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>0</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Bournemouth.png">
                                                <h5>Bournemouth</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <div class="red-card"></div>
                                                <div class="red-card"></div>
                                                <h5>Brighton</h5>
                                                <img src="/images/logo-team/Brighton.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>3</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Burnley.png">
                                                <h5>Burnley</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">32'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <h5>Chelsea</h5>
                                                <img src="/images/logo-team/Chelsea.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>0</li>
                                                <li>-</li>
                                                <li>3</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Everton.png">
                                                <h5>Everton</h5>
                                                <div class="red-card"></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">45+3'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <h5>Liverpool</h5>
                                                <img src="/images/logo-team/Liverpool.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>3</li>
                                                <li>-</li>
                                                <li>1</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/manchester.png">
                                                <h5>manchester</h5>
                                                <div class="red-card"></div>
                                                <div class="red-card"></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="main-live-box full-width inline-box ">
                        <div class="title-league pull-left">
                            <img src="/images/five-leagues/La-Liga-Logo-1.png">
                            <h5>La Liga</h5>
                        </div>
                        <div class="inline-box full-width">
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <div class="red-card"></div>
                                                <h5>Arsenal</h5>
                                                <img src="/images/logo-team/Arsenal.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>0</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Bournemouth.png">
                                                <h5>Bournemouth</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <div class="red-card"></div>
                                                <div class="red-card"></div>
                                                <h5>Brighton</h5>
                                                <img src="/images/logo-team/Brighton.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>3</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Burnley.png">
                                                <h5>Burnley</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $('#accordion').on('shown.bs.collapse', function () {

        var panel = $(this).find('.in');

        $('html, body').animate({
            scrollTop: panel.offset().top
        }, 1000);

    });
</script>











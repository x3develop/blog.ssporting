<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/mobile/css/all-style.css" rel="stylesheet">
    <link href="/mobile/css/article.css" rel="stylesheet">
    <link href="/mobile/css/bootstrap.css" rel="stylesheet">
    <link href="/mobile/css/docs.css" rel="stylesheet">
    <link href="/mobile/css/game.css" rel="stylesheet">
    <link href="/mobile/css/highlight.css" rel="stylesheet">
    <link href="/mobile/css/index.css" rel="stylesheet">
    <link href="/mobile/css/live-football.css" rel="stylesheet">
    <link href="/mobile/css/news.css" rel="stylesheet">
    <link href="/mobile/css/profile.css" rel="stylesheet">
    <link href="/mobile/css/ranking.css" rel="stylesheet">
    <link href="/mobile/css/review.css" rel="stylesheet">
    <link href="/mobile/css/livescore-mobile.css" rel="stylesheet">
    <link href="/mobile/css/football-symbol-mobile.css" rel="stylesheet">
    <link href="/mobile/css/swiper.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/mobile/css/font-awesome-4.7.0/css/font-awesome.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/js/moment.js"></script>
    <script src="/js/moment-with-locales.min.js"></script>
    <script src="/js/newFB.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
</head>
<body class="container-profile-mobile" style="background-color: white!important">
<?php include("menu-mobile.php"); ?>
<style>

    .content-nav-footer ul li.active-nf-profile .nf-profile:before,
    .content-nav-footer ul li.active-nf-profile span {
        color: rgba(255, 255, 255, 1);
    }
</style>
<div class="profile-topic">
    <div class="container">
        <div class="full-width">
            <div class="box-image-profile pull-left">
                <img class="image-profile" src="/images/p1.png">
            </div>

            <div class="pull-left text-left">
                <h2>อลัน เชียร์เร่อ</h2>
                <ul class="padding-default margin-default">
                    <li><p>เด็กเก็บบอล</p></li>
                    <li>LV : 2</li>
                </ul>
            </div>
        </div>


        <div class="container-graph-play inline-box">
            <div class="detail-graph-play">
                <table>
                    <tr>
                        <td>
                            <ul>
                                <li><h3>41</h3></li>
                                <li>
                                    <div class="win-status"> Win</div>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul>
                                <li><h3>58</h3></li>
                                <li>
                                    <div class="draw-status"> Draw</div>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <ul>
                                <li><h3>0</h3></li>
                                <li>
                                    <div class="lose-status"> Lose</div>
                                </li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<!--สถาะ Sgold ,Scoin-->
<div class="container">
    <div class="container-coin-profile inline-box full-width">
        <ul>
            <li>
                <div class="icon-coin"><img src="/images/coin/sgold100.png"></div>
                <div class="inline-box">
                    <p>Sgold</p>
                    <h3>300.00</h3>
                </div>


            </li>
            <li>
                <div class="icon-coin"><img src="/images/coin/scoin100.png"></div>
                <div class="inline-box">
                    <p>Scoin</p>
                    <h3>15,300.00</h3>
                </div>


            </li>
        </ul>
    </div>
</div>

<!--สถานะ coin-->
<div class="container container-change-coin">
    <div class="text-center" style="margin-bottom: 10px;">
        <button class="btn btn-primary" type="button" data-toggle="collapse"
                data-target="#collapseChangeCoin"
                aria-expanded="false" aria-controls="collapseChangeCoin">
            แลกเปลี่ยน Coin
        </button>
    </div>

    <!--    แลกเปลี่ยนจาก Sgold เป็น Scoin-->
    <div class="collapse" id="collapseChangeCoin">
        <div class="thumbnail">
            <div class="container-show-coin">
                <div class="mini-title" style="margin-top: 0;">
                    <h3>แลกเปลี่ยนจาก <br>Sgold เป็น Scoin</h3>
                </div>

                <div class="show-coin">
                    <ul>
                        <li>
                            <div class="icon-coin"><img src="/images/coin/sgold100.png"></div>
                            <h3>300.00</h3>
                            <p>Sgold</p>
                        </li>
                        <li>
                            <div class="icon-coin"><img src="/images/coin/scoin100.png"></div>
                            <h3>15,300.00</h3>
                            <p>Scoin</p>
                        </li>
                    </ul>
                </div>
                <div class="input-group input-group-lg col-xs-9" style="margin: 0 auto;">
                    <select class="form-control">
                        <option>1</option>
                        <option>10</option>
                        <option>20</option>
                        <option>50</option>
                    </select>
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button"><i class="fa fa-exchange" aria-hidden="true"></i> แลกเปลี่ยน</button>
                    </span>
                </div>
                <div class="box-collapse-text" style="margin: 10px 0 20px 0;">
                    <ul>
                        <li class="text-right"><img src="/images/coin-change.png"></li>
                        <li><p>1 Sgold = 100 Scoin</p></li>
                    </ul>
                </div>
                <div class="box-collapse-text" style="margin: 20px 0;">
                    <ul style="font-size: 25px;">
                        <li><p>คุณได้รับ</p></li>
                        <li><img src="/images/coin/scoin100.png"></li>
                        <li><p>100</p></li>
                    </ul>
                </div>
                <div class="text-center">
                    <button type="button" class="btn btn-danger btn-reset-gold">
                        <i class="fa fa-refresh" aria-hidden="true"></i> Reset Sgold
                        <!--                                        <img src="images/icon/reset.png" style="width: 30px;"> Reset-->
                    </button>
                    <p style="margin-top: 5px;">กด Reset เพื่อเปลี่ยน Sgold ให้เป็น 0 ได้ทุกวันที่ 1, 16
                        ของทุกเดือน</p>
                </div>
            </div>


            <div style="clear: both;"></div>
        </div>


    </div>

</div>




<!--สถิติการ Win/Loss ต่อเนื่อง(Step)-->
<div class="container">
    <div class="mini-title" style="margin-top: 0;">
        <h3 class="padding-bottom">สถิติการ Win/Loss <br>ต่อเนื่อง(Step)</h3>
    </div>
    <div class="container-step">
        <div class="row">
            <div class="bx-level-step">
                <div class="status-level first-status-level active">
                    <span>3</span>
                </div>
                <div class="status-level active">
                    <span>4</span>
                </div>
                <div class="status-level active">
                    <span>5</span>
                </div>
                <div class="status-level">
                    <span>6</span>
                </div>
                <div class="status-level">
                    <span>7</span>
                </div>
                <div class="status-level last-status-level">
                    <span>8</span>
                </div>
            </div>
        </div>


        <div class="text-center alert alert-info margin-default">
            <span>แพ้ บอลชุด 5 ทีม</span>
        </div>
    </div>
</div>


<!--สถิติการ Win/Loss รายวันต่อเนื่อง (Combo)-->
<div class="container" style="margin: 10px 0 0 0;">
    <div class="mini-title text-center" style="margin: 0;">
        <h3 class="padding-bottom">สถิติการ Win/Loss <br>รายวันต่อเนื่อง (Combo)</h3>
        <p class="margin-default" style="color: #aaa;">[ Combo แลกได้(4,7,10,15,20) ]</p>
    </div>

    <div class="container-combo">

        <div class="bx-progress">
            <div class="progress">
                <div class="progress-bar active" style="width: 15%">
                    <div class="num-combo">
                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                        <p>4</p>
                    </div>
                </div>
                <div class="progress-bar" style="width: 15%">
                    <div class="num-combo">
                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                        <p>7</p>
                    </div>

                </div>
                <div class="progress-bar" style="width: 20%">
                    <div class="num-combo">
                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                        <p>10</p>
                    </div>
                </div>
                <div class="progress-bar" style="width: 25%">
                    <div class="num-combo">
                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                        <p>15</p>
                    </div>
                </div>
                <div class="progress-bar" style="width: 25%">
                    <div class="num-combo">
                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                        <p>20</p>
                    </div>
                </div>
            </div>
            <!--                <ul>-->
            <!--                    <li>แพ้ ต่อเนื่อง 1 คู่</li>-->
            <!--                </ul>-->
        </div>
    </div>
    <div class="text-center alert alert-info margin-default">
        <span>แพ้ combo 4</span>
    </div>
</div>

<!--คู่บอล-->

<div class="container-march-playing">
<div class="container">
    <div class="mini-title" style="margin-top: 0;">
        <h3>คู่บอล</h3>
    </div>
    <div class="title-filter">
        <div class="pull-left">
            <button type="button" class="btn btn-sm btn-default"><i class="fa fa-calendar" aria-hidden="true"></i>
                เลือกวันที่
            </button>
        </div>
        <div class="pull-right">
            <p>Filter by Win & Lose</p>
            <select class="form-control input-sm">
                <option>All</option>
                <option>Win</option>
                <option>Lose</option>
                <option>Drae</option>
            </select>

        </div>
    </div>
    <div class="container-event-match">
        <div class="content-event-match">
            <div class="topic-event-match">
                <div>17 FEB 2018</div>
                <div>HDP</div>
            </div>
            <div class="box-event-match">
                <div>
                    <ul>
                        <li>Newcastle</li>
                        <li class="match-select">Manchester City</li>
                    </ul>
                </div>
                <div>
                    <ul>
                        <li>0</li>
                        <li>1</li>
                    </ul>
                </div>
                <div>
                    1.75
                </div>
                <div>
                    <div class="win-status"></div>
                </div>
            </div>
            <div class="box-event-match">
                <div>
                    <ul>
                        <li>Newcastle</li>
                        <li class="match-select">Manchester City</li>
                    </ul>
                </div>
                <div>
                    <ul>
                        <li>0</li>
                        <li>1</li>
                    </ul>
                </div>
                <div>
                    1.75
                </div>
                <div>
                    <div class="lose-status"></div>
                </div>
            </div>
            <div class="box-event-match">
                <div>
                    <ul>
                        <li>Newcastle</li>
                        <li class="match-select">Manchester City</li>
                    </ul>
                </div>
                <div>
                    <ul>
                        <li>0</li>
                        <li>1</li>
                    </ul>
                </div>
                <div>
                    1.75
                </div>
                <div>
                    <div class="win-status"></div>
                </div>
            </div>
        </div>
        <div class="content-event-match">
            <div class="topic-event-match">
                <div>10 FEB 2018</div>
                <div>HDP</div>
            </div>
            <div class="box-event-match">
                <div>
                    <ul>
                        <li>Newcastle</li>
                        <li class="match-select">Manchester City</li>
                    </ul>
                </div>
                <div>
                    <ul>
                        <li>0</li>
                        <li>1</li>
                    </ul>
                </div>
                <div>
                    1.75
                </div>
                <div>
                    <div class="draw-status"></div>
                </div>
            </div>
            <div class="box-event-match">
                <div>
                    <ul>
                        <li>Newcastle</li>
                        <li class="match-select">Manchester City</li>
                    </ul>
                </div>
                <div>
                    <ul>
                        <li>0</li>
                        <li>1</li>
                    </ul>
                </div>
                <div>
                    1.75
                </div>
                <div>
                    <div class="lose-status"></div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<!--คู่บอล-->
<div class="container">
    <div class="mini-title" style="margin-top: 0;">
        <h3>คู่บอล</h3>
    </div>
    <div class="well well-sm text-center">
        <p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ยังไม่เล่นเกมทายผล</p>
        <button type="button" class="btn btn-default">ทายผลบอล</button>
    </div>
</div>


<!--statement-->
<div class="container">
    <div class="mini-title" style="margin-top: 0;">
        <h3>statement</h3>
    </div>
    <div class="container-statement">
        <div class="content-statement thumbnail">
            <div class="title-statement">
                <div><i class="fa fa-calendar" aria-hidden="true"></i> 18 Jul 2018</div>
                <div class="text-center">กิจกรรม</div>
            </div>
            <div class="box-statement">
                <div>
                    <p>- กดรับ 500 coin</p>
                    <p>วันที่ 2</p>
                </div>
                <div>
                    <ul>
                        <li><img src="/images/coin/sgold100.png">+ 500</li>
                        <li><img src="/images/coin/scoin100.png">-</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content-statement thumbnail">
            <div class="title-statement">
                <div><i class="fa fa-calendar" aria-hidden="true"></i> 18 Jul 2018</div>
                <div class="text-center">กิจกรรม</div>
            </div>
            <div class="box-statement">
                <div>
                    <p>- กดรับ 500 coin</p>
                    <p>วันที่ 1</p>
                </div>
                <div>
                    <ul>
                        <li><img src="/images/coin/sgold100.png">+ 500</li>
                        <li><img src="/images/coin/scoin100.png">-</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content-statement thumbnail">
            <div class="title-statement">
                <div><i class="fa fa-calendar" aria-hidden="true"></i> 18 Jul 2018</div>
                <div class="text-center">กิจกรรม</div>
            </div>
            <div class="box-statement">
                <div>
                    <p>- กดรับ 500 coin</p>
                    <p>วันที่ 0</p>
                </div>
                <div>
                    <ul>
                        <li><img src="/images/coin/sgold100.png">+ 500</li>
                        <li><img src="/images/coin/scoin100.png">-</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include("footer.php"); ?>
$(document).ready(function () {
    moment.tz.add("Asia/Bangkok|LMT BMT +07|-6G.4 -6G.4 -70|012|-3D8SG.4 1C000|15e6");
    moment.tz.add("Asia/Singapore|LMT SMT +07 +0720 +0730 +09 +08|-6T.p -6T.p -70 -7k -7u -90 -80|01234546|-2M0ST.p aIM0 17anT.p l5XE 17bO 8Fyu 1so1u|56e5");
    var host = "https://www.ngoal.com/";
    var livelist = [];
    var endlist = [];
    var queuemid = [];
    var laststop = 0;
    var boxstrucori = null;
    var lineupsboxrow = null;
    var expandedmid = 0;
    var h2hstructure = null;
    var h2hbodystructure = null;
    var reviewbodystruct = null;
    var lineboard = null;
    var fbid = $("#user-fb-avatar").attr("fbuid");
    var token = null;
    var checkin = false;
    var betmid = 0;
    var betval = 1200;


    // timestate();
    checkuser();
    inituser();
    getHLUpdate();
    setInterval(getHLUpdate, 60000 * 1);
    setInterval(getLiveEvent, 60000 * 2);
    setInterval(getStatUpdate, 60000 * 2);

    function getLinupeStructure() {
        if (lineupsboxrow == null) {
            $.ajax({
                url: "/mobile/include/lineups-player.html",
                method: "GET",
                dataType: "html"
            }).done(function (response) {
                lineupsboxrow = response;

            });
        }
    }

    function getH2HStructure() {
        if (h2hstructure == null) {
            $.ajax({
                url: "/mobile/include/h2h-league-header.html",
                method: "GET",
                dataType: "html"
            }).done(function (response) {
                h2hstructure = response;
            });
        }

        if (h2hbodystructure == null) {
            $.ajax({
                url: "/mobile/include/h2h-match-body.html",
                method: "GET",
                dataType: "html"
            }).done(function (response) {
                h2hbodystructure = response;
            });
        }
    }

    function getReviewStructure() {
        if (reviewbodystruct == null) {
            $.ajax({
                url: "/mobile/include/review-body.html",
                method: "GET",
                dataType: "html"
            }).done(function (response) {
                reviewbodystruct = response;

            });
        }
    }

    function getLineBoard() {
        if (lineboard == null) {
            $.ajax({
                url: "/mobile/include/lineup-board.html",
                method: "GET",
                dataType: "html"
            }).done(function (response) {
                lineboard = response;
            });
        }
    }

    function checktin(fbid, email, name) {
        $.ajax({
            url: "/play/checkIn",
            method: "GET",
            data: {id: fbid, email: email, name: name}
        }).done(function (response) {
            checkin = true;
        });
    }

    $(document).on("click", ".match-expand", function (e) {
        var mid = $(e.currentTarget).attr("mid");
        if (mid != expandedmid) {
            $(".panel-collapse[mid=" + expandedmid + "]").collapse("hide");
            expandedmid = mid;
        } else if (mid == expandedmid) {
            expandedmid = 0;
        }
        console.log(expandedmid);
        var aid = 0;
        var bid = 0;
        $(".view-tab[mid=" + mid + "]").each(function (k, e) {
            var tag = $(e).attr("tag");
            if ($(e).hasClass("active")) {
                // console.log(tag);
                switch (tag) {
                    case "h2h":
                        aid = $(".team-home-name[mid=" + mid + "]").attr("tid");
                        bid = $(".team-away-name[mid=" + mid + "]").attr("tid");
                        if ($(".h2h-view-box[mid=" + expandedmid + "]").children().length == 0) {
                            getH2H(aid, bid)
                        }
                        break;
                    case "formguide":

                        aid = $(".team-home-name[mid=" + mid + "]").attr("tid");
                        bid = $(".team-away-name[mid=" + mid + "]").attr("tid");
                        if ($(".st-home-view-box[mid=" + expandedmid + "]").children().length == 0 || $(".st-away-view-box[mid=" + expandedmid + "]").children().length == 0) {
                            getFormGuide(aid, bid);
                        }
                        break;
                    case "lineups":
                        // getLineups(mid);
                        getLineupFormation(mid);
                        break;
                    default:
                        break;

                }
            }
        })


    });


    $(".mtab-header-lu").on("click", function (e) {
        var mid = $(e.currentTarget).attr("mid");
        // getLineups(mid)
        getLineupFormation(mid);
    });

    function getLineups(mid) {
        $.when(getLinupeStructure()).then(function () {
            if ($(".lineups-list-home[mid=" + mid + "]").hasClass("no-data") || $(".lineups-list-away[mid=" + mid + "]").hasClass("no-data")) {
                $.ajax({
                    url: "/services/match/getlineups",
                    method: "GET",
                    data: {mid: mid},
                    dataType: "JSON"
                }).done(function (response) {
                    $.each(response, function (k, v) {
                        if (v.side == "home") {
                            var hbox = $(lineupsboxrow).clone();
                            $(hbox).find(".player-number").first().text(v.number);
                            $(hbox).find(".player-name").first().text(v.name);
                            $(hbox).find(".player-position").first().text(v.position);

                            $(".lineups-list-home[mid=" + mid + "]").append($(hbox));
                            $(".lineups-list-home[mid=" + mid + "]").removeClass("no-data");
                        } else {
                            var abox = $(lineupsboxrow).clone();
                            $(abox).find(".player-number").first().text(v.number);
                            $(abox).find(".player-name").first().text(v.name);
                            $(abox).find(".player-position").first().text(v.position);

                            $(".lineups-list-away[mid=" + mid + "]").append($(abox));
                            $(".lineups-list-away[mid=" + mid + "]").removeClass("no-data");
                        }
                    });
                });
            }
        })

    }

    function getLineupFormation(mid) {
        if ($("#lineups-box-" + mid).children().length == 0) {
            $.when(getLineBoard()).then(function () {
                $.ajax({
                    url: "/services/match/getlineupsformation",
                    method: "GET",
                    data: {mid: mid},
                    dataType: "JSON"
                }).done(function (response) {
                    var home = response.home;
                    var away = response.away;
                    var newboard = $(lineboard).clone();
                    $(newboard).find(".home-board-formation").first().text(response.home.formation.pattern);
                    $(newboard).find(".away-board-formation").first().text(response.away.formation.pattern);
                    $(newboard).find(".lb-home-logo").first().attr("src", $("#main-home-logo-" + mid).attr("src"));
                    $(newboard).find(".lb-away-logo").first().attr("src", $("#main-away-logo-" + mid).attr("src"));
                    var hgk = response.home.main.GK[0];
                    var hgkbox = $(newboard).find(".home-gk").first();
                    $(hgkbox).attr("id", "lineup-pid-" + hgk.pid);
                    $(hgkbox).find(".player-number").first().text(hgk.number);
                    $(hgkbox).find(".player-name").first().text(hgk.name);
                    var agk = response.away.main.GK[0];
                    var agkbox = $(newboard).find(".away-gk").first();
                    $(agkbox).attr("id", "lineup-pid-" + agk.pid);
                    $(agkbox).find(".player-number").first().text(agk.number);
                    $(agkbox).find(".player-name").first().text(agk.name);

                    var hdfmainbox = $(newboard).find(".home-df-row-content").first();
                    var hdfbox = $(hdfmainbox).find(".df-row-" + home.formation.DF).first();
                    $(hdfbox).removeClass("hide");

                    var adfmainbox = $(newboard).find(".away-df-row-content").first();
                    var adfbox = $(adfmainbox).find(".df-row-" + away.formation.DF).first();
                    $(adfbox).removeClass("hide");

                    var hmfmainbox = $(newboard).find(".home-cm-row-content").first();
                    var hmfbox = $(hmfmainbox).find(".cm-row-" + home.formation.MF).first();
                    $(hmfbox).removeClass("hide");

                    var amfmainbox = $(newboard).find(".away-cm-row-content").first();
                    var amfbox = $(amfmainbox).find(".cm-row-" + away.formation.MF).first();
                    $(amfbox).removeClass("hide");

                    var hfwmainbox = $(newboard).find(".home-ft-row-content").first();
                    var hfwbox = $(hfwmainbox).find(".ft-row-" + home.formation.FW).first();
                    $(hfwbox).removeClass("hide");

                    var afwmainbox = $(newboard).find(".away-ft-row-content").first();
                    var afwbox = $(afwmainbox).find(".ft-row-" + away.formation.FW).first();
                    $(afwbox).removeClass("hide");

                    $.each($(hdfbox).find(".info-box-pos"), function (k, el) {
                        $(el).attr("id", "lineup-pid-" + home.main.DF[k].pid);
                        $(el).find("span").first().text(home.main.DF[k].number);
                        $(el).find("p").first().text(home.main.DF[k].name);
                    });
                    $.each($(adfbox).find(".info-box-pos"), function (k, el) {
                        $(el).attr("id", "lineup-pid-" + away.main.DF[k].pid);
                        $(el).find("span").first().text(away.main.DF[k].number);
                        $(el).find("p").first().text(away.main.DF[k].name);
                    });

                    $.each($(hmfbox).find(".info-box-pos"), function (k, el) {
                        $(el).attr("id", "lineup-pid-" + home.main.MF[k].pid);
                        $(el).find("span").first().text(home.main.MF[k].number);
                        $(el).find("p").first().text(home.main.MF[k].name);
                    });
                    $.each($(amfbox).find(".info-box-pos"), function (k, el) {
                        $(el).attr("id", "lineup-pid-" + away.main.MF[k].pid);
                        $(el).find("span").first().text(away.main.MF[k].number);
                        $(el).find("p").first().text(away.main.MF[k].name);
                    });

                    $.each($(hfwbox).find(".info-box-pos"), function (k, el) {
                        $(el).attr("id", "lineup-pid-" + home.main.FW[k].pid);
                        $(el).find("span").first().text(home.main.FW[k].number);
                        $(el).find("p").first().text(home.main.FW[k].name);
                    });
                    $.each($(afwbox).find(".info-box-pos"), function (k, el) {
                        $(el).attr("id", "lineup-pid-" + away.main.FW[k].pid);
                        $(el).find("span").first().text(away.main.FW[k].number);
                        $(el).find("p").first().text(away.main.FW[k].name);
                    });

                    var hreservebox = $(newboard).find(".home-sub-content").first();
                    var hreserveplayer = $(hreservebox).find(".home-sub").first().clone();
                    $(hreservebox).empty();

                    var areservebox = $(newboard).find(".away-sub-content").first();
                    var areserveplayer = $(areservebox).find(".away-sub").first().clone();
                    $(areservebox).empty();

                    // console.log(home.reserve, away.reserve);
                    $.each(home.reserve, function (k, v) {
                        var rplayer = $(hreserveplayer).clone();
                        var number = v.number;
                        if (v.number == " ") {
                            number = "-";
                        }
                        $(rplayer).find(".num-sub-t").first().children().first().text(number);
                        $(rplayer).find(".name").first().children().first().text(v.name);
                        $(rplayer).find(".detail-player").first().children().first().text(v.position);
                        $(hreservebox).append($(rplayer));
                    });

                    $.each(away.reserve, function (k, v) {
                        var rplayer = $(areserveplayer).clone();
                        var number = v.number;
                        if (v.number == " ") {
                            number = "-";
                        }
                        $(rplayer).find(".num-sub-t").first().children().first().text(number);
                        $(rplayer).find(".name").first().children().first().text(v.name);
                        $(rplayer).find(".detail-player").first().children().first().text(v.position);
                        $(areservebox).append($(rplayer));
                    });


                    $("#lineups-box-" + mid).append($(newboard));

                });
            });
        }
    }

    $(".mtab-header-h2h").on("click", function (e) {
        var mid = $(e.currentTarget).attr("mid");
        var aid = $(".team-home-name[mid=" + mid + "]").attr("tid");
        var bid = $(".team-away-name[mid=" + mid + "]").attr("tid");
        if ($(".h2h-view-box[mid=" + expandedmid + "]").children().length == 0) {
            getH2H(aid, bid)
        }
    });

    function getH2H(aid, bid) {
        $.when(getH2HStructure()).then(function () {
            $.ajax({
                url: "/services/match/geth2h",
                method: "GET",
                data: {hid: aid, aid: bid},
                dataType: "JSON"
            }).done(function (response) {
                subTabViewFill($(".h2h-view-box[mid=" + expandedmid + "]"), response, aid, "h2h");

            });
        });
    }

    $(".mtab-header-st").on("click", function (e) {
        var mid = $(e.currentTarget).attr("mid");
        var aid = $(".team-home-name[mid=" + mid + "]").attr("tid");
        var bid = $(".team-away-name[mid=" + mid + "]").attr("tid");
        if ($(".st-home-view-box[mid=" + expandedmid + "]").children().length == 0 || $(".st-away-view-box[mid=" + expandedmid + "]").children().length == 0) {
            getFormGuide(aid, bid);
        }
    });

    function getFormGuide(aid, bid) {
        $.when(getH2HStructure()).then(function () {
            $.ajax({
                url: "/services/match/getformguide",
                method: "GET",
                data: {tid: aid},
                dataType: "JSON"
            }).done(function (response) {
                subTabViewFill($(".st-home-view-box[mid=" + expandedmid + "]"), response, aid, "fghome");

            });
            $.ajax({
                url: "/services/match/getformguide",
                method: "GET",
                data: {tid: bid},
                dataType: "JSON"
            }).done(function (response) {
                subTabViewFill($(".st-away-view-box[mid=" + expandedmid + "]"), response, bid, "fgaway");

            });
        });

    }

    function subTabViewFill(el, data, tid, boxtype) {
        var lid = 0;
        var leaguebox = null;
        var winsum = 0;
        var drawsum = 0;
        var losesum = 0;
        $.each(data.data, function (k, v) {

            lid = v.league.id;
            leaguebox = $(h2hstructure).clone();

            $(leaguebox).find(".h2h-header-league-name").first().text(v.league.name);
            var leaguelogo = "/images/football.png";
            if (v.league.logo != null) {
                leaguelogo = v.league.logo;
            }
            $(leaguebox).find("img").first().attr("src", host + leaguelogo);

            $.each(v.matches, function (mk, matchdata) {
                var match = $(h2hbodystructure).clone();
                $(match).find(".h2h-home-name").first().text(matchdata.teamHomeEn);
                $(match).find(".h2h-away-name").first().text(matchdata.teamAwayEn);
                if (v.hdp > 0) {
                    $(match).find(".h2h-away-name").first().addClass("team-bet-color");
                } else if (v.hdp < 0) {
                    $(match).find(".h2h-home-name").first().addClass("team-bet-color");
                }
                var homelogo = "/images/team/team_default_32x32.png";
                if (matchdata.teamHomePath != null) {
                    homelogo = matchdata.teamHomePath;
                }
                var awaylogo = "/images/team/team_default_32x32.png";
                if (matchdata.teamAwayPath != null) {
                    awaylogo = matchdata.teamAwayPath;
                }
                $(match).find(".h2h-home-logo").first().attr("src", host + homelogo);
                $(match).find(".h2h-away-logo").first().attr("src", host + awaylogo);


                var mdate = moment(matchdata.time_match, "YYYY-MM-DD HH:mm:ss");
                $(match).find(".date-match").first().text(mdate.format("DD MMM YY"));
                $(match).find(".time-match").first().text(mdate.format("HH:mm"));
                $(match).find(".home-score-match").first().text(matchdata.homeScore);
                $(match).find(".away-score-match").first().text(matchdata.awayScore);


                var oddsresult = "draw-status";
                if (matchdata.odds_result == "w") {
                    oddsresult = "win-status";
                } else if (matchdata.odds_result == "l") {
                    oddsresult = "lose-status";
                } else if (matchdata.odds_result == "-") {
                    $(match).find(".odds-match").first().children().first().addClass("hide");
                }
                $(match).find(".odds-match").first().children().first().addClass(oddsresult);

                var matchresult = "draw-status";

                if (matchdata.result == "w") {
                    matchresult = "win-status";
                } else if (matchdata.result == "l") {
                    matchresult = "lose-status";
                }
                $(match).find(".result-match").first().children().first().addClass(matchresult);

                $(leaguebox).append($(match));
            });
            $(el).append($(leaguebox));
        });


        // console.log(data.result_summary);
        var mid = $(el).attr("mid");
        if (boxtype == "h2h") {
            $(".h2h-win-summary[mid=" + mid + "]").text(data.result_summary.win);
            $(".h2h-draw-summary[mid=" + mid + "]").text(data.result_summary.draw);
            $(".h2h-lose-summary[mid=" + mid + "]").text(data.result_summary.lose);
        }
        if (boxtype == "fghome") {
            $(".fg-home-win[mid=" + mid + "]").text(data.result_summary.win);
            $(".fg-home-draw[mid=" + mid + "]").text(data.result_summary.draw);
            $(".fg-home-lose[mid=" + mid + "]").text(data.result_summary.lose);
        }
        if (boxtype == "fgaway") {
            $(".fg-away-win[mid=" + mid + "]").text(data.result_summary.win);
            $(".fg-away-draw[mid=" + mid + "]").text(data.result_summary.draw);
            $(".fg-away-lose[mid=" + mid + "]").text(data.result_summary.lose);
        }

    }


    $(".mtab-header-re").on("click", function (e) {
        var mid = $(e.currentTarget).attr("mid");
        if ($(".review-list[mid=" + expandedmid + "]").children().length == 0) {
            getReview(mid);
        }
        if ($(".live-hdp[mid=" + mid + "]").attr("hdp") == '-') {
            showVoteBar(mid);
        }
        if (fbid != null) {
            checkBet(mid, fbid);
        }
    });

    function getReview(mid) {
        $.when(getReviewStructure()).then(function () {
            $.ajax({
                url: "/services/match/getearlycomment",
                method: "GET",
                data: {mid: mid},
                dataType: "JSON"
            }).done(function (response) {

                $.each(response, function (k, v) {

                    var rerow = $(reviewbodystruct).clone();
                    if (v.user_type == 'reviewer') {
                        $(rerow).find(".review-avatar").first().attr("src", host + v.owner.path);
                    } else if (v.user_type == 'facebook') {
                        $(rerow).find(".review-avatar").first().attr("src", "https://graph.facebook.com/v3.2/" + v.owner.fb_uid + "/picture?type=small");
                    }
                    var side = $(".team-home-name[mid=" + mid + "]").text();
                    if (v.team == "away") {
                        side = $(".team-away-name[mid=" + mid + "]").text();
                    }
                    $(rerow).find(".review-at-side").first().text("@" + side.toLowerCase().replace(/\s/g, ""));
                    $(rerow).find(".review-text-body").first().text(v.comment);
                    $(".review-list[mid=" + mid + "]").append($(rerow));
                    if (k == 0) {
                        $(".read-review-more[mid=" + mid + "]").removeClass("hide");
                    }


                });
            });

        });

    }

    $(".mtab-header-de").on("click", function (e) {
        var mid = $(e.currentTarget).attr("mid");
        expandedmid = mid;
        if ($(".content-event-match[mid=" + expandedmid + "]").children().length == 0) {
            getLiveEvent();
        }

    });


    function getLiveEvent() {
        var midlist = expandedmid;
        var icon = {
            goal: "goal-score",
            penalty: "penalty-score",
            "own goal": "own-goal",
            "yellow card": "yellow-card",
            "red card": "red-card",
            "yellow to red": "group-yellow-card",
            substutitions: "substitute-player",
            "penalty miss": "penalty-none-score"
        };
        var homeboxori = '<div class="full-width line-content-all">';
        homeboxori += '    <div class="col-home-live">';
        homeboxori += '    <div class="box-football-icon">';
        homeboxori += '                <div class="icon-mark"></div>';
        homeboxori += "    </div>";
        homeboxori += '            <div class="min-live"></div>';
        homeboxori += '            <div class="score-live"></div>';
        homeboxori += '            <div class="name-live name-normal hide"> </div>';
        homeboxori += '            <div class="name-live name-sub-out hide">';
        homeboxori += '                <span class="substitute-player-out"></span>';
        homeboxori += '                <p class="out-mark"></p>';
        homeboxori += "            </div>";
        homeboxori += '            <div class="name-live name-sub-in hide">';
        homeboxori += '                <span class="substitute-player-in"></span>';
        homeboxori += '                <p class="in-mark"></p>';
        homeboxori += "            </div>";
        homeboxori += "        </div>";
        homeboxori += "    </div>";

        var awayboxori = '<div class="full-width line-content-all">';
        awayboxori += '        <div class="col-away-live pull-right">';
        awayboxori += '            <div class="name-live name-normal hide"></div>';
        awayboxori += '            <div class="name-live name-sub-in hide">';
        awayboxori += '                <span class="substitute-player-in"></span>';
        awayboxori += '                <p class="in-mark"></p>';
        awayboxori += "            </div>";
        awayboxori += '            <div class="name-live name-sub-out hide">';
        awayboxori += '                <span class="substitute-player-out"></span>';
        awayboxori += '                <p class="out-mark"></p>';
        awayboxori += "            </div>";
        awayboxori += '            <div class="score-live"></div>';
        awayboxori += '            <div class="min-live"></div>';
        awayboxori += '            <div class="box-football-icon">';
        awayboxori += '                <div class="icon-mark"></div>';
        awayboxori += "            </div>";
        awayboxori += "        </div>";
        awayboxori += "    </div>";

        var homeboxdom = $.parseHTML(homeboxori);
        var awayboxdom = $.parseHTML(awayboxori);


        if (midlist != 0) {
            $.ajax({
                url: "/services/matchevents/geteventsinlist",
                data: {
                    midlist: midlist
                },
                dataType: "JSON",
                method: "GET"
            }).done(function (response) {
                $.each(response, function (midk, matcevent) {
                    $(".content-event-match[mid=" + midk + "]").empty();
                    $.each(matcevent, function (k, v) {
                        var eventbox = $(homeboxdom).clone();
                        if (v.side == "away") {
                            eventbox = $(awayboxdom).clone();
                        }
                        $(eventbox).find(".icon-mark").first().addClass(icon[v.event]);
                        $(eventbox).find(".min-live").first().text(v.at_minute + "'");
                        $(eventbox).find(".score-live").first().text(v.score);
                        if (v.event == "substutitions") {
                            $(eventbox).find(".name-sub-in").first().removeClass("hide");
                            $(eventbox).find(".name-sub-out").first().removeClass("hide");
                            $(eventbox).find(".out-mark").first().text(v.player);
                            $(eventbox).find(".in-mark").first().text(v.assist);
                        } else {
                            var etxt = v.player;
                            if (v.assist != "") {
                                etxt = v.player + "(" + v.assist + ")";
                            }
                            if (v.side == "away") {
                                etxt = v.player;
                                if (v.assist != "") {
                                    etxt = "(" + v.assist + ")" + v.player;
                                }
                            }

                            $(eventbox).find(".name-normal").first().text(etxt);
                            $(eventbox).find(".name-normal").removeClass("hide");
                        }
                        $(".content-event-match[mid=" + midk + "]").append($(eventbox));
                    });
                });
            });
        }
    }


    $(".mtab-header-ls").on("click", function (e) {
        var mid = $(e.currentTarget).attr("mid");
        expandedmid = mid;
        if ($(".statistics-mark[mid=" + expandedmid + "]").children().length == 0) {
            getStatUpdate();
        }

    });


    function getStatUpdate() {
        var midlist = expandedmid;
        var rowdom = "<ul>\n" +
            "<li class=\"home-st-point\"><span>0</span></li>\n" +
            "<li class=\"match-st-type\">-</li>\n" +
            "<li class=\"away-st-point\"><span>-</span></li>\n" +
            "</ul>";

        if (midlist != 0) {
            $.ajax({
                url: "/services/matchevents/getstatinlist",
                data: {
                    midlist: midlist
                },
                dataType: "JSON",
                method: "GET"
            }).done(function (response) {
                // console.log(response);

                $.each(response, function (mid, val) {
                    // console.log(mid, val);
                    $(".statistics-mark[mid=" + mid + "]").empty();
                    $.each(val, function (k, v) {
                        var row = $.parseHTML(rowdom);
                        $(row).find(".match-st-type").first().text(v.type);
                        var hval = v.hval;
                        var aval = v.aval;
                        if (v.type == "ball possession") {
                            hval += "%";
                            aval += "%";

                        }
                        $(row).find(".home-st-point").first().children().first().text(hval);
                        $(row).find(".away-st-point").first().children().first().text(aval);
                        if (v.hval > v.aval) {
                            $(row).find(".home-st-point").first().addClass("high-num");
                        } else if (v.hval < v.aval) {
                            $(row).find(".away-st-point").first().addClass("high-num");
                        }

                        $(".statistics-mark[mid=" + mid + "]").append($(row));
                    });
                });
            });
        }
    }


    function getHLUpdate() {
        var midlist = "0";
        if (queuemid.length == 0) {
            $.each($(".at-minute-mark"), function (k, v) {
                var state = $(v).attr("state");
                var mid = $(v).attr("mid");
                if (state != 4) {
                    queuemid.push(mid);
                    if (midlist == "0") {
                        midlist = mid;
                    } else {
                        midlist += "," + mid;
                    }
                }
            });
        } else {
            for (i = 0; i <= queuemid.length; i++) {
                if (midlist == "0") {
                    midlist = queuemid[i];
                } else {
                    midlist += "," + queuemid[i];
                }
            }
        }
        // console.log(queuemid.length, queuemid);

        if (midlist != "0") {
            $.ajax({
                url: "/services/highlight/gethlupdate",
                data: {
                    midlist: midlist
                },
                dataType: "JSON",
                method: "GET"
            }).done(function (response) {
                // console.log(response);
                $.each(response, function (k, v) {
                    // console.log(v);
                    $(".at-minute-mark[mid=" + v.id + "]").attr("state", v.state);
                    $(".at-minute-mark[mid=" + v.id + "]").attr("ht", v.ht_start);
                    $(".live-h-score[mid=" + v.id + "]").text(v.live_score_home);
                    $(".live-a-score[mid=" + v.id + "]").text(v.live_score_away);
                    // console.log($(".at-minute-mark[mid=" + v.id + "]")[0].outerHTML);
                    if (v.hdp != null) {
                        // $(".live-h-hdp[mid=" + v.id + "]").text(numeral(v.hdp.home_water_bill).format("0.00"));
                        $(".live-hdp[mid=" + v.id + "]").text(v.hdp.handicap);
                        $(".live-hdp-play[mid=" + v.id + "]").text(v.hdp.handicap);
                        // $(".live-a-hdp[mid=" + v.id + "]").text(numeral(v.hdp.away_water_bill).format("0.00"));
                    }
                });
                $.when(timestate()).then(function () {
                    movematchbox()
                })

            });
        }
    }


    function timestate() {
        moment.locale();
        var mn = moment();
        mn.tz("Asia/Bangkok");
        var aminute = 0;

        // $(".hdp-float").addClass("hide");
        $.each($(".at-minute-mark"), function (k, v) {
            // console.log(k, $(v).attr("at"));
            var stateTime = moment.tz($(v).attr("at"), "Asia/Singapore");
            console.log($(v).attr("mid"), mn.diff(stateTime, "minute"));
            var state = $(v).attr("state");
            var mid = $(v).attr("mid");
            var mainscore = $(".main-score[mid=" + mid + "]");

            if (state == 17 || state == 0) {

            } else if (state == 4) {
                $(v).text("FT");
                $(v).addClass("end-time");
                $(".row-hdp[mid=" + mid + "]").addClass("hide");

                $(mainscore).removeClass("hide");
                $(mainscore).removeClass("score-live");
                $(mainscore).addClass("score-live-ft");
                if (livelist.indexOf(mid) >= 0) {
                    livelist.splice(livelist.indexOf(mid), 1);
                    if (endlist.indexOf(mid) < 0) {
                        endlist.push(mid);
                    }
                }
                if (queuemid.indexOf(mid) >= 0) {
                    queuemid.splice(queuemid.indexOf(mid), 1);
                }
            } else if (state == 1 || state == 2 || state == 3 || state == 8) {
                $(v).addClass("time-live");
                $(".row-hdp[mid=" + mid + "]").addClass("hide");
                $(mainscore).removeClass("hide");
                if (livelist.indexOf(mid) < 0) {
                    livelist.push(mid);
                }
            }

            if (state == 2) {
                $(v).text("HT");
            }

            if (state == 8) {
                $(v).text("AET");
            }

            if (state == 13) {
                $(v).text("Cancel");
            }

            if (state == 3) {
                var htstart = moment.tz($(v).attr("ht"), "Asia/Bangkok");
                aminute = 45 + mn.diff(htstart, "minute");
                $(v).text(aminute + "'");
                if (aminute > 90) {
                    aminute = 90;
                    $(v).text("90+");
                }

            }

            if (state == 1) {
                aminute = mn.diff(stateTime, "minute");
                $(v).text(aminute + "'");
                if (aminute > 45) {
                    aminute = 45;
                    $(v).text("45+");
                }
            }
        });

        // console.log('live', livelist);
        // console.log('end', endlist);
    }


    function movematchbox() {
        $.each(livelist, function (k, mid) {
            if (!$(".match-box[mid=" + mid + "]").parent().hasClass("league-live-banner")) {
                var lid = $(".match-box[mid=" + mid + "]").parent().attr("lid");
                $(".match-box[mid=" + mid + "]").detach().appendTo(".league-live-banner[lid=" + lid + "]");
            }
        });
        // $.each(endlist, function (k, mid) {
        //     if (!$(".match-box[mid=" + mid + "]").parent().hasClass("league-end-banner")) {
        //         var lid = $(".match-box[mid=" + mid + "]").parent().attr("lid");
        //         $(".match-box[mid=" + mid + "]").detach().appendTo(".league-end-banner[lid=" + lid + "]");
        //     }
        // });

        var liveboxs = $(document).find(".main-live-box");
        $.each(liveboxs, function (k, e) {
            if ($(e).children().length <= 1) {
                $(e).addClass("hide");
            } else {
                $(e).removeClass("hide");
            }
        })

    }


    $(".vote-match").on("click", function (e) {
        var mid = $(e.currentTarget).attr("mid");
        var side = $(e.currentTarget).attr("side");
        checkuser().then(function () {

            $.ajax({
                url: "/services/match/addvote",
                method: "GET",
                data: {mid: mid, fb_uid: fbid, side: side, fb_token: token},
                dataType: "JSON"
            }).done(function (response) {
                if (response.success) {
                    // Swal.fire(
                    //     'Done',
                    //     response.desc,
                    //     'success'
                    // );
                    showVoteBar(mid);
                } else {
                    Swal.fire(
                        'Error!',
                        response.desc,
                        'error'
                    );
                }
            });
        });
    });

    function checkuser() {
        console.log("check user data");
        return new Promise(function (resolve, reject) {
            if (fbid != undefined) {
                if (token == null) {
                    FB.getLoginStatus(function (response) {
                        console.log("Request FB data", response);
                        if (response.status == "connected") {
                            fbid = response.authResponse.userID;
                            token = response.authResponse.accessToken;
                            resolve();
                        }
                    });
                } else {
                    resolve();
                }
            } else {
                // reject();
                resolve();
            }

        })
    }


    function showVoteBar(mid) {
        $.ajax({
            url: "/services/match/getvote",
            method: "GET",
            data: {mid: mid, fb_uid: fbid},
            dataType: "JSON"
        }).done(function (response) {
            var allvote = response.home + response.draw + response.away;
            if (allvote != 0) {
                var home = Math.round(response.home / allvote * 100);
                var draw = Math.round(response.draw / allvote * 100);
                var away = Math.round(response.away / allvote * 100);
                console.log(home, draw, away);
                $(".vote-bar-home[mid=" + mid + "]").text(home + "%");
                $(".vote-bar-draw[mid=" + mid + "]").text(draw + "%");
                $(".vote-bar-away[mid=" + mid + "]").text(away + "%");

                $(".vote-bar-home[mid=" + mid + "]").css({'width': home + '%'});
                $(".vote-bar-draw[mid=" + mid + "]").css({'width': draw + '%'});
                $(".vote-bar-away[mid=" + mid + "]").css({'width': away + '%'});
            }

            if (response.voted) {
                $(".match-vote-bar[mid=" + mid + "]").removeClass("hide");
            }
        });

    }


    $(".bet-modal").on("click", function (e) {
        var side = $(e.currentTarget).attr("side");
        var mid = $(e.currentTarget).attr("mid");
        betmid = mid;
        var hlogo = $("#main-home-logo-" + mid).attr("src");
        var alogo = $("#main-away-logo-" + mid).attr("src");
        var hname = $(".team-home-name[mid=" + mid + "]").text();
        var aname = $(".team-away-name[mid=" + mid + "]").text();
        $("#home-comment-box").val("");
        $("#away-comment-box").val("");
        if (side == "home") {
            $(".home-logo-mark").attr("src", hlogo);
            $("#bet-modal-hname").text(hname);
        } else {
            $(".away-logo-mark").attr("src", alogo);
            $("#bet-modal-aname").text(aname);
        }

        $('.bet-waiting-img').addClass("hide");

    });

    $(".select-coin").on("click", function (e) {
        var val = $(e.currentTarget).attr("value");
        betval = val;
        $(".bet-coin").text($(e.currentTarget).text());
    });

    $(".bet-save").on("click", function (e) {
        var side = $(e.currentTarget).attr("side");
        var hdp = $(".live-hdp[mid=" + betmid + "]").attr("hdp");
        var reward = $(".live-hdp[mid=" + betmid + "]").attr("home");
        var comment = $("#home-comment-box").val();
        if (side == "away") {
            reward = $(".live-hdp[mid=" + betmid + "]").attr("away");
            comment = $("#away-comment-box").val();
        }
        checkuser().then(function (resolve, reject) {
            // console.log(resolve, reject);
            $('.bet-waiting-img').removeClass("hide");
            $.ajax({
                url: "/services/bet/addbet",
                method: "GET",
                data: {
                    mid: betmid,
                    fb_uid: fbid,
                    team: side,
                    coin: betval,
                    hdp: hdp,
                    reward: reward,
                    comment: comment,
                    fb_token: token
                },
                dataType: "JSON"
            }).done(function (response) {
                console.log(response);
                if (response.success) {
                    Swal.fire(
                        'Success.',
                        response.desc,
                        'success'
                    );
                    $(".modal-vote").modal("hide");

                    updateUserResource();
                    checkBet(betmid, fbid);
                } else {
                    Swal.fire(
                        'Error!',
                        response.desc,
                        'error'
                    );
                }
                $('.bet-waiting-img').addClass("hide");
            });

        });
    }).error(function () {
        console.log("Reject");
    });


    function updateUserResource() {
        $.ajax({
            url: "/services/user/getuser",
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
            if (response.session_active) {
                $("#user-current-coin").text(response.user_data.coin);
                $("#user-current-gold").text(response.user_data.gold);
                $("#user-current-coupon").text(response.user_data.coupon);
            }
        })
    }

    function checkBet(mid, fbid) {
        $.ajax({
            url: "/services/bet/checkbet",
            method: "GET",
            data: {mid: mid, fb_uid: fbid},
            dataType: "JSON"
        }).done(function (response) {
            console.log(response);
            if (response.bet) {
                if (response.bet_data.team == "home") {
                    $(".nav-review-home[mid=" + mid + "]").addClass("active");
                } else {
                    $(".nav-review-away[mid=" + mid + "]").addClass("active");
                }
            }
        });
    }
})
;
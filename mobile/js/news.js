$(document).ready(function () {
    var host = "https://www.ngoal.com/";
    var page = 0;
    var laststop = 0;
    var cleague = 'premier league';
    var needrequest = true;
    var nomorenews = false;
    var reloadhighlight = false;
    var leaguelist = {
        'premier league': "English Premier League",
        "la liga": "Spanish Primera Division",
        "league 1": "French Le Championnat Ligue 1",
        "bundesliga": "German Bundesliga",
        "serie a": "Italian Serie A",
        "thai": "Thai League 1",
        "ucl": "UEFA Champions League",
        "europa": "UEFA Europa League"
    };
    var leaguelid = {
        'premier league': 1,
        "la liga": 18,
        "league 1": 36,
        "bundesliga": 44,
        "serie a": 27,
        "thai": 590,
        "ucl": 105,
        "europa": 608
    };
    var leaguegroup = {
        'premier league': 'N',
        "la liga": 'N',
        "league 1": 'N',
        "bundesliga": 'N',
        "serie a": 'N',
        "thai": 'N',
        "ucl": 'Y',
        "europa": 'Y'
    };
    var ctab = 'news';
    moment.tz.add("Asia/Bangkok|LMT BMT +07|-6G.4 -6G.4 -70|012|-3D8SG.4 1C000|15e6");
    moment.tz.add("Asia/Singapore|LMT SMT +07 +0720 +0730 +09 +08|-6T.p -6T.p -70 -7k -7u -90 -80|01234546|-2M0ST.p aIM0 17anT.p l5XE 17bO 8Fyu 1so1u|56e5");

    var newsboxori = $("#news-main-box").children().first().clone();
    var standingtable = $(".standing-group-table").first().clone();
    $(standingtable).find(".standing-order").first().empty();
    var standingpos = $(".std-row").first().clone();
    $("#standing-box").empty();
    var fixturebox = $(".box-match-tournament").first().clone();
    var fixturematch = $(".box-match-list").first().clone();
    $(fixturebox).find(".fix-match").first().empty();
    $("#fixture-box").empty();
    var tprows = $(".tp-rows").first().clone();
    $("#tp-list").empty();


    initdate();

    function initdate() {
        moment.locale();
        var mn = moment();
        mn.tz("Asia/Bangkok");
        $(document).find(".post-at").each(function (k, v) {
            var postat = $(v).attr("at");
            var posttime = moment(postat, "YYYY-MM-DD HH:mm:ss");
            $(v).text(posttime.fromNow());
        });
    }

    $(window).scroll(function () {
        console.log($(window).scrollTop(), $(window).height(), $(document).height());
        var lastbox = $("#news-main-box").children().last();
        console.log(isOnScreen(lastbox));
        // if ($(window).scrollTop() + $(window).height() == $(document).height()) {
        //     // alert("bottom!");
        //     page++;
        //     getNews();
        // }

        if (isOnScreen(lastbox) && needrequest) {
            if (ctab == "news") {
                page++;
                getNews();
            }
        }

        if ($(window).scrollTop() > laststop) {
            $("#footer-mark").addClass("hide");
        } else {
            $("#footer-mark").removeClass("hide");
        }
        laststop = $(window).scrollTop();




    });

    $(".league-news").on("click", function (e) {
        var leaguename = $(e.currentTarget).attr("lname");
        var logo = $(e.currentTarget).find("img").first().attr("src");
        if (cleague != leaguename) {
            cleague = leaguename;
            $("#news-main-box").empty();
            $("#standing-box").empty();
            $("#fixture-box").empty();
            $("#tp-list").empty();
            page = 0;
            nomorenews = false;
            reloadhighlight = true;
            $("#league-news-logo").attr("src", logo);
            $("#league-news-name").text(leaguelist[leaguename]);

            switch (ctab) {
                case 'news':
                    getNews();
                    break;
                case 'standing':
                    getStanding();
                    break;
                case 'fixture':
                    getFixture();
                    break;
                case 'player':
                    getTopPlayer();
                    break;
                default:
                    break;
            }

        }
    });

    $(".tab-select").on('click', function (e) {
        var ntab = $(e.currentTarget).attr("attab");
        if (ntab != ctab) {
            ctab = ntab;
            switch (ctab) {
                case 'news':
                    // console.log($("#news-main-box").children().size())
                    if ($("#news-main-box").children().size() == 0) {
                        getNews();
                    }
                    break;
                case 'standing':
                    if ($("#standing-box").children().size() == 0) {
                        getStanding();
                    }
                    break;
                case 'fixture':
                    if ($("#fixture-box").children().size() == 0) {
                        getFixture();
                    }
                    break;
                case 'player':
                    if ($("#tp-list").children().size() == 0) {
                        getTopPlayer();
                    }
                    break;
                default:
                    break;
            }
        }
    });


    function getNews() {
        if (!nomorenews) {
            $("#loading-img").removeClass("hide");
            needrequest = false;
            $.ajax({
                url: "/services/news/getbycat",
                data: {
                    cat: cleague,
                    page: page
                },
                dataType: "JSON",
                method: "GET"
            }).done(function (response) {
                console.log(response);
                $.each(response, function (k, v) {
                    if (k == 0 && reloadhighlight) {
                        var hlbox = $(".highlight-news").first();
                        $(hlbox).find(".link_wrap").first().attr("href", "/mobile/news-info?id=" + v.newsId);
                        $(hlbox).find(".hl-img").first().attr("src", host + v.imageLink);
                        $(hlbox).find(".hl-headline").first().text(v.titleTh);
                        reloadhighlight=false;
                    }
                    var newsbox = $(newsboxori).clone();
                    $(newsbox).find(".link_wrap").first().attr("href", "/mobile/news-info?id=" + v.newsId);
                    $(newsbox).find("img").first().attr("src", host + v.imageLink);
                    $(newsbox).find(".media-heading").first().text(v.titleTh);
                    var posttime = moment(v.created_at, "YYYY-MM-DD HH:mm:ss","UTC");
                    posttime.tz("Asia/Bangkok");
                    $(newsbox).find(".post-at").first().attr("at", posttime.format("YYYY-MM-DD HH:mm:ss"));
                    $(newsbox).find(".view-box").first().text(v.readCount);
                    $("#news-main-box").append($(newsbox));
                });
                initdate();
                needrequest = true;
                if (response.length == 0) {
                    alert("no more news!!!");
                    nomorenews = true;
                }
                $("#loading-img").addClass("hide");
            });
        }
    }

    function isOnScreen(elem) {
        // if the element doesn't exist, abort
        if (elem.length == 0) {
            return;
        }
        var $window = jQuery(window)
        var viewport_top = $window.scrollTop()
        var viewport_height = $window.height()
        var viewport_bottom = viewport_top + viewport_height
        var $elem = jQuery(elem)
        var top = $elem.offset().top
        var height = $elem.height()
        var bottom = top + height

        return (top >= viewport_top && top < viewport_bottom) ||
            (bottom > viewport_top && bottom <= viewport_bottom) ||
            (height > viewport_height && top <= viewport_top && bottom >= viewport_bottom)
    }



    function getStanding() {
        $("#loading-img").removeClass("hide");
        $.ajax({
            url: "/services/league/getleaguestanding",
            data: {
                lid: leaguelid[cleague],
                group: leaguegroup[cleague]
            },
            dataType: "JSON",
            method: "GET"
        }).done(function (response) {
            var arrgroup = [];
            if (leaguegroup[cleague] == 'N') {
                var stdtable = $(standingtable).clone();
                $.each(response, function (k, v) {
                    var pos = $(standingpos).clone();
                    $(pos).find(".std-pos").first().text((k + 1));
                    $(pos).find("img").first().attr("src", v.path);
                    $(pos).find(".std-tname").first().text(v.name_en);
                    $(pos).find(".std-p").first().text(v.PI);
                    $(pos).find(".std-gd").first().text(v.GD);
                    $(pos).find(".std-pts").first().text(v.totalPts);
                    $(stdtable).find(".standing-order").first().append($(pos));
                });
                $("#standing-box").append($(stdtable));

            } else {
                var stdtable = $(standingtable).clone();
                var atgroup = "";
                var atpos = 0;
                $.each(response, function (k, v) {
                    if (k == 0) {
                        atgroup = v.groups;
                    }
                    if (atgroup != v.groups) {
                        $(stdtable).find(".group-title").first().text("Group " + atgroup);
                        $("#standing-box").append($(stdtable));
                        stdtable = $(standingtable).clone();
                        atgroup = v.groups;
                        atpos = 0;
                    }
                    var pos = $(standingpos).clone();
                    $(pos).find(".std-pos").first().text((atpos + 1));
                    $(pos).find("img").first().attr("src", v.path);
                    $(pos).find(".std-name").first().text(v.name_en);
                    $(pos).find(".std-p").first().text(v.PI);
                    $(pos).find(".std-gd").first().text(v.GD);
                    $(pos).find(".std-pts").first().text(v.totalPts);
                    $(stdtable).find(".standing-order").first().append($(pos));
                    atpos++;
                });
                $(stdtable).find(".group-title").first().text("Group " + atgroup);
                $("#standing-box").append($(stdtable));

            }


            $("#loading-img").addClass("hide");
        });
    }

    function getFixture() {
        $("#loading-img").removeClass("hide");
        $.ajax({
            url: "/services/league/getleaguefixture",
            data: {
                lid: leaguelid[cleague]
            },
            dataType: "JSON",
            method: "GET"
        }).done(function (response) {
            var matchdatebox = $(fixturebox).clone();
            var cdate = moment();
            var mdate = moment();
            $.each(response, function (k, v) {
                mdate = moment.tz(v.time_match, "Asia/Singapore");
                mdate.tz("Asia/Bangkok");
                if (k == 0) {
                    cdate = mdate.clone();
                }
                // console.log(k,mdate.format("DD MMM YYYY"), cdate.format("DD MMM YYYY"));
                if (cdate.format("DD MMM YYYY") != mdate.format("DD MMM YYYY")) {
                    $(matchdatebox).find(".fix-date").first().text(cdate.format("DD MMM YYYY"));
                    $("#fixture-box").append($(matchdatebox));
                    matchdatebox = $(fixturebox).clone();
                    cdate = mdate.clone();
                }
                var newfixmatch = $(fixturematch).clone();
                $(newfixmatch).find(".fix-time").first().text(mdate.format("HH:mm"));
                $(newfixmatch).find(".fix-hlogo").first().attr("src", v.pathTeamHome);
                $(newfixmatch).find(".fix-alogo").first().attr("src", v.pathTeamAway);
                $(newfixmatch).find(".fix-hname").first().text(v.nameTeamHome);
                $(newfixmatch).find(".fix-aname").first().text(v.nameTeamAway);
                $(newfixmatch).find(".home-score-match").first().text(v.home_end_time_score);
                $(newfixmatch).find(".away-score-match").first().text(v.away_end_time_score);
                $(matchdatebox).find(".fix-match").first().append($(newfixmatch));
            });
            $(matchdatebox).find(".fix-date").first().text(cdate.format("DD MMM YYYY"));
            $("#fixture-box").append($(matchdatebox));
            $("#loading-img").addClass("hide");
        });
    }


    function getTopPlayer() {
        $("#loading-img").removeClass("hide");
        $.ajax({
            url: "/services/league/getleaguetopplayer",
            data: {
                lid: leaguelid[cleague]
            },
            dataType: "JSON",
            method: "GET"
        }).done(function (response) {
            var teamdata = response['team'];
            $.each(response['player'], function (k, v) {
                var row = $(tprows).clone();
                $(row).find(".tp-rank").first().text((k + 1));
                if (v.path == null || v.path.length == 0) {
                    $(row).find(".tp-img").first().attr("src", host + "/images/avatar.png");
                } else {
                    $(row).find(".tp-img").first().attr("src", host + v.path);
                }
                $(row).find(".tp-name").first().text(v.name);
                $(row).find(".tp-goal").first().text(v.goal);
                $(row).find(".tp-tlogo").first().attr("src", host + teamdata[v.id]['sc']);
                $(row).find(".tp-tname").first().text(teamdata[v.id]['tname']);
                $("#tp-list").append($(row));
            });

            $("#loading-img").addClass("hide");
        });
    }



});
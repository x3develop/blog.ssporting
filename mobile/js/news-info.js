$(document).ready(function () {
    var host = "https://www.ngoal.com/";
    moment.tz.add("Asia/Bangkok|LMT BMT +07|-6G.4 -6G.4 -70|012|-3D8SG.4 1C000|15e6");
    moment.tz.add("Asia/Singapore|LMT SMT +07 +0720 +0730 +09 +08|-6T.p -6T.p -70 -7k -7u -90 -80|01234546|-2M0ST.p aIM0 17anT.p l5XE 17bO 8Fyu 1so1u|56e5");
    var laststop = 0;
    var needrequest = true;
    var newsrowori = $(".news-row").first().clone();
    var relatedrowori = $(".content-item-recent").first().clone();
    var nextnews = $(relatedrowori).attr("newsid");





    $(window).scroll(function () {

        var lastbox = $(".content-item-recent").children().last();
        console.log(isOnScreen(lastbox));

        if (isOnScreen(lastbox) && needrequest) {
            // getWithRelated();
        }

        if ($(window).scrollTop() > laststop) {
            $("#footer-mark").addClass("hide");
        } else {
            $("#footer-mark").removeClass("hide");
        }
        laststop = $(window).scrollTop();
    });


    initdate();

    function initdate() {
        moment.locale();
        var mn = moment();
        mn.tz("Asia/Bangkok");
        $(document).find(".post-at").each(function (k, v) {
            var postat = $(v).attr("at");
            var posttime = moment(postat, "YYYY-MM-DD HH:mm:ss","Asia/Bangkok");       
            $(v).text(posttime.fromNow());
        });
    }


    function isOnScreen(elem) {
        // if the element doesn't exist, abort
        if (elem.length == 0) {
            return;
        }
        var $window = jQuery(window)
        var viewport_top = $window.scrollTop()
        var viewport_height = $window.height()
        var viewport_bottom = viewport_top + viewport_height
        var $elem = jQuery(elem)
        var top = $elem.offset().top
        var height = $elem.height()
        var bottom = top + height

        return (top >= viewport_top && top < viewport_bottom) ||
            (bottom > viewport_top && bottom <= viewport_bottom) ||
            (height > viewport_height && top <= viewport_top && bottom >= viewport_bottom)
    }

    function getWithRelated() {
        $("#loading-img").removeClass("hide");
        needrequest = false;
        $.ajax({
            url: "/services/news/getwithrelated",
            data: {
                id: nextnews
            },
            dataType: "JSON",
            method: "GET"
        }).done(function (response) {



            needrequest = true;
            $("#loading-img").addClass("hide");
            initdate();
        });
    }


});
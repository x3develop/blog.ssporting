$(document).ready(function () {
    var fbid = $("#user-fb-avatar").attr("fbuid");
    var mid = getParam('mid');
    var token = null;
    var beton = false;
    var commentstructure = null;
    inituser(fbid, token);
    showComment();

    function getCommentBoxStructure() {
        return new Promise(function (resolve, reject) {
            if (commentstructure == null) {
                $.ajax({
                    url: "/mobile/include/matchcommentbox.html",
                    method: "GET",
                    dataType: "html"
                }).done(function (response) {
                    commentstructure = response;
                    resolve();
                });
            }
        })

    }

    $("#bx-add-review").on('click', function (e) {
        $(".bet-modal").each(function (k, v) {
            if ($(v).hasClass("active")) {
                beton = true;
            }
        });
        if (fbid != undefined && beton) {
            $("#commentReview").modal("show");
        } else {
            Swal.fire(
                'Error',
                'Please bet before comment',
                'error'
            )
        }
    });

    function showComment() {
        getCommentBoxStructure().then(function () {
            $.ajax({
                url: "/services/match/getcomment",
                method: "GET",
                data: {mid: mid},
                dataType: "JSON"
            }).done(function (response) {
                $('#HomeReview').empty();
                $('#AwayReview').empty();
                $.each(response.home, function (k, v) {
                    var row = $.parseHTML(commentstructure);
                    if (v.user_type == 'facebook') {
                        $(row).find('.commentator-avatar').first().attr('src', "https://graph.facebook.com/v3.2/" + v.owner.fb_uid + "/picture?type=large");
                        $(row).find('.commentator-win-rate').first().text(v.owner.accuracy.toFixed(0) + '%');
                        $(row).find('.avatar-box').first().addClass('user-avatar');
                    } else if (v.user_type == 'reviewer') {
                        $(row).find('.commentator-avatar').first().attr('src', v.owner.path);
                        $(row).find('.commentator-win-rate').first().text(v.owner.accurate.toFixed(0) + '%');
                    }


                    if (v.side == 'home') {
                        $(row).find('.commentator-team-logo').first().attr('src', $('#main-home-logo').attr('src'));
                        $(row).find('.commentator-team-name').first().text($('#main-home-name').text());
                    } else {

                    }
                    $(row).find('.commentator-name').first().text(v.owner.name);
                    $(row).find('.full-review-caption').first().text(v.comment);
                    $('#HomeReview').append($(row));
                });
                $.each(response.away, function (k, v) {
                    var row = $.parseHTML(commentstructure);
                    if (v.user_type == 'facebook') {
                        $(row).find('.commentator-avatar').first().attr('src', "https://graph.facebook.com/v3.2/" + v.owner.fb_uid + "/picture?type=large");
                        $(row).find('.commentator-win-rate').first().text(v.owner.accuracy.toFixed(0) + '%');
                    } else if (v.user_type == 'reviewer') {
                        $(row).find('.commentator-avatar').first().attr('src', v.owner.path);
                        $(row).find('.commentator-win-rate').first().text(v.owner.accurate.toFixed(0) + '%');
                    }


                    if (v.side == 'away') {
                        $(row).find('.commentator-team-logo').first().attr('src', $('#main-away-logo').attr('src'));
                        $(row).find('.commentator-team-name').first().text($('#main-away-name').text());
                    } else {

                    }
                    $(row).find('.commentator-name').first().text(v.owner.name);
                    $(row).find('.full-review-caption').first().text(v.comment);
                    $('#AwayReview').append($(row));
                });
            });
        });

    }

    $('#add-comment').on('click', function (e) {
        var comment = $('#comment-box').val().trim();
        console.log(comment);
        if (comment != "") {
            $.ajax({
                url: "/services/match/addcomment",
                method: 'GET',
                data: {mid: mid, fb_uid: fbid, comment: comment},
                dataType: 'JSON'
            }).done(function (response) {
                if (response.success) {
                    Swal.fire(
                        'Success',
                        response.desc,
                        'success'
                    );
                    $("#commentReview").modal("hide");
                    showComment();
                }
            })
        }
    });
});
FB.init({
    appId: '2323790581232085',
    // appId: '188797391962899',
    cookie: true,  // enable cookies to allow the server to access
                   // the session
    xfbml: true,  // parse social plugins on this page
    version: 'v3.2' // The Graph API version to use for the call
});
// fblogin();

function fblogin() {
    FB.login(function (response) {
        console.log(response);
        if (response.status === 'connected') {
            var token = response.authResponse.accessToken;
            FB.api('/me?fields=id,name,email', function (response) {
                console.log(response);
                $.ajax({
                    url: "/services/user/updateuser",
                    method: "GET",
                    data: {fb_uid: response.id, name: response.name, email: response.email, fb_token: token},
                    dataType: "JSON"
                }).done(function (res) {
                    // console.log(res);
                    location.reload();
                });
            });
        } else {
            swal.fire(
                "Error",
                "Unable to login facebook",
                "error"
            );

        }
    });
}

function inituser(fbid,token) {
    console.log("init user data");
        if (fbid != undefined) {
            if (token == null) {
                FB.getLoginStatus(function (response) {
                    console.log("Request FB data", response);
                    if (response.status == "connected") {
                        fbid = response.authResponse.userID;
                        token = response.authResponse.accessToken;
                    }
                });
            }
        }


}


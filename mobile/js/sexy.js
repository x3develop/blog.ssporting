$(document).ready(function () {
    var host = 'https://ngoal.com';
    var apage = 0;
    var ppage = 0;
    var vpage = 0;
    var type = 'all';
    var cardbox = null;
    var lastallcard = null;
    var needrequestall = true;
    getGallerry($('#grid_container'));


    $('.sexy-tab').on('click', function (e) {
        type = $(e.currentTarget).attr('gtype');
        if (type == 'all') {
            if (apage == 0)
                getGallerry($('#grid_container'));
        } else if (type == 'picture') {
            if (ppage == 0)
                getGallerry($('#spic-container'));
        } else if (type == 'video') {
            if (vpage == 0)
                getGallerry($('#svideo-container'));
        }

    });

    $(document).on('click', '.card', function (e) {
        var gid = $(e.currentTarget).attr('gid');
        var type = $(e.currentTarget).attr('type');
        console.log(gid);
        if (type == 'picture') {
            window.location = '/mobile/sexy-picpost-info?gid=' + gid;
        } else if (type == 'video') {
            window.location = '/mobile/sexy-video-info?gid=' + gid;
        }
    });

    function getCardbox() {
        return new Promise(function (resolve, reject) {
            if (cardbox == null) {
                $.ajax({
                    url: '/mobile/include/sexybox.html',
                    method: 'GET',
                    dataType: 'html'
                }).done(function (response) {
                    cardbox = response;
                    // console.log(cardbox);
                    resolve();
                });
            } else {
                resolve();
            }

        });
    }

    function getGallerry(el) {
        console.log(apage, ppage, vpage);
        var page = 0;
        if (type == 'all') {
            page = apage;
        } else if (type == 'picture') {
            page = ppage;
        } else if (type == 'video') {
            page = vpage;
        }
        getCardbox().then(function () {
            // console.log(cardbox);
            if (needrequestall) {
                needrequestall = false;
                $('#loading-img').removeClass('hide');
                $.ajax({
                    url: '/services/media/getallsexy',
                    method: 'GET',
                    data: {page: page, type: type},
                    dataType: 'JSON'
                }).done(function (response) {
                    $.each(response, function (k, v) {
                        console.log(v);
                        var ncard = $.parseHTML(cardbox);
                        $(ncard).attr('id', 'card' + v.id);
                        $(ncard).attr('gid', v.id);
                        $(ncard).attr('type', v.gall_type);
                        $(ncard).find('.cover-pic').first().attr('src', host + v.cover.thumbnail)
                        $(ncard).find('.title').first().children().first().text(v.title);
                        if (v.gall_type == 'picture') {
                            $(ncard).find('.media-icon').first().addClass('icon-type-image');
                        } else if (v.gall_type == 'video') {
                            $(ncard).find('.media-icon').first().addClass('icon-type-video');
                        }
                        // lastallcard = $(ncard);
                        $(el).append($(ncard))
                    });
                    $('#loading-img').addClass('hide');
                    needrequestall = true;
                    if (type == 'all') {
                        apage++;
                    } else if (type == 'picture') {
                        ppage++;
                    } else if (type == 'video') {
                        vpage++;
                    }
                });
            }
        })

    }

    $(window).scroll(function () {
        console.log($(window).scrollTop(), $(window).height(), $(document).height());
        var lastbox = $("#grid_container").children().last();

        if (type == 'all') {
            lastbox = $("#grid_container").children().last();
            if (isOnScreen(lastbox)) {
                getGallerry($('#grid_container'));
            }
        } else if (type == 'picture') {
            lastbox = $("#spic-container").children().last();
            if (isOnScreen(lastbox)) {
                getGallerry($('#spic-container'));
            }
        } else if (type == 'video') {
            lastbox = $("#svideo-container").children().last();
            if (isOnScreen(lastbox)) {
                getGallerry($('#svideo-container'));
            }
        }


    });


});
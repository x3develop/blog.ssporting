$(document).ready(function () {
    var fbid = $("#user-fb-avatar").attr("fbuid");
    var token = null;
    var mid = getParam('mid');
    var betmid = mid;
    var betval = 1200;
    if (fbid != undefined) {
        checkBet(mid, fbid);
    }
    $(".bet-modal").on("click", function (e) {
        var side = $(e.currentTarget).attr("side");
        var mid = $(e.currentTarget).attr("mid");
        betmid = mid;
        var hlogo = $("#main-home-logo").attr("src");
        var alogo = $("#main-away-logo").attr("src");
        var hname = $("#main-home-name").text();
        var aname = $("#main-away-name").text();
        $("#home-comment-box").val("");
        $("#away-comment-box").val("");
        if (side == "home") {
            $(".home-logo-mark").attr("src", hlogo);
            $("#bet-modal-hname").text(hname);
        } else {
            $(".away-logo-mark").attr("src", alogo);
            $("#bet-modal-aname").text(aname);
        }

        $('.bet-waiting-img').addClass("hide");

    });

    $(".select-coin").on("click", function (e) {
        var val = $(e.currentTarget).attr("value");
        betval = val;
        $(".bet-coin").text($(e.currentTarget).text());
    });

    $(".bet-save").on("click", function (e) {
        var side = $(e.currentTarget).attr("side");
        var hdp = $(".live-hdp-play[mid=" + betmid + "]").attr("hdp");
        var reward = $(".live-hdp-play[mid=" + betmid + "]").attr("home");
        var comment = $("#home-comment-box").val();
        if (side == "away") {
            reward = $(".live-hdp-play[mid=" + betmid + "]").attr("away");
            comment = $("#away-comment-box").val();
        }
        checkuser().then(function (resolve, reject) {
            // console.log(resolve, reject);
            $('.bet-waiting-img').removeClass("hide");
            $.ajax({
                url: "/services/bet/addbet",
                method: "GET",
                data: {
                    mid: betmid,
                    fb_uid: fbid,
                    team: side,
                    coin: betval,
                    hdp: hdp,
                    reward: reward,
                    comment: comment,
                    fb_token: token
                },
                dataType: "JSON"
            }).done(function (response) {
                console.log(response);
                if (response.success) {
                    Swal.fire(
                        'Success.',
                        response.desc,
                        'success'
                    );
                    $(".modal-vote").modal("hide");

                    updateUserResource();
                    checkBet(betmid, fbid);
                } else {
                    Swal.fire(
                        'Error!',
                        response.desc,
                        'error'
                    );
                }
                $('.bet-waiting-img').addClass("hide");
            });

        });
    }).error(function () {
        console.log("Reject");
    });


    function updateUserResource() {
        $.ajax({
            url: "/services/user/getuser",
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
            if (response.session_active) {
                $("#user-current-coin").text(response.user_data.coin);
                $("#user-current-gold").text(response.user_data.gold);
                $("#user-current-coupon").text(response.user_data.coupon);
            }
        })
    }

    function checkBet(mid, fbid) {
        $.ajax({
            url: "/services/bet/checkbet",
            method: "GET",
            data: {mid: mid, fb_uid: fbid},
            dataType: "JSON"
        }).done(function (response) {
            console.log(response);
            if (response.bet) {
                if (response.bet_data.team == "home") {
                    $(".nav-review-home[mid=" + mid + "]").addClass("active");
                    $(".select-team-name").text("@" + $("#main-home-name").text());
                    $(".select-team-logo").attr('src', $("#main-home-logo").attr('src'));
                } else {
                    $(".nav-review-away[mid=" + mid + "]").addClass("active");
                    $(".select-team-name").text("@" + $("#main-away-name").text());
                    $(".select-team-logo").attr('src', $("#main-away-logo").attr('src'));
                }
            }
        });
    }

    function checkuser() {
        console.log("check user data");
        return new Promise(function (resolve, reject) {
            if (fbid != undefined) {
                if (token == null) {
                    FB.getLoginStatus(function (response) {
                        console.log("Request FB data", response);
                        if (response.status == "connected") {
                            fbid = response.authResponse.userID;
                            token = response.authResponse.accessToken;
                            resolve();
                        }
                    });
                } else {
                    resolve();
                }
            } else {
                // reject();
                resolve();
            }

        })
    }

});
<div class="panel-body">
    <!--                                        last 5 match-->
    <div class="panel-body-status-match inline-box full-width bt-flex">
        <div class="home-status-match text-right">
            <div class="standing-view inline-box">
                <ul class="standing-view_last5">
                    <li>Last 5 match</li>
                    <li class="win-status"></li>
                    <li class="win-status"></li>
                    <li class="win-status"></li>
                    <li class="win-status"></li>
                    <li class="win-status"></li>
                </ul>
                <ul class="standing-view_odd">
                    <li>Odds</li>
                    <li class="win-status"></li>
                    <li class="win-status"></li>
                    <li class="win-status"></li>
                    <li class="win-status"></li>
                    <li class="win-status"></li>
                </ul>
            </div>
            <div class="standing-view-logo inline-box">
                <img src="/images/logo-team/Arsenal.png">
                <span>(5)</span>
            </div>
        </div>
        <div class="away-status-match text-left">
            <div class="standing-view-logo inline-box ">
                <img src="/images/logo-team/Bournemouth.png">
                <span>(12)</span>
            </div>
            <div class="standing-view inline-box">
                <ul class="standing-view_last5">
                    <li class="win-status"></li>
                    <li class="win-status"></li>
                    <li class="win-status"></li>
                    <li class="win-status"></li>
                    <li class="win-status"></li>
                    <li>Last 5 match</li>
                </ul>
                <ul class="standing-view_odd">
                    <li class="win-status"></li>
                    <li class="win-status"></li>
                    <li class="win-status"></li>
                    <li class="win-status"></li>
                    <li class="win-status"></li>
                    <li>Odds</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="content-vote-review">
        <div class="box-vote-win full-width inline-box text-center">

            <div class="title-center">
                โหวดผลการแข่งขัน
            </div>

            <div class="box-vote-win-btn inline-box">

                <div class="btn-group full-width" role="group" aria-label="...">
                    <button type="button" class="btn btn-primary">Home</button>
                    <button type="button" class="btn btn-default">Draw</button>
                    <button type="button" class="btn btn-success">Away</button>

                </div>
            </div>
        </div>
        <div class="box-progress vote-win full-width inline-box text-center">
            <div class="progress">
                <div class="progress-bar progress-bar-primary" style="width: 35%">
                    35%
                </div>
                <div class="progress-bar progress-bar-draw" style="width: 20%">
                    20%
                </div>
                <div class="progress-bar progress-bar-success" style="width: 45%">
                    45%
                </div>
            </div>
        </div>
    </div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs-event" role="tablist">
        <li role="presentation" class="active"><a href="#detail-tab-ft" role="tab" data-toggle="tab">Detail</a>
        </li>
        <li role="presentation"><a href="#h2h-tab-ft" role="tab" data-toggle="tab">H2H2</a>
        </li>
        <li role="presentation"><a href="#lineups-tab-ft" role="tab" data-toggle="tab">Lineups</a>
        </li>
        <li role="presentation"><a href="#statistics-tab-ft" role="tab" data-toggle="tab">Statistics</a>
        </li>
        <li role="presentation"><a href="#review-tab-ft" role="tab" data-toggle="tab">Review</a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="detail-tab-ft">

            <div class="ft-Highlight">
                <div class="content-recent">
                    <div class="content-item-recent">
                        <a class="link_wrap" href="/mobile/hothits-video-info.php"></a>
                        <div class="item-recent">
                            <div><img src="/images/cliphot/simple01_.jpg"></div>
                            <div class="bx-2">
                                <h4 class="media-heading">ไฮไลท์ กระชับมิตร Atalanta VS Chiasso</h4>
                                <div class="time-box box-inline">4 hours ago</div>
                                <div class="vertical-line-between box-inline"></div>
                                <div class="view-box box-inline">20</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content-event-match">

                <!--                                        Home ใบแดง-->
                <div class="full-width line-content-all">
                    <div class="col-home-live">
                        <div class="box-football-icon">
                            <div class="red-card"></div>
                        </div>
                        <div class="min-live">90+4'</div>
                        <div class="name-live">Willian</div>
                    </div>
                </div>
                <!--                                        Home ใบเหลือง-->
                <div class="full-width line-content-all">
                    <div class="col-home-live">
                        <div class="box-football-icon">
                            <div class="yellow-card"></div>
                        </div>
                        <div class="min-live">87'</div>
                        <div class="name-live">Cesc Fàbregas</div>
                    </div>
                </div>
                <!--                                        Home ทำประตู-->
                <div class="full-width line-content-all">
                    <div class="col-home-live">
                        <div class="box-football-icon">
                            <div class="goal-score"></div>
                        </div>
                        <div class="min-live">71'</div>
                        <div class="score-live">2 - 4</div>
                        <div class="name-live">Cesc Fàbregas</div>
                    </div>
                </div>
                <!--                                        Home เปลี่ยนตัว-->
                <div class="full-width line-content-all">
                    <div class="col-home-live">
                        <div class="box-football-icon">
                            <div class="substitute-player"></div>
                        </div>
                        <div class="min-live">69'</div>
                        <div class="name-live"><span
                                    class="substitute-player-out">out</span>
                            David
                            Luiz
                        </div>
                        <div class="name-live"><span
                                    class="substitute-player-in">in</span>
                            Victor
                            Moses
                        </div>
                    </div>
                </div>
                <!--Home ใบเหลือง+ใบเหลือง=ใบแดง-->
                <div class="full-width line-content-all">
                    <div class="col-home-live">
                        <div class="box-football-icon">
                            <div class="group-yellow-card"></div>
                        </div>
                        <div class="min-live">55'</div>
                        <div class="name-live">Ross Barkley</div>
                    </div>
                </div>
                <!--Home จุดโทษ-->
                <div class="full-width line-content-all">
                    <div class="col-home-live">
                        <div class="box-football-icon">
                            <div class="penalty-score"></div>
                        </div>
                        <div class="min-live">45'</div>
                        <div class="name-live">Gary Cahill</div>
                    </div>
                </div>
                <!--Home พลาดจุดโทษ-->
                <div class="full-width line-content-all">
                    <div class="col-home-live">
                        <div class="box-football-icon">
                            <div class="penalty-none-score"></div>
                        </div>
                        <div class="min-live">41'</div>
                        <div class="name-live">Álvaro Morata</div>
                    </div>
                </div>
                <!--Home ทำเข้าประตูตัวเอง-->
                <div class="full-width line-content-all">
                    <div class="col-home-live">
                        <div class="box-football-icon">
                            <div class="own-goal"></div>
                        </div>
                        <div class="min-live">28'</div>
                        <div class="name-live">Álvaro Morata</div>
                    </div>
                </div>


                <!--                                        Away ใบแดง-->
                <div class="full-width line-content-all">
                    <div class="col-away-live pull-right">
                        <div class="name-live">Willian</div>
                        <div class="min-live">90+4'</div>
                        <div class="box-football-icon">
                            <div class="red-card"></div>
                        </div>
                    </div>
                </div>
                <!--                                        Away ใบเหลือง-->
                <div class="full-width line-content-all">
                    <div class="col-away-live pull-right">
                        <div class="name-live">Cesc Fàbregas</div>
                        <div class="min-live">87'</div>
                        <div class="box-football-icon">
                            <div class="yellow-card"></div>
                        </div>
                    </div>
                </div>
                <!--                                        Away ทำประตู-->
                <div class="full-width line-content-all">
                    <div class="col-away-live pull-right">
                        <div class="name-live">Cesc Fàbregas</div>
                        <div class="score-live">2 - 4</div>
                        <div class="min-live">71'</div>
                        <div class="box-football-icon">
                            <div class="goal-score"></div>
                        </div>
                    </div>
                </div>
                <!--                                        Away เปลี่ยนตัว-->
                <div class="full-width line-content-all">
                    <div class="col-away-live pull-right">
                        <div class="name-live"><span
                                    class="substitute-player-in">in</span>
                            Victor
                            Moses
                        </div>
                        <div class="name-live"><span
                                    class="substitute-player-out">out</span>
                            David
                            Luiz
                        </div>
                        <div class="min-live">69'</div>
                        <div class="box-football-icon">
                            <div class="substitute-player"></div>
                        </div>
                    </div>
                </div>
                <!--Away ใบเหลือง+ใบเหลือง=ใบแดง-->
                <div class="full-width line-content-all">
                    <div class="col-away-live pull-right">
                        <div class="name-live">Ross Barkley</div>
                        <div class="min-live">55'</div>
                        <div class="box-football-icon">
                            <div class="group-yellow-card"></div>
                        </div>
                    </div>
                </div>
                <!--Away จุดโทษ-->
                <div class="full-width line-content-all">
                    <div class="col-away-live pull-right">
                        <div class="name-live">Gary Cahill</div>
                        <div class="min-live">45'</div>
                        <div class="box-football-icon">
                            <div class="penalty-score"></div>
                        </div>
                    </div>
                </div>
                <!--Away พลาดจุดโทษ-->
                <div class="full-width line-content-all">
                    <div class="col-away-live pull-right">
                        <div class="name-live">Álvaro Morata</div>
                        <div class="min-live">41'</div>
                        <div class="box-football-icon">
                            <div class="penalty-none-score"></div>
                        </div>
                    </div>
                </div>
                <!--Away ทำเข้าประตูตัวเอง-->
                <div class="full-width line-content-all">
                    <div class="col-away-live pull-right">
                        <div class="name-live">Álvaro Morata</div>
                        <div class="min-live">28'</div>
                        <div class="box-football-icon">
                            <div class="own-goal"></div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
        <div role="tabpanel" class="tab-pane" id="h2h-tab-ft">
            <!--                                       h2h-->
            <div class="box-h2h inline-box full-width">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="nav-tabs-team active">
                        <a href="#home-h2h" role="tab" data-toggle="tab">
                            <div class="pull-right">
                                <img src="/images/logo-team/Arsenal.png">
                            </div>
                            <div class="pull-right text-right">
                                <h4>Arsenal</h4>
                                <ul>
                                    <li>
                                        <span>4</span>
                                        <small>Win</small>
                                    </li>
                                    <li>
                                        <span>5</span>
                                        <small>Draw</small>
                                    </li>
                                    <li>
                                        <span>9</span>
                                        <small>Lose</small>
                                    </li>
                                </ul>
                            </div>

                        </a>
                    </li>
                    <li class="nav-tabs-team" role="presentation">
                        <a href="#away-h2h" role="tab" data-toggle="tab">
                            <div class="pull-left">
                                <img src="/images/logo-team/Bournemouth.png">
                            </div>
                            <div class="pull-left text-left">
                                <h4>Bournemouth</h4>
                                <ul>
                                    <li>
                                        <span>4</span>
                                        <small>Win</small>
                                    </li>
                                    <li>
                                        <span>5</span>
                                        <small>Draw</small>
                                    </li>
                                    <li>
                                        <span>9</span>
                                        <small>Lose</small>
                                    </li>
                                </ul>
                            </div>
                        </a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home-h2h">
                        <div class="box-match-tournament">
                            <div class="box-match-list-topic">
                                <div class="pull-left">
                                    <img src="/images/logo-team/fa-cup-2017.png">
                                    <p>FA Cup</p>
                                </div>
                                <div class="pull-right">
                                    <ul>
                                        <li>Odds</li>
                                        <li>Result</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="box-match-list">

                                <div class="text-center">
                                    <div class="date-match">15 Apr 18</div>
                                    <div class="time-match">15:00</div>
                                </div>
                                <div>
                                    <div class="home-name-match team-bet-color"><img
                                                src="/images/logo-team/Chelsea.png">Chelsea
                                    </div>
                                    <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                        united
                                    </div>
                                </div>
                                <div>
                                    <div class="home-score-match">0</div>
                                    <div class="away-score-match">2</div>
                                </div>
                                <div class="text-center">
                                    <div class="odds-match">
                                        <img src="/images/icon-stat/result/0.png">
                                    </div>
                                </div>
                                <div class="text-center">
                                    <div class="result-match">
                                        <img src="/images/icon-stat/result/2.png">
                                    </div>
                                </div>
                            </div>
                            <div class="box-match-list">

                                <div class="text-center">
                                    <div class="date-match">15 Apr 18</div>
                                    <div class="time-match">15:00</div>
                                </div>
                                <div>
                                    <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea</div>
                                    <div class="away-name-match team-bet-color"><img
                                                src="/images/logo-team/manchester.png">Manchester
                                        united
                                    </div>
                                </div>
                                <div>
                                    <div class="home-score-match">0</div>
                                    <div class="away-score-match">2</div>
                                </div>
                                <div class="text-center">
                                    <div class="odds-match">
                                        <img src="/images/icon-stat/result/0.png">
                                    </div>
                                </div>
                                <div class="text-center">
                                    <div class="result-match">
                                        <img src="/images/icon-stat/result/2.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-match-tournament">
                            <div class="box-match-list-topic">
                                <div class="pull-left">
                                    <img src="/images/logo-team/fa-cup-2017.png">
                                    <p>FA Cup</p>
                                </div>
                                <div class="pull-right">
                                    <ul>
                                        <li>Odds</li>
                                        <li>Result</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="box-match-list">

                                <div class="text-center">
                                    <div class="date-match">15 Apr 18</div>
                                    <div class="time-match">15:00</div>
                                </div>
                                <div>
                                    <div class="home-name-match team-bet-color"><img
                                                src="/images/logo-team/Chelsea.png">Chelsea
                                    </div>
                                    <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                        united
                                    </div>
                                </div>
                                <div>
                                    <div class="home-score-match">0</div>
                                    <div class="away-score-match">2</div>
                                </div>
                                <div class="text-center">
                                    <div class="odds-match">
                                        <img src="/images/icon-stat/result/0.png">
                                    </div>
                                </div>
                                <div class="text-center">
                                    <div class="result-match">
                                        <img src="/images/icon-stat/result/2.png">
                                    </div>
                                </div>
                            </div>
                            <div class="box-match-list">

                                <div class="text-center">
                                    <div class="date-match">15 Apr 18</div>
                                    <div class="time-match">15:00</div>
                                </div>
                                <div>
                                    <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea</div>
                                    <div class="away-name-match team-bet-color"><img
                                                src="/images/logo-team/manchester.png">Manchester
                                        united
                                    </div>
                                </div>
                                <div>
                                    <div class="home-score-match">0</div>
                                    <div class="away-score-match">2</div>
                                </div>
                                <div class="text-center">
                                    <div class="odds-match">
                                        <img src="/images/icon-stat/result/0.png">
                                    </div>
                                </div>
                                <div class="text-center">
                                    <div class="result-match">
                                        <img src="/images/icon-stat/result/2.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="away-h2h">
                        <div class="box-match-tournament">
                            <div class="box-match-list-topic">
                                <div class="pull-left">
                                    <img src="/images/logo-team/fa-cup-2017.png">
                                    <p>FA Cup</p>
                                </div>
                                <div class="pull-right">
                                    <ul>
                                        <li>Odds</li>
                                        <li>Result</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="box-match-list">

                                <div class="text-center">
                                    <div class="date-match">15 Apr 18</div>
                                    <div class="time-match">15:00</div>
                                </div>
                                <div>
                                    <div class="home-name-match team-bet-color"><img
                                                src="/images/logo-team/Chelsea.png">Chelsea
                                    </div>
                                    <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                        united
                                    </div>
                                </div>
                                <div>
                                    <div class="home-score-match">0</div>
                                    <div class="away-score-match">2</div>
                                </div>
                                <div class="text-center">
                                    <div class="odds-match">
                                        <img src="/images/icon-stat/result/0.png">
                                    </div>
                                </div>
                                <div class="text-center">
                                    <div class="result-match">
                                        <img src="/images/icon-stat/result/2.png">
                                    </div>
                                </div>
                            </div>
                            <div class="box-match-list">

                                <div class="text-center">
                                    <div class="date-match">15 Apr 18</div>
                                    <div class="time-match">15:00</div>
                                </div>
                                <div>
                                    <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea</div>
                                    <div class="away-name-match team-bet-color"><img
                                                src="/images/logo-team/manchester.png">Manchester
                                        united
                                    </div>
                                </div>
                                <div>
                                    <div class="home-score-match">0</div>
                                    <div class="away-score-match">2</div>
                                </div>
                                <div class="text-center">
                                    <div class="odds-match">
                                        <img src="/images/icon-stat/result/0.png">
                                    </div>
                                </div>
                                <div class="text-center">
                                    <div class="result-match">
                                        <img src="/images/icon-stat/result/2.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-match-tournament">
                            <div class="box-match-list-topic">
                                <div class="pull-left">
                                    <img src="/images/logo-team/fa-cup-2017.png">
                                    <p>FA Cup</p>
                                </div>
                                <div class="pull-right">
                                    <ul>
                                        <li>Odds</li>
                                        <li>Result</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="box-match-list">

                                <div class="text-center">
                                    <div class="date-match">15 Apr 18</div>
                                    <div class="time-match">15:00</div>
                                </div>
                                <div>
                                    <div class="home-name-match team-bet-color"><img
                                                src="/images/logo-team/Chelsea.png">Chelsea
                                    </div>
                                    <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                        united
                                    </div>
                                </div>
                                <div>
                                    <div class="home-score-match">0</div>
                                    <div class="away-score-match">2</div>
                                </div>
                                <div class="text-center">
                                    <div class="odds-match">
                                        <img src="/images/icon-stat/result/0.png">
                                    </div>
                                </div>
                                <div class="text-center">
                                    <div class="result-match">
                                        <img src="/images/icon-stat/result/2.png">
                                    </div>
                                </div>
                            </div>
                            <div class="box-match-list">

                                <div class="text-center">
                                    <div class="date-match">15 Apr 18</div>
                                    <div class="time-match">15:00</div>
                                </div>
                                <div>
                                    <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea</div>
                                    <div class="away-name-match team-bet-color"><img
                                                src="/images/logo-team/manchester.png">Manchester
                                        united
                                    </div>
                                </div>
                                <div>
                                    <div class="home-score-match">0</div>
                                    <div class="away-score-match">2</div>
                                </div>
                                <div class="text-center">
                                    <div class="odds-match">
                                        <img src="/images/icon-stat/result/0.png">
                                    </div>
                                </div>
                                <div class="text-center">
                                    <div class="result-match">
                                        <img src="/images/icon-stat/result/2.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="lineups-tab-ft">
            <div class="box-lineup-m inline-box full-width">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="tab-lineups-home active">
                        <a href="#lineups-home-tab" role="tab" data-toggle="tab">


                            <img class="pull-right" src="/images/logo-team/Arsenal.png">
                            <div class="pull-right text-right">
                                <h4>Arsenal</h4>
                                <small><span style="margin-right: 5px;">Coach</span>Unai Emery</small>
                            </div>
                        </a>
                    </li>
                    <li role="presentation" class="tab-lineups-away">
                        <a href="#lineups-away-tab" role="tab" data-toggle="tab">
                            <img src="/images/logo-team/Bournemouth.png">
                            <div class="pull-left">
                                <h4>Bournemouth</h4>
                                <small>Eddie Howe<span>Coach</span></small>
                            </div>
                        </a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="lineups-home-tab">
                        <div class="text-title">
                            <h4>Player</h4>
                        </div>
                        <table class="table">
                            <tbody>
                            <tr>
                                <td><img src="/images/1.jpg"></td>
                                <td>14</td>
                                <td>Pierre-Emerick Aubameyang</td>
                                <td>FT</td>
                            </tr>
                            <tr>
                                <td><img src="/images/1.jpg"></td>
                                <td>14</td>
                                <td>Pierre-Emerick Aubameyang</td>
                                <td>FT</td>
                            </tr>
                            <tr>
                                <td><img src="/images/1.jpg"></td>
                                <td>14</td>
                                <td>Pierre-Emerick Aubameyang</td>
                                <td>FT</td>
                            </tr>
                            <tr>
                                <td><img src="/images/1.jpg"></td>
                                <td>14</td>
                                <td>Pierre-Emerick Aubameyang</td>
                                <td>FT</td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="text-title">
                            <h4>Sub Player</h4>
                        </div>
                        <table class="table">
                            <tbody>
                            <tr>
                                <td><img src="/images/1.jpg"></td>
                                <td>14</td>
                                <td>Pierre-Emerick Aubameyang</td>
                                <td>FT</td>
                            </tr>
                            <tr>
                                <td><img src="/images/1.jpg"></td>
                                <td>14</td>
                                <td>Pierre-Emerick Aubameyang</td>
                                <td>FT</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lineups-away-tab">...</div>
                </div>
            </div>

        </div>
        <div role="tabpanel" class="tab-pane" id="statistics-tab-ft">
            <div class="full-width inline-box box-statistics">
                <ul>
                    <li class="high-num"><span>19</span></li>
                    <li>shots</li>
                    <li><span>8</span></li>
                </ul>
                <ul>
                    <li class="high-num"><span>7</span></li>
                    <li>shots on goal</li>
                    <li><span>3</span></li>
                </ul>
                <ul>
                    <li><span>15</span></li>
                    <li>fouls</li>
                    <li class="high-num"><span>19</span></li>
                </ul>
                <ul>
                    <li class="high-num"><span>7</span></li>
                    <li>corner kicks</li>
                    <li><span>5</span></li>
                </ul>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="review-tab-ft">
            <!--                                        Review-->
            <div class="box-review-simple inline-box">
                <a href="#" class="link_wrap"></a>
                <div class="row-review-simple bt-flex">

                    <div class="rrs-profile">
                        <img src="/images/2.jpg">
                    </div>
                    <div class="rrs-team">
                        <span>@arsenalarsenalarsenalarsenal</span>
                    </div>
                    <div class="rrs-review-simple">
                                                        <span>สองทีมนี้ผลงานต่างกันราวฟ้ากับเหว ทางเจ้าบ้านไปในทางที่ดี ฮอนก้า
                                                        เกมนี้ได้เปรียบอย่างมาก มีทั้งความมั่นใจในมุกๆเกมที่ลงสนาม
                                                        พร้อมเปิดบ้านซัดประตูคู่แข่ง ด้วยฟอร์มการเล่น 5 นัดหลัง ชนะ 3 เสมอ 1
                                                        แพ้ 1 เก็บได้ถึง 10 คะแนนเลยแค่ไม่กี่เกม แต่ทาง พีเอส เคมี่ กลับกัน
                                                        คว้าสองแต้มจาก 5 นัดหลัง เสมอ 2 แพ้ 3
                                                        </span>
                    </div>


                </div>
                <div class="row-review-simple bt-flex">

                    <div class="rrs-profile">
                        <img src="/images/profess-player/sample-logo04.png">
                    </div>
                    <div class="rrs-team">
                        <span>@bournemouth</span>
                    </div>
                    <div class="rrs-review-simple">
                                                    <span>มาดริด กำลังโชว์ฟอร์มเยี่ยม ก่อนมินิเบรค จึงกลับประเดิมเลก 2 ด้วยความฮึกเหิม นอกจากนั้น "ราชันชุดขาว" ทะยอยได้ตัวหลักคืนทีมเกือบครบ แถมฟิตเต็มร้อยทุกราย เชื่อว่าแชมป์ยุโรป 3 สมัยโฟกัสที่ 3 แต้มแน่นอน กอปรกับบียาร์เรอัล ไม่ได้อยู่ในช่วงเวลาทีดีนัก ล่าสุดรั้งท้ายตาราง ถึงแม้จะเปิดเกมสู้แบบหลังพิงฝา แต่มาเจอกับมาดริด ที่เต็มไปด้วยแข้งเกรดเอ บวกกับฟอร์มเริ่มเข้าฝัก ก็คงรอดยากสำหรับเจ้าบ้าน นอกจากนั้นปูมหลังเก่าๆ ฝั่งมาดริด บุกชนะบียาร์เรอัล ได้บ่อยครั้งอีกด้วย ดังนั้นราคาแค่ 0.5 ยังไงก็ต้องวางบอลต่อ
                                                        </span>
                    </div>


                </div>
                <div class="row-review-simple bt-flex">

                    <div class="rrs-profile">
                        <img src="/images/2.jpg">
                    </div>
                    <div class="rrs-team">
                        <span>@arsenalarsenalarsenalarsenal</span>
                    </div>
                    <div class="rrs-review-simple">
                                                        <span>สองทีมนี้ผลงานต่างกันราวฟ้ากับเหว ทางเจ้าบ้านไปในทางที่ดี ฮอนก้า
                                                        เกมนี้ได้เปรียบอย่างมาก มีทั้งความมั่นใจในมุกๆเกมที่ลงสนาม
                                                        พร้อมเปิดบ้านซัดประตูคู่แข่ง ด้วยฟอร์มการเล่น 5 นัดหลัง ชนะ 3 เสมอ 1
                                                        แพ้ 1 เก็บได้ถึง 10 คะแนนเลยแค่ไม่กี่เกม แต่ทาง พีเอส เคมี่ กลับกัน
                                                        คว้าสองแต้มจาก 5 นัดหลัง เสมอ 2 แพ้ 3
                                                        </span>
                    </div>


                </div>
                <div class="row-review-simple bt-flex">

                    <div class="rrs-profile">
                        <img src="/images/2.jpg">
                    </div>
                    <div class="rrs-team">
                        <span>@arsenalarsenalarsenalarsenal</span>
                    </div>
                    <div class="rrs-review-simple">
                                                        <span>สองทีมนี้ผลงานต่างกันราวฟ้ากับเหว ทางเจ้าบ้านไปในทางที่ดี ฮอนก้า
                                                        เกมนี้ได้เปรียบอย่างมาก มีทั้งความมั่นใจในมุกๆเกมที่ลงสนาม
                                                        พร้อมเปิดบ้านซัดประตูคู่แข่ง ด้วยฟอร์มการเล่น 5 นัดหลัง ชนะ 3 เสมอ 1
                                                        แพ้ 1 เก็บได้ถึง 10 คะแนนเลยแค่ไม่กี่เกม แต่ทาง พีเอส เคมี่ กลับกัน
                                                        คว้าสองแต้มจาก 5 นัดหลัง เสมอ 2 แพ้ 3
                                                        </span>
                    </div>


                </div>

                <div class="inline-box full-width text-center">
                    <a class="btn btn-primary btn-sm" href="#"
                       role="button">ดูทรรศนะทั้งหมด</a>
                </div>
            </div>
        </div>
    </div>


</div>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/mobile/css/all-style.css" rel="stylesheet">
    <link href="/mobile/css/article.css" rel="stylesheet">
    <link href="/mobile/css/bootstrap.css" rel="stylesheet">
    <link href="/mobile/css/docs.css" rel="stylesheet">
    <link href="/mobile/css/game.css" rel="stylesheet">
    <link href="/mobile/css/highlight.css" rel="stylesheet">
    <link href="/mobile/css/index.css" rel="stylesheet">
    <link href="/mobile/css/live-football.css" rel="stylesheet">
    <link href="/mobile/css/news.css" rel="stylesheet">
    <link href="/mobile/css/profile.css" rel="stylesheet">
    <link href="/mobile/css/ranking.css" rel="stylesheet">
    <link href="/mobile/css/review.css" rel="stylesheet">
    <link href="/mobile/css/livescore-mobile.css" rel="stylesheet">
    <link href="/mobile/css/football-symbol-mobile.css" rel="stylesheet">
    <link href="/mobile/css/swiper.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/mobile/css/font-awesome-4.7.0/css/font-awesome.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/js/moment.js"></script>
    <script src="/js/moment-with-locales.min.js"></script>
    <script src="/js/newFB.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
</head>
<body>
<?php include("menu-mobile.php"); ?>
<style type="text/css">
    body {
        background-color: #f1f1f1;
    }
</style>

<!--เลือกคู่-->
<div class="content-list-game" id="list_game">


    <div class="thumbnail">
        <ul>
            <li><img src="/images/logo-team/manchester.png"></li>
            <li><img src="/images/logo-team/manchester.png"></li>
        </ul>
        <h4>HDP</h4>
        <h3>-2.50</h3>
    </div>

    <div class="thumbnail">
        <ul>
            <li><img src="/images/logo-team/manchester.png"></li>
            <li><img src="/images/logo-team/manchester.png"></li>
        </ul>
        <h4>HDP</h4>
        <h3>-2.50</h3>
    </div>

    <div class="thumbnail active">
        <ul>
            <li><img src="/images/logo-team/manchester.png"></li>
            <li><img src="/images/logo-team/manchester.png"></li>
        </ul>
        <h4>HDP</h4>
        <h3>-2.50</h3>
    </div>

    <div class="thumbnail">
        <ul>
            <li><img src="/images/logo-team/manchester.png"></li>
            <li><img src="/images/logo-team/manchester.png"></li>
        </ul>
        <h4>HDP</h4>
        <h3>-2.50</h3>
    </div>

    <div class="thumbnail">
        <ul>
            <li><img src="/images/logo-team/manchester.png"></li>
            <li><img src="/images/logo-team/manchester.png"></li>
        </ul>
        <h4>HDP</h4>
        <h3>-2.50</h3>
    </div>

    <div class="thumbnail">
        <ul>
            <li><img src="/images/logo-team/manchester.png"></li>
            <li><img src="/images/logo-team/manchester.png"></li>
        </ul>
        <h4>HDP</h4>
        <h3>-2.50</h3>
    </div>

    <div class="thumbnail">
        <ul>
            <li><img src="/images/logo-team/manchester.png"></li>
            <li><img src="/images/logo-team/manchester.png"></li>
        </ul>
        <h4>HDP</h4>
        <h3>-2.50</h3>
    </div>

    <div class="thumbnail">
        <ul>
            <li><img src="/images/logo-team/manchester.png"></li>
            <li><img src="/images/logo-team/manchester.png"></li>
        </ul>
        <h4>HDP</h4>
        <h3>-2.50</h3>
    </div>

    <div class="thumbnail">
        <ul>
            <li><img src="/images/logo-team/manchester.png"></li>
            <li><img src="/images/logo-team/manchester.png"></li>
        </ul>
        <h4>HDP</h4>
        <h3>-2.50</h3>
    </div>

</div>

<!--ทายผลเกม-->
<div class="content-game-vote" style="">


    <div class="container container-game">
        <div class="row">

            <div class="col-lg-12">
                <div class="header-game">
                    <h4>UEFA Champions League</h4>
                    <ul>
                        <li><i class="fa fa-calendar" aria-hidden="true"></i> 03/07/2018</li>
                    </ul>
                </div>


                <div class="thumbnail-review">
                    <div class="HDP-box-center">
                        <div class="time-box">01:00</div>
                        <strong>HDP</strong>
                        <h2>-2.50</h2>
                        <div class="box-water-text hide">
                            <div>1.9</div>
                            <div>1.69</div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="home-review-box">
                            <div class="logo"><img src="/images/logo-team/manchester.png"></div>
                            <div class="name">Manchester</div>
                            <div class="btn btn-primary" data-toggle="modal" data-target="#myModalHome">1@1.90</div>
                            <!--                                <div class="mobile-vote-team active" >-->
                            <!--                                    <h4>Voted</h4>-->
                            <!--                                    <div class="bg-mobile-vote-team"></div>-->
                            <!--                                </div>-->
                            <ul class="last-player last-match">
                                <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                <li>
                                    <div class="win-status"></div>
                                </li>
                                <li>
                                    <div class="win-status"></div>
                                </li>
                                <li>
                                    <div class="lose-status"></div>
                                </li>
                                <li>
                                    <div class="win-status"></div>
                                </li>
                                <li>
                                    <div class="win-status"></div>
                                </li>
                            </ul>
                            <ul class="last-player">
                                <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                <li><img src="/images/avatar.png"></li>
                                <li><img src="/images/avatar.png"></li>
                                <li><img src="/images/avatar.png"></li>
                                <li><img src="/images/img-user.jpg"></li>
                                <li><img src="/images/p1.png"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="away-review-box">
                            <div class="logo"><img src="/images/logo-team/Chelsea.png"></div>
                            <div class="name">Chelsea</div>
                            <div class="btn btn-success" data-toggle="modal" data-target="#myModalAway">2@0.50</div>
                            <!--                                <div class="mobile-vote-team" >-->
                            <!--                                    <h4>Vote</h4>-->
                            <!--                                    <div class="bg-mobile-vote-team"></div>-->
                            <!--                                </div>-->
                            <ul class="last-player last-match">
                                <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                <li>
                                    <div class="lose-status"></div>
                                </li>
                                <li>
                                    <div class="win-status"></div>
                                </li>
                                <li>
                                    <div class="lose-status"></div>
                                </li>
                                <li>
                                    <div class="win-status"></div>
                                </li>
                                <li>
                                    <div class="draw-status"></div>
                                </li>
                            </ul>
                            <ul class="last-player">
                                <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                <li><img src="/images/p1.png"></li>
                                <li><img src="/images/4.jpg"></li>
                                <li><img src="/images/avatar.png"></li>
                                <li><img src="/images/avatar.png"></li>
                                <li><img src="/images/avatar.png"></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div style="clear: both"></div>


            </div>
        </div>
    </div>
    <div class="container">
        <div class="progress progress-in-game">
            <div class="progress-bar" style="width: 35%">
                <span>35%</span>
            </div>
            <div class="progress-bar progress-bar-success" style="width: 65%">
                <span>65%</span>
            </div>
        </div>
    </div>
</div>

<!--            ทีเด็ด ทรรศนะ-->
<div class="container-game container-game-review">
    <div class="container">
        <div class="mini-title">
            <h3>ทีเด็ด ทรรศนะ</h3>
        </div>
        <!-- Swiper -->
        <div class="thumbnail">
            <div class="swiper-container">
                <div class="swiper-wrapper" data-toggle="modal" data-target="#myModalFullreview">
                    <div class="swiper-slide">
                        <a class="link_wrap" href="/mobile/review-info.php"></a>
                        <div class="thumbnail-review thumbnail-review-index">
                            <div class="box-review-list">
                                <div class="box-review-list-row">
                                    <div><img src="/images/profess-player/sample-logo01.png"></div>
                                    <div class="text-add-team">
                                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                                        <div class="detail-review">
                                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์
                                            สโมลอฟ
                                            กองหน้าตัวเก่ง
                                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                            ชุดนี้มีตัวดังๆ
                                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล
                                            มูวัลลัด
                                            เจ้าภาพ
                                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย
                                            แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง
                                            เชื่อว่าเกมนี้
                                            รัสเซีย
                                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                            ฟันธง : รอง ซาอุดีอาระเบีย"
                                        </div>
                                    </div>
                                </div>
                                <div class="box-review-list-row">
                                    <div><img src="/images/profess-player/sample-logo02.png"></div>
                                    <div class="text-add-team">
                                        <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                                        <div class="detail-review">
                                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์
                                            สโมลอฟ
                                            กองหน้าตัวเก่ง
                                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                            ชุดนี้มีตัวดังๆ
                                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล
                                            มูวัลลัด
                                            เจ้าภาพ
                                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย
                                            แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง
                                            เชื่อว่าเกมนี้
                                            รัสเซีย
                                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                            ฟันธง : รอง ซาอุดีอาระเบีย"
                                        </div>
                                    </div>
                                </div>
                                <div class="box-review-list-row">
                                    <div><img src="/images/profess-player/sample-logo03.png"></div>
                                    <div class="text-add-team">
                                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                                        <div class="detail-review">
                                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์
                                            สโมลอฟ
                                            กองหน้าตัวเก่ง
                                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                            ชุดนี้มีตัวดังๆ
                                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล
                                            มูวัลลัด
                                            เจ้าภาพ
                                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย
                                            แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง
                                            เชื่อว่าเกมนี้
                                            รัสเซีย
                                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                            ฟันธง : รอง ซาอุดีอาระเบีย"
                                        </div>
                                    </div>
                                </div>
                                <div class="box-review-list-row">
                                    <div class="user-bet"><img src="/images/3.jpg"></div>
                                    <div class="text-add-team">
                                        <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                                        <div class="detail-review">
                                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์
                                            สโมลอฟ
                                            กองหน้าตัวเก่ง
                                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                            ชุดนี้มีตัวดังๆ
                                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล
                                            มูวัลลัด
                                            เจ้าภาพ
                                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย
                                            แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง
                                            เชื่อว่าเกมนี้
                                            รัสเซีย
                                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                            ฟันธง : รอง ซาอุดีอาระเบีย"
                                        </div>
                                    </div>
                                </div>
                                <div class="box-review-list-row">
                                    <div><img src="/images/profess-player/sample-logo04.png"></div>
                                    <div class="text-add-team">
                                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                                        <div class="detail-review">
                                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์
                                            สโมลอฟ
                                            กองหน้าตัวเก่ง
                                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                            ชุดนี้มีตัวดังๆ
                                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล
                                            มูวัลลัด
                                            เจ้าภาพ
                                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย
                                            แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง
                                            เชื่อว่าเกมนี้
                                            รัสเซีย
                                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                            ฟันธง : รอง ซาอุดีอาระเบีย"
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="clear: both"></div>
                        </div>

                    </div>
                    <div class="swiper-slide">
                        <a class="link_wrap" href="/mobile/review-info.php"></a>
                        <div class="thumbnail-review thumbnail-review-index">
                            <div class="box-review-list">
                                <div class="box-review-list-row">
                                    <div><img src="/images/profess-player/sample-logo01.png"></div>
                                    <div class="text-add-team">
                                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                                        <div class="detail-review">
                                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์
                                            สโมลอฟ
                                            กองหน้าตัวเก่ง
                                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                            ชุดนี้มีตัวดังๆ
                                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล
                                            มูวัลลัด
                                            เจ้าภาพ
                                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย
                                            แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง
                                            เชื่อว่าเกมนี้
                                            รัสเซีย
                                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                            ฟันธง : รอง ซาอุดีอาระเบีย"
                                        </div>
                                    </div>
                                </div>
                                <div class="box-review-list-row">
                                    <div><img src="/images/profess-player/sample-logo02.png"></div>
                                    <div class="text-add-team">
                                        <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                                        <div class="detail-review">
                                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์
                                            สโมลอฟ
                                            กองหน้าตัวเก่ง
                                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                            ชุดนี้มีตัวดังๆ
                                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล
                                            มูวัลลัด
                                            เจ้าภาพ
                                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย
                                            แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง
                                            เชื่อว่าเกมนี้
                                            รัสเซีย
                                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                            ฟันธง : รอง ซาอุดีอาระเบีย"
                                        </div>
                                    </div>
                                </div>
                                <div class="box-review-list-row">
                                    <div><img src="/images/profess-player/sample-logo03.png"></div>
                                    <div class="text-add-team">
                                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                                        <div class="detail-review">
                                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์
                                            สโมลอฟ
                                            กองหน้าตัวเก่ง
                                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                            ชุดนี้มีตัวดังๆ
                                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล
                                            มูวัลลัด
                                            เจ้าภาพ
                                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย
                                            แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง
                                            เชื่อว่าเกมนี้
                                            รัสเซีย
                                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                            ฟันธง : รอง ซาอุดีอาระเบีย"
                                        </div>
                                    </div>
                                </div>
                                <div class="box-review-list-row">
                                    <div class="user-bet"><img src="/images/3.jpg"></div>
                                    <div class="text-add-team">
                                        <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                                        <div class="detail-review">
                                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์
                                            สโมลอฟ
                                            กองหน้าตัวเก่ง
                                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                            ชุดนี้มีตัวดังๆ
                                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล
                                            มูวัลลัด
                                            เจ้าภาพ
                                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย
                                            แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง
                                            เชื่อว่าเกมนี้
                                            รัสเซีย
                                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                            ฟันธง : รอง ซาอุดีอาระเบีย"
                                        </div>
                                    </div>
                                </div>
                                <div class="box-review-list-row">
                                    <div><img src="/images/profess-player/sample-logo04.png"></div>
                                    <div class="text-add-team">
                                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                                        <div class="detail-review">
                                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์
                                            สโมลอฟ
                                            กองหน้าตัวเก่ง
                                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                            ชุดนี้มีตัวดังๆ
                                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล
                                            มูวัลลัด
                                            เจ้าภาพ
                                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย
                                            แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง
                                            เชื่อว่าเกมนี้
                                            รัสเซีย
                                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                            ฟันธง : รอง ซาอุดีอาระเบีย"
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="clear: both"></div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>

            <div class="full-width text-center" style="margin: 15px 0">
                <div class="btn btn-sm btn-default">ทีเด็ดทรรศนะทั้งหมด</div>
            </div>
        </div>
    </div>
</div>


<div class="container-game" data-spy="scroll" data-target="#navbar-example" style="margin-bottom: 20px;">

    <div class="full-width inline-box box-nav-game-data" id="affix2" data-spy="affix" data-offset-top="60">
        <ul id="nav" class="nav navbar-nav" style="color: #000">
            <li class="active"><a href="#h2h_game">h2h</a></li>
            <li><a href="#form_guide_game">form guide</a></li>
            <li><a href="#next_match_game">next match</a></li>
            <li><a href="#table_game">table</a></li>
        </ul>
    </div>


    <!--            HEAD TO HEAD-->
    <div id="h2h_game" class="head2head">
        <div class="container">
            <div class="mini-title">
                <h3>HEAD TO HEAD</h3>
            </div>
            <div class="thumbnail">
                <div class="container-pie-chart">
                    <div class="col-box-chart">
                        <div class="text-title text-center">
                            <h4>Odds</h4>
                        </div>
                        <div class="full-width">
                            <div class="box-pie-chart inline-box">
                                <img src="/images/icon-play.png">
                            </div>
                            <div class="detail-pie-chart inline-box text-left">
                                <div>
                                    <h3>10</h3>
                                    <span>Win</span>
                                </div>
                                <div>
                                    <h3>10</h3>
                                    <span>Lose</span>
                                </div>
                                <div>
                                    <h3>10</h3>
                                    <span>Draw</span>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-box-chart">
                        <div class="text-title text-center">
                            <h4>Result</h4>
                        </div>
                        <div class="full-width">
                            <div class="box-pie-chart inline-box">
                                <img src="/images/icon-play.png">
                            </div>
                            <div class="detail-pie-chart inline-box text-left">
                                <div>
                                    <h3>10</h3>
                                    <span>Win</span>
                                </div>
                                <div>
                                    <h3>10</h3>
                                    <span>Lose</span>
                                </div>
                                <div>
                                    <h3>10</h3>
                                    <span>Draw</span>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="container-table-last">
                    <div class="box-match-list-title-last">


                        <div class="bh-ctn full-width text-center well well-sm">
                            <div class="form-inline-checkbox" data-toggle="buttons">
                                <label class="btn btn-xs btn-default active">
                                    <input type="checkbox" autocomplete="off" checked> All
                                </label>
                                <label class="btn btn-xs btn-default">
                                    <input type="checkbox" autocomplete="off"> Premier League
                                </label>
                                <label class="btn btn-xs btn-default">
                                    <input type="checkbox" autocomplete="off"> FA Cup
                                </label>
                                <label class="btn btn-xs btn-default">
                                    <input type="checkbox" autocomplete="off"> UEFA Champions League
                                </label>
                                <label class="btn btn-xs btn-default">
                                    <input type="checkbox" autocomplete="off"> EFL Cup
                                </label>

                            </div>
                        </div>
                        <div class="col-xs-12 text-center hide" style="margin-bottom: 10px;">
                            <div class="btn-group" role="group" aria-label="...">
                                <button type="button" class="btn btn-default btn-sm active">All</button>
                                <button type="button" class="btn btn-default btn-sm">Home</button>
                                <button type="button" class="btn btn-default btn-sm">Away</button>
                            </div>
                        </div>
                    </div>

                    <div class="box-match-tournament">
                        <div class="box-match-list-topic">
                            <div class="pull-left">
                                <img src="/images/logo-team/fa-cup-2017.png">
                                <p>FA Cup</p>
                            </div>
                            <div class="pull-right">
                                <ul>
                                    <li>Odds</li>
                                    <li>Result</li>
                                </ul>
                            </div>
                        </div>
                        <div class="box-match-list">

                            <div class="text-center">
                                <div class="date-match">15 Apr 18</div>
                                <div class="time-match">15:00</div>
                            </div>
                            <div>
                                <div class="home-name-match team-bet-color">
                                    <img src="/images/logo-team/Chelsea.png">Chelsea
                                    <div class="teamhome"></div>
                                </div>
                                <div class="away-name-match">
                                    <img src="/images/logo-team/manchester.png">Manchesterunited
                                </div>
                            </div>
                            <div>
                                <div class="home-score-match">0</div>
                                <div class="away-score-match">2</div>
                            </div>
                            <div class="text-center">
                                <div class="odds-match">
                                    <img src="/images/icon-stat/result/0.png">
                                </div>
                            </div>
                            <div class="text-center">
                                <div class="result-match">
                                    <img src="/images/icon-stat/result/2.png">
                                </div>
                            </div>
                        </div>
                        <div class="box-match-list">

                            <div class="text-center">
                                <div class="date-match">15 Apr 18</div>
                                <div class="time-match">15:00</div>
                            </div>
                            <div>
                                <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                    <div class="teamhome"></div>
                                </div>
                                <div class="away-name-match team-bet-color"><img
                                            src="/images/logo-team/manchester.png">Manchester
                                    united
                                </div>
                            </div>
                            <div>
                                <div class="home-score-match">0</div>
                                <div class="away-score-match">2</div>
                            </div>
                            <div class="text-center">
                                <div class="odds-match">
                                    <img src="/images/icon-stat/result/0.png">
                                </div>
                            </div>
                            <div class="text-center">
                                <div class="result-match">
                                    <img src="/images/icon-stat/result/2.png">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-match-tournament">
                        <div class="box-match-list-topic">
                            <div class="pull-left">
                                <img src="/images/five-leagues/premier-league-logo.png">
                                <p>Premier League</p>
                            </div>
                            <div class="pull-right">
                                <ul>
                                    <li>Odds</li>
                                    <li>Result</li>
                                </ul>
                            </div>
                        </div>
                        <div class="box-match-list">

                            <div class="text-center">
                                <div class="date-match">15 Apr 18</div>
                                <div class="time-match">15:00</div>
                            </div>
                            <div>
                                <div class="home-name-match team-bet-color"><img
                                            src="/images/logo-team/Chelsea.png">Chelsea
                                    <div class="teamhome"></div>
                                </div>
                                <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                    united
                                </div>
                            </div>
                            <div>
                                <div class="home-score-match">0</div>
                                <div class="away-score-match">2</div>
                            </div>
                            <div class="text-center">
                                <div class="odds-match">
                                    <img src="/images/icon-stat/result/0.png">
                                </div>
                            </div>
                            <div class="text-center">
                                <div class="result-match">
                                    <img src="/images/icon-stat/result/2.png">
                                </div>
                            </div>
                        </div>
                        <div class="box-match-list">

                            <div class="text-center">
                                <div class="date-match">15 Apr 18</div>
                                <div class="time-match">15:00</div>
                            </div>
                            <div>
                                <div class="home-name-match team-bet-color"><img
                                            src="/images/logo-team/Chelsea.png">Chelsea
                                    <div class="teamhome"></div>
                                </div>
                                <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                    united
                                </div>
                            </div>
                            <div>
                                <div class="home-score-match">0</div>
                                <div class="away-score-match">2</div>
                            </div>
                            <div class="text-center">
                                <div class="odds-match">
                                    <img src="/images/icon-stat/result/0.png">
                                </div>
                            </div>
                            <div class="text-center">
                                <div class="result-match">
                                    <img src="/images/icon-stat/result/2.png">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-match-tournament">
                        <div class="box-match-list-topic">
                            <div class="pull-left">
                                <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                <p>UEFA Champions League</p>
                            </div>
                            <div class="pull-right">
                                <ul>
                                    <li>Odds</li>
                                    <li>Result</li>
                                </ul>
                            </div>
                        </div>
                        <div class="box-match-list">

                            <div class="text-center">
                                <div class="date-match">15 Apr 18</div>
                                <div class="time-match">15:00</div>
                            </div>
                            <div>
                                <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                    <div class="teamhome"></div>
                                </div>
                                <div class="away-name-match team-bet-color"><img
                                            src="/images/logo-team/manchester.png">Manchester
                                    united
                                </div>
                            </div>
                            <div>
                                <div class="home-score-match">0</div>
                                <div class="away-score-match">2</div>
                            </div>
                            <div class="text-center">
                                <div class="odds-match">
                                    <img src="/images/icon-stat/result/0.png">
                                </div>
                            </div>
                            <div class="text-center">
                                <div class="result-match">
                                    <img src="/images/icon-stat/result/2.png">
                                </div>
                            </div>
                        </div>
                        <div class="box-match-list">

                            <div class="text-center">
                                <div class="date-match">15 Apr 18</div>
                                <div class="time-match">15:00</div>
                            </div>
                            <div>
                                <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                    <div class="teamhome"></div>
                                </div>
                                <div class="away-name-match team-bet-color"><img
                                            src="/images/logo-team/manchester.png">Manchester
                                    united
                                </div>
                            </div>
                            <div>
                                <div class="home-score-match">0</div>
                                <div class="away-score-match">2</div>
                            </div>
                            <div class="text-center">
                                <div class="odds-match">
                                    <img src="/images/icon-stat/result/0.png">
                                </div>
                            </div>
                            <div class="text-center">
                                <div class="result-match">
                                    <img src="/images/icon-stat/result/2.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--            FORM GUIDE-->
    <div id="form_guide_game" class="form-guide">
        <div class="container">
            <div class="mini-title">
                <h3>FORM GUIDE</h3>
            </div>
            <div class="thumbnail" style="margin-bottom: 0;">
                <div class="tab-header-stat">
                    <ul class="nav nav-tabs" id="myTabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#home-form-guide" id="home-tab" role="tab" data-toggle="tab"
                               aria-controls="home"
                               aria-expanded="true">
                                <div class="bx-tn">
                                    <img src="/images/logo-team/manchester.png">
                                    <span>Manchester United</span>
                                </div>

                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#away-form-guide" role="tab" id="profile-tab"
                               data-toggle="tab" aria-controls="profile"
                               aria-expanded="false">
                                <div class="bx-tn">
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>Chelsea</span>
                                </div>
                            </a>
                        </li>

                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active in" role="tabpanel" id="home-form-guide"
                             aria-labelledby="home-tab">

                            <div class="container-pie-chart">
                                <div class="col-box-chart">
                                    <div class="text-title text-center">
                                        <h4>Odds</h4>
                                    </div>
                                    <div class="full-width">
                                        <div class="box-pie-chart inline-box">
                                            <img src="/images/icon-play.png">
                                        </div>
                                        <div class="detail-pie-chart inline-box text-left">
                                            <div>
                                                <h3>10</h3>
                                                <span>Win</span>
                                            </div>
                                            <div>
                                                <h3>10</h3>
                                                <span>Lose</span>
                                            </div>
                                            <div>
                                                <h3>10</h3>
                                                <span>Draw</span>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-box-chart">
                                    <div class="text-title text-center">
                                        <h4>Result</h4>
                                    </div>
                                    <div class="full-width">
                                        <div class="box-pie-chart inline-box">
                                            <img src="/images/icon-play.png">
                                        </div>
                                        <div class="detail-pie-chart inline-box text-left">
                                            <div>
                                                <h3>10</h3>
                                                <span>Win</span>
                                            </div>
                                            <div>
                                                <h3>10</h3>
                                                <span>Lose</span>
                                            </div>
                                            <div>
                                                <h3>10</h3>
                                                <span>Draw</span>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="container-table-last">
                                <div class="box-match-list-title-last">


                                    <div class="bh-ctn full-width text-center well well-sm">
                                        <div class="form-inline-checkbox" data-toggle="buttons">
                                            <label class="btn btn-xs btn-default active">
                                                <input type="checkbox" autocomplete="off" checked> All
                                            </label>
                                            <label class="btn btn-xs btn-default">
                                                <input type="checkbox" autocomplete="off"> Premier League
                                            </label>
                                            <label class="btn btn-xs btn-default">
                                                <input type="checkbox" autocomplete="off"> FA Cup
                                            </label>
                                            <label class="btn btn-xs btn-default">
                                                <input type="checkbox" autocomplete="off"> UEFA Champions League
                                            </label>
                                            <label class="btn btn-xs btn-default">
                                                <input type="checkbox" autocomplete="off"> EFL Cup
                                            </label>

                                        </div>
                                    </div>
                                    <div class="col-xs-12 text-center hide" style="margin-bottom: 10px;">
                                        <div class="btn-group" role="group" aria-label="...">
                                            <button type="button" class="btn btn-default btn-sm active">All</button>
                                            <button type="button" class="btn btn-default btn-sm">Home</button>
                                            <button type="button" class="btn btn-default btn-sm">Away</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/logo-team/fa-cup-2017.png">
                                            <p>FA Cup</p>
                                        </div>
                                        <div class="pull-right">
                                            <ul>
                                                <li>Odds</li>
                                                <li>Result</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match team-bet-color">
                                                <img src="/images/logo-team/Chelsea.png">Chelsea
                                                <div class="teamhome"></div>
                                            </div>
                                            <div class="away-name-match">
                                                <img src="/images/logo-team/manchester.png">Manchesterunited
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">2</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="odds-match">
                                                <img src="/images/icon-stat/result/0.png">
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <div class="result-match">
                                                <img src="/images/icon-stat/result/2.png">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                                <div class="teamhome"></div>
                                            </div>
                                            <div class="away-name-match team-bet-color"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">2</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="odds-match">
                                                <img src="/images/icon-stat/result/0.png">
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <div class="result-match">
                                                <img src="/images/icon-stat/result/2.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/five-leagues/premier-league-logo.png">
                                            <p>Premier League</p>
                                        </div>
                                        <div class="pull-right">
                                            <ul>
                                                <li>Odds</li>
                                                <li>Result</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match team-bet-color"><img
                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                <div class="teamhome"></div>
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">2</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="odds-match">
                                                <img src="/images/icon-stat/result/0.png">
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <div class="result-match">
                                                <img src="/images/icon-stat/result/2.png">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match team-bet-color"><img
                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                <div class="teamhome"></div>
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">2</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="odds-match">
                                                <img src="/images/icon-stat/result/0.png">
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <div class="result-match">
                                                <img src="/images/icon-stat/result/2.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                            <p>UEFA Champions League</p>
                                        </div>
                                        <div class="pull-right">
                                            <ul>
                                                <li>Odds</li>
                                                <li>Result</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                                <div class="teamhome"></div>
                                            </div>
                                            <div class="away-name-match team-bet-color"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">2</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="odds-match">
                                                <img src="/images/icon-stat/result/0.png">
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <div class="result-match">
                                                <img src="/images/icon-stat/result/2.png">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                                <div class="teamhome"></div>
                                            </div>
                                            <div class="away-name-match team-bet-color"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">2</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="odds-match">
                                                <img src="/images/icon-stat/result/0.png">
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <div class="result-match">
                                                <img src="/images/icon-stat/result/2.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="away-form-guide"
                             aria-labelledby="profile-tab">

                            <div class="container-pie-chart">
                                <div class="col-box-chart">
                                    <div class="text-title text-center">
                                        <h4>Odds</h4>
                                    </div>
                                    <div class="full-width">
                                        <div class="box-pie-chart inline-box">
                                            <img src="/images/icon-play.png">
                                        </div>
                                        <div class="detail-pie-chart inline-box text-left">
                                            <div>
                                                <h3>10</h3>
                                                <span>Win</span>
                                            </div>
                                            <div>
                                                <h3>10</h3>
                                                <span>Lose</span>
                                            </div>
                                            <div>
                                                <h3>10</h3>
                                                <span>Draw</span>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-box-chart">
                                    <div class="text-title text-center">
                                        <h4>Result</h4>
                                    </div>
                                    <div class="full-width">
                                        <div class="box-pie-chart inline-box">
                                            <img src="/images/icon-play.png">
                                        </div>
                                        <div class="detail-pie-chart inline-box text-left">
                                            <div>
                                                <h3>10</h3>
                                                <span>Win</span>
                                            </div>
                                            <div>
                                                <h3>10</h3>
                                                <span>Lose</span>
                                            </div>
                                            <div>
                                                <h3>10</h3>
                                                <span>Draw</span>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="container-table-last">
                                <div class="box-match-list-title-last">


                                    <div class="bh-ctn full-width text-center well well-sm">
                                        <div class="form-inline-checkbox" data-toggle="buttons">
                                            <label class="btn btn-xs btn-default active">
                                                <input type="checkbox" autocomplete="off" checked> All
                                            </label>
                                            <label class="btn btn-xs btn-default">
                                                <input type="checkbox" autocomplete="off"> Premier League
                                            </label>
                                            <label class="btn btn-xs btn-default">
                                                <input type="checkbox" autocomplete="off"> FA Cup
                                            </label>
                                            <label class="btn btn-xs btn-default">
                                                <input type="checkbox" autocomplete="off"> UEFA Champions League
                                            </label>
                                            <label class="btn btn-xs btn-default">
                                                <input type="checkbox" autocomplete="off"> EFL Cup
                                            </label>

                                        </div>
                                    </div>
                                    <div class="col-xs-12 text-center hide" style="margin-bottom: 10px;">
                                        <div class="btn-group" role="group" aria-label="...">
                                            <button type="button" class="btn btn-default btn-sm active">All</button>
                                            <button type="button" class="btn btn-default btn-sm">Home</button>
                                            <button type="button" class="btn btn-default btn-sm">Away</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/logo-team/fa-cup-2017.png">
                                            <p>FA Cup</p>
                                        </div>
                                        <div class="pull-right">
                                            <ul>
                                                <li>Odds</li>
                                                <li>Result</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match team-bet-color">
                                                <img src="/images/logo-team/Chelsea.png">Chelsea
                                                <div class="teamhome"></div>
                                            </div>
                                            <div class="away-name-match">
                                                <img src="/images/logo-team/manchester.png">Manchesterunited
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">2</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="odds-match">
                                                <img src="/images/icon-stat/result/0.png">
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <div class="result-match">
                                                <img src="/images/icon-stat/result/2.png">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                                <div class="teamhome"></div>
                                            </div>
                                            <div class="away-name-match team-bet-color"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">2</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="odds-match">
                                                <img src="/images/icon-stat/result/0.png">
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <div class="result-match">
                                                <img src="/images/icon-stat/result/2.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/five-leagues/premier-league-logo.png">
                                            <p>Premier League</p>
                                        </div>
                                        <div class="pull-right">
                                            <ul>
                                                <li>Odds</li>
                                                <li>Result</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match team-bet-color"><img
                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                <div class="teamhome"></div>
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">2</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="odds-match">
                                                <img src="/images/icon-stat/result/0.png">
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <div class="result-match">
                                                <img src="/images/icon-stat/result/2.png">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match team-bet-color"><img
                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                <div class="teamhome"></div>
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">2</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="odds-match">
                                                <img src="/images/icon-stat/result/0.png">
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <div class="result-match">
                                                <img src="/images/icon-stat/result/2.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                            <p>UEFA Champions League</p>
                                        </div>
                                        <div class="pull-right">
                                            <ul>
                                                <li>Odds</li>
                                                <li>Result</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                                <div class="teamhome"></div>
                                            </div>
                                            <div class="away-name-match team-bet-color"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">2</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="odds-match">
                                                <img src="/images/icon-stat/result/0.png">
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <div class="result-match">
                                                <img src="/images/icon-stat/result/2.png">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                                <div class="teamhome"></div>
                                            </div>
                                            <div class="away-name-match team-bet-color"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">2</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="odds-match">
                                                <img src="/images/icon-stat/result/0.png">
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <div class="result-match">
                                                <img src="/images/icon-stat/result/2.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--            Next Match-->
    <div id="next_match_game" class="form-next-match">
        <div class="container">
            <div class="mini-title">
                <h3>Next Match</h3>
            </div>
            <div class="thumbnail" style="margin-bottom: 0;">
                <div class="tab-header-stat">
                    <ul class="nav nav-tabs" id="myTabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#home-next-match" id="home-tab" role="tab" data-toggle="tab"
                               aria-controls="home"
                               aria-expanded="true">
                                <div class="bx-tn">
                                    <img src="/images/logo-team/manchester.png">
                                    <span>Manchester United</span>
                                </div>
                            </a>
                        </li>
                        <li role="presentation" class=""><a href="#away-next-match" role="tab" id="profile-tab"
                                                            data-toggle="tab" aria-controls="profile"
                                                            aria-expanded="false">
                                <div class="bx-tn">
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>Chelsea</span>
                                </div>
                            </a></li>

                    </ul>
                    <div class="tab-content" id="myTabContent" style="margin-top: 15px;">
                        <div class="tab-pane fade active in" role="tabpanel" id="home-next-match"
                             aria-labelledby="home-tab">
                            <div class="container-table-last">

                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/logo-team/fa-cup-2017.png">
                                            <p>FA Cup</p>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/five-leagues/premier-league-logo.png">
                                            <p>Premier League</p>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>

                                    </div>
                                </div>
                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                            <p>UEFA Champions League</p>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="away-next-match"
                             aria-labelledby="home-tab">
                            <div class="container-table-last">

                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/logo-team/fa-cup-2017.png">
                                            <p>FA Cup</p>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/five-leagues/premier-league-logo.png">
                                            <p>Premier League</p>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>

                                    </div>
                                </div>
                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                            <p>UEFA Champions League</p>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img
                                                        src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!--            Table-->
    <div id="table_game" class="container">
        <div class="form-table-standing">

            <div class="mini-title">
                <h3>Table</h3>
            </div>
            <div class="thumbnail" style="margin-bottom: 0;">
                <div class="container-table-last">

                        <span class="pull-left"><img
                                    src="/images/five-leagues/premier-league-logo.png"> Season 18/19</span>
                    <div class="text-right">
                        <div class="btn-group" role="group" aria-label="...">
                            <button type="button" class="btn btn-default btn-xs active">All</button>
                            <button type="button" class="btn btn-default btn-xs">Home</button>
                            <button type="button" class="btn btn-default btn-xs">Away</button>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                    <div class="content-table-standing">
                        <table>
                            <tbody>
                            <tr>
                                <th></th>
                                <th>Team</th>
                                <th>P</th>
                                <th>GD</th>
                                <th>PTS</th>
                            </tr>
                            <tr class="box-highlight-team">
                                <td>1</td>
                                <td>
                                    <img src="/images/logo-team/manchester.png">
                                    <p>Manchester united</p>
                                </td>
                                <td>37</td>
                                <td>22</td>
                                <td>60</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <p>Arsenal</p>
                                </td>
                                <td>37</td>
                                <td>22</td>
                                <td>60</td>
                            </tr>
                            <tr class="box-highlight-team-2">
                                <td>3</td>
                                <td>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <p>Chelsea</p>
                                </td>
                                <td>37</td>
                                <td>19</td>
                                <td>55</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
                <div style="clear: both;"></div>
            </div>
        </div>

    </div>
</div>

<!--Smooth Scroll-->
<script>
    $("#nav li a[href^='#']").on('click', function (e) {

        // prevent default anchor click behavior
        e.preventDefault();

        // store hash
        var hash = this.hash;

        // animate
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 1000, function () {

            // when done, add hash to url
            // (default click behaviour)
            window.location.hash = hash;
        });

    });
</script>
<?php include("footer.php"); ?>
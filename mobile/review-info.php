<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Cache-control" content="public,max-age=604800">
    <link href="/mobile/css/all-style.css" rel="stylesheet">
    <link href="/mobile/css/article.css" rel="stylesheet">
    <link href="/mobile/css/bootstrap.css" rel="stylesheet">
    <link href="/mobile/css/docs.css" rel="stylesheet">
    <link href="/mobile/css/game.css" rel="stylesheet">
    <link href="/mobile/css/highlight.css" rel="stylesheet">
    <link href="/mobile/css/index.css" rel="stylesheet">
    <link href="/mobile/css/live-football.css" rel="stylesheet">
    <link href="/mobile/css/news.css" rel="stylesheet">
    <link href="/mobile/css/profile.css" rel="stylesheet">
    <link href="/mobile/css/ranking.css" rel="stylesheet">
    <link href="/mobile/css/review.css" rel="stylesheet">
    <link href="/mobile/css/livescore-mobile.css" rel="stylesheet">
    <link href="/mobile/css/football-symbol-mobile.css" rel="stylesheet">
    <link href="/mobile/css/swiper.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <script src="https://connect.facebook.net/en_US/sdk.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="/mobile/js/util.js"></script>
    <script src="/mobile/js/bet.js"></script>
    <script src="/mobile/js/login.js"></script>
    <script src="/mobile/js/comment.js"></script>


</head>
<?php
include_once __DIR__ . "/../bootstrap.php";

use Carbon\Carbon;

$mid = $_REQUEST['mid'];
if (empty($mid)) {
    header("location:/mobile");
}
$mcon = new \controller\MatchController();
$host = "https://ngoal.com/";

$match = $mcon->getMatch(($mid));
$mdt = Carbon::createFromFormat("Y-m-d H:i:s", $match->time_match, "Asia/singapore");
$mdt->setTimezone("Asia/Bangkok");

?>
<body style="background-color: #fff!important">
<?php include("menu-mobile.php"); ?>

<div class="modal-content-review full-width inline-box">
    <div class="container">
        <div class="text-title">
            <h4>เกมทายผล</h4>
        </div>
        <div class="full-width inline-box topic-team-review">
            <h4 id="main-home-name"><?php echo $match->teama->name_en; ?></h4>
            <h4>-</h4>
            <h4 id="main-away-name"><?php echo $match->teamb->name_en; ?></h4>
        </div>
        <div class="modal-body-tabs">
            <?php if (!empty($match->hdp)) { ?>
                <ul class="nav-review full-width inline-box">
                    <li class="nav-review-home bet-modal" side="home" mid="<?php echo $match->id; ?>"
                        data-toggle="modal"
                        data-target="#myModalHome">
                        <a href="javascript:void(0)">
                            <div class="full-width text-center">
                                <img id="main-home-logo" src="<?php echo $match->teama->getLogo(); ?>">
                            </div>
                            <div class="full-width text-center">
                                <button type="button" class="btn btn-primary bet-btn" side="home"
                                        mid="<?php echo $match->id; ?>">
                                    <?php echo number_format($match->hdp->home_water_bill, 2); ?>
                                </button>
                            </div>
                        </a>
                    </li>
                    <li>
                        <div class="HDP-box-center">
                            <div class="time-box match-time"
                                 at="<?php echo $match->time_match ?>"><?php echo $mdt->format("H:i"); ?></div>
                            <strong>HDP</strong>
                            <h2 class="live-hdp-play"
                                mid="<?php echo $match->id; ?>"
                                home="<?php echo number_format($match->hdp->home_water_bill, 2); ?>"
                                away="<?php echo number_format($match->hdp->away_water_bill, 2); ?>"
                                hdp="<?php echo $match->hdp->handicap; ?>"><?php echo $match->hdp->handicap; ?></h2>
                            <div class="box-water-text hide">
                                <div>1.9</div>
                                <div>1.69</div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-review-away bet-modal" side="away" mid="<?php echo $match->id; ?>"
                        data-toggle="modal"
                        data-target="#myModalAway">
                        <a href="javascript:void(0)">
                            <div class="full-width text-center">
                                <img id="main-away-logo" src="<?php echo $match->teamb->getLogo(); ?>">
                            </div>
                            <div class="full-width text-center">
                                <button type="button" class="btn btn-success bet-btn" side="away"
                                        mid="<?php echo $match->id; ?>">
                                    <?php echo number_format($match->hdp->away_water_bill, 2); ?>
                                </button>
                            </div>
                        </a>
                    </li>
                </ul>
            <?php } ?>
        </div>

        <div class="content-vote-review">
            <div class="box-vote-win full-width inline-box text-center">

                <div class="title-center padding-top margin-default">
                    โหวดผลการแข่งขัน
                </div>

                <div class="box-vote-win-btn inline-box">

                    <div class="btn-group full-width" role="group" aria-label="...">
                        <button type="button" class="btn btn-primary">Home</button>
                        <button type="button" class="btn btn-default">Draw</button>
                        <button type="button" class="btn btn-success">Away</button>

                    </div>
                </div>
            </div>
            <div class="box-progress vote-win full-width inline-box text-center">
                <div class="progress">
                    <div class="progress-bar progress-bar-primary" style="width: 35%">
                        35%
                    </div>
                    <div class="progress-bar progress-bar-draw" style="width: 20%">
                        20%
                    </div>
                    <div class="progress-bar progress-bar-success" style="width: 45%">
                        45%
                    </div>
                </div>
            </div>
        </div>
        <div class="title-center margin-default padding-bottom">
            ทีเด็ดทรรศนะ
        </div>
    </div>


    <div class="tab-review">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#HomeReview" role="tab" data-toggle="tab">
                    <img src="<?php echo $match->teama->getLogo(); ?>">
                    <h4><?php echo $match->teama->name_en ?></h4>
                </a>
            </li>
            <li role="presentation">
                <a href="#AwayReview" role="tab" data-toggle="tab">
                    <img src="<?php echo $match->teamb->getLogo(); ?>">
                    <h4><?php echo $match->teamb->name_en; ?></h4>
                </a>
            </li>
        </ul>
    </div>

    <div class="tab-content tab-content-review">
        <div id="HomeReview" class="tab-pane fade in active">

        </div>
        <div id="AwayReview" class="tab-pane fade">

        </div>
    </div>
</div>

<div class="box-add-review" id="bx-add-review">
    <div class="inline-box">
        <div class="text-guide-add">
            Add Review
        </div>
    </div>
    <div class="inline-box">
        <span><i class="fa fa-comments" aria-hidden="true"></i></span>
    </div>
</div>

<div class="modal modal-add-review fade" id="commentReview" tabindex="-1" role="dialog"
     aria-labelledby="">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">แสดงรีวิว ทรรศนะ</h4>
            </div>
            <div class="modal-body">
                <?php if (isset($_SESSION['login'])) { ?>
                    <div class="full-width inline-box topic-review-modal">
                        <ul>
                            <li>
                                <img class="login-avatar-plate" src="<?php echo "https://graph.facebook.com/v3.2/".$_SESSION['login']['fb_uid']."/picture?type=normal" ?>">
                            </li>
                            <li>
                                <h6 class="login-name-plate"><?php echo $_SESSION['login']['name']; ?></h6>
                                <span><img class="select-team-logo" src="/images/logo-team/manchester.png"></span>
                                <span class="select-team-name">@Manchester United</span>
                            </li>
                        </ul>
                    </div>
                <?php } ?>
                <textarea id="comment-box" class="form-control" placeholder="เขียนทรรศนะ..." rows="10" maxlength="250"></textarea>
                <!--                <div class="alert alert-info" style="margin-top: 10px;" role="alert">-->
                <!--                    <strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> แจ้งเตือน</strong>-->
                <!--                    ทรรศะเดิมคุณจะหาย ถ้าเพิ่มทรรศนะใหม่เข้าไป-->
                <!--                </div>-->
            </div>


            <div class="modal-footer full-width text-center">
                <button id="add-comment" type="button" class="btn btn-primary btn-block">เพิ่มทรรศนะ</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    /* When the user scrolls down, hide the navbar. When the user scrolls up, show the navbar */
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function () {
        var currentScrollPos = window.pageYOffset;
        if (prevScrollpos > currentScrollPos) {
            document.getElementById("bx-add-review").style.bottom = "70px";
        } else {
            document.getElementById("bx-add-review").style.bottom = "-70px";
        }
        prevScrollpos = currentScrollPos;
    }
</script>

<?php include("footer.php"); ?>

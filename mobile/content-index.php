<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/HomeM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/VideoM.php';
$homem = new HomeM();
$VideoMObj = new VideoM();
$recommendnews = $homem->getRecommentNews();
$highlightvideo = $VideoMObj->getSlideVideos("highlight", 24);
?>
<!--ข่าว-->
<div class="container container-news-index">
    <div class="text-title">
        <a class="link_wrap" href="/mobile/news.php"></a>
        <h4>ข่าว</h4>
        <p>More</p>
    </div>
    <div class="row">
        <div class="col-lg-12 col-news">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <?php foreach ($recommendnews as $key=>$value){ ?>
                        <div class="swiper-slide">
                            <a class="link_wrap" href="/mobile/news-info.php?id=<?php echo $value->newsid?>"></a>
                            <div class="item-recent">
                                <div><img src="<?php echo $value->imageLink?>"></div>
                                <div class="bx-2">
                                    <h4 class="media-heading"><?php echo $value->titleTh; ?></h4>
                                    <div class="time-box box-inline how-long"><?php echo $value->createDatetime; ?></div>
                                    <div class="vertical-line-between box-inline"></div>
                                    <div class="view-box box-inline"><?php echo $value->readCount; ?></div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination" style="width: 100%;"></div>
            </div>
        </div>
    </div>
</div>
<!--ไฮไลท์ฟุตบอล-->
<div class="container-fluid" style="background-color: #f1f2f5; ">
    <div class="col-highlight">
        <div class="text-title">
            <a class="link_wrap" href="/mobile/highlight-football.php"></a>
            <h4>ไฮไลท์ฟุตบอล</h4>
            <p>More</p>
        </div>
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach ($highlightvideo as $key=>$value){?>
                    <?php if((($key+1)%2)==1){ ?>
                    <div class="swiper-slide">
                        <div class="content-wrap-mobile">
                            <a class="link_wrap" href="/mobile/highlight-football.php?id=<?php echo $value->video_id;?>"></a>
                            <div class="thumbnail-highlight">
                                <div class="thumbnail-img">
                                    <img src="<?php echo $value->urlImg;?>">
                                </div>
                                <div class="caption">
                                    <p><?php echo $value->title;?></p>
                                    <div class="time-box box-inline how-long"><?php echo $value->create_datetime;?></div>
                                    <div class="vertical-line-between box-inline"></div>
                                    <div class="view-box box-inline"><?php echo $value->view;?></div>
                                </div>
                            </div>
                        </div>
                    <?php }else{ ?>
                        <div class="content-wrap-mobile">
                            <a class="link_wrap" href="/mobile/highlight-football.php?id=<?php echo $value->video_id;?>"></a>
                            <div class="thumbnail-highlight">
                                <div class="thumbnail-img">
                                    <img src="<?php echo $value->urlImg;?>">
                                </div>
                                <div class="caption">
                                    <p><?php echo $value->title;?></p>
                                    <div class="time-box box-inline how-long"><?php echo $value->create_datetime;?></div>
                                    <div class="vertical-line-between box-inline"></div>
                                    <div class="view-box box-inline"><?php echo $value->view;?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                <?php } ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
        </div>
    </div>
</div>


<!--ทีเด็ด ทรรศนะ-->
<div class="container">
    <div class="row">
        <!--        Review-->
        <div class="col-lg-12">
            <div class="text-title">
                <a class="link_wrap" href="/mobile/review.php"></a>
                <h4>ทีเด็ด ทรรศนะ</h4>
                <p>More</p>
            </div>
            <div class="thumbnail-review thumbnail-review-index">
                <div class="top-section">
                    <a class="link_wrap" href="/mobile/game.php"></a>
                    <div class="topic-review">
                        <div class="topic-review-title">
                            <h4>UEFA Champions League</h4>
                            <div class="date-box">03/07/2018</div>
                        </div>

                        <div class="more-box">
                            <h6>ดูทั้งหมด</h6>
                        </div>
                    </div>
                    <div class="content-top-review">


                        <div class="home-review-box">
                            <div class="logo"><img src="/images/logo-team/manchester.png"></div>
                            <div class="name">Man United</div>
                            <div class="mobile-vote-team active">
                                <h4>Voted</h4>
                                <div class="bg-mobile-vote-team"></div>
                            </div>
                            <ul class="last-match">
                                <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                        <div class="HDP-box-center">
                            <!--                        <div class="time-box">01:00</div>-->
                            <strong>HDP</strong>
                            <h2>-2.50</h2>
                            <div class="box-water-text">
                                <div>1.9</div>
                                <div>1.69</div>
                            </div>
                            <div class="box-chanel">
                                <ul>
                                    <li>Live On</li>
                                    <li><img src="/images/channel/True-Sport-1-1.png"></li>
                                </ul>
                            </div>
                        </div>


                        <div class="away-review-box">
                            <div class="logo"><img src="/images/logo-team/Chelsea.png"></div>
                            <div class="name">Chelsea</div>
                            <div class="mobile-vote-team">
                                <h4>Vote</h4>
                                <div class="bg-mobile-vote-team"></div>
                            </div>
                            <ul class="last-match">
                                <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            </ul>
                        </div>

                    </div>
                    <div class="progress">
                        <div class="progress-bar" style="width: 35%">
                            <span>35%</span>
                        </div>
                        <div class="progress-bar progress-bar-danger" style="width: 65%">
                            <span>65%</span>
                        </div>
                    </div>

                </div>
                <div class="box-review-list">
                    <a class="link_wrap" href="/mobile/review-info.php"></a>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo01.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo02.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo03.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div class="user-bet"><img src="/images/3.jpg"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo04.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear: both"></div>
            </div>
            <div class="thumbnail-review thumbnail-review-index">
                <div class="top-section">
                    <a class="link_wrap" href="/mobile/game.php"></a>
                    <div class="topic-review">
                        <div class="topic-review-title">
                            <h4>UEFA Champions League</h4>
                            <div class="date-box">03/07/2018</div>
                        </div>

                        <div class="more-box">
                            <h6>ดูทั้งหมด</h6>
                        </div>
                    </div>
                    <div class="content-top-review">


                        <div class="home-review-box">
                            <div class="logo"><img src="/images/logo-team/manchester.png"></div>
                            <div class="name">Man United</div>
                            <div class="mobile-vote-team active">
                                <h4>Voted</h4>
                                <div class="bg-mobile-vote-team"></div>
                            </div>
                            <ul class="last-match">
                                <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                        <div class="HDP-box-center">
                            <!--                        <div class="time-box">01:00</div>-->
                            <strong>HDP</strong>
                            <h2>-2.50</h2>
                            <div class="box-water-text">
                                <div>1.9</div>
                                <div>1.69</div>
                            </div>
                            <div class="box-chanel">
                                <ul>
                                    <li>Live On</li>
                                    <li><img src="/images/channel/True-Sport-1-1.png"></li>
                                </ul>
                            </div>
                        </div>


                        <div class="away-review-box">
                            <div class="logo"><img src="/images/logo-team/Chelsea.png"></div>
                            <div class="name">Chelsea</div>
                            <div class="mobile-vote-team">
                                <h4>Vote</h4>
                                <div class="bg-mobile-vote-team"></div>
                            </div>
                            <ul class="last-match">
                                <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            </ul>
                        </div>

                    </div>
                    <div class="progress">
                        <div class="progress-bar" style="width: 35%">
                            <span>35%</span>
                        </div>
                        <div class="progress-bar progress-bar-danger" style="width: 65%">
                            <span>65%</span>
                        </div>
                    </div>

                </div>
                <div class="box-review-list">
                    <a class="link_wrap" href="/mobile/review-info.php"></a>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo01.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo02.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo03.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div class="user-bet"><img src="/images/3.jpg"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo04.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear: both"></div>
            </div>
            <div class="thumbnail-review thumbnail-review-index">
                <div class="top-section">
                    <a class="link_wrap" href="/mobile/game.php"></a>
                    <div class="topic-review">
                        <div class="topic-review-title">
                            <h4>UEFA Champions League</h4>
                            <div class="date-box">03/07/2018</div>
                        </div>

                        <div class="more-box">
                            <h6>ดูทั้งหมด</h6>
                        </div>
                    </div>
                    <div class="content-top-review">


                        <div class="home-review-box">
                            <div class="logo"><img src="/images/logo-team/manchester.png"></div>
                            <div class="name">Man United</div>
                            <div class="mobile-vote-team active">
                                <h4>Voted</h4>
                                <div class="bg-mobile-vote-team"></div>
                            </div>
                            <ul class="last-match">
                                <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                        <div class="HDP-box-center">
                            <!--                        <div class="time-box">01:00</div>-->
                            <strong>HDP</strong>
                            <h2>-2.50</h2>
                            <div class="box-water-text">
                                <div>1.9</div>
                                <div>1.69</div>
                            </div>
                            <div class="box-chanel">
                                <ul>
                                    <li>Live On</li>
                                    <li><img src="/images/channel/True-Sport-1-1.png"></li>
                                </ul>
                            </div>
                        </div>


                        <div class="away-review-box">
                            <div class="logo"><img src="/images/logo-team/Chelsea.png"></div>
                            <div class="name">Chelsea</div>
                            <div class="mobile-vote-team">
                                <h4>Vote</h4>
                                <div class="bg-mobile-vote-team"></div>
                            </div>
                            <ul class="last-match">
                                <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            </ul>
                        </div>

                    </div>
                    <div class="progress">
                        <div class="progress-bar" style="width: 35%">
                            <span>35%</span>
                        </div>
                        <div class="progress-bar progress-bar-danger" style="width: 65%">
                            <span>65%</span>
                        </div>
                    </div>

                </div>
                <div class="box-review-list">
                    <a class="link_wrap" href="/mobile/review-info.php"></a>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo01.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo02.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo03.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div class="user-bet"><img src="/images/3.jpg"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo04.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear: both"></div>
            </div>
            <div class="thumbnail-review thumbnail-review-index">
                <div class="top-section">
                    <a class="link_wrap" href="/mobile/game.php"></a>
                    <div class="topic-review">
                        <div class="topic-review-title">
                            <h4>UEFA Champions League</h4>
                            <div class="date-box">03/07/2018</div>
                        </div>

                        <div class="more-box">
                            <h6>ดูทั้งหมด</h6>
                        </div>
                    </div>
                    <div class="content-top-review">


                        <div class="home-review-box">
                            <div class="logo"><img src="/images/logo-team/manchester.png"></div>
                            <div class="name">Man United</div>
                            <div class="mobile-vote-team active">
                                <h4>Voted</h4>
                                <div class="bg-mobile-vote-team"></div>
                            </div>
                            <ul class="last-match">
                                <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                        <div class="HDP-box-center">
                            <!--                        <div class="time-box">01:00</div>-->
                            <strong>HDP</strong>
                            <h2>-2.50</h2>
                            <div class="box-water-text">
                                <div>1.9</div>
                                <div>1.69</div>
                            </div>
                            <div class="box-chanel">
                                <ul>
                                    <li>Live On</li>
                                    <li><img src="/images/channel/True-Sport-1-1.png"></li>
                                </ul>
                            </div>
                        </div>


                        <div class="away-review-box">
                            <div class="logo"><img src="/images/logo-team/Chelsea.png"></div>
                            <div class="name">Chelsea</div>
                            <div class="mobile-vote-team">
                                <h4>Vote</h4>
                                <div class="bg-mobile-vote-team"></div>
                            </div>
                            <ul class="last-match">
                                <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            </ul>
                        </div>

                    </div>
                    <div class="progress">
                        <div class="progress-bar" style="width: 35%">
                            <span>35%</span>
                        </div>
                        <div class="progress-bar progress-bar-danger" style="width: 65%">
                            <span>65%</span>
                        </div>
                    </div>

                </div>
                <div class="box-review-list">
                    <a class="link_wrap" href="/mobile/review-info.php"></a>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo01.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo02.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo03.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div class="user-bet"><img src="/images/3.jpg"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo04.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear: both"></div>
            </div>
            <div class="thumbnail-review thumbnail-review-index">
                <div class="top-section">
                    <a class="link_wrap" href="/mobile/game.php"></a>
                    <div class="topic-review">
                        <div class="topic-review-title">
                            <h4>UEFA Champions League</h4>
                            <div class="date-box">03/07/2018</div>
                        </div>

                        <div class="more-box">
                            <h6>ดูทั้งหมด</h6>
                        </div>
                    </div>
                    <div class="content-top-review">


                        <div class="home-review-box">
                            <div class="logo"><img src="/images/logo-team/manchester.png"></div>
                            <div class="name">Man United</div>
                            <div class="mobile-vote-team active">
                                <h4>Voted</h4>
                                <div class="bg-mobile-vote-team"></div>
                            </div>
                            <ul class="last-match">
                                <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                        <div class="HDP-box-center">
                            <!--                        <div class="time-box">01:00</div>-->
                            <strong>HDP</strong>
                            <h2>-2.50</h2>
                            <div class="box-water-text">
                                <div>1.9</div>
                                <div>1.69</div>
                            </div>
                            <div class="box-chanel">
                                <ul>
                                    <li>Live On</li>
                                    <li><img src="/images/channel/True-Sport-1-1.png"></li>
                                </ul>
                            </div>
                        </div>


                        <div class="away-review-box">
                            <div class="logo"><img src="/images/logo-team/Chelsea.png"></div>
                            <div class="name">Chelsea</div>
                            <div class="mobile-vote-team">
                                <h4>Vote</h4>
                                <div class="bg-mobile-vote-team"></div>
                            </div>
                            <ul class="last-match">
                                <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            </ul>
                        </div>

                    </div>
                    <div class="progress">
                        <div class="progress-bar" style="width: 35%">
                            <span>35%</span>
                        </div>
                        <div class="progress-bar progress-bar-danger" style="width: 65%">
                            <span>65%</span>
                        </div>
                    </div>

                </div>
                <div class="box-review-list">
                    <a class="link_wrap" href="/mobile/review-info.php"></a>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo01.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo02.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo03.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div class="user-bet"><img src="/images/3.jpg"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo04.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear: both"></div>
            </div>
            <div class="thumbnail-review thumbnail-review-index">
                <div class="top-section">
                    <a class="link_wrap" href="/mobile/game.php"></a>
                    <div class="topic-review">
                        <div class="topic-review-title">
                            <h4>UEFA Champions League</h4>
                            <div class="date-box">03/07/2018</div>
                        </div>

                        <div class="more-box">
                            <h6>ดูทั้งหมด</h6>
                        </div>
                    </div>
                    <div class="content-top-review">


                        <div class="home-review-box">
                            <div class="logo"><img src="/images/logo-team/manchester.png"></div>
                            <div class="name">Man United</div>
                            <div class="mobile-vote-team active">
                                <h4>Voted</h4>
                                <div class="bg-mobile-vote-team"></div>
                            </div>
                            <ul class="last-match">
                                <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                        <div class="HDP-box-center">
                            <!--                        <div class="time-box">01:00</div>-->
                            <strong>HDP</strong>
                            <h2>-2.50</h2>
                            <div class="box-water-text">
                                <div>1.9</div>
                                <div>1.69</div>
                            </div>
                            <div class="box-chanel">
                                <ul>
                                    <li>Live On</li>
                                    <li><img src="/images/channel/True-Sport-1-1.png"></li>
                                </ul>
                            </div>
                        </div>


                        <div class="away-review-box">
                            <div class="logo"><img src="/images/logo-team/Chelsea.png"></div>
                            <div class="name">Chelsea</div>
                            <div class="mobile-vote-team">
                                <h4>Vote</h4>
                                <div class="bg-mobile-vote-team"></div>
                            </div>
                            <ul class="last-match">
                                <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                                <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            </ul>
                        </div>

                    </div>
                    <div class="progress">
                        <div class="progress-bar" style="width: 35%">
                            <span>35%</span>
                        </div>
                        <div class="progress-bar progress-bar-danger" style="width: 65%">
                            <span>65%</span>
                        </div>
                    </div>

                </div>
                <div class="box-review-list">
                    <a class="link_wrap" href="/mobile/review-info.php"></a>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo01.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo02.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo03.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div class="user-bet"><img src="/images/3.jpg"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                    <div class="box-review-list-row">
                        <div><img src="/images/profess-player/sample-logo04.png"></div>
                        <div class="text-add-team">
                            <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                            <div class="detail-review">
                                "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                รัสเซีย
                                ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                                กองหน้าตัวเก่ง
                                และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                                ชุดนี้มีตัวดังๆ
                                ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                                รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                                แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                                รัสเซีย
                                จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                                ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                                ฟันธง : รอง ซาอุดีอาระเบีย"
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear: both"></div>
            </div>
        </div>
    </div>
</div>


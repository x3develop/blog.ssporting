<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/mobile/css/all-style.css" rel="stylesheet">
    <link href="/mobile/css/article.css" rel="stylesheet">
    <link href="/mobile/css/bootstrap.css" rel="stylesheet">
    <link href="/mobile/css/docs.css" rel="stylesheet">
    <link href="/mobile/css/game.css" rel="stylesheet">
    <link href="/mobile/css/highlight.css" rel="stylesheet">
    <link href="/mobile/css/index.css" rel="stylesheet">
    <link href="/mobile/css/live-football.css" rel="stylesheet">
    <link href="/mobile/css/news.css" rel="stylesheet">
    <link href="/mobile/css/profile.css" rel="stylesheet">
    <link href="/mobile/css/ranking.css" rel="stylesheet">
    <link href="/mobile/css/review.css" rel="stylesheet">
    <link href="/mobile/css/livescore-mobile.css" rel="stylesheet">
    <link href="/mobile/css/football-symbol-mobile.css" rel="stylesheet">
    <link href="/mobile/css/swiper.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone.min.js"></script>
    <script src="/js/newFB.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/mobile/js/news-info.js"></script>
</head>
<?php
include_once __DIR__ . "/../bootstrap.php";
use Carbon\Carbon;

$id = $_REQUEST['id'];
if (empty($id)) {
    header("location:/mobile/news");
}
$ncon = new \controller\NewsController();
$news = $ncon->get($id);
$related = $ncon->getRelated($id);

$posttime = Carbon::createFromFormat("Y-m-d H:i:s", $news->created_at, "UTC");
$posttime->setTimezone("Asia/Bangkok");
$host = "https://ngoal.com/";
?>
<style>
    body {
        background-color: #fff !important
    }

    .content-nav-footer ul li.active-nf-news .nf-news:before,
    .content-nav-footer ul li.active-nf-news span {
        color: rgba(255, 255, 255, 1);
    }
</style>
<body>
<?php include "menu-mobile.php"; ?>
<div class="container" id="main-news-box">
    <div class="row news-row">
        <div class="container">
            <div class="row">
                <div class="feature-news" style="background-image:url(<?php echo $host . $news->imageLink; ?>);"></div>
            </div>
        </div>
        <div class="container-title-news-info">
            <span class="label label-info">ข่าวพรีเมียร์ลีก</span>
            <h1 class="headline"><?php echo $news->titleTh; ?></h1>
            <div class="content-news-info">
                <div class="time-box box-inline post-at" at="<?php echo $posttime ?>"></div>
                <div class="vertical-line-between box-inline"></div>
                <div class="view-box box-inline"><?php echo $news->readCount; ?></div>
            </div>
        </div>
        <div class="container">

            <div class="container-share-news text-center full-width">
                <span class="ft-share-box">Share</span>
                <span class="gp-share-box">Share</span>
            </div>
        </div>
        <div class="container container-detail-news-info">
            <p class="news-detail">
                <?php echo $news->contentTh; ?>
            </p>
        </div>
        <div class="container">
            <div class="bx-publish-recommend">
                <div class="text-title">
                    <h4>Related</h4>
                </div>
                <div class="content-recent">
                    <?php foreach ($related as $rnews) { ?>
                        <?php
                        $rposttime = Carbon::createFromFormat("Y-m-d H:i:s", $rnews->created_at, "UTC");
                        $rposttime->setTimezone("Asia/Bangkok");
                        ?>
                        <div class="content-item-recent" newsid="<?php echo $rnews->newsId; ?>">
                            <a class="link_wrap" href="/mobile/news-info?id=<?php echo $rnews->newsId; ?>"></a>
                            <div class="item-recent">
                                <div><img src="<?php echo $host . $rnews->imageLink; ?>"></div>
                                <div class="bx-2">
                                    <h4 class="media-heading"><?php echo $rnews->titleTh; ?></h4>
                                    <div class="time-box box-inline post-at" at="<?php echo $rposttime ?>"></div>
                                    <div class="vertical-line-between box-inline"></div>
                                    <div class="view-box box-inline"><?php echo $news->readCount; ?></div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include "footer.php"; ?>
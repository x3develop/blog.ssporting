<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>รวบรวมคลิปวีดีโอกีฬาที่น่าสนใจทั่วโลก คลิปบอล วีดีโอบอล จังหวะยิง คลิปเลี้ยงบอล ส่งบอล</title>
    <meta name="keywords" content="วีดีโอบอล, วีดีโอบอล, คลิปบอล, ไฮไลท์ฟุตบอล, ไฮไลท์บอลเมื่อคืน, บอลเมื่อคืน, บอลล่าสุด, เร็วที่สุด, คมชัดที่สุด">
    <meta name="description" content="รวบรวมคลิปวีดีโอกีฬาที่น่าสนใจทั่วโลก ไม่ว่าจะเป็นคลิปจังหวะยิงประตูสวยๆ คลิปเลี้ยงบอล คลิปส่งบอล คลิปประวัติตำนานนักเตะ อัพเดททุกวัน คมชัดระดับHD">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="shortcut icon" type="images/x-icon" href="/images/favicon.ico.png">
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="/js/flogin/src/flogin.js"></script>
    <script src="/js/moment.js"></script>
    <?php include 'view/index/googletagmanager.php'; ?>
</head>
<body style="background-color: #f1f1f1;">
<?php
require_once 'model/VideoM.php';
$VideoMObj = new VideoM();
$Page = (isset($_GET['Page']) ? $_GET['Page'] : 1);

if (isset($Page)) {
    $dataVideoAll = $VideoMObj->getAllVideo(($Page - 1) * 12, 12, 'general');
    $dataMoreRecentedVideo = $VideoMObj->getAllVideo(($Page) * 12, 12, 'highlight');
} else {
    $dataVideoAll = $VideoMObj->getAllVideo(0, 12, 'general');
    $dataMoreRecentedVideo = $VideoMObj->getAllVideo((1 * 12), 12, 'highlight');
}
$maxPage = array();
$maxPage = $VideoMObj->getAllVideoCountPage(12, 'general');
//$dataMoreRecentedGallery=$GalleryObject->getMoreRecentedGallery('น่ารัก',12);

?>
<!--index menu-->
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<!--<nav class="navbar navbar-menu-v2">-->
<!--    --><?php //include 'view/menu/navbar-menu-v2.php'; ?>
<!--</nav>-->


<div class="wrapper-news-articles">
    <div class="bx-head-fixtures bx-head-fixtures-hothit">
        <div class="container">
            <div class="hilight-cover-videos"></div>
            <div class="articles-title">

                <div class="full-width text-left">
                    <h1 class="head-title-top full-width">
                        HotHit Video
                    </h1>
                    <p>รวบรวมคลิปวีดีโอกีฬาที่น่าสนใจทั่วโลก คลิป วีดีโอ ที่คนชอบมากที่สุด ไม่ว่าจะเป็นคลิปจังหวะยิงประตูสวยๆ คลิปเลี้ยงบอล ขั้นเทพ คลิปส่งบอล คลิปประวัติตำนานนักเตะ อัพเดททุกวัน คมชัดระดับHD </p>
                </div>
            </div>
            <!--            <div class="background-image_image2" style="background-image: url('/images/gallery/6.jpg');"></div>-->
        </div>
    </div>
    <div class="container container-all-hothit">
        <div class="wrap-content-cols">
            <div class="row">
                <div class="col-sm-9">
                    <div class="topic-hothit full-width">
                        <h3 class="title-sortBy inline-box inline-box-middle margin-default">
                            <span class="most-pop hide">Most popular</span>
                            <span class="recent-update">Recent Update</span>
                            <span class="last-update hide">Last Update</span>
                        </h3>
                        <div class="bx-btn-sortBy inline-box inline-box-middle ">
                            <div class="pull-right ">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    Sort By
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <li><a href="#"><span class="most-pop">Most popular</span></a></li>
                                    <li><a href="#"><span class="recent-update">Recent Update</span></a></li>
                                    <li><a href="#"><span class="last-update">Last Update</span></a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                    <div class="content-articles">
                        <div class="wrap-gallery mg-bt15">
                            <?php foreach ($dataVideoAll as $key => $value) { ?>
                                <div class="col-md-4">

                                    <div class="wrap-box-gallery"
                                         onclick="location.href='/hothit-videos?videoId=<?php echo $value->video_id ?>'">
                                        <a class="link_wrap"
                                           href="/hothit-videos?videoId=<?php echo $value->video_id ?>"></a>
                                        <div class="box-videos box-related box-related-sexy" style="height: 159px;">
                                            <div class="bx-type-gallery">
                                                <?php if ($value->videosource == 'streamable') { ?>
                                                    <i class="fa fa-video-camera"></i>
                                                <?php } else if ($value->videosource == 'picture') { ?>
                                                    <i class="fa fa-picture-o"></i>
                                                <?php } ?>
                                            </div>

                                            <?php if ($value->videosource == 'streamable') { ?>
                                                <div class="crop">
                                                    <img src="<?php echo((!empty($value->thumbnail)) ? $value->thumbnail : '/images/image-not-found.png') ?>">
                                                </div>
                                            <?php } else if ($value->videosource == 'video') { ?>
                                                <div class="crop">
                                                    <img src="<?php echo((!empty($value->thumbnail)) ? $value->thumbnail : '/images/image-not-found.png') ?>">
                                                </div>
                                            <?php } else if ($value->videosource == 'youtube') { ?>
                                                <div class="crop">
                                                    <img src="<?php echo((!empty($value->thumbnail)) ? $value->thumbnail : '/images/image-not-found.png') ?>">
                                                </div>
                                            <?php } ?>
                                        </div>
                                      <div class="detail-bx-hothit">

                                          <h4>
                                              <?php echo $value->title; ?>
                                          </h4>
                                          <div class="tab-datetime">
                                            <span class="pull-left">
                                                <?php if (!empty($value->create_datetime)) { ?>
                                                    <?php echo date(' j/n/Y', strtotime($value->create_datetime)) ?>
                                                <?php } ?>
                                            </span>
                                              <span class="pull-right"><i
                                                          class="fa fa-eye"></i> <?php echo $value->view; ?></span>
                                              <div style="clear: both;"></div>
                                          </div>
                                      </div>

                                    </div>
                                </div>
                                <?php if (($key + 1) % 3 == 0) { ?>
                                    <div style="clear: both;"></div>
                                <?php }
                            } ?>
                        </div>
                        <div style="clear: both;"></div>


                        <div class="text-center">
                            <nav aria-label="Page navigation" count="<?php echo $maxPage[0]->maxPage; ?>">
                                <ul class="pagination">
                                    <?php if ((isset($Page) && $Page > 1)) { ?>
                                        <li>
                                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?Page=<?php echo $Page - 1; ?>"
                                               aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <?php for ($i = 1; $i <= $maxPage[0]->maxPage; $i++) { ?>
                                        <?php if ((($Page + 4 > $maxPage[0]->maxPage) and $i < 4) || (($i >= ($Page - 3) && $i <= ($Page + 3)) || $i >= ($maxPage[0]->maxPage - 3))) { ?>
                                            <li class="<?php echo((isset($Page) && $Page == $i) ? 'active' : '') ?>"><a
                                                        href="<?php echo $_SERVER['PHP_SELF']; ?>?Page=<?php echo $i; ?>"><?php echo $i ?></a>
                                            </li>
                                        <?php } else { ?>
                                            <?php if ((($Page + 4 > $maxPage[0]->maxPage) and $i == 4) || (($i == $Page + 4) and ($Page + 4 < $maxPage[0]->maxPage))) { ?>
                                                <li class="disabled"><a>...</a></li>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if ((isset($Page) && $Page < $maxPage[0]->maxPage)) { ?>
                                        <li>
                                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?Page=<?php echo $Page + 1; ?>"
                                               aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="bx-publish-recommend">
                        <div class="title">
                            <h3>ไฮไลท์ฟุตบอล</h3>
                        </div>
                        <table>
                            <?php foreach ($dataMoreRecentedVideo as $key => $value) { ?>
                                <tr>
                                    <td>
                                        <a href="/videos?videoId=<?php echo $value->video_id ?>" class="news-image">
                                            <div class="news-image">
                                                <div class="crop">
                                                    <img src="<?php echo((!empty($value->thumbnail)) ? $value->thumbnail : '/images/image-not-found.png'); ?>">
                                                </div>
                                            </div>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/videos?videoId=<?php echo $value->video_id ?>"><?php echo $value->title ?></a>
                                        <div class="times-content">
                                            <span class="timeMoreRecentedGallery"
                                                  date="<?php echo $value->create_datetime; ?>">
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>

    </div>
</div>
<?php //include 'footer.php';
include 'view/index/footer.php';
?>

<script>
    loadMoment();
    function loadMoment() {
        $('.timeMoreRecentedGallery').each(function (index) {
            if ($.isNumeric($(this).attr('date'))) {
                $(this).html(moment($(this).attr('date'), "X").fromNow())
            } else {
                $(this).html(moment($(this).attr('date'), "YYYY-MM-DD HH:mm:ss").fromNow())
            }
        })
    }
</script>

</body>
</html>
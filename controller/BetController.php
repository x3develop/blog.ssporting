<?php

namespace controller;

require_once __DIR__ . "/../bootstrap.php";

use Cache\Adapter\Filesystem\FilesystemCachePool;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use model\BetElo;
use Carbon\Carbon;
use model\MatchCommentElo;
use model\MatchElo;
use model\UserElo;

class BetController
{
    public function getByMatch($mid, $reset = false)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/bet_side");
        $item = $pool->getItem($mid . "");
        if (!$item->isHit() || $reset) {
            $rs = BetElo::where("match_id", $mid)->get();
            $item->set($rs);
            $item->expiresAfter(180);
            $pool->save($item);
        } else {
            $rs = $item->get();
        }
        return $rs;
    }

    public function addBet($mid, $fbid, $coin, $team, $hdp, $reward, $comment = null, $token = null)
    {
        $rs = ['success' => false, 'desc' => "", 'bet' => []];
        if (UserController::isLogin($fbid, $token)) {
            $user = UserElo::where("fb_uid", $fbid)->first();
            $dup = $this->checkBetOnTeam($mid, $user->id, false);
            if (!$dup['bet']) {
                $match = MatchController::getMatch($mid);
                if (!empty($match)) {
                    $mdate = Carbon::parse($match->time_match, "Asia/singapore");
                    $mdate->timezone = "Asia/Bangkok";
                    $ctime = Carbon::now("Asia/Bangkok");
                    if ($ctime->getTimestamp() < $mdate->getTimestamp()) {
                        if (UserController::betAble($coin, $user)) {
                            $bet = new BetElo();
                            $bet->user_id = $user->id;
                            $bet->match_id = $mid;
                            $bet->team = $team;
                            $bet->use_bet_coin = $coin;
                            $bet->use_bet_coupon = 1;
                            $bet->use_bet_gold = 0;
                            $bet->hdp = $hdp;
                            $bet->reward = $reward;
                            $bet->status = "bet";
                            if ($team == "home") {
                                $bet->tid = $match->team_home;
                            } else {
                                $bet->tid = $match->team_away;
                            }
                            if ($bet->save()) {
                                if (!empty($comment)) {
                                    $mcomment = new MatchCommentElo();
                                    $mcomment->mid = $mid;
                                    $mcomment->user_id = $user->id;
                                    $mcomment->side = $bet->team;
                                    $mcomment->comment = $comment;
                                    $mcomment->save();
                                    MatchController::getMatchComment($mid, false);
                                }

                                $user->coin = $user->coin - $coin;
                                $user->coupon = $user->coupon - 1;
                                $user->save();
                                UserController::refreshSessionData($user->fb_uid, $user);

                                $rs['success'] = true;
                                $rs['desc'] = "Bet done.";
                                $rs['bet'] = $bet;
                                $this->checkBetOnTeam($mid, $user->id, true);
                            }
                        } else {
                            $rs['desc'] = "No coin or coupon left!";
                        }
                    } else {
                        $rs['desc'] = "Time's up!";
                    }

                } else {
                    $rs['desc'] = "Match not found.";
                }
            } else {
                $rs['desc'] = "Already bet on this match.";
            }
        } else {
            $rs['desc'] = "Token expired, Please re-login.";
        }

        return $rs;
    }

    public function checkBetOnTeam($mid, $uid, $reset = false)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/bet_on");
        $item = $pool->getItem($uid . "_" . $mid);
        $found = 0;
        $data = ['bet' => false, 'bet_data' => []];
        if (!$item->isHit() || $reset) {
            $rs = BetElo::where("match_id", $mid)->where('user_id', $uid)->first();
            if (!empty($rs)) {
                if ($rs->tid == 0) {
                    $match = MatchController::getMatch($mid);
                    if ($rs->team == "home") {
                        $rs->tid = $match->team_home;
                    } else {
                        $rs->tid = $match->team_away;
                    }
                    $rs->save();
                }
                $data['bet'] = true;
                $data['bet_data'] = $rs;
            }
            $item->set($data);
            $item->expiresAfter(60 * 60 * 24 * 3);
            $pool->save($item);
        } else {
            $data = $item->get();
        }
        return $data;

    }


}

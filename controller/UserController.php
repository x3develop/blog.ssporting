<?php

namespace controller;

require_once __DIR__ . "/../bootstrap.php";

use Cache\Adapter\Filesystem\FilesystemCachePool;
use Carbon\Carbon;
use Facebook\FacebookBatchRequest;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use model\BetElo;
use model\UserElo;

class UserController
{
    public function isLogIn($fbid, $token)
    {
        return FbController::validToken($fbid, $token);
    }


    public function betAble($coin, $user)
    {
        $betable = false;
        if ($user->coin >= $coin && $user->coupon > 0) {
            $betable = true;
        }
        return $betable;
    }

    public function refreshSessionData($fb_uid, $user = null)
    {
        @session_start();
        if ($user == null) {
            $user = UserElo::where("fb_uid", $fb_uid)->first();
        }
        $_SESSION['login']['name'] = $user->name;
        $_SESSION['login']['id'] = $user->id;
        $_SESSION['login']['username'] = $user->username;
        $_SESSION['login']['email'] = $user->email;
        $_SESSION['login']['coin'] = $user->coin;
        $_SESSION['login']['gold'] = $user->gold;
        $_SESSION['login']['path'] = $user->path;
        $_SESSION['login']['coupon'] = $user->coupon;
        $_SESSION['login']['combo_login'] = $user->combo_login;
        $_SESSION['login']['lv'] = $user->lv;
        $_SESSION['login']['star'] = $user->star;
        $_SESSION['login']['coupon'] = $user->coupon;
        $_SESSION['login']['fb_uid'] = $user->fb_uid;

        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/user_fbid");
        $item = $pool->getItem($user->fb_uid . "");

        $rs = ['valid_user' => true, 'user_data' => $user];
        $item->set($rs);
        $item->expiresAfter(60 * 10);
        $pool->save($item);

    }

    public function getByFbid($fbid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/user_fbid");
        $item = $pool->getItem($fbid . "");
        $rs = ['valid_user' => false, 'user_data' => []];
        if (!$item->isHit()) {
            $user = UserElo::where("fb_uid", $fbid)->first();
            if (!empty($user)) {
                $rs['valid_user'] = true;
                $rs['user_data'] = $user;
                $item->set($rs);
                $item->expiresAfter(60 * 10);
                $pool->save($item);
            }
        } else {
            $rs = $item->get();
        }
        return $rs;
    }

    public function updateUser($fbid, $name, $email, $fb_token = null)
    {
        @session_start();
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/user_token");
        $item = $pool->getItem($fbid . "");


        $dt = Carbon::now("Asia/Bangkok");

        $rs = ['success' => false, "desc" => "Token expired, Please re-login Facebook", 'first_login' => true, 'bonus_login_coin' => 0, "user_data" => []];

        $validtoken = FbController::validToken($fbid, $fb_token);
        $rs['success'] = $validtoken;
        if ($validtoken) {
            $rs['desc'] = "Token is valid";
        } else {
            $rs['desc'] = "Token is expired!";
        }

        $user = UserElo::where("fb_uid", $fbid)->first();
        if (empty($user)) {
            $user = new UserElo();
            $user->fb_uid = $fbid;
            $user->name = $name;

            $yt = $dt->copy();
            $yt->subDays(1);
            $user->last_login = $yt->toDateTimeString();
        }
        $lastdate = Carbon::parse($user->last_login, "Asia/Bangkok");

        if ($dt->diffInDays($lastdate) == 0) {
            $rs['first_login'] = false;
        } else if ($dt->diffInDays($lastdate) == 1) {
            $user->combo_login = $user->combo_login + 1;
            $user->coin = $user->coin + 500;
            $user->coupon = 5;
            $rs['bonus_login'] = 500;
        } else {
            $user->combo_login = 0;
            $user->coin = $user->coin + 500;
            $user->coupon = 5;
            $rs['bonus_login'] = 500;
        }
        $user->email = $email;
        $user->last_login = $dt->toDateTimeString();
        $user->save();

        $item->set($fb_token);
        $item->expiresAfter(60 * 60 * 3);
        $pool->save($item);

        $this->refreshSessionData($fbid);
        $rs['user_data'] = $user;

        return $rs;
    }
}

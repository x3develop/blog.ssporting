<?php

namespace controller;

require_once __DIR__ . "/../bootstrap.php";

use Cache\Adapter\Filesystem\FilesystemCachePool;
use Carbon\Carbon;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use model\HdpElo;
use model\HdpHistory;
use model\LineUp;
use model\MatchCommentElo;
use model\MatchElo;
use model\MatchEvent;
use model\MatchStat;
use model\ReviewMatch;
use model\VoteElo;
use model\UserElo;

class MatchController
{
    public function getEvent($mid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/match_event");
        $item = $pool->getItem($mid . "");
        $events = null;
        if (!$item->isHit()) {
            $events = MatchEvent::where("mid", $mid)->orderBy("real_min")->get();
            if (!$events->isEmpty()) {
                $item->set($events);
                $item->expiresAfter(120);
                $pool->save($item);
            }
        } else {
            $events = $item->get();
        }
        return $events;
    }

    public function getHdpHistory($mid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/hdp");
        $item = $pool->getItem($mid . "");
        $history = null;
        if (!$item->isHit()) {
            $history = HdpHistory::where("mid", $mid)->get();
            if (!$history->isEmpty()) {
                $item->set($history);
                $item->expiresAfter(120);
                $pool->save($item);
            }
        } else {
            $history = $item->get();
        }
        return $history;
    }

    public function getLineups($mid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/lineups");
        $item = $pool->getItem($mid . "");
        $lineup = null;
        if (!$item->isHit()) {
            $lineup = LineUp::where("mid", $mid)->orderBy("porder")->get();
            if (!$lineup->isEmpty()) {
                $item->set($lineup);
                $item->expiresAfter(60 * 10);
                $pool->save($item);
            }
        } else {
            $lineup = $item->get();
        }
        return $lineup;
    }

    public function getStat($mid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/statistics");
        $item = $pool->getItem($mid . "");
        $stats = null;
        if (!$item->isHit()) {
            $stats = MatchStat::where("mid", $mid)->orderBy("korder")->get();
            if (!$stats->isEmpty()) {
                $item->set($stats);
                $item->expiresAfter(180);
                $pool->save($item);
            }
        } else {
            $stats = $item->get();
        }
        return $stats;
    }

    public function winner($mid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/winner");
        $item = $pool->getItem($mid . "");
        $rs = null;
        if (!$item->isHit()) {
            $match = MatchElo::find($mid);
            if ($match->state == 4) {
                if ($match->live_score_home > $match->live_score_away) {
                    $rs = "home";
                } else if ($match->live_score_home < $match->live_score_away) {
                    $rs = "away";
                } else {
                    $rs = "draw";
                }
                $item->set($rs);
                $item->expiresAfter(600);
                $pool->save($item);
            }
        } else {
            $rs = $item->get();
        }
        return $rs;

    }

    public function getAllLSort($day)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/match_leaguesort");

        $rh = null;
        $rs = [];
        $data = [];
        $live = [];
        $mul = 2;

        // dd($ckey);
        $start = Carbon::now("UTC");
        $start->setTimezone("Asia/Bangkok");
        $stop = Carbon::now("UTC");
        $stop->setTimezone("Asia/Bangkok");
        $today = Carbon::now("UTC");
        $today->setTimezone("Asia/Bangkok");
        if (empty($day)) {
            $ckey = 'all';
        } else {
            $start = Carbon::createFromFormat("Y-m-d", $day, "Asia/Bangkok");
            $stop = Carbon::createFromFormat("Y-m-d", $day, "Asia/Bangkok");
            // $start->timezone("Asia/Bangkok");
            // $stop->timezone("Asia/Bangkok");
            if ($today->hour < 12) {
                $start->subDays(1);
                $start->hour = 12;
                $start->minute = 0;
                $start->second = 0;
                $stop->hour = 12;
                $stop->minute = 0;
                $stop->second = 0;
            } else {
                $stop->addDays(1);
                $start->hour = 12;
                $start->minute = 0;
                $start->second = 0;
                $stop->hour = 12;
                $stop->minute = 0;
                $stop->second = 0;

                if ($today > $start) {
                    $mul = 60 * 4;
                }
            }

        }
        $ckey = str_replace("-", "", $start->toDateString());

        $item = $pool->getItem($ckey);
        // echo $start->toDateTimeString();
        // echo $stop->toDateTimeString();
        // exit();

        if (!$item->isHit()) {

            $matches = MatchElo::whereBetween("time_match", [$start, $stop])->with('league')->with('teama')->with('teamb')->with('hdp')->with('reviews.owner')->orderBy('time_match')->get();
            $leagueorder = [];
            foreach ($matches as $match) {
                if (!array_key_exists($match->league_id, $rs)) {
                    $rs[$match->league_id] = [];
                }
                if (!array_key_exists('league', $rs[$match->league_id])) {
                    $rs[$match->league_id]['league'] = $match->league;
                }
                if ($match->state == 1 || $match->state == 2 || $match->state == 3) {
                    $live[$match->league_id] = $match->id;
                }
                $leagueorder[$match->league_id] = $match->league->priority;
                $rs[$match->league_id]['matches'][] = $match;
            }
            asort($leagueorder);
            $data['order'] = $leagueorder;
            $data['data'] = $rs;
            $data['live_league'] = $live;
            $item->set($data);
            $item->expiresAfter(60 * 3);
            $pool->save($item);
        } else {
            $data = $item->get();
        }

        return $data;
    }

    public function getAllLSortMobile($day)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/match_leaguesort_mobile");

        $rh = null;
        $rs = [];
        $data = [];
        $live = [];
        $mul = 2;

        // dd($ckey);
        $start = Carbon::now("UTC");
        $start->setTimezone("Asia/Bangkok");
        $stop = Carbon::now("UTC");
        $stop->setTimezone("Asia/Bangkok");
        $today = Carbon::now("UTC");
        $today->setTimezone("Asia/Bangkok");
        if (empty($day)) {
            $ckey = 'all';
        } else {
            $start = Carbon::createFromFormat("Y-m-d", $day, "Asia/Bangkok");
            $stop = Carbon::createFromFormat("Y-m-d", $day, "Asia/Bangkok");
            // $start->timezone("Asia/Bangkok");
            // $stop->timezone("Asia/Bangkok");
            if ($today->hour < 12) {
                $start->subDays(1);
                $start->hour = 12;
                $start->minute = 0;
                $start->second = 0;
                $stop->hour = 12;
                $stop->minute = 0;
                $stop->second = 0;
            } else {
                $stop->addDays(1);
                $start->hour = 12;
                $start->minute = 0;
                $start->second = 0;
                $stop->hour = 12;
                $stop->minute = 0;
                $stop->second = 0;

                if ($today > $start) {
                    $mul = 60 * 4;
                }
            }

        }
        $ckey = str_replace("-", "", $start->toDateString());

        $item = $pool->getItem($ckey);
        // echo $start->toDateTimeString();
        // echo $stop->toDateTimeString();
        // exit();

        if (!$item->isHit()) {
            $matches = MatchElo::whereBetween("time_match", [$start, $stop])->with('league')->with('teama')->with('teamb')->with('hdp')->orderBy('time_match')->get();
            $leagueorder = [];
            foreach ($matches as $match) {
                if (!array_key_exists($match->league_id, $rs)) {
                    $rs[$match->league_id] = [];
                }
                if (!array_key_exists('league', $rs[$match->league_id])) {
                    $rs[$match->league_id]['league'] = $match->league;
                }
                if ($match->state == 1 || $match->state == 2 || $match->state == 3) {
                    $live[$match->league_id] = $match->id;
                }
                $leagueorder[$match->league_id] = $match->league->priority;
                $rs[$match->league_id]['matches'][] = $match;
            }
            asort($leagueorder);
            $data['order'] = $leagueorder;
            $data['data'] = $rs;
            $data['live_league'] = $live;
            $item->set($data);
            $item->expiresAfter(60 * 3);
            $pool->save($item);
        } else {
            $data = $item->get();
        }

        return $data;
    }

    public function getAllLSortMobilePhone($day)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/match_leaguesort_mobilephone");

        $rh = null;
        $rs = [];
        $data = [];
        $live = [];
        $mul = 2;

        // dd($ckey);
        $start = Carbon::now("UTC");
        $start->setTimezone("Asia/Bangkok");
        $stop = Carbon::now("UTC");
        $stop->setTimezone("Asia/Bangkok");
        $today = Carbon::now("UTC");
        $today->setTimezone("Asia/Bangkok");
        if (empty($day)) {
            $ckey = 'all';
        } else {
            $start = Carbon::createFromFormat("Y-m-d", $day, "Asia/Bangkok");
            $stop = Carbon::createFromFormat("Y-m-d", $day, "Asia/Bangkok");
            // $start->timezone("Asia/Bangkok");
            // $stop->timezone("Asia/Bangkok");
            if ($today->hour < 12) {
                $start->subDays(1);
                $start->hour = 12;
                $start->minute = 0;
                $start->second = 0;
                $stop->hour = 12;
                $stop->minute = 0;
                $stop->second = 0;
            } else {
                $stop->addDays(1);
                $start->hour = 12;
                $start->minute = 0;
                $start->second = 0;
                $stop->hour = 12;
                $stop->minute = 0;
                $stop->second = 0;

                if ($today > $start) {
                    $mul = 60 * 4;
                }
            }

        }
        $ckey = str_replace("-", "", $start->toDateString());

        $item = $pool->getItem($ckey);
        // echo $start->toDateTimeString();
        // echo $stop->toDateTimeString();
        // exit();

        if (!$item->isHit()) {
            $matches = MatchElo::whereBetween("time_match", [$start, $stop])->with('league')->with('teama')->with('teamb')->orderBy('time_match')->get();
            $leagueorder = [];
            foreach ($matches as $match) {
                if (!array_key_exists($match->league_id, $rs)) {
                    $rs[$match->league_id] = [];
                }
                if (!array_key_exists('league', $rs[$match->league_id])) {
                    $rs[$match->league_id]['league'] = $match->league;
                }
                if ($match->state == 1 || $match->state == 2 || $match->state == 3) {
                    $live[$match->league_id] = $match->id;
                }
                if ($match->hdp == null) {
                    unset($match->hdp);
                    $match->hdp = (object)[];
                }
                $mdt = Carbon::createFromFormat("Y-m-d H:i:s", $match->time_match, "Asia/Singapore");
                $mdt->setTimezone("Asia/Bangkok");
                $match->time_match = $mdt->toDateTimeString();
                $leagueorder[$match->league_id] = $match->league->priority;
                $rs[$match->league_id]['matches'][] = $match;
            }
            asort($leagueorder);
            $data['order'] = $leagueorder;
            $data['data'] = $rs;
            $data['live_match'] = $live;
            if (empty($data['order'])) {
                $data['order'] = (object)[];

            }
            if (empty($data['data'])) {
                $data['data'] = (object)[];
            }
            if (empty($data['live_match'])) {
                $data['live_match'] = (object)[];
            }

            $item->set($data);
            $item->expiresAfter(60 * 3);
            $pool->save($item);
        } else {
            $data = $item->get();
        }

        return $data;
    }


    function getH2H($aid, $bid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/h2h");
        $item = $pool->getItem($aid . "_" . $bid);
        $rs = ['data' => [], "result_summary" => []];
        $data = [];
        $mresult = ['win' => 0, 'draw' => 0, 'lose' => 0];
        if (!$item->isHit()) {
            $pm = new \playMatch();
            $h2h = $pm->getHeadToHead($aid, $bid);
            foreach ($h2h as $match) {
                $match->odds_result = "-";
                $match->result = "-";
                if ($match->homeScore > $match->awayScore) {
                    if ($match->team_home == $aid) {
                        $match->result = "w";
                        $mresult['win']++;
                    } else {
                        $match->result = "l";
                        $mresult['lose']++;
                    }
                } else if ($match->homeScore < $match->awayScore) {
                    if ($match->team_home == $aid) {
                        $match->result = "l";
                        $mresult['lose']++;
                    } else {
                        $match->result = "w";
                        $mresult['win']++;
                    }
                } else {
                    $match->result = "d";
                    $mresult['draw']++;
                }

                if (!empty($match->hdp)) {
                    if ($match->homeScore + $match->hdp > $match->awayScore) {
                        if ($match->team_home == $aid) {
                            $match->odds_result = "w";
                        } else {
                            $match->odds_result = "l";
                        }
                    } else if ($match->homeScore + $match->hdp < $match->awayScore) {
                        if ($match->team_home == $aid) {
                            $match->odds_result = "l";
                        } else {
                            $match->odds_result = "w";
                        }
                    } else {
                        $match->odds_result = "d";
                    }
                    $match->hdp .= "";
                } else {
                    $match->hdp = "-";
                }

                $data[$match->league_id]['league']['id'] = $match->league_id;
                $data[$match->league_id]['league']['name'] = $match->leagueName;
                $data[$match->league_id]['league']['logo'] = $match->leaguePath;
                $data[$match->league_id]['matches'][] = $match;
            }

            foreach ($data as $league) {
                $rs["data"][] = $league;
            }
            if (array_key_exists("data", $rs)) {
                $rs['result_summary'] = $mresult;
            }
            if (empty($rs)) {
                $rs = (object)[];
            }
//            $rs = $data;
            $item->set($rs);
            $item->expiresAfter(60 * 10);
            $pool->save($item);

        } else {
            $rs = $item->get();
        }
        return $rs;
    }

    function getFormGuild($tid, $limit = 20)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/formguide");
        $item = $pool->getItem($tid . "_" . $limit);
        $rs = ['data' => [], "result_summary" => []];
        $data = [];
        $mresult = ['win' => 0, 'draw' => 0, 'lose' => 0];
        if (!$item->isHit()) {
            $pm = new \playMatch();
            $fgrs = $pm->getFormGuide($tid, $limit);
            if (!empty($rs)) {
                array_multisort(array_column($rs, 'league_id'), SORT_ASC, array_column($rs, 'time_match'), SORT_DESC, $rs);
            }
            foreach ($fgrs as $match) {
                $match->odds_result = "-";
                $match->result = "-";
                if ($match->homeScore > $match->awayScore) {
                    if ($match->team_home == $tid) {
                        $match->result = "w";
                        $mresult['win']++;
                    } else {
                        $match->result = "l";
                        $mresult['lose']++;
                    }
                } else if ($match->homeScore < $match->awayScore) {
                    if ($match->team_home == $tid) {
                        $match->result = "l";
                        $mresult['lose']++;
                    } else {
                        $match->result = "w";
                        $mresult['win']++;
                    }
                } else {
                    $match->result = "d";
                    $mresult['draw']++;
                }
                if (!empty($match->hdp)) {
                    if ($match->homeScore + $match->hdp > $match->awayScore) {
                        if ($match->team_home == $tid) {
                            $match->odds_result = "w";
                        } else {
                            $match->odds_result = "l";
                        }
                    } else if ($match->homeScore + $match->hdp < $match->awayScore) {
                        if ($match->team_home == $tid) {
                            $match->odds_result = "l";
                        } else {
                            $match->odds_result = "w";
                        }
                    } else {
                        $match->odds_result = "d";
                    }
                    $match->hdp .= "";
                } else {
                    $match->hdp = "-";
                }

                $data[$match->league_id]['league']['id'] = $match->league_id;
                $data[$match->league_id]['league']['name'] = $match->leagueName;
                $data[$match->league_id]['league']['logo'] = $match->leaguePath;
                $data[$match->league_id]['matches'][] = $match;
            }


            foreach ($data as $league) {
                $rs["data"][] = $league;
            }
            if (array_key_exists("data", $rs)) {
                $rs['result_summary'] = $mresult;
            }
            if (empty($rs)) {
                $rs = (object)[];
            }
//            $rs = $data;
            $item->set($rs);
            $item->expiresAfter(60 * 10);
            $pool->save($item);

        } else {
            $rs = $item->get();
        }
        return $rs;
    }

    public function getReview($mid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/match_review");
        $item = $pool->getItem($mid . "");
        $rs = null;
        if (!$item->isHit()) {
            $rs = ReviewMatch::where("match_id", "=", $mid)->with("owner")->get();
            $item->set($rs);
            $item->expiresAfter(60 * 10);
            $pool->save($item);

        } else {
            $rs = $item->get();
        }
        return $rs;
    }

    public function getMatch($mid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/match");
        $item = $pool->getItem($mid . "");
        $rs = null;
        if (!$item->isHit()) {
            $rs = MatchElo::where("id", $mid)->with('league')->with('teama')->with('teamb')->with('hdp')->first();
            $item->set($rs);
            $item->expiresAfter(60 * 10);
            $pool->save($item);

        } else {
            $rs = $item->get();
        }
        return $rs;
    }

    public function getUpdate($mid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/match_state");
        $item = $pool->getItem($mid . "");
        $rs = [];
        if ($item->isHit()) {
            $rs = $item->get();
        } else {
            $rs = MatchElo::where("id", $mid)->with("hdp")->first();
            if ($rs->hdp == null) {
                unset($rs->hdp);
                $rs->hdp = (object)[];
            }
            $item->set($rs);
            $item->expiresAfter(60);
            $pool->save($item);
        }
        return $rs;
    }

    public function getLineupFormation($mid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/lineup_formation");
        $item = $pool->getItem($mid . "");
        $rs = [];
        $data = ["home" => ["main" => ["GK" => [], "DF" => [], "MF" => [], "FW" => []], "reserve" => [], "formation" => ["pattern" => "-", "FW" => 0, "MF" => 0, "DF" => 0]], "away" => ["main" => ["GK" => [], "DF" => [], "MF" => [], "FW" => []], "reserve" => [], "formation" => ["pattern" => "-", "FW" => 0, "MF" => 0, "DF" => 0]]];
        if ($item->isHit()) {
            $rs = $item->get();
        } else {
            $match = MatchElo::find($mid);
            $players = LineUp::where("mid", $mid)->get();
            foreach ($players as $player) {
                if ($player->type == "main") {
                    $data[$player->side][$player->type][$player->position][] = $player;
                } else {
                    $data[$player->side][$player->type][] = $player;
                }
            }
            $data["home"]["formation"]["pattern"] = $match->home_formation;
            $data["away"]["formation"]["pattern"] = $match->away_formation;

            $hpattern = explode("-", $match->home_formation);
            $apattern = explode("-", $match->away_formation);

            $data["home"]["formation"]["DF"] = (array_key_exists(0, $hpattern)) ? (int)$hpattern[0] : 0;
            $data["home"]["formation"]["MF"] = (array_key_exists(1, $hpattern)) ? (int)$hpattern[1] : 0;
            $data["home"]["formation"]["FW"] = (array_key_exists(2, $hpattern)) ? (int)$hpattern[2] : 0;

            $data["away"]["formation"]["DF"] = (array_key_exists(0, $apattern)) ? (int)$apattern[0] : 0;
            $data["away"]["formation"]["MF"] = (array_key_exists(1, $apattern)) ? (int)$apattern[1] : 0;
            $data["away"]["formation"]["FW"] = (array_key_exists(2, $apattern)) ? (int)$apattern[2] : 0;


            $rs = $data;
            $item->set($rs);
            $item->expiresAfter(60 * 60 * 10);
            $pool->save($item);
        }

        return $rs;
    }

    public function addVote($mid, $fbid, $side, $token)
    {
        $rs = ["success" => false, "desc" => "Already voted.", "data" => []];
        if (UserController::isLogin($fbid, $token)) {
            $match = MatchController::getMatch($mid);
            if (!empty($match)) {
                $mdate = Carbon::parse($match->time_match, "Asia/singapore");
                $mdate->timezone = "Asia/Bangkok";
                $ctime = Carbon::now("Asia/Bangkok");
                if ($ctime->getTimestamp() < $mdate->getTimestamp()) {
                    $vote = VoteElo::where("mid", $mid)->where("fb_uid", $fbid)->first();
                    if (empty($vote)) {
                        $vote = new VoteElo();
                        $vote->mid = $mid;
                        $vote->fb_uid = $fbid;
                        $vote->side = $side;
                        $vote->save();
                        $vote->save();

                    } else {
                        $vote->side = $side;
                        $vote->save();
                    }
                    $rs["success"] = true;
                    $rs["desc"] = "Vote success";
                    $rs["data"] = $vote;
                } else {
                    $rs["desc"] = "Time's up!";
                }
            }
        } else {
            $rs['desc'] = "Login session has been expired. Please re-login.";
        }
        return $rs;
    }

    public function getVote($mid, $fbid = null)
    {
        $rs = ['home' => 0, 'draw' => 0, 'away' => 0, 'voted' => false, 'vote_side' => ""];
        $votes = VoteElo::where("mid", $mid)->get();
        foreach ($votes as $vote) {
            $rs[$vote->side]++;
            if ($vote->fb_uid == $fbid && !empty($fbid)) {
                $rs['voted'] = true;
                $rs['vote_side'] = $vote->side;
            }
        }
        return $rs;

    }

    public function getMyVote($mid, $fbid)
    {
        $rs = VoteElo::where("mid", $mid)->where("fbid", $fbid)->first();
        if (empty($rs)) {
            $rs = (object)[];
        }
        return $rs;

    }

    public function getMatchComment($mid, $getcache = true)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/match_comment");
        $item = $pool->getItem($mid . "");
        $rs = ['home' => [], 'away' => []];
        if ($item->isHit() && $getcache) {
            $rs = $item->get();
        } else {
            $comments = MatchCommentElo::where("mid", $mid)->orderBy('user_type', 'DESC')->get();
            foreach ($comments as $comment) {
                $comment->owner;
                $rs[$comment->side][] = $comment;
//                $rs['owner'][$comment->user_type][$comment->user_id] = $comment->owner;
            }
            $item->set($rs);
            $item->expiresAfter(60 * 10);
            $pool->save($item);
        }
        return $rs;
    }

    public function getEarlyComment($mid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/match_comment_early");
        $item = $pool->getItem($mid . "");
        $rs = [];
        $row = 1;
        if ($item->isHit()) {
            $rs = $item->get();
        } else {
            $comments = MatchCommentElo::where("mid", $mid)->orderBy('user_type', 'DESC')->limit(5)->get();
            foreach ($comments as $comment) {
                $comment->owner;
                $rs[] = $comment;
//                $rs['owner'][$comment->user_type][$comment->user_id] = $comment->owner;
                $row++;
            }
            if ($row >= 5) {
                $item->set($rs);
                $item->expiresAfter(60 * $row);
                $pool->save($item);
            }
        }
        return $rs;
    }

    public function addComment($mid, $fbid, $comment)
    {
        $rs = ['success' => false, 'desc' => "", 'comment' => []];
            $user = UserElo::where("fb_uid", $fbid)->first();
            $beton = BetController::checkBetOnTeam($mid, $user->id, false);
            if ($beton['bet']) {
                if (!empty($comment)) {
                    $mcomment = new MatchCommentElo();
                    $mcomment->mid = $mid;
                    $mcomment->user_id = $user->id;
                    $mcomment->side = $beton['bet_data']->team;
                    $mcomment->comment = $comment;
                    $mcomment->save();
                    $rs['comment']=$mcomment;
                    $rs['desc']="Done.";
                    $rs['success']=true;
                    MatchController::getMatchComment($mid, false);
                }
            } else {
                $rs['desc'] = "Please bet on this match before comment";
            }


        return $rs;
    }


}

<?php

namespace controller;

require_once __DIR__ . "/../bootstrap.php";

use Cache\Adapter\Filesystem\FilesystemCachePool;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use model\MediaGalleryElo;
use model\NewsElo;

class MediaController
{
    public function getAllSexy($page, $gall_type)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/sexy_" . $gall_type);
        $item = $pool->getItem($page . "");
        $rs = [];
        $gtype = ['all' => ['picture', 'video'], 'video' => ['video'], 'picture' => ['picture']];
        if ($item->isHit()) {
            $rs = $item->get();
        } else {
            if ($page == 0) {
                $pool->clear();
            }
            $galls = MediaGalleryElo::whereIn('gall_type', $gtype[$gall_type])->orderBy('created_at', 'DESC')->offset($page)->limit(20)->get();
            foreach ($galls as $gall) {
                $gall->cover;
                $rs[] = $gall;
            }
            $item->set($rs);
            $item->expiresAfter(60 * 10);
            $pool->save($item);
        }
        return $rs;
    }

    public function getMedia($gid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/sexy_media");
        $item = $pool->getItem($gid . "");
        $rs = [];
        $gall = MediaGalleryElo::where('id', $gid)->first();
        $gall->view = $gall->view + 1;
        $gall->save();
        if ($item->isHit()) {
            $rs = $item->get();
        } else {
            $galls = MediaGalleryElo::where('id', $gid)->first();
            $galls->cover;
            $galls->media;
            $rs = $galls;

            $item->set($rs);
            $item->expiresAfter(60 * 10);
            $pool->save($item);
        }
        return $rs;
    }

}

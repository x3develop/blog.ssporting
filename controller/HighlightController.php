<?php
namespace controller;

require_once __DIR__ . "/../bootstrap.php";
use Cache\Adapter\Filesystem\FilesystemCachePool;
use Carbon\Carbon;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use model\MatchElo;
use model\ReviewHighlight;

class HighlightController
{
    public function updateHighlight($mid, $active)
    {
        $rh = ReviewHighlight::where("mid", $mid)->first();
        if (empty($rh)) {
            $match = MatchElo::find($mid);
            $rh = new ReviewHighlight();
            $rh->mid = $mid;
            $rh->show_date = $match->time_match;
        }
        $rh->active = $active;
        $rh->save();
        $match = MatchElo::find($mid);
        if ($match->status != "fullTime") {
            if ($active == "Y") {
                $match->status = "bet";
            } else {
                $match->status = "create";
            }
            $match->save();
        }
        return $rh;
    }

    public function getHighlight($mid)
    {
        $rh = ReviewHighlight::where("mid", $mid)->first();
        return $rh;
    }

    public function getHighlightAll($day)
    {
        $rh = null;
        if (empty($day)) {
            $rh = ReviewHighlight::all();
        } else {
            $rh = ReviewHighlight::where("active", "Y")->whereDate("show_date", $day)->get();
        }
        return $rh;
    }

    public function getHighlightAllLSort($day)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/highlight_leaguesort");

        $rh = null;
        $rs = [];

        // dd($ckey);
        $start = Carbon::now("Asia/Bangkok");
        $stop = Carbon::now("Asia/Bangkok");
        if (empty($day)) {
            $ckey = 'all';
        } else {
            $start = Carbon::createFromFormat("Y-m-d", $day);
            $stop = Carbon::createFromFormat("Y-m-d", $day);
            $start->timezone("Asia/Bangkok");
            $stop->timezone("Asia/Bangkok");
            if (Carbon::now()->hour < 12) {
                $start->subDays(1);
                $start->hour = 12;
                $start->minute = 0;
                $start->second = 0;
                $stop->hour = 12;
                $stop->minute = 0;
                $stop->second = 0;
            } else {
                $stop->addDays(1);
                $start->hour = 12;
                $start->minute = 0;
                $start->second = 0;
                $stop->hour = 12;
                $stop->minute = 0;
                $stop->second = 0;
            }

        }
        $ckey = str_replace("-", "", $start->toDateString());

        $item = $pool->getItem($ckey);
        if (!$item->isHit()) {
            if (empty($day)) {
                $rh = ReviewHighlight::where("active", "Y")->with('match.teama')->with('match.teamb')->with('match.hdp')->with('match.reviews')->orderBy('show_date')->get();
            } else {
                $rh = ReviewHighlight::where("active", "Y")->whereBetween("show_date", [$start, $stop])->with('match.teama')->with('match.teamb')->with('match.hdp')->with('match.reviews.owner')->orderBy('show_date')->get();
            }
            foreach ($rh as $r) {
                if (!array_key_exists($r->match->league_id, $rs)) {
                    $rs[$r->match->league_id] = [];
                }

                if (!array_key_exists('league', $rs[$r->match->league_id])) {
                    $rs[$r->match->league_id]['league'] = $r->match->league;
                }

                $rs[$r->match->league_id]['matches'][] = $r;
            }
            $item->set($rs);
            $item->expiresAfter(300);
            $pool->save($item);
        } else {
            $rs = $item->get();
        }

        return $rs;
    }

    public function getHighlightList($list)
    {
        $rh = null;
        if (empty($list)) {
            $rh = ReviewHighlight::all();
        } else {
            $rh = ReviewHighlight::where("active", "Y")->whereIn("mid", explode(",", $list))->get();
        }
        return $rh;
    }

    public function getHLUpdate($midlist)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/match_hdp_hl");
        $ckey = str_replace("-", "", Carbon::now()->toDateString());
        $item = $pool->getItem($ckey);
        $rs = [];
        if ($item->isHit()) {
            $rs = $item->get();
        } else {
            $rs = MatchElo::whereIn("id", explode(",", $midlist))->with("hdp")->get();
            $item->set($rs);
            $item->expiresAfter(90);
            $pool->save($item);
        }
        return $rs;
    }
}

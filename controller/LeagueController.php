<?php
namespace controller;

require_once __DIR__ . "/../bootstrap.php";
use Cache\Adapter\Filesystem\FilesystemCachePool;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use model\CountryElo;
use model\LeagueElo;
use model\TeamElo;

class LeagueController
{
    public function updatePriority($lid, $priority)
    {
        $league = LeagueElo::find($lid);
        $league->priority = $priority;
        $league->save();
        return $league;
    }

    public function getAll()
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/league");

        $item = $pool->getItem("all");
        $rs = [];
        if ($item->isHit()) {
            $rs = $item->get();
        } else {
            $rs = LeagueElo::all();
            $item->set($rs);
            $item->expiresAfter(60 * 60 * 5);
            $pool->save($item);
        }
        return $rs;

    }

    public function getCountry()
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/country");

        $item = $pool->getItem("all");
        $rs = [];
        if ($item->isHit()) {
            $rs = $item->get();
        } else {
            $rs = CountryElo::all();
            $item->set($rs);
            $item->expiresAfter(60 * 60 * 24);
            $pool->save($item);
        }
        return $rs;

    }

    public function getByCountry($cid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/countryleague");

        $item = $pool->getItem($cid . "");
        $rs = [];
        if ($item->isHit()) {
            $rs = $item->get();
        } else {
            $rs = LeagueElo::where('country_id', $cid)->get();
            $item->set($rs);
            $item->expiresAfter(60 * 60 * 24);
            $pool->save($item);
        }
        return $rs;

    }

    public function getByLeague($lid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/leagueteam");

        $item = $pool->getItem($lid . "");
        $rs = [];
        if ($item->isHit()) {
            $rs = $item->get();
        } else {
            $rs = TeamElo::where('league_id', $lid)->get();
            $item->set($rs);
            $item->expiresAfter(60 * 60 * 5);
            $pool->save($item);
        }
        return $rs;

    }

    public function getStanding($lid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/league");

        $item = $pool->getItem("standing" . $lid);
        $rs = [];
        if ($item->isHit()) {
            $rs = $item->get();
        } else {
            $pl = new \playLeague();
            $rs = $pl->checkTeamByLeague($lid);
            $item->set($rs);
            $item->expiresAfter(60 * 60 * 5);
            $pool->save($item);
        }
        return $rs;
    }

    public function getStandingGroup($lid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/league");

        $item = $pool->getItem("standing" . $lid);
        $rs = [];
        if ($item->isHit()) {
            $rs = $item->get();
        } else {
            $pl = new \playLeague();
            $rs = $pl->checkTeamByChampionsLeague($lid);
            $item->set($rs);
            $item->expiresAfter(60 * 60 * 5);
            $pool->save($item);
        }
        return $rs;
    }

    public function getFixture($lid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/league");

        $item = $pool->getItem("fixture" . $lid);
        $rs = [];
        if ($item->isHit()) {
            $rs = $item->get();
        } else {
            $pl = new \playLeague();
            $rs = $pl->checkMatchLastByLeague($lid);
            $item->set($rs);
            $item->expiresAfter(60 * 60 * 5);
            $pool->save($item);
        }
        return $rs;
    }

    public function getTopplayer($lid)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/league");

        $item = $pool->getItem("topplayer" . $lid);
        $rs = [];
        if ($item->isHit()) {
            $rs = $item->get();
        } else {
            $pl = new \playLeague();
            $playerlist = $pl->checkGoalScorerByLeague($lid);
            $rs["player"] = $playerlist;
            foreach ($playerlist as $player) {
                $rs["team"] [$player['id']] = $pl->checkPathTeamPlayer2($player['id'],$lid);
            }
            $item->set($rs);
            $item->expiresAfter(60 * 60 * 5);
            $pool->save($item);
        }
        return $rs;
    }

}

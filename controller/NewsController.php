<?php
namespace controller;

require_once __DIR__ . "/../bootstrap.php";
use Cache\Adapter\Filesystem\FilesystemCachePool;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use model\NewsElo;

class NewsController
{
    public function getByCat($cat, $page)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/news/");
        $item = $pool->getItem($cat . $page);
        if (!$item->isHit()) {
            $rs = NewsElo::where("category", "=", $cat)->orderBy('created_at', 'DESC')->offset($page)->limit(20)->get();
            $item->set($rs);
            $item->expiresAfter(600);
            $pool->save($item);
        } else {
            $rs = $item->get();
        }
        return $rs;
    }

    public function get($id)
    {
        $bpath = __DIR__ . '/../buffer';
        $bfile = $bpath . "/news" . $id . ".json";
        if (!is_dir($bpath)) {
            mkdir($bpath, 0777, true);
        }
        $arr = ['view' => 1];
        if (!is_file($bfile)) {
            file_put_contents($bfile, json_encode($arr));
        } else {
            $arr = json_decode(file_get_contents($bfile), true);
        }

        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/news/");
        $item = $pool->getItem($id . "");
        if (!$item->isHit()) {
            $rs = NewsElo::where("newsId", "=", $id)->first();
            $rs->readCount += $arr['view'];
            NewsElo::where("newsId", "=", $id)->update(['readCount' => $rs->readCount]);
            $arr['view'] = 0;
            $item->set($rs);
            $item->expiresAfter(600);
            $pool->save($item);
        } else {
            $arr['view'] += 1;
            $rs = $item->get();
        }
        file_put_contents($bfile, json_encode($arr));
        return $rs;
    }

    public function getRelated($id)
    {
        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache/news/");
        $item = $pool->getItem("related" . $id);
        if (!$item->isHit()) {
            $news = NewsElo::where("newsId", "=", $id)->first();
            $tags = explode(",", $news->news_tag);
            $query = NewsElo::query();
            $query->where("category", "=", $news->category);
            $query->where("newsId", "<", $news->newsId);
            $indx = 0;
            $query->where(function ($q) {
                foreach ($tags as $key => $tag) {
                    $tag = trim($tag);
                    if (!empty($tag)) {
                        if ($indx == 0) {
                            $q->where("news_tag", "LIKE", "%{$tag}%");
                        } else {
                            $q->orWhere("news_tag", "LIKE", "%{$tag}%");
                        }
                        $indx++;
                    }
                }
            });
            $rs = $query->orderBy("newsId", "DESC")->limit(5)->get();
            $item->set($rs);
            $item->expiresAfter(60 * 60 * 24 * 1);
            $pool->save($item);
        } else {
            $rs = $item->get();
        }
        return $rs;
    }
   
}

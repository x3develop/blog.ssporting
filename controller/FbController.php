<?php

namespace controller;

require_once __DIR__ . "/../bootstrap.php";
use Facebook\Facebook;
use Facebook\Exceptions;

class FbController
{
    public function validToken($fb_uid, $token)
    {
        $isconnect = false;
        $fb = new Facebook([
            'app_id' => '2323790581232085',
            'app_secret' => '8954da6e589b7f75f1f45303a7978543',
            'default_graph_version' => 'v3.2',

        ]);

//        $fb = new Facebook([
//            'app_id' => '188797391962899',
//            'app_secret' => '2fda28798228584ff3d3ddc288289819',
//            'default_graph_version' => 'v3.2',
//
//        ]);

        $response = null;

        try {
            $response = $fb->get('/me?fields=id,name', $token);

        } catch (Exceptions\FacebookResponseException $e) {
//            echo 'Graph returned an error: ' . $e->getMessage();
//            exit;
        } catch (Exceptions\FacebookSDKException $e) {
//            echo 'Facebook SDK returned an error: ' . $e->getMessage();
//            exit;
        }catch (Exceptions\FacebookAuthenticationException $e) {
//            echo 'Facebook SDK returned an error: ' . $e->getMessage();
//            exit;
        } finally {
            if (!empty($response)) {
                $user = $response->getGraphUser();
//                dd($user);
                if ($user['id'] == $fb_uid) {
                    $isconnect = true;
                } else {
                    $isconnect = false;
                }
            }
        }

        return $isconnect;
    }
}

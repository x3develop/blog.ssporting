<?php
session_start();
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playBet.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playComment.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playReviewMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playLeague.php";
$playLeague = new playLeague();
$playMatch = new playMatch();
$playComment = new playComment();
$playReviewMatch = new playReviewMatch();
$playBet = new playBet();
$mid = ($_REQUEST["mid"]) ? $_REQUEST['mid'] : 0;


//if(){
//    header("Location: " . $_SERVER["HTTP_REFERER"] . '#Success' . $type);
//    exit;
//}

$match=array();
$match = $playMatch->getFirstMathStatusBet(30);
if(!empty($mid) && file_exists($_SERVER["DOCUMENT_ROOT"]."/match/".$mid.".html") ) {
    header('Location: /match/' . $mid . '.html');
    exit();
}elseif(!empty($mid) && file_exists($_SERVER["DOCUMENT_ROOT"]."/match/games-".$mid.".html") ) {
    header('Location: /match/games-' . $mid . '.html');
    exit();
}elseif (empty($mid)){
    header('Location: /livescore');
    exit();
}elseif (!empty($mid)) {
    $matchView = $playMatch->getFirstMathById($mid)[0];
} elseif (!empty($match[0]->id)) {
    $matchView = $playMatch->getFirstMathById($match[0]->id)[0];
}else{
    header('Location: /');
    exit();
}

$matchResultsHome = $playMatch->getListMatchResults($matchView->team_home, 6);
$matchResultsAway = $playMatch->getListMatchResults($matchView->team_away, 6);
$percentBet = $playBet->percentBet($matchView->id);

//comment
$comment = $playMatch->getListComment($matchView->id);
$commentMini = array();
foreach ($comment as $key => $value) {
    if (!empty($value->countComment)) {
        $commentMini[$value->id] = $playComment->getMiniComment($matchView->id, $value->id);
    }
}

$commentHome = $playMatch->getListCommentByTeam($matchView->id, 'home');
$commentAway = $playMatch->getListCommentByTeam($matchView->id, 'away');
$heartList = $playComment->checkHeartByMatch($matchView->id);
$heartListByUser = $playComment->checkHeartByUser($matchView->id);
$reviewMatch = $playReviewMatch->getReviewMatchByMatchId($matchView->id);
$resultsReviewMatch = array();
foreach ($reviewMatch as $key => $value) {
    $resultsReviewMatch[$value['user_review_id']] = $playReviewMatch->getResultsReviewMatchByUserReview($value['user_review_id'], 14);
}

if (isset($_SESSION['login'])) {
    $matchBet = $playBet->getTeamMatchById($_SESSION['login']['id'], $mid);
}

//HEAD-TO-HEAD
$dataHeadToHead=$playMatch->getHeadToHead($matchView->team_home,$matchView->team_away);
$dataFormGuideHome=$playMatch->getFormGuide($matchView->team_home,50);
$dataFormGuideAway=$playMatch->getFormGuide($matchView->team_away,50);
$dataNextMatchHome=$playMatch->getNextMatch($matchView->team_home,$matchView->id);
$dataNextMatchAway=$playMatch->getNextMatch($matchView->team_away,$matchView->id);
$tableLeague = $playLeague->checkTeamByLeague($matchView->league_id);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ngoal | เกมทายผล</title>
    <link rel="shortcut icon" type="images/x-icon" href="images/favicon.ico.png">
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/livescore.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!--js-->
    <script src="js/jquery/dist/jquery.min.js"></script>
    <script src="js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="nanoscrollbar/nanoscrollbar.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/timeview.js"></script>
    <script src="js/game.js"></script>
    <script src="js/sly/sly.js"></script>
    <?php include 'view/index/googletagmanager.php'; ?>
</head>

<body>
<style>
    h3.text-blue {
        text-decoration: underline !important;
    }
</style>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div style="clear: both;"></div>
<div id="fade-in-index" class="box-fade">
    <div class="wrap-selected-team-play wrap-left-team">
        <p>คุณเลือกเล่นทีม</p>
        <img id="img-TeamHomePath" src="<?= $matchView->teamHomePath ?>">
        <h1 id="h1-TeamHomeEn"><?= $matchView->teamHomeEn ?></h1>
        <form id="bet-away-match">
            <input name="bet[time_match]" class="form-control" type="hidden" value="<?= $matchView->time_match; ?>">
            <input name="bet[user_id]" class="form-control" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
            <input name="bet[match_id]" class="form-control" type="hidden" value="<?= $matchView->id; ?>">
            <input name="bet[team]" type="hidden" value="home">
            <!--                <input name="bet[use_bet_coin]" type="hidden" value="750">-->
            <input name="bet[use_bet_gold]" type="hidden" value="0">
            <input name="bet[use_bet_coupon]" type="hidden" value="1">
            <input name="bet[status]" type="hidden" value="bet">

            <div class="box-commented" style="margin: 20px 0px;">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="box-input-select">
                            <textarea name="bet[comment]" class="form-control"
                                      placeholder="แสดงความคิดเห็น..."></textarea>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>

            <div class="box-button-send">
                <div class="col-sm-2"></div>
                <div class="col-sm-4">
                    <div class="box-input-select">
                        <img src="/images/coin/scoin100.png">
                        <select name="bet[use_bet_coin]">
                            <option value="1200">1200</option>
                            <option value="800">800</option>
                            <option value="400">400</option>
                            <option value="200">200</option>
                        </select>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box-button">
                        <button type="submit" class="btn btn-info btn-lg btn-block">Confirm</button>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
        </form>
        <p>เลือกจำนวน Coin ที่ใช้ในการทายผล</p>
        <div class="block-close-vote text-center">
            <div class="box-button close-tab close-tab-right">
                <button type="button" class="btn btn-default btn-lg btn-block" data-dismiss="modal">
                    ปิด
                </button>
            </div>
        </div>
    </div>
</div>
<div id="fade-in-right-index" class="box-fade">
    <!--        <div class="close-tab pull-left close-tab-left"><i class="fa fa-times" aria-hidden="true"></i></div>-->
    <div class="wrap-selected-team-play wrap-right-team">
        <p>คุณเลือกเล่นทีม</p>
        <img id="img-TeamAwayPath" src="<?= $matchView->teamAwayPath ?>">
        <h1 id="h1-TeamAwayEn"><?= $matchView->teamAwayEn ?></h1>
        <form id="bet-away-match">
            <input name="bet[time_match]" class="form-control" type="hidden" value="<?= $matchView->time_match; ?>">
            <input name="bet[user_id]" class="form-control" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
            <input name="bet[match_id]" class="form-control" type="hidden" value="<?= $matchView->id; ?>">
            <input name="bet[team]" type="hidden" value="away">
            <!--                <input name="bet[use_bet_coin]" type="hidden" value="750">-->
            <input name="bet[use_bet_gold]" type="hidden" value="0">
            <input name="bet[use_bet_coupon]" type="hidden" value="1">
            <input name="bet[status]" type="hidden" value="bet">
            <div class="box-commented" style="margin: 20px 0px;">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="box-input-select">
                            <textarea name="bet[comment]" class="form-control"
                                      placeholder="แสดงความคิดเห็น..."></textarea>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>

            <div class="box-button-send">
                <div class="col-sm-2"></div>
                <div class="col-sm-4">
                    <div class="box-input-select">
                        <img src="/images/coin/scoin100.png">
                        <select name="bet[use_bet_coin]">
                            <option value="1200">1200</option>
                            <option value="800">800</option>
                            <option value="400">400</option>
                            <option value="200">200</option>
                        </select>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box-button">
                        <button type="submit" class="btn btn-info btn-lg btn-block">Confirm</button>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
        </form>
        <p>เลือกจำนวน Coin ที่ใช้ในการทายผล</p>
        <div class="block-close-vote text-center">
            <div class="box-button close-tab close-tab-left">
                <button type="button" class="btn btn-default btn-lg btn-block" data-dismiss="modal">
                    ปิด
                </button>
            </div>
        </div>
    </div>
</div>
<div class="content-livescore-main">
    <div class="container">
        <div class="col-md-12">
            <div class="content-list-livescore wrap-box-livescore">
                <div class="col-md-12" id="liveScoreHead" style="width: 100%;overflow: hidden;height: 104px;">
                    <ul class="" id="ul-liveScoreHead" style="margin: 0px">
                        <?php if (!empty($match)) {
                            foreach ($match as $key => $value) { ?>
                                <li data-key="<?php echo $key; ?>" data-matchId="<?= $value->id ?>"
                                    data-timematch="<?= $value->time_match ?>"
                                    class="match-slide <?= (($value->id == $mid) ? 'active' : '') ?>"
                                    style="float: left;">
                                    <a href="/games.php?mid=<?= $value->id ?>" class="col-md-12" style="100%;">
                                        <div class="thumbnail content-team-slide">
                                            <div class="pull-left">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <img src="<?= $value->teamHomePath ?>">
                                                        </td>
                                                        <td class="<?= ((!empty($value->handicap) && $value->handicap < 0) ? 'active' : '') ?>">
                                                            <div class="name-team-slide-top"><?= $value->teamHomeEn ?></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="<?= $value->teamAwayPath ?>">
                                                        </td>
                                                        <td class="<?= ((!empty($value->handicap) && $value->handicap > 0) ? 'active' : '') ?>">
                                                            <div class="name-team-slide-top"><?= $value->teamAwayEn ?></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="pull-right">
                                                <div class="time-live">
                                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                    <p><?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?></p>
                                                </div>
                                                <div class="HDP-live">HDP</div>
                                                <div class="num-live"><?= $value->handicap ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <?php if (!empty($matchView)) { ?>
            <div class="match-team" id="match-team-<?= $matchView->id ?>">
                <div class="col-md-12">
                    <div class="topic-league">
                        <h3><img src="<?= $matchView->path ?>"> <?= $matchView->name ?></h3>
                        <ul>
                            <li><i class="fa fa-clock-o"
                                   aria-hidden="true"></i> <?= date("H:i", (strtotime($matchView->time_match) - (60 * 60))) ?>
                            </li>
                            <li>|</li>
                            <li><i class="fa fa-calendar"
                                   aria-hidden="true"></i> <?= date("d/m/Y", (strtotime($matchView->time_match) - (60 * 60))) ?>
                            </li>
                        </ul>
                    </div>
                    <div class="content-select-team-vote">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="pull-right">
                                            <div class="name-team-vote pull-left text-right">
                                                <h3 class="<?php echo(($matchView->handicap < 0) ? 'text-blue' : '') ?>"><?= $matchView->teamHomeEn ?></h3>
                                            </div>
                                            <div class="box-vote-team">
                                                <div class="logo-vote-team-home">
                                                    <img src="<?= $matchView->teamHomePath ?>">
                                                </div>
                                                <div class="btn-vote-team-home">
                                                    <?php if ($matchBet !== false && $matchBet == "away") { ?>

                                                    <?php } else { ?>
                                                        <a style="color: #FFFFFF"
                                                           class="<?= (($matchBet !== false) ?: 'a-vote-team-home') ?>">VOTE</a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="pull-left">
                                            <div class="box-vote-team">
                                                <div class="logo-vote-team-away">
                                                    <img src="<?= $matchView->teamAwayPath ?>">
                                                </div>
                                                <div class="btn-vote-team-away">
                                                    <?php if ($matchBet !== false && $matchBet == "home") { ?>
                                                        <!--                                                            <a style="color: #FFFFFF" class="a-vote-team-home">VOTE</a>-->
                                                    <?php } else { ?>
                                                        <a style="color: #FFFFFF"
                                                           class="<?= (($matchBet !== false) ?: 'a-vote-team-away') ?>">VOTE</a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="name-team-vote pull-left">
                                                <h3 class="<?php echo(($matchView->handicap > 0) ? 'text-blue' : '') ?>"><?= $matchView->teamAwayEn ?></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 content-HDP-vote">
                    <ul>
                        <li><?= $matchView->home_water_bill; ?></li>
                        <li class="text-blue">[<?= $matchView->handicap ?>]</li>
                        <li><?= $matchView->away_water_bill; ?></li>
                    </ul>
                </div>
            </div>
        <?php } ?>
    </div>
    <!--    stats-->
    <div class="container content-stats-game" style="padding-top: 10px;margin-top: 10px;">
        <table>
            <tr>
                <td>
                    <ul class="text-right">
                        <?php if (!empty($matchResultsHome)) { ?>
                            <?php foreach (array_reverse($matchResultsHome) as $keyResults => $valueResults) { ?>
                                <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                    <li><img title="<?= $valueResults->time_match ?>"
                                             src="/images/icon-stat/result/1.png"></li>
                                <?php } elseif ($valueResults->team_home == $matchView->team_home && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                    <li><img title="<?= $valueResults->time_match ?>"
                                             src="/images/icon-stat/result/0.png"></li>
                                <?php } elseif ($valueResults->team_away == $matchView->team_home && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                    <li><img title="<?= $valueResults->time_match ?>"
                                             src="/images/icon-stat/result/0.png"></li>
                                <?php } else { ?>
                                    <li><img title="<?= $valueResults->time_match ?>"
                                             src="/images/icon-stat/result/2.png"></li>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </td>
                <td class="text-center">
                    <h3 class="label label-default">State</h3>
                </td>
                <td>
                    <ul class="text-left">
                        <?php if (!empty($matchResultsAway)) { ?>
                            <?php foreach ($matchResultsAway as $keyResults => $valueResults) { ?>
                                <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                    <img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/result/1.png">
                                <?php } elseif ($valueResults->team_home == $matchView->team_away && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                    <img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/result/0.png">
                                <?php } elseif ($valueResults->team_away == $matchView->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                    <img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/result/0.png">
                                <?php } else { ?>
                                    <img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/result/2.png">
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </td>
            </tr>
        </table>
    </div>
    <div class="container content-stats-game" style="padding-top: 10px;margin-top: 10px;">
        <table>
            <tr>
                <td style="width: 40%">
                    <!--                    <ul class="text-right">-->
                    <?php foreach ($userListHome as $keyH => $valueH) {
                        if ($keyH < 8 && $valueH->team == "home") { ?>
                            <!--                            <li style="float: right;">-->
                            <img style="float: right;margin: 3px"
                                 src="https://graph.facebook.com/v2.8/<?php echo $valueH->fb_uid; ?>/picture">
                            <!--                            </li>-->
                        <?php } ?>
                    <?php } ?>
                    <!--                    </ul>-->
                </td>
                <td class="text-center" style="width: 20%">
                    <span style="margin: 0 5px;"><?= intval($percentBet['pAway']) ?>%</span>
                    <h3 class="label label-default" style="">Player</h3>
                    <span style="margin: 0 5px;"><?= intval($percentBet['pHome']) ?>%</span>
                </td>
                <td style="width: 40%">
                    <!--                    <ul class="text-left">-->
                    <?php foreach ($userListHome as $keyH => $valueH) { ?>
                        <?php if ($keyH < 8 && $valueH->team == "away") { ?>
                            <!--                                    <li style="float: left;">-->
                            <img style="float: left;margin: 3px"
                                 src="https://graph.facebook.com/v2.8/<?php echo $valueH->fb_uid; ?>/picture">
                            <!--                                    </li>-->
                        <?php } ?>
                    <?php } ?>
                    <!--                    </ul>-->
                </td>
            </tr>
        </table>
    </div>
</div>
<!--mobile-->
<div class="content-livescore-main-mobile content-livescore-main">
    <div class="container">
        <div class="row">
            <div class="content-list-livescore content-list-livescore-mobile">
                <div class="col-md-12">
                    <ul>
                        <li class="active">
                            <div class="thumbnail content-team-slide">
                                <div class="pull-left">
                                    <table>
                                        <tr>
                                            <td>Chelsea</td>
                                        </tr>
                                        <tr>
                                            <td class="active">manchester</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="pull-right">
                                    <div class="time-live">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <p>21:00</p>
                                    </div>
                                    <div class="HDP-live">HDP</div>
                                    <div class="num-live">-025</div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="thumbnail content-team-slide">
                                <div class="pull-left">
                                    <table>
                                        <tr>
                                            <td>Chelsea</td>
                                        </tr>
                                        <tr>
                                            <td class="active">manchester</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="pull-right">
                                    <div class="time-live">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <p>21:00</p>
                                    </div>
                                    <div class="HDP-live">HDP</div>
                                    <div class="num-live">-025</div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="topic-league">
                    <span class="text-center">
                        <img src="/images/logo-team/fa-cup-2017.png">
                    </span>
                <h3>ENGLAND FA CUP</h3>
                <ul>
                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> 20:00</li>
                    <li>|</li>
                    <li><i class="fa fa-calendar" aria-hidden="true"></i> 21/07/2018</li>
                </ul>
            </div>
            <div class="content-select-team-vote content-select-team-vote-mobile">
                <div class="row">
                    <table>
                        <tr>
                            <td>
                                <ul class="text-center">
                                    <li><img src="/images/logo-team/Chelsea.png"></li>
                                    <li><strong>Chelsea FC</strong></li>
                                    <li>
                                        <button type="button" class="btn btn-primary btn-lg">Vote</button>
                                    </li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-center">
                                    <li><img src="/images/logo-team/manchester.png"></li>
                                    <li><strong>Manchester UTD</strong></li>
                                    <li>
                                        <button type="button" class="btn btn-primary btn-lg">Vote</button>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row content-HDP-vote">
            <ul>
                <li>1.99</li>
                <li class="text-blue">[0.5]</li>
                <li>1.88</li>
            </ul>
        </div>
        <div class="content-stats-game">
            <table>
                <tr>
                    <td>
                        <ul class="text-right">
                            <li><img src="/images/icon-stat/betlist_result/lose.png"></li>
                            <li><img src="/images/icon-stat/betlist_result/lose_half.png"></li>
                            <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                            <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                        </ul>
                    </td>
                    <td class="text-center">
                        <h3 class="label label-default">Status</h3>
                    </td>
                    <td>
                        <ul class="text-left">
                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                            <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                            <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>

</div>
<div class="content-detail-game">
    <div class="container">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" >
                <a href="#box-view" aria-controls="box-view" role="tab" data-toggle="tab">ทรรศนะ</a>
            </li>
            <li role="presentation">
                <a href="#box-comment" aria-controls="box-comment" role="tab" data-toggle="tab">คอมเม้น</a>
            </li>
            <li role="presentation" class="active">
                <a href="#box-h2h" aria-controls="box-comment" role="tab" data-toggle="tab">สถิติคู่บอล</a>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane " id="box-view">
                <div class="row">
                    <div class="col-md-6 content-view-game-border-right">
                        <?php foreach ($reviewMatch as $key => $value) { ?>
                            <?php if ($value['team'] == "home") { ?>
                                <div class="content-view-game">
                                    <div class="col-sm-12">
                                        <span><img style="width: 140px;height: 35px;"
                                                   src="<?= $value['path'] ?>"></span>
                                        <ul>
                                            <?php if (!empty($resultsReviewMatch[$value['user_review_id']])) { ?>
                                                <?php foreach ($resultsReviewMatch[$value['user_review_id']] as $keyr => $valuer) { ?>
                                                    <?php if ($valuer['results'] == "win") { ?>
                                                        <li><img title="<?= $valuer['updated_at'] ?>"
                                                                 src="/images/icon-stat/betlist_result/lose.png"></li>
                                                    <?php } else if ($valuer['results'] == "lose") { ?>
                                                        <li><img title="<?= $valuer['updated_at'] ?>"
                                                                 src="/images/icon-stat/betlist_result/win.png"></li>
                                                    <?php } else if ($valuer['results'] == "equal") { ?>
                                                        <li><img title="<?= $valuer['updated_at'] ?>"
                                                                 src="/images/icon-stat/betlist_result/draw.png"></li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="col-sm-12 content-view-game-detail">
                                        <div class="well well-sm">
                                            <strong>@<?= (($value['team'] == "away") ? $value['awayname'] : $value['homename']) ?></strong>
                                            <span><?= $value['review_text'] ?></span>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-6">
                        <?php foreach ($reviewMatch as $key => $value) { ?>
                            <?php if ($value['team'] == "away") { ?>
                                <div class="content-view-game">
                                    <div class="col-sm-12">
                                        <span><img src="<?= $value['path'] ?>"></span>
                                        <ul>
                                            <?php if (!empty($resultsReviewMatch[$value['user_review_id']])) { ?>
                                                <?php foreach ($resultsReviewMatch[$value['user_review_id']] as $keyr => $valuer) { ?>
                                                    <?php if ($valuer['results'] == "win") { ?>
                                                        <li><img title="<?= $valuer['updated_at'] ?>"
                                                                 src="/images/icon-stat/betlist_result/lose.png"></li>
                                                    <?php } else if ($valuer['results'] == "lose") { ?>
                                                        <li><img title="<?= $valuer['updated_at'] ?>"
                                                                 src="/images/icon-stat/betlist_result/win.png"></li>
                                                    <?php } else if ($valuer['results'] == "equal") { ?>
                                                        <li><img title="<?= $valuer['updated_at'] ?>"
                                                                 src="/images/icon-stat/betlist_result/draw.png"></li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="col-sm-12 content-view-game-detail">
                                        <div class="well well-sm">
                                            <strong>@<?= (($value['team'] == "away") ? $value['awayname'] : $value['homename']) ?></strong>
                                            <span><?= $value['review_text'] ?></span>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane " id="box-comment">
                <div class="bx-comments-games mg-bt15" style="background-color: #fff; box-shadow: none;">
                    <div class="rows">
                        <div class="col-sm-6" style="border-right: solid 1px #ccc;">
                            <div class="box-comments" style="overflow-y: auto;">
                                <table>
                                    <tbody id="box-comments-home-<?= $matchView->id; ?>">
                                    <?php if (isset($_SESSION['login'])) { ?>
                                        <tr>
                                            <td>
                                                <div class="box-img-user">
                                                    <img src="https://graph.facebook.com/v2.8/<?= $_SESSION['login']['fb_uid'] ?>/picture">
                                                </div>
                                            </td>
                                            <td>
                                                <form id="form-comment-home-<?= $matchView->id; ?>"
                                                      class="form-comment-home">
                                                    <input type="hidden" name="comment_match[team]" value="home">
                                                    <input type="hidden" name="comment_match[match_id]"
                                                           value="<?= $matchView->id; ?>">
                                                    <input class="form-control" name="comment_match[comment]"
                                                           placeholder="ตอบกลับความคิดเห็น">
                                                </form>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    <?php foreach ($commentHome as $key => $value) {
                                        if ($value->team == "home") { ?>
                                            <tr id="box-comment-<?= $value->id ?>">
                                                <td>
                                                    <div class="box-img-user"><img
                                                                src="<?= ((!empty($value->fb_uid)) ? "https://graph.facebook.com/v2.8/" . $value->fb_uid . "/picture" : ((!empty($value->path)) ? $value->path : '/images/avatar.png')) ?>">
                                                    </div>
                                                </td>
                                                <td>
                                                    <strong><?= ((!empty($value->name)) ? $value->name : $value->username); ?></strong>
                                                    <?= $value->comment; ?>
                                                    <div class="times-content"><span class="comment-updated_at"
                                                                                     data-updated_at="<?= $value->updated_at ?>"><?= $value->updated_at ?></span>
                                                        <?php if (isset($_SESSION['login'])){ ?>
                                                        <span style="cursor: pointer;">
                                                            <?php if (!empty($heartListByUser[$_SESSION['login']['fb_uid'] . "-" . $value->id])){ ?>
                                                            <i id="commemt-heart-<?= $value->id ?>"
                                                               onclick="removeHeart(<?= $heartListByUser[$_SESSION['login']['fb_uid'] . "-" . $value->id][0] ?>)"
                                                               class="fa fa-heart heart-active"></i>
                                                                <span class="heart-count"
                                                                      id="commemt-comment_match-<?= $value->id ?>"><?= ((!empty($heartList[$value->id])) ? $heartList[$value->id][0] : '') ?>
                                                                    <?php }else{ ?>
                                                                    <i id="commemt-heart-<?= $value->id ?>"
                                                                       onclick="addHeart(<?= $value->id ?>,<?= $matchView->id; ?>)"
                                                                       class="fa fa-heart"></i>
                                                                <span class="heart-count"
                                                                      id="commemt-comment_match-<?= $value->id ?>"><?= ((!empty($heartList[$value->id])) ? $heartList[$value->id][0] : '') ?>
                                                                    <?php } ?>
                                                        </span>
                                                                    <?php } ?>
                                                                    <?php if (!empty($value->countComment)) { ?>
                                                                        <span class="show-bx-reply"><?= $value->countComment . ' ข้อความตอบกลับ' ?></span>
                                                                    <?php } else { ?>
                                                                        <span class="show-bx-reply">ตอบกลับ</span>
                                                                    <?php } ?>
                                                    </div>
                                                    <div class="bx-reply hide">
                                                        <table>
                                                            <tbody id="comment-bx_reply-<?= $value->id ?>">
                                                            <?php if (!empty($commentMini[$value->id])) { ?>
                                                                <?php foreach ($commentMini[$value->id] as $kMini => $vMini) { ?>
                                                                    <tr id="box-comment-<?= $vMini['id'] ?>">
                                                                        <td>
                                                                            <div class="box-img-user">
                                                                                <img src="https://graph.facebook.com/v2.8/<?= $vMini['fb_uid'] ?>/picture">
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <strong id="Minicomment-name"><?= ((!empty($vMini['name'])) ? $vMini['name'] : $vMini['username']); ?></strong>
                                                                            <span id="Minicomment-comment"><?= $vMini['comment']; ?></span>
                                                                            <div class="times-content"><span
                                                                                        class="comment-updated_at"
                                                                                        data-updated_at="<?= $vMini['updated_at'] ?>"><?= $vMini['updated_at'] ?></span>
                                                                                <?php if (isset($_SESSION['login'])){ ?>
                                                                                <span style="cursor: pointer;">
                                                                        <?php if (!empty($heartListByUser[$_SESSION['login']['fb_uid'] . "-" . $vMini['id']])){ ?>
                                                                                    <i id="Minicommemt-heart-<?= $vMini['id'] ?>"
                                                                                       onclick="removeHeart(<?= $heartListByUser[$_SESSION['login']['fb_uid'] . "-" . $vMini['id']][0] ?>)"
                                                                                       class="fa fa-heart heart-active"></i>
                                                                                <span class="heart-count"
                                                                                      id="Minicommemt-comment_match-<?= $vMini['id'] ?>"><?= $vMini['countHeart'] ?>
                                                                                    <?php }else{ ?>
                                                                                    <i id="Minicommemt-heart-<?= $vMini['id'] ?>"
                                                                                       onclick="addHeart(<?= $vMini['id'] ?>,<?= $matchView->id; ?>)"
                                                                                       class="fa fa-heart"></i>
                                                                                <span class="heart-count"
                                                                                      id="Minicommemt-comment_match-<?= $vMini['id'] ?>"><?= $vMini['countHeart'] ?>
                                                                                    <?php } ?>
                                                                            </span>
                                                                                    <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                <?php }
                                                            } ?>
                                                            <tr>
                                                                <td>
                                                                    <div class="box-img-user">
                                                                        <img src="https://graph.facebook.com/v2.8/<?= $_SESSION['login']['fb_uid'] ?>/picture">
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <form class="form-comment form-comment-<?= $value->id ?>">
                                                                        <input type="hidden" name="comment_match[team]"
                                                                               value="home">
                                                                        <input type="hidden"
                                                                               name="comment_match[comment_match_id]"
                                                                               value="<?= $value->id ?>">
                                                                        <input type="hidden"
                                                                               name="comment_match[match_id]"
                                                                               value="<?= $matchView->id; ?>">
                                                                        <input name="comment_match[comment]"
                                                                               class="form-control"
                                                                               placeholder="ตอบกลับความคิดเห็น">
                                                                    </form>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php }
                                    } ?>
                                    <tr id="main-miniComments" class="hide">
                                        <td>
                                            <div class="box-img-user">
                                                <img id="comment-profile" src="">
                                            </div>
                                        </td>
                                        <td>
                                            <strong id="comment-name"></strong>
                                            <span id="comment-comment"></span>
                                            <div class="times-content">
                                                <span class="comment-updated_at"></span>
                                                <span>
                                                            <i id="commemt-heart-" class="fa fa-heart"></i>
                                                            <span id="commemt-comment_match-"
                                                                  class="heart-count"></span>
                                                        </span>
                                                <!--                                                        <span>ตอบกลับ</span>-->
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id="main-Comments" class="hide">
                                        <td>
                                            <div class="box-img-user"><img id="comment-profile" src=""></div>
                                        </td>
                                        <td>
                                            <strong id="comment-name"></strong>
                                            <span id="comment-comment"></span>
                                            <div class="times-content">
                                                <span class="comment-updated_at"></span>
                                                <span>
                                                            <i class="fa fa-heart"></i>
                                                            <span class="heart-count"></span>
                                                        </span>
                                                <span class="show-bx-reply">ตอบกลับ</span>
                                            </div>
                                            <div class="bx-reply hide">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <div class="box-img-user">
                                                                <img src="https://graph.facebook.com/v2.8/<?= $_SESSION['login']['fb_uid'] ?>/picture">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <form id="form-comment">
                                                                <input type="hidden" name="comment_match[team]"
                                                                       value="home">
                                                                <input type="hidden"
                                                                       name="comment_match[comment_match_id]">
                                                                <input type="hidden" name="comment_match[match_id]">
                                                                <input class="form-control"
                                                                       placeholder="ตอบกลับความคิดเห็น">
                                                            </form>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="box-comments" style="overflow-y: auto;">
                                <table>
                                    <tbody id="box-comments-away-<?= $matchView->id; ?>">
                                    <?php if (isset($_SESSION['login'])) { ?>
                                        <tr>
                                            <td>
                                                <div class="box-img-user">
                                                    <img src="https://graph.facebook.com/v2.8/<?= $_SESSION['login']['fb_uid'] ?>/picture">
                                                </div>
                                            </td>
                                            <td>
                                                <form id="form-comment-away-<?= $matchView->id; ?>"
                                                      class="form-comment-away">
                                                    <input type="hidden" name="comment_match[team]" value="away">
                                                    <input type="hidden" name="comment_match[match_id]"
                                                           value="<?= $matchView->id; ?>">
                                                    <input class="form-control" name="comment_match[comment]"
                                                           placeholder="ตอบกลับความคิดเห็น">
                                                </form>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    <?php foreach ($commentAway as $key => $value) {
                                        if ($value->team == "away") { ?>
                                            <tr id="box-comment-<?= $value->id ?>">
                                                <td>
                                                    <div class="box-img-user"><img
                                                                src="<?= ((!empty($value->fb_uid)) ? "https://graph.facebook.com/v2.8/" . $value->fb_uid . "/picture" : ((!empty($value->path)) ? $value->path : '/images/avatar.png')) ?>">
                                                    </div>
                                                </td>
                                                <td>
                                                    <strong><?= ((!empty($value->name)) ? $value->name : $value->username); ?></strong>
                                                    <?= $value->comment; ?>
                                                    <div class="times-content"><span class="comment-updated_at"
                                                                                     data-updated_at="<?= $value->updated_at ?>"><?= $value->updated_at ?></span>
                                                        <?php if (isset($_SESSION['login'])){ ?>
                                                        <span style="cursor: pointer;">
                                                            <?php if (!empty($heartListByUser[$_SESSION['login']['fb_uid'] . "-" . $value->id])){ ?>
                                                            <i id="commemt-heart-<?= $value->id ?>"
                                                               onclick="removeHeart(<?= $heartListByUser[$_SESSION['login']['fb_uid'] . "-" . $value->id][0] ?>)"
                                                               class="fa fa-heart heart-active"></i>
                                                                <span class="heart-count"
                                                                      id="commemt-comment_match-<?= $value->id ?>"><?= ((!empty($heartList[$value->id])) ? $heartList[$value->id][0] : '') ?>
                                                                    <?php }else{ ?>
                                                                    <i id="commemt-heart-<?= $value->id ?>"
                                                                       onclick="addHeart(<?= $value->id ?>,<?= $matchView->id; ?>)"
                                                                       class="fa fa-heart"></i>
                                                                <span class="heart-count"
                                                                      id="commemt-comment_match-<?= $value->id ?>"><?= ((!empty($heartList[$value->id])) ? $heartList[$value->id][0] : '') ?>
                                                                    <?php } ?>
                                                            </span>
                                                                    <?php } ?>
                                                                    <?php if (!empty($value->countComment)) { ?>
                                                                        <span class="show-bx-reply"><?= $value->countComment . ' ข้อความตอบกลับ' ?></span>
                                                                    <?php } else { ?>
                                                                        <span class="show-bx-reply">ตอบกลับ</span>
                                                                    <?php } ?>
                                                    </div>
                                                    <div class="bx-reply hide">
                                                        <table>
                                                            <tbody id="comment-bx_reply-<?= $value->id ?>">
                                                            <?php if (!empty($commentMini[$value->id])) { ?>
                                                                <?php foreach ($commentMini[$value->id] as $kMini => $vMini) { ?>
                                                                    <tr id="box-comment-<?= $vMini['id'] ?>">
                                                                        <td>
                                                                            <div class="box-img-user">
                                                                                <img src="https://graph.facebook.com/v2.8/<?= $vMini['fb_uid'] ?>/picture">
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <strong id="Minicomment-name"><?= ((!empty($vMini['name'])) ? $vMini['name'] : $vMini['username']); ?></strong>
                                                                            <span id="Minicomment-comment"><?= $vMini['comment']; ?></span>
                                                                            <div class="times-content"><span
                                                                                        class="comment-updated_at"
                                                                                        data-updated_at="<?= $vMini['updated_at'] ?>"><?= $vMini['updated_at'] ?></span>
                                                                                <?php if (isset($_SESSION['login'])){ ?>
                                                                                <span style="cursor: pointer;">
                                                                                <?php if (!empty($heartListByUser[$_SESSION['login']['fb_uid'] . "-" . $vMini['id']])){ ?>
                                                                                    <i id="Minicommemt-heart-<?= $vMini['id'] ?>"
                                                                                       onclick="removeHeart(<?= $heartListByUser[$_SESSION['login']['fb_uid'] . "-" . $vMini['id']][0] ?>)"
                                                                                       class="fa fa-heart heart-active"></i>
                                                                                <span class="heart-count"
                                                                                      id="Minicommemt-comment_match-<?= $vMini['id'] ?>"><?= $vMini['countHeart'] ?>
                                                                                    <?php }else{ ?>
                                                                                    <i id="Minicommemt-heart-<?= $vMini['id'] ?>"
                                                                                       onclick="addHeart(<?= $vMini['id'] ?>,<?= $matchView->id; ?>)"
                                                                                       class="fa fa-heart"></i>
                                                                                <span class="heart-count"
                                                                                      id="Minicommemt-comment_match-<?= $vMini['id'] ?>"><?= $vMini['countHeart'] ?>
                                                                                    <?php } ?>
                                                                            </span>
                                                                                    <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                <?php }
                                                            } ?>
                                                            <tr>
                                                                <td>
                                                                    <div class="box-img-user">
                                                                        <img src="https://graph.facebook.com/v2.8/<?= $_SESSION['login']['fb_uid'] ?>/picture">
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <form class="form-comment form-comment-<?= $value->id ?>">
                                                                        <input type="hidden" name="comment_match[team]"
                                                                               value="away">
                                                                        <input type="hidden"
                                                                               name="comment_match[comment_match_id]"
                                                                               value="<?= $value->id ?>">
                                                                        <input type="hidden"
                                                                               name="comment_match[match_id]"
                                                                               value="<?= $matchView->id; ?>">
                                                                        <input name="comment_match[comment]"
                                                                               class="form-control"
                                                                               placeholder="ตอบกลับความคิดเห็น">
                                                                    </form>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php }
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane active" id="box-h2h">
                <?php if(!empty($dataHeadToHead)){?>
                <div class="wrap mg-bt15">
                    <div class="tab-head text-center">
                        <ul>
                            <li onclick="changeTeam(this)" class="h2h-select active" tab="team1" tid="<?=$matchView->team_home;?>">
                                <img src="<?=$matchView->teamHomePath;?>"
                                     style="width: 25px;"> <?=$matchView->teamHomeEn;?></li>
                            <li onclick="changeTeam(this)" class="h2h-select " tab="team2" tid="<?=$matchView->team_away;?>">
                                <img src="<?=$matchView->teamAwayPath;?>"
                                     style="width: 25px;"> <?=$matchView->teamAwayEn;?></li>
                        </ul>
                    </div>
                    <div class="h2h-tabs-team1 h2h-tabs " tab="team1">
                        <div id="h2h-section" class="tab-head-title text-center mg-bt15">
                            <strong>HEAD-TO-HEAD</strong>
                        </div>
                        <div class="tab-filter">
                            <div class="pull-left h-league">
                                <img src="<?=$matchView->teamHomePath?>"> <?=$matchView->teamHomeEn;?>
                            </div>
                            <div class="pull-right">
                                <div class="dropdown" style="float:left; margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                    <a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        Last Match (<label id="h2h-h-max-match"><?php echo count($dataHeadToHead)?></label>)
                                        <span class="caret"></span>
                                    </a>
                                    <ul id="h2h-h-select-limit" class="dropdown-menu" aria-labelledby="dLabel">
                                        <?php
                                        if(count($dataHeadToHead)>=20){
                                            for ($i=1;$i<=20;$i++){ ?>
                                                <li><a onclick="viewHeadToHeadHome(<?=(($i))?>)">Last Match(<?=(($i))?>)</a></li>
                                            <?php }}else{
                                            for ($i=1;$i<=(count($dataHeadToHead)-1);$i++){ ?>
                                            <li><a onclick="viewHeadToHeadHome(<?=(($i))?>)">Last Match(<?=(($i))?>)</a></li>
                                        <?php }} ?>
                                        <li><a onclick="viewHeadToHeadHome(<?=count($dataHeadToHead)?>)">Last Match(<?=count($dataHeadToHead)?>)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="bx-h2h">
                            <div class="col-sm-6">
                                <div class="pull-right text-right">
                                    <div id="h2hdonut-h-oresult" style="height: 180px;"></div>
                                </div>
                                <div class="pull-right">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-winrate" ><span id="oddPWin">0</span>%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-loserate"><span id="oddPLose">0</span>%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-drawrate"><span id="oddPDraw">0</span>%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <h3>Odds</h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="pull-left text-left">
                                    <div id="h2hdonut-h-result" style="height: 180px;"></div>
                                </div>
                                <div class="pull-left">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-result-winrate"><span id="resultPWin">0</span>%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-result-loserate"><span id="resultPLose">0</span>%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                                <td>
                                                    <div class="bx-percent h2hh-result-drawrate"><span id="resultPDraw">0</span>%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <h3>Result</h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="mg-bt15">
                            <table class="table-h2h table-striped">
                                <thead>
                                <tr>
                                    <th>League</th>
                                    <th>Date</th>
                                    <th colspan="3">
                                        <div class="tab-filter text-center">
                                            <div class="dropdown"
                                                 style="display:inline-block;margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                                <a data-target="#" href="http://example.com" data-toggle="dropdown"
                                                   role="button" aria-haspopup="true" aria-expanded="true">
                                                    <span class="h2h-currentside-view"><span id="status-view-h2h">Total</span> Match</span>
                                                    <span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li><a class="h2h-viewside" onclick="viewMatchHome('Total')" type="GameHistory">Total</a></li>
                                                    <li><a class="h2h-viewside" onclick="viewMatchHome('Home')" type="GameHistory">Home</a></li>
                                                    <li><a class="h2h-viewside" onclick="viewMatchHome('Away')" type="GameHistory">Away</a></li>
                                                </ul>
                                            </div>
                                            <div style="clear:both;"></div>
                                        </div>
                                    </th>
                                    <th>HDP</th>
                                    <th>Odds</th>
                                    <th>Result</th>
                                </tr>
                                </thead>
                                <tbody id="teamhome-h2h-list">
                                <?php foreach ($dataHeadToHead as $key=>$value){ ?>
                                    <tr id="guide-row-original" class="guide-row tr-h2h tr-h2h-<?=(($matchView->teamHomeEn==$value->teamHomeEn)?'home':'away')?> tr-h2h-<?=floor(($key)/10)?> <?=((floor(($key)/10)==0)?'':'hide')?>">
                                        <td class="guide-league">
                                            <img style="width:25px;" src="<?=$value->leaguePath?>">
                                            <?=$value->leagueName?>
                                        </td>
                                        <td class="guide-date"><?=date("d F Y",strtotime($value->time_match))?></td>
                                        <td class="text-right"><span class="guide-home-name"><?=$value->teamHomeEn?></span>
                                            <img class="guide-home-img" onerror="imgError(this)" src="<?=$value->teamHomePath?>">
                                        </td>
                                        <td class="text-center">
                                            <div class="bx-label-hdp guide-score bg-hdp-blue">
                                                <?=(($value->homeScore==null)?0:$value->homeScore)?> - <?=(($value->awayScore==null)?0:$value->awayScore)?>
                                            </div>
                                        </td>
                                        <td class="text-left">
                                            <img class="guide-away-img" onerror="imgError(this)" src="<?=$value->teamAwayPath?>">
                                            <span class="guide-away-name"><?=$value->teamAwayEn?></span></td>
                                        <td class="guide-odds"><?=(($value->hdp==null)?'-':$value->hdp)?></td>
                                        <td class="tr-odd">
                                            <?php if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and ((($matchView->teamAwayEn==$value->teamHomeEn and ($value->homeScore+$value->hdp)<$value->awayScore)) or ($matchView->teamHomeEn==$value->teamHomeEn and ($value->homeScore+$value->hdp)>$value->awayScore))){ ?>
                                                <img class="guide-result img-win" src="/images/icon-stat/result/0.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and ((($matchView->teamAwayEn==$value->teamHomeEn and ($value->homeScore+$value->hdp)>$value->awayScore)) or ($matchView->teamHomeEn==$value->teamHomeEn and ($value->homeScore+$value->hdp)<$value->awayScore))){?>
                                                <img class="guide-result img-lose" src="/images/icon-stat/result/2.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($value->homeScore+$value->hdp)==$value->awayScore)){?>
                                                <img class="guide-result img-draw" src="/images/icon-stat/result/1.png">
                                            <?php }else{ ?>
                                                -
                                            <?php } ?>
                                        </td>
                                        <td class="tr-result">
                                            <?php if(($value->homeScore!==null and $value->awayScore!==null) and (($matchView->teamAwayEn==$value->teamHomeEn and $value->awayScore>$value->homeScore) or ($matchView->teamHomeEn==$value->teamHomeEn and $value->homeScore>$value->awayScore))){ ?>
                                                <img class="guide-result img-win" src="/images/icon-stat/result/0.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null) and (($matchView->teamAwayEn==$value->teamHomeEn and $value->awayScore<$value->homeScore) or ($matchView->teamHomeEn==$value->teamHomeEn and $value->homeScore<$value->awayScore))){?>
                                                <img class="guide-result img-lose" src="/images/icon-stat/result/2.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null) and $value->homeScore==$value->awayScore){?>
                                                <img class="guide-result img-draw" src="/images/icon-stat/result/1.png">
                                            <?php }else{ ?>
                                                -
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="h2h-tabs-team2 h2h-tabs hide" tab="team2">
                        <div class="tab-head-title text-center mg-bt15">
                            <strong>HEAD-TO-HEAD</strong>
                        </div>
                        <div class="tab-filter">
                            <div class="pull-left h-league">
                                <img src="<?=$matchView->teamAwayPath?>"> <?=$matchView->teamAwayEn;?>
                            </div>
                            <div class="pull-right">
                                <div class="dropdown"
                                     style="float:left; margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                    <a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        Last Match <label id="h2h-g-max-match">(<label id="h2h-h-max-match"><?php echo count($dataHeadToHead)?></label>)</label>
                                        <span class="caret"></span>
                                    </a>
                                    <ul id="h2h-g-select-limit" class="dropdown-menu" aria-labelledby="dLabel">
                                        <?php
                                        if(count($dataHeadToHead)>=20){
                                            for ($i=1;$i<=20;$i++){ ?>
                                                <li><a onclick="viewHeadToHeadAway(<?=(($i))?>)">Last Match(<?=(($i))?>)</a></li>
                                            <?php }}else{
                                            for ($i=1;$i<=(count($dataHeadToHead)-1);$i++){ ?>
                                                <li><a onclick="viewHeadToHeadAway(<?=(($i))?>)">Last Match(<?=(($i))?>)</a></li>
                                            <?php }} ?>
                                        <li><a onclick="viewHeadToHeadAway(<?=count($dataHeadToHead)?>)">Last Match(<?=count($dataHeadToHead)?>)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="bx-h2h mg-bt15">
                            <div class="col-sm-6">
                                <div class="pull-right text-right">
                                    <div id="h2hdonut-a-oresult"></div>
                                </div>
                                <div class="pull-right">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-winrate" ><span id="oddPWin">0</span>%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-loserate"><span id="oddPLose">0</span>%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-drawrate"><span id="oddPDraw">0</span>%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <h3>Odds</h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="pull-left">
                                    <div id="h2hdonut-a-result"></div>
                                </div>
                                <div class="pull-left">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-result-winrate"><span id="resultPWin">0</span>%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-result-loserate"><span id="resultPLose">0</span>%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                                <td>
                                                    <div class="bx-percent h2hh-result-drawrate"><span id="resultPDraw">0</span>%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <h3>Result</h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="mg-bt15">
                            <table class="table-h2h table-striped">
                                <thead>
                                <tr>
                                    <th>League</th>
                                    <th>Date</th>
                                    <th colspan="3">
                                        <div class="tab-filter text-center">
                                            <div class="dropdown"
                                                 style="display:inline-block;margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                                <a data-target="#" href="http://example.com" data-toggle="dropdown"
                                                   role="button" aria-haspopup="true" aria-expanded="fasle">
                                                    <span class="h2h-currentside-view"><span id="status-view-h2h">Total</span> Match</span>
                                                    <span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li><a class="h2h-viewside" onclick="viewMatchAway('Total')" side="all" type="GameHistory">Total</a></li>
                                                    <li><a class="h2h-viewside" onclick="viewMatchAway('Home')" side="home" type="GameHistory">Home</a></li>
                                                    <li><a class="h2h-viewside" onclick="viewMatchAway('Away')" side="away" type="GameHistory">Away</a></li>
                                                </ul>
                                            </div>
                                            <div style="clear:both;"></div>
                                        </div>
                                    </th>
                                    <th>HDP</th>
                                    <th>Odds</th>
                                    <th>Result</th>
                                </tr>
                                </thead>
                                <tbody id="teamaway-h2h-list">
                                <?php foreach ($dataHeadToHead as $key=>$value){ ?>
                                    <tr id="guide-row-original" class="guide-row tr-h2h tr-h2h-<?=(($matchView->teamAwayEn==$value->teamAwayEn)?'home':'away')?> tr-h2h-<?=floor(($key)/10)?> <?=((floor(($key)/10)==0)?'':'hide')?>">
                                        <td class="guide-league">
                                            <img style="width:25px;" src="<?=$value->leaguePath?>">
                                            <?=$value->leagueName?>
                                        </td>
                                        <td class="guide-date"><?=date("d F Y",strtotime($value->time_match))?></td>
                                        <td class="text-right"><span class="guide-home-name"><?=$value->teamHomeEn?></span>
                                            <img class="guide-home-img" onerror="imgError(this)" src="<?=$value->teamHomePath?>">
                                        </td>
                                        <td class="text-center">
                                            <div class="bx-label-hdp guide-score bg-hdp-blue">
                                                <?=(($value->homeScore==null)?0:$value->homeScore)?> - <?=(($value->awayScore==null)?0:$value->awayScore)?>
                                            </div>
                                        </td>
                                        <td class="text-left">
                                            <img class="guide-away-img" onerror="imgError(this)" src="<?=$value->teamAwayPath?>">
                                            <span class="guide-away-name"><?=$value->teamAwayEn?></span></td>
                                        <td class="guide-odds"><?=(($value->hdp==null)?'-':$value->hdp)?></td>
                                        <td class="tr-odd">
                                            <?php if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($matchView->teamAwayEn==$value->teamHomeEn and ($value->homeScore+($value->hdp))>$value->awayScore) or ($matchView->teamAwayEn==$value->teamAwayEn and ($value->homeScore+($value->hdp))<$value->awayScore))){ ?>
                                                <img class="guide-result img-win" src="/images/icon-stat/result/0.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($matchView->teamAwayEn==$value->teamHomeEn and ($value->homeScore+($value->hdp))<$value->awayScore) or ($matchView->teamAwayEn==$value->teamAwayEn and ($value->homeScore+($value->hdp))>$value->awayScore))){?>
                                                <img class="guide-result img-lose" src="/images/icon-stat/result/2.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($value->homeScore+($value->hdp))==$value->awayScore)){?>
                                                <img class="guide-result img-draw" src="/images/icon-stat/result/1.png">
                                            <?php }else{ ?>
                                                -
                                            <?php } ?>
                                        </td>
                                        <td class="tr-result">
                                            <?php if(($value->homeScore!==null and $value->awayScore!==null) and (($matchView->teamAwayEn==$value->teamHomeEn and $value->homeScore>$value->awayScore) or ($matchView->teamAwayEn==$value->teamAwayEn and $value->awayScore>$value->homeScore))){ ?>
                                                <img class="guide-result img-win" src="/images/icon-stat/result/0.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null) and (($matchView->teamAwayEn==$value->teamHomeEn and $value->homeScore<$value->awayScore) or ($matchView->teamAwayEn==$value->teamAwayEn and $value->awayScore<$value->homeScore))){?>
                                                <img class="guide-result img-lose" src="/images/icon-stat/result/2.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null) and $value->awayScore==$value->homeScore){?>
                                                <img class="guide-result img-draw" src="/images/icon-stat/result/1.png">
                                            <?php }else{ ?>
                                                -
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
                <?php } ?>
                <div class="wrap mg-bt15">
                    <div class="tab-head text-center">
                        <ul>
                            <li onclick="formguideChangeTeam(this)" class="guide-select active" tab="team1" tid="<?=$matchView->team_home;?>">
                                <img src="<?=$matchView->teamHomePath;?>"
                                     style="width: 25px;"> <?=$matchView->teamHomeEn;?></li>
                            <li onclick="formguideChangeTeam(this)" class="guide-select " tab="team2" tid="<?=$matchView->team_away;?>">
                                <img src="<?=$matchView->teamAwayPath;?>"
                                     style="width: 25px;"> <?=$matchView->teamAwayEn;?></li>
                        </ul>
                    </div>
                    <div class="guide-tabs-team1 guide-tabs " tab="team1">
                        <div class="tab-head-title text-center mg-bt15">
                            <strong>Form Guide</strong>
                        </div>
                        <div class="tab-filter">
                            <div class="pull-left h-league">
                                <img src="<?=$matchView->teamHomePath?>"> <?=$matchView->teamHomeEn;?>
                            </div>
                            <div class="pull-right">
                                <div class="dropdown"
                                     style="float:left; margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                    <a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        Last Match <label id="gt-h-max-match">(<label id="formguide-h-max-match"><?php echo ((count($dataFormGuideHome)>=20)?'20':count($dataFormGuideHome))?></label>)</label>
                                        <span class="caret"></span>
                                    </a>
                                    <ul id="tg-h-select-limit" class="dropdown-menu" aria-labelledby="dLabel">
                                        <?php
                                        if(count($dataFormGuideHome)>=20){
                                            for ($i=1;$i<=20;$i++){ ?>
                                                <li><a onclick="viewFormGuideHome(<?=(($i))?>)">Last Match(<?=(($i))?>)</a></li>
                                            <?php }} ?>
                                        <li><a onclick="viewFormGuideHome(<?=count($dataFormGuideHome)?>)">Last Match(<?=count($dataFormGuideHome)?>)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="bx-h2h">
                            <div class="col-sm-6">
                                <div class="pull-right text-right">
                                    <div id="formguidedonut-gh-oresult" style="height: 180px;"></div>
                                </div>
                                <div class="pull-right">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-winrate" ><span id="oddPWin">0</span>%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-loserate"><span id="oddPLose">0</span>%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-drawrate"><span id="oddPDraw">0</span>%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <h3>Odds</h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="pull-left text-left">
                                    <div id="formguidedonut-gh-result" style="height: 180px;"></div>
                                </div>
                                <div class="pull-left">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-result-winrate"><span id="resultPWin">0</span>%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-result-loserate"><span id="resultPLose">0</span>%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                                <td>
                                                    <div class="bx-percent h2hh-result-drawrate"><span id="resultPDraw">0</span>%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <h3>Result</h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="mg-bt15">
                            <table class="table-h2h table-striped">
                                <thead>
                                <tr>
                                    <th>League</th>
                                    <th>Date</th>
                                    <th colspan="3">
                                        <div class="tab-filter text-center">
                                            <div class="dropdown"
                                                 style="display:inline-block;margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                                <a data-target="#" href="http://example.com" data-toggle="dropdown"
                                                   role="button" aria-haspopup="true" aria-expanded="true">
                                                    <span class="tg-currentside-view"><span id="status-view-formguide">Total</span> Match</span>
                                                    <span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li><a class="tg-viewside" onclick="viewMatchFormguideHome('Total')" side="all" type="GameHistory">Total Math</a></li>
                                                    <li><a class="tg-viewside" onclick="viewMatchFormguideHome('Home')" side="home" type="GameHistory">Home</a></li>
                                                    <li><a class="tg-viewside" onclick="viewMatchFormguideHome('Away')" side="away" type="GameHistory">Away</a></li>
                                                </ul>
                                            </div>
                                            <div style="clear:both;"></div>
                                        </div>
                                    </th>
                                    <th>HDP</th>
                                    <th>Odds</th>
                                    <th>Result</th>
                                </tr>
                                </thead>
                                <tbody id="teamhome-guide-list">
                                <?php foreach ($dataFormGuideHome as $key=>$value){ ?>
                                    <tr id="guide-row-original" class="guide-row tr-formguide tr-formguide-<?=(($matchView->teamHomeEn==$value->teamHomeEn)?'home':'away')?> tr-formguide-<?=floor(($key)/10)?> <?=((floor(($key)/10)==0)?'':'hide')?>">
                                        <td class="guide-league">
                                            <img style="width:25px;" src="<?=$value->leaguePath?>">
                                            <?=$value->leagueName?>
                                        </td>
                                        <td class="guide-date"><?=date("d F Y",strtotime($value->time_match))?></td>
                                        <td class="text-right"><span class="guide-home-name"><?=$value->teamHomeEn?></span>
                                            <img class="guide-home-img" onerror="imgError(this)" src="<?=$value->teamHomePath?>">
                                        </td>
                                        <td class="text-center">
                                            <div class="bx-label-hdp guide-score bg-hdp-blue">
                                                <?=(($value->homeScore==null)?0:$value->homeScore)?> - <?=(($value->awayScore==null)?0:$value->awayScore)?>
                                            </div>
                                        </td>
                                        <td class="text-left">
                                            <img class="guide-away-img" onerror="imgError(this)" src="<?=$value->teamAwayPath?>">
                                            <span class="guide-away-name"><?=$value->teamAwayEn?></span></td>
                                        <td class="guide-odds"><?=(($value->hdp==null)?'-':($value->hdp))?></td>
                                        <td class="tr-odd">
                                            <?php if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($matchView->teamHomeEn==$value->teamAwayEn and ($value->homeScore+($value->hdp))<$value->awayScore) or ($matchView->teamHomeEn==$value->teamHomeEn and ($value->homeScore+($value->hdp))>$value->awayScore))){ ?>
                                                <img class="guide-result img-win" src="/images/icon-stat/result/0.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($matchView->teamHomeEn==$value->teamAwayEn and ($value->homeScore+($value->hdp))>$value->awayScore) or ($matchView->teamHomeEn==$value->teamHomeEn and ($value->homeScore+($value->hdp))<$value->awayScore))){?>
                                                <img class="guide-result img-lose" src="/images/icon-stat/result/2.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($value->homeScore+($value->hdp))==$value->awayScore)){?>
                                                <img class="guide-result img-draw" src="/images/icon-stat/result/1.png">
                                            <?php }else{ ?>
                                                -
                                            <?php } ?>
                                        </td>
                                        <td class="tr-result">
                                            <?php if(($value->homeScore!==null and $value->awayScore!==null) and (($matchView->teamHomeEn==$value->teamAwayEn and $value->homeScore<$value->awayScore) or ($matchView->teamHomeEn==$value->teamHomeEn and $value->homeScore>$value->awayScore))){ ?>
                                                <img class="guide-result img-win" src="/images/icon-stat/result/0.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null) and (($matchView->teamHomeEn==$value->teamAwayEn and $value->homeScore>$value->awayScore) or ($matchView->teamHomeEn==$value->teamHomeEn and $value->homeScore<$value->awayScore))){?>
                                                <img class="guide-result img-lose" src="/images/icon-stat/result/2.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null) and $value->awayScore==$value->homeScore){?>
                                                <img class="guide-result img-draw" src="/images/icon-stat/result/1.png">
                                            <?php }else{ ?>
                                                -
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="guide-tabs-team2 guide-tabs hide" tab="team2">
                        <div class="tab-head-title text-center mg-bt15">
                            <strong>Form Guide</strong>
                        </div>
                        <div class="tab-filter">
                            <div class="pull-left h-league">
                                <img src="<?=$matchView->teamAwayPath?>"> <?=$matchView->teamAwayEn;?>
                            </div>
                            <div class="pull-right">
                                <div class="dropdown"
                                     style="float:left; margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                    <a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        All Match <label id="gt-g-max-match">(<label id="formguide-h-max-match"><?php echo ((count($dataFormGuideAway)>=10)?'10':count($dataFormGuideAway))?></label>)</label>
                                        <span class="caret"></span>
                                    </a>
                                    <ul id="tg-g-select-limit" class="dropdown-menu" aria-labelledby="dLabel">
                                        <?php
                                        if(count($dataFormGuideHome)>=20){
                                            for ($i=1;$i<=20;$i++){ ?>
                                                <li><a onclick="viewFormGuideAway(<?=(($i))?>)">Last Match(<?=(($i))?>)</a></li>
                                            <?php }} ?>
                                        <li><a onclick="viewFormGuideAway(<?=count($dataFormGuideHome)?>)">Last Match(<?=count($dataFormGuideHome)?>)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="bx-h2h mg-bt15">
                            <div class="col-sm-6">
                                <div class="pull-right text-right">
                                    <div id="formguidedonut-ga-oresult"></div>
                                </div>
                                <div class="pull-right">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-winrate" ><span id="oddPWin">0</span>%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-loserate"><span id="oddPLose">0</span>%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-drawrate"><span id="oddPDraw">0</span>%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <h3>Odds</h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="pull-left">
                                    <div id="formguidedonut-ga-result"></div>
                                </div>
                                <div class="pull-left">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-result-winrate"><span id="resultPWin">0</span>%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-result-loserate"><span id="resultPLose">0</span>%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                                <td>
                                                    <div class="bx-percent h2hh-result-drawrate"><span id="resultPDraw">0</span>%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <h3>Result</h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="mg-bt15">
                            <table class="table-h2h table-striped">
                                <thead>
                                <tr>
                                    <th>League</th>
                                    <th>Date</th>
                                    <th colspan="3">
                                        <div class="tab-filter text-center">
                                            <div class="dropdown"
                                                 style="display:inline-block;margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                                <a data-target="#" href="http://example.com" data-toggle="dropdown"
                                                   role="button" aria-haspopup="true" aria-expanded="true">
                                                    <span class="tg-currentside-view"><span id="status-view-formguide">Total</span> Match</span>
                                                    <span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li><a class="tg-viewside" onclick="viewMatchFormguideAway('Total')" side="all" type="GameHistory">Total Math</a></li>
                                                    <li><a class="tg-viewside" onclick="viewMatchFormguideAway('Home')" side="home" type="GameHistory">Home</a></li>
                                                    <li><a class="tg-viewside" onclick="viewMatchFormguideAway('Away')" side="away" type="GameHistory">Away</a></li>
                                                </ul>
                                            </div>
                                            <div style="clear:both;"></div>
                                        </div>
                                    </th>
                                    <th>HDP</th>
                                    <th>Odds</th>
                                    <th>Result</th>
                                </tr>
                                </thead>
                                <tbody id="teamaway-guide-list">
                                <?php foreach ($dataFormGuideAway as $key=>$value){ ?>
                                    <tr id="guide-row-original" class="guide-row tr-formguide tr-formguide-<?=(($matchView->teamAwayEn==$value->teamAwayEn)?'home':'away')?> tr-formguide-<?=floor(($key)/10)?> <?=((floor(($key)/10)==0)?'':'hide')?>">
                                        <td class="guide-league">
                                            <img style="width:25px;" src="<?=$value->leaguePath?>">
                                            <?=$value->leagueName?>
                                        </td>
                                        <td class="guide-date"><?=date("d F Y",strtotime($value->time_match))?></td>
                                        <td class="text-right"><span class="guide-home-name"><?=$value->teamHomeEn?></span>
                                            <img class="guide-home-img" onerror="imgError(this)" src="<?=$value->teamHomePath?>">
                                        </td>
                                        <td class="text-center">
                                            <div class="bx-label-hdp guide-score bg-hdp-blue">
                                                <?=(($value->homeScore==null)?0:$value->homeScore)?> - <?=(($value->awayScore==null)?0:$value->awayScore)?>
                                            </div>
                                        </td>
                                        <td class="text-left">
                                            <img class="guide-away-img" onerror="imgError(this)" src="<?=$value->teamAwayPath?>">
                                            <span class="guide-away-name"><?=$value->teamAwayEn?></span></td>
                                        <td class="guide-odds"><?=(($value->hdp==null)?'-':($value->hdp))?></td>
                                        <td class="tr-odd">
                                            <?php if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($matchView->teamAwayEn==$value->teamAwayEn and ($value->homeScore+($value->hdp))<$value->awayScore) or ($matchView->teamAwayEn==$value->teamHomeEn and ($value->homeScore+($value->hdp))>$value->awayScore))){ ?>
                                                <img class="guide-result img-win" src="/images/icon-stat/result/0.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and(($matchView->teamAwayEn==$value->teamAwayEn and ($value->homeScore+($value->hdp))>$value->awayScore) or ($matchView->teamAwayEn==$value->teamHomeEn and($value->homeScore+($value->hdp))<$value->awayScore))){?>
                                                <img class="guide-result img-lose" src="/images/icon-stat/result/2.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($value->homeScore+($value->hdp))==$value->awayScore)){?>
                                                <img class="guide-result img-draw" src="/images/icon-stat/result/1.png">
                                            <?php }else{ ?>
                                                -
                                            <?php } ?>
                                        </td>
                                        <td class="tr-result">
                                            <?php if(($value->homeScore!==null and $value->awayScore!==null) and (($matchView->teamAwayEn==$value->teamAwayEn and $value->homeScore<$value->awayScore) or ($matchView->teamAwayEn==$value->teamHomeEn and $value->homeScore>$value->awayScore))){ ?>
                                                <img class="guide-result img-win" src="/images/icon-stat/result/0.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null) and (($matchView->teamAwayEn==$value->teamAwayEn and $value->homeScore>$value->awayScore) or ($matchView->teamAwayEn==$value->teamHomeEn and $value->homeScore<$value->awayScore))){?>
                                                <img class="guide-result img-lose" src="/images/icon-stat/result/2.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null) and $value->awayScore==$value->homeScore){?>
                                                <img class="guide-result img-draw" src="/images/icon-stat/result/1.png">
                                            <?php }else{ ?>
                                                -
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
                <?php if(!empty($tableLeague) and  count($tableLeague)<=20){?>
                <div class="wrap mg-bt15">
                    <div class="tab-head-title text-center mg-bt15">
                        <strong>Table League</strong>
                    </div>
                    <div class="tab-filter">
                        <div class="pull-left h-league">
                            <img onerror="flagNotFound(this)" src="<?php echo $matchView->path;?>"> <?php echo $matchView->name;?>
                        </div>
                        <div class="pull-right">
                            <div class="times-content">Filter by League Or Match</div>
                            <div class="dropdown" style="float:left; margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                <a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown"
                                   role="button" aria-haspopup="true" aria-expanded="false">
                                    Filter
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                    <li><a class="rank-select-view" view="all">League</a></li>
                                    <li><a class="rank-select-view" view="match">Match</a></li>
                                </ul>
                            </div>
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                    <table class="table-stat table-striped" style="margin-top: 5px;">
                        <thead>
                        <tr>
                            <th>Position</th>
                            <th>Club</th>
                            <th>Played</th>
                            <th>Win</th>
                            <th>Draw</th>
                            <th>Lost</th>
                            <th>GF</th>
                            <th>GA</th>
                            <th>GD</th>
                            <th>Points</th>
                        </tr>
                        </thead>
                        <tbody id="rank-list">
                        <?php if(!empty($tableLeague)){
                            foreach ($tableLeague as $key=>$value){
                                if(!empty($value)){ ?>
                                <tr id="rank-original-row" style="<?=((!empty($value["name_en"]) and ($value["name_en"]==$matchView->teamHomeEn || $value["name_en"]==$matchView->teamAwayEn))?'color: #ff0000':'')?> ">
                                    <td class="rank-no" style="color: #000000 !important;"><?php echo $key + 1; ?></td>
                                    <td>
                                        <img class="rank-logo" onerror="imgError(this)" src="<?= ((!empty($value["path"])) ? "" . $value["path"] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                        <label class="rank-team"><?php echo $value["name_en"]; ?></label>
                                    </td>
                                    <td class="rank-play"><?php echo $value["PI"]; ?></td>
                                    <td class="rank-win"><?php echo $value["win"]; ?></td>
                                    <td class="rank-draw"><?php echo $value["draw"]; ?></td>
                                    <td class="rank-lose"><?php echo $value["lose"]; ?></td>
                                    <td class="rank-gf"><?php echo $value["GF"]; ?></td>
                                    <td class="rank-ga"><?php echo $value["GA"]; ?></td>
                                    <td class="rank-gd"><?php echo $value["GD"]; ?></td>
                                    <td class="rank-pts"><?php echo $value["totalPts"]; ?></td>
                                </tr>
                            <?php }}} ?>
                        </tbody>
                    </table>
                </div>
                <?php } ?>
                <div class="wrap mg-bt15">
                    <div class="tab-head-title text-center mg-bt15">
                        <strong>Next Match</strong>
                    </div>
                    <div class="bx-next-match mg-bt15">
                        <div class="col-sm-6">
                            <div class="text-left">
                                <img src="<?=$matchView->teamHomePath?>">
                                <span><?=$matchView->teamHomeEn;?></span>
                            </div>
                            <div style="border-right: solid 1px #ddd;">
                                <table class="table-striped">
                                    <?php foreach ($dataNextMatchHome as $value) { ?>
                                        <tr>
                                            <td class="">
                                                <img style="width: 25px;height: 25px;" src="<?=$value->teamHomePath?>">
                                                <?php echo $value->teamHomeEn; ?>
                                            </td>
                                            <td class="text-center">
                                                <div class="bx-label-hdp bg-hdp-blue"><?php echo date("H:i",strtotime($value->time_match));?></div>
                                                <div
                                                        class="times-content"><?php echo $value->leagueName . " " .date("\"d/m/Y\"",strtotime($value->time_match)); ?></div>
                                            </td>
                                            <td class="text-right">
                                                <img style="width: 25px;height: 25px;" src="<?=$value->teamAwayPath?>">
                                                <?php echo $value->teamAwayEn; ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="text-right">
                                <img src="<?=$matchView->teamAwayPath?>">
                                <span><?=$matchView->teamAwayEn;?></span>
                            </div>
                            <div>
                                <table class="table-striped">
                                    <?php foreach ($dataNextMatchAway as $value) { ?>
                                        <tr>
                                            <td class="">
                                                <img style="width: 25px;height: 25px;" src="<?=$value->teamHomePath?>">
                                                <?php echo $value->teamHomeEn; ?>
                                            </td>
                                            <td class="text-center">
                                                <div class="bx-label-hdp bg-hdp-blue"><?php echo date("H:i",strtotime($value->time_match));?></div>
                                                <div
                                                        class="times-content"><?php echo $value->leagueName . " " .date("\"d/m/Y\"",strtotime($value->time_match)); ?></div>
                                            </td>
                                            <td class="text-right">
                                                <img style="width: 25px;height: 25px;" src="<?=$value->teamAwayPath?>">
                                                <?php echo $value->teamAwayEn; ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="clear: both;"></div>
<script>
    google.charts.load("current", {packages: ["corechart"]});
    $(document).ready(function () {
//        HEAD-TO-HEAD
        viewHeadToHeadAway($('.h2h-tabs-team2').find('#h2h-h-max-match').html());
        viewHeadToHeadHome($('.h2h-tabs-team1').find('#h2h-h-max-match').html());
        viewFormGuideHome($('.guide-tabs-team1').find('#formguide-h-max-match').html());
        viewFormGuideAway($('.guide-tabs-team2').find('#formguide-h-max-match').html());

        $("#ul-liveScoreHead").find('li.match-slide').each(function (index) {
            $(this).css('width', ($('#liveScoreHead').width() / 4));
        });

        $(window).resize(function () {
            $("#ul-liveScoreHead").find('li.match-slide').each(function (index) {
                $(this).css('width', ($('#liveScoreHead').width() / 4));
            });
        });

        var $frame = $('#liveScoreHead');
        var startAt = $("#liveScoreHead").find("li.active").first().attr('data-key');
        var frame = new Sly('#liveScoreHead', {
//        slidee:$frame.find('#ul-liveScoreHead'),
            horizontal: 1,
            itemNav: 'basic',
            smart: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 0,
//            scrollBar: $wrap.find('.scrollbar'),
            scrollBy: 1,
            speed: 2000,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,

            // Cycling
            cycleBy: 'items',
            cycleInterval: 6000,
            pauseOnHover: 1,

            // Buttons
//        forward: $wrap.find('.btnforward'),
//        backward: $wrap.find('.btnbackward'),
//        prev: $wrap.find('.btnbackward'),
//        next: $wrap.find('.btnforward'),
//        prevPage: $wrap.find('.backward'),
//        nextPage: $wrap.find('.forward'),

//        draggedClass:  'dragged', // Class for dragged elements (like SLIDEE or scrollbar handle).
            activeClass: 'active-game',  // Class for active items and pages.
//        disabledClass: 'disabled' // Class for disabled navigation elements.
        });
        frame.init();

    });
//    HEAD-TO-HEAD
    function changeTeam(elem) {
        var id = $(elem).attr("tab");

        $('.h2h-select').removeClass('active');
        $(elem).addClass('active');

        $('.h2h-tabs').addClass('hide');
        $('.h2h-tabs-'+id).removeClass('hide');
    }
    function formguideChangeTeam(elem) {
        var id = $(elem).attr("tab");

        $('.guide-select').removeClass('active');
        $(elem).addClass('active');

        $('.guide-tabs').addClass('hide');
        $('.guide-tabs-'+id).removeClass('hide');
    }
    function viewMatchFormguideAway(status) {
        $('.guide-tabs-team2').find('#status-view-formguide').html(status);
        viewFormGuideAway($('.guide-tabs-team2').find('#formguide-h-max-match').html());
    }
    function viewFormGuideAway(total) {
//        console.log(total);
        console.log($('.guide-tabs-team2').find('#status-view-formguide').html());
        var resultWin=0,resultLose=0,resultDraw=0,resultPWin=0,resultPLose=0,resultPDraw=0,oddWin=0,oddLose=0,oddDraw=0,oddPWin=0,oddPLose=0,oddPDraw=0;
        $('.guide-tabs-team2').find('tr.tr-formguide').addClass('hide');

        for (i = 0; i < total; i++) {
//            console.log($('tr.tr-formguide').eq(i).attr('class')+" :: "+$('tr.tr-formguide').eq(i).hasClass('tr-formguide-home'));
            if($('.guide-tabs-team2').find('#status-view-formguide').html()=="Total"){
                $('.guide-tabs-team2').find('tr.tr-formguide').eq(i).removeClass('hide');
            }else if($('.guide-tabs-team2').find('#status-view-formguide').html()=="Away" && $('.guide-tabs-team2').find('tr.tr-formguide').eq(i).hasClass('tr-formguide-home')){
                console.log("IN");
                $('.guide-tabs-team2').find('tr.tr-formguide').eq(i).removeClass('hide');
            }else if($('.guide-tabs-team2').find('#status-view-formguide').html()=="Home" && $('.guide-tabs-team2').find('tr.tr-formguide').eq(i).hasClass('tr-formguide-away')){
                console.log("out");
                $('.guide-tabs-team2').find('tr.tr-formguide').eq(i).removeClass('hide');
            }
        }

        resultWin=$('.guide-tabs-team2').find('tr.tr-formguide').not(".hide").find('td.tr-result').find('img.img-win').length;
        resultLose=$('.guide-tabs-team2').find('tr.tr-formguide').not(".hide").find('td.tr-result').find('img.img-lose').length;
        resultDraw=$('.guide-tabs-team2').find('tr.tr-formguide').not(".hide").find('td.tr-result').find('img.img-draw').length;
        oddWin=$('.guide-tabs-team2').find('tr.tr-formguide').not(".hide").find('td.tr-odd').find('img.img-win').length;
        oddLose=$('.guide-tabs-team2').find('tr.tr-formguide').not(".hide").find('td.tr-odd').find('img.img-lose').length;
        oddDraw=$('.guide-tabs-team2').find('tr.tr-formguide').not(".hide").find('td.tr-odd').find('img.img-draw').length;

        resultPWin=parseInt(100-((((resultWin+resultLose+resultDraw)-resultWin)/(resultWin+resultLose+resultDraw))*100));
        resultPLose=parseInt(100-((((resultWin+resultLose+resultDraw)-resultLose)/(resultWin+resultLose+resultDraw))*100));
        resultPDraw=parseInt(100-((((resultWin+resultLose+resultDraw)-resultDraw)/(resultWin+resultLose+resultDraw))*100));

        if(oddWin!==0 || oddLose!==0 || oddDraw!==0) {
            oddPWin = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddWin) / (oddWin + oddLose + oddDraw)) * 100));
            oddPLose = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddLose) / (oddWin + oddLose + oddDraw)) * 100));
            oddPDraw = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddDraw) / (oddWin + oddLose + oddDraw)) * 100));
        }

        $('.guide-tabs-team2').find('#resultPWin').html(resultPWin);
        $('.guide-tabs-team2').find('#resultPLose').html(resultPLose);
        $('.guide-tabs-team2').find('#resultPDraw').html(resultPDraw);

        $('.guide-tabs-team2').find('#oddPWin').html(oddPWin);
        $('.guide-tabs-team2').find('#oddPLose').html(oddPLose);
        $('.guide-tabs-team2').find('#oddPDraw').html(oddPDraw);

        google.charts.setOnLoadCallback(
            function () {
                drawDonut('formguidedonut-ga-result', [['result', 'value'], ['draw', resultPDraw], ['lose', resultPLose], ['win', resultPWin]]);
                drawDonut('formguidedonut-ga-oresult', [['result', 'value'], ['draw', oddPDraw], ['lose', oddPLose], ['win', oddPWin]]);
            }
        )

        $('.guide-tabs-team2').find('#formguide-h-max-match').html(total);
    }
    function viewMatchFormguideHome(status) {
        $('.guide-tabs-team1').find('#status-view-formguide').html(status);
        viewFormGuideHome($('.guide-tabs-team1').find('#formguide-h-max-match').html());
    }
    function viewFormGuideHome(total) {
        var resultWin=0,resultLose=0,resultDraw=0,resultPWin=0,resultPLose=0,resultPDraw=0,oddWin=0,oddLose=0,oddDraw=0,oddPWin=0,oddPLose=0,oddPDraw=0;
        $('.guide-tabs-team1').find('tr.tr-formguide').addClass('hide');

        for (i = 0; i < total; i++) {
            if($('.guide-tabs-team1').find('#status-view-formguide').html()=="Total"){
                $('.guide-tabs-team1').find('tr.tr-formguide').eq(i).removeClass('hide');
            }else if($('.guide-tabs-team1').find('#status-view-formguide').html()=="Home" && $('.guide-tabs-team1').find('tr.tr-formguide').eq(i).hasClass('tr-formguide-home')){
                $('.guide-tabs-team1').find('tr.tr-formguide').eq(i).removeClass('hide');
            }else if($('.guide-tabs-team1').find('#status-view-formguide').html()=="Away" && $('.guide-tabs-team1').find('tr.tr-formguide').eq(i).hasClass('tr-formguide-away')){
                $('.guide-tabs-team1').find('tr.tr-formguide').eq(i).removeClass('hide');
            }
        }

        resultWin=$('.guide-tabs-team1').find('tr.tr-formguide').not(".hide").find('td.tr-result').find('img.img-win').length;
        resultLose=$('.guide-tabs-team1').find('tr.tr-formguide').not(".hide").find('td.tr-result').find('img.img-lose').length;
        resultDraw=$('.guide-tabs-team1').find('tr.tr-formguide').not(".hide").find('td.tr-result').find('img.img-draw').length;
        oddWin=$('.guide-tabs-team1').find('tr.tr-formguide').not(".hide").find('td.tr-odd').find('img.img-win').length;
        oddLose=$('.guide-tabs-team1').find('tr.tr-formguide').not(".hide").find('td.tr-odd').find('img.img-lose').length;
        oddDraw=$('.guide-tabs-team1').find('tr.tr-formguide').not(".hide").find('td.tr-odd').find('img.img-draw').length;

        resultPWin=parseInt(100-((((resultWin+resultLose+resultDraw)-resultWin)/(resultWin+resultLose+resultDraw))*100));
        resultPLose=parseInt(100-((((resultWin+resultLose+resultDraw)-resultLose)/(resultWin+resultLose+resultDraw))*100));
        resultPDraw=parseInt(100-((((resultWin+resultLose+resultDraw)-resultDraw)/(resultWin+resultLose+resultDraw))*100));

        if(oddWin!==0 || oddLose!==0 || oddDraw!==0) {
            oddPWin = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddWin) / (oddWin + oddLose + oddDraw)) * 100));
            oddPLose = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddLose) / (oddWin + oddLose + oddDraw)) * 100));
            oddPDraw = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddDraw) / (oddWin + oddLose + oddDraw)) * 100));
        }

        $('.guide-tabs-team1').find('#resultPWin').html(resultPWin);
        $('.guide-tabs-team1').find('#resultPLose').html(resultPLose);
        $('.guide-tabs-team1').find('#resultPDraw').html(resultPDraw);

        $('.guide-tabs-team1').find('#oddPWin').html(oddPWin);
        $('.guide-tabs-team1').find('#oddPLose').html(oddPLose);
        $('.guide-tabs-team1').find('#oddPDraw').html(oddPDraw);

        google.charts.setOnLoadCallback(
            function () {
                drawDonut('formguidedonut-gh-result', [['result', 'value'], ['draw', resultPDraw], ['lose', resultPLose], ['win', resultPWin]]);
                drawDonut('formguidedonut-gh-oresult', [['result', 'value'], ['draw', oddPDraw], ['lose', oddPLose], ['win', oddPWin]]);
            }
        )

        $('.guide-tabs-team1').find('#formguide-h-max-match').html(total);
    }
    function viewMatchAway(status) {
        $('.h2h-tabs-team2').find('#status-view-h2h').html(status);
        viewHeadToHeadAway($('.h2h-tabs-team2').find('#h2h-h-max-match').html());
    }
    function viewHeadToHeadAway(total) {
        var resultWin=0,resultLose=0,resultDraw=0,resultPWin=0,resultPLose=0,resultPDraw=0,oddWin=0,oddLose=0,oddDraw=0,oddPWin=0,oddPLose=0,oddPDraw=0;
        $('.h2h-tabs-team2').find('tr.tr-h2h').addClass('hide');

        for (i = 0; i < total; i++) {
            if($('.h2h-tabs-team2').find('#status-view-h2h').html()=="Total"){
                $('.h2h-tabs-team2').find('tr.tr-h2h').eq(i).removeClass('hide');
            }else if($('.h2h-tabs-team2').find('#status-view-h2h').html()=="Away" && $('.h2h-tabs-team2').find('tr.tr-h2h').eq(i).hasClass('tr-h2h-home')){
                $('.h2h-tabs-team2').find('tr.tr-h2h').eq(i).removeClass('hide');
            }else if($('.h2h-tabs-team2').find('#status-view-h2h').html()=="Home" && $('.h2h-tabs-team2').find('tr.tr-h2h').eq(i).hasClass('tr-h2h-away')){
                $('.h2h-tabs-team2').find('tr.tr-h2h').eq(i).removeClass('hide');
            }
        }

        resultWin=$('.h2h-tabs-team2').find('tr.tr-h2h').not(".hide").find('td.tr-result').find('img.img-win').length;
        resultLose=$('.h2h-tabs-team2').find('tr.tr-h2h').not(".hide").find('td.tr-result').find('img.img-lose').length;
        resultDraw=$('.h2h-tabs-team2').find('tr.tr-h2h').not(".hide").find('td.tr-result').find('img.img-draw').length;
        oddWin=$('.h2h-tabs-team2').find('tr.tr-h2h').not(".hide").find('td.tr-odd').find('img.img-win').length;
        oddLose=$('.h2h-tabs-team2').find('tr.tr-h2h').not(".hide").find('td.tr-odd').find('img.img-lose').length;
        oddDraw=$('.h2h-tabs-team2').find('tr.tr-h2h').not(".hide").find('td.tr-odd').find('img.img-draw').length;

        resultPWin=parseInt(100-((((resultWin+resultLose+resultDraw)-resultWin)/(resultWin+resultLose+resultDraw))*100));
        resultPLose=parseInt(100-((((resultWin+resultLose+resultDraw)-resultLose)/(resultWin+resultLose+resultDraw))*100));
        resultPDraw=parseInt(100-((((resultWin+resultLose+resultDraw)-resultDraw)/(resultWin+resultLose+resultDraw))*100));

        if(oddWin!==0 || oddLose!==0 || oddDraw!==0) {
            oddPWin = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddWin) / (oddWin + oddLose + oddDraw)) * 100));
            oddPLose = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddLose) / (oddWin + oddLose + oddDraw)) * 100));
            oddPDraw = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddDraw) / (oddWin + oddLose + oddDraw)) * 100));
        }

        $('.h2h-tabs-team2').find('#resultPWin').html(resultPWin);
        $('.h2h-tabs-team2').find('#resultPLose').html(resultPLose);
        $('.h2h-tabs-team2').find('#resultPDraw').html(resultPDraw);

        $('.h2h-tabs-team2').find('#oddPWin').html(oddPWin);
        $('.h2h-tabs-team2').find('#oddPLose').html(oddPLose);
        $('.h2h-tabs-team2').find('#oddPDraw').html(oddPDraw);

        google.charts.setOnLoadCallback(
            function () {
                drawDonut('h2hdonut-a-result', [['result', 'value'], ['draw', resultPDraw], ['lose', resultPLose], ['win', resultPWin]]);
                drawDonut('h2hdonut-a-oresult', [['result', 'value'], ['draw', oddPDraw], ['lose', oddPLose], ['win', oddPWin]]);
            }
        )

        $('.h2h-tabs-team2').find('#h2h-h-max-match').html(total);
    }
    function viewMatchHome(status) {
        $('.h2h-tabs-team1').find('#status-view-h2h').html(status);
        viewHeadToHeadHome($('.h2h-tabs-team1').find('#h2h-h-max-match').html());
    }
    function viewHeadToHeadHome(total) {
        var resultWin=0,resultLose=0,resultDraw=0,resultPWin=0,resultPLose=0,resultPDraw=0,oddWin=0,oddLose=0,oddDraw=0,oddPWin=0,oddPLose=0,oddPDraw=0;
        $('.h2h-tabs-team1').find('tr.tr-h2h').addClass('hide');

        for (i = 0; i < total; i++) {
            if($('.h2h-tabs-team1').find('#status-view-h2h').html()=="Total"){
                $('.h2h-tabs-team1').find('tr.tr-h2h').eq(i).removeClass('hide');
            }else if($('.h2h-tabs-team1').find('#status-view-h2h').html()=="Home" && $('.h2h-tabs-team1').find('tr.tr-h2h').eq(i).hasClass('tr-h2h-home')){
                $('.h2h-tabs-team1').find('tr.tr-h2h').eq(i).removeClass('hide');
            }else if($('.h2h-tabs-team1').find('#status-view-h2h').html()=="Away" && $('.h2h-tabs-team1').find('tr.tr-h2h').eq(i).hasClass('tr-h2h-away')){
                $('.h2h-tabs-team1').find('tr.tr-h2h').eq(i).removeClass('hide');
            }
        }

        resultWin=$('.h2h-tabs-team1').find('tr.tr-h2h').not(".hide").find('td.tr-result').find('img.img-win').length;
        resultLose=$('.h2h-tabs-team1').find('tr.tr-h2h').not(".hide").find('td.tr-result').find('img.img-lose').length;
        resultDraw=$('.h2h-tabs-team1').find('tr.tr-h2h').not(".hide").find('td.tr-result').find('img.img-draw').length;
        oddWin=$('.h2h-tabs-team1').find('tr.tr-h2h').not(".hide").find('td.tr-odd').find('img.img-win').length;
        oddLose=$('.h2h-tabs-team1').find('tr.tr-h2h').not(".hide").find('td.tr-odd').find('img.img-lose').length;
        oddDraw=$('.h2h-tabs-team1').find('tr.tr-h2h').not(".hide").find('td.tr-odd').find('img.img-draw').length;

        resultPWin=parseInt(100-((((resultWin+resultLose+resultDraw)-resultWin)/(resultWin+resultLose+resultDraw))*100));
        resultPLose=parseInt(100-((((resultWin+resultLose+resultDraw)-resultLose)/(resultWin+resultLose+resultDraw))*100));
        resultPDraw=parseInt(100-((((resultWin+resultLose+resultDraw)-resultDraw)/(resultWin+resultLose+resultDraw))*100));

        if(oddWin!==0 || oddLose!==0 || oddDraw!==0) {
            oddPWin = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddWin) / (oddWin + oddLose + oddDraw)) * 100));
            oddPLose = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddLose) / (oddWin + oddLose + oddDraw)) * 100));
            oddPDraw = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddDraw) / (oddWin + oddLose + oddDraw)) * 100));
        }

        $('.h2h-tabs-team1').find('#resultPWin').html(resultPWin);
        $('.h2h-tabs-team1').find('#resultPLose').html(resultPLose);
        $('.h2h-tabs-team1').find('#resultPDraw').html(resultPDraw);

        $('.h2h-tabs-team1').find('#oddPWin').html(oddPWin);
        $('.h2h-tabs-team1').find('#oddPLose').html(oddPLose);
        $('.h2h-tabs-team1').find('#oddPDraw').html(oddPDraw);

        google.charts.setOnLoadCallback(
            function () {
                drawDonut('h2hdonut-h-result', [['result', 'value'], ['draw', resultPDraw], ['lose', resultPLose], ['win', resultPWin]]);
                drawDonut('h2hdonut-h-oresult', [['result', 'value'], ['draw', oddPDraw], ['lose', oddPLose], ['win', oddPWin]]);
            }
        )

        $('.h2h-tabs-team1').find('#h2h-h-max-match').html(total);
    }
    function drawDonut(element, data) {
//        console.log(data);
        var data = google.visualization.arrayToDataTable(data);
        var options = {
            pieStartAngle: 0,
            chartArea: {top: 7, height: '90%', width: '100%'},
            height: '180',
            width: '203',
            colors: ['#888', '#e40520', '#0091c8'],
            pieHole: 0.5,
            legend: 'none',
        };

        var chart = new google.visualization.PieChart(document.getElementById(element));
        chart.draw(data, options);
    }
    function imgError(image) {
        image.onerror = "";
        image.src = "http://api.ssporting.com/teams_clean/team_default_32x32.png";
        return true;
    }
</script>
<script src="/js/play/game.js"></script>
<?php //include 'footer.php';
include 'view/index/footer.php';
?>
</body>
</html>



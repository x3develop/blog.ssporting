<?php
namespace model;

require_once __DIR__ . "/../bootstrap.php";
use Illuminate\Database\Eloquent\Model;

class ReviewHighlight extends Model
{
    protected $table = "review_highlight";
    protected $fillable = ['mid', 'show_date', 'active', 'created_at', 'updated_at'];

    public function match()
    {
        return $this->hasOne(MatchElo::class, "id", "mid");
    }

}

<?php
namespace model;

require_once __DIR__ . "/../bootstrap.php";
use Illuminate\Database\Eloquent\Model;

class HdpHistory extends Model
{
    protected $table = "hdp_history";
    protected $fillable = ['mid', 'home', 'hdp', 'away', 'created_at', 'updated_at'];

    public function matche()
    {
        return $this->belongsTo(MatchElo::class, "id", "mid");
    }

}

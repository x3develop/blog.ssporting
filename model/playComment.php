<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/29/2017
 * Time: 15:19
 */
include_once "DBPDO.php";

class playComment
{
    function checkReviewMatch(){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sth = $dbh->prepare("");
            $sth->bindParam(':match_id', $match_id);
            $sth->bindParam(':comment_match_id', $comment_match_id);
            $sth->execute();
            $miniComment = $sth->fetchAll(PDO::FETCH_COLUMN|PDO::FETCH_GROUP);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $miniComment;
    }

    function getMiniComment($match_id,$comment_match_id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sth = $dbh->prepare("select comment_match.id,comment_match.fb_uid,comment_match.fb_uid,comment_match.`comment`,comment_match.updated_at,userFb.name,userNo.username,mp.countHeart from comment_match
left join user userNo on userNo.id=comment_match.user_id
left join user userFb on userFb.fb_uid=comment_match.fb_uid
LEFT JOIN (SELECT comment_heart.comment_match_id, count(*) as countHeart FROM comment_heart where comment_heart.match_id=:match_id GROUP BY comment_heart.comment_match_id) mp ON mp.comment_match_id = comment_match.id
where comment_match.comment_match_id=:comment_match_id");
            $sth->bindParam(':match_id', $match_id);
            $sth->bindParam(':comment_match_id', $comment_match_id);
            $sth->execute();
            $miniComment = $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $miniComment;
    }

    function checkHeartByUser($match_id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sth = $dbh->prepare("select CONCAT(fb_uid, '-', comment_match_id),id from comment_heart where comment_heart.match_id=:match_id");
            $sth->bindParam(':match_id', $match_id);
            $sth->execute();
            $heartlist = $sth->fetchAll(PDO::FETCH_COLUMN|PDO::FETCH_GROUP);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $heartlist;
    }

    function checkHeartByMatch($match_id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sth = $dbh->prepare("select comment_match_id,count(*) from `comment_heart` where match_id=:match_id group by comment_heart.comment_match_id");
            $sth->bindParam(':match_id', $match_id);
            $sth->execute();
            $heartlist = $sth->fetchAll(PDO::FETCH_COLUMN|PDO::FETCH_GROUP);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $heartlist;
    }

    function getCommentMatchById($id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $user=array();
        try {
            $sql = "select *,userFb.name from `comment_match` left join user userFb on userFb.fb_uid=comment_match.fb_uid where `comment_match`.`id`=:id";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $comment=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $comment;
    }

    function checkHeart($comment_id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $user=array();
        try {
            $sql = "select count(*) as heart from comment_heart where comment_match_id=:comment_match_id";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':comment_match_id', $comment_id);
            $stmt->execute();
            $heart=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $heart;
    }

    function removeHeart($id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select * from comment_heart where id=:id";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $commentHeart=$stmt->fetch();
            if(!empty($commentHeart)) {
                $sql = "DELETE FROM comment_heart WHERE id=:id";
                $stmt = $dbh->prepare($sql);
                $stmt->bindParam(':id', $id);
                $stmt->execute();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $commentHeart;
    }

    function addHeart($id,$match_id,$fb_uid){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select * from comment_heart where match_id=:match_id and fb_uid=:fb_uid and comment_match_id=:comment_match_id";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':fb_uid', $fb_uid);
            $stmt->bindParam(':match_id',$match_id);
            $stmt->bindParam(':comment_match_id', $id);
            $stmt->execute();
            $heart=$stmt->fetch();
            if(empty($heart)){
                $sql = "INSERT INTO `comment_heart` (`fb_uid`, `comment_match_id`,`match_id`, `created_at`,`updated_at`) VALUES (:fb_uid,:comment_match_id,:match_id,:updated_at,:created_at);";
                $stmt = $dbh->prepare($sql);
                $stmt->bindParam(':fb_uid', $fb_uid);
                $stmt->bindParam(':comment_match_id', $id);
                $stmt->bindParam(':match_id', $match_id);
                $stmt->bindParam(':created_at', date('Y-m-d H:i:s'));
                $stmt->bindParam(':updated_at', date('Y-m-d H:i:s'));
                $stmt->execute();
                $heart=$dbh->lastInsertId();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $heart;
    }

    function addComment($match_id,$team,$comment,$fb_uid,$comment_match_id=null){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            if(!empty($comment_match_id)){
                $sql = "INSERT INTO `comment_match` (`comment_match_id`,`fb_uid`, `match_id`, `team`,`comment`,`created_at`,`updated_at`) VALUES (:comment_match_id,:fb_uid,:match_id,:team,:comment,:updated_at,:created_at);";
                $stmt = $dbh->prepare($sql);
                $stmt->bindParam(':comment_match_id', $comment_match_id);
                $stmt->bindParam(':fb_uid', $fb_uid);
                $stmt->bindParam(':match_id', $match_id);
                $stmt->bindParam(':team', $team);
                $stmt->bindParam(':comment', $comment);
                $stmt->bindParam(':created_at', date('Y-m-d H:i:s'));
                $stmt->bindParam(':updated_at', date('Y-m-d H:i:s'));
            }else {
                $sql = "INSERT INTO `comment_match` (`fb_uid`, `match_id`, `team`,`comment`,`created_at`,`updated_at`) VALUES (:fb_uid,:match_id,:team,:comment,:updated_at,:created_at);";
                $stmt = $dbh->prepare($sql);
                $stmt->bindParam(':fb_uid', $fb_uid);
                $stmt->bindParam(':match_id', $match_id);
                $stmt->bindParam(':team', $team);
                $stmt->bindParam(':comment', $comment);
                $stmt->bindParam(':created_at', date('Y-m-d H:i:s'));
                $stmt->bindParam(':updated_at', date('Y-m-d H:i:s'));
            }
            $stmt->execute();
            $comment=$dbh->lastInsertId();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $comment;
    }
}



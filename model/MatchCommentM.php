<?php

/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 27/2/2560
 * Time: 14:36 น.
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
require_once 'DBPDO.php';

class MatchCommentM
{
    function getComment($mid = 0)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($mid);
        $sql = "SELECT cm.*,fu.uid,fu.display_name FROM comment_on_match cm
LEFT JOIN facebook_user fu ON fu.`fb_uid`=cm.`fb_uid`
WHERE cm.match_id=?
AND cm.remove='N'";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getgetCommentPack($mid = 0)
    {
        $result = array("commentlist" => [], 'likelist' => [], "keylist" => array("comment" => [], 'like' => []));
        $rs = $this->getComment($mid);
        foreach ($rs as $k => $v) {
            $keycount = 0;
            if ($v->parent_id == 0) {
                $comment = array('comment' => [], 'reply' => []);
                foreach ($v as $key => $val) {
                    $comment['comment'][] = $val;
                    if ($k == 0) {
                        $result["keylist"]["comment"][$key] = $keycount;
                        $keycount++;
                    }
                }
                $result['commentlist'][$v->id] = $comment;
            } else {
                $rp = array();
                foreach ($v as $key => $val) {
                    $rp[] = $val;
                }
                $result['commentlist'][$v->parent_id]['reply'][] = $rp;
            }
        }

        $rslike = $this->getLikeList($mid);
        foreach ($rslike as $k => $v) {
            $result['likelist'][$v->comment_id][] = $v->fb_uid;
        }

        return $result;
    }

    function addComment($mid, $fbuid, $parent, $side, $msg)
    {
        $dbpdo = new DBPDO();
        $last_id = 0;
        $dt = \Carbon\Carbon::now();
        $params = array($parent, $mid, $fbuid, $msg, $side, $dt->timestamp, $dt->timestamp);
        $sql = "INSERT INTO `comment_on_match` (`parent_id`, `match_id`, `fb_uid`, `message`, `choose`, `comment_at`, `last_update`)
VALUES (?, ?, ?, ?, ?, ?, ?);";
//        echo $sql;
        try {
            $dbpdo->connect();
            $last_id = $dbpdo->insert($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $last_id;
    }

    function addLike($mid, $commentid, $fbuid, $status = 'like')
    {
        $dbpdo = new DBPDO();
        $params = array($mid, $commentid, $fbuid, $status, $status);
        $last_id = 0;
        $sql = "INSERT INTO `match_comment_like` (`match_id`,`comment_id`, `fb_uid`, `status`)
VALUES (?, ?, ?, ?)
ON DUPLICATE KEY  UPDATE `status`=?
";
//        echo $sql;
        try {
            $dbpdo->connect();
            $last_id = $dbpdo->insert($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $last_id;
    }


    function updateXVal($comment_id, $col, $val)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($val, $comment_id);
        $sql = "UPDATE `comment_on_match` com SET {$col}=?
WHERE com.`id` = ?
";

//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->update($sql, $params);
            if ($rs) {

            }

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function increaseLike($comment_id)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($comment_id);

        $sql = "UPDATE `comment_on_match` SET `like`=`like`+1
WHERE `id` = ?
";


        try {
            $dbpdo->connect();
            $rs = $dbpdo->update($sql, $params);
            if ($rs) {

            }

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function decreaseLike($comment_id)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($comment_id);

        $sql = "UPDATE `comment_on_match` SET `like`=`like`-1
WHERE `id` = ?
";

        try {
            $dbpdo->connect();
            $rs = $dbpdo->update($sql, $params);
            if ($rs) {

            }

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function decreaseXVal($comment_id, $col, $val)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($val, $comment_id);
        $sql = "UPDATE `comment_on_match` com SET {$col}={$col}-?
WHERE com.`id` = ?
";

//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->update($sql, $params);
            if ($rs) {

            }

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getLikeList($mid)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($mid);
        $sql = "SELECT mcl.match_id,mcl.comment_id,mcl.fb_uid,mcl.`status` FROM match_comment_like mcl
WHERE mcl.match_id=?
AND mcl.`status`='like'";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

}
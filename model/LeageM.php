<?php

/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 9/2/2560
 * Time: 13:26 น.
 */
require_once "DBPDO.php";

class LeageM
{


    function getRanking($lid, $slid)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($lid, $slid);
        $sql = "SELECT st.`tid`, t.teamNameEn,t.teamNameTh,st.`leagueId`,st.subLeagueId,ll.leagueNameEn,ll.leagueNameTh,st.`no`,st.w,st.d,st.l,st.`gp`,st.`gf`,st.ga,st.`pts` FROM stat_table st
LEFT JOIN lang_team t ON t.tid = st.`tid`
LEFT JOIN lang_league ll ON ll.leagueId = st.subLeagueId
WHERE st.`leagueId`=?
AND st.subLeagueId=?
ORDER BY st.`group`,st.`no`";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getRankingPack($lid, $slid)
    {
        $result = array("rank" => [], "keylist" => array("rank" => []));
        $rs = $this->getRanking($lid, $slid);
        $keycount = 0;
        foreach ($rs as $k => $v) {
            foreach ($v as $key => $val) {
                $result["rank"][$k][] = $val;
                if ($k == 0) {
                    $result["keylist"]["rank"][$key] = $keycount;
                    $keycount++;
                }

            }
        }
        return $result;

    }


}
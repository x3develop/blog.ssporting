<?php
namespace model;

require_once __DIR__ . "/../bootstrap.php";
use Illuminate\Database\Eloquent\Model;

class CountryElo extends Model
{
    protected $table = "country";
    protected $fillable = ['id_7m', 'name', 'created_at', 'updated_at'];

    public function leagues()
    {
        return $this->hasMany(LeagueElo::class, "country_id", "id");
    }
    
}

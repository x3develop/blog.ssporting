<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 11/18/2016
 * Time: 16:32
 */
class news extends db_connect{
    protected $nameTable="news";

    public function findAll($limit){
        $data=array();
        $conn=$this->db_connect();
        $query=$conn->query("select titleTh,imageLink from ".$this->nameTable." order by newsId DESC limit ".$limit);
        if($query->num_rows>0){
            while ($row = $query->fetch_object()) {
                array_push($data,$row);
            }
            return $data;
        }else{
            return 0;
        }
        $conn->close();
        $query->free();
    }
}
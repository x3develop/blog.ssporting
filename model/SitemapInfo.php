<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 1/25/2019
 * Time: 9:46 AM
 */
require_once "DBPDO.php";
class SitemapInfo
{
    public function getByOffset($offset){
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array();
        $sql = "SELECT * FROM sitemap_info order by id asc limit 20000 offset ".$offset;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }
    public function countPage(){
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array();
        $sql = "SELECT CEILING (count(*)/20000) as maxPage FROM sitemap_info";
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    public function add($url){
        $dbpdo = new DBPDO();
        $last_id = 0;
        $sql = "insert into sitemap_info (url) values (?)";
        $params = array($url);
        $sqlCheck = "SELECT * FROM sitemap_info where url=?";
        $paramsCheck = array($url);
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sqlCheck, $paramsCheck);
            if(count($rs)==0) {
                $last_id = $dbpdo->insert($sql, $params);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $last_id;
    }
}
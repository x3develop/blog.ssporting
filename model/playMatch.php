<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/27/2017
 * Time: 17:19
 */
include_once "DBPDO.php";

class playMatch
{
    function percentBet($id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sth = $dbh->prepare("select ((((sum(case bet.`team` when 'home' then 1 else 0 end)
+sum(case bet.`team` when 'away' then 1 else 0 end))
-sum(case bet.`team` when 'away' then 1 else 0 end))
/(sum(case bet.`team` when 'home' then 1 else 0 end)
+sum(case bet.`team` when 'away' then 1 else 0 end)))*100) as pHome,
((((sum(case bet.`team` when 'home' then 1 else 0 end)+sum(case bet.`team` when 'away' then 1 else 0 end))
-sum(case bet.`team` when 'home' then 1 else 0 end))
/(sum(case bet.`team` when 'home' then 1 else 0 end)+sum(case bet.`team` when 'away' then 1 else 0 end)))*100) as pAway
from bet where bet.match_id=".$id);
        $sth->execute();
        return $sth->fetch();
    }

    public function getByYesterdayTodayTomorrow(){
        $dbpdo = new DBPDO();
        $match = array();
        $params = array();
        $sql = "select DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) as dateMatch,`match`.match_7m_id,`match`.id as match_id,`match`.time_match,`match`.`status`,`match`.team_home,`match`.team_away,`match`.home_end_time_score,`match`.home_over_time_score,`match`.away_end_time_score,`match`.away_over_time_score,teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,teamAway.name_en as teamAwayEn,teamAway.path as teamAwayPath,`league`.name,`league`.path,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join `gamble` on `gamble`.match_id=`match`.id 
left join `league` on `league`.id=`match`.league_id 
left join `review_match` on `review_match`.match_id=`match`.id
where review_match.id IS NOT NULL and `gamble`.handicap IS NOT NULL and (DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR))>=(CURDATE() - INTERVAL 1 DAY)) and (DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR))<=(CURDATE() + INTERVAL 1 DAY)) group by `match`.id order by `match`.id DESC";
        try {
            $dbpdo->connect2();
            $match = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $match;
    }

    public  function getByReviewYesterday($limit){
        $dbpdo = new DBPDO();
        $match = array();
        $params = array();
        $sql = "select DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) as dateMatch,`match`.match_7m_id,`match`.id as match_id,`match`.time_match,`match`.`status`,`match`.team_home,`match`.team_away,`match`.home_end_time_score,`match`.home_over_time_score,`match`.away_end_time_score,`match`.away_over_time_score,teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,teamAway.name_en as teamAwayEn,teamAway.path as teamAwayPath,`league`.name,`league`.path,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join `gamble` on `gamble`.match_id=`match`.id 
left join `league` on `league`.id=`match`.league_id 
left join `review_match` on `review_match`.match_id=`match`.id
where `review_match`.id IS NOT NULL and `gamble`.handicap IS NOT NULL and (DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR))=(CURDATE() - INTERVAL 1 DAY))
group by match.id 
order by time_match DESC limit ".$limit;
        try {
            $dbpdo->connect2();
            $match = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $match;
    }

    public  function getByReviewToday($limit){
        $dbpdo = new DBPDO();
        $match = array();
        $params = array();
        $sql = "select DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) as dateMatch,`match`.match_7m_id,`match`.id as match_id,`match`.time_match,`match`.`status`,`match`.team_home,`match`.team_away,`match`.home_end_time_score,`match`.home_over_time_score,`match`.away_end_time_score,`match`.away_over_time_score,teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,teamAway.name_en as teamAwayEn,teamAway.path as teamAwayPath,`league`.name,`league`.path,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join `gamble` on `gamble`.match_id=`match`.id 
left join `league` on `league`.id=`match`.league_id 
left join `review_match` on `review_match`.match_id=`match`.id
where `review_match`.id IS NOT NULL and `gamble`.handicap IS NOT NULL and (DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR))=CURDATE())
group by match.id 
order by time_match DESC limit ".$limit;
        try {
            $dbpdo->connect2();
            $match = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $match;
    }

    public  function getByReviewTomorrow($limit){
        $dbpdo = new DBPDO();
        $match = array();
        $params = array();
        $sql = "select DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) as dateMatch,`match`.match_7m_id,`match`.id as match_id,`match`.time_match,`match`.`status`,`match`.team_home,`match`.team_away,`match`.home_end_time_score,`match`.home_over_time_score,`match`.away_end_time_score,`match`.away_over_time_score,teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,teamAway.name_en as teamAwayEn,teamAway.path as teamAwayPath,`league`.name,`league`.path,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join `gamble` on `gamble`.match_id=`match`.id 
left join `league` on `league`.id=`match`.league_id 
left join `review_match` on `review_match`.match_id=`match`.id
where `review_match`.id IS NOT NULL and `gamble`.handicap IS NOT NULL and (DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR))=(CURDATE() + INTERVAL 1 DAY))
group by match.id 
order by time_match DESC limit ".$limit;
        try {
            $dbpdo->connect2();
            $match = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $match;
    }

    public  function getByReview($limit){
        $dbpdo = new DBPDO();
        $match = array();
        $params = array();
        $sql = "select DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) as dateMatch,`match`.match_7m_id,`match`.id as match_id,`match`.time_match,`match`.`status`,`match`.team_home,`match`.team_away,`match`.home_end_time_score,`match`.home_over_time_score,`match`.away_end_time_score,`match`.away_over_time_score,teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,teamAway.name_en as teamAwayEn,teamAway.path as teamAwayPath,`league`.name,`league`.path,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join `gamble` on `gamble`.match_id=`match`.id 
left join `league` on `league`.id=`match`.league_id 
left join `review_match` on `review_match`.match_id=`match`.id
where `review_match`.id IS NOT NULL and `gamble`.handicap IS NOT NULL and (DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR))=CURDATE() or DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR))=CURDATE()-INTERVAL 1 DAY)
group by match.id 
order by time_match DESC limit ".$limit;
        try {
            $dbpdo->connect2();
            $match = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $match;
    }

    public function getMathByStatus($status,$limit){
        $dbpdo = new DBPDO();
        $match = array();
        $params = array();
        $sql = "select `match`.match_7m_id,`match`.id as match_id,`match`.time_match,`match`.`status`,`match`.team_home,`match`.team_away,`match`.home_end_time_score,`match`.home_over_time_score,`match`.away_end_time_score,`match`.away_over_time_score,teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,teamAway.name_en as teamAwayEn,teamAway.path as teamAwayPath,`league`.name,`league`.path,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join `gamble` on `gamble`.match_id=`match`.id 
left join `league` on `league`.id=`match`.league_id 
where match.status='".$status."' and `match`.time_match>DATE_ADD(NOW(), INTERVAL 8 hour)
group by match.id 
order by time_match ASC limit ".$limit;
        try {
            $dbpdo->connect2();
            $match = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $match;
    }

    public function getMathByMainLeagueAndDate($date){
        $dtm = new DateTime($date);
        $startDate=$dtm->format('Y-m-d 12:00:00');
        $dtm->modify('+1 day');
        $toDate=$dtm->format('Y-m-d 12:00:00');
        $dbpdo = new DBPDO();
        $match = array();
        $params = array($startDate,$toDate);
        $sql = "select `match`.id,`match`.match_7m_id,BetHome.countBet as countBetHome,BetAway.countBet as countBetAway,`match`.time_match,`match`.`status`,`match`.team_home,`match`.team_away,`match`.home_end_time_score,
`match`.home_over_time_score,`match`.away_end_time_score,`match`.away_over_time_score,teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,teamAway.name_en as teamAwayEn,teamAway.path as teamAwayPath,
`league`.id as league_id ,`league`.name,`league`.path,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill,`channel_match`.id as channel_match_id,`channel`.channel_name,`channel`.channel_path
from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join `gamble` on `gamble`.match_id=`match`.id 
left join `league` on `league`.id=`match`.league_id 
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='home') BetHome ON BetHome.match_id = `match`.id
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='away') BetAway ON BetAway.match_id = `match`.id
left join `channel_match` on `channel_match`.match_id=`match`.match_7m_id
left join `channel` on `channel`.id=`channel_match`.channel_id
where (`match`.time_match>=? and `match`.time_match<=?) and 
(`league`.id = 1 or `league`.id = 18 or `league`.id = 44 or `league`.id = 27 or `league`.id = 36 or `league`.id = 105 or `league`.id = 608 or `league`.id = 590 or `league`.id =645)
group by `match`.match_7m_id
order by 
CASE
   WHEN `league`.id = 645 THEN 9
   WHEN `league`.id = 1 THEN 8
   WHEN `league`.id = 18 THEN 7
   WHEN `league`.id = 44 THEN 6
   WHEN `league`.id = 27 THEN 5
   WHEN `league`.id = 36 THEN 4
   WHEN `league`.id = 105 THEN 3
   WHEN `league`.id = 608 THEN 2
   WHEN `league`.id = 590 THEN 1
END desc , `match`.time_match ASC";
        try {
            $dbpdo->connect2();
            $match = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $match;
    }

    public function getMathByLeagueAndDate($league_id,$date){
        $dtm = new DateTime($date);
        $startDate=$dtm->format('Y-m-d 12:00:00');
        $dtm->modify('+1 day');
        $toDate=$dtm->format('Y-m-d 12:00:00');
        $dbpdo = new DBPDO();
        $match = array();
        $params = array($league_id,$startDate,$toDate);
        $sql = "select `match`.id,`match`.match_7m_id,BetHome.countBet as countBetHome,BetAway.countBet as countBetAway,`match`.time_match,`match`.`status`,`match`.team_home,`match`.team_away,`match`.home_end_time_score,`match`.home_over_time_score,`match`.away_end_time_score,`match`.away_over_time_score,teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,teamAway.name_en as teamAwayEn,teamAway.path as teamAwayPath,`league`.id as league_id ,`league`.name,`league`.path,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join `gamble` on `gamble`.match_id=`match`.id 
left join `league` on `league`.id=`match`.league_id 
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='home') BetHome ON BetHome.match_id = `match`.id
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='away') BetAway ON BetAway.match_id = `match`.id
where `match`.league_id=? and (`match`.time_match>=? and `match`.time_match<=?)
order by time_match ASC ";
        try {
            $dbpdo->connect2();
            $match = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $match;
    }

    public function getNextMatch($teamid,$mathid){
        $dbpdo = new DBPDO();
        $user = array();
        $params = array();
        $sql = "select `match`.id,match.time_match,league.name as leagueName,league.path as leaguePath,
teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,`match`.home_end_time_score as homeScore,
`match`.away_end_time_score as awayScore,teamAway.path as teamAwayPath,teamAway.name_en as teamAwayEn,
gamble.handicap as hdp
from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join gamble on gamble.match_id=`match`.id
left join `league` on `league`.id=`match`.league_id 
where `match`.id!=".$mathid." and `match`.status!='fullTime' and `match`.time_match>=CURDATE() and (`match`.team_home=".$teamid." or `match`.team_away=".$teamid.") order by `match`.time_match asc ";
        try {
            $dbpdo->connect2();
            $user = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $user;
    }

    public function getLeagueByFormGuide($idteam,$limit){
        $dbpdo = new DBPDO();
        $user = array();
        $data = array();
        $params = array();
        $sql = "select `league`.id,`league`.name
from (select * from `match` where `match`.status='fullTime' and (`match`.team_home=".$idteam." or `match`.team_away=".$idteam.") order by `match`.time_match DESC limit ".$limit.") as m
left join `league` on `league`.id=m.league_id 
group by `league`.id";
        try {
            $dbpdo->connect2();
            $data = $dbpdo->query($sql, $params);
            foreach ($data as $key=>$value){
                $league[$value->id]=$value->name;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $league;
    }

    public function getFormGuide($idHome,$limit){
        $dbpdo = new DBPDO();
        $user = array();
        $params = array();
        $sql = "select `match`.id,match.time_match,league.name as leagueName,league.path as leaguePath,league.id as league_id,
teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,`match`.home_end_time_score as homeScore,
`match`.away_end_time_score as awayScore,teamAway.path as teamAwayPath,teamAway.name_en as teamAwayEn,
gamble.handicap as hdp, `match`.team_home, `match`.team_away
from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join gamble on gamble.match_id=`match`.id
left join `league` on `league`.id=`match`.league_id 
where `match`.status='fullTime' and (`match`.team_home=".$idHome." or `match`.team_away=".$idHome.") order by `match`.time_match desc limit ".$limit;
        try {
            $dbpdo->connect2();
            $user = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $user;
    }

    public function getLeagueByHeadToHead($idHome,$idAway){
        $dbpdo = new DBPDO();
        $league = array();
        $data=array();
        $params = array();
        $sql = "select `league`.id,`league`.name
from `match` 
left join `league` on `league`.id=`match`.league_id 
where `match`.status='fullTime' and ((`match`.team_home=".$idAway." and `match`.team_away=".$idHome.") or (`match`.team_home=".$idHome." and `match`.team_away=".$idAway.")) group by `league`.id";
        try {
            $dbpdo->connect2();
            $data = $dbpdo->query($sql, $params);
            foreach ($data as $key=>$value){
                $league[$value->id]=$value->name;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $league;
    }

    public function getHeadToHead($idHome,$idAway){
        $dbpdo = new DBPDO();
        $user = array();
        $params = array();
        $sql = "select `match`.id,match.time_match,league.name as leagueName,league.path as leaguePath,league.id as league_id,
teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,`match`.home_end_time_score as homeScore,
`match`.away_end_time_score as awayScore,teamAway.path as teamAwayPath,teamAway.name_en as teamAwayEn,
gamble.handicap as hdp, `match`.team_home, `match`.team_away
from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join gamble on gamble.match_id=`match`.id
left join `league` on `league`.id=`match`.league_id 
where `match`.status='fullTime' and ((`match`.team_home=".$idAway." and `match`.team_away=".$idHome.") or (`match`.team_home=".$idHome." and `match`.team_away=".$idAway.")) order by `match`.time_match desc";
        try {
            $dbpdo->connect2();
            $user = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $user;
    }

    public function getUserBetByTeam($id,$team,$limit){
        $dbpdo = new DBPDO();
        $user = array();
        $params = array();
        $sql = "select user.id,user.fb_uid,bet.team from bet inner join user on user.id=bet.user_id where bet.team='".$team."' and bet.match_id=".$id." limit ".$limit;
        try {
            $dbpdo->connect2();
            $user = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $user;
    }

    public function getUserBet($id,$team,$limit){
        $dbpdo = new DBPDO();
        $user = array();
        $params = array();
        $sql = "select user.id,user.fb_uid,bet.team from bet inner join user on user.id=bet.user_id where bet.match_id=".$id." and bet.team='".$team."' order by bet.updated_at DESC limit ".$limit;
        try {
            $dbpdo->connect2();
            $user = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $user;
    }

    public function getFirstMathById($id)
    {
        $dbpdo = new DBPDO();
        $match = array();
        $params = array();
        $sql = "select `match`.id,COALESCE(BetHome.countBet,0) as countBetHome,COALESCE(BetAway.countBet,0) as countBetAway,`match`.time_match,`match`.`status`,`match`.team_home,`match`.team_away,`match`.home_end_time_score,`match`.home_over_time_score,`match`.away_end_time_score,`match`.away_over_time_score,teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,teamAway.name_en as teamAwayEn,teamAway.path as teamAwayPath,`league`.id as league_id ,`league`.name,`league`.path,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join `gamble` on `gamble`.match_id=`match`.id 
left join `league` on `league`.id=`match`.league_id 
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='home' and `bet`.match_id=".$id.") BetHome ON BetHome.match_id = `match`.id
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='away' and `bet`.match_id=".$id.") BetAway ON BetAway.match_id = `match`.id
where `match`.id=".$id."
order by time_match DESC ";

        try {
            $dbpdo->connect2();
            $match = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $match;
    }

    public function getMathByStatusBet($limit)
    {
        $dbpdo = new DBPDO();
        $match = array();
        $params = array();
        $sql = "select teamHome.path as teamHomePath,teamAway.path as teamAwayPath,userReviewHome.name as userReviewHomeText,userReviewHome.path as userReviewHomePath,reviewHome.review_text as reviewHomeText,
userReviewAway.name as userReviewAwayText,userReviewAway.path as userReviewAwayPath,reviewAway.review_text as reviewAwayText,`match`.id,BetHome.countBet as countBetHome,BetAway.countBet as countBetAway,`match`.time_match,`match`.`status`,`match`.team_home,`match`.team_away,`match`.home_end_time_score,`match`.home_over_time_score,`match`.away_end_time_score,`match`.away_over_time_score,teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,teamAway.name_en as teamAwayEn,teamAway.path as teamAwayPath,`league`.name,`league`.path,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` 
left join `review_highlight` as reviewHighlight on reviewHighlight.`mid`=`match`.id
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join `gamble` on `gamble`.match_id=`match`.id 
left join `league` on `league`.id=`match`.league_id 
left join `review_match` as reviewHome on reviewHome.match_id=`match`.id and reviewHome.team='home'
left join `review_match` as reviewAway on reviewAway.match_id=`match`.id and reviewAway.team='away'
left join `user_review` as userReviewHome on userReviewHome.id=reviewHome.user_review_id
left join `user_review` as userReviewAway on userReviewAway.id=reviewAway.user_review_id
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='home' group by bet.match_id order by bet.match_id DESC limit 100 ) BetHome ON BetHome.match_id = `match`.id
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='away' group by bet.match_id order by bet.match_id DESC limit 100 ) BetAway ON BetAway.match_id = `match`.id
where match.status='bet' and reviewHighlight.active='Y' and `match`.time_match>DATE_ADD(NOW(), INTERVAL 8 hour)
group by match.id 
order by `match`.updated_at ASC limit ".$limit;
        try {
            $dbpdo->connect2();
            $match = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $match;
    }

    public function getFirstMathByTime()
    {
        $dbpdo = new DBPDO();
        $match = array();
        $params = array();
        $sql = "select teamHome.path as teamHomePath,teamAway.path as teamAwayPath,userReviewHome.name as userReviewHomeText,userReviewHome.path as userReviewHomePath,reviewHome.review_text as reviewHomeText,
userReviewAway.name as userReviewAwayText,userReviewAway.path as userReviewAwayPath,reviewAway.review_text as reviewAwayText,`match`.id,BetHome.countBet as countBetHome,BetAway.countBet as countBetAway,`match`.time_match,`match`.`status`,`match`.team_home,`match`.team_away,`match`.home_end_time_score,`match`.home_over_time_score,`match`.away_end_time_score,`match`.away_over_time_score,teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,teamAway.name_en as teamAwayEn,teamAway.path as teamAwayPath,`league`.name,`league`.path,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` 
left join `review_highlight` as reviewHighlight on reviewHighlight.`mid`=`match`.id
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join `gamble` on `gamble`.match_id=`match`.id 
left join `league` on `league`.id=`match`.league_id 
left join `review_match` as reviewHome on reviewHome.match_id=`match`.id and reviewHome.team='home'
left join `review_match` as reviewAway on reviewAway.match_id=`match`.id and reviewAway.team='away'
left join `user_review` as userReviewHome on userReviewHome.id=reviewHome.user_review_id
left join `user_review` as userReviewAway on userReviewAway.id=reviewAway.user_review_id
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='home' group by bet.match_id order by bet.match_id DESC limit 100 ) BetHome ON BetHome.match_id = `match`.id
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='away' group by bet.match_id order by bet.match_id DESC limit 100 ) BetAway ON BetAway.match_id = `match`.id
where `match`.time_match>DATE_ADD((NOW()- INTERVAL 1 DAY), INTERVAL 8 hour) and `match`.time_match <(DATE_ADD((NOW()+ INTERVAL 2 DAY), INTERVAL 8 hour))
group by match.id 
order by `match`.updated_at ASC";
        try {
            $dbpdo->connect2();
            $match = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $match;
    }

    public function getFirstMathStatusBet($limit)
    {
        $dbpdo = new DBPDO();
        $match = array();
        $params = array();
        $sql = "select teamHome.path as teamHomePath,teamAway.path as teamAwayPath,userReviewHome.name as userReviewHomeText,userReviewHome.path as userReviewHomePath,reviewHome.review_text as reviewHomeText,
userReviewAway.name as userReviewAwayText,userReviewAway.path as userReviewAwayPath,reviewAway.review_text as reviewAwayText,`match`.id,BetHome.countBet as countBetHome,BetAway.countBet as countBetAway,`match`.time_match,`match`.`status`,`match`.team_home,`match`.team_away,`match`.home_end_time_score,`match`.home_over_time_score,`match`.away_end_time_score,`match`.away_over_time_score,teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,teamAway.name_en as teamAwayEn,teamAway.path as teamAwayPath,`league`.name,`league`.path,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` 
left join `review_highlight` as reviewHighlight on reviewHighlight.`mid`=`match`.id
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join `gamble` on `gamble`.match_id=`match`.id 
left join `league` on `league`.id=`match`.league_id 
left join `review_match` as reviewHome on reviewHome.match_id=`match`.id and reviewHome.team='home'
left join `review_match` as reviewAway on reviewAway.match_id=`match`.id and reviewAway.team='away'
left join `user_review` as userReviewHome on userReviewHome.id=reviewHome.user_review_id
left join `user_review` as userReviewAway on userReviewAway.id=reviewAway.user_review_id
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='home' group by bet.match_id order by bet.match_id DESC limit 100 ) BetHome ON BetHome.match_id = `match`.id
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='away' group by bet.match_id order by bet.match_id DESC limit 100 ) BetAway ON BetAway.match_id = `match`.id
where match.status='bet' and reviewHighlight.active='Y' 
group by match.id 
order by `match`.updated_at ASC limit ".$limit;
//        and `match`.time_match>DATE_ADD(NOW(), INTERVAL 8 hour)
        try {
            $dbpdo->connect2();
            $match = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $match;
    }

    public function getFirstMathStatusBetById($id)
    {
        $dbpdo = new DBPDO();
        $match = array();
        $params = array();
        $sql = "select teamHome.path as teamHomePath,teamAway.path as teamAwayPath,userReviewHome.name as userReviewHomeText,userReviewHome.path as userReviewHomePath,reviewHome.review_text as reviewHomeText,
userReviewAway.name as userReviewAwayText,userReviewAway.path as userReviewAwayPath,reviewAway.review_text as reviewAwayText,`match`.id,BetHome.countBet as countBetHome,BetAway.countBet as countBetAway,`match`.time_match,`match`.`status`,`match`.team_home,`match`.team_away,`match`.home_end_time_score,`match`.home_over_time_score,`match`.away_end_time_score,`match`.away_over_time_score,teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,teamAway.name_en as teamAwayEn,teamAway.path as teamAwayPath,`league`.name,`league`.path,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` 
left join `review_highlight` as reviewHighlight on reviewHighlight.`mid`=`match`.id
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join `gamble` on `gamble`.match_id=`match`.id 
left join `league` on `league`.id=`match`.league_id 
left join `review_match` as reviewHome on reviewHome.match_id=`match`.id and reviewHome.team='home'
left join `review_match` as reviewAway on reviewAway.match_id=`match`.id and reviewAway.team='away'
left join `user_review` as userReviewHome on userReviewHome.id=reviewHome.user_review_id
left join `user_review` as userReviewAway on userReviewAway.id=reviewAway.user_review_id
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='home' group by bet.match_id order by bet.match_id DESC limit 100 ) BetHome ON BetHome.match_id = `match`.id
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='away' group by bet.match_id order by bet.match_id DESC limit 100 ) BetAway ON BetAway.match_id = `match`.id
where match.id=".$id."
group by match.id 
order by `match`.updated_at ASC ";
        try {
            $dbpdo->connect2();
            $match = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $match;
    }

    public function getFirstMath($limit)
    {
        $dbpdo = new DBPDO();
        $match = array();
        $params = array();
        $sql = "select userReviewHome.name as userReviewHomeText,userReviewHome.path as userReviewHomePath,reviewHome.review_text as reviewHomeText,
userReviewAway.name as userReviewAwayText,userReviewAway.path as userReviewAwayPath,reviewAway.review_text as reviewAwayText,`match`.id,BetHome.countBet as countBetHome,BetAway.countBet as countBetAway,`match`.time_match,`match`.`status`,`match`.team_home,`match`.team_away,`match`.home_end_time_score,`match`.home_over_time_score,`match`.away_end_time_score,`match`.away_over_time_score,teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,teamAway.name_en as teamAwayEn,teamAway.path as teamAwayPath,`league`.name,`league`.path,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join `gamble` on `gamble`.match_id=`match`.id 
left join `league` on `league`.id=`match`.league_id 
left join `review_match` as reviewHome on reviewHome.match_id=`match`.id and reviewHome.team='home'
left join `review_match` as reviewAway on reviewAway.match_id=`match`.id and reviewAway.team='away'
left join `user_review` as userReviewHome on userReviewHome.id=reviewHome.user_review_id
left join `user_review` as userReviewAway on userReviewAway.id=reviewAway.user_review_id
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='home') BetHome ON BetHome.match_id = `match`.id
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='away') BetAway ON BetHome.match_id = `match`.id
order by time_match DESC limit ".$limit;
        try {
            $dbpdo->connect2();
            $match = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $match;
    }

    public function getListCommentByTeam($match,$team){
        $dbpdo = new DBPDO();
        $comment = array();
        $params = array();
        $sql = "select comment_match.id,comment_match.`comment`,comment_match.team,comment_match.updated_at,comment_match.fb_uid,userFb.name,userNo.username,userNo.path,mp.countComment from comment_match
left join user userNo on userNo.id=comment_match.user_id
left join user userFb on userFb.fb_uid=comment_match.fb_uid
LEFT JOIN (SELECT comment_match_id, count(*) as countComment FROM comment_match where comment_match.team='".$team."' and comment_match.match_id=".$match." and comment_match.comment_match_id IS NOT NULL GROUP BY comment_match.comment_match_id) mp ON mp.comment_match_id = comment_match.id
where comment_match.match_id=".$match." and comment_match.comment_match_id IS NULL order by comment_match.updated_at DESC";
        try {
            $dbpdo->connect2();
            $comment = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $comment;
    }

    public function getListComment($match){
        $dbpdo = new DBPDO();
        $comment = array();
        $params = array();
        $sql = "select comment_match.id,comment_match.`comment`,comment_match.team,comment_match.updated_at,comment_match.fb_uid,userFb.name,userNo.username,userNo.path,mp.countComment from comment_match
left join user userNo on userNo.id=comment_match.user_id
left join user userFb on userFb.fb_uid=comment_match.fb_uid
LEFT JOIN (SELECT comment_match_id, count(*) as countComment FROM comment_match where comment_match.match_id=".$match." and comment_match.comment_match_id IS NOT NULL GROUP BY comment_match.comment_match_id) mp ON mp.comment_match_id = comment_match.id
where comment_match.match_id=".$match." and comment_match.comment_match_id IS NULL order by comment_match.team";
        try {
            $dbpdo->connect2();
            $comment = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $comment;
    }

    public function getListMatchResults($team_id,$limit){
        $dbpdo = new DBPDO();
        $match = array();
        $params = array();
        $sql = "select `match`.time_match,`match`.team_away,`match`.team_home,(home_end_time_score+COALESCE(home_over_time_score,0)) as scoreHome,(away_end_time_score+COALESCE(away_over_time_score,0)) as scoreAway from `match` where `match`.`status`='fullTime' && (`match`.team_home=".$team_id." || `match`.team_away=".$team_id.") order by `match`.time_match DESC limit ".$limit;
        try {
            $dbpdo->connect2();
            $match = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $match;
    }

    public function getListMatchResultsBy($team_home,$team_away,$limit){
        $dbpdo = new DBPDO();
        $match = array();
        $params = array();
        $sql = "select league.name,`match`.time_match,`match`.team_away,teamHome.name_en as name_home,teamHome.path as path_home,`match`.team_home,teamAway.name_en as name_away,teamAway.path as path_away,(home_end_time_score+COALESCE(home_over_time_score,0)) as scoreHome,(away_end_time_score+COALESCE(away_over_time_score,0)) as scoreAway from `match` ";
        $sql .= " left join team as teamHome on teamHome.id=`match`.team_home";
        $sql .= " left join team as teamAway on teamAway.id=`match`.team_away";
        $sql .= " left join league on league.id=`match`.league_id";
        $sql .= " where `match`.`status`='fullTime' && ((`match`.team_home=".$team_home." && `match`.team_away=".$team_away." ) || (`match`.team_away=".$team_home." && `match`.team_home=".$team_away.")) order by `match`.time_match DESC limit ".$limit;
        try {
            $dbpdo->connect2();
            $match = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $match;
    }
}
<?php
namespace model;

require_once __DIR__ . "/../bootstrap.php";
use Illuminate\Database\Eloquent\Model;

class MatchEvent extends Model
{
    protected $table = "match_event";
    protected $fillable = ['mid', 'at_minute', 'side', 'event', 'real_min', 'player', 'assist', 'score', 'created_at', 'updated_at'];
    public function matche()
    {
        return $this->belongsTo(MatchElo::class, "id", "mid");
    }

    public function getEventImgClass()
    {
        $divclass = "";

        switch ($this->event) {
            case "goal":
                $divclass = "goal-score";
                break;
            case "penalty":
                $divclass = "penalty-score";
                break;
            case "own goal":
                $divclass = "own-goal";
                break;
            case "yellow card":
                $divclass = "yellow-card";
                break;
            case "red card":
                $divclass = "red-card";
                break;
            case "yellow to red":
                $divclass = "group-yellow-card";
                break;
            case "substutitions":
                $divclass = "substitute-player";
                break;
            default:
                break;
        }

        return $divclass;
    }
}

<?php
namespace model;

require_once __DIR__ . "/../bootstrap.php";
use Illuminate\Database\Eloquent\Model;

class Reviewer extends Model
{
    protected $table = "user_review";
    protected $fillable = ['name', 'path', 'accurate', 'created_at', 'updated_at'];
    public function comments()
    {
        return $this->hasMany(ReviewMatch::class, "id", "user_review_id");
    }

}

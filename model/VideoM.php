<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VideoM
 *
 * @author xsiwa
 */

include_once 'DBPDO.php';
include_once $_SERVER["DOCUMENT_ROOT"].'/utility/StringUtility.php';

class VideoM
{
    public function getAllVideoCountPage($page = 12,$videotype)
    {
        //cat:[other,thai,'premier league','la liga','bundesliga','serie a','league 1'
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                        CEILING(count(*)/" . $page . ") as maxPage
                   from 
                        video_highlight
                   where
                        `videotype`='".$videotype."'
                           ";
            $params = array();
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getAllVideo($offset, $limit,$videotype)
    {
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select * from video_highlight WHERE videotype='".$videotype."' order by video_highlight.`video_id` desc limit ? offset ?";
            $params = array($limit, $offset);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }


    public function getVideosByLastVideoGeneral(){
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select 
                        `video_id`,
                        `content`,
                        `title`,
                        `create_datetime`,
                        `videosource`,
                        `videotype`,
                        `video_tag`,
                        `desc`,
                        `thumbnail`,
                        `category`,
                        `pathFile`
                from 
                        video_highlight
                where
                      `videotype`='general'
                order by 
                video_id DESC 
                limit 1
                 ";
            $return = $dbpdo->query($sql);
            $return = $this->getUrlIframe($return);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getVideosByLastVideo(){
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select 
                        `video_id`,
                        `content`,
                        `title`,
                        `create_datetime`,
                        `videosource`,
                        `videotype`,
                        `video_tag`,
                        `desc`,
                        `thumbnail`,
                        `category`,
                        `pathFile`
                from 
                        video_highlight
                order by 
                video_id DESC 
                limit 1
                 ";
            $return = $dbpdo->query($sql);
            $return = $this->getUrlIframe($return);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getVideosById($id){
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select 
                        `video_id`,
                        `content`,
                        `title`,
                        `create_datetime`,
                        `videosource`,
                        `videotype`,
                        `videostyle`,
                        `video_tag`,
                        `desc`,
                        `thumbnail`,
                        `category`,
                        `pathFile`
                from 
                        video_highlight
                WHERE
                      `video_id`=?
                 ";
            $params[] = $id;
            $return = $dbpdo->query($sql, $params);
            $return = $this->getUrlIframe($return);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getVideosByIdMoreWeek($video_id,$type){
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select 
                        `video_id`,
                        `content`,
                        `title`,
                        `create_datetime`,
                        `videosource`,
                        `videotype`,
                        `video_tag`,
                        `desc`,
                        `thumbnail`
                from 
                        video_highlight
                WHERE
                      `video_id`!= $video_id 
                AND   
                      `videotype`= '$type'
                AND
                      `create_datetime` > DATE_SUB(NOW(), INTERVAL 1 WEEK)
                order by
                      `create_datetime` DESC
                 ";
            $return = $dbpdo->query($sql);
            $return = $this->getUrlIframe($return);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getVideosByIdMore($video_id,$limit){
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select 
                        `video_id`,
                        `content`,
                        `title`,
                        `create_datetime`,
                        `videosource`,
                        `videotype`,
                        `video_tag`,
                        `desc`,
                        `thumbnail`
                from 
                        video_highlight
                WHERE
                      `video_id`< $video_id
                order by
                      `create_datetime` DESC
                limit $limit
                 ";
            $return = $dbpdo->query($sql);
            $return = $this->getUrlIframe($return);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getSlideVideosCategoryMoreId($id,$type,$category="",$limit = 0)
    {
        //cat:[other,thai,'premier league','la liga','bundesliga','serie a','league 1'
        $dbpdo = new DBPDO();
        $return = array();
        $parames = array();
        $stringUtil = new StringUtility();

        if($category==""){
            $sqlcondition = " where video_id!=".$id;
        }else {
            $sqlcondition = " where video_id!=".$id." and category='" . $category . "'";
        }

        if (("" !== $type) and ("all" !== $type)) {
            if ($sqlcondition == "") {
                $sqlcondition .= " where videotype = '" . $type . "'";
            }else {
                $sqlcondition .= " and videotype = '" . $type . "'";
            }
        }

        $params[] = $limit;
        $sqlcondition .= " order by create_datetime DESC
                      limit ?";

        try {
            $dbpdo->connect();
            $sql = "select 
                        `video_id`,
                        `content`,
                        `title`,
                        `create_datetime`,
                        `videosource`,
                        `videotype`,
                        `video_tag`,
                        `desc`,
                        `thumbnail`,
                        `category`
                from 
                        video_highlight
                 " . $sqlcondition;
            //print_r($parames);

//            echo $sql;
//            exit;

            $return = $dbpdo->query($sql, $params);
            $return = $this->getUrlIframe($return);
//            for ($i = 0; $i < count($return); ++$i) {
//
//                $strKey = $stringUtil->getKeyFromVideoURL($return[$i]->videosource, $return[$i]->content);
//
//                $return[$i]->videokey = $strKey;
//            }


        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getCountSlideVideosCategoryByStyleAndDay($day="",$category="",$type = "",$tag="",$style="normal")
    {
        //cat:[other,thai,'premier league','la liga','bundesliga','serie a','league 1'
        $dbpdo = new DBPDO();
        $return = array();
        $parames = array();
        $stringUtil = new StringUtility();

        if($category==""){
            $sqlcondition = "";
        }else {
            $sqlcondition = " where category='" . $category . "'";
        }

        if (("" !== $type) and ("all" !== $type)) {
            if ($sqlcondition == "") {
                $sqlcondition .= " where videotype = '" . $type . "'";
            }else {
                $sqlcondition .= " and videotype = '" . $type . "'";
            }
        }

        if (("" !== $style) and ("all" !== $style)) {
            $sqlcondition .= " and videostyle = '" . $style . "'";
        }

        if($tag!=="") {
            $taglist = explode(",", $tag);
            foreach ($taglist as $key => $val) {
                if ($sqlcondition == "") {
                    $sqlcondition .= " where  video_tag like '%" . $val . "%'";
                } else {
                    $sqlcondition .= " or video_tag like '%" . $val . "%'";
                }
            }
        }

        if($day!==""){
            if ($sqlcondition !== "") {
                $sqlcondition.=" and ";
            }
            $sqlcondition.="video_highlight.create_datetime>=DATE_ADD(('".$day."'), INTERVAL -8 hour) and  video_highlight.create_datetime<=DATE_ADD(('".$day."')+ INTERVAL 1 DAY, INTERVAL -8 hour)";
        }

        $sqlcondition .= " order by create_datetime DESC";
        try {
            $dbpdo->connect();
            $sql = "select 
                        count(*) as total
                from 
                        video_highlight
                 " . $sqlcondition;
//            echo $sql;
//            exit;
            $return = $dbpdo->query($sql);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getSlideVideosCategoryByStyleAndDay($day="",$category="",$type = "", $limit = 0,$tag="",$style="normal",$offset=0)
    {

        //cat:[other,thai,'premier league','la liga','bundesliga','serie a','league 1'
        $dbpdo = new DBPDO();
        $return = array();
        $parames = array();
        $stringUtil = new StringUtility();

        if($category==""){
            $sqlcondition = "";
        }else {
            $sqlcondition = " where category='" . $category . "'";
        }

        if (("" !== $type) and ("all" !== $type)) {
            if ($sqlcondition == "") {
                $sqlcondition .= " where videotype = '" . $type . "'";
            }else {
                $sqlcondition .= " and videotype = '" . $type . "'";
            }
        }

        if (("" !== $style) and ("all" !== $style)) {
            $sqlcondition .= " and videostyle = '" . $style . "'";
        }

        if($day!==""){
            if ($sqlcondition !== "") {
                $sqlcondition.=" and ";
            }
            $sqlcondition.="video_highlight.create_datetime>=DATE_ADD(('".$day."'), INTERVAL -8 hour) and  video_highlight.create_datetime<=DATE_ADD(('".$day."')+ INTERVAL 1 DAY, INTERVAL -8 hour)";
        }

        if($tag!=="") {
            $taglist = explode(",", $tag);
            foreach ($taglist as $key => $val) {
                if ($sqlcondition == "") {
                    $sqlcondition .= " where  video_tag like '%" . $val . "%'";
                } else {
                    $sqlcondition .= " or video_tag like '%" . $val . "%'";
                }
            }
        }

        $params[] = $limit;
        $params[] = $offset;
        $sqlcondition .= " order by create_datetime DESC
                      limit ? offset ?";

        try {
            $dbpdo->connect();
            $sql = "select 
                        `video_id`,
                        `content`,
                        `title`,
                        `create_datetime`,
                        `videosource`,
                        `videotype`,
                        `video_tag`,
                        `desc`,
                        `thumbnail`,
                        `pathFile`
                from 
                        video_highlight
                 " . $sqlcondition;
            //print_r($parames);

//            echo $sql;
//            exit;

            $return = $dbpdo->query($sql, $params);
            $return = $this->getUrlIframe($return);
//            for ($i = 0; $i < count($return); ++$i) {
//
//                $strKey = $stringUtil->getKeyFromVideoURL($return[$i]->videosource, $return[$i]->content);
//
//                $return[$i]->videokey = $strKey;
//            }


        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getCountSlideVideosCategoryByStyle($category="",$type = "",$tag="",$style="normal")
    {

        //cat:[other,thai,'premier league','la liga','bundesliga','serie a','league 1'
        $dbpdo = new DBPDO();
        $return = array();
        $parames = array();
        $stringUtil = new StringUtility();

        if($category==""){
            $sqlcondition = "";
        }else {
            $sqlcondition = " where category='" . $category . "'";
        }

        if (("" !== $type) and ("all" !== $type)) {
            if ($sqlcondition == "") {
                $sqlcondition .= " where videotype = '" . $type . "'";
            }else {
                $sqlcondition .= " and videotype = '" . $type . "'";
            }
        }

        if (("" !== $style) and ("all" !== $style)) {
            $sqlcondition .= " and videostyle = '" . $style . "'";
        }

        if($tag!=="") {
            $taglist = explode(",", $tag);
            foreach ($taglist as $key => $val) {
                if ($sqlcondition == "") {
                    $sqlcondition .= " where  video_tag like '%" . $val . "%'";
                } else {
                    $sqlcondition .= " or video_tag like '%" . $val . "%'";
                }
            }
        }
        $sqlcondition .= " order by create_datetime DESC";
        try {
            $dbpdo->connect();
            $sql = "select 
                        count(*) as total
                from 
                        video_highlight
                 " . $sqlcondition;
            $return = $dbpdo->query($sql);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getSlideVideosCategoryByStyle($category="",$type = "", $limit = 0,$tag="",$style="normal",$offset=0)
    {

        //cat:[other,thai,'premier league','la liga','bundesliga','serie a','league 1'
        $dbpdo = new DBPDO();
        $return = array();
        $parames = array();
        $stringUtil = new StringUtility();

        if($category==""){
            $sqlcondition = "";
        }else {
            $sqlcondition = " where category='" . $category . "'";
        }

        if (("" !== $type) and ("all" !== $type)) {
            if ($sqlcondition == "") {
                $sqlcondition .= " where videotype = '" . $type . "'";
            }else {
                $sqlcondition .= " and videotype = '" . $type . "'";
            }
        }

        if (("" !== $style) and ("all" !== $style)) {
            $sqlcondition .= " and videostyle = '" . $style . "'";
        }

        if($tag!=="") {
            $taglist = explode(",", $tag);
            foreach ($taglist as $key => $val) {
                if ($sqlcondition == "") {
                    $sqlcondition .= " where  video_tag like '%" . $val . "%'";
                } else {
                    $sqlcondition .= " or video_tag like '%" . $val . "%'";
                }
            }
        }

        $params[] = $limit;
        $params[] = $offset;
        $sqlcondition .= " order by create_datetime DESC
                      limit ? offset ?";

        try {
            $dbpdo->connect();
            $sql = "select 
                        `video_id`,
                        `content`,
                        `title`,
                        `create_datetime`,
                        `videosource`,
                        `videotype`,
                        `video_tag`,
                        `desc`,
                        `thumbnail`,
                        `pathFile`
                from 
                        video_highlight
                 " . $sqlcondition;
            //print_r($parames);

//            echo $sql;
//            exit;

            $return = $dbpdo->query($sql, $params);
            $return = $this->getUrlIframe($return);
//            for ($i = 0; $i < count($return); ++$i) {
//
//                $strKey = $stringUtil->getKeyFromVideoURL($return[$i]->videosource, $return[$i]->content);
//
//                $return[$i]->videokey = $strKey;
//            }


        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getSlideVideosCategory($category="",$type = "", $limit = 0,$tag="")
    {
        //cat:[other,thai,'premier league','la liga','bundesliga','serie a','league 1'
        $dbpdo = new DBPDO();
        $return = array();
        $parames = array();
        $stringUtil = new StringUtility();

        if($category==""){
            $sqlcondition = "";
        }else {
            $sqlcondition = " where category='" . $category . "'";
        }

        if (("" !== $type) and ("all" !== $type)) {
            if ($sqlcondition == "") {
                $sqlcondition .= " where videotype = '" . $type . "'";
            }else {
                $sqlcondition .= " and videotype = '" . $type . "'";
            }
        }

        if($tag!=="") {
            $taglist = explode(",", $tag);
            foreach ($taglist as $key => $val) {
                if ($sqlcondition == "") {
                    $sqlcondition .= " where  video_tag like '%" . $val . "%'";
                } else {
                    $sqlcondition .= " or video_tag like '%" . $val . "%'";
                }
            }
        }

        $params[] = $limit;
        $sqlcondition .= " order by create_datetime DESC
                      limit ?";

        try {
            $dbpdo->connect();
            $sql = "select 
                        `video_id`,
                        `content`,
                        `title`,
                        `create_datetime`,
                        `videosource`,
                        `videotype`,
                        `video_tag`,
                        `desc`,
                        `thumbnail`
                from 
                        video_highlight
                 " . $sqlcondition;
            //print_r($parames);

//            echo $sql;
//            exit;

            $return = $dbpdo->query($sql, $params);
            $return = $this->getUrlIframe($return);
//            for ($i = 0; $i < count($return); ++$i) {
//
//                $strKey = $stringUtil->getKeyFromVideoURL($return[$i]->videosource, $return[$i]->content);
//
//                $return[$i]->videokey = $strKey;
//            }


        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getSlideVideosRandom($category="", $limit = 0)
    {
        $dbpdo = new DBPDO();
        $return = array();
        $parames = array();
        $stringUtil = new StringUtility();

        $sqlcondition = "";
//        if (("" !== $type) and ("all" !== $type)) {
            $sqlcondition = " where video_highlight.videotype='highlight' and video_highlight.create_datetime > DATE_SUB(NOW(), INTERVAL 1 month)";
//        }

        if($category!=="") {
            if ($sqlcondition == "") {
                $sqlcondition .= " where category = '" . $category . "'";
            } else {
                $sqlcondition .= " and category = '" . $category . "'";
            }
        }

        $params[] = $limit;
        $sqlcondition .= " order by video_id desc limit ?";

        try {
            $dbpdo->connect();
            $sql = "select 
                        `video_id`,
                        `content`,
                        `title`,
                        `create_datetime`,
                        `videosource`,
                        `videotype`,
                        `video_tag`,
                        `desc`,
                        `thumbnail`,
                        `category`
                from 
                        video_highlight
                 " . $sqlcondition;
            $return = $dbpdo->query($sql, $params);
            $return = $this->getUrlIframe($return);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getSlideVideos($type = "", $limit = 0,$tag="")
    {
        //cat:[other,thai,'premier league','la liga','bundesliga','serie a','league 1'
        $dbpdo = new DBPDO();
        $return = array();
        $parames = array();
        $stringUtil = new StringUtility();

        $sqlcondition = "";
        if (("" !== $type) and ("all" !== $type)) {
            $sqlcondition = " where  videotype = '" . $type . "'";
        }

        if($tag!=="") {
            if("" !== $type){
                $sqlcondition .= " and (";
            }
            $taglist = explode(",", $tag);
            foreach ($taglist as $key => $val) {
                if ($sqlcondition == "") {
                    $sqlcondition .= " where  video_tag like '%" . $val . "%'";
                } else {
                    if($key!=0) {
                        $sqlcondition .= " or video_tag like '%" . $val . "%'";
                    }else{
                        $sqlcondition .= "video_tag like '%" . $val . "%'";
                    }
                }
            }
            if("" !== $type){
                $sqlcondition .= ")";
            }
        }

        $params[] = $limit;
        $sqlcondition .= " order by Date(video_highlight.create_datetime) DESC ,video_highlight.pin DESC , video_highlight.create_datetime DESC
                      limit ?";

        try {
            $dbpdo->connect();
            $sql = "select 
                        `video_id`,
                        `content`,
                        `title`,
                        `create_datetime`,
                        `videosource`,
                        `videotype`,
                        `video_tag`,
                        `desc`,
                        `thumbnail`,
                        `category`,
                        `pathFile`,
                        `view`
                from 
                        video_highlight
                 " . $sqlcondition;
            //print_r($parames);

//            echo $sql;
//            exit;

            $return = $dbpdo->query($sql, $params);
            $return = $this->getUrlIframe($return);

//            for ($i = 0; $i < count($return); ++$i) {
//
//                $strKey = $stringUtil->getKeyFromVideoURL($return[$i]->videosource, $return[$i]->content);
//
//                $return[$i]->videokey = $strKey;
//            }


        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getSlideVideosMoreByStyle($id,$type = "", $limit = 0,$tag="",$style="normal")
    {
        //cat:[other,thai,'premier league','la liga','bundesliga','serie a','league 1'
        $dbpdo = new DBPDO();
        $return = array();
        $parames = array();
        $stringUtil = new StringUtility();
        $sqlcondition = " WHERE video_id != $id";

        if (("" !== $type) and ("all" !== $type)) {
            $sqlcondition .= " and videotype = '" . $type . "'";
        }

        if (("" !== $style) and ("all" !== $style)) {
            $sqlcondition .= " and videostyle = '" . $style . "'";
        }

        if($tag!=="") {
            $sqlcondition.=" and (";
            $taglist = explode(",", $tag);
            foreach ($taglist as $key => $val) {
                if ($key == 0) {
                    $sqlcondition .= " video_tag like '%" . $val . "%'";
                } else {
                    $sqlcondition .= " or video_tag like '%" . $val . "%'";
                }
            }
            $sqlcondition.=")";
        }

        $params[] = $limit;
        $sqlcondition .= " order by create_datetime DESC
                      limit ?";

        try {
            $dbpdo->connect();
            $sql = "select 
                        `video_id`,
                        `content`,
                        `title`,
                        `create_datetime`,
                        `videosource`,
                        `videotype`,
                        `video_tag`,
                        `desc`,
                        `thumbnail`,
                        `pathFile`
                from 
                        video_highlight
                 " . $sqlcondition;
            //print_r($parames);

//            echo $sql;
//            exit;

            $return = $dbpdo->query($sql, $params);
            $return = $this->getUrlIframe($return);
//            for ($i = 0; $i < count($return); ++$i) {
//
//                $strKey = $stringUtil->getKeyFromVideoURL($return[$i]->videosource, $return[$i]->content);
//
//                $return[$i]->videokey = $strKey;
//            }


        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getSlideVideosMore($id,$type = "", $limit = 0,$tag="")
    {
        //cat:[other,thai,'premier league','la liga','bundesliga','serie a','league 1'
        $dbpdo = new DBPDO();
        $return = array();
        $parames = array();
        $stringUtil = new StringUtility();
        $sqlcondition = " WHERE video_id != $id";

        if (("" !== $type) and ("all" !== $type)) {
            $sqlcondition .= " and videotype = '" . $type . "'";
        }

        if($tag!=="") {
            $sqlcondition.=" and (";
            $taglist = explode(",", $tag);
            foreach ($taglist as $key => $val) {
                if ($key == 0) {
                    $sqlcondition .= " video_tag like '%" . $val . "%'";
                } else {
                    $sqlcondition .= " or video_tag like '%" . $val . "%'";
                }
            }
            $sqlcondition.=")";
        }

        $params[] = $limit;
        $sqlcondition .= " order by create_datetime DESC
                      limit ?";

        try {
            $dbpdo->connect();
            $sql = "select 
                        `video_id`,
                        `content`,
                        `title`,
                        `create_datetime`,
                        `videosource`,
                        `videotype`,
                        `video_tag`,
                        `desc`,
                        `thumbnail`
                from 
                        video_highlight
                 " . $sqlcondition;
            //print_r($parames);

//            echo $sql;
//            exit;

            $return = $dbpdo->query($sql, $params);
            $return = $this->getUrlIframe($return);
//            for ($i = 0; $i < count($return); ++$i) {
//
//                $strKey = $stringUtil->getKeyFromVideoURL($return[$i]->videosource, $return[$i]->content);
//
//                $return[$i]->videokey = $strKey;
//            }


        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    function getUrlIframe($return)
    {
        $stringUtil = new StringUtility();
        for ($i = 0; $i < count($return); ++$i) {
            $strKey = $stringUtil->getKeyFromVideoURL($return[$i]->videosource, $return[$i]->content);
            if ($return[$i]->videosource == "dailymotion") {
                $return[$i]->videokey = $strKey;
                $return[$i]->stopurlIframe='http://www.dailymotion.com/embed/video/'.$return[$i]->videokey;
                $return[$i]->urlIframe = 'http://www.dailymotion.com/embed/video/' . $return[$i]->videokey . '?autoplay=1';
                if(!empty($return[$i]->thumbnail)){
                    $return[$i]->urlImg =$return[$i]->thumbnail;
                }else {
                    $return[$i]->urlImg = 'http://www.dailymotion.com/thumbnail/video/' . $return[$i]->videokey;
                }
            } elseif ($return[$i]->videosource == "twitter") {
                $dataUrl = explode('/', $return[$i]->content);
                //index =5 url
                $return[$i]->videokey = $dataUrl[5];
//                $return[$i]->urlIframe = 'https://twitter.com/i/cards/tfw/v1/' . $return[$i]->videokey . '?autoplay=1';
                $return[$i]->urlIframe=$return[$i]->content;
                $return[$i]->stopUrlIframe='https://twitter.com/i/cards/tfw/v1/'.$return[$i]->videokey;
                if(!empty($return[$i]->thumbnail)){
                    $return[$i]->urlImg =$return[$i]->thumbnail;
                }else {
                    $return[$i]->urlImg = '/images/icon/twitter-video.png';
                }
            } elseif ($return[$i]->videosource == "facebook") {
                $dataUrl = explode('/', $return[$i]->content);
                //index =5 url
                $return[$i]->videokey = $dataUrl[5];
                $return[$i]->urlIframe = 'https://www.facebook.com/v2.3/plugins/video.php?autoplay=true&href=' . $return[$i]->content;
                $return[$i]->stopUrlIframe='https://www.facebook.com/v2.3/plugins/video.php?allowfullscreen=true&container_width=800&href='.$return[$i]->content;
                if(!empty($return[$i]->thumbnail)){
                    $return[$i]->urlImg =$return[$i]->thumbnail;
                }else {
                    $return[$i]->urlImg = 'https://graph.facebook.com/' . $return[$i]->videokey . '/picture';
                }
            } else if($return[$i]->videosource == "youtube") {
                $return[$i]->videokey = $strKey;
                $return[$i]->urlIframe = 'https://www.youtube.com/embed/' . $return[$i]->videokey . '?autoplay=1';
                $return[$i]->stopUrlIframe='https://www.youtube.com/embed/'.$return[$i]->videokey;
                if(!empty($return[$i]->thumbnail)){
                    $return[$i]->urlImg =$return[$i]->thumbnail;
                }else {
                    $return[$i]->urlImg = 'https://img.youtube.com/vi/' . $return[$i]->videokey . '/1.jpg;';
                }
            }else if($return[$i]->videosource == "streamable"){
                $return[$i]->videokey = $strKey;
                if(!empty($return[$i]->pathFile)) {
                    $return[$i]->urlIframe = $return[$i]->pathFile;
                }else {
                    $return[$i]->urlIframe = 'https://streamable.com/s/' . $return[$i]->videokey . '?autoplay=1';
                }
                $return[$i]->stopUrlIframe=$return[$i]->videokey;
                if(!empty($return[$i]->thumbnail)){
                    $return[$i]->urlImg =$return[$i]->thumbnail;
                }else {
                    $return[$i]->urlImg = 'https://cf-e2.streamablevideo.com/image/' . $return[$i]->videokey . '_1.jpg';
                }
            }
            $return[$i]->desc=str_replace('”','',str_replace('“','',str_replace('"','',str_replace("'","",$return[$i]->desc))));
            $return[$i]->title=str_replace('”','',str_replace('“','',str_replace('"','',str_replace("'","",$return[$i]->title))));
        }
        return $return;
    }

    function getRelatedVideoByType($type,$tag, $limit = 6)

    {
        $dbpdo = new DBPDO();
        $return = array();
        $parames = array($limit);
        $taglist = explode(",", $tag);
        $condition="";

        if (("" !== $type) and ("all" !== $type)) {
            $condition = " where  videotype = '" . $type . "'";
        }

        if($tag!=="") {
            if("" !== $type){
                $condition .= " and (";
            }
            $taglist = explode(",", $tag);
            foreach ($taglist as $key => $val) {
                if ($condition == "") {
                    $condition .= " where  video_tag like '%" . $val . "%'";
                } else {
                    if($key!=0) {
                        $condition .= " or video_tag like '%" . $val . "%'";
                    }else{
                        $condition .= "video_tag like '%" . $val . "%'";
                    }
                }
            }
            if("" !== $type){
                $condition .= ")";
            }
        }

        try {
            $dbpdo->connect();
            $sql = "SELECT     vh.`video_id`,
                        vh.`content`,
                        vh.`title`,
                        vh.`create_datetime`,
                        vh.`videosource`,
                        vh.`videotype`,
                        vh.`video_tag`,
                        vh.`desc`,
                        vh.`thumbnail`
            FROM video_highlight vh

	              $condition
            ORDER BY vh.create_datetime DESC
            LIMIT ?";

            $return = $dbpdo->query($sql, $parames);
            $return = $this->getUrlIframe($return);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $return;
    }

    function getRelatedVideo($tag, $limit = 6)

    {
        $dbpdo = new DBPDO();
        $return = array();
        $parames = array($limit);
        $taglist = explode(",", $tag);
        $condition = " WHERE vh.`video_tag` LIKE '%%'";
        foreach ($taglist as $key => $val) {
            if ($key == 0) {
                $condition = " WHERE vh.`video_tag` LIKE '%$val%' ";
            } else {
                $condition .= " OR vh.`video_tag` LIKE '%$val%' ";
            }
        }
        try {
            $dbpdo->connect();
            $sql = "SELECT     vh.`video_id`,
                        vh.`content`,
                        vh.`title`,
                        vh.`create_datetime`,
                        vh.`videosource`,
                        vh.`videotype`,
                        vh.`video_tag`,
                        vh.`desc`,
                        vh.`thumbnail`
            FROM video_highlight vh

	              $condition
            ORDER BY vh.create_datetime DESC
            LIMIT ?";


            $return = $dbpdo->query($sql, $parames);
            $return = $this->getUrlIframe($return);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $return;
    }

    function getRelatedVideoMore($id,$tag, $limit = 6)

    {
        $dbpdo = new DBPDO();
        $return = array();
        $parames = array($limit);
        $taglist = explode(",", $tag);
        $condition = " WHERE vh.`video_id` != $id and (";
        foreach ($taglist as $key => $val) {
            if ($key == 0) {
                $condition .= " vh.`video_tag` LIKE '%$val%' ";
            } else {
                $condition .= " OR vh.`video_tag` LIKE '%$val%' ";
            }
        }
        $condition .= ")";
        try {
            $dbpdo->connect();
            $sql = "SELECT     vh.`video_id`,
                        vh.`content`,
                        vh.`title`,
                        vh.`create_datetime`,
                        vh.`videosource`,
                        vh.`videotype`,
                        vh.`video_tag`,
                        vh.`desc`,
                        vh.`thumbnail`,
                        vh.`category`,
                        vh.`pathFile`
            FROM video_highlight vh

	              $condition
            ORDER BY vh.create_datetime DESC
            LIMIT ?";
            $return = $dbpdo->query($sql, $parames);
            $return = $this->getUrlIframe($return);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $return;

    }

    function getRelatedVideoMoreByVideoType($id,$type, $limit = 6)

    {
        $dbpdo = new DBPDO();
        $return = array();
        $parames = array($limit);
        $condition = " WHERE vh.`video_id` != ".$id ;
        if (("" !== $type) and ("all" !== $type)) {
            $condition .= " and videotype = '" . $type . "'";
        }
        try {
            $dbpdo->connect();
            $sql = "SELECT     vh.`video_id`,
                        vh.`content`,
                        vh.`title`,
                        vh.`create_datetime`,
                        vh.`videosource`,
                        vh.`videotype`,
                        vh.`video_tag`,
                        vh.`desc`,
                        vh.`thumbnail`,
                        vh.`category`,
                        vh.`pathFile`
            FROM video_highlight vh

	              $condition
            ORDER BY vh.create_datetime DESC
            LIMIT ?";
            $return = $dbpdo->query($sql, $parames);
            $return = $this->getUrlIframe($return);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $return;
    }

}

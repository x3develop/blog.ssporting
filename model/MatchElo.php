<?php
namespace model;

require_once __DIR__ . "/../bootstrap.php";
use Illuminate\Database\Eloquent\Model;

class MatchElo extends Model
{
    protected $table = "match";
    protected $fillable = ['match_7m_id', 'league_id', 'team_home', 'team_away', 'status', 'time_match', 'state', 'live_score_home', 'live_score_away', 'ht_start', 'lastresult_home', 'lastresult_away', 'rastresult_odds_home', 'lastresult_odds_away', 'created_at', 'updated_at'];

    public function events()
    {
        return $this->hasMany(MatchEvent::class, "id", "mid");
    }
    public function hdphistory()
    {
        return $this->hasMany(HdpHistory::class, "id", "mid");
    }
    public function lineups()
    {
        return $this->hasMany(LineUp::class, "id", "mid");
    }

    public function league()
    {
        return $this->BelongsTo(LeagueElo::class, "league_id", "id");
    }

    public function teama()
    {
        return $this->hasOne(TeamElo::class, "id", "team_home");
    }
    public function teamb()
    {
        return $this->hasOne(TeamElo::class, "id", "team_away");
    }

    public function reviews()
    {
        return $this->hasMany(ReviewMatch::class, "match_id", "id");
    }

    public function statistics()
    {
        return $this->hasMany(MatchStat::class, "mid", "id");
    }

    public function hdp()
    {
        return $this->hasOne(HdpElo::class, "match_id", "id");
    }

    public function bets()
    {
        return $this->hasMany(BetElo::class, "match_id", "id");
    }

}

<?php

/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 16/11/2559
 * Time: 14:40 น.
 */
class DBPDO
{
    //pass90.com
//    private $servername = "eeball.com";
//    private $username = "dev";
//    private $password = "@x1234";
//    private $dbname = "dev_ssporting_swcp";
//    private $conn = null;
//    private $stmt = null;

//    private $servername = "localhost";
//    private $username = "root";
//    private $password = "50179042";
//    private $dbname = "play";
//    private $conn = null;
//    private $stmt = null;

    private $servername = "159.65.143.250";
    private $username = "root";
    private $password = "@x1234";
    private $dbname = "ngoal";
    private $conn = null;
    private $stmt = null;
    //eeball
    private $servername2 = "159.65.143.250";
    private $username2 = "root";
    private $password2 = "@x1234";
    private $dbname2 = "ngoal";

    function connect()
    {
        try {
            $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
            // set the PDO error mode to exception
//            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $this->conn->exec("SET NAMES UTF8");
            $this->conn->exec("SET profiling = 1;");

        } catch (Exception $e) {
            echo "Connection failed: " . $e->getMessage();
        }
        return $this->conn;
    }

    function connect2()
    {
        try {
            $this->conn = new PDO("mysql:host=$this->servername2;dbname=$this->dbname2", $this->username2, $this->password2);
            // set the PDO error mode to exception
//            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $this->conn->exec("SET NAMES UTF8");
            $this->conn->exec("SET profiling = 1;");

        } catch (Exception $e) {
            echo "Connection failed: " . $e->getMessage();
        }
        return $this->conn;
    }

    function close()
    {
        try {
            $this->stmt = null;
            $this->conn = null;
        } catch (Exception $e) {
            echo "Connection failed: " . $e->getMessage();
        } finally {
//            $this->conn = null;
        }
    }

    function  query($sql, $params)
    {

        try {
            $this->stmt = $this->conn->prepare($sql);
//            if (count($params) == 0) {
//                $this->stmt->execute();
//            } else {
//            $this->stmt->errorInfo();
            $this->stmt->execute($params);
//            $this->stmt->debugDumpParams();
//            exit;
//            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $this->stmt->fetchAll(PDO::FETCH_OBJ);
    }

    function insert($sql, $arr)
    {
        //sql pattern is INSERT INTO fruit(name, colour) VALUES (?, ?)
        $success = 0;
        try {
            $this->stmt = $this->conn->prepare($sql);
            $success = $this->stmt->execute($arr);
            $last_id = $this->conn->lastInsertId();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $last_id;
    }

    function update($sql, $arr)
    {
        //sql pattern is INSERT INTO fruit(name, colour) VALUES (?, ?)
        $success = 0;
        $affectedrows = 0;
        try {
            $this->stmt = $this->conn->prepare($sql);
            $success = $this->stmt->execute($arr);
            $affectedrows = $this->stmt->rowCount();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $affectedrows;
    }

}
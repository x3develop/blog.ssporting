<?php

namespace model;

require_once __DIR__ . "/../bootstrap.php";

use Illuminate\Database\Eloquent\Model;

class BetElo extends Model
{
    protected $table = "bet";
    protected $fillable = ['user_id', 'match_id', 'team', 'use_bet_coin', 'use_bet_coupon', 'status', 'hdp', 'reward', 'tid', 'created_at', 'updated_at'];

    public function match()
    {
        return $this->BelongsTo(MatchElo::class, "match_id", "id");
    }

}

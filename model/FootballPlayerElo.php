<?php
namespace model;

require_once __DIR__ . "/../bootstrap.php";
use Illuminate\Database\Eloquent\Model;

class FootbalPlayerElo extends Model
{
    protected $table = "player";
    protected $fillable = ['name', 'path', 'tid', 'name_7m', 'created_at', 'updated_at'];

    public function getLogo()
    {
        $path = "/images/avatar.png";
        if (!empty($this->path) && file_exists($_SERVER['DOCUMENT_ROOT'].$this->path)) {
            $path = $this->path;
        }
        return $path;
    }

}

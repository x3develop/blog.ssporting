<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 2/2/2017
 * Time: 11:26
 */
include_once "DBPDO.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/FileManager.php";
use Carbon\Carbon;

class Statistics
{
    public function getGameHistory($f24_mid,$type,$date)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($f24_mid,$type,$date);
        $sql = "select away.teamNameTh as awaynameth,away.teamNameEn as awaynameen,home.teamNameTh as homenameth,away.tid as awayitd,home.teamNameEn as homenameen,home.tid as hometid,
lang_league.leagueNameTh,lang_league.leagueNameEn,game_history.home_score,game_history.away_score,game_history.ht_score,
game_history.odds,game_history.result,game_history.odds_result,game_history.show_date from matches_map 
inner join game_history on matches_map.o7m_mid=game_history.game_mid
inner join lang_league on lang_league.id7m=game_history.id7m_lid
inner join lang_team home on home.id7m=game_history.id7m_hid
inner join lang_team away on away.id7m=game_history.id7m_gid
where matches_map.f24_mid=? and game_history.`type`=? and game_history.show_date>=? and game_history.odds_result!=-1  order by game_history.show_date desc";
//        echo $sql;
//        exit;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    public function getTotalGameHistory($f24_mid,$type,$date){
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($f24_mid,$type,$date);
        $sql = "select COUNT(*) as total,
COUNT(IF(game_history.result=0,1,null)) as wincount,
COUNT(IF(game_history.result=1,1,null)) as drowcount,
COUNT(IF(game_history.result=2,1,null)) as losscount,
ROUND(100-(((COUNT(*)-COUNT(IF(game_history.result=0,1,null)))/COUNT(*))*100)) as percentwin,
ROUND(100-(((COUNT(*)-COUNT(IF(game_history.result=1,1,null)))/COUNT(*))*100)) as percentdraw,
ROUND(100-(((COUNT(*)-COUNT(IF(game_history.result=2,1,null)))/COUNT(*))*100)) as percentloss,
COUNT(IF(game_history.odds_result=0 || game_history.odds_result=1,1,null)) as winoddscount,
COUNT(IF(game_history.odds_result=2,1,null)) as drowoddscount,
COUNT(IF(game_history.odds_result=3 || game_history.odds_result=4,1,null)) as lossoddscount,
ROUND(100-(((COUNT(*)-COUNT(IF(game_history.odds_result=0 || game_history.odds_result=1,1,null)))/COUNT(*))*100)) as percentoddswin,
ROUND(100-(((COUNT(*)-COUNT(IF(game_history.odds_result=2,1,null)))/COUNT(*))*100)) as percentoddsdraw,
ROUND(100-(((COUNT(*)-COUNT(IF(game_history.odds_result=3 || game_history.odds_result=4,1,null)))/COUNT(*))*100)) as percentoddsloss
from matches_map
inner join game_history on matches_map.o7m_mid=game_history.game_mid
where matches_map.f24_mid=? and game_history.`type`=? and game_history.show_date>=? and game_history.odds_result!=-1 order by game_history.show_date desc";
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 15/2/2560
 * Time: 14:05 น.
 */
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/DBPDO.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";

class RankM
{
    function getGpRank($offset = 0, $limit = 5)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($offset, $limit);
        $sql = "SELECT gr.*,fb.display_name,fb.pts,fb.w,lastResult,gr.`rank` as `current_rank`, gr.`overall_gp` as `indicator` FROM `gp_statistic` gr  LEFT JOIN `facebook_user` fb ON gr.fb_uid=fb.fb_uid
WHERE  gr.`update_at`=CURRENT_DATE ORDER BY `rank` LIMIT ?,?";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getGpPeriodRank($offset = 0, $limit = 5)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $dt = \Carbon\Carbon::now();
        if ($dt->day < 17) {
            $dt->day(1);
        } else {
            $dt->day(16);
        }
        $params = array($dt->format("Y-m-d"), $offset, $limit);
        $sql = "SELECT gr.*,fb.display_name,fb.pts,fb.w,lastResult,gr.`rank` as `current_rank`, gr.`overall_gp` as `indicator` FROM `gp_statistic` gr
LEFT JOIN `facebook_user` fb ON gr.fb_uid=fb.fb_uid
WHERE  gr.`update_at`=?
ORDER BY `rank` limit ?,?";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getGpAllTimeRank($offset = 0, $limit = 5)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($offset, $limit);
        $sql = "SELECT gr.*,fb.display_name,fb.pts,fb.w,lastResult,gr.`all_rank` as `current_rank`, gr.`real_gp` as `indicator` FROM `gp_statistic` gr  LEFT JOIN `facebook_user` fb ON gr.fb_uid=fb.fb_uid
WHERE  gr.`update_at`=CURRENT_DATE ORDER BY `all_rank` LIMIT ?,?";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getGoldRank($offset = 0, $limit = 5)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($offset, $limit);
        $sql = "SELECT ss.*,fb.display_name,fb.pts,fb.w,lastResult,ss.`rank` as `current_rank`,ss.`overall_sgold` as `indicator` FROM sgold_play_statistic ss LEFT JOIN facebook_user fb ON ss.fb_uid=fb.fb_uid
WHERE ss.update_at=CURRENT_DATE ORDER BY `rank` LIMIT ?,?";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getGoldPeriodRank($offset = 0, $limit = 5)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $dt = \Carbon\Carbon::now();
        if ($dt->day < 17) {
            $dt->day(1);
        } else {
            $dt->day(16);
        }
        $params = array($dt->format("Y-m-d"), $offset, $limit);
        $sql = "SELECT ss.*,fb.display_name,fb.pts,fb.w,lastResult, ss.`rank` as `current_rank`,ss.`overall_sgold` as `indicator` FROM sgold_play_statistic ss LEFT JOIN facebook_user fb ON ss.fb_uid=fb.fb_uid
WHERE ss.update_at=? ORDER BY `rank` LIMIT ?,?";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getGoldAllTimeRank($offset = 0, $limit = 5)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($offset, $limit);
        $sql = "SELECT ss.*,fb.display_name,fb.pts,fb.w,lastResult, ss.`all_sgold_rank` as `current_rank`,ss.`user_sgold` as `indicator` FROM sgold_play_statistic ss LEFT JOIN facebook_user fb ON ss.fb_uid=fb.fb_uid
WHERE ss.update_at=CURRENT_DATE ORDER BY `all_sgold_rank` LIMIT ?,?";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getLeagueRank($league = 0, $offset = 0, $limit = 5)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($league,$offset, $limit);
        $sql = "SELECT lr.*,fu.uid,fu.display_name,lr.`rank` as `current_rank`, lr.`overall_gp` as `indicator` FROM league_ranking lr
LEFT JOIN facebook_user fu ON fu.fb_uid=lr.fb_uid
WHERE lr._lid=?
AND lr.update_at=CURRENT_DATE
ORDER BY lr.rank
LIMIT ?,?";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getUserRankPack($offset = 0, $limit = 5)
    {
        $result = array("gp" => array("alltime" => [], "period" => [], "now" => []), "gold" => array("alltime" => [], "period" => [], "now" => []), "league" => array("thai_premier_league" => [], "english_premier_league" => [], "spain_laliga" => [], "german_bundesliga" => [], "italy_seriea" => [], "france_ligue1" => [], "uefa_champion_league" => [], "europa_league" => []), "keylist" => array("gp" => [], "gold" => [], "league" => []));
        $gprank = $this->getGpRank($offset, $limit);
        $gpperiodrank = $this->getGpPeriodRank($offset, $limit);
        $gpalltimerank = $this->getGpAllTimeRank($offset, $limit);
        $goldrank = $this->getGoldRank($offset, $limit);
        $goldpperiodrank = $this->getGoldPeriodRank($offset, $limit);
        $goldalltimerank = $this->getGoldAllTimeRank($offset, $limit);
        $gpkeycount = 0;
        $goldcount = 0;

        foreach ($gprank as $k => $v) {
            foreach ($v as $key => $val) {
                $result["gp"]["now"][$k][] = $val;

                if ($k == 0) {
                    $result["keylist"]["gp"][$key] = $gpkeycount++;
                }
            }
        }
        $gpkeycount = 0;
        $goldcount = 0;
        foreach ($gpperiodrank as $k => $v) {
            foreach ($v as $key => $val) {
                $result["gp"]["period"][$k][] = $val;

                if ($k == 0) {
                    $result["keylist"]["gp"][$key] = $gpkeycount++;
                }
            }
        }
        $gpkeycount = 0;
        $goldcount = 0;
        foreach ($gpalltimerank as $k => $v) {
            foreach ($v as $key => $val) {
                $result["gp"]["alltime"][$k][] = $val;

                if ($k == 0) {
                    $result["keylist"]["gp"][$key] = $gpkeycount++;
                }
            }
        }

        $gpkeycount = 0;
        $goldcount = 0;
        foreach ($goldrank as $k => $v) {
            foreach ($v as $key => $val) {
                $result["gold"]["now"][$k][] = $val;

                if ($k == 0) {
                    $result["keylist"]["gold"][$key] = $goldcount++;
                }
            }
        }
        $gpkeycount = 0;
        $goldcount = 0;
        foreach ($goldpperiodrank as $k => $v) {
            foreach ($v as $key => $val) {
                $result["gold"]["period"][$k][] = $val;

                if ($k == 0) {
                    $result["keylist"]["gold"][$key] = $goldcount++;
                }
            }
        }
        $gpkeycount = 0;
        $goldcount = 0;
        foreach ($goldalltimerank as $k => $v) {
            foreach ($v as $key => $val) {
                $result["gold"]["alltime"][$k][] = $val;

                if ($k == 0) {
                    $result["keylist"]["gold"][$key] = $goldcount++;
                }
            }
        }

        $mainleague = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/system_config/main_league_id.json"), true);
        foreach ($mainleague as $name => $lid) {
            $rankleague = $this->getLeagueRank($lid, $offset, $limit);
            $keycount = 0;
            foreach ($rankleague as $k => $v) {
                foreach ($v as $key => $val) {
                    $result["league"][$name][$k][] = $val;

                    if ($k == 0) {
                        $result["keylist"]["league"][$key] = $keycount++;
                    }
                }
            }

        }

        return $result;
    }

}
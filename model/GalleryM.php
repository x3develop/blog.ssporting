<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 11/29/2016
 * Time: 16:34
 */
include_once 'DBPDO.php';
include_once $_SERVER["DOCUMENT_ROOT"].'/utility/StringUtility.php';
class GalleryM
{
    function getVideosByLastVideo(){

    }


    function getOthersVideo($limit = 6)
    {
        $dbpdo = new DBPDO();
        $return = array();
        $parames = array($limit);
        try {
            $dbpdo->connect();
            $sql = "SELECT     vg.`id`,
                        vg.`desc`,
                        vg.`title`,
                        vg.`update_at`,
                        vg.`tag`,
                        sg.`path`,
                        sg.`typeVideo`,
                        sg.`thumbnail`,
                        sg.`created_at` as `create_datetime`
            FROM media_gallery vg
            inner join media_store sg on sg.gall_id=vg.id
            WHERE vg.`gall_type`='video'
            ORDER BY vg.update_at DESC
            LIMIT ?";
            $return = $dbpdo->query($sql, $parames);
            $return = $this->getUrlIframe($return);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $return;
    }

    function getRelatedVideo($tag,$id, $limit = 6)
    {
        $dbpdo = new DBPDO();
        $return = array();
        $parames = array($limit);
        $taglist = explode(",", $tag);
        $condition = " WHERE vg.id!=".$id." AND vg.`gall_type`='video' AND vg.`tag` LIKE '%%'";
        if(!empty($tag)) {
            foreach ($taglist as $key => $val) {
                if ($key == 0) {
                    $condition = " WHERE vg.id!=".$id." AND  vg.`gall_type`='video' AND (vg.`tag` LIKE '%$val%' ";
                } else {
                    $condition .= " OR vg.`tag` LIKE '%$val%' ";
                }
            }
            if (!empty($taglist)) {
                $condition .= ")";
            }
        }
        try {
            $dbpdo->connect();
            $sql = "SELECT     vg.`id`,
                        vg.`desc`,
                        vg.`title`,
                        vg.`update_at`,
                        vg.`tag`,
                        sg.`path`,
                        sg.`typeVideo`,
                        sg.`thumbnail`,
                        sg.`created_at` as `create_datetime`
            FROM media_gallery vg
            inner join media_store sg on sg.gall_id=vg.id
	              $condition
            ORDER BY vg.update_at DESC
            LIMIT ?";
            $return = $dbpdo->query($sql, $parames);
            $return = $this->getUrlIframe($return);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $return;
    }

    public function getMediaGalleryVideoRandom($limit){
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select media_gallery.id,media_gallery.update_at,media_gallery.title,media_store.thumbnail,media_store.path,media_store.`desc`,media_store.typeVideo from media_gallery
left join media_store on media_store.gall_id=media_gallery.id where media_gallery.update_at > DATE_SUB(NOW(), INTERVAL 1 month) and media_gallery.gall_type='video' order by RAND() DESC limit ".$limit;
            $params = array();
            $return = $dbpdo->query($sql, $params);
            $return=$this->getUrlIframe($return);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getMediaGalleryVideo($limit){
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select media_gallery.id,media_gallery.update_at,media_gallery.title,media_store.thumbnail,media_store.path,media_store.`desc`,media_store.typeVideo from media_gallery
left join media_store on media_store.gall_id=media_gallery.id where media_gallery.gall_type='video' order by media_gallery.update_at DESC limit ".$limit;
            $params = array();
            $return = $dbpdo->query($sql, $params);
            $return=$this->getUrlIframe($return);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getMediaStoreByGalleryId($id)
    {
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                        media_store.`path`,
                        media_store.`desc` as storeDesc,
                        media_store.`thumbnail`,
                        media_store.`typeVideo`,
                        media_gallery.tag,
                        media_gallery.`id`,
                        media_gallery.`title` as title,
                        media_gallery.`desc` as `desc`,
                        media_gallery.`created_at` as `create_datetime`
                   from 
                        media_store
                        inner join media_gallery on media_gallery.id=media_store.gall_id
                   WHERE 
                        gall_id=?
                        ";
            $params = array($id);
            $return = $dbpdo->query($sql, $params);
            $return=$this->getUrlIframe($return);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getMediaStoreByNotId($id)
    {
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select media_gallery.id,media_store.`path`, media_store.`desc`, media_store.`thumbnail`, media_store.`typeVideo`, media_gallery.tag 
                      from media_store inner join media_gallery on media_gallery.id=media_store.gall_id 
                      WHERE media_gallery.id<? and media_gallery.gall_type='video' order by media_gallery.id DESC";
            $params = array($id);
            $return = $dbpdo->query($sql, $params);
            $return=$this->getUrlIframe($return);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getMediaStoreByGalleryIdWeek($id)
    {
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select media_gallery.id,media_store.`path`, media_store.`desc`, media_store.`thumbnail`, media_store.`typeVideo`, media_gallery.tag 
                      from media_store inner join media_gallery on media_gallery.id=media_store.gall_id 
                      WHERE media_gallery.id!=? and media_gallery.gall_type='video'
                      AND media_gallery.`update_at` > DATE_SUB(NOW(), INTERVAL 1 WEEK)";
            $params = array($id);
            $return = $dbpdo->query($sql, $params);
            $return=$this->getUrlIframe($return);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    function getUrlIframe($return)
    {
        $stringUtil = new StringUtility();
        for ($i = 0; $i < count($return); ++$i) {
            $strKey = $stringUtil->getKeyFromVideoURL($return[$i]->typeVideo, $return[$i]->path);
            if ($return[$i]->typeVideo == "dailymotion") {
                $return[$i]->videokey = $strKey;
                $return[$i]->stopurlIframe='http://www.dailymotion.com/embed/video/'.$return[$i]->videokey;
                $return[$i]->urlIframe = 'http://www.dailymotion.com/embed/video/' . $return[$i]->videokey . '?autoplay=1';
                if(!empty($return[$i]->thumbnail)){
                    $return[$i]->urlImg =$return[$i]->thumbnail;
                }else {
                    $return[$i]->urlImg = 'http://www.dailymotion.com/thumbnail/video/' . $return[$i]->videokey;
                }
            } elseif ($return[$i]->typeVideo == "twitter") {
                $dataUrl = explode('/', $return[$i]->path);
                //index =5 url
                $return[$i]->videokey = $dataUrl[5];
                $return[$i]->urlIframe = 'https://twitter.com/i/cards/tfw/v1/' . $return[$i]->videokey . '?autoplay=1';
                $return[$i]->stopUrlIframe='https://twitter.com/i/cards/tfw/v1/'.$return[$i]->videokey;
                if(!empty($return[$i]->thumbnail)){
                    $return[$i]->urlImg =$return[$i]->thumbnail;
                }else {
                    $return[$i]->urlImg = '/images/icon/twitter-video.png';
                }
            }else if($return[$i]->typeVideo == "streamable"){
                $return[$i]->videokey = $strKey;
                $return[$i]->urlIframe = 'https://streamable.com/s/'.$return[$i]->videokey . '?autoplay=1';
                $return[$i]->stopUrlIframe='https://streamable.com/s/'.$return[$i]->videokey;
                if(!empty($return[$i]->thumbnail)){
                    $return[$i]->urlImg =$return[$i]->thumbnail;
                }else {
                    $return[$i]->urlImg = 'https://cf-e2.streamablevideo.com/image/' . $return[$i]->videokey . '.jpg';
                }
            } elseif ($return[$i]->typeVideo == "facebook") {
                $dataUrl = explode('/', $return[$i]->path);
                //index =5 url
                $return[$i]->videokey = $dataUrl[5];
                $return[$i]->urlIframe = 'https://www.facebook.com/v2.3/plugins/video.php?autoplay=true&href=' . $return[$i]->path;
                $return[$i]->stopUrlIframe='https://www.facebook.com/v2.3/plugins/video.php?allowfullscreen=true&container_width=800&href='.$return[$i]->path;
                if(!empty($return[$i]->thumbnail)){
                    $return[$i]->urlImg =$return[$i]->thumbnail;
                }else {
                    $return[$i]->urlImg = 'https://graph.facebook.com/' . $return[$i]->videokey . '/picture';
                }
            } else {
                $return[$i]->videokey = $strKey;
                $return[$i]->urlIframe = 'https://www.youtube.com/embed/' . $return[$i]->videokey . '?autoplay=1';
                $return[$i]->stopUrlIframe='https://www.youtube.com/embed/'.$return[$i]->videokey;
                if(!empty($return[$i]->thumbnail)){
                    $return[$i]->urlImg =$return[$i]->thumbnail;
                }else {
                    $return[$i]->urlImg = 'https://img.youtube.com/vi/' . $return[$i]->videokey . '/1.jpg;';
                }
            }
            $return[$i]->desc=str_replace('”','',str_replace('“','',str_replace('"','',str_replace("'","",$return[$i]->desc))));
            $return[$i]->title=str_replace('”','',str_replace('“','',str_replace('"','',str_replace("'","",$return[$i]->title))));
        }
        return $return;
    }

    public function getByGalleryId($id)
    {
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                        media_gallery.*,media_store.`thumbnail`
                   from 
                          media_gallery
                   inner join 
                          media_store
                   on 
                          media_gallery.id=media_store.gall_id
                   WHERE 
                        media_gallery.id=?
                        ";
            $params = array($id);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getAllGalleryCountPage($page = 12)
    {
        //cat:[other,thai,'premier league','la liga','bundesliga','serie a','league 1'
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                        CEILING(count(*)/" . $page . ") as maxPage
                   from 
                        media_gallery
                   where
                        `group`='sexy'
                           ";
            $params = array();
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getAllGalleryPicture($offset, $limit)
    {
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                        media_gallery.`id`,
                        media_gallery.`title`,
                        media_gallery.`update_at`,
                        media_gallery.`desc`,
                        media_gallery.`gall_type`,
                        media_gallery.`view`,
                        media_store.`path`,
                        media_store.`thumbnail`
                   from 
                          media_gallery
                   inner join 
                          media_store
                   on 
                          media_gallery.id=media_store.gall_id
                   WHERE
                        media_gallery.`group`='sexy' 
                        AND media_gallery.`gall_type`='picture'
                        AND media_gallery.`remove`='N'
                   group by id
                   order by 
                           media_gallery.`id` desc
                           limit ? offset ?";

//            echo $sql;exit;
            $params = array($limit, $offset);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getAllGallery($offset, $limit)
    {
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                        media_gallery.`id`,
                        media_gallery.`title`,
                        media_gallery.`update_at`,
                        media_gallery.`desc`,
                        media_gallery.`gall_type`,
                        media_gallery.`view`,
                        media_store.`path`,
                        media_store.`thumbnail`
                   from 
                          media_gallery
                   inner join 
                          media_store
                   on 
                          media_gallery.id=media_store.gall_id
                   WHERE
                        media_gallery.`group`='sexy' 
                        AND (media_gallery.`gall_type`='picture' OR media_gallery.`gall_type`='video')
                        AND media_gallery.`remove`='N'
                   group by id
                   order by 
                           media_gallery.`id` desc
                           limit ? offset ?";

//            echo $sql;exit;
            $params = array($limit, $offset);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getMoreRecentedAllGallery($id, $offset, $limit)
    {
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                        media_gallery.`id`,
                        media_gallery.`update_at`,
                        media_gallery.`desc`,
                        media_gallery.`gall_type`,
                        media_gallery.`view`,
                        media_store.`thumbnail`,
                        media_gallery.`title`
                   from 
                          media_gallery
                   inner join 
                          media_store
                   on 
                          media_gallery.id=media_store.gall_id
                   WHERE
                        media_gallery.`id`< ?
                        AND 
                        media_store.`thumbnail` IS NOT NULL
                        AND media_gallery.`group`='sexy'
                        AND media_gallery.`gall_type`='picture'
                   group by id
                   order by 
                           media_gallery.`id` desc
                           limit ? offset ?";

//            echo $sql;exit;

            $params = array($id, $limit, $offset);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getMoreRecentedGallery($tag,$id, $limit)
    {
        $dbpdo = new DBPDO();
        $return = array();
        $taglist = explode(",", $tag);
        $condition = " WHERE media_gallery.id!=".$id." and media_gallery.`gall_type`='picture' and media_gallery.`tag` LIKE '%%'";
        foreach ($taglist as $key => $val) {
            $val = trim($val);
            if ($key == 0) {
                $condition = " WHERE media_gallery.id!=".$id." and media_gallery.`gall_type`='picture' and (media_gallery.`tag` LIKE '%$val%' ";
            } else {
                $condition .= " OR media_gallery.`tag` LIKE '%$val%' ";
            }
        }
        $condition .= ")";
        try {
            $dbpdo->connect();
            $sql = "select
                        media_gallery.`id`,
                        media_gallery.`title`,
                        media_gallery.`desc`,
                        media_gallery.`update_at`,
                        media_store.`thumbnail`
                   from 
                          media_gallery
                   inner join 
                          media_store 
                   on 
                          media_gallery.id=media_store.gall_id
                   $condition
                   group by media_gallery.id
                   order by 
                           update_at desc
                           limit ?";
            $params = array($limit);
//            var_dump($sql);
//            exit;
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getMoreRecentedGalleryIdByDay($id,$day)
    {
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                        media_gallery.`id`,
                        media_gallery.`title`,
                        media_gallery.`desc`,
                        media_store.`thumbnail`
                   from 
                          media_gallery
                   inner join 
                          media_store 
                   on 
                          media_gallery.id=media_store.gall_id
                   where
                        media_gallery.`id` < " . $id . "
                   and
                      media_gallery.`created_at` > DATE_SUB(NOW(), INTERVAL $day DAY)
                   group by media_gallery.id
                   order by 
                           update_at desc";
            $params = array();
            $return = $dbpdo->query($sql,$params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getMoreRecentedGalleryId($id, $limit)
    {
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                        media_gallery.`id`,
                        media_gallery.`title`,
                        media_gallery.`desc`,
                        media_store.`thumbnail`
                   from 
                          media_gallery
                   inner join 
                          media_store 
                   on 
                          media_gallery.id=media_store.gall_id
                   where
                        media_gallery.`id` < " . $id . "
                   group by media_gallery.id
                   order by 
                           update_at desc
                           limit ?";
            $params = array($limit);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function updateView($gid)
    {
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "UPDATE `media_gallery` SET `view`=`view`+1 WHERE  `id`=?";
            $params = array($gid);
            $dbpdo->insert($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
    }

}
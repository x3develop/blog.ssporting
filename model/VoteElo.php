<?php

namespace model;

require_once __DIR__ . "/../bootstrap.php";

use Illuminate\Database\Eloquent\Model;

class VoteElo extends Model
{
    protected $table = "match_vote";
    protected $fillable = ['mid', 'fbid', 'side', 'created_at', 'updated_at'];

    public function match()
    {
        return $this->BelongsTo(MatchElo::class, "mid", "id");
    }

}

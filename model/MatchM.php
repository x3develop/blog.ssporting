<?php

/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 29/11/2559
 * Time: 20:48 น.
 */
include_once "DBPDO.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/FileManager.php";
use Carbon\Carbon;

class MatchM
{
    /*
    public function getLiveFile()
    {
        $file = $_SERVER["DOCUMENT_ROOT"] . "/live/allgames.csv";
//        print_r(file_get_contents($file));
        $matchs = str_getcsv(file_get_contents($file), "\n");
//        var_dump($matchs);
        $matchslist = array();
        $playable = array();
        $livelist = array();
        $gamecount = 0;
        $betcount = 0;
        $livecount = 0;
        $mlist = "0";
        foreach ($matchs as $key => $m) {
            $match = str_getcsv($m);
            $matchslist[$match[1]][] = $match;
            $gamecount++;
            $mlist .= "," . $match[0];
            if (array_key_exists(3, $match)) {
                if ((int)$match[3] > 1 && (int)$match[3] < 5) {
                    $livelist[$match[1]][] = $match;
                    $livecount++;
                }
            }
        }
        $odds = $this->getOdd($mlist);

        $betcount = count($odds);
        return array('all' => $matchslist, 'bet' => $playable, 'live' => $livelist, 'gamecount' => $gamecount, 'livecount' => $livecount, 'betcount' => $betcount, 'odds' => $odds);
    }

*/
    public function getOdd($matchlist, $day)
    {
        $dt = Carbon::now();
        if (empty($day)) {
            $day = $dt->toDateString();
        }
        $fileman = new FileManager();
        $fileman->setPath("cache");
        $fileman->setFilename("odds_" . $day . ".json");
        $needupdate = true;
        $oddslist = array();
        if (file_exists($fileman->getFullPath())) {
            $mt = \Carbon\Carbon::createFromTimestamp(filemtime($fileman->getFullPath()));
            if ($dt->diffInMinutes($mt) < 5) {
                $needupdate = false;
            } else {
                $needupdate = true;
            }
        }
        if ($needupdate) {
            $dbpdo = new DBPDO();
            $rs = array();
            $sql = "SELECT o7.match_id,o7.`type`,o7.hdp,o7.hdp_home,o7.hdp_away,mm.f24_mid FROM matches_map mm
RIGHT JOIN odds_7m o7 ON o7.match_id = mm.o7m_mid
WHERE mm.f24_mid IN ($matchlist)
ORDER BY o7.match_id
";

            try {
                $dbpdo->connect();
                $rs = $dbpdo->query($sql, array());
                foreach ($rs as $key => $val) {
                    $arr = [];
                    foreach ($val as $attr => $v) {
                        $arr[] = $v;
                    }
                    $oddslist[$val->f24_mid] = $arr;
                }
                $fileman->setContent(json_encode($oddslist));
                $fileman->write();

            } catch (Exception $e) {
                echo $e->getMessage();
            } finally {
                $dbpdo->close();
            }
        } else {
            $oddslist = json_decode($fileman->read(), true);
        }

        $dt->subDays(1);
        $day = $dt->toDateString();
        $fileman->setFilename("odds_" . $day . ".json");
        if (file_exists($fileman->getFullPath())) {
            $yesterdayodds = json_decode($fileman->read(), true);
            foreach ($yesterdayodds as $key => $val) {
                if (!array_key_exists($key, $oddslist)) {
                    $oddslist[$key] = $val;
                }
            }

        }

        return $oddslist;
    }

    public function getMatchByDate($day)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($day);
        $sql = "select
lm.mid,lm.lid,lm._lid,lm.sid,lm.c0,lm.c1,lm.c2,lm.hid,lm.gid,lm.hn,lm.gn,lm.s1,lm.s2,lm.date,lm.showDate,
ll.ln,
CASE WHEN ht.teamNameTh IS NULL THEN lm.hn ELSE ht.teamNameTh END as hnTh ,
CASE WHEN ht.teamNameEn IS NULL THEN lm.hn ELSE ht.teamNameEn END as hnEn,
CASE WHEN gt.teamNameTh IS NULL THEN lm.gn ELSE gt.teamNameTh END as gnTh,
CASE WHEN gt.teamNameEn IS NULL THEN lm.gn ELSE gt.teamNameEn END as gnEn,
ll.competitionId as cid,
lnl.priority,
lm.ao,
lm.hrci,
lm.grci,
lm.cx
FROM live_match lm
LEFT JOIN live_league ll on ll.leagueId=lm._lid AND ll.subleagueId=lm.lid
LEFT JOIN lang_league lnl on lnl.leagueId = lm._lid
left join lang_team  as ht on ht.tid = lm.hid
left join lang_team  as gt on gt.tid= lm.gid
where lm.showDate=? and lm.new='Y'
group by mid 
order by -lnl.priority DESC, lm._lid ASC,lm.date ASC
";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $rs;
    }

    public function getMatchPack($day)
    {
        $fileman = new FileManager();
        $dir = "cache";
        $filename = "allmacht_$day.json";
        $fileman->setPath($dir);
        $fileman->setFilename($filename);
        $result = [];
        if ($fileman->isFile() && $fileman->howOldInMinute() < 5) {
            $result = json_decode($fileman->read(), true);
        } else {
            $nowday = Carbon::now();
            $matchlist = $this->getMatchByDate($day);
            $datapack = array();
            $livepack = array();
            $betpack = array();
            $resultpack = array();
            $leaugepack = array();
            $allmatchpack = array();
            $mlist = "0";
            $gamecount = 0;
            $livecount = 0;
            $resultcount = 0;
            $allmachcount = 0;
            $keylist = array('match' => []);
            $keycount = 0;
            foreach ($matchlist as $key => $match) {
                $arr = array();
                foreach ($match as $k => $v) {
                    $arr[] = $v;
                    if ($key == 0) {
                        $keylist['match'][$k] = $keycount;
                        $keycount++;
                    }
                }
                $datapack[$match->lid][$match->mid] = $arr;
                $gamecount++;
                $mlist .= "," . $match->mid;

                $gamedate = Carbon::createFromTimestamp($match->c0);
                $currentdate = Carbon::now();
                if ($currentdate->diffInHours($gamedate, false) > -5) {
                    $allmatchpack[$match->lid][] = $match->mid;
                    $allmachcount++;
                }
                if ($match->sid > 1 && $match->sid < 5) {
//                $livepack[$match->lid][] = $arr;
                    $livepack[$match->lid][] = $match->mid;
                    $livecount++;
                }
                if ($match->sid >= 5) {
                    $resultpack[$match->lid][] = $match->mid;
                    $resultcount++;
                }
                $leaugepack[$match->lid] = $match->ln;
                $cidpack[$match->lid] = $match->cid;
            }
            $odds = $this->getOdd($mlist, $day);
            $betcount = 0;
            foreach ($matchlist as $key => $match) {
                if (array_key_exists($match->mid, $odds) && $match->sid == 1) {
                    if (is_numeric($odds[$match->mid][2])) {
                        $betpack[$match->lid][] = $match->mid;
                        $betcount++;
                    }
                }
            }

            $zid = 0;
            $lf = $this->getLiveFile($day);
            if (!empty($lf)) {
                $zid = $lf[0]->zid;
            }

            $dt = \Carbon\Carbon::now();
            $result = array('all' => $datapack, 'allmatch' => $allmatchpack, 'allmatchcount' => $allmachcount, 'live' => $livepack, 'bet' => $betpack, 'result' => $resultpack, 'gamecount' => $gamecount, 'livecount' => $livecount, 'betcount' => $betcount, 'resultcount' => $resultcount, 'odds' => $odds, 'leaguelist' => $leaugepack, 'countrylist' => $cidpack, 'c3' => $dt->getTimestamp(), 'zid' => $zid, "keylist" => $keylist);
            $fileman->setContent(json_encode($result));
            $fileman->write();
        }
        return $result;
    }

    public function getMatchPackMobile($day, $lang)
    {
        $matchlist = $this->getMatchByDate($day);

        $datapack = array();
        $livepack = array();
        $betpack = array();
        $leaugepack = array();
        $mlist = "0";
        $gamecount = 0;
        $livecount = 0;
        $leaugmatch = array("name" => "", "lid" => 0, "cid" => 0, "match" => []);
        $prevlid = 0;
        foreach ($matchlist as $key => $match) {
            if ($prevlid != 0 && $prevlid != $match->lid) {
                $datapack[] = $leaugmatch;
                $leaugmatch = array("name" => "", "lid" => 0, "match" => []);
            }
            $arr = array();
            $arr[] = $match->mid;
            $arr[] = $match->lid;
            $arr[] = $match->_lid;
            $arr[] = $match->sid;
            $arr[] = $match->c0;
            $arr[] = $match->c1;
            $arr[] = $match->c2;
            $arr[] = $match->hid;
            $arr[] = $match->gid;
//            $arr[] = $match->hn;
//            $arr[] = $match->gn;
            if ($match->sid > 3) {
                $arr[] = $match->s1;
            } else {
                $arr[] = $match->s2;
            }
            $arr[] = $match->date;
            $arr[] = $match->showDate;
//            $arr[] = $match->ln;
            if ($lang == "th") {
                $arr[] = empty($match->hnTh) ? $match->hn : $match->hnTh;
                $arr[] = empty($match->gnTh) ? $match->gn : $match->gnTh;
            } else {
                $arr[] = empty($match->hnEn) ? $match->hn : $match->hnEn;
                $arr[] = empty($match->gnEn) ? $match->gn : $match->gnEn;
            }
            $arr[] = $match->hlogo;
            $arr[] = $match->glogo;
//            $arr[] = $match->cid;
            $leaugmatch["match"][] = $arr;
            $leaugmatch["name"] = $match->ln;
            $leaugmatch['lid'] = $match->lid;
            $leaugmatch["cid"] = $match->cid;
            $prevlid = $match->lid;
            $gamecount++;
            $mlist .= "," . $match->mid;
            if ($match->sid > 1 && $match->sid < 5) {
//                $livepack[$match->lid][] = $arr;
                $livepack[] = $match->mid;
                $livecount++;
            }
            $leaugepack[$match->lid] = $match->ln;
        }
        $odds = $this->getOdd($mlist);
        $betcount = count($odds);
//        foreach ($matchlist as $key => $match) {
//            if (array_key_exists($match->mid, $odds) && $match->sid == 1) {
//                $betpack[$match->lid][] = $match->mid;
//            }
//            $betcount = count($betpack);
//        }
        $dt = \Carbon\Carbon::now();
        $result = array('all' => $datapack, 'live' => $livepack, 'gamecount' => $gamecount, 'livecount' => $livecount, 'betcount' => $betcount, 'odds' => $odds, 'c3' => $dt->getTimestamp());
        return $result;
    }

    function getGameByMid($mid)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($mid);
        $sql = "SELECT lm.*,ll.ln FROM live_match lm
LEFT JOIN live_league ll ON ll.leagueId=lm._lid
WHERE lm.mid = ?
LIMIT 1";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $rs;
    }

    function getGameByMidPack($mid)
    {

        $rs = $this->getGameByMid($mid);
        $matchkeycount = 0;
        $result = array("match" => [], "odds" => [], 'keylist' => array("match" => [], "odds" => []));
        $fileman = new FileManager();
        $fileman->setPath("cache");

        if (array_key_exists(0, $rs)) {
            foreach ($rs[0] as $k => $v) {
                $result["match"][] = $v;
                $result["keylist"]["match"][$k] = $matchkeycount;
                $matchkeycount++;
            }

            $fileman->setFilename("odds_" . $rs[0]->showDate . ".json");
            if (file_exists($fileman->getFullPath())) {
                $odds = json_decode($fileman->read(), true);
                if (array_key_exists($mid, $odds)) {
                    $result["odds"] = $odds[$mid];
                }
            }

        }


        return $result;
    }

    function getLiveEvent($mid)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($mid);
        $sql = "SELECT eo.id,eo.odds7m_mid,eo.at_minute,eo.pid,eo.`type`,eo.score,eo.side,p.playerNameEn FROM matches_map mm
RIGHT JOIN event_odds7m eo ON eo.odds7m_mid=mm.o7m_mid
LEFT JOIN player p ON p.playerId = eo.pid
WHERE mm.f24_mid=?";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }


    function getLiveEventPack($mid)
    {
        $result = array("event" => array());
        $rs = $this->getLiveEvent($mid);

        $result["event_with_key"] = $rs;
        foreach ($rs as $k => $v) {
            foreach ($v as $key => $val) {
                $result["event"][$k][] = $val;
            }
        }


        return $result;

    }

    function getSubstitutes($mid)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($mid);
        $sql = "SELECT sub.odds7m_mid,sub.pid_in,sub.pid_out,sub.minute,sub.side,ip.playerNameEn as in_name,op.playerNameEn as out_name FROM matches_map mm
RIGHT JOIN substitutes sub ON sub.odds7m_mid=mm.o7m_mid
LEFT JOIN player ip ON ip.playerId=sub.pid_in
LEFT JOIN player op ON op.playerId=sub.pid_out
WHERE
mm.f24_mid=? ORDER BY sub.id";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getSubstitutesPack($mid)
    {
        $result = array("substitutes" => array());
        $rs = $this->getSubstitutes($mid);

        $result["substitutes_with_key"] = $rs;
        foreach ($rs as $k => $v) {
            foreach ($v as $key => $val) {
                $result["substitutes"][$k][] = $val;
            }
        }


        return $result;

    }


    function getGameInfo($showdate)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($showdate);
        $sql = "SELECT mm.f24_mid,gi.odds7m_mid,gi.home_rank,gi.away_rank,gi.home_last_result,gi.away_last_result,gi.channel,gi.weather,gi.temperature,gi.home_formation,gi.away_formation FROM matches_map mm
RIGHT JOIN game_info gi ON gi.odds7m_mid=mm.o7m_mid
WHERE mm.showdate=?";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getGameInfoPack($showdate)
    {
        $result = array("game_info" => array());
        $rs = $this->getGameInfo($showdate);

        $result["game_info_with_key"] = $rs;
        foreach ($rs as $k => $v) {
            foreach ($v as $key => $val) {
                $result["game_info"][$v->f24_mid][] = $val;
            }
        }


        return $result;

    }

    function getGameInfoByMid($mid)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($mid);
        $sql = "SELECT mm.f24_mid,gi.odds7m_mid,gi.home_rank,gi.away_rank,gi.home_last_result,gi.away_last_result,gi.channel,gi.weather,gi.temperature,gi.home_formation,gi.away_formation FROM matches_map mm
RIGHT JOIN game_info gi ON gi.odds7m_mid=mm.o7m_mid
WHERE mm.f24_mid=? LIMIT 1";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getGameInfoByMidPack($mid)
    {
        $result = array("game_info" => array());
        $rs = $this->getGameInfoByMid($mid);

        $result["game_info_with_key"] = $rs;
        foreach ($rs as $k => $v) {
            foreach ($v as $key => $val) {
                $result["game_info"][] = $val;
            }
        }


        return $result;

    }

    function getGameLineUp($mid)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($mid);
        $sql = "SELECT l.* FROM matches_map mm
RIGHT JOIN lineup l ON l.id7m_mid=mm.o7m_mid
WHERE mm.f24_mid=?
ORDER BY l.id7m_tid,l.position";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getGameLineUpPack($mid)
    {
        $result = array("lineup" => array());
        $rs = $this->getGameLineUp($mid);

        $result["lineup_with_key"] = $rs;
        foreach ($rs as $k => $v) {
            foreach ($v as $key => $val) {
                $result["lineup"][$k][] = $val;
            }
        }


        return $result;

    }

    function getTeamFixture($mid)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($mid);
        $sql = "SELECT tf.id, tf.odds7m_mid,tf.odds7m_lid,tf.odds7m_hid,tf.odds7m_gid,tf.show_date,tf.side,ll.leagueId ,ht.tid as hid, gt.tid as gid, ll.leagueNameEn, ll.leagueNameTh, ht.teamNameEn as hname_en, ht.teamNameTh as hname_th, gt.teamNameEn as gname_en, gt.teamNameTh as gname_th FROM matches_map mm
RIGHT JOIN team_fixtures tf ON tf.odds7m_mid=mm.o7m_mid
LEFT JOIN lang_team ht ON ht.id7m = tf.odds7m_hid
LEFT JOIN lang_team gt ON gt.id7m = tf.odds7m_gid
LEFT JOIN lang_league ll ON ll.id7m=tf.odds7m_lid
WHERE mm.f24_mid=?";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getTeamFixturePack($mid)
    {
        $result = array("fixture" => array("home" => [], "away" => []), "keylist" => array("fixture" => []));
        $rs = $this->getTeamFixture($mid);

//        $result["fixture_with_key"] = $rs;
        $hkey = 0;
        $gkey = 0;
        $side = "";
        $fkeycount = 0;
        foreach ($rs as $k => $v) {
            foreach ($v as $key => $val) {
                if ($k == 0) {
                    $result["keylist"]["fixture"][$key] = $fkeycount++;

                }
                if ($v->side == "home") {
                    $result["fixture"][$v->side][$hkey][] = $val;

                } else {
                    $result["fixture"][$v->side][$gkey][] = $val;
                }

            }
            if ($v->side == "home") {
                $hkey++;
            } else {
                $gkey++;
            }
        }


        return $result;

    }


    function getH2H($mid)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($mid);
        $sql = "SELECT gh.id,gh.game_mid,gh.history_mid,gh.id7m_lid,gh.id7m_hid,gh.id7m_gid,gh.home_score,gh.away_score,gh.odds,gh.result,gh.odds_result,gh.show_date,gh.`type`,ll.leagueId, ht.tid as hid, gt.tid as gid, ll.leagueNameEn, ll.leagueNameTh, ht.teamNameEn as hname_en, ht.teamNameTh as hname_th, gt.teamNameEn as gname_en, gt.teamNameTh as gname_th FROM  matches_map mm
RIGHT JOIN game_history gh ON gh.game_mid = mm.o7m_mid
LEFT JOIN lang_league ll ON ll.id7m=gh.id7m_lid
LEFT JOIN lang_team ht ON ht.id7m = gh.id7m_hid
LEFT JOIN lang_team gt ON gt.id7m = gh.id7m_gid
WHERE mm.f24_mid=?
AND gh.`type` = 'game'
AND ll.new = 'Y'
ORDER BY gh.show_date DESC";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getH2HPack($mid)
    {
        $result = array("h2h" => [], "keylist" => array("h2h" => []));
        $rs = $this->getH2H($mid);
        $gkeycount = 0;
        foreach ($rs as $k => $v) {
            foreach ($v as $key => $val) {
                if ($k == 0) {
                    $result["keylist"]["h2h"][$key] = $gkeycount++;
                }
                $result["h2h"][$k][] = $val;
            }
        }
        return $result;

    }


    function getTeamGuide($mid)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($mid);
        $sql = "SELECT gh.id,gh.game_mid,gh.history_mid,gh.id7m_lid,gh.id7m_hid,gh.id7m_gid,gh.home_score,gh.away_score,gh.odds,gh.result,gh.odds_result,gh.show_date,gh.`type`,ll.leagueId, ht.tid as hid, gt.tid as gid, ll.leagueNameEn, ll.leagueNameTh, ht.teamNameEn as hname_en, ht.teamNameTh as hname_th, gt.teamNameEn as gname_en, gt.teamNameTh as gname_th FROM  matches_map mm
RIGHT JOIN game_history gh ON gh.game_mid = mm.o7m_mid
LEFT JOIN lang_league ll ON ll.id7m=gh.id7m_lid
LEFT JOIN lang_team ht ON ht.id7m = gh.id7m_hid
LEFT JOIN lang_team gt ON gt.id7m = gh.id7m_gid
WHERE mm.f24_mid=?
AND gh.`type` <> 'game'
ORDER BY gh.show_date DESC";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getTeamGuidePack($mid)
    {
        $result = array("guide" => array("home" => [], "away" => []), "keylist" => array("guide" => []));
        $rs = $this->getTeamGuide($mid);

//        $result["guide_with_key"] = $rs;
        $hkey = 0;
        $gkey = 0;
        $side = "";
        $gkeycount = 0;
        foreach ($rs as $k => $v) {
            foreach ($v as $key => $val) {
                if ($k == 0) {
                    $result["keylist"]["guide"][$key] = $gkeycount++;
                }
                if ($v->type == "home") {
                    $result["guide"][$v->type][$hkey][] = $val;

                } else {
                    $result["guide"][$v->type][$gkey][] = $val;
                }

            }
            if ($v->type == "home") {
                $hkey++;
            } else {
                $gkey++;
            }
        }


        return $result;

    }

    function getLiveStats($mid)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($mid);
        $sql = "SELECT mm.f24_mid,mm.o7m_mid,ls.`type`,ls.value FROM matches_map mm
LEFT JOIN live_stats ls ON ls.odds7m_mid=mm.o7m_mid
WHERE mm.f24_mid=?";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getLiveStatsPack($mid)
    {
        $result = array("livestats" => [], "keylist" => array("livestats" => []));
        $rs = $this->getLiveStats($mid);
        $keycount = 0;
        foreach ($rs as $k => $v) {
            foreach ($v as $key => $val) {
                $result["livestats"][$k][] = $val;
                if ($k == 0) {
                    $result["keylist"]["livestats"][$key] = $keycount;
                    $keycount++;
                }

            }
        }


        return $result;

    }

    function bet($bettype, $hid, $gid, $leagueId, $kickOff, $betDatetime, $betValue, $choose, $betHdp, $mid, $fb_uid, $status_id, $bet_sgold)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $success = 0;
        $last_id = 0;
        $params = array($bettype, $hid, $gid, $leagueId, $kickOff, $betDatetime, $betValue, $choose, $betHdp, $mid, $fb_uid, $status_id, $bet_sgold);
        $sql = "INSERT IGNORE INTO `bet` (`betType`, `hid`, `gid`, `leagueId`, `kickOff`, `betDatetime`, `betValue`, `choose`, `betHdp`, `mid`, `fb_uid`, `status_id`, `bet_sgold`)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
//        echo $sql;

        try {
            $dbpdo->connect();
            $last_id = $dbpdo->insert($sql, $params);


        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $last_id;

    }

    function betCount($mid)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($mid);
        $sql = "SELECT * FROM bet b
WHERE b.mid=?";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function betCountPack($mid)
    {
        $result = array("betlist" => array('home' => [], 'away' => [], 'draw' => []), "keylist" => array("betlist" => []));
        $rs = $this->betCount($mid);
        $keycount = 0;
        $hcount = 0;
        $gcount = 0;
        $dcount = 0;
        foreach ($rs as $k => $v) {
            foreach ($v as $key => $val) {
                if ($v->choose == "home") {
                    $result["betlist"][$v->choose][$hcount][] = $val;
                } else if ($v->choose == "away") {
                    $result["betlist"][$v->choose][$gcount][] = $val;
                } else {
                    $result["betlist"][$v->choose][$dcount][] = $val;
                }
                if ($k == 0) {
                    $result["keylist"]["betlist"][$key] = $keycount;
                    $keycount++;
                }
            }
            if ($v->choose == "home") {
                $hcount++;
            } else if ($v->choose == "away") {
                $gcount++;
            } else {
                $dcount++;
            }
        }


        return $result;

    }

    function getMatchBet($mid, $fbuid)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($mid, $fbuid);
        $sql = "SELECT b.*,fu.`uid`,fu.`display_name` FROM bet b
LEFT JOIN facebook_user fu ON fu.`fb_uid`=b.`fb_uid`
WHERE b.mid=?
AND b.`fb_uid`=?";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getMatchBetPack($mid, $fb_uid)
    {
        $result = array("bet" => [], "keylist" => array("bet" => []));
        $rs = $this->getMatchBet($mid, $fb_uid);
        $keycount = 0;
        foreach ($rs as $k => $v) {
            foreach ($v as $key => $val) {
                $result["bet"][] = $val;
                if ($k == 0) {
                    $result["keylist"]["bet"][$key] = $keycount;
                    $keycount++;
                }

            }
        }


        return $result;

    }


    function getLiveFile($date)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($date);
        $sql = "SELECT * FROM live_file lf
WHERE lf.date=?";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/28/2017
 * Time: 16:59
 */
include_once "DBPDO.php";

class playLeague
{
    public function getLeagueById($id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $league=array();
        try {
            $sql = "select * from league where id=:id";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $league=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $league;
    }

    public function getTeamByLeague($league_id){
        $dataReturn=array();
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "select * from league where league.id=:league_id";
        $stmt = $dbh->prepare($sql);
        $stmt->bindParam(':league_id', $id);
        $stmt->execute();
        $league=$stmt->fetch(PDO::FETCH_ASSOC);

        $date1 = new DateTime();
        $date2 = new DateTime($league['date_open_season']);
        $diff = intval($date2->diff($date1)->format("%y"));
        if(!empty($diff)) {
            $date2->modify($diff . ' year');
        }

        if(!empty($league['date_open_season'])) {
            $sqlHome="select `match`.team_home,team.name_en,team.path from `match` inner join team on team.id=`match`.team_home where `match`.league_id=:league_id and `match`.time_match>='".$league['date_open_season']."' group by `match`.team_home";
            $sqlAway="select `match`.team_away,team.name_en,team.path from `match` inner join team on team.id=`match`.team_away where `match`.league_id=:league_id and `match`.time_match>='".$league['date_open_season']."' group by `match`.team_away";
//            $sql = "select * from `match` where `match`.`status`='create' and `match`.league_id=:league_id and `match`.time_match>='".$league['date_open_season']."' ORDER BY time_match ASC limit 1";
        }else{
            $sqlHome="select `match`.team_home,team.name_en,team.path from `match` inner join team on team.id=`match`.team_home where `match`.league_id=:league_id group by `match`.team_home";
            $sqlAway="select `match`.team_away,team.name_en,team.path from `match` inner join team on team.id=`match`.team_away where `match`.league_id=:league_id group by `match`.team_away";
//            $sql = "select * from `match` where `match`.`status`='create' and `match`.league_id=:league_id ORDER BY time_match ASC limit 1";
        }
        $stmt = $dbh->prepare($sqlHome);
        $stmt->bindParam(':league_id', $league_id);
        $stmt->execute();
        $matchHome=$stmt->fetch(PDO::FETCH_ASSOC);

        $stmt = $dbh->prepare($sqlAway);
        $stmt->bindParam(':league_id', $league_id);
        $stmt->execute();
        $matchAway=$stmt->fetch(PDO::FETCH_ASSOC);

        foreach ($matchHome as $key=>$value){
            if(empty($dataReturn[$value->team_home])){
                $dataReturn[$value->team_home]=$value;
            }
        }

        foreach ($matchAway as $key=>$value){
            if(empty($dataReturn[$value->team_away])){
                $dataReturn[$value->team_away]=$value;
            }
        }
        return $dataReturn;
    }

    public function checkPathTeamPlayer($player_id,$league_id)
    {
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $user=array();
        try {
            $sql = "select match_player.player_id,
CASE 
 WHEN match_player.team='home' THEN teamHome.path
 WHEN match_player.team='away' THEN teamAway.path
 END as sc 
  from match_player 
inner join `match` on `match`.id=match_player.match_id
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
where match_player.player_id=:player_id and match_player.league_id=:league_id order by `match`.time_match DESC limit 1";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':player_id', $player_id);
            $stmt->bindParam(':league_id', $league_id);
            $stmt->execute();
            $user=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $user['sc'];
    }
//clone for mobile view
    public function checkPathTeamPlayer2($player_id,$league_id)
    {
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $user=array();
        try {
            $sql = "select match_player.player_id,
CASE 
 WHEN match_player.team='home' THEN teamHome.path
 WHEN match_player.team='away' THEN teamAway.path
 END as sc,
 CASE 
 WHEN match_player.team='home' THEN teamHome.name_en
 WHEN match_player.team='away' THEN teamAway.name_en
 END as tname  
  from match_player 
inner join `match` on `match`.id=match_player.match_id
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
where match_player.player_id=:player_id and match_player.league_id=:league_id order by `match`.time_match DESC limit 1";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':player_id', $player_id);
            $stmt->bindParam(':league_id', $league_id);
            $stmt->execute();
            $user=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $user;
    }

    public function checkMatchLastByLeague($id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "select * from league where league.id=:league_id";
        $stmt = $dbh->prepare($sql);
        $stmt->bindParam(':league_id', $id);
        $stmt->execute();
        $league=$stmt->fetch(PDO::FETCH_ASSOC);

        $date1 = new DateTime();
        $date2 = new DateTime($league['date_open_season']);
//            $date2->setDate($dateOpenSeason[0],$dateOpenSeason[1],$dateOpenSeason[2]);
        $diff = intval($date2->diff($date1)->format("%y"));
        if(!empty($diff)) {
            $date2->modify($diff . ' year');
        }
        if(!empty($league['date_open_season'])) {
            $sql = "select * from `match` where `match`.`status`='create' and `match`.league_id=:league_id and `match`.time_match>='".$league['date_open_season']."' ORDER BY time_match ASC limit 1";
        }else{
            $sql = "select * from `match` where `match`.`status`='create' and `match`.league_id=:league_id ORDER BY time_match ASC limit 1";
        }
        $stmt = $dbh->prepare($sql);
        $stmt->bindParam(':league_id', $id);
        $stmt->execute();
        $matchCreateLast=$stmt->fetch(PDO::FETCH_ASSOC);

        $team=array();
        try {
            if(!empty($league['date_open_season'])){
                if (empty($matchCreateLast)) {
                    $sql = "select match.status,home_end_time_score,away_end_time_score,teamHome.name_en as nameTeamHome,teamHome.path as pathTeamHome,teamAway.name_en as nameTeamAway,teamAway.path as pathTeamAway,`match`.time_match from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
where `match`.league_id=:league_id and `match`.time_match>=:date_open_season order by time_match DESC limit 10";
                    $stmt = $dbh->prepare($sql);
                    $stmt->bindParam(':league_id', $id);
                    $stmt->bindParam(':date_open_season', $date2->format("Y-m-d"));
                    $stmt->execute();
                    $matchLast = array_reverse($stmt->fetchAll(PDO::FETCH_ASSOC));
                } else {
                    $sql = "select match.status,home_end_time_score,away_end_time_score,teamHome.name_en as nameTeamHome,teamHome.path as pathTeamHome,teamAway.name_en as nameTeamAway,teamAway.path as pathTeamAway,`match`.time_match from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
where `match`.league_id=:league_id and time_match>=:time_match and `match`.time_match>=:date_open_season order by time_match ASC limit 10";
                    $stmt = $dbh->prepare($sql);
                    $stmt->bindParam(':league_id', $id);
                    $stmt->bindParam(':time_match', $matchCreateLast['time_match']);
                    $stmt->bindParam(':date_open_season', $date2->format("Y-m-d"));
                    $stmt->execute();
                    $matchLast = $stmt->fetchAll(PDO::FETCH_ASSOC);
                }
            }else {
                if (empty($matchCreateLast)) {
                    $sql = "select match.status,home_end_time_score,away_end_time_score,teamHome.name_en as nameTeamHome,teamHome.path as pathTeamHome,teamAway.name_en as nameTeamAway,teamAway.path as pathTeamAway,`match`.time_match from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
where `match`.league_id=:league_id order by time_match DESC limit 10";
                    $stmt = $dbh->prepare($sql);
                    $stmt->bindParam(':league_id', $id);
                    $stmt->execute();
                    $matchLast = array_reverse($stmt->fetchAll(PDO::FETCH_ASSOC));
                } else {
                    $sql = "select match.status,home_end_time_score,away_end_time_score,teamHome.name_en as nameTeamHome,teamHome.path as pathTeamHome,teamAway.name_en as nameTeamAway,teamAway.path as pathTeamAway,`match`.time_match from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
where `match`.league_id=:league_id and time_match>=:time_match order by time_match ASC limit 10";
                    $stmt = $dbh->prepare($sql);
                    $stmt->bindParam(':league_id', $id);
                    $stmt->bindParam(':time_match', $matchCreateLast['time_match']);
                    $stmt->execute();
                    $matchLast = $stmt->fetchAll(PDO::FETCH_ASSOC);
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $matchLast;
    }

    public function checkGoalScorerByLeague($id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "select * from league where league.id=:league_id";
        $stmt = $dbh->prepare($sql);
        $stmt->bindParam(':league_id', $id);
        $stmt->execute();
        $league=$stmt->fetch(PDO::FETCH_ASSOC);

        $team=array();
        try {
            if(!empty($league) && !empty($league['date_open_season'])) {
//                var_dump(!empty($league['date_open_season']));
                $dateOpenSeason = explode("-", $league['date_open_season']);
                $date1 = new DateTime();
                $date2 = new DateTime($league['date_open_season']);
//                $date2->setDate($dateOpenSeason[0],$dateOpenSeason[1],$dateOpenSeason[2]);
                $diff = intval($date2->diff($date1)->format("%y"));
                if(!empty($diff)) {
                    $date2->modify($diff . ' year');
                }
//                var_dump($date2->format("Y-m-d"));
                $sql = "select player.id,player.name,player.path,count(*) as goal from match_player 
left join player on player.id=match_player.player_id
left join `match` on `match`.id=match_player.match_id
where player.name!='' and `match`.time_match>=:date_open_season and match_player.league_id=:league_id && (match_player.status='goal' || match_player.status='freekick' || match_player.status='penalty') group by match_player.player_id order by goal DESC limit 10";
                $stmt = $dbh->prepare($sql);
                $stmt->bindParam(':league_id', $id);
                $stmt->bindParam(':date_open_season', $date2->format("Y-m-d"));
                $stmt->execute();
                $goalScorer = $stmt->fetchAll(PDO::FETCH_ASSOC);
            }else {
                $sql = "select player.id,player.name,player.path,count(*) as goal from match_player 
left join player on player.id=match_player.player_id
where player.name!='' and match_player.league_id=:league_id && (match_player.status='goal' || match_player.status='freekick' || match_player.status='penalty') group by match_player.player_id order by goal DESC limit 10";
                $stmt = $dbh->prepare($sql);
                $stmt->bindParam(':league_id', $id);
                $stmt->execute();
                $goalScorer = $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $goalScorer;
    }

    public function checkTeamByLeague($id)
    {
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "select * from league where league.id=:league_id";
        $stmt = $dbh->prepare($sql);
        $stmt->bindParam(':league_id', $id);
        $stmt->execute();
        $league=$stmt->fetch(PDO::FETCH_ASSOC);
        $team=array();
        if(!empty($league) && !empty($league['date_open_season'])) {
//            var_dump(!empty($league['date_open_season']));
            $dateOpenSeason = explode("-", $league['date_open_season']);
            $date1 = new DateTime();
            $date2 = new DateTime($league['date_open_season']);
//            $date2->setDate($dateOpenSeason[0],$dateOpenSeason[1],$dateOpenSeason[2]);
            $diff = intval($date2->diff($date1)->format("%y"));
            if(!empty($diff)) {
                $date2->modify($diff . ' year');
            }
//            var_dump($date2->format("Y-m-d"));
            try {
                $sql = "select team.name_en,team.path,
(COALESCE(MatchHome.win,0)+COALESCE(MatchAway.win,0)) as win,(COALESCE(MatchHome.always,0)+COALESCE(MatchAway.always,0)) as draw,(COALESCE(MatchHome.lose,0)+COALESCE(MatchAway.lose,0)) as lose,
(COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0)) as goal,
(COALESCE(MatchHome.scoreAwayEndScore,0)+COALESCE(MatchAway.scoreEndHomeScore,0)) as waste,
(COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0)) as GF,
(COALESCE(MatchHome.scoreAwayEndScore,0)+COALESCE(MatchAway.scoreEndHomeScore,0)) as GA,
((COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0))-(COALESCE(MatchHome.scoreAwayEndScore,0)+COALESCE(MatchAway.scoreEndHomeScore,0))) as GD,
(COALESCE(MatchHome.totalMatch,0)+COALESCE(MatchAway.totalMatch,0)) as PI,COALESCE(MatchHome.sc,0) as HPts,COALESCE(MatchAway.sc,0) as APts,(COALESCE(MatchHome.sc,0)+COALESCE(MatchAway.sc,0)) as totalPts
 from team
left join (SELECT `match`.league_id,`match`.team_home,
count(`match`.team_home) as totalMatch,
sum(`match`.home_end_time_score) as scoreEndHomeScore,
sum(home_over_time_score) as scoreHomeOverScore,
sum(away_end_time_score) as scoreAwayEndScore,
sum(away_over_time_score) as scoreAwayOverScore,
sum(
 CASE 
 WHEN `match`.home_end_time_score<`match`.away_end_time_score THEN 1
 END) as win,
 sum(
 CASE 
 WHEN `match`.home_end_time_score=`match`.away_end_time_score THEN 1
 END) as always,
 sum(
 CASE 
 WHEN `match`.home_end_time_score>`match`.away_end_time_score THEN 1
 END) as lose,
sum(
 CASE 
 WHEN `match`.home_end_time_score>`match`.away_end_time_score THEN 3
 WHEN `match`.home_end_time_score<`match`.away_end_time_score THEN 0
 WHEN `match`.home_end_time_score=`match`.away_end_time_score THEN 1
 END) as sc
FROM `match` where `match`.`status`='fullTime' and `match`.league_id=:league_id_1 and `match`.time_match>=:date_open_season group by `match`.team_home) MatchHome ON MatchHome.team_home = `team`.id
left join (SELECT `match`.league_id,`match`.team_away,
count(`match`.team_away) as totalMatch,
sum(`match`.home_end_time_score) as scoreEndHomeScore,
sum(home_over_time_score) as scoreHomeOverScore,
sum(away_end_time_score) as scoreAwayEndScore,
sum(away_over_time_score) as scoreAwayOverScore,
sum(
 CASE 
 WHEN `match`.home_end_time_score<`match`.away_end_time_score THEN 1
 END) as win,
 sum(
 CASE 
 WHEN `match`.home_end_time_score=`match`.away_end_time_score THEN 1
 END) as always,
 sum(
 CASE 
 WHEN `match`.home_end_time_score>`match`.away_end_time_score THEN 1
 END) as lose,
sum(
 CASE 
 WHEN `match`.home_end_time_score<`match`.away_end_time_score THEN 3
 WHEN `match`.home_end_time_score>`match`.away_end_time_score THEN 0
 WHEN `match`.home_end_time_score=`match`.away_end_time_score THEN 1
 END) as sc
 FROM `match` where `match`.`status`='fullTime' and `match`.league_id=:league_id_2 and `match`.time_match>=:date_open_season1 group by `match`.team_away) MatchAway ON MatchAway.team_away = `team`.id
where (MatchAway.league_id=:league_id_3 OR MatchHome.league_id=:league_id_4) order by (COALESCE(MatchHome.sc,0)+COALESCE(MatchAway.sc,0)) DESC,((COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0))-(COALESCE(MatchHome.scoreAwayEndScore,0)+COALESCE(MatchAway.scoreEndHomeScore,0))) DESC,(COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0)) DESC,team.name_en ASC";
                $stmt = $dbh->prepare($sql);
                $stmt->bindParam(':league_id_1', $id);
                $stmt->bindParam(':date_open_season', $date2->format("Y-m-d"));
                $stmt->bindParam(':date_open_season1', $date2->format("Y-m-d"));
                $stmt->bindParam(':league_id_2', $id);
                $stmt->bindParam(':league_id_3', $id);
                $stmt->bindParam(':league_id_4', $id);
                $stmt->execute();
                $team = $stmt->fetchAll(PDO::FETCH_ASSOC);
            } catch (Exception $e) {
//                echo $e->getMessage();
            } finally {
                $db->close();
            }
        }else {
            try {
                $sql = "select team.name_en,team.path,
(COALESCE(MatchHome.win,0)+COALESCE(MatchAway.win,0)) as win,(COALESCE(MatchHome.always,0)+COALESCE(MatchAway.always,0)) as draw,(COALESCE(MatchHome.lose,0)+COALESCE(MatchAway.lose,0)) as lose,
(COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0)) as goal,
(COALESCE(MatchHome.scoreAwayEndScore,0)+COALESCE(MatchAway.scoreEndHomeScore,0)) as waste,
(COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0)) as GF,
(COALESCE(MatchHome.scoreAwayEndScore,0)+COALESCE(MatchAway.scoreEndHomeScore,0)) as GA,
((COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0))-(COALESCE(MatchHome.scoreAwayEndScore,0)+COALESCE(MatchAway.scoreEndHomeScore,0))) as GD,
(COALESCE(MatchHome.totalMatch,0)+COALESCE(MatchAway.totalMatch,0)) as PI,COALESCE(MatchHome.sc,0) as HPts,COALESCE(MatchAway.sc,0) as APts,(COALESCE(MatchHome.sc,0)+COALESCE(MatchAway.sc,0)) as totalPts
 from team
left join (SELECT `match`.league_id,`match`.team_home,
count(`match`.team_home) as totalMatch,
sum(`match`.home_end_time_score) as scoreEndHomeScore,
sum(home_over_time_score) as scoreHomeOverScore,
sum(away_end_time_score) as scoreAwayEndScore,
sum(away_over_time_score) as scoreAwayOverScore,
sum(
 CASE 
 WHEN `match`.home_end_time_score>`match`.away_end_time_score THEN 3
 WHEN `match`.home_end_time_score<`match`.away_end_time_score THEN 0
 WHEN `match`.home_end_time_score=`match`.away_end_time_score THEN 1
 END) as sc
FROM `match` where `match`.`status`='fullTime' and `match`.league_id=:league_id_1 group by `match`.team_home) MatchHome ON MatchHome.team_home = `team`.id
left join (SELECT `match`.league_id,`match`.team_away,
count(`match`.team_away) as totalMatch,
sum(`match`.home_end_time_score) as scoreEndHomeScore,
sum(home_over_time_score) as scoreHomeOverScore,
sum(away_end_time_score) as scoreAwayEndScore,
sum(away_over_time_score) as scoreAwayOverScore,
sum(
 CASE 
 WHEN `match`.home_end_time_score<`match`.away_end_time_score THEN 3
 WHEN `match`.home_end_time_score>`match`.away_end_time_score THEN 0
 WHEN `match`.home_end_time_score=`match`.away_end_time_score THEN 1
 END) as sc
 FROM `match` where `match`.`status`='fullTime' and `match`.league_id=:league_id_2 group by `match`.team_away) MatchAway ON MatchAway.team_away = `team`.id
where (MatchAway.league_id=:league_id_3 OR MatchHome.league_id=:league_id_4) order by (COALESCE(MatchHome.sc,0)+COALESCE(MatchAway.sc,0)) DESC,((COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0))-(COALESCE(MatchHome.scoreAwayEndScore,0)+COALESCE(MatchAway.scoreEndHomeScore,0))) DESC,(COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0)) DESC,team.name_en ASC";
                $stmt = $dbh->prepare($sql);
                $stmt->bindParam(':league_id_1', $id);
                $stmt->bindParam(':league_id_2', $id);
                $stmt->bindParam(':league_id_3', $id);
                $stmt->bindParam(':league_id_4', $id);
                $stmt->execute();
                $team = $stmt->fetchAll(PDO::FETCH_ASSOC);
            } catch (Exception $e) {
//                echo $e->getMessage();
            } finally {
                $db->close();
            }
        }
        return $team;
    }

    public function checkTeamByChampionsLeague($id)
    {
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "select * from league where league.id=:league_id";
        $stmt = $dbh->prepare($sql);
        $stmt->bindParam(':league_id', $id);
        $stmt->execute();
        $league=$stmt->fetch(PDO::FETCH_ASSOC);
        $team=array();
        if(!empty($league) && !empty($league['date_open_season'])) {
//            var_dump(!empty($league['date_open_season']));
            $dateOpenSeason = explode("-", $league['date_open_season']);
            $date1 = new DateTime();
            $date2 = new DateTime($league['date_open_season']);
//            $date2->setDate($dateOpenSeason[0],$dateOpenSeason[1],$dateOpenSeason[2]);
            $diff = intval($date2->diff($date1)->format("%y"));
            if(!empty($diff)) {
                $date2->modify($diff . ' year');
            }
            try {
                $sql = "select team.name_en,team.path,MatchHome.groups,
(COALESCE(MatchHome.win,0)+COALESCE(MatchAway.win,0)) as win,(COALESCE(MatchHome.always,0)+COALESCE(MatchAway.always,0)) as draw,(COALESCE(MatchHome.lose,0)+COALESCE(MatchAway.lose,0)) as lose,
(COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0)) as goal,
(COALESCE(MatchHome.scoreAwayEndScore,0)+COALESCE(MatchAway.scoreEndHomeScore,0)) as waste,
(COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0)) as GF,
(COALESCE(MatchHome.scoreAwayEndScore,0)+COALESCE(MatchAway.scoreEndHomeScore,0)) as GA,
((COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0))-(COALESCE(MatchHome.scoreAwayEndScore,0)+COALESCE(MatchAway.scoreEndHomeScore,0))) as GD,
(COALESCE(MatchHome.totalMatch,0)+COALESCE(MatchAway.totalMatch,0)) as PI,COALESCE(MatchHome.sc,0) as HPts,COALESCE(MatchAway.sc,0) as APts,(COALESCE(MatchHome.sc,0)+COALESCE(MatchAway.sc,0)) as totalPts
 from team
left join (SELECT `match`.league_id,`match`.team_home,`match`.groups,
count(`match`.team_home) as totalMatch,
sum(`match`.home_end_time_score) as scoreEndHomeScore,
sum(home_over_time_score) as scoreHomeOverScore,
sum(away_end_time_score) as scoreAwayEndScore,
sum(away_over_time_score) as scoreAwayOverScore,
sum(
 CASE 
 WHEN `match`.home_end_time_score<`match`.away_end_time_score THEN 1
 END) as win,
 sum(
 CASE 
 WHEN `match`.home_end_time_score=`match`.away_end_time_score THEN 1
 END) as always,
 sum(
 CASE 
 WHEN `match`.home_end_time_score>`match`.away_end_time_score THEN 1
 END) as lose,
sum(
 CASE 
 WHEN `match`.home_end_time_score>`match`.away_end_time_score THEN 3
 WHEN `match`.home_end_time_score<`match`.away_end_time_score THEN 0
 WHEN `match`.home_end_time_score=`match`.away_end_time_score THEN 1
 END) as sc
FROM `match` where `match`.league_id=:league_id_1 and `match`.time_match>=:date_open_season group by `match`.team_home) MatchHome ON MatchHome.team_home = `team`.id
left join (SELECT `match`.league_id,`match`.team_away,`match`.groups,
count(`match`.team_away) as totalMatch,
sum(`match`.home_end_time_score) as scoreEndHomeScore,
sum(home_over_time_score) as scoreHomeOverScore,
sum(away_end_time_score) as scoreAwayEndScore,
sum(away_over_time_score) as scoreAwayOverScore,
sum(
 CASE 
 WHEN `match`.home_end_time_score<`match`.away_end_time_score THEN 1
 END) as win,
 sum(
 CASE 
 WHEN `match`.home_end_time_score=`match`.away_end_time_score THEN 1
 END) as always,
 sum(
 CASE 
 WHEN `match`.home_end_time_score>`match`.away_end_time_score THEN 1
 END) as lose,
sum(
 CASE 
 WHEN `match`.home_end_time_score<`match`.away_end_time_score THEN 3
 WHEN `match`.home_end_time_score>`match`.away_end_time_score THEN 0
 WHEN `match`.home_end_time_score=`match`.away_end_time_score THEN 1
 END) as sc
 FROM `match` where `match`.league_id=:league_id_2 and `match`.time_match>=:date_open_season1 group by `match`.team_away) MatchAway ON MatchAway.team_away = `team`.id
where (MatchAway.league_id=:league_id_3 OR MatchHome.league_id=:league_id_4) order by MatchHome.groups ASC, (COALESCE(MatchHome.sc,0)+COALESCE(MatchAway.sc,0)) DESC,((COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0))-(COALESCE(MatchHome.scoreAwayEndScore,0)+COALESCE(MatchAway.scoreEndHomeScore,0))) DESC,(COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0)) DESC,team.name_en ASC";
                $stmt = $dbh->prepare($sql);
                $stmt->bindParam(':league_id_1', $id);
                $stmt->bindParam(':date_open_season', $date2->format("Y-m-d"));
                $stmt->bindParam(':date_open_season1', $date2->format("Y-m-d"));
                $stmt->bindParam(':league_id_2', $id);
                $stmt->bindParam(':league_id_3', $id);
                $stmt->bindParam(':league_id_4', $id);
                $stmt->execute();
                $team = $stmt->fetchAll(PDO::FETCH_ASSOC);
            } catch (Exception $e) {
//                echo $e->getMessage();
            } finally {
                $db->close();
            }
        }else {
            try {
                $sql = "select team.name_en,team.path,MatchHome.groups,
(COALESCE(MatchHome.win,0)+COALESCE(MatchAway.win,0)) as win,(COALESCE(MatchHome.always,0)+COALESCE(MatchAway.always,0)) as draw,(COALESCE(MatchHome.lose,0)+COALESCE(MatchAway.lose,0)) as lose,
(COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0)) as goal,
(COALESCE(MatchHome.scoreAwayEndScore,0)+COALESCE(MatchAway.scoreEndHomeScore,0)) as waste,
(COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0)) as GF,
(COALESCE(MatchHome.scoreAwayEndScore,0)+COALESCE(MatchAway.scoreEndHomeScore,0)) as GA,
((COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0))-(COALESCE(MatchHome.scoreAwayEndScore,0)+COALESCE(MatchAway.scoreEndHomeScore,0))) as GD,
(COALESCE(MatchHome.totalMatch,0)+COALESCE(MatchAway.totalMatch,0)) as PI,COALESCE(MatchHome.sc,0) as HPts,COALESCE(MatchAway.sc,0) as APts,(COALESCE(MatchHome.sc,0)+COALESCE(MatchAway.sc,0)) as totalPts
 from team
left join (SELECT `match`.league_id,`match`.team_home,`match`.groups,
count(`match`.team_home) as totalMatch,
sum(`match`.home_end_time_score) as scoreEndHomeScore,
sum(home_over_time_score) as scoreHomeOverScore,
sum(away_end_time_score) as scoreAwayEndScore,
sum(away_over_time_score) as scoreAwayOverScore,
sum(
 CASE 
 WHEN `match`.home_end_time_score>`match`.away_end_time_score THEN 3
 WHEN `match`.home_end_time_score<`match`.away_end_time_score THEN 0
 WHEN `match`.home_end_time_score=`match`.away_end_time_score THEN 1
 END) as sc
FROM `match` where `match`.league_id=:league_id_1 group by `match`.team_home) MatchHome ON MatchHome.team_home = `team`.id
left join (SELECT `match`.league_id,`match`.team_away,`match`.groups,
count(`match`.team_away) as totalMatch,
sum(`match`.home_end_time_score) as scoreEndHomeScore,
sum(home_over_time_score) as scoreHomeOverScore,
sum(away_end_time_score) as scoreAwayEndScore,
sum(away_over_time_score) as scoreAwayOverScore,
sum(
 CASE 
 WHEN `match`.home_end_time_score<`match`.away_end_time_score THEN 3
 WHEN `match`.home_end_time_score>`match`.away_end_time_score THEN 0
 WHEN `match`.home_end_time_score=`match`.away_end_time_score THEN 1
 END) as sc
 FROM `match` where `match`.league_id=:league_id_2 group by `match`.team_away) MatchAway ON MatchAway.team_away = `team`.id
where (MatchAway.league_id=:league_id_3 OR MatchHome.league_id=:league_id_4) order by MatchHome.groups ASC, (COALESCE(MatchHome.sc,0)+COALESCE(MatchAway.sc,0)) DESC,((COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0))-(COALESCE(MatchHome.scoreAwayEndScore,0)+COALESCE(MatchAway.scoreEndHomeScore,0))) DESC,(COALESCE(MatchHome.scoreEndHomeScore,0)+COALESCE(MatchAway.scoreAwayEndScore,0)) DESC,team.name_en ASC";
                $stmt = $dbh->prepare($sql);
                $stmt->bindParam(':league_id_1', $id);
                $stmt->bindParam(':league_id_2', $id);
                $stmt->bindParam(':league_id_3', $id);
                $stmt->bindParam(':league_id_4', $id);
                $stmt->execute();
                $team = $stmt->fetchAll(PDO::FETCH_ASSOC);
            } catch (Exception $e) {
//                echo $e->getMessage();
            } finally {
                $db->close();
            }
        }
        return $team;
    }

}
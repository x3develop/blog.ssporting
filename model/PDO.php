<?php

/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 16/11/2559
 * Time: 14:40 น.
 */
class PDO
{
    private $servername = "localhost";
    private $username = "root";
    private $password = "1234";
    private $dbname = "eeball.com";
    private $conn = null;

    function connect()
    {
        try {
            $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    function close()
    {
        try {
            $this->conn->close();
        } catch (Exception $e) {
            echo "Connection failed: " . $e->getMessage();
        } finally {
            $this->conn = null;
        }
    }

    function  query($sql)
    {
        $sth = $this->conn->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(5);
    }

    function insert($sql, $arr)
    {
        //sql pattern is INSERT INTO fruit(name, colour) VALUES (?, ?)
        $success = 0;
        try {
            $insert = $this->conn->prepare($sql);
            $success = $insert->execute($arr);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $success;
    }
}
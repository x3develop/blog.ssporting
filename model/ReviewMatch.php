<?php
namespace model;

require_once __DIR__ . "/../bootstrap.php";
use Illuminate\Database\Eloquent\Model;

class ReviewMatch extends Model
{
    protected $table = "review_match";
    protected $fillable = ['user_id', 'match_id', 'user_review_id','team','review_text', 'created_at', 'updated_at'];

    public function match()
    {
        return $this->BelongsTo(MatchElo::class, "id", "match_id");
    }

    public function owner()
    {
        return $this->HasOne(Reviewer::class, "id", "user_review_id");
    }

}

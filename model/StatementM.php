<?php

/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 21/2/2560
 * Time: 14:45 น.
 */
require_once "DBPDO.php";

class StatementM
{
    public function addStatement($uid, $fb_uid, $statement_type, $type_addition, $before_sgold, $before_scoin, $sgold_income, $scoin_income, $sgold_outcome, $scoin_outcome, $balance_sgold, $balance_scoin, $result, $bet_id = null, $game_id = null, $note = null, $before_diamond = 0, $diamond_income = 0, $diamond_outcome = 0, $balance_diamond = 0)
    {
        $dbpdo = new DBPDO();
        $last_id = 0;
        $sql = "insert into total_statement
            (uid,
            fb_uid,
            statement_type,
            type_addition,
            before_sgold,
            before_scoin,
            sgold_income,
            scoin_income,
            sgold_outcome,
            scoin_outcome,
            balance_sgold,
            balance_scoin,
            result,
            bet_id,
            game_id,
            note,
            before_diamond,
            diamond_income,
            diamond_outcome,
            balance_diamond
            )
            values(
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?
            )
    ";
        $params = array($uid, $fb_uid, $statement_type, $type_addition, $before_sgold, $before_scoin, $sgold_income, $scoin_income, $sgold_outcome, $scoin_outcome, $balance_sgold, $balance_scoin, $result, $bet_id, $game_id, $note, $before_diamond, $diamond_income, $diamond_outcome, $balance_diamond);
        try {
            $dbpdo->connect();
            $last_id = $dbpdo->insert($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $last_id;
    }

    public function getByUser($uid, $offset = 0, $limit = 50)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($uid, $offset, $limit);
        $sql = "SELECT * FROM total_statement ts
WHERE ts.fb_uid=?
ORDER BY ts.statement_timestamp DESC
LIMIT ?,?";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getByUserPack($uid, $offset = 0, $limit = 50)
    {
        $result = array("statement" => [], "keylist" => array("statement" => []));
        $rs = $this->getByUser($uid, $offset, $limit);
        $keycount = 0;
        foreach ($rs as $k => $v) {
            foreach ($v as $key => $val) {
                $result["statement"][$k][] = $val;
                if ($k == 0) {
                    $result["keylist"]["statement"][$key] = $keycount;
                    $keycount++;
                }

            }
        }
        return $result;

    }

}
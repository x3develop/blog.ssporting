<?php
namespace model;

require_once __DIR__ . "/../bootstrap.php";
use Illuminate\Database\Eloquent\Model;

class TeamElo extends Model
{
    protected $table = "team";
    protected $fillable = ['name_th', 'name_en', 'path', 'created_at', 'updated_at'];

    public function getLogo()
    {
        $path = "/images/team/team_default_64x64.png";
        if (!empty($this->path) && file_exists($_SERVER['DOCUMENT_ROOT'].$this->path)) {
            $path = $this->path;
        }
        return $path;
    }

}

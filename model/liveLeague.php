<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 11/22/2016
 * Time: 10:50
 */
class liveLeague extends db_connect
{
    protected $nameTable = "live_league";

    public function getLeagueAndMatchToDay($limitLeague){
        $dataLeagueAndMatch=array();
        $listSpecial="(";
        $listAll="(";
        $dateStart = new DateTime();
        $queryMatch="select mid,lid,hid,gid,hn,gn,sid,s1,s2,date,sid from live_match where live_match.showDate='".$dateStart->format('Y-m-d')."' and lid in ";
        $conn=$this->db_connect();
        $query=$conn->query("select leagueId,ln,kn from ".$this->nameTable." where date = '".$dateStart->format('Y-m-d')."'");
        if($query->num_rows>0){
            while ($row = $query->fetch_object()) {
                if(($row->ln=="Premier League" and $row->kn="England")){
                    if($listSpecial=="("){
                        $listSpecial.=$row->leagueId;
                    }else{
                        $listSpecial.=','.$row->leagueId;
                    }
                }else if($row->ln=="Primera Division" and $row->kn="Spain"){
                    if($listSpecial=="("){
                        $listSpecial.=$row->leagueId;
                    }else{
                        $listSpecial.=','.$row->leagueId;
                    }
                }else if($row->ln=="Bundesliga" and $row->kn="Germany"){
                    if($listSpecial=="("){
                        $listSpecial.=$row->leagueId;
                    }else{
                        $listSpecial.=','.$row->leagueId;
                    }
                }else if($row->ln=="Serie A" and $row->kn="Italy"){
                    if($listSpecial=="("){
                        $listSpecial.=$row->leagueId;
                    }else{
                        $listSpecial.=','.$row->leagueId;
                    }
                }else if($row->ln=="Ligue 1" and $row->kn="France"){
                    if($listSpecial=="("){
                        $listSpecial.=$row->leagueId;
                    }else{
                        $listSpecial.=','.$row->leagueId;
                    }
                }else if($row->ln=="Premier League" and $row->kn="Thailand"){
                    if($listSpecial=="("){
                        $listSpecial.=$row->leagueId;
                    }else{
                        $listSpecial.=','.$row->leagueId;
                    }
                }else{
                    if($listAll=="("){
                        $listAll.=$row->leagueId;
                    }else{
                        $listAll.=','.$row->leagueId;
                    }
                }
            }
            $listSpecial.=")";
            $listAll.=")";

            $dataLeagueAndMatch['All']=array();
            $dataLeagueAndMatch['Special']=array();
            $connLeague = $this->db_connect();
            $queryLeague = $connLeague->query($queryMatch . $listAll);

            if ($queryLeague->num_rows > 0) {
                while ($rowM = $queryLeague->fetch_object()) {
                    if(!empty($dataLeagueAndMatch['All'][$rowM->lid])) {
                        array_push($dataLeagueAndMatch['All'][$rowM->lid], $rowM);
                    }else{
                        $dataLeagueAndMatch['All'][$rowM->lid]=array();
                        array_push($dataLeagueAndMatch['All'][$rowM->lid], $rowM);
                    }
                }
            }
            $queryLeague->close();

            $connLeague = $this->db_connect();
            $queryLeague = $connLeague->query($queryMatch . $listSpecial);
            if ($queryLeague->num_rows > 0) {
                while ($rowM = $queryLeague->fetch_object()) {
                    if(!empty($dataLeagueAndMatch['Special'][$rowM->lid])) {
                        array_push($dataLeagueAndMatch['Special'][$rowM->lid], $rowM);
                    }else{
                        $dataLeagueAndMatch['Special'][$rowM->lid]=array();
                        array_push($dataLeagueAndMatch['Special'][$rowM->lid], $rowM);
                    }
                }
            }
            $queryLeague->close();
            return $dataLeagueAndMatch;
        }else{
            return 0;
        }
        $conn->close();
        $query->free();
    }
}
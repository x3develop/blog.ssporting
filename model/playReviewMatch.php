<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 3/13/2018
 * Time: 22:55
 */
include_once "DBPDO.php";
class playReviewMatch{

    function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key=> $row) {
            $sort_col[$key] = $row[$col];
        }
        array_multisort($sort_col, $dir, $arr);
    }

    public function getListUserReview(){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select * from user_review";
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $topRanking=$stmt->fetchAll(\PDO::FETCH_UNIQUE|\PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $topRanking;
    }

    public function topRanking($limit=null){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql="select user_review.id,user_review.name,user_review.path,";
            $sql.="sum(CASE WHEN (r_m.home_end_time_score>r_m.away_end_time_score and r_m.team='home') or (r_m.away_end_time_score>r_m.home_end_time_score and r_m.team='away') THEN 1 END) as win,";
            $sql.="sum(CASE WHEN r_m.home_end_time_score=r_m.away_end_time_score THEN 1 END) as always,";
            $sql.="sum(CASE WHEN (r_m.away_end_time_score>r_m.home_end_time_score and r_m.team='home') or (r_m.home_end_time_score>r_m.away_end_time_score and r_m.team='away') THEN 1 END) as lose,";
            $sql.="count(*) as total,";
            $sql.="(((sum(CASE WHEN (r_m.home_end_time_score>r_m.away_end_time_score and r_m.team='home') or (r_m.away_end_time_score>r_m.home_end_time_score and r_m.team='away') THEN 1 END))/count(*))*100) as awayRate,";
            $sql.="(((sum(CASE WHEN (r_m.home_end_time_score>r_m.away_end_time_score and r_m.team='home') or (r_m.away_end_time_score>r_m.home_end_time_score and r_m.team='away') THEN 1 END)+sum(CASE WHEN r_m.home_end_time_score=r_m.away_end_time_score THEN 1 END))/count(*))*100) as winRate";
            $sql.=" from (select review_match.*,`match`.home_end_time_score,`match`.away_end_time_score from review_match inner join `match` on `match`.id=review_match.match_id";
            $sql.=" where `match`.`status`='fullTime' and review_match.updated_at>=DATE(NOW() - INTERVAL 1 MONTH) order by review_match.updated_at ) as r_m";
            $sql.=" inner join user_review on r_m.user_review_id=user_review.id";
            $sql.=" group by r_m.user_review_id order by (((sum(CASE WHEN (r_m.home_end_time_score>r_m.away_end_time_score and r_m.team='home') or (r_m.away_end_time_score>r_m.home_end_time_score and r_m.team='away') THEN 1 END)+sum( CASE  WHEN r_m.home_end_time_score=r_m.away_end_time_score THEN 1 END))/count(*))*100) DESC";
            if(!empty($limit)){
                $sql .= 'limit '.$limit;
            }
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $topRanking=$stmt->fetchAll();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $topRanking;
    }

    public function getWinRate($id,$limit){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select 
 sum(
 CASE 
 WHEN r_m.home_end_time_score>r_m.away_end_time_score and r_m.team='home' THEN 1
 WHEN r_m.home_end_time_score<r_m.away_end_time_score and r_m.team='away' THEN 1
 END) as win,
sum(
 CASE 
 WHEN r_m.home_end_time_score=r_m.away_end_time_score THEN 1
 END) as always,
sum(
 CASE 
 WHEN r_m.home_end_time_score>r_m.away_end_time_score and r_m.team='away' THEN 1
 WHEN r_m.home_end_time_score<r_m.away_end_time_score and r_m.team='home' THEN 1
 END) as lose,
count(*) as total ,
(((sum(CASE WHEN (r_m.home_end_time_score>r_m.away_end_time_score and r_m.team='home') or (r_m.away_end_time_score>r_m.home_end_time_score and r_m.team='away') THEN 1 END)+sum(CASE WHEN r_m.home_end_time_score=r_m.away_end_time_score THEN 1 END))/count(*))*100) as winRate
from (select review_match.*,(`match`.home_end_time_score) as home_end_time_score,`match`.away_end_time_score from review_match 
inner join `match` on `match`.id=review_match.match_id 
where review_match.user_review_id=".$id." and `match`.`status`='fullTime' and review_match.updated_at>=DATE(NOW() - INTERVAL 1 MONTH) order by review_match.updated_at desc) as  r_m
order by r_m.updated_at asc ";
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $getRate=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $getRate;
    }

    public function getResultsReviewMatchByUserReview($id,$limit){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select review_match.updated_at,review_match.user_review_id,review_match.match_id,gamble.home_water_bill,gamble.handicap,gamble.away_water_bill,`match`.home_end_time_score,`match`.away_end_time_score,
CASE
    WHEN (`match`.home_end_time_score) > `match`.away_end_time_score and review_match.team='home' THEN 'win'
    WHEN (`match`.home_end_time_score) > `match`.away_end_time_score and review_match.team='away' THEN 'lose'
    WHEN (`match`.home_end_time_score) < `match`.away_end_time_score and review_match.team='away' THEN 'win'
    WHEN (`match`.home_end_time_score) < `match`.away_end_time_score and review_match.team='home' THEN 'lose'
    ELSE 'equal'
END as results  from review_match 
inner join gamble on gamble.match_id=review_match.match_id 
inner join `match` on `match`.id=review_match.match_id
where review_match.user_review_id=".$id." and `match`.`status`='fullTime' order by review_match.updated_at asc limit ".$limit;
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $reviewMatch=$stmt->fetchAll();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $reviewMatch;
    }

    public function getReviewAndUserMatchByMatchId($id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select `user`.fb_uid as user_fb_uid,`user`.name as user_name,`user`.accuracy as user_accuracy,user_review.name,teamHome.name_en as homename,teamHome.path as homepath,teamAway.name_en as awayname,teamAway.path as awaypath,review_match.*,gamble.handicap,user_review.path from review_match 
inner join `gamble` on `gamble`.match_id=review_match.match_id
inner join `match` on `match`.id=review_match.match_id
left join user_review on user_review.id=review_match.user_review_id
left join `user` on `user`.id=review_match.user_id
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
where review_match.match_id=".$id." order by review_match.team ASC";
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $reviewMatch=$stmt->fetchAll();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $reviewMatch;
    }

    public function getReviewAndUserMatchAwayByMatchIdAndLimit($id,$limit){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select `user`.fb_uid as user_fb_uid,`user`.name as user_name,`user`.accuracy as user_accuracy,user_review.name,teamHome.name_en as homename,teamHome.path as homepath,teamAway.name_en as awayname,teamAway.path as awaypath,review_match.*,gamble.handicap,user_review.path from review_match 
inner join `gamble` on `gamble`.match_id=review_match.match_id
inner join `match` on `match`.id=review_match.match_id
left join user_review on user_review.id=review_match.user_review_id
left join `user` on `user`.id=review_match.user_id
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
where review_match.team='away' and review_match.match_id=".$id." order by review_match.team ASC limit ".$limit;
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $reviewMatch=$stmt->fetchAll();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $reviewMatch;
    }

    public function getReviewAndUserMatchAwayByMatchId($id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select `user`.fb_uid as user_fb_uid,`user`.name as user_name,`user`.accuracy as user_accuracy,user_review.name,teamHome.name_en as homename,teamHome.path as homepath,teamAway.name_en as awayname,teamAway.path as awaypath,review_match.*,gamble.handicap,user_review.path from review_match 
inner join `gamble` on `gamble`.match_id=review_match.match_id
inner join `match` on `match`.id=review_match.match_id
left join user_review on user_review.id=review_match.user_review_id
left join `user` on `user`.id=review_match.user_id
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
where review_match.team='away' and review_match.match_id=".$id." order by review_match.team ASC";
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $reviewMatch=$stmt->fetchAll();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $reviewMatch;
    }

    public function getReviewAndUserMatchHomeByMatchIdAndLimit($id,$limit){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select `user`.fb_uid as user_fb_uid,`user`.name as user_name,`user`.accuracy as user_accuracy,user_review.name,teamHome.name_en as homename,teamHome.path as homepath,teamAway.name_en as awayname,teamAway.path as awaypath,review_match.*,gamble.handicap,user_review.path from review_match 
inner join `gamble` on `gamble`.match_id=review_match.match_id
inner join `match` on `match`.id=review_match.match_id
left join user_review on user_review.id=review_match.user_review_id
left join `user` on `user`.id=review_match.user_id
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
where review_match.team='home' and review_match.match_id=".$id." order by review_match.team ASC limit ".$limit;
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $reviewMatch=$stmt->fetchAll();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $reviewMatch;
    }

    public function getReviewAndUserMatchHomeByMatchId($id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select `user`.fb_uid as user_fb_uid,`user`.name as user_name,`user`.accuracy as user_accuracy,user_review.name,teamHome.name_en as homename,teamHome.path as homepath,teamAway.name_en as awayname,teamAway.path as awaypath,review_match.*,gamble.handicap,user_review.path from review_match 
inner join `gamble` on `gamble`.match_id=review_match.match_id
inner join `match` on `match`.id=review_match.match_id
left join user_review on user_review.id=review_match.user_review_id
left join `user` on `user`.id=review_match.user_id
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
where review_match.team='home' and review_match.match_id=".$id." order by review_match.team ASC";
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $reviewMatch=$stmt->fetchAll();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $reviewMatch;
    }

    public function getReviewMatchByMatchId($id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select user_review.name,teamHome.name_en as homename,teamHome.path as homepath,teamAway.name_en as awayname,teamAway.path as awaypath,review_match.*,gamble.handicap,user_review.path from review_match 
inner join `gamble` on `gamble`.match_id=review_match.match_id
inner join user_review on user_review.id=review_match.user_review_id
inner join `match` on `match`.id=review_match.match_id
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
where review_match.match_id=".$id." group by review_match.user_review_id order by review_match.team ASC";
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $reviewMatch=$stmt->fetchAll();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $reviewMatch;
    }

    public function getReviewByMatch(){
        $dataReturn=array();
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select `user`.fb_uid,`user`.name,`user`.accuracy,`match`.id,gamble.handicap,teamHome.name_en as homename,teamAway.name_en as awayname,user_review.updated_at,review_match.user_review_id,review_match.match_id,review_match.team,review_match.review_text,user_review.name,user_review.path from `match` 
right join review_match on review_match.match_id=`match`.id
left join user_review on user_review.id=review_match.user_review_id
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join `gamble` on `gamble`.match_id=`match`.id 
left join `user` on `user`.id=review_match.user_id
where `match`.`status`='bet' order by review_match.match_id";
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $reviewMatch=$stmt->fetchAll();
            foreach ($reviewMatch as $key=>$value){
                if(empty($dataReturn[$value['match_id']])){
                    $dataReturn[$value['match_id']]=array();
                    array_push($dataReturn[$value['match_id']],$value);
                }else{
                    array_push($dataReturn[$value['match_id']],$value);
                }
            }
            return $dataReturn;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $reviewMatch;
    }
}
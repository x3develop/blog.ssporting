<?php

namespace model;

require_once __DIR__ . "/../bootstrap.php";

use Illuminate\Database\Eloquent\Model;

class MatchCommentElo extends Model
{
    protected $table = "match_comment";
    protected $fillable = ['mid', 'side', 'user_id', 'user_type', 'comment', 'created_at', 'updated_at'];

    public function match()
    {
        return $this->BelongsTo(MatchElo::class, "mid", "id");
    }

    public function owner()
    {
        if ($this->user_type == "facebook") {
            return $this->BelongsTo(UserElo::class, "user_id", "id")->select(array('id', 'fb_uid','name','accuracy'));
        } else {
            return $this->BelongsTo(Reviewer::class, "user_id", "id");
        }
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 7/6/2018
 * Time: 3:05 PM
 */
include_once "DBPDO.php";

class playBet
{

    function percentBet($id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sth = $dbh->prepare("select ((((sum(case bet.`team` when 'home' then 1 else 0 end)
+sum(case bet.`team` when 'away' then 1 else 0 end))
-sum(case bet.`team` when 'away' then 1 else 0 end))
/(sum(case bet.`team` when 'home' then 1 else 0 end)
+sum(case bet.`team` when 'away' then 1 else 0 end)))*100) as pHome,
((((sum(case bet.`team` when 'home' then 1 else 0 end)+sum(case bet.`team` when 'away' then 1 else 0 end))
-sum(case bet.`team` when 'home' then 1 else 0 end))
/(sum(case bet.`team` when 'home' then 1 else 0 end)+sum(case bet.`team` when 'away' then 1 else 0 end)))*100) as pAway
from bet where bet.match_id=".$id);
        $sth->execute();
        return $sth->fetch();
    }

    function getNow(){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sth = $dbh->prepare("SELECT NOW() as dateTime");
        $sth->execute();
        return $sth->fetch();
    }

    function calculateAccuracy($id){
        $dateNow = new DateTime();
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sth = $dbh->prepare("select * from user where id=".$id);
        $sth->execute();
        $user = $sth->fetch();
        $accuracy=0;
        if(!empty($user['accuracy_at']) and !empty($user['accuracy'])){
            $sql="select DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) as date_bet,bet.`status` from bet
inner join `match` on `match`.id=bet.match_id
where DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) > ".$user['accuracy_at']." and bet.user_id=".$id." and (bet.`status`='cheap' or bet.`status`='wrong') order by `match`.time_match ASC";
            $sth = $dbh->prepare($sql);
            $sth->execute();
            $bet = $sth->fetchAll();
            $accuracy=$user['accuracy'];
        }else{
            $sth = $dbh->prepare("select DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) as date_bet,bet.`status` from bet
inner join `match` on `match`.id=bet.match_id
where bet.user_id=? and (bet.`status`='cheap' or bet.`status`='wrong') order by `match`.time_match ASC");
            $sth->execute([$id]);
            $bet = $sth->fetchAll();
        }
        if(!empty($bet)){
            $date="";
            $win=0;
            $lose=0;
            foreach ($bet as $key=>$value){
                if($date!==$value['date_bet']){
                    if($win!=0 || $lose!=0){
                        $percent=(100-(((($win+$lose)-$win)/($win+$lose))*100));
                        if($accuracy!=0){
                            $accuracy=(($accuracy+$percent)/2);
                        }else{
                            $accuracy=$percent;
                        }
                    }
//                    echo "date: ".$date." accuracy:: ".$accuracy.'<br>';
                    $date=$value['date_bet'];
                    $win=0;
                    $lose=0;
                    if($value['status']=="cheap"){
                        $win=$win+1;
                    }elseif($value['status']=="wrong"){
                        $lose=$lose+1;
                    }
                }else{
                    if($value['status']=="cheap"){
                        $win=$win+1;
                    }elseif($value['status']=="wrong"){
                        $lose=$lose+1;
                    }
                }
            }
            if($win!=0 || $lose!=0){
                $percent=(100-(((($win+$lose)-$win)/($win+$lose))*100));
                if($accuracy!=0){
                    $accuracy=(($accuracy+$percent)/2);
                }else{
                    $accuracy=$percent;
                }
            }

            if($user['accuracy_half_at']!=date('Y-m-d') and date('j')==15 and (intval(date("H"))+7)>=12){
                $sql = "update user set user.accuracy_half=user.accuracy,user.accuracy_half_at=CURDATE()";
                $sth = $dbh->prepare($sql)->execute();
                $this->awardAccuracy();
            }elseif ($user['accuracy_month_at']!=date('Y-m-d') and date('j')==1 and (intval(date("H"))+7)>=12){
                $sql = "update user set user.accuracy_month=user.accuracy,user.accuracy_month_at=CURDATE()";
                $sth = $dbh->prepare($sql)->execute();
                $this->awardAccuracy();
            }

            $sql = "update `user` set accuracy=" . $accuracy . ",accuracy_at='" . $dateNow->format('Y-m-d') . "',updated_at='".$dateNow->format("Y-m-d H:i:s")."' where id=" . $id;
            $sth = $dbh->prepare($sql)->execute();
            if($sth){
                $_SESSION['login']['accuracy_at']=$dateNow->format('Y-m-d');
                $_SESSION['login']['updated_at']=$dateNow->format('Y-m-d H:i:s');
            }


        }
    }

    function checkBetComboContinuous($id){
        $count=0;
        $date="";
        $status="";
        $comboIncome=null;
        $listIncome="";
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sth = $dbh->prepare("select DATE(date_record) as date_record from log_income where log_income.user_id=? and log_income.income_derived='combo' order by log_income.date_record desc");
        $sth->execute([$id]);
        $comboIncome = $sth->fetch();
        if(!empty($comboIncome)){
            $sql="select B.* from bet inner join `match` on `match`.id=bet.match_id 
left join (select 
bet.user_id,bet.match_id,DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) as time_match,
sum(case bet.`status` when 'wrong' then 1 else 0 end) as `wrong` ,
sum(case bet.`status` when 'bet' then 1 else 0 end) as `bet`,
sum(case bet.`status` when 'cheap' then 1 else 0 end) as `cheap`,
sum(case bet.`status` when 'always' then 1 else 0 end) as `always` from bet
inner join `match` on `match`.id=bet.match_id 
where bet.user_id=".$id."
group by DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) 
order by DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) ASC) B ON B.time_match = DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR))
where bet.user_id=".$id." and bet.status!='bet' and DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR))>'".$comboIncome['date_record']."'
group by (DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR))) order by DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) DESC limit 10";
            $sth = $dbh->prepare($sql);
            $sth->execute();
        }else {
            $sql="select B.* from bet inner join `match` on `match`.id=bet.match_id 
left join (select 
bet.user_id,bet.match_id,DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) as time_match,
sum(case bet.`status` when 'wrong' then 1 else 0 end) as `wrong` ,
sum(case bet.`status` when 'bet' then 1 else 0 end) as `bet`,
sum(case bet.`status` when 'cheap' then 1 else 0 end) as `cheap`,
sum(case bet.`status` when 'always' then 1 else 0 end) as `always` from bet
inner join `match` on `match`.id=bet.match_id 
where bet.user_id=".$id."
group by DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) 
order by DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) ASC) B ON B.time_match = DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR))
where bet.user_id=".$id." and bet.status!='bet'
group by (DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR))) order by DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) DESC limit 10";
            $sth = $dbh->prepare($sql);
            $sth->execute();
        }

        $dataMatch = $sth->fetchAll(PDO::FETCH_ASSOC);
        if(!empty($dataMatch)){
            foreach (array_reverse($dataMatch) as $key=>$value){
                if($value['bet']==0) {
                    if (($value['wrong'] == 0 || $value['cheap'] == 0)) {
                        if ($value['wrong'] == 0) {
                            if ($status != "cheap") {
                                $status = "cheap";
                                $count = 0;
                            }
                            $date = $value['time_match'];
                            $count += ($value['cheap']);
                        } elseif ($value['cheap'] == 0) {
                            if ($status != "wrong") {
                                $status = "wrong";
                                $count = 0;
                            }
                            $date = $value['time_match'];
                            $count += ($value['wrong']);
                        }
                    } else {
                        $status = "";
                        $count = 0;
                    }
                }
            }
        }
        $dataReturn['countCombo']=$count;
        $dataReturn['statusCombo']=$status;
        $dataReturn['date']=$date;
        return $dataReturn;
    }

    function exchangeStep($id){
        $dateNow = new DateTime();
        $dataStep=$this->checkBetByDayBefore($id);
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $listIncome="";

        $sth = $dbh->prepare("select bet_id from log_income where log_income.income_derived='bet' and log_income.income_type='gold' and log_income.user_id=".$id." and log_income.date_record=CURDATE()");
        $sth->execute();
        $logIncomeBet = $sth->fetchAll(PDO::FETCH_COLUMN, 0);

        if(!empty($logIncomeBet)) {
            foreach ($logIncomeBet as $key => $value) {
                if ($key == 0) {
                    $listIncome .= "'" . $value . "'";
                } else {
                    $listIncome .= ",'" . $value . "'";
                }
            }
            $sth = $dbh->prepare("select sum(case bet.team when 'away' then ((gamble.home_water_bill-1)*bet.use_bet_coin) else ((gamble.away_water_bill-1)*bet.use_bet_coin) end) as `sumGold` from bet left join `match` on `match`.id=bet.match_id left join gamble on gamble.match_id=`match`.id where bet.user_id=".$id." and bet.id IN (".$listIncome.") order by bet.created_at ASC");
            $sth->execute();
            $logIncomeStep = $sth->fetch();
        }


        $sql="select * from `log_income` where date_record='".$dateNow->format('Y-m-d')."' and user_id=" . $id . " and income_derived='step'";
        $sth = $dbh->prepare($sql);
        $sth->execute();
        $log_income = $sth->fetch();

        if(empty($log_income) and !empty($logIncomeStep) and $dataStep['countStep']>=3){
            $user_id=$id;
            $befor_gold=$_SESSION['login']['gold'];
            $befor_coin=$_SESSION['login']['coin'];
            $befor_coupon=$_SESSION['login']['coupon'];
            $befor_lv=$_SESSION['login']['lv'];
            $befor_star=$_SESSION['login']['star'];
            $income_derived='step';
            $income_type='gold';
            $income_format='plus';
            $after_gold=($befor_gold+$logIncomeStep['sumGold']);
            $after_coin=$befor_coin;
            $after_coupon=$befor_coupon;
            $after_lv=$_SESSION['login']['lv'];
            $after_star=$_SESSION['login']['star'];
            $comment="ได้รับ ".($after_gold-$befor_gold)."gold จากการ แทง Step ".$dataStep['countStep']."คู่";
            $sql = "INSERT INTO `log_income` (`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
            $sql.=" VALUES (" . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "',".$after_gold.",".$after_coin.",".$after_coupon.",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
            $sth = $dbh->prepare($sql)->execute();
            if($sth){
                $_SESSION['login']['gold']=$after_gold;
                $sql="update `user` set gold=".$after_gold." where id=".$user_id;
                $sth = $dbh->prepare($sql)->execute();
            }
        }
    }

    function checkBetByDayBefore($id){
        $dataReturn=array();
        $countStep=0;
        $countStepWin=0;
        $countStepLose=0;
        $countStepAlways=0;
        $statusStep="";
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sth = $dbh->prepare("select bet.user_id,bet.match_id,DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) as time_match,sum(case bet.`status` when 'wrong' then 1 else 0 end) as `wrong`,sum(case bet.`status` when 'bet' then 1 else 0 end) as `bet`, sum(case bet.`status` when 'cheap' then 1 else 0 end) as `cheap`, sum(case bet.`status` when 'always' then 1 else 0 end) as `always` from bet inner join `match` on `match`.id=bet.match_id where bet.user_id=? group by DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) order by DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) DESC limit 1");
        $sth->execute([$id]);
        $dateBet = $sth->fetch(PDO::FETCH_ASSOC);
        if(!empty($dateBet)){
            if($dateBet['wrong']==0){
                $statusStep="cheap";
                $countStep = $dateBet['cheap'];
            }elseif ($dateBet['cheap']==0) {
                $statusStep="wrong";
                $countStep = $dateBet['wrong'];
            }else{
                $statusStep="";
                $countStep = 0;
            }
        }
        $dataReturn['countStepLose'] = $countStepLose;
        $dataReturn['countStepAlways'] = $countStepAlways;
        $dataReturn['countStepWin'] = $countStepWin;
        $dataReturn['countStep']=$countStep;
        $dataReturn['statusStep']=$statusStep;
        return $dataReturn;
    }

    function checkStatusBetByUser($id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sth = $dbh->prepare("select bet.`status`,count(*) from bet where bet.user_id=? group by bet.`status`");
        $sth->execute([$id]);
        $dateBetStatus = $sth->fetchAll(PDO::FETCH_COLUMN|PDO::FETCH_GROUP);
        return $dateBetStatus;
    }

    public function awardAccuracy($top=10){
        $award=[1000,900,800,700,600,500,400,300,200,100];
        $db = new DBPDO();
        $dbh = $db->connect();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select * from `user` order by user.gold DESC limit ".$top;
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $userList=$stmt->fetchAll();
            foreach ($userList as $key=>$value){
                $sql = "select id from log_income where log_income.date_record=CURDATE() and log_income.income_derived='awardAccuracy' and log_income.user_id=".$value['id'];
                $stmt = $dbh->prepare($sql);
                $stmt->execute();
                $logIncome=$stmt->fetch();
                if(empty($logIncome)) {
                    $user_id = $value['id'];
                    $befor_gold = $value['gold'];
                    $befor_coin = $value['coin'];
                    $befor_coupon = $value['coupon'];
                    $befor_lv = $value['lv'];
                    $befor_star = $value['star'];
                    $income_derived = "awardAccuracy";
                    $income_type = "gold";
                    $income_format = "plus";
                    $after_gold = ($befor_gold + $award[$key]);
                    $after_coin = $befor_coin;
                    $after_coupon = $befor_coupon;
                    $after_lv = $befor_lv;
                    $after_star = $befor_star;
                    $comment = "ได้รีบรางวัล " . $award[$key] . " gold จากการ ติดอับดับ Top Accuracy อันดับ " . ($key + 1);

                    $sql = "INSERT INTO `log_income` (`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`befor_lv`,`befor_star`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`after_lv`,`after_star`,`comment`,`date_record`,`created_at`,`updated_at`) ";
                    $sql .= "VALUES (:user_id,:befor_gold,:befor_coin,:befor_coupon,:befor_lv,:befor_star,:income_derived,:income_type,:income_format,:after_gold,:after_coin,:after_coupon,:after_lv,:after_star,:comment,:date_record,:updated_at,:created_at);";
                    $stmt = $dbh->prepare($sql);
                    $stmt->bindParam(':user_id', $user_id);
                    $stmt->bindParam(':befor_gold', $befor_gold);
                    $stmt->bindParam(':befor_coin', $befor_coin);
                    $stmt->bindParam(':befor_coupon', $befor_coupon);
                    $stmt->bindParam(':befor_lv', $befor_lv);
                    $stmt->bindParam(':befor_star', $befor_star);
                    $stmt->bindParam(':income_derived', $income_derived);
                    $stmt->bindParam(':income_type', $income_type);
                    $stmt->bindParam(':income_format', $income_format);
                    $stmt->bindParam(':after_gold', $after_gold);
                    $stmt->bindParam(':after_coin', $after_coin);
                    $stmt->bindParam(':after_coupon', $after_coupon);
                    $stmt->bindParam(':after_lv', $after_lv);
                    $stmt->bindParam(':after_star', $after_star);
                    $stmt->bindParam(':comment', $comment);
                    $stmt->bindParam(':date_record', date("Y-m-d"));
                    $stmt->bindParam(':created_at', date('Y-m-d H:i:s'));
                    $stmt->bindParam(':updated_at', date('Y-m-d H:i:s'));
                    if ($stmt->execute()) {
                        $sql = "update `user` set gold=" . $after_gold . ",updated_at='" . date("Y-m-d H:i:s") . "' where id=" . $value['id'];
                        $sth = $dbh->prepare($sql)->execute();
                    }
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
    }

    public function awardGold($top=10){
        $award=[1000,900,800,700,600,500,400,300,200,100];
        $db = new DBPDO();
        $dbh = $db->connect();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select * from `user` order by user.gold DESC limit ".$top;
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $userList=$stmt->fetchAll();
            foreach ($userList as $key=>$value){
                $sql = "select id from log_income where log_income.date_record=CURDATE() and log_income.income_derived='awardGold' and log_income.user_id=".$value['id'];
                $stmt = $dbh->prepare($sql);
                $stmt->execute();
                $logIncome=$stmt->fetch();
                if(empty($logIncome)) {
                    $user_id = $value['id'];
                    $befor_gold = $value['gold'];
                    $befor_coin = $value['coin'];
                    $befor_coupon = $value['coupon'];
                    $befor_lv = $value['lv'];
                    $befor_star = $value['star'];
                    $income_derived = "awardGold";
                    $income_type = "gold";
                    $income_format = "plus";
                    $after_gold = ($befor_gold + $award[$key]);
                    $after_coin = $befor_coin;
                    $after_coupon = $befor_coupon;
                    $after_lv = $befor_lv;
                    $after_star = $befor_star;
                    $comment = "ได้รีบรางวัล " . $award[$key] . " gold จากการ ติดอับดับ Top Gold อันดับ " . ($key + 1);

                    $sql = "INSERT INTO `log_income` (`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`befor_lv`,`befor_star`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`after_lv`,`after_star`,`comment`,`date_record`,`created_at`,`updated_at`) ";
                    $sql .= "VALUES (:user_id,:befor_gold,:befor_coin,:befor_coupon,:befor_lv,:befor_star,:income_derived,:income_type,:income_format,:after_gold,:after_coin,:after_coupon,:after_lv,:after_star,:comment,:date_record,:updated_at,:created_at);";
                    $stmt = $dbh->prepare($sql);
                    $stmt->bindParam(':user_id', $user_id);
                    $stmt->bindParam(':befor_gold', $befor_gold);
                    $stmt->bindParam(':befor_coin', $befor_coin);
                    $stmt->bindParam(':befor_coupon', $befor_coupon);
                    $stmt->bindParam(':befor_lv', $befor_lv);
                    $stmt->bindParam(':befor_star', $befor_star);
                    $stmt->bindParam(':income_derived', $income_derived);
                    $stmt->bindParam(':income_type', $income_type);
                    $stmt->bindParam(':income_format', $income_format);
                    $stmt->bindParam(':after_gold', $after_gold);
                    $stmt->bindParam(':after_coin', $after_coin);
                    $stmt->bindParam(':after_coupon', $after_coupon);
                    $stmt->bindParam(':after_lv', $after_lv);
                    $stmt->bindParam(':after_star', $after_star);
                    $stmt->bindParam(':comment', $comment);
                    $stmt->bindParam(':date_record', date("Y-m-d"));
                    $stmt->bindParam(':created_at', date('Y-m-d H:i:s'));
                    $stmt->bindParam(':updated_at', date('Y-m-d H:i:s'));
                    if ($stmt->execute()) {
                        $sql = "update `user` set gold=" . $after_gold . ",updated_at='" . date("Y-m-d H:i:s") . "' where id=" . $value['id'];
                        $sth = $dbh->prepare($sql)->execute();
                    }
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
    }

    function calculateBet($id){
        $dateNow = new DateTime();
        $dateLast = new DateTime();
        $dateLastBet = new DateTime();
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sth = $dbh->prepare("select * from user where id=".$id);
        $sth->execute();
        $user = $sth->fetch();

        $sth = $dbh->prepare("select min(bet.created_at) as maxDate from bet where status='bet' and user_id=".$user['id']);
        $sth->execute();
        $betLast = $sth->fetch();
        if(!empty($betLast['maxDate'])) {
            $dateLastBet->setTimestamp(strtotime($betLast['maxDate']));
            $betDateStart = $dateLastBet->format('Y-m-d');
            $sql="select bet.id as bet_id,`match`.`status` as match_status,`match`.`status` as match_status,bet.`status`,((`match`.home_end_time_score+gamble.handicap)-`match`.away_end_time_score) as homeScore,`match`.away_end_time_score,
CASE
    WHEN (`match`.home_end_time_score+gamble.handicap) > `match`.away_end_time_score and bet.team='home' THEN 'win'
    WHEN (`match`.home_end_time_score+gamble.handicap) > `match`.away_end_time_score and bet.team='away' THEN 'lose'
    WHEN (`match`.home_end_time_score+gamble.handicap) < `match`.away_end_time_score and bet.team='away' THEN 'win'
    WHEN (`match`.home_end_time_score+gamble.handicap) < `match`.away_end_time_score and bet.team='home' THEN 'lose'
    ELSE 'equal'
END as results
,tameHome.name_en,tameHome.path,tameAway.name_en,tameAway.path,bet.team,bet.`status`,bet.use_bet_coin,gamble.home_water_bill,gamble.away_water_bill,bet.id,bet.created_at from bet 
left join `match` on `match`.id=bet.match_id
left join team as tameHome on tameHome.id=`match`.team_home
left join team as tameAway on tameAway.id=`match`.team_away
left join gamble on gamble.match_id=`match`.id
where bet.`status`='bet' and bet.created_at>='" . $betDateStart . "' and bet.user_id=" . $user['id'] . " and `match`.away_end_time_score IS NOT NULL and `match`.home_end_time_score IS NOT NULL order by bet.created_at ASC";
            $sth = $dbh->prepare($sql);
            $sth->execute();
            $resultsBet = $sth->fetchAll();
            $betCount = 0;
            $betWin = 0;
            $betLose = 0;
            $betEqual = 0;
            $dateSelect = "";
            $statusHalf=false;
            $after_gold=$_SESSION['login']['gold'];
            $after_coin=$_SESSION['login']['coin'];
            if(!empty($resultsBet)) {
                foreach ($resultsBet as $key => $value) {
                    $statusHalf=false;
                    if ($value['match_status'] == "fullTime") {
                        if ($value['results'] == "win") {
                            if ($value['homeScore'] == 0.25 || $value['homeScore'] == (-0.25)) {
                                $statusHalf = true;
                            }
                            if ($value['status'] == "bet") {
                                $user_id = $_SESSION['login']['id'];
                                $befor_gold = $_SESSION['login']['gold'];
                                $befor_coin = $_SESSION['login']['coin'];
                                $befor_coupon = $_SESSION['login']['coupon'];
                                $befor_lv = $_SESSION['login']['lv'];
                                $befor_star = $_SESSION['login']['star'];
                                $income_derived = 'bet';
                                $income_type = 'gold';
                                $income_format = 'plus';
//                            สูตร คำนวน จ่าย  W คืน coin ที่เเทงมา G=(home_water_bill-1)*coin; * จ่ายคลึ้ง  G=((home_water_bill-1)/2)*coin;
                                if ($value['team'] == "home") {
                                    if ($statusHalf) {
                                        $after_gold = (intval($befor_gold) + (intval($value['use_bet_coin'] * (($value['home_water_bill']) - 1) / 2)));
                                        $after_coin = ($befor_coin + ($value['use_bet_coin']/2));
                                    } else {
                                        $after_gold = (intval($befor_gold) + (intval($value['use_bet_coin']) * (($value['home_water_bill']) - 1)));
                                        $after_coin = ($befor_coin + $value['use_bet_coin']);
                                    }
                                } else if ($value['team'] == "away") {
                                    if ($statusHalf) {
                                        $after_gold = (intval($befor_gold) + (intval($value['use_bet_coin'] * (($value['away_water_bill']) - 1) / 2)));
                                        $after_coin = ($befor_coin + ($value['use_bet_coin']/2));
                                    } else {
                                        $after_gold = (intval($befor_gold) + (intval($value['use_bet_coin']) * (($value['away_water_bill']) - 1)));
                                        $after_coin = ($befor_coin + $value['use_bet_coin']);
                                    }
                                }
                                $after_coupon = $befor_coupon;
                                $after_lv = $befor_lv;
                                $after_star = $befor_star;
                                $comment = "ได้รับ " . ($after_gold - $befor_gold) . "gold เเละ " . ($after_coin - $befor_coin) . "coin จากการ ทายผลฟุตบอล ถูก";
                                $sql = "INSERT INTO `log_income` (`bet_id`,`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                                $sql .= " VALUES (" . $value['bet_id'] . "," . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                                $sth = $dbh->prepare($sql)->execute();
                                if ($sth) {
                                    $_SESSION['login']['gold'] = $after_gold;
                                    $_SESSION['login']['coin'] = $after_coin;
                                    $sql = "update `bet` set status='cheap' where id=" . $value['id'];
                                    $sth = $dbh->prepare($sql)->execute();
                                }
                            }
                            $betWin++;
                        } else if ($value['results'] == "lose") {
                            if ($value['homeScore'] == (-0.25) || $value['homeScore'] == (0.25)) {
                                $user_id = $_SESSION['login']['id'];
                                $befor_gold = $_SESSION['login']['gold'];
                                $befor_coin = $_SESSION['login']['coin'];
                                $befor_coupon = $_SESSION['login']['coupon'];
                                $befor_lv = $_SESSION['login']['lv'];
                                $befor_star = $_SESSION['login']['star'];
                                $income_derived = 'bet';
                                $income_type = 'coin&gold';
                                $income_format = 'minus';
                                if ($value['homeScore'] == (-0.25) || $value['homeScore'] == (0.25)) {
                                    $after_gold = (intval($befor_gold) - (intval($value['use_bet_coin'] / 2)));
                                    $after_coin = (intval($befor_coin) + (intval($value['use_bet_coin'] / 2)));
                                }
                                $after_coupon = $befor_coupon;
                                $after_lv = $befor_lv;
                                $after_star = $befor_star;
                                $comment = "เสีย " . ($befor_gold - $after_gold) . "gold ได้รับ " . ($after_coin - $befor_coin) . "coin คืน จากการ ทายผลฟุตบอล ผ ิด";
                                $sql = "INSERT INTO `log_income` (`bet_id`,`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                                $sql .= " VALUES (" . $value['bet_id'] . "," . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                                $sth = $dbh->prepare($sql)->execute();
                                if ($sth) {
                                    $_SESSION['login']['coin'] = $after_coin;
                                    $_SESSION['login']['gold'] = $after_gold;
                                    $sql = "update `bet` set status='wrong' where id=" . $value['id'];
                                    $sth = $dbh->prepare($sql)->execute();
                                }
                            } else {
                                $user_id = $_SESSION['login']['id'];
                                $befor_gold = $_SESSION['login']['gold'];
                                $befor_coin = $_SESSION['login']['coin'];
                                $befor_coupon = $_SESSION['login']['coupon'];
                                $befor_lv = $_SESSION['login']['lv'];
                                $befor_star = $_SESSION['login']['star'];
                                $income_derived = 'bet';
                                $income_type = 'gold';
                                $income_format = 'minus';
                                $after_gold = (intval($befor_gold) - (intval($value['use_bet_coin'])));
                                $after_coin = $befor_coin;
                                $after_coupon = $befor_coupon;
                                $after_lv = $befor_lv;
                                $after_star = $befor_star;
                                $comment = "เสีย " . ($befor_gold - $after_gold) . "gold จากการ ทายผลฟุตบอล ผ ิด";
                                $sql = "INSERT INTO `log_income` (`bet_id`,`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                                $sql .= " VALUES (" . $value['bet_id'] . "," . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                                $sth = $dbh->prepare($sql)->execute();
                                if ($sth) {
                                    $_SESSION['login']['coin'] = $after_coin;
                                    $_SESSION['login']['gold'] = $after_gold;
                                    $sql = "update `bet` set status='wrong' where id=" . $value['id'];
                                    $sth = $dbh->prepare($sql)->execute();
                                }
                            }
                            if ($value['status'] == "bet") {
                                $sql = "update `bet` set status='wrong' where id=" . $value['id'];
                                $sth = $dbh->prepare($sql)->execute();
                            }
                            $betLose++;
                        } else if ($value['results'] == "equal") {
                            if ($value['status'] == "bet") {
                                $user_id = $_SESSION['login']['id'];
                                $befor_gold = $_SESSION['login']['gold'];
                                $befor_coin = $_SESSION['login']['coin'];
                                $befor_coupon = $_SESSION['login']['coupon'];
                                $befor_lv = $_SESSION['login']['lv'];
                                $befor_star = $_SESSION['login']['star'];
                                $income_derived = 'bet';
                                $income_type = 'gold';
                                $income_format = 'plus';
                                $after_gold = $befor_gold;
                                $after_coin = ($befor_coin + $value['use_bet_coin']);
                                $after_coupon = $befor_coupon;
                                $after_lv = $befor_lv;
                                $after_star = $befor_star;
                                $comment = "ได้รับ " . ($after_coin - $befor_coin) . "coin คืน จากการ ทายผลฟุตบอล เสมอ";
                                $sql = "INSERT INTO `log_income` (`bet_id`,`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                                $sql .= " VALUES (" . $value['bet_id'] . "," . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                                $sth = $dbh->prepare($sql)->execute();
                                if ($sth) {
                                    $_SESSION['login']['coin'] = $after_coin;
                                    $_SESSION['login']['gold'] = $after_gold;
                                }
                                $sql = "update `bet` set status='always' where id=" . $value['id'];
                                $sth = $dbh->prepare($sql)->execute();
                            }
                            $betEqual++;
                        }
                    }
                    $betCount++;
                }
//            update coin&gold User
                if(($_SESSION['login']['gold'] != $user['gold']) || $_SESSION['login']['coin'] != $user['coin']) {
                    $_SESSION['login']['gold'] = $after_gold;
                    $_SESSION['login']['coin'] = $after_coin;

                    if($user['gold_half_at']!=date('Y-m-d') and date('j')==15 and (intval(date("H"))+7)>=12){
                        $sql = "update user set user.gold_half=user.gold,user.gold_half_at=CURDATE()";
                        $sth = $dbh->prepare($sql)->execute();
                        $this->awardGold();
                    }elseif ($user['gold_month_at']!=date('Y-m-d') and date('j')==1 and (intval(date("H"))+7)>=12){
                        $sql = "update user set user.gold_month=user.gold,user.gold_month_at=CURDATE()";
                        $sth = $dbh->prepare($sql)->execute();
                        $this->awardGold();
                    }

//                    if(date('j')==5){
//                        $sql = "update `user` set gold_half=" . $after_gold . ",updated_at='" . date("Y-m-d H:i:s") . "' where id=" . $id;
//                        $sth = $dbh->prepare($sql)->execute();
//                    }elseif (date('j')==1){
//                        $sql = "update `user` set gold_half=" . $after_gold . ",updated_at='" . date("Y-m-d H:i:s") . "' where id=" . $id;
//                        $sth = $dbh->prepare($sql)->execute();
//                    }

                    $sql = "update `user` set coin=" . $after_coin . ",gold=" . $after_gold . ",updated_at='" . date("Y-m-d H:i:s") . "' where id=" . $id;
                    $sth = $dbh->prepare($sql)->execute();
                }
            }
        }
    }

    function getWeekByUserId($user_id){
        $dataReturn=array();
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sth = $dbh->prepare("select DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) as date_bet from bet
inner join `match` on `match`.id=bet.match_id
inner join `gamble` on `gamble`.match_id=bet.match_id
where bet.user_id=:user_id group by DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) order by `match`.time_match desc limit 6");
            $sth->bindParam(':user_id', $user_id);
            $sth->execute();
            $dataBet = $sth->fetchAll(PDO::FETCH_ASSOC);
            foreach ($dataBet as $key=>$value){
                $sth = $dbh->prepare("select DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR)) as date_bet,home_end_time_score,away_end_time_score,teamHome.name_en as thm,teamAway.name_en as tam,`gamble`.handicap,`gamble`.home_water_bill,`gamble`.away_water_bill,bet.* from bet
inner join `match` on `match`.id=bet.match_id
inner join `gamble` on `gamble`.match_id=bet.match_id
left join team as teamHome on teamHome.id=`match`.team_home
left join team as teamAway on teamAway.id=`match`.team_away 
where DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR))=:dateMath and bet.user_id=:user_id order by `match`.time_match desc");
                $sth->bindParam(':dateMath', $value['date_bet']);
                $sth->bindParam(':user_id', $user_id);
                $sth->execute();
                $dataMathList = $sth->fetchAll(PDO::FETCH_ASSOC);

                if(!empty($dataMathList)) {
                    foreach ($dataMathList as $keyMathList=>$valueMathList) {
                        if (empty($dataReturn[$value['date_bet']])) {
                            $dataReturn[$value['date_bet']] = array();
                        }
                        array_push($dataReturn[$value['date_bet']], $valueMathList);
                    }
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $dataReturn;
    }

    function addBet($user_id,$match_id,$team,$use_bet_coin,$use_bet_gold,$use_bet_coupon,$status){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "INSERT INTO `bet` (`user_id`,`match_id`, `team`, `use_bet_coin`,`use_bet_gold`,`use_bet_coupon`,`status`,`created_at`,`updated_at`) VALUES (:user_id,:match_id,:team,:use_bet_coin,:use_bet_gold,:use_bet_coupon,:status,:updated_at,:created_at);";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':match_id', $match_id);
            $stmt->bindParam(':team', $team);
            $stmt->bindParam(':use_bet_coin', $use_bet_coin);
            $stmt->bindParam(':use_bet_gold', $use_bet_gold);
            $stmt->bindParam(':use_bet_coupon', $use_bet_coupon);
            $stmt->bindParam(':status', $status);
            $stmt->bindParam(':created_at', date('Y-m-d H:i:s'));
            $stmt->bindParam(':updated_at', date('Y-m-d H:i:s'));
            $stmt->execute();
            $comment=$dbh->lastInsertId();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $comment;
    }

    function getTeamMatchById($user_id,$match_id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $user=array();
        try {
            $sql = "select bet.team from `bet` where `user_id`=:user_id and `match_id`=:match_id";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':match_id', $match_id);
            $stmt->execute();
            $bet=$stmt->fetch();
            if(!empty($bet)) {
                return $bet['team'];
            }else {
                return false;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $bet;
    }

    function getBetMatchById($user_id,$match_id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $user=array();
        try {
            $sql = "select * from `bet` where `user_id`=:user_id and `match_id`=:match_id";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':match_id', $match_id);
            $stmt->execute();
            $bet=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $bet;
    }

}
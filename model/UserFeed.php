<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 9/25/2018
 * Time: 10:33 AM
 */
include_once "DBPDO.php";

class UserFeed
{
    public function userFeedBetAndReview($limit=10){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select `user`.name,`user`.fb_uid,user_feed.* from user_feed inner join `user` on `user`.id=user_feed.user_id WHERE user_feed.event='bet' or user_feed.event='review' order by created_at DESC limit ?";
            $stmt = $dbh->prepare($sql);
            $stmt->execute([$limit]);
            $userFeed=$stmt->fetchAll();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $userFeed;
    }

    public function addUserFeedMatch($user_id,$match_id,$event,$message){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "INSERT INTO `user_feed` (`user_id`, `match_id`, `event`, `message`, `IP`,`created_at`,`updated_at`) VALUES (:user_id,:match_id,:event,:message,:IP,:created_at,:updated_at);";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':match_id', $match_id);
            $stmt->bindParam(':event', $event);
            $stmt->bindParam(':message', $message);
            $stmt->bindParam(':IP', $this->getUserIpAddr());
            $stmt->bindParam(':created_at', date('Y-m-d H:i:s'));
            $stmt->bindParam(':updated_at', date('Y-m-d H:i:s'));
            $stmt->execute();
            $feed=$stmt->fetch();
            $this->writeFileUserFeed();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $feed;
    }

    public function addUserFeed($user_id,$event,$message){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "INSERT INTO `user_feed` (`user_id`, `event`, `message`, `IP`,`created_at`,`updated_at`) VALUES (:user_id,:event,:message,:IP,:created_at,:updated_at);";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':event', $event);
            $stmt->bindParam(':message', $message);
            $stmt->bindParam(':IP', $this->getUserIpAddr());
            $stmt->bindParam(':created_at', date('Y-m-d H:i:s'));
            $stmt->bindParam(':updated_at', date('Y-m-d H:i:s'));
            $stmt->execute();
            $feed=$stmt->fetch();
            $this->writeFileUserFeed();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $feed;
    }

    public function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function writeFileUserFeed(){
        if (file_exists($_SERVER['DOCUMENT_ROOT'] .'/json/userFeed.json')) {
            $dataReturn=[];
            $userFeed=$this->userFeedBetAndReview(30);
            foreach ($userFeed as $key=>$value){
                $dataReturn[$value['id']]='<div class="box-feed">
                    <a class="link_wrap" href="/match/'.$value['match_id'].'.html"></a>
                    <ul>
                        <li>
                            <div class="image-user">
                                <img src="https://graph.facebook.com/v2.8/'.$value['fb_uid'].'/picture?type=large">
                            </div>
                        </li>
                        <li>
                            <h4>'.$value['name'].'</h4>
                            <span>'.date("D, G:i", strtotime($value['created_at'])).'</span>
                            <p>'.$value['message'].'</p>
                        </li>
                    </ul>
                </div>';
            }
            file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/json/userFeed.json', json_encode($dataReturn));
        }
    }
}
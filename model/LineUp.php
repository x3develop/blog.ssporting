<?php
namespace model;

require_once __DIR__ . "/../bootstrap.php";
use Illuminate\Database\Eloquent\Model;

class LineUp extends Model
{
    protected $table = "match_lineup";
    protected $fillable = ['mid', 'side', 'porder', 'name', 'number', 'position', 'created_at', 'updated_at'];

    public function match()
    {
        return $this->BelongsTo(MatchElo::class, "id", "mid");
    }
}

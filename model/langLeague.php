<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 11/18/2016
 * Time: 16:32
 */

include_once $_SERVER["DOCUMENT_ROOT"] . "/model/DBPDO.php";

class langLeague
{
    public function getNameLeague($id){
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array();
        $query="select leagueNameTh from lang_league where lang_league.leagueId=".$id;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($query, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs[0]->leagueNameTh;
    }

    public function getNameTeam($id){
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array();
        $query="select teamNameTh from lang_team where lang_team.tid=".$id;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($query, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs[0]->teamNameTh;
    }
}
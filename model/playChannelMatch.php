<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 5/24/2018
 * Time: 12:46
 */
include_once "DBPDO.php";

class playChannelMatch
{
    public function getChannel($match_7m_id){
        $dbpdo = new DBPDO();
        $match = array();
        $params = array($match_7m_id);
        $sql = "select channel_match.id,channel_match.link,channel_match.match_id,channel.channel_name,channel.channel_path from channel_match
inner join channel on channel.id=channel_match.channel_id
 WHERE match_id=? order by channel_match.id DESC";
        try {
            $dbpdo->connect2();
            $match = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $match;
    }
}
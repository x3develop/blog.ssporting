<?php

/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 17/2/2560
 * Time: 9:33 น.
 */
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/DBPDO.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";

class UserM
{


    function updateUser($fb_uid, $fb_email, $fb_firstname, $fb_middle_name, $fb_lastname, $fb_name, $fb_username, $display_name)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($fb_uid, $fb_email, $fb_firstname, $fb_middle_name, $fb_lastname, $fb_name, $fb_username, $display_name);
        $sql = "INSERT INTO `facebook_user` (`fb_uid`, `fb_email`, `fb_firstname`, `fb_middle_name`, `fb_lastname`, `fb_name`, `fb_username`, `display_name`)
VALUES (?, ?, ?, ?, ?, ?, ?, ?)
ON DUPLICATE KEY UPDATE `last_login_date` = CURRENT_DATE";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->insert($sql, $params);
            @session_start();
            $_SESSION['fb_uid'] = $fb_uid;
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getUser($fbuid)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($fbuid);
        $sql = "SELECT * FROM facebook_user fu
WHERE fu.fb_uid=? LIMIT 1";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getUserPack($uid)
    {
        $result = array("user" => [], "keylist" => array("user" => []));
        $rs = $this->getUser($uid);
        $keycount = 0;
        foreach ($rs as $k => $v) {
            foreach ($v as $key => $val) {
                $result["user"][] = $val;
                if ($k == 0) {
                    $result["keylist"]["user"][$key] = $keycount;
                    $keycount++;
                }

            }
        }


        return $result;

    }

    function getBetList($fbuid, $limit = 100)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($fbuid, $limit);
        $sql = "SELECT b.betId,b.hid,b.gid,b.leagueId,b.FT_score,b.choose,b.betHdp,b.result,b.betResult,lm.hn,lm.gn,lm.showDate,b.mid FROM bet b
RIGHT JOIN live_match lm ON lm.mid = b.mid
WHERE b.fb_uid=?
GROUP BY b.betId
ORDER BY b.betId DESC LIMIT ?";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getBetListPack($fbuid, $limit = 100)
    {
        $result = array("betlist" => [], "keylist" => array("betlist" => []));
        $rs = $this->getBetList($fbuid, $limit);
        $keycount = 0;
        foreach ($rs as $k => $v) {
            foreach ($v as $key => $val) {
                $result["betlist"][$k][] = $val;
                if ($k == 0) {
                    $result["keylist"]["betlist"][$key] = $keycount;
                    $keycount++;
                }

            }
        }
        return $result;

    }

    function updateCoinBalance($fbuid, $type, $amount)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($amount, $fbuid);
        $sql = "UPDATE facebook_user fu SET fu.scoin=fu.scoin+?
WHERE fu.fb_uid = ?
";
        if ($type == 'decrease') {
            $sql = "UPDATE facebook_user fu SET fu.scoin=fu.scoin-?
WHERE fu.fb_uid = ?
AND fu.scoin > ?";
            $params = array($amount, $fbuid, $amount);
        }
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->update($sql, $params);
            if ($rs) {

            }

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function updateDiamondBalance($fbuid, $type, $amount)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($amount, $fbuid);
        $sql = "UPDATE facebook_user fu SET fu.diamond=fu.diamond+?
WHERE fu.fb_uid = ?
";
        if ($type == 'decrease') {
            $sql = "UPDATE facebook_user fu SET fu.diamond=fu.diamond-?
WHERE fu.fb_uid = ?
";
        }
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->update($sql, $params);
            if ($rs) {

            }

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }


    function updateXVal($fbuid, $col, $val)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($val, $fbuid);
        $sql = "UPDATE facebook_user fu SET {$col}=?
WHERE fu.fb_uid = ?
";

//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->update($sql, $params);
            if ($rs) {

            }

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getTrophy($fbuid)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array($fbuid);
        $sql = "SELECT *, count(a.trophy_id) as quantity FROM achieve a
LEFT JOIN trophy2 t ON a.trophy_id=t.id
WHERE a.fb_uid=?
GROUP BY a.trophy_id";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getTrophyPack($fbuid)
    {
        $result = array("trophy" => [], "keylist" => array("trophy" => []));
        $rs = $this->getTrophy($fbuid);
        $keycount = 0;
        foreach ($rs as $k => $v) {
            foreach ($v as $key => $val) {
                $result["trophy"][$k][] = $val;
                if ($k == 0) {
                    $result["keylist"]["trophy"][$key] = $keycount;
                    $keycount++;
                }
            }
        }
        return $result;

    }

}
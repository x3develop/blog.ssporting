<?php

namespace model;

require_once __DIR__ . "/../bootstrap.php";

use Illuminate\Database\Eloquent\Model;

class UserElo extends Model
{
    protected $table = "user";
    protected $fillable = ['fb_uid', 'name', 'email', 'coin', 'gold', 'coupon', 'accuracy', 'lv', 'star', 'last_login', 'created_at', 'updated_at'];
    protected $hidden = ['password'];

//    public function match()
//    {
//        return $this->BelongsTo(MatchElo::class, "match_id", "id");
//    }

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NewsM
 *
 * @author xsiwa
 */
include_once "DBPDO.php";
include_once $_SERVER["DOCUMENT_ROOT"].'/utility/StringUtility.php';

class NewsM
{
    public function getAllNewsCountPage($category, $groupnews, $datenews, $page = 9)
    {
        //cat:[other,thai,'premier league','la liga','bundesliga','serie a','league 1'
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                        CEILING(count(*)/$page) as maxPage
                   from 
                           news
                   where
                           category = ?
                           and
                           groupNews = ?	
                           and
                           createDatetime >= ?
                   order by 
                           createDatetime desc";
            $params = array($category, $groupnews, $datenews);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getAllNewsByParaWeek($category, $groupnews,$limit)
    {
        //cat:[other,thai,'premier league','la liga','bundesliga','serie a','league 1'
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                        `newsid`,
                        `titleTh`,
                        `createDatetime`,
                        `imageLink`,
                        `readCount`,
                        `groupNews`,
                        `news_tag`,
                        `category`
                   from 
                           news
                   where
                           category = ?
                           and
                           groupNews = ?	
                           and
                           `createDatetime` > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 7 DAY))
                   order by 
                           readCount desc
                   limit ?";
            $params = array($category, $groupnews,$limit);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getAllNewsByPara($category, $groupnews, $datenews, $limit, $offset = 0)
    {
        //cat:[other,thai,'premier league','la liga','bundesliga','serie a','league 1'
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                        `newsid`,
                        `titleTh`,
                        `createDatetime`,
                        `imageLink`,
                        `readCount`,
                        `groupNews`,
                        `news_tag`,
                        `category`
                   from 
                           news
                   where
                           category = ?
                           and
                           groupNews = ?	
                           and
                           createDatetime >= ?
                   order by 
                           createDatetime desc
                   limit ? offset ?";
            $params = array($category, $groupnews, $datenews, $limit, $offset);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    function getRelatedNews($tag, $limit = 6)
    {
        $dbpdo = new DBPDO();
        $return = array();
        $taglist = explode(",", $tag);
        $condition = " WHERE `news_tag` LIKE '%%'";
        foreach ($taglist as $key => $val) {
            $val = trim($val);
            if ($key == 0) {
                $condition = " WHERE `news_tag` LIKE '%$val%' ";
            } else {
                $condition .= " OR `news_tag` LIKE '%$val%' ";
            }
        }
        try {
            $dbpdo->connect();
            $sql = "select
                        `newsid`,
                        `titleTh`,
                        `createDatetime`,
                        `imageLink`,
                        `readCount`,
                        `groupNews`,
                        `news_tag`
                   from
                           news

                        $condition
                   order by
                           createDatetime desc
                   limit ? ";
            $params = array($limit);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    function getRelatedNewsByIdNews($newsid,$tag="", $limit = 6)
    {
        $dbpdo = new DBPDO();
        $return = array();
        $condition = " WHERE `newsid` < $newsid ";
        if($tag!=="") {
            $taglist = explode(",", $tag);
            $condition .= ' and (';
            foreach ($taglist as $key => $val) {
                $val = trim($val);

                if ($key == 0) {
                    $condition .= " `news_tag` LIKE '%$val%' ";
                } else {
                    $condition .= " OR `news_tag` LIKE '%$val%' ";
                }
            }
            $condition .= ')';
        }
        try {
            $dbpdo->connect();
            $sql = "select
                        `newsid`,
                        `titleTh`,
                        `createDatetime`,
                        `imageLink`,
                        `readCount`,
                        `groupNews`,
                        `news_tag`
                   from
                           news
                        $condition
                   order by
                           createDatetime desc
                   limit ? ";
            $params = array($limit);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    function getListByIdNewsByDay($newsid, $day)
    {
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                        `newsid`
                   from
                           news
                   WHERE
                        `newsid` != ?
                   and
                      `createDatetime` > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL ? DAY))
                   order by
                           createDatetime desc";
            $params = array($newsid,$day);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    function getListByIdNews($newsid, $limit)
    {
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                        `newsid`
                   from
                           news
                   WHERE
                        `newsid` < ?
                   order by
                           createDatetime desc 
                           limit ?";
            $params = array($newsid, $limit);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    function getNewsById($newsid)
    {
        $stringUtil = new StringUtility();
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                        `newsid`,
                        `titleTh`,
                        `shortDescriptionTh`,
                        `contentTh`,
                        `createDatetime`,
                        `updateDatetime`,
                        `imageLink`,
                        `readCount`,
                        `category`,
                        `groupNews`,
                        `news_tag`,
                        `typeVideo`,
                        `linkVideo`
                   from
                           news
                   WHERE
                        `newsid`=?
                   order by
                           createDatetime desc";
            $params = array($newsid);
            $return = $dbpdo->query($sql, $params);
            $return = $this->getUrlIframe($return);
//            $return[0]->videokey = $stringUtil->getKeyFromVideoURL($return[0]->typeVideo, $return[0]->linkVideo);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    function getUrlIframe($return)
    {
        $stringUtil = new StringUtility();
        for ($i = 0; $i < count($return); ++$i) {
            if(!empty($return[$i]->typeVideo) and !empty($return[$i]->linkVideo)) {
                $strKey = $stringUtil->getKeyFromVideoURL($return[$i]->typeVideo, $return[$i]->linkVideo);
                if ($return[$i]->typeVideo == "dailymotion") {
                    $return[$i]->videokey = $strKey;
                    $return[$i]->urlIframe = 'http://www.dailymotion.com/embed/video/' . $return[$i]->videokey . '?autoplay=1';
                    if (!empty($return[$i]->thumbnail)) {
                        $return[$i]->urlImg = $return[$i]->thumbnail;
                    } else {
                        $return[$i]->urlImg = 'http://www.dailymotion.com/thumbnail/video/' . $return[$i]->videokey;
                    }
                } elseif ($return[$i]->typeVideo == "twitter") {
                    $dataUrl = explode('/', $return[$i]->linkVideo);
                    //index =5 url
                    $return[$i]->videokey = $dataUrl[5];
                    $return[$i]->urlIframe = 'https://twitter.com/i/cards/tfw/v1/' . $return[$i]->videokey . '?autoplay=1';
                    if (!empty($return[$i]->thumbnail)) {
                        $return[$i]->urlImg = $return[$i]->thumbnail;
                    } else {
                        $return[$i]->urlImg = '/images/icon/twitter-video.png';
                    }
                } elseif ($return[$i]->typeVideo == "facebook") {
                    $dataUrl = explode('/', $return[$i]->linkVideo);
                    //index =5 url
                    $return[$i]->videokey = $dataUrl[5];
                    $return[$i]->urlIframe = 'https://www.facebook.com/v2.3/plugins/video.php?autoplay=true&href=' . $return[$i]->linkVideo;

                    if (!empty($return[$i]->thumbnail)) {
                        $return[$i]->urlImg = $return[$i]->thumbnail;
                    } else {
                        $return[$i]->urlImg = 'https://graph.facebook.com/' . $return[$i]->videokey . '/picture';
                    }
                }else if($return[$i]->typeVideo == "streamable"){
                    $return[$i]->videokey = $strKey;
                    $return[$i]->urlIframe = 'https://streamable.com/s/'.$return[$i]->videokey . '?autoplay=1';
                    $return[$i]->stopUrlIframe=$return[$i]->videokey;
                    if(!empty($return[$i]->thumbnail)){
                        $return[$i]->urlImg =$return[$i]->thumbnail;
                    }else {
                        $return[$i]->urlImg = 'https://cf-e2.streamablevideo.com/image/' . $return[$i]->videokey . '.jpg';
                    }
                } else {
                    $return[$i]->videokey = $strKey;
                    $return[$i]->urlIframe = 'https://www.youtube.com/embed/' . $return[$i]->videokey . '?autoplay=1';
                    if (!empty($return[$i]->thumbnail)) {
                        $return[$i]->urlImg = $return[$i]->thumbnail;
                    } else {
                        $return[$i]->urlImg = 'https://img.youtube.com/vi/' . $return[$i]->videokey . '/1.jpg;';
                    }
                }
                $return[$i]->desc = str_replace('”', '', str_replace('“', '', str_replace('"', '', str_replace("'", "", $return[$i]->desc))));
                $return[$i]->title = str_replace('”', '', str_replace('“', '', str_replace('"', '', str_replace("'", "", $return[$i]->title))));
            }
        }
        return $return;
    }


    function getRecommendNews($cat, $limit = 6)
    {
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                        `newsid`,
                        `titleTh`,
                        `createDatetime`,
                        `imageLink`,
                        `readCount`,
                        `groupNews`,
                        `news_tag`,
                        `category`
                   from
                           news
                   WHERE
                        `category` <> ?
                   AND 
                   `createDatetime` > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 3 DAY))
                   order by
                           RAND(),createDatetime desc
                   limit ? ";

            $params = array($cat, $limit);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $return;
    }

    function updateView($newid)
    {
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "UPDATE `news` SET `readCount`=`readCount`+1 WHERE  `newsId`=?";
            $params = array($newid);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
    }

}

<?php

/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 14/2/2560
 * Time: 13:11 น.
 */
include_once $_SERVER["DOCUMENT_ROOT"] . "/model/DBPDO.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/model/MatchM.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";

class OddsM
{
    public function getRecommendOdds($matchlist)
    {
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array();
        $sql = "SELECT lm.mid,lm.lid,lm._lid,lm.hid,lm.gid,lm.hn,lm.gn,lm.`date`,o7.hdp,o7.hdp_home,o7.hdp_away,o7.`type`,ll.priority FROM live_match lm
RIGHT JOIN matches_map mm ON mm.f24_mid=lm.mid
RIGHT JOIN odds_7m o7 ON o7.match_id=mm.o7m_mid
RIGHT JOIN lang_league ll ON ll.leagueId=lm._lid
WHERE mm.f24_mid IN ($matchlist)
AND lm.sid = 1
ORDER BY lm.showDate, ll.priority,o7.`type`
";
//        echo $sql;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }

    function getRecommendOddsPack($limit = 10)
    {
        $result = array("match" => [], "keylist" => array("match" => []));
        $matchm = new MatchM();
        $dt = \Carbon\Carbon::now();
        $mlist = "0";
        $matchlist = $matchm->getMatchByDate($dt->toDateString());
        foreach ($matchlist as $key => $match) {
//            if ($match->sid == 1) {
                $mlist .= "," . $match->mid;
//            }
        }
        $rs = $this->getRecommendOdds($mlist);
        $keycount = 0;
        foreach ($rs as $k => $v) {
            $result["match"][$v->mid] = [];
            foreach ($v as $key => $val) {
                $result["match"][$v->mid][] = $val;
                if ($k == 0) {
                    $result["keylist"]["match"][$key] = $keycount;
                    $keycount++;
                }
            }
            if (count($result["match"]) == $limit) {
                break;
            }
        }


        return $result;

    }

    public function getOdd($day)
    {
        $dt = Carbon::now();
        if (empty($day)) {
            $day = $dt->toDateString();
        }
        $fileman = new FileManager();
        $fileman->setPath("cache");
        $fileman->setFilename("odds_recommend.json");
        $needupdate = true;
        $oddslist = array();
        if (file_exists($fileman->getFullPath())) {
            $mt = \Carbon\Carbon::createFromTimestamp(filemtime($fileman->getFullPath()));
            if ($dt->diffInMinutes($mt) < 5) {
                $needupdate = false;
            } else {
                $needupdate = true;
            }
        }
        if ($needupdate) {
            $dbpdo = new DBPDO();
            $rs = array();
            $sql = "SELECT o7.match_id,o7.`type`,o7.hdp,o7.hdp_home,o7.hdp_away,mm.f24_mid FROM matches_map mm
RIGHT JOIN odds_7m o7 ON o7.match_id = mm.o7m_mid
WHERE mm.showdate=CURRENT_DATE
ORDER BY o7.match_id
";

            try {
                $dbpdo->connect();
                $rs = $dbpdo->query($sql, array());
                foreach ($rs as $key => $val) {
                    $arr = [];
                    foreach ($val as $attr => $v) {
                        $arr[] = $v;
                    }
                    $oddslist[$val->f24_mid] = $arr;
                }
                $fileman->setContent(json_encode($oddslist));
                $fileman->write();

            } catch (Exception $e) {
                echo $e->getMessage();
            } finally {
                $dbpdo->close();
            }
        } else {
            $oddslist = json_decode($fileman->read(), true);
        }
        return $oddslist;
    }


}
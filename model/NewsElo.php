<?php
namespace model;

require_once __DIR__ . "/../bootstrap.php";
use Illuminate\Database\Eloquent\Model;

class NewsElo extends Model
{
    protected $table = "news";
    protected $primaryKey = 'newsid';
    protected $fillable = ['titleTh', 'shortDescriptionTh', 'contentTh', 'imageLink', 'like', 'readCount', 'recommentNews', 'category', 'groupNews', 'report', 'worldcups', 'news_tag', 'linkVideo', 'typeVideo', 'created_at', 'updated_at'];

}

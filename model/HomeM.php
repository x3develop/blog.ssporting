<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HomeM
 *
 * @author xsiwa
 */

require_once 'DBPDO.php';

class HomeM
{
    //put your code here

    public function getTenMatch()
    {
        //england 43948,ger 44352,spain 45141,italy 45146,fr 43893,th 43105,cl 44042, eu 44094, wc 44465
        $dbpdo = new DBPDO();
        $dbpdo->connect();

        $sql = "select 
                        lm.`hid`,
                        lm.`gid`,
                        lm.`hn`,
                        lm.`gn`,
                   lm.`date`,
                   lm.`mid`,
                   lm.`sid`,
                   lm.`s1`,
                   lm.`s2`
                from
                        live_match lm
                where
                        lm._lid IN (?,?,?,?,?,?,?,?,?)
                AND
                        lm.`new`=?
                order by 
                        lm.`showDate` desc
                limit 30";
        $params = array(43948, 44352, 45141, 45146, 43893, 43105, 44042, 44094, 44465, 'Y');
        $return = $dbpdo->query($sql, $params);
        $dbpdo->close();

        return $return;
    }

    public function getHeightLightMatch()
    {

        $dbpdo = new DBPDO();
        $dbpdo->connect();

        $sql = "select 
                        lm.`hid`,
                        lm.`gid`,
                        lm.`hn`,
                        lm.`gn`,
                   lm.`date`,
                   lm.`mid`
                from
                        live_match lm
                where
                        lm.showDate = Date(now())
                order by 
                        lm.mid desc
                limit 10";
        $params = array();
        $return = $dbpdo->query($sql, $params);
        $dbpdo->close();

        return $return;
    }

    public function getTopRanking()
    {

        $arrReturn = array();

        for ($i = 0; $i < 10; ++$i) {

            $arrdata = array();
            $arrdata[] = $i;
            $arrdata[] = "Name : $i";
            $arrdata[] = 900 + $i;

            $arrReturn[] = $arrdata;
        }
        return $arrReturn;
    }

    public function getGoldRanking()
    {

        $arrReturn = array();

        for ($i = 0; $i < 10; ++$i) {

            $arrdata = array();
            $arrdata[] = $i;
            $arrdata[] = "Name : $i";
            $arrdata[] = 900 + $i;

            $arrReturn[] = $arrdata;
        }
        return $arrReturn;
    }

    public function getAllRanking()
    {

        $arrReturn = array();

        for ($i = 0; $i < 10; ++$i) {

            $arrdata = array();
            $arrdata[] = $i;
            $arrdata[] = "Name : $i";
            $arrdata[] = 900 + $i;

            $arrReturn[] = $arrdata;
        }
        return $arrReturn;
    }

    public function getHotNews()
    {

        $str = "ยินดีต้อนร้บสู่เว็บทายผลบอลอันดับ 1 ของประเทศ ร่วมทายผลจากแมทซ์แข่งขันจริงพร้อมแจกเงินสดมูลค่ากว่า 10,000 บาททุกสัปดาห์";
        return $str;
    }

    public function getHomeNews()
    {

        $arrReturn = array();

        for ($i = 0; $i < 10; ++$i) {

            $arrdata = array();
            $arrdata[] = "News topic : $i";
            $arrdata[] = "News images : $i";

            $arrReturn[] = $arrdata;
        }
        return $arrReturn;
    }

    public function getRecommentNews()
    {

        $dbpdo = new DBPDO();
        $dbpdo->connect();

        $sql = "select 
                        `newsid`,
                        `titleTh`,
                        `createDatetime`,
                        `imageLink`,
                        `readCount`,
                        `recommentNews`
                from
                        news
                where
                        recommentNews > 0
                order by
                         `newsid` DESC
                limit
                        5";
        $params = array();
        $return = $dbpdo->query($sql, $params);
        $dbpdo->close();

        return $return;
    }

    public function getLastestNews()
    {

        $dbpdo = new DBPDO();
        $dbpdo->connect();

        $sql = "select 
                        `newsid`,
                        `titleTh`,
                        `createDatetime`,
                        `imageLink`,
                        `readCount`,
                        `recommentNews`
                from
                        news
                where
                        `recommentNews` = 0
                order by
                        createDatetime desc 
                limit
                        8";
        $params = array();
        $return = $dbpdo->query($sql, $params);
        $dbpdo->close();

        return $return;
    }

    public function getMostPopularNews()
    {

        $dbpdo = new DBPDO();
        $dbpdo->connect();

        $sql = "select 
                        `newsid`,
                        `titleTh`,
                        `createDatetime`,
                        `imageLink`,
                        `readCount`,
                        `recommentNews`
                from
                        news
                WHERE 
                      `createDatetime` > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 1 WEEK))
                order by
                        readCount desc,
                        createDatetime desc 
                limit
                        7";
        $params = array();
        $return = $dbpdo->query($sql, $params);
        $dbpdo->close();

        return $return;
    }

    public function getLastestVideos()
    {

        $dbpdo = new DBPDO();
        $dbpdo->connect();

        $sql = "select 
                        `video_id`,
                        `content`,
                        `title`,
                        `create_datetime`,
                        `videosource`,
                        `videotype`
                from 
                        video_highlight
                where
                        videotype = 'general'
                order by
                        create_datetime desc
                limit 
                        15";
        $params = array();
        $return = $dbpdo->query($sql, $params);
        $dbpdo->close();

        return $return;
    }

    public function getHighlightVideos()
    {

        $dbpdo = new DBPDO();
        $dbpdo->connect();

        $sql = "select
                        `video_id`,
                        `content`,
                        `title`,
                        `create_datetime`,
                        `videosource`,
                        `videotype`
                from
                        video_highlight
                where
                        videotype = ?
                order by
                        create_datetime desc
                limit
                        4";
        $params = array('highlight');
        $return = $dbpdo->query($sql, $params);
        $dbpdo->close();

        return $return;
    }


    public function getNewsByCategory($cat, $limit = 7)
    {
        //cat:[other,thai,'premier league','la liga','bundesliga','serie a','league 1'
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                          `newsid`,
                          `titleTh`,
                          `createDatetime`,
                          `imageLink`,
                          `readCount`
                    from news
                    WHERE
                          `category` =?
                    order by newsId DESC
                    limit ?";
            $params = array($cat, $limit);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $return;
    }


    public function getMatchTodayByLId($Lid)
    {

        //============= get team list =================
        $dbpdo = new DBPDO();
        $dbpdo->connect();
        $listbydate = array();
        $sql = "select 
                        distinct
                        lm.hn,
                        lm.gn,
                        lm.hid,
                        lm.gid,
                        lm.mid,
                        lm.`date`
                from
                   ((
                        fixture f
                        left join 
                                competitions c
                                on f.competitionNamePk = c.competitionNamePk and f.leagueNamePk = c.leagueNamePk)
                        left join
                                league l
                                on l.competitionId = c.competitionId and l.leagueNamePk = c.leagueNamePk)
                        right join
                                live_match lm
                                on lm._lid = l.leagueId
                where
                        l.leagueid = ?
                ORDER BY lm.date";
        $params = array($Lid);
        $matchs = $dbpdo->query($sql, $params);
//echo $sql;
//    print_r($matchs);
        //============= get logo list =================

        if (!empty($matchs)) {
            $teamid = "";
            for ($i = 0; $i < count($matchs); ++$i) {
                if (0 == $i) {
                    $teamid = "" . $matchs[$i]->hid;
                    $teamid .= "," . $matchs[$i]->gid;
                } else {
                    $teamid .= "," . $matchs[$i]->hid;
                    $teamid .= "," . $matchs[$i]->gid;
                }

            }

            $sql = 'select
                        *
                from 
                        team_logos
                where
                        tid in (' . $teamid . ')';

//            echo $sql;
            $params2 = array();
//            echo $sql;exit;
            $logos = $dbpdo->query($sql, $params2);


            //============= add logo to team  =================

            for ($i = 0; $i < count($matchs); ++$i) {
                $matchs[$i]->hlogo = "http://api.ssporting.com/teams_clean/team_default_32x32.png";
                $matchs[$i]->glogo = "http://api.ssporting.com/teams_clean/team_default_32x32.png";
                for ($j = 0; $j < count($logos); ++$j) {
                    if ($matchs[$i]->hid == $logos[$j]->tid) {
                        $matchs[$i]->hlogo = $logos[$j]->{'32x32'};
                        break;
                    }
                }
                for ($j = 0; $j < count($logos); ++$j) {
                    if ($matchs[$i]->gid == $logos[$j]->tid) {
                        $matchs[$i]->glogo = $logos[$j]->{'32x32'};
                        break;
                    }
                }
                $listbydate[date('Y-m-d', strtotime($matchs[$i]->date))][] = $matchs[$i];
            }
        }
        $dbpdo->close();
        return $listbydate;
    }

    public function getLeagueFixture($Lid)
    {

        //============= get team list =================
        $dbpdo = new DBPDO();
        $dbpdo->connect();
        $listbydate = array();
        $sql = "SELECT f.* FROM lang_league ll
RIGHT JOIN competitions c ON c.competitionId=ll.competitionId
RIGHT JOIN league l ON l.leagueId=ll.leagueId
RIGHT JOIN fixture f ON f.competitionType=c.competitionType AND f.competitionNamePk=c.competitionNamePk AND f.leagueNamePk = c.leagueNamePk AND f.leagueName=ll.leagueName AND f.leagueSeason=l.leagueSeason
WHERE ll.leagueId=?";
        $params = array($Lid);
        $matchs = $dbpdo->query($sql, $params);
//echo $sql;
//    print_r($matchs);
        //============= get logo list =================

        if (!empty($matchs)) {
            $teamid = "";
            for ($i = 0; $i < count($matchs); ++$i) {
                if (0 == $i) {
                    $teamid = "'" . $matchs[$i]->teamHomeNamePk."'";
                    $teamid .= ",'" . $matchs[$i]->teamAwayNamePk."'";
                } else {
                    $teamid .= ",'" . $matchs[$i]->teamHomeNamePk."'";
                    $teamid .= ",'" . $matchs[$i]->teamAwayNamePk."'";
                }

            }

            $sql = "SELECT tl.*,t.tnPk FROM team t
LEFT JOIN team_logos tl ON tl.tid=t.tid
WHERE t.tnPk IN ($teamid)";

//            echo $sql;
            $params2 = array();
//            echo $sql;exit;
            $logos = $dbpdo->query($sql, $params2);


            //============= add logo to team  =================

            for ($i = 0; $i < count($matchs); ++$i) {
                $matchs[$i]->hlogo = "http://api.ssporting.com/teams_clean/team_default_32x32.png";
                $matchs[$i]->glogo = "http://api.ssporting.com/teams_clean/team_default_32x32.png";
                for ($j = 0; $j < count($logos); ++$j) {
                    if ($matchs[$i]->teamHomeNamePk == $logos[$j]->tnPk) {
                        $matchs[$i]->hlogo = $logos[$j]->{'32x32'};
                        break;
                    }
                }
                for ($j = 0; $j < count($logos); ++$j) {
                    if ($matchs[$i]->teamAwayNamePk == $logos[$j]->tnPk) {
                        $matchs[$i]->glogo = $logos[$j]->{'32x32'};
                        break;
                    }
                }
                $listbydate[date('Y-m-d', strtotime($matchs[$i]->date))][] = $matchs[$i];
            }
        }
        $dbpdo->close();
        return $listbydate;
    }


    public function getTeamRank($lid = 43105)
    {
        $dbpdo = new DBPDO();
        $dbpdo->connect();
        $result = array();
        try {
            $sql = "SELECT t.`tn`,st.`leagueId`,st.`no`,st.`gp`,st.`gf`,st.`pts`,st.`tid` FROM stat_table st
LEFT JOIN team t ON t.tid = st.`tid`
WHERE st.`leagueId`=?
ORDER BY st.`group`,st.`no`";
            $params = array($lid);
            $result = $dbpdo->query($sql, $params);
            if (!empty($result)) {
                $teamid = "";
                for ($i = 0; $i < count($result); ++$i) {
                    if (0 == $i) {
                        $teamid = $result[$i]->tid;

                    } else {
                        $teamid .= "," . $result[$i]->tid;
                    }

                }

                $sql = 'select
                        *
                from
                        team_logos
                where
                        tid in (' . $teamid . ')';

                $params2 = array();
                $logos = $dbpdo->query($sql, $params2);
                //============= add logo to team  =================
//                echo $lid.":". count($logos)." \n";
                for ($i = 0; $i < count($result); $i++) {
                    $result[$i]->logo = "/images/teams_clean/team_default_32x32.png";
                    for ($j = 0; $j < count($logos); $j++) {
                        if ($result[$i]->tid == $logos[$j]->tid) {
                            $result[$i]->logo = $logos[$j]->{'32x32'};
                            break;
                        }
                    }
                }

            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $dbpdo->close();
        return $result;
    }

    public function getGoalRanking($leagueid)
    {
        //cat:[other,thai,'premier league','la liga','bundesliga','serie a','league 1'
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                            `no`,
                            `playerNameEn`,
                            `totalScore`
                    from
                            top_player_score
                    where
                            leagueId = ?
                    order by
                            no ";
            $params = array($leagueid);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

    public function getAssistRanking($leagueid)
    {
        //cat:[other,thai,'premier league','la liga','bundesliga','serie a','league 1'
        $dbpdo = new DBPDO();
        $return = array();
        try {
            $dbpdo->connect();
            $sql = "select
                            `no`,
                            `playerNameEn`,
                            `totalScore`
                    from
                            top_player_score
                    where
                            leagueId = ?
                    order by
                            no ";
            $params = array($leagueid);
            $return = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }

        return $return;
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/28/2017
 * Time: 16:59
 */
include_once "DBPDO.php";

class playUser
{
    public function goldUserLastChamp(){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select id,`fb_uid`,`name`,`gold_month` from user WHERE fb_uid IS NOT NULL order by user.gold_month DESC limit 10";
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $user=$stmt->fetchAll();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $user;
    }

    public function goldUserAll(){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select id,`fb_uid`,`name`,`gold` from user WHERE fb_uid IS NOT NULL order by user.gold DESC limit 10";
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $user=$stmt->fetchAll();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $user;
    }


    public function goldUserNow(){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select id,fb_uid,name,gold_half from user where user.fb_uid is not null order by user.gold_half DESC limit 10;";
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $user=$stmt->fetchAll();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $user;
    }

    public function accuracyUserAll(){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select id,`fb_uid`,`name`,`accuracy` from user WHERE fb_uid IS NOT NULL order by user.accuracy DESC limit 10";
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $user=$stmt->fetchAll();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $user;
    }

    public function accuracyUserLastChamp(){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select id,fb_uid,name,accuracy_month from user where user.fb_uid is not null order by user.accuracy_month DESC limit 10;";
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $user=$stmt->fetchAll();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $user;
    }

    public function accuracyUserNow(){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "select id,fb_uid,name,accuracy_half from user where user.fb_uid is not null order by user.accuracy_half DESC limit 10;";
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $user=$stmt->fetchAll();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $user;
    }

    public function updateCoinAndCoupon($fb_uid,$coin,$coupon){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "update `user` set `coin`=:coin,`coupon`=:coupon,`updated_at`=:updated_at where fb_uid=:fb_uid";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':fb_uid', $fb_uid);
            $stmt->bindParam(':coin', $coin);
            $stmt->bindParam(':coupon', $coupon);
            $stmt->bindParam(':updated_at', date('Y-m-d H:i:s'));
            $stmt->execute();
            $user=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $user;
    }

    public function checkUserByUserName($userName){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $user=array();
        try {
            if (filter_var($userName, FILTER_VALIDATE_EMAIL)) {
                $sql = "select * from user where user.email=:email";
                $stmt = $dbh->prepare($sql);
                $stmt->bindParam(':email', $userName);
            }else {
                $sql = "select * from user where user.username=:username";
                $stmt = $dbh->prepare($sql);
                $stmt->bindParam(':username', $userName);
            }
            $stmt->execute();
            $user=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $user;
    }

    public function checkUserPlay($id)
    {
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $user=array();
        try {
            $sql = "select * from user where user.id=:id";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $user=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $user;
    }

    public function checkUserPlayFb($fb_uid)
    {
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $user=array();
        try {
            $sql = "select * from user where user.fb_uid=:fb_uid order by id ASC";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':fb_uid', $fb_uid);
            $stmt->execute();
            $user=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $user;
    }

    public function updateUserPlay($fb_uid,$name,$email){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "update `user` set `email`=:email,`name`=:sname,`updated_at`=:updated_at,`last_login`=:last_login where fb_uid=:fb_uid";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':fb_uid', $fb_uid);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':sname', $name);
            $stmt->bindParam(':updated_at', date('Y-m-d H:i:s'));
//            $stmt->bindParam(':last_login', date('Y-m-d 00:00:00'));
            $stmt->execute();
            $user=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $user;
    }

    public function editUser($id,$email,$name,$username,$password,$path){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "update `user` set `username`=:username,`email`=:email,`name`=:name,`password`=:password,`path`=:path,`updated_at`=:updated_at where `id`=:id";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':id', $id);
            $stmt->bindParam(':username', $username);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':password', md5($password));
            $stmt->bindParam(':path', $path);
            $stmt->bindParam(':updated_at', date('Y-m-d H:i:s'));
            $stmt->execute();
            $user=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $user;
    }

    public function addUserEmail($email,$name,$username,$password,$path){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "INSERT INTO `user` (`username`, `name`, `email`,`password`,`path`,`created_at`,`updated_at`) VALUES (:username,:name,:email,:password,:path,:updated_at,:created_at);";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':username', $username);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':password', md5($password));
            $stmt->bindParam(':path', $path);
            $stmt->bindParam(':created_at', date('Y-m-d H:i:s'));
            $stmt->bindParam(':updated_at', date('Y-m-d H:i:s'));
            $stmt->execute();
            $user=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $user;
    }

    public function addUserPlay($fb_uid,$name,$email){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "INSERT INTO `user` (`fb_uid`, `name`, `email`,`created_at`,`updated_at`) VALUES (:fb_uid,:name,:email,:updated_at,:created_at);";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':fb_uid', $fb_uid);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':created_at', date('Y-m-d H:i:s'));
            $stmt->bindParam(':updated_at', date('Y-m-d H:i:s'));
            $stmt->execute();
            $user=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $user;
    }

}
<?php
namespace model;

require_once __DIR__ . "/../bootstrap.php";
use Illuminate\Database\Eloquent\Model;

class LeagueElo extends Model
{
    protected $table = "league";
    protected $fillable = ['country_id', 'id_7m', 'team_home', 'name', 'path','priority', 'created_at', 'updated_at'];

    public function matchs()
    {
        return $this->hasMany(MatchElo::class, "league_id", "id");
    }
    public function getLogo()
    {
        $path = "/images/football.png";
        if (!empty($this->path) && file_exists($_SERVER['DOCUMENT_ROOT'].$this->path)) {
            $path = $this->path;
        }
        return $path;
    }
}

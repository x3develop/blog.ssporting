<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 1/25/2019
 * Time: 10:14 AM
 */
require_once "DBPDO.php";
class SitemapSearch
{
    public function getByLimit($limit){
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array();
        $sql = "SELECT * FROM sitemap_search order by id desc limit ".$limit;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }
    public function getByOffset($offset){
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array();
        $sql = "SELECT * FROM sitemap_search order by id asc limit 20000 offset ".$offset;
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }
    public function countPage(){
        $dbpdo = new DBPDO();
        $rs = array();
        $params = array();
        $sql = "SELECT CEILING (count(*)/20000) as maxPage FROM sitemap_search";
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sql, $params);

        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $rs;
    }
    public function add($text){
        $dbpdo = new DBPDO();
        $last_id = 0;
        $sql = "insert into sitemap_search (text) values (?)";
        $params = array($text);
        $sqlCheck = "SELECT * FROM sitemap_search where text=?";
        $paramsCheck = array($text);
        try {
            $dbpdo->connect();
            $rs = $dbpdo->query($sqlCheck, $paramsCheck);
            if(count($rs)==0) {
                $last_id = $dbpdo->insert($sql, $params);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $dbpdo->close();
        }
        return $last_id;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 11/21/2016
 * Time: 16:46
 */
class liveMatch extends db_connect
{
    protected $nameTable = "live_match";

    public function listMatchToday(){
        $dateStart = new DateTime();

        $conn=$this->db_connect();
        $query=$conn->query("select * from ".$this->nameTable." where showDate >= ".$dateStart->format('Y-m-d 00:00:00')." and showDate<=".$dateStart->format('Y-m-d 23:59:59'));
        if($query->num_rows>0){
            $data=$query->fetch_object();
            return $data;
        }else{
            return 0;
        }
        $conn->close();
        $query->free();
    }
}
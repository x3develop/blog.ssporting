<?php
namespace model;

require_once __DIR__ . "/../bootstrap.php";
use Illuminate\Database\Eloquent\Model;

class HdpElo extends Model
{
    protected $table = "gamble";
    protected $fillable = ['match_id', 'home_water_bill', 'handicap', 'away_water_bill', 'created_at', 'updated_at'];  

}

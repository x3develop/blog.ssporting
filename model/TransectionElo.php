<?php

namespace model;

require_once __DIR__ . "/../bootstrap.php";

use Illuminate\Database\Eloquent\Model;

class TransectionElo extends Model
{
    protected $table = "log_income";
    protected $fillable = ['user_id', 'bet_id', 'before_gold', 'before_coin', 'use_bet_coupon', 'status', 'hdp', 'reward', 'tid', 'created_at', 'updated_at'];

    public function match()
    {
        return $this->BelongsTo(MatchElo::class, "match_id", "id");
    }

}

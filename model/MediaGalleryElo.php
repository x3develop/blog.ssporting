<?php

namespace model;

require_once __DIR__ . "/../bootstrap.php";

use Illuminate\Database\Eloquent\Model;

class MediaGalleryElo extends Model
{
    protected $table = "media_gallery";
    protected $fillable = ['view','updated_at'];

    public function cover()
    {
        return $this->hasOne(MediaStoreElo::class, "gall_id", "id");
    }

    public function media(){
        return $this->hasMany(MediaStoreElo::class, "gall_id", "id");
    }

}

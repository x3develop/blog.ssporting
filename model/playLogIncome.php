<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 12/13/2017
 * Time: 18:18
 */
include_once "DBPDO.php";
class playLogIncome
{

    public function checkNewUserIncome($user_id)
    {
        $dateNow = new DateTime();
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $user=array();
        try {
            $sql = "select id from log_income where log_income.user_id=".$user_id." and income_derived='newuser'";
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $income=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        if(!empty($income)){
            return false;
        }else {
            return true;
        }
    }

    public function checkComboIncome($user_id)
    {
        $dateNow = new DateTime();
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $user=array();
        try {
            $sql = "select id from log_income where log_income.user_id=".$user_id." and income_derived='combo' and date_record=CURDATE()";
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $income=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        if(!empty($income['id'])){
            return false;
        }else {
            return true;
        }
    }

    public function checkStepIncome($user_id)
    {
        $dateNow = new DateTime();
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $user=array();
        try {
            $sql = "select id from log_income where log_income.user_id=".$user_id." and income_derived='step' and date_record='".$dateNow->format('Y-m-d')."'";
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $income=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        if(!empty($income)){
            return false;
        }else {
            return true;
        }
    }

    public function checkDailyIncome($user_id)
    {
        $dateNow = new DateTime();
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $user=array();
        try {
            $sql = "select id from log_income where log_income.user_id=".$user_id." and income_derived='daily' and date_record='".$dateNow->format('Y-m-d')."'";
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            $income=$stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        if(!empty($income)){
            return false;
        }else {
            return true;
        }
    }

    public function getByUser($user_id){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sth = $dbh->prepare("select log_income.*,bet.match_id from log_income left join bet on bet.id=log_income.bet_id where log_income.created_at > DATE_SUB(NOW(), INTERVAL 1 WEEK) and log_income.user_id=:user_id order by log_income.id desc");
            $sth->bindParam(':user_id', $user_id);
            $sth->execute();
            $dataIncome = $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $db->close();
        }
        return $dataIncome;
    }

    public function addLogInCome($user_id,$bet_id,$befor_gold,$befor_coin,$befor_coupon,$befor_lv,$befor_star,$income_derived,$income_type,$income_format,$after_gold,$after_coin,$after_coupon,$after_lv,$after_star,$comment){
        $db = new DBPDO();
        $dbh = $db->connect2();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = "INSERT INTO `log_income` (`user_id`,`bet_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`befor_lv`,`befor_star`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`after_lv`,`after_star`,`comment`,`date_record`,`created_at`,`updated_at`) ";
            $sql .="VALUES (:user_id,:bet_id,:befor_gold,:befor_coin,:befor_coupon,:befor_lv,:befor_star,:income_derived,:income_type,:income_format,:after_gold,:after_coin,:after_coupon,:after_lv,:after_star,:comment,:date_record,:updated_at,:created_at);";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':bet_id', $bet_id);
            $stmt->bindParam(':befor_gold', $befor_gold);
            $stmt->bindParam(':befor_coin', $befor_coin);
            $stmt->bindParam(':befor_coupon', $befor_coupon);
            $stmt->bindParam(':befor_lv', $befor_lv);
            $stmt->bindParam(':befor_star', $befor_star);
            $stmt->bindParam(':income_derived', $income_derived);
            $stmt->bindParam(':income_type', $income_type);
            $stmt->bindParam(':income_format', $income_format);
            $stmt->bindParam(':after_gold', $after_gold);
            $stmt->bindParam(':after_coin', $after_coin);
            $stmt->bindParam(':after_coupon', $after_coupon);
            $stmt->bindParam(':after_lv', $after_lv);
            $stmt->bindParam(':after_star', $after_star);
            $stmt->bindParam(':comment', $comment);
            $stmt->bindParam(':date_record', date("Y-m-d"));
            $stmt->bindParam(':created_at', date('Y-m-d H:i:s'));
            $stmt->bindParam(':updated_at', date('Y-m-d H:i:s'));
            $stmt->execute();
            $logIncome=$dbh->lastInsertId();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        } finally {
            $db->close();
        }
        return $logIncome;
    }
}
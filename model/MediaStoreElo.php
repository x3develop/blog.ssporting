<?php

namespace model;

require_once __DIR__ . "/../bootstrap.php";

use Illuminate\Database\Eloquent\Model;

class MediaStoreElo extends Model
{
    protected $table = "media_store";
    protected $fillable = ['view'];
}

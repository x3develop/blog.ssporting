<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">

    <title>Ngoal เว็บทายผลบอลอันดับ 1 ของประเทศ ข่าวฟุตบอล ไฮไลท์การแข่งขัน</title>
    <meta name="keywords" content="News Football,Highlight Football,Video Football,Football">
    <meta name="description" content="เว็บทายผลบอลอันดับ 1 ของประเทศ ข่าวฟุตบอล ไฮไลท์การแข่งขัน">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">

    <link rel="stylesheet" href="css/style-new.css">
    <!--    <link rel="stylesheet" href="css/style.css">-->
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="css/gallery.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/livescore.css">
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="js/jquery/dist/jquery.min.js"></script>
    <script src="js/jquery-ui/jquery-ui.min.js"></script>
    <!--    <script src="js/flogin/src/flogin.js"></script>-->
    <script src="/js/moment.js"></script>
    <script src="/js/moment-with-locales.min.js"></script>
    <script src="css/bootstrap/js/bootstrap.min.js"></script>
    <script src="css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="js/sly/sly.js"></script>
    <script src="js/jquery-timeago/jquery.timeago.js"></script>
    <script src="nanoscrollbar/nanoscrollbar.js"></script>
    <script>
        function imgError(image) {
            image.onerror = "";
            image.src = "http://api.ssporting.com/teams_clean/team_default_32x32.png";
            return true;
        }
    </script>
    <script src="js/utils.js"></script>
    <!--    <script src="js/timeview.js"></script>-->
    <script src="js/index_game.js"></script>
    <script src="/js/newFB.js"></script>
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
</head>
<body id="main-scrollbar">
<div class="scroll-content">
    <style>
        div.twitter-video {
            max-height: none !important;
            max-width: none !important;
            min-width: none !important;
            min-height: none !important;
            width: 100% !important;
            height: 100% !important;
        }
    </style>
    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    <?php
    session_start();
    ?>
    <?php
    require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playComment.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/model/HomeM.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/model/VideoM.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playBet.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playLogIncome.php";
    $playBet = new playBet();
    $playMatch = new playMatch();
    $playComment = new playComment();
    $VideoMObj = new VideoM();
    $homem = new HomeM();
    $playLogIncome = new playLogIncome();

    if (isset($_SESSION['login'])) {
        //calculate Bet
        $playBet->calculateBet($_SESSION['login']['id']);
    }
    $match=array();
    $match = $playMatch->getFirstMathStatusBet(30);
    $matchResultsHome = array();
    $matchResultsAway = array();
    $betMatchById = array();
    if (!empty($match[0])) {
        $matchResultsHome = $playMatch->getListMatchResults($match[0]->team_home, 6);
        $matchResultsAway = $playMatch->getListMatchResults($match[0]->team_away, 6);
        if (isset($_SESSION['login'])) {
            $betMatchById = $playBet->getBetMatchById($_SESSION['login']['id'], $match[0]->id);
        }
        $comment = $playMatch->getListComment($match[0]->id);
        $commentMini = array();
        foreach ($comment as $key => $value) {
            if (!empty($value->countComment)) {
                $commentMini[$value->id] = $playComment->getMiniComment($match[0]->id, $value->id);
            }
        }
        $commentHome = $playMatch->getListCommentByTeam($match[0]->id, 'home');
        $commentAway = $playMatch->getListCommentByTeam($match[0]->id, 'away');
        $heartList = $playComment->checkHeartByMatch($match[0]->id);
        $heartListByUser = $playComment->checkHeartByUser($match[0]->id);
    }

    //include 'header.php';
    //include 'config/db_connect.php';
    //include 'model/liveLeague.php';

    //index menu
    include 'view/index/menu-index-big.php';
    include 'view/index/livescore-index.php';
    //index livescore
    //include 'view/index/livescore.php';
    //require_once 'model/GalleryM.php';
    //require_once $_SERVER["DOCUMENT_ROOT"] . "/model/MatchM.php";
    //require_once $_SERVER["DOCUMENT_ROOT"] . "/model/OddsM.php";
    //require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/LogoManager.php";
    //require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/DataMap.php";
    //require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
    //require_once $_SERVER["DOCUMENT_ROOT"] . "/model/NewsM.php";
    //require_once $_SERVER["DOCUMENT_ROOT"] . "/model/RankM.php";
    ?>

    <?php
    //england 43948,ger 44352,spain 45141,italy 45146,fr 43893,th 43105,cl 44042, eu 44094, wc 44465
    //$GalleryObject = new GalleryM();
    //$recommendnews = $homem->getRecommentNews();
    //$popularnews = $homem->getMostPopularNews();
    //$lastestnews = $homem->getLastestNews();
    $lastestvideo = $VideoMObj->getSlideVideos("general", 16);
    $highlightvideo = $VideoMObj->getSlideVideos("highlight", 16);
    ////$lastestvideo = $homem->getLastestVideos();
    ////$highlightvideo = $homem->getHighlightVideos();
    $thainews = $homem->getNewsByCategory('thai', 8);
    $foreignNewsPlData = $homem->getNewsByCategory('premier league', 7);
    //$othernews = $homem->getNewsByCategory('other');
    //$thai_fixture = $homem->getLeagueFixture(43036);
    //
    //$thai_rank = $homem->getTeamRank(43105);
    //$topplayer_thai = $homem->getGoalRanking(43105);
    //
    //$dataGalleryAll = $GalleryObject->getAllGallery(0, 5);
    //
    //
    //$mid = 1513531;
    //$matchm = new MatchM();
    //$logoman = new LogoManager();
    //$oddsm = new  OddsM();
    //$datamap = new DataMap();
    //$newsm = new NewsM();
    //$rankm = new RankM();
    //@session_start();
    //$recommendodds = $oddsm->getRecommendOddsPack(20);
    //if (count($recommendodds['match']) != 0) {
    //    foreach ($recommendodds['match'] as $key => $val) {
    //        $mid = $key;
    //        break;
    //    }
    //}
    //$gamedata = $matchm->getGameByMidPack($mid);
    ////if (empty($gamedata["match"])) {
    ////    header("fixtures.php");
    ////}
    //
    //$teamguide = $matchm->getTeamGuidePack($mid);
    //$ranking = $rankm->getUserRankPack();
    //$betside = $matchm->betCountPack($mid);
    //$sysconfig = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/system_config/file.json'), true);
    //$ownbet = [];
    //if (!empty($_SESSION['fb_uid'])) {
    //    $matchbet = $matchm->getMatchBetPack($mid, $_SESSION['fb_uid']);
    //    $ownbet = $datamap->mapByKey($matchbet['keylist']['bet'], $matchbet['bet']);
    //}
    //$thismatch = $datamap->mapByKey($gamedata['keylist']['match'], $gamedata['match']);
    ?>
    <input id="game-info" type="hidden" mid="<?php echo $thismatch['mid']; ?>" lid="<?php echo $thismatch['lid']; ?>"
           hid="<?php echo $thismatch['hid']; ?>" gid="<?php echo $thismatch['gid']; ?>"
           c0="<?php echo $thismatch['c0']; ?>" sid="<?php echo $thismatch['sid']; ?>"
           choose="<?php echo (!empty($ownbet)) ? $ownbet['choose'] : ''; ?>"
           displayname="<?php echo (!empty($ownbet)) ? $ownbet['display_name'] : ''; ?>"/>

    <div class="container-fluid">
        <!--    header news-->
        <?php include $_SERVER["DOCUMENT_ROOT"] . '/view/index/headerNews.php'; ?>
        <!--        Most Popular-->
        <?php include $_SERVER["DOCUMENT_ROOT"] . '/view/index/mostPopular.php'; ?>
        <!--    game-->
        <!--    --><?php //include $_SERVER["DOCUMENT_ROOT"] . '/view/index/gameNew.php'; ?>
        <!--    video top-->
        <!--        *--><?php //include $_SERVER["DOCUMENT_ROOT"] .'/view/index/videoTop.php'; ?>
        <!--    video main-->
        <?php include $_SERVER["DOCUMENT_ROOT"] . '/view/index/videoMain.php'; ?>
        <!--    video bottom-->
        <!--        *--><?php //include $_SERVER["DOCUMENT_ROOT"] .'/view/index/videobottom.php'; ?>
        <!--    ข่าวต่างประเทศ-->
        <!--    --><?php //include $_SERVER["DOCUMENT_ROOT"] . '/view/index/foreignNews.php'; ?>
        <!--    ข่าวบอลไทย-->
        <!--        *--><?php //include $_SERVER["DOCUMENT_ROOT"] .'/view/index/thaiNews.php'; ?>
        <!--    ดาวซัลโว-->
        <!--        *--><?php //include $_SERVER["DOCUMENT_ROOT"] .'/view/index/goalScorer.php'; ?>
        <!--    ข่าวกีฬาอื่นๆ-->
        <!--        *--><?php //include $_SERVER["DOCUMENT_ROOT"] .'/view/index/otherNews.php'; ?>
        <!--    Sexy Football-->

    </div>
    <div class="row container-foreignNews-index">
        <?php include $_SERVER["DOCUMENT_ROOT"] . '/view/index/foreignNews-edit.php'; ?>
    </div>
    <div class="container-fluid">
        <?php include $_SERVER["DOCUMENT_ROOT"] . '/view/index/sexyFootball.php'; ?>
    </div>


    <div class="modal fade in" role="dialog" id="myModal" tabindex="-1" aria-labelledby="myModalLabel"
         style="display: none;">
        <div class="modal-dialog" role="document" style="">
            <div class="modal-content">

                <div class="modal-body">
                    <table class="table-played">
                        <tr>
                            <td>
                                <div class="button-play select-home bg-played"><span><i
                                                class="fa fa-check-square-o"></i><?php echo $thismatch['hn']; ?></span>
                                </div>
                                <div class="button-play select-away hide"><span><?php echo $thismatch['hn']; ?></span>
                                </div>
                            </td>
                            <td>
                                <div class="bx-border">
                                    <div class="pull-left"><img src="/images/coin/scoin100.png"></div>
                                    <select id="bet-amount">
                                        <?php foreach ($sysconfig['ball_game_scoin'] as $key => $val) { ?>
                                            <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                        <?php } ?>
                                    </select>

                                    <div style="clear:both;"></div>
                                </div>
                            </td>
                            <td>
                                <div class="button-play select-home"><span><?php echo $thismatch['gn']; ?></span></div>
                                <div class="button-play select-away bg-played hide"><span><i
                                                class="fa fa-check-square-o"></i><?php echo $thismatch['gn']; ?></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="text-center">เลือกจำนวน Coin</td>
                        </tr>
                        <tr style="border-top: solid 1px #eee; border-bottom: solid 1px #eee; padding: 5px;">
                            <td><strong><?php echo count($betside['betlist']['home']); ?></strong></td>
                            <td style="font-weight: bold;">
                                <div>HDP</div>
                                <?php
                                if (!empty($gamedata["odds"])) {
                                    echo $gamedata["odds"][2];
                                }
                                ?>
                            </td>
                            <td><strong><?php echo count($betside['betlist']['away']); ?></strong></td>
                        </tr>
                        <tr>
                            <td colspan="3"><input id="bet-comment" class="form-control" placeholder="Comment.."></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <button class="btn btn-info btn-bet">ทายผล</button>
                                <img id="bet-wait-img" style="width: 60px;" class="hide"
                                     src="images/loading-spinner.gif"/>
                            </td>
                        </tr>

                    </table>

                </div>

            </div>
        </div>
    </div>
    <?php //include 'footer.php';
    include 'view/index/footer.php';
    ?>
    <script>
        function setIndexVideosAutoPlay(link, tag, title, desc, date, id) {
            $('#large-videos-autoPlay').attr("src", link);
        }

        $(".nano").nanoScroller();

        //    $(document).ready(function () {
        //        $('#loginbutton').facebook_login({
        //            appId: '1677798712517610',   //# your facebook application id
        //            endpoint: '/createSessions.php',  //# where to POST the response to
        //            onSuccess: function (data) {
        //                location.reload();
        //            },  //# what to do on success, usually redirect
        //            onError: function (data) {
        //            }, //# what to do on error
        //            permissions: 'public_profile,user_friends,email'  //# what permissions you need, default is just email
        //        });

        //        $("#owl-demo,#owl-demo2").owlCarousel({
        //            loop: true,
        //            items: 6,
        //            navigation: true, // Show next and prev buttons
        //            slideSpeed: 300,
        //            paginationSpeed: 400,
        //            singleItem: true,
        //            margin: 25,
        //            responsiveClass: true,
        //            responsive: {
        //                350: {
        //                    items: 1,
        //                    nav: true
        //                },
        //                600: {
        //                    items: 3,
        //                    nav: false
        //                },
        //                1000: {
        //                    items: 4,
        //                    nav: true,
        //                    loop: false
        //                }
        //            }

        // "singleItem:true" is a shortcut for:
        // items : 1,
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false

        //        });

        refreshTime();
        function refreshTime() {
//            console.log('refresh');
            $.each($(".how-long"), function (k, v) {
//                console.log(k,v);
                if ($.isNumeric($(this).text())) {
                    $(this).html(moment($(this).text(), "X").fromNow())
                } else {
                    $(this).html(moment($(this).text(), "YYYY-MM-DD HH:mm:ss").fromNow())
                }
//                var d = new Date(parseInt($(this).text()) * 1000);
//                $(this).text(jQuery.timeago(d));
            })

        }


        //        $(".lastest-video").on('click', function (e) {
        //            var key = $(e.currentTarget).attr('videokey');
        //            var source = $(e.currentTarget).attr('source');
        //            setplayer(key, source, true);
        //        });
        //        $(".highlight-video").on('click', function (e) {
        //            var key = $(e.currentTarget).attr('videokey');
        //            var source = $(e.currentTarget).attr('source');
        //            setplayer(key, source, true);
        //        });

        //        setplayer($(".lastest-video").first().attr('videokey'), $(".lastest-video").first().attr('source'), false);
        //        function setplayer(key, source, autoplay) {
        ////            console.log(key, source);
        //            var sourcelist = {
        //                "youtube": "https://www.youtube.com/embed/",
        //                "dailymotion": "http://www.dailymotion.com/embed/video/"
        //            };
        //            var path = sourcelist[source] + key;
        //            if (autoplay) {
        //                path += "?autoplay=1";
        //            }
        //            console.log(path);
        //            $("#videos-player").attr('src', path);
        //        }


        //        var owl = $("#owl-demo2");
        //
        //        owl.owlCarousel({
        //            items: 6, //10 items above 1000px browser width
        ////                itemsDesktop : [1000,5], //5 items between 1000px and 901px
        ////                itemsDesktopSmall : [900,3], // betweem 900px and 601px
        ////                itemsTablet: [600,2], //2 items between 600 and 0
        ////                itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
        //            responsiveClass: true,
        //            responsive: {
        //                350: {
        //                    items: 1,
        //                    nav: true
        //                },
        //                600: {
        //                    items: 3,
        //                    nav: false
        //                },
        //                1000: {
        //                    items: 6,
        //                    nav: true,
        //                    loop: false
        //                }
        //            }
        //        });

        //    });

    </script>
</div>
</body>
</html>
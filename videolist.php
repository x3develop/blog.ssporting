<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 9/17/2018
 * Time: 9:39 PM
 */
$category=(isset($_REQUEST['category'])?$_REQUEST['category']:null);
$tag=(isset($_REQUEST['tag'])?$_REQUEST['tag']:null);
$limit=(isset($_REQUEST['limit'])?$_REQUEST['limit']:10);
$offset=(isset($_REQUEST['offset'])?$_REQUEST['offset']:0);
$style=(isset($_REQUEST['style'])?$_REQUEST['style']:0);

require_once 'model/VideoM.php';
$VideoMObj = new VideoM();
if($style=="normal") {
    $dataVideoMAllByNormal = $VideoMObj->getSlideVideosCategoryByStyle($category, "highlight", $limit, $tag, 'normal', $offset);
}elseif($style=="goal") {
    $dataVideoMAllByGoal = $VideoMObj->getSlideVideosCategoryByStyle($category, "highlight", $limit, $tag, 'goal', $offset);
}
?>
<?php
if($style=="normal") {
foreach ($dataVideoMAllByNormal as $key => $value) { ?>
<tr tag="<?=trim($value->video_tag)?>" data-number="<?= ceil(($key + 1) / 7) ?>"
    class="<?=((!empty($dataVideo[0]) and $value->video_id==$dataVideo[0]->video_id)?'playing-box':'')?> tr-normal tr-<?= ceil(($key + 1) / 7) ?> <?= ((ceil(($key + 1) / 7) <= 3) ? '' : 'hide') ?>"
    onclick="setIndexVideosAutoPlay('<?php echo $value->urlIframe ?>','<?php echo $value->video_tag ?>','<?php echo $value->title ?>','','<?php echo $value->create_datetime ?>','<?php echo $value->video_id ?>')">
    <td>
        <div class="box-list-video-side">
            <div class="icon-play">
                <i class="fa fa-play"></i>
            </div>
            <img width="168" height="94"
                 src="<?= $value->urlImg; ?>">
        </div>
    </td>
    <td>
        <div class="box-content-list">
            <?= $value->title ?>
        </div>
        <?php if((!empty($dataVideo[0]) and $value->video_id==$dataVideo[0]->video_id)){ ?>
            <span class="label label-danger">
                                                                    <i class="fa fa-play" aria-hidden="true"></i> กำลังเล่น
                                                                </span>
        <?php }else{ ?>
            <span class="label">
                                                                    <?= date('d-M-Y', strtotime($value->create_datetime)); ?>
                                                                </span>
        <?php }?>
    </td>
</tr>
<?php } ?>
<?php }elseif($style=="goal"){ ?>
    <?php foreach ($dataVideoMAllByGoal as $key => $value) { ?>
        <tr tag="<?=trim($value->video_tag)?>" data-number="<?= ceil(($key + 1) / 7) ?>"
            class="<?=((!empty($dataVideo[0]) and $value->video_id==$dataVideo[0]->video_id)?'playing-box':'')?> tr-goal tr-<?= ceil(($key + 1) / 7) ?> <?= ((ceil(($key + 1) / 7) <= 3) ? '' : 'hide') ?>"
            onclick="setIndexVideosAutoPlay('<?php echo $value->urlIframe ?>','<?php echo $value->video_tag ?>','<?php echo $value->title ?>','','<?php echo $value->create_datetime ?>','<?php echo $value->video_id ?>')">
            <td>
                <div class="box-list-video-side">
                    <div class="icon-play">
                        <i class="fa fa-play"></i>
                    </div>
                    <img width="168" height="94"
                         src="<?= $value->urlImg; ?>">
                </div>
            </td>
            <td>
                <div class="box-content-list">
                    <?= $value->title ?>
                </div>
                <?php if((!empty($dataVideo[0]) and $value->video_id==$dataVideo[0]->video_id)){ ?>
                    <span class="label label-danger">
                                                                    <i class="fa fa-play" aria-hidden="true"></i> กำลังเล่น
                                                                </span>
                <?php }else{ ?>
                    <span class="label">
                                                                    <?= date('d-M-Y', strtotime($value->create_datetime)); ?>
                                                                </span>
                <?php }?>
            </td>
        </tr>
    <?php } ?>
<?php } ?>



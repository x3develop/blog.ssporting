<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="shortcut icon" type="images/x-icon" href="/images/favicon.ico.png">
    <title>FIFA World Cup 2018</title>

    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/worldcup2018.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
</head>
<?php
require_once __DIR__ . "/bootstrap.php";
$mcon = new \controller\MatchController();
$matches = $mcon->getAvailable();


?>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="container">
    <div class="row">
        <div class="top-container-fifa-mobile">
            <img src="/images/top-bg-fifa-text.png">
            <p>ทายทีมเข้ารอบตั้งแต่รอบ 8 ทีม จนถึงรอบชิงชนะเลิศ โดยแบ่งเป็น 4 ช่วง
                เพื่อแจกของรางวัลมูลค่ากว่า 50,000 บาท</p>
            <div class="btn-sign-FB"></div>
        </div>
        <div class="container-fifa-mobile">


            <!--            Round 18-->
            <div class="content-fifa-mobile-box" style="height: 390px;">

                <div class="fifa-mobile-box-href">

                    <div class="box-fifa-16 box-fifa-match" style="margin: 69px 0 0 10px;">
                        <div class="check-box" mid="1">

                        </div>
                        <?php if (array_key_exists(1, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[1]->mid; ?>"
                                 tid="<?php echo $matches[1]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[1]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[1]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[1]->mid; ?>"
                                 tid="<?php echo $matches[1]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[1]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[1]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="box-fifa-16 box-fifa-match" style="margin: 120px 0 0 10px;">
                        <div class="check-box" mid="2">

                        </div>
                        <?php if (array_key_exists(2, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[2]->mid; ?>"
                                 tid="<?php echo $matches[2]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[2]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[2]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[2]->mid; ?>"
                                 tid="<?php echo $matches[2]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[2]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[2]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="box-fifa-16 box-fifa-match" style="margin: 178px 0 0 10px;">
                        <div class="check-box" mid="3">

                        </div>
                        <?php if (array_key_exists(3, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[3]->mid; ?>"
                                 tid="<?php echo $matches[3]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[3]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[3]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[3]->mid; ?>"
                                 tid="<?php echo $matches[3]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[3]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[3]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="box-fifa-16 box-fifa-match" style="margin: 233px 0 0 10px;">
                        <div class="check-box" mid="4">

                        </div>
                        <?php if (array_key_exists(4, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[4]->mid; ?>"
                                 tid="<?php echo $matches[4]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[2]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[4]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[4]->mid; ?>"
                                 tid="<?php echo $matches[4]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[4]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[4]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="box-award-mobile">
                        <img src="/images/mony_preview.png">
                    </div>
                </div>
                <div class="fifa-mobile-box-href">
                    <div class="box-fifa-16 box-fifa-match" style="margin: 69px 15px 0;">
                        <div class="check-box" mid="5">

                        </div>
                        <?php if (array_key_exists(5, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[5]->mid; ?>"
                                 tid="<?php echo $matches[1]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[5]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[5]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[5]->mid; ?>"
                                 tid="<?php echo $matches[5]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[5]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[5]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="box-fifa-16 box-fifa-match" style="margin: 120px 15px 0;">
                        <div class="check-box" mid="6">

                        </div>
                        <?php if (array_key_exists(6, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[6]->mid; ?>"
                                 tid="<?php echo $matches[6]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[6]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[6]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[6]->mid; ?>"
                                 tid="<?php echo $matches[6]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[6]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[6]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="box-fifa-16 box-fifa-match" style="margin: 178px 15px 0;">
                        <div class="check-box" mid="7">

                        </div>
                        <?php if (array_key_exists(7, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[7]->mid; ?>"
                                 tid="<?php echo $matches[7]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[7]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[7]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[7]->mid; ?>"
                                 tid="<?php echo $matches[7]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[7]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[7]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="box-fifa-16 box-fifa-match" style="margin: 233px 15px 0;">
                        <div class="check-box" mid="8">

                        </div>
                        <?php if (array_key_exists(8, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[8]->mid; ?>"
                                 tid="<?php echo $matches[8]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[8]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[8]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[8]->mid; ?>"
                                 tid="<?php echo $matches[8]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[8]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[8]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>

                    </div>
                    <div class="box-award-mobile">
                        <h4>เงินสด 2,000 บาท</h4>
                        <div class="btn-col save-selected">
                            ยืนยันการเลือกทีม
                        </div>
                    </div>
                </div>


            </div>


            <!--            Round 8-->
            <div class="content-fifa-mobile-box" style="height: 308px;">

                <div class="fifa-mobile-box-href">

                    <div class="box-fifa-16 box-fifa-match" style="margin: 69px 0 0 10px;">
                        <div class="check-box" mid="9">

                        </div>
                        <?php if (array_key_exists(9, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[9]->mid; ?>"
                                 tid="<?php echo $matches[9]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[9]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[9]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[9]->mid; ?>"
                                 tid="<?php echo $matches[9]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[9]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[9]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="box-fifa-16 box-fifa-match" style="margin: 120px 0 0 10px;">
                        <div class="check-box" mid="10">

                        </div>
                        <?php if (array_key_exists(10, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[10]->mid; ?>"
                                 tid="<?php echo $matches[1]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[10]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[10]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[10]->mid; ?>"
                                 tid="<?php echo $matches[10]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[10]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[10]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="box-award-mobile">
                        <img src="/images/shirt_preview.png">
                    </div>
                </div>
                <div class="fifa-mobile-box-href">
                    <div class="box-fifa-16 box-fifa-match" style="margin: 69px 15px 0;">
                        <div class="check-box" mid="11">

                        </div>
                        <?php if (array_key_exists(11, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[11]->mid; ?>"
                                 tid="<?php echo $matches[11]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[11]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[11]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[11]->mid; ?>"
                                 tid="<?php echo $matches[11]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[11]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[11]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="box-fifa-16 box-fifa-match" style="margin: 120px 15px 0;">
                        <div class="check-box" mid="12">

                        </div>
                        <?php if (array_key_exists(12, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[12]->mid; ?>"
                                 tid="<?php echo $matches[12]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[12]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[12]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[12]->mid; ?>"
                                 tid="<?php echo $matches[12]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[12]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[12]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>

                    </div>
                    <div class="box-award-mobile">
                        <h4>เสื้อลิขสิทธิ์แท้มูลค่า 5,000 บาท</h4>
                        <div class="btn-col save-selected">
                            ยืนยันการเลือกทีม
                        </div>
                    </div>
                </div>


            </div>


            <!--            Semi-finals-->
            <div class="content-fifa-mobile-box" style="height: 262px;">

                <div class="fifa-mobile-box-href">

                    <div class="box-fifa-16 box-fifa-match" style="margin: 69px 0 0 10px;">
                        <div class="check-box" mid="13">

                        </div>
                        <?php if (array_key_exists(13, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[13]->mid; ?>"
                                 tid="<?php echo $matches[13]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[13]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[13]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[13]->mid; ?>"
                                 tid="<?php echo $matches[13]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[13]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[13]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="box-award-mobile">
                        <img src="/images/ps4-preview.png">
                    </div>
                </div>
                <div class="fifa-mobile-box-href">
                    <div class="box-fifa-16 box-fifa-match" style="margin: 69px 15px 0;">
                        <div class="check-box" mid="14">

                        </div>
                        <?php if (array_key_exists(14, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[14]->mid; ?>"
                                 tid="<?php echo $matches[14]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[14]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[14]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[14]->mid; ?>"
                                 tid="<?php echo $matches[14]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[14]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[14]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>

                    </div>
                    <div class="box-award-mobile">
                        <h4>PS4 มูลค่า 15,000 บาท</h4>
                        <div class="btn-col save-selected">
                            ยืนยันการเลือกทีม
                        </div>
                    </div>
                </div>


            </div>


            <!--            Final-->
            <div class="content-fifa-mobile-box" style="height: 348px;">
                <div class="content-fifa-mobile-box-final box-fifa-16 box-fifa-final" style="margin-top: 98px;">
                    <div class="box-fifa-match-final">
                        <div class="check-box" mid="15">

                        </div>

                        <?php if (array_key_exists(15, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[15]->mid; ?>"
                                 tid="<?php echo $matches[15]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[15]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[15]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>

                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[15]->mid; ?>"
                                 tid="<?php echo $matches[15]->bid; ?>">
                                <ul>


                                    <li class="flag-fifa-16 text-right">
                                        <img src="<?php echo $matches[15]->teamb->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16 pull-right">
                                        <span><?php echo strtoupper(substr($matches[15]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>


                                    <li class="flag-fifa-16 text-right">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16 pull-right"><span>???</span></li>
                                </ul>
                            </div>
                        <?php } ?>


                    </div>

                </div>

                <div class="box-award-mobile" style="margin-top: 100px;">
                    <img src="/images/iphone_preview.png">
                    <h4>IPhone X มูลค่า 35,000 บาท</h4>
                    <div class="btn-col save-selected">
                        ยืนยันการเลือกทีม
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="reward-board" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="exampleModalLongTitle">ประกาศรายชื่อผู้โชคดี รอบ Final</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive" src="/worldcup/images/final.jpg">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">
                        <span style="color: red;font-size: 18px">หมายเหตุ: ผู้ที่ได้รับรางวัลต้องติดต่อขอรับรางวัลภายในวันที่ 17 ก.ค. 2561 เวลา 18.00น. หากเลยกำหนดถือว่าสละสิทธิ์</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                <button type="button" id="dismiss-reward" class="btn btn-primary">ไม่ต้องแสดงอีก</button>
            </div>
        </div>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://connect.facebook.net/en_US/sdk.js"></script>

<script type='text/javascript'>
    var hassession = <?php echo (isset($_SESSION['login'])) ? 1 : 0; ?>;
</script>

<script>
    FB.init({
        // appId      : '1630409860331473',
        appId: '188797391962899',
        // appId: '1761102130633089',
        cookie: true,  // enable cookies to allow the server to access
                       // the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.8
    });
</script>
<script src="/worldcup/js/wc.js"></script>
</body>
</html>
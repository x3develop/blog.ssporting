<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/27/2018
 * Time: 4:53 PM
 */

require_once __DIR__ . "/../bootstrap.php";


$fbid = $_REQUEST['fbid'];
$list = json_decode($_REQUEST['list']);


$bcon = new \controller\BetController();
$rs=[];
foreach ($list as $mid=>$tid){
    $rs[] = $bcon->add($fbid, $mid, $tid);
}

header("ContentType:application/json");

echo json_encode($rs);
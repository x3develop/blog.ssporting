<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 13/6/2561
 * Time: 17:20 น.
 */

require_once __DIR__ . "/../bootstrap.php";

$round = $_REQUEST['round'];

$ccon = new \controller\CandidateController();

$list = $ccon->getAllCandidate($round);

header('Content-Type: application/csv; charset=UTF-8');
header("Content-Disposition: attachment;filename=player_round{$round}.csv;");

foreach ($list as $d) {
    echo $d->fb_id . "," . $d->user->name . "," . $d->round . "," . $d->created_date . "\n";
}


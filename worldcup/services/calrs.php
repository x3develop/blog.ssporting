<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/27/2018
 * Time: 4:53 PM
 */

require_once __DIR__ . "/../bootstrap.php";


$mid = $_REQUEST['mid'];
$bcon = new \controller\BetController();
$rs['row'] = $bcon->calResult($mid);
header("ContentType:application/json");
echo json_encode($rs);
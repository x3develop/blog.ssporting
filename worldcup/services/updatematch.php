<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/26/2018
 * Time: 2:26 PM
 */

require_once __DIR__ . "/../bootstrap.php";

$mid = $_REQUEST['mid'];
$aid = $_REQUEST['aid'];
$bid = $_REQUEST['bid'];
$play_date = $_REQUEST['play_date'];
$deadline = $_REQUEST['deadline'];
$winner = $_REQUEST['winner'];

$mcon = new \controller\MatchController();

$match = $mcon->update($mid, $aid, $bid, $winner, $play_date, $deadline);

header("ContentType:application/json");
echo json_encode($match);

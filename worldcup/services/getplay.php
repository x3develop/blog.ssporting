<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/27/2018
 * Time: 5:39 PM
 */

require_once __DIR__ . "/../bootstrap.php";


$fbid = $_REQUEST['fbid'];
$bcon = new \controller\BetController();
$rs = $bcon->getByFbid($fbid);
header("ContentType:application/json");
echo json_encode($rs);

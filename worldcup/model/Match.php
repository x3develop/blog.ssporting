<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/25/2018
 * Time: 5:07 PM
 */

namespace model;
require_once __DIR__ . "/../bootstrap.php";

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $table = "match";
    protected $fillable = ['mid', 'aid', 'bid', 'winner', 'start_time','deadline', 'created_at', 'updated_at'];

    public function teama()
    {
        return $this->hasOne(Team::class, "id", "aid");
    }

    public function teamb()
    {
        return $this->hasOne(Team::class, "id", "bid");
    }

    public function winnerteam()
    {
        return $this->hasOne(Team::class, "id", "winner");
    }

}
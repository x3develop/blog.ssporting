<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="shortcut icon" type="images/x-icon" href="/images/favicon.ico.png">
    <title>ทายทีมเข้ารอบ World Cup 2018</title>

    <meta property="og:title" content="ทายทีมเข้ารอบ World cup 2018"/>
    <meta property="og:description" content="ทายทีมเข้ารอบเพื่อรุ้นรับรางวัลใหญ่มากมาย iphoneX Ps4 เสื้อทีมกิฬาลิขสิทธิ์แท้"/>
    <meta property="og:image" content="https://ngoal.com/images/worldcup.jpg"/>

    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/worldcup2018.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <script src="/worldcup/vendor/components/jquery/jquery.js"></script>
    <script src="/css/bootstrap/js/bootstrap.js"></script>

</head>
<?php
require_once __DIR__ . "/bootstrap.php";

$isMobile = (bool)preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet' .
    '|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]' .
    '|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', $_SERVER['HTTP_USER_AGENT']);

if ($isMobile) {
    header('Location: /worldcup/mobile.php');
}
$mcon = new \controller\MatchController();
$matches = $mcon->getAvailable();


?>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="content-event">
    <div class="content-event-side-title">
        <div class="btn-sign-FB"></div>
    </div>
    <div class="content-fifa-world-cup">


        <!--        Round of 16-->
        <div class="columns-fifa01">
            <div class="title-columns-fifa">
                <h2>Round of 16</h2>
                <small>Wednesday 3 July</small>
            </div>
            <div class="content-fifa-16">
                <?php for ($i = 1; $i <= 8; $i++) { ?>
                    <div class="box-fifa-16">
                        <div class="check-box" mid="<?php echo $i; ?>">

                        </div>

                        <?php if (array_key_exists($i, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[$i]->mid; ?>"
                                 tid="<?php echo $matches[$i]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[$i]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[$i]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[$i]->mid; ?>"
                                 tid="<?php echo $matches[$i]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[$i]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[$i]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>

                                </ul>
                            </div>
                        <?php } ?>

                    </div>
                <?php } ?>
            </div>
            <div class="box-award">
                <img src="/images/mony_preview.png">
                <h3>เงินสด 2,000 บาท</h3>
            </div>
            <div class="btn-col">
                หมดเวลาทายผล
            </div>
        </div>


        <!--        Round of 8-->
        <div class="columns-fifa02">
            <div class="title-columns-fifa">
                <h2>Round of 8</h2>
                <small>Saturday 7 July</small>
            </div>
            <div class="content-top-fifa-8">
                <?php for ($i = 9; $i <= 10; $i++) { ?>
                    <div class="box-fifa-16 box-fifa-8">
                        <div class="check-box" mid="<?php echo $i; ?>">

                        </div>
                        <?php if (array_key_exists($i, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[$i]->mid; ?>"
                                 tid="<?php echo $matches[$i]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[$i]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[$i]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[$i]->mid; ?>"
                                 tid="<?php echo $matches[$i]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[$i]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[$i]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>

                                </ul>
                            </div>
                        <?php } ?>

                    </div>
                <?php } ?>
            </div>
            <div class="content-bottom-fifa-8">
                <?php for ($i = 11; $i <= 12; $i++) { ?>
                    <div class="box-fifa-16 box-fifa-8">
                        <div class="check-box" mid="<?php echo $i; ?>">

                        </div>
                        <?php if (array_key_exists($i, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[$i]->mid; ?>"
                                 tid="<?php echo $matches[$i]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[$i]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[$i]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[$i]->mid; ?>"
                                 tid="<?php echo $matches[$i]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[$i]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[$i]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>

                                </ul>
                            </div>
                        <?php } ?>

                    </div>
                <?php } ?>
            </div>

            <div class="box-award">
                <img src="/images/shirt_preview.png">
                <h3>เสื้อลิขสิทธิ์แท้มูลค่า 5,000 บาท</h3>
            </div>
            <div class="btn-col save-selected">
                ยืนยันการเลือกทีม
            </div>
        </div>


        <!--        Semi-final-->
        <div class="columns-fifa03">
            <div class="title-columns-fifa">
                <h2>Semi-final</h2>
                <small>Wednesday 11 July</small>
            </div>
            <div class="content-top-fifa-8" style="margin-top: 80px;">
                <div class="box-fifa-16 box-fifa-8">
                    <div class="check-box" mid="13">

                    </div>
                    <?php if (array_key_exists(13, $matches)) { ?>
                        <div class="pull-left box-fifa-16-haft select-team team-mark"
                             mid="<?php echo $matches[13]->mid; ?>"
                             tid="<?php echo $matches[13]->aid; ?>">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="<?php echo $matches[13]->teama->flag; ?>">
                                </li>
                                <li class="name-fifa-16">
                                    <span><?php echo strtoupper(substr($matches[13]->teama->name, 0, 3)); ?></span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft select-team team-mark"
                             mid="<?php echo $matches[13]->mid; ?>"
                             tid="<?php echo $matches[13]->bid; ?>">
                            <ul>
                                <li class="name-fifa-16">
                                    <span><?php echo strtoupper(substr($matches[13]->teamb->name, 0, 3)); ?></span></li>
                                <li class="flag-fifa-16">
                                    <img src="<?php echo $matches[13]->teamb->flag; ?>">
                                </li>
                            </ul>
                        </div>
                    <?php } else { ?>
                        <div class="pull-left box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-default.jpg">
                                </li>
                                <li class="name-fifa-16"><span>???</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="name-fifa-16"><span>???</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-default.jpg">
                                </li>

                            </ul>
                        </div>
                    <?php } ?>

                </div>
            </div>
            <div class="content-top-fifa-8" style="margin-top: 113px;">
                <div class="box-fifa-16 box-fifa-8">
                    <div class="check-box" mid="14">

                    </div>
                    <?php if (array_key_exists(14, $matches)) { ?>
                        <div class="pull-left box-fifa-16-haft select-team team-mark"
                             mid="<?php echo $matches[14]->mid; ?>"
                             tid="<?php echo $matches[14]->aid; ?>">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="<?php echo $matches[14]->teama->flag; ?>">
                                </li>
                                <li class="name-fifa-16">
                                    <span><?php echo strtoupper(substr($matches[14]->teama->name, 0, 3)); ?></span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft select-team team-mark"
                             mid="<?php echo $matches[14]->mid; ?>"
                             tid="<?php echo $matches[14]->bid; ?>">
                            <ul>
                                <li class="name-fifa-16">
                                    <span><?php echo strtoupper(substr($matches[14]->teamb->name, 0, 3)); ?></span></li>
                                <li class="flag-fifa-16">
                                    <img src="<?php echo $matches[14]->teamb->flag; ?>">
                                </li>
                            </ul>
                        </div>
                    <?php } else { ?>
                        <div class="pull-left box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-default.jpg">
                                </li>
                                <li class="name-fifa-16"><span>???</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="name-fifa-16"><span>???</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-default.jpg">
                                </li>

                            </ul>
                        </div>
                    <?php } ?>

                </div>
            </div>
            <div class="box-award" style="margin-top: 25px;">
                <img src="/images/ps4-preview.png">
                <h3>PS4 มูลค่า 15,000 บาท</h3>
            </div>
            <div class="btn-col save-selected">
                ยืนยันการเลือกทีม
            </div>
        </div>


        <!--        Final-->
        <div class="columns-fifa04">
            <div class="title-columns-fifa">
                <h2>Final</h2>
                <small>Sunday 15 July</small>
            </div>
            <div class="content-top-fifa-8" style="margin-top: 80px;">

                <div class="final-cup"><img src="/images/world-cup-logo.png"></div>

                <div class="box-fifa-16 box-fifa-final" style="margin-top: 140px;">
                    <div class="check-box" mid="15" style="margin-top: 34px;">

                    </div>
                    <?php if (array_key_exists(15, $matches)) { ?>
                        <div class="pull-left box-fifa-16-haft select-team team-mark"
                             mid="<?php echo $matches[15]->mid; ?>"
                             tid="<?php echo $matches[15]->aid; ?>">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="<?php echo $matches[15]->teama->flag; ?>">
                                </li>
                                <li class="name-fifa-16">
                                    <span><?php echo strtoupper(substr($matches[15]->teama->name, 0, 3)); ?></span></li>
                            </ul>
                        </div>

                        <div class="pull-right box-fifa-16-haft select-team team-mark"
                             mid="<?php echo $matches[15]->mid; ?>"
                             tid="<?php echo $matches[15]->bid; ?>">
                            <ul>


                                <li class="flag-fifa-16 text-right">
                                    <img src="<?php echo $matches[15]->teamb->flag; ?>">
                                </li>
                                <li class="name-fifa-16 pull-right">
                                    <span><?php echo strtoupper(substr($matches[15]->teamb->name, 0, 3)); ?></span></li>
                                </li>
                            </ul>
                        </div>
                    <?php } else { ?>
                        <div class="pull-left box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-default.jpg">
                                </li>
                                <li class="name-fifa-16"><span>???</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>


                                <li class="flag-fifa-16 text-right">
                                    <img src="/images/Flag-default.jpg">
                                </li>
                                <li class="name-fifa-16 pull-right"><span>???</span></li>
                            </ul>
                        </div>
                    <?php } ?>

                </div>
            </div>

            <div class="box-award" style="margin-top: 75px;">
                <img src="/images/iphone_preview.png">
                <h3>IPhone X มูลค่า 35,000 บาท</h3>
            </div>
            <div class="btn-col save-selected">
                ยืนยันการเลือกทีม
            </div>
        </div>
    </div>
</div>

<div class="container container-list">
    <div class="col-lg-12">
        <h3>เงื่อนไขการร่วมชิงรางวัล</h3>
        <ol>
            <li>เริ่มทายผลผ่านเว็บไซต์ ได้ตั้งแต่วันที่ 1 มิถุนายน 2561 สิ้นสุดวันที่ 14 กรกฏาคม 2561 โดย
                ทายผลผ่านเว็บไซต์หมดเขตส่งคำทายเวลา 23.59 น. โดยถือเวลาที่ทายผล ครั้งสุดท้ายเป็นสำคัญ
            </li>
            <li>ผู้ที่มีสิทธิ์ลุ้นรับรางวัล ต้องทายทีมเข้ารอบถูกต้องทั้งหมดในแต่ละรอบการแข่งขัน</li>
            <li>
                กำหนดสุ่มจับรางวัล และประกาศรายชื่อผู้โชคดีแบ่งเป็น 4 รอบ
                ทางเว็บ ngoal.com และทางเพสเฟสบุ๊ก ngoalth
                <ul>
                    <li>รอบที่ 1 วันที่ 6 กรกฎาคม</li>
                    <li>รอบที่ 2 วันที่ 10 กรกฎาคม</li>
                    <li>รอบที่ 3 วันที่ 15 กรกฎาคม</li>
                    <li>รอบที่ 4 วันที่ 16 กรกฎาคม</li>
                </ul>
            </li>
            <li>ผู้ได้รับรางวัลมีสิทธิรับรางวัลที่มีมูลค่าสูงสุดเพียงรางวัลเดียวเท่านั้น</li>
            <li>คำตัดสินของคณะกรรมการให้ถือเป็นที่สุด</li>
            <li>ทางเว็บไชต์ ngoal.com ขอสงวนสิทธิในการเปลี่ยนแปลงรายละเอียดหรือเงื่อนไขโดยไม่ต้องแจ้งให้ทราบล่วงหน้า
            </li>
        </ol>
    </div>

    <div class="col-lg-12">
        <h3>รางวัลสำหรับการทายผล</h3>
        <ul>
            <li>รางวัลรอบที่ 4 Iphone x มูลค่า 35,000 บาท จำนวน 1 รางวัล</li>
            <li>รางวัลรอบที่ 3 เครื่องเล่นเกม PS4 มูลค่า 15,000 บาท จำนวน 1 รางวัล</li>
            <li>รางวัลรอบที่ 2 เสื้อฟุตบอลทีมชาติเกรดนักเตะลิขสิทธิ์แท้ มูลค่า 5,000 บาท จำนวน 1 รางวัล</li>
            <li>รางวัลรอบที่ 1 เงินสด มูลค่า 2,000 บาท จำนวน 1 รางวัล</li>
        </ul>
        <p><u>
                <i>
                    *** ทุกรอบการแจกรางวัล จะแจกเสื้อลิขสิทธิ์แท้มูลค่า 290 บาท 5 ตัว/รอบ สำหรับผู้ตอบถูก
                </i>
            </u>
        </p>
    </div>
    <div class="col-lg-12">
        <h3>วิธีการร่วมสนุกทายผลผ่านเว็บไซต์</h3>
        <ol>
            <li>เลือกทายประเทศที่จะเข้ารอบฟุตบอลโลก 2018 ของแต่ละรอบการแข่งขันผ่านเว็บไซต์ www.ngoal.com/worldcup</li>
            <li>ลงทะเบียนในเว็บไซต์ โดยการ login ด้วย facebook ของจริงที่ลงทะเบียนมาแล้วไม่ต่ำกว่า 3 เดือน</li>
            <li>ผู้ร่วมกิจกรรมไม่สามารถเปลี่ยนคำตอบได้เมื่อกดยืนยันคำตอบแล้ว
                แต่สามารถตอบเพิ่มได้ในคู่ที่ยังไม่ได้เล่นจนกว่าจะหมดเขตทายผลของแต่ละรอบ
            </li>
            <li>ผู้ร่วมกิจกรรม 1 คน มีสิทธิเพียง 1 สิทธิเท่านั้น</li>
        </ol>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="reward-board" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="exampleModalLongTitle">ประกาศรายชื่อผู้โชคดี รอบ Final</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive" src="/worldcup/images/final.jpg">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">
                       <span style="color: red;font-size: 18px">หมายเหตุ: ผู้ที่ได้รับรางวัลต้องติดต่อขอรับรางวัลภายในวันที่ 17 ก.ค. 2561 เวลา 18.00น. หากเลยกำหนดถือว่าสละสิทธิ์</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                <button type="button" id="dismiss-reward" class="btn btn-primary">ไม่ต้องแสดงอีก</button>
            </div>
        </div>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://connect.facebook.net/en_US/sdk.js"></script>

<script type='text/javascript'>
    var hassession = <?php echo (isset($_SESSION['login'])) ? 1 : 1; ?>;
</script>

<script>
    FB.init({
        // appId      : '1630409860331473',
        appId: '188797391962899',
        // appId: '1761102130633089',
        cookie: true,  // enable cookies to allow the server to access
                       // the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.8
    });

</script>
<script src="/worldcup/js/wc.js"></script>

</body>
</html>
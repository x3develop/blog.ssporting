<!DOCTYPE html>
<html>
<head>
    <title>Facebook Login JavaScript Example</title>
    <meta charset="UTF-8">
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="/js/fb.js">

</script>

<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->

<fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
</fb:login-button>

<div id="status">
</div>

<?php
require_once __DIR__ . "/../bootstrap.php";
$mcon = new \controller\MatchController();
$matches = $mcon->getAvailable();

?>

<div class="row">
    <div class="col-lg-12">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <td>#</td>
                <td>Match ID</td>
                <td>A Team</td>
                <td>B Team</td>
                <td>DATE</td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($matches as $key => $match) { ?>
                <tr>
                    <td><?php echo $key; ?></td>
                    <td><?php echo $match->mid; ?></td>
                    <td><img src="<?php echo $match->teama->flag; ?>"><?php echo strtoupper($match->teama->name); ?>
                    </td>
                    <td><?php echo strtoupper($match->teamb->name); ?><img src="<?php echo $match->teamb->flag; ?>">
                    </td>
                    <td><?php echo $match->match_time; ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>


</body>
</html>
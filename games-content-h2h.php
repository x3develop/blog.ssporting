<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 6/26/2018
 * Time: 10:11 AM
 */
$id = ($_REQUEST["id"]) ? $_REQUEST['id'] : null;
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
$playMatch = new playMatch();
$matchView = $playMatch->getFirstMathById($id)[0];
$dataHeadToHead = $playMatch->getHeadToHead($matchView->team_home, $matchView->team_away);
$dataLeagueByHeadToHead = $playMatch->getLeagueByHeadToHead($matchView->team_home, $matchView->team_away);

if(!empty($dataHeadToHead)){ ?>
    <div class="content-state-game" style="width: 234px; margin-top: 40px;">
        <h2>HEAD TO HEAD</h2>
        <div class="head-state-game"></div>
    </div>
    <div class="col-sm-6 h2h-tabs-team1">
        <div class="pull-right">
            <!--            กราฟวงกลม-->
            <div class="section-state-box section-state-box-circle text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Chatdata Odds/ Result</h4>
                            </td>
                            <td class="text-right"></td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="col-sm-6">
                        <div class="box-circle-data">
                            <div id="h2hdonut-h-oresult"
                                 style="z-index: 1; width: 204px; height: 174px;">
                                <img src="/images/chart-simple.png">
                            </div>
                            <div class="text-h2hdonut-h-oresult">Odds</div>
                        </div>
                        <div class="data-chart">
                            <div class="clearfix">
                                <h4 id="oddPWin">41.4</h4>
                                <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                            </div>
                            <div class="clearfix">
                                <h4 id="oddPLose">58.6</h4>
                                <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                            </div>
                            <div class="clearfix">
                                <h4 id="oddPDraw">0</h4>
                                <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div class="box-circle-data">
                            <div id="h2hdonut-h-result"
                                 style="z-index: 1; width: 204px; height: 174px;">
                                <img src="/images/chart-simple.png">
                            </div>
                            <div class="text-h2hdonut-h-oresult">Result</div>
                        </div>
                        <div class="data-chart">
                            <div class="clearfix">
                                <h4 id="resultPWin">41.4</h4>
                                <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                            </div>
                            <div class="clearfix">
                                <h4 id="resultPLose">58.6</h4>
                                <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                            </div>
                            <div class="clearfix">
                                <h4 id="resultPDraw">0</h4>
                                <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--            ตาราง-->
            <div class="section-state-box text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><img src="<?= $matchView->teamHomePath ?>"> <?= $matchView->teamHomeEn ?></h4>
                            </td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-default dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Last Match(<label
                                            id="h2h-h-max-match"><?php echo((count($dataHeadToHead) >= 10) ? "10" : count($dataHeadToHead)); ?></label>)
                                        <span class="caret"></span>
                                    </button>
                                    <ul id="h2h-h-select-limit" class="dropdown-menu" aria-labelledby="dLabel">
                                        <?php
                                        if (count($dataHeadToHead) >= 20) {
                                            for ($i = 1; $i <= 20; $i++) { ?>
                                                <li><a onclick="viewHeadToHeadHome(<?= (($i)) ?>)">Last
                                                        Match(<?= (($i)) ?>)</a></li>
                                            <?php }
                                        } else {
                                            for ($i = 1; $i <= (count($dataHeadToHead) - 1); $i++) { ?>
                                                <li><a onclick="viewHeadToHeadHome(<?= (($i)) ?>)">Last
                                                        Match(<?= (($i)) ?>)</a></li>
                                            <?php }
                                        } ?>
                                        <li><a onclick="viewHeadToHeadHome(<?= count($dataHeadToHead) ?>)">Last
                                                Match(<?= count($dataHeadToHead) ?>)</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="box-table-result-state" style="padding: 0 15px;">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-pills" role="tablist">
                            <li role="presentation" class="active">
                                <a onclick="viewHeadToHeadHome()" href="#h2hHTotal" aria-controls="h2hTotal" role="tab"
                                   data-toggle="tab">Total</a></li>
                            <li role="presentation">
                                <a onclick="viewHeadToHeadHome()" href="#h2hHHome" aria-controls="h2hHome" role="tab"
                                   data-toggle="tab">Home</a></li>
                            <li role="presentation">
                                <a onclick="viewHeadToHeadHome()" href="#h2hHAway" aria-controls="h2hAway" role="tab"
                                   data-toggle="tab">Away</a></li>
                            <li class="pull-right">
                                <label class="checkbox-inline">
                                    <input onclick="viewHeadToHeadHome()" type="checkbox" class="checkbox_team1_all"
                                           value="all" checked> All
                                </label>
                                <?php foreach ($dataLeagueByHeadToHead as $key => $value) { ?>
                                    <label class="checkbox-inline">
                                        <input onclick="viewHeadToHeadHome()" type="checkbox"
                                               class="checkbox_team1_<?= $key ?>" value="<?= $key ?>"> <?= $value ?>
                                    </label>
                                <?php } ?>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active " id="h2hHTotal">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center mini-col">HDP</th>
                                        <th class="text-center mini-col">Odds</th>
                                        <th class="text-center mini-col">Result</th>
                                    </tr>
                                    </thead>
                                    <?php foreach ($dataHeadToHead as $key => $value) { ?>
                                        <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                            id="guide-row-original"
                                            class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                            <td>
                                                <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                <p><i class="fa fa-calendar"
                                                      aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                    | <i
                                                        class="fa fa-clock-o"
                                                        aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                </p>
                                            </td>
                                            <td class="text-right box-team-state">
                                                <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                <img src="<?= $value->teamHomePath ?>">
                                            </td>
                                            <td style="width: 12%;">
                                                <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                    - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                            </td>
                                            <td class="text-left box-team-state">
                                                <img src="<?= $value->teamAwayPath ?>">
                                                <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                            </td>
                                            <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                            <td class="text-center tr-odd">
                                                <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                    <img class="guide-result img-win"
                                                         src="/images/icon-stat/result/0.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                    <img class="guide-result img-lose"
                                                         src="/images/icon-stat/result/2.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                    <img class="guide-result img-draw"
                                                         src="/images/icon-stat/result/1.png">
                                                <?php } else { ?>
                                                    -
                                                <?php } ?>
                                            </td>
                                            <td class="text-center tr-result">
                                                <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                    <img class="guide-result img-win"
                                                         src="/images/icon-stat/result/0.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                    <img class="guide-result img-lose"
                                                         src="/images/icon-stat/result/2.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                    <img class="guide-result img-draw"
                                                         src="/images/icon-stat/result/1.png">
                                                <?php } else { ?>
                                                    -
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="h2hHHome">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center mini-col">HDP</th>
                                        <th class="text-center mini-col">Odds</th>
                                        <th class="text-center mini-col">Result</th>
                                    </tr>
                                    </thead>
                                    <?php foreach ($dataHeadToHead as $key => $value) { ?>
                                        <?php if ($matchView->teamAwayEn == $value->teamAwayEn) { ?>
                                            <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                                id="guide-row-original"
                                                class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                                <td>
                                                    <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                    <p><i class="fa fa-calendar"
                                                          aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                        | <i
                                                            class="fa fa-clock-o"
                                                            aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                    </p>
                                                </td>
                                                <td class="text-right box-team-state">
                                                    <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                    <img src="<?= $value->teamHomePath ?>">
                                                </td>
                                                <td>
                                                    <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                        - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                                </td>
                                                <td class="text-left box-team-state">
                                                    <img src="<?= $value->teamAwayPath ?>">
                                                    <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                                </td>
                                                <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                                <td class="text-center tr-odd">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center tr-result">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="h2hHAway">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center mini-col">HDP</th>
                                        <th class="text-center mini-col">Odds</th>
                                        <th class="text-center mini-col">Result</th>
                                    </tr>
                                    </thead>
                                    <?php foreach ($dataHeadToHead as $key => $value) { ?>
                                        <?php if ($matchView->teamAwayEn != $value->teamAwayEn) { ?>
                                            <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                                id="guide-row-original"
                                                class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                                <td>
                                                    <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                    <p><i class="fa fa-calendar"
                                                          aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                        | <i
                                                            class="fa fa-clock-o"
                                                            aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                    </p>
                                                </td>
                                                <td class="text-right box-team-state">
                                                    <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                    <img src="<?= $value->teamHomePath ?>">
                                                </td>
                                                <td>
                                                    <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                        - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                                </td>
                                                <td class="text-left box-team-state">
                                                    <img src="<?= $value->teamAwayPath ?>">
                                                    <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                                </td>
                                                <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                                <td class="text-center tr-odd">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center tr-result">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <div style="clear: both"></div>
    </div>
    <div class="col-sm-6 h2h-tabs-team2" style="background-color: #f1f1f1;">
        <div class="pull-left">
            <!--            กราฟวงกลม-->
            <div class="section-state-box section-state-box-circle text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Chatdata Odds/ Result</h4>
                            </td>
                            <td class="text-right">

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="col-sm-6">
                        <div class="box-circle-data">
                            <div id="h2hdonut-a-oresult" style="z-index: 1; width: 204px; height: 174px;">
                                <img src="/images/chart-simple.png">
                            </div>
                            <div class="text-h2hdonut-h-oresult">Odds</div>
                        </div>
                        <div class="data-chart">
                            <div class="clearfix">
                                <h4 id="oddPWin">41.4</h4>
                                <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                            </div>
                            <div class="clearfix">
                                <h4 id="oddPLose">58.6</h4>
                                <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                            </div>
                            <div class="clearfix">
                                <h4 id="oddPDraw">0</h4>
                                <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-circle-data">
                            <div id="h2hdonut-a-result" style="z-index: 1; width: 204px; height: 174px;">
                                <img src="/images/chart-simple.png">
                            </div>
                            <div class="text-h2hdonut-h-oresult">Result</div>
                        </div>
                        <div class="data-chart">
                            <div class="clearfix">
                                <h4 id="resultPWin">41.4</h4>
                                <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                            </div>
                            <div class="clearfix">
                                <h4 id="resultPLose">58.6</h4>
                                <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                            </div>
                            <div class="clearfix">
                                <h4 id="resultPDraw">0</h4>
                                <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--            ตาราง-->
            <div class="section-state-box text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><img src="<?= $matchView->teamAwayPath ?>"> <?= $matchView->teamAwayEn ?></h4>
                            </td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-default dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Last Match(<label
                                            id="h2h-a-max-match"><?php echo((count($dataHeadToHead) >= 10) ? "10" : count($dataHeadToHead)) ?></label>)
                                        <span class="caret"></span>
                                    </button>
                                    <ul id="h2h-a-select-limit" class="dropdown-menu">
                                        <?php
                                        if (count($dataHeadToHead) >= 20) {
                                            for ($i = 1; $i <= 20; $i++) { ?>
                                                <li><a onclick="viewHeadToHeadAway(<?= (($i)) ?>)">Last
                                                        Match(<?= (($i)) ?>)</a></li>
                                            <?php }
                                        } else {
                                            for ($i = 1; $i <= (count($dataHeadToHead) - 1); $i++) { ?>
                                                <li><a onclick="viewHeadToHeadAway(<?= (($i)) ?>)">Last
                                                        Match(<?= (($i)) ?>)</a></li>
                                            <?php }
                                        } ?>
                                        <li><a onclick="viewHeadToHeadAway(<?= count($dataHeadToHead) ?>)">Last
                                                Match(<?= count($dataHeadToHead) ?>)</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="box-table-result-state" style="padding: 0 15px;">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-pills" role="tablist">
                            <li role="presentation" class="active">
                                <a onclick="viewHeadToHeadAway()" href="#h2hATotal" aria-controls="h2hTotal" role="tab"
                                   data-toggle="tab">Total</a></li>
                            <li role="presentation">
                                <a onclick="viewHeadToHeadAway()" href="#h2hAHome" aria-controls="h2hHome" role="tab"
                                   data-toggle="tab">Home</a></li>
                            <li role="presentation">
                                <a onclick="viewHeadToHeadAway()" href="#h2hAAway" aria-controls="h2hAway" role="tab"
                                   data-toggle="tab">Away</a></li>
                            <li class="pull-right">
                                <label class="checkbox-inline">
                                    <input onclick="viewHeadToHeadAway()" type="checkbox" class="checkbox_team2_all"
                                           value="all" checked> All
                                </label>
                                <?php foreach ($dataLeagueByHeadToHead as $key => $value) { ?>
                                    <label class="checkbox-inline">
                                        <input onclick="viewHeadToHeadAway()" type="checkbox"
                                               class="checkbox_team2_<?= $key ?>" value="<?= $key ?>"> <?= $value ?>
                                    </label>
                                <?php } ?>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="h2hATotal">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center mini-col">HDP</th>
                                        <th class="text-center mini-col">Odds</th>
                                        <th class="text-center mini-col">Result</th>
                                    </tr>
                                    </thead>
                                    <?php foreach ($dataHeadToHead as $key => $value) { ?>
                                        <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                            id="guide-row-original"
                                            class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                            <td>
                                                <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                <p><i class="fa fa-calendar"
                                                      aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                    | <i
                                                        class="fa fa-clock-o"
                                                        aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                </p>
                                            </td>
                                            <td class="text-right box-team-state">
                                                <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                <img src="<?= $value->teamHomePath ?>">
                                            </td>
                                            <td style="width: 12%;">
                                                <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                    - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                            </td>
                                            <td class="text-left box-team-state">
                                                <img src="<?= $value->teamAwayPath ?>">
                                                <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                            </td>
                                            <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                            <td class="text-center tr-odd">
                                                <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                    <img class="guide-result img-win"
                                                         src="/images/icon-stat/result/0.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                    <img class="guide-result img-lose"
                                                         src="/images/icon-stat/result/2.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                    <img class="guide-result img-draw"
                                                         src="/images/icon-stat/result/1.png">
                                                <?php } else { ?>
                                                    -
                                                <?php } ?>
                                            </td>
                                            <td class="text-center tr-result">
                                                <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                    <img class="guide-result img-win"
                                                         src="/images/icon-stat/result/0.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                    <img class="guide-result img-lose"
                                                         src="/images/icon-stat/result/2.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                    <img class="guide-result img-draw"
                                                         src="/images/icon-stat/result/1.png">
                                                <?php } else { ?>
                                                    -
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="h2hAHome">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center mini-col">HDP</th>
                                        <th class="text-center mini-col">Odds</th>
                                        <th class="text-center mini-col">Result</th>
                                    </tr>
                                    </thead>
                                    <?php foreach ($dataHeadToHead as $key => $value) { ?>
                                        <?php if ($matchView->teamAwayEn != $value->teamAwayEn) { ?>
                                            <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                                id="guide-row-original"
                                                class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                                <td>
                                                    <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                    <p><i class="fa fa-calendar"
                                                          aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                        | <i
                                                            class="fa fa-clock-o"
                                                            aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                    </p>
                                                </td>
                                                <td class="text-right box-team-state">
                                                    <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                    <img src="<?= $value->teamHomePath ?>">
                                                </td>
                                                <td>
                                                    <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                        - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                                </td>
                                                <td class="text-left box-team-state">
                                                    <img src="<?= $value->teamAwayPath ?>">
                                                    <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                                </td>
                                                <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                                <td class="text-center tr-odd">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center tr-result">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="h2hAAway">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center mini-col">HDP</th>
                                        <th class="text-center mini-col">Odds</th>
                                        <th class="text-center mini-col">Result</th>
                                    </tr>
                                    </thead>
                                    <?php foreach ($dataHeadToHead as $key => $value) { ?>
                                        <?php if ($matchView->teamAwayEn == $value->teamAwayEn) { ?>
                                            <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                                id="guide-row-original"
                                                class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                                <td>
                                                    <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                    <p><i class="fa fa-calendar"
                                                          aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                        | <i
                                                            class="fa fa-clock-o"
                                                            aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                    </p>
                                                </td>
                                                <td class="text-right box-team-state">
                                                    <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                    <img src="<?= $value->teamHomePath ?>">
                                                </td>
                                                <td>
                                                    <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                        - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                                </td>
                                                <td class="text-left box-team-state">
                                                    <img src="<?= $value->teamAwayPath ?>">
                                                    <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                                </td>
                                                <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                                <td class="text-center tr-odd">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center tr-result">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both"></div>
    </div>
<?php } ?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="/css/style-new.css">
<!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="/js/flogin/src/flogin.js"></script>
    <script src="/js/moment.js"></script>
    <script src="js/donutchart.js"></script>
    <script src="/js/utils.js"></script>
<!--    <script src="/js/statistics.js"></script>-->
    <script>
        function imgError(image) {
            image.onerror = "";
            image.src = "images/teams_clean/team_default_32x32.png";
            return true;
        }
        function flagNotFound(image) {
            image.src = "images/countries/0.png";
        }
    </script>

</head>
<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/MatchM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/HomeM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/statistics.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/LogoManager.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/DataMap.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/NewsM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/langLeague.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playLeague.php";
$playLeague = new playLeague();
$playMatch = new playMatch();

$mid = ($_REQUEST["mid"]) ? $_REQUEST['mid'] : 0;
$match=array();
$match = $playMatch->getFirstMathStatusBet(30);

if (!empty($mid)) {
    $matchView = $playMatch->getFirstMathById($mid)[0];
} elseif (!empty($match[0]->id)) {
    $matchView = $playMatch->getFirstMathById($match[0]->id)[0];
}
var_dump($matchView);
$dataHeadToHead=$playMatch->getHeadToHead($matchView->team_home,$matchView->team_away);
$dataFormGuideHome=$playMatch->getFormGuide($matchView->team_home,50);
$dataFormGuideAway=$playMatch->getFormGuide($matchView->team_away,50);
$dataNextMatchHome=$playMatch->getNextMatch($matchView->team_home,50);
$dataNextMatchAway=$playMatch->getNextMatch($matchView->team_away,50);
$tableLeague = $playLeague->checkTeamByLeague(1);



//var_dump($matchView);
//var_dump($matchView->team_home." :: ".$matchView->team_away);
//exit;
$matchm = new MatchM();
$logoman = new LogoManager();
$statistics = new Statistics();
$homem = new HomeM();
$langLeague=new langLeague();
$mid=1802366;
$gamedata = $matchm->getGameByMidPack($mid);
if (empty($gamedata["match"])) {
    header("fixtures.php");
}

$eventcircle = $matchm->getLiveEventPack($mid);
$stitutes = $matchm->getSubstitutesPack($mid);

//$gameHistory = $statistics->getGameHistory($mid, 'game', '2000-01-01');
//$gameTotal = $statistics->getTotalGameHistory($mid, 'game', '2000-01-01');
//$homeHistory = $statistics->getGameHistory($mid, 'home', '2000-01-01');
//$homeTotal = $statistics->getTotalGameHistory($mid, 'home', '2000-01-01');
//$awayHistory = $statistics->getGameHistory($mid, 'away', '2000-01-01');
//$awayTotal = $statistics->getTotalGameHistory($mid, 'away', '2000-01-01');
$lineups = $matchm->getGameLineUpPack($mid);
$gameinfo = $matchm->getGameInfoByMidPack($mid);
$teamfixture = $matchm->getTeamFixturePack($mid);
$livestats = $matchm->getLiveStatsPack($mid);

//$nameLeagueThai=(($langLeague->getNameLeague($gamedata["match"][1]))?$langLeague->getNameLeague($gamedata["match"][1]):$gamedata["match"][38]);
//$nameTeamHomeThai=(($langLeague->getNameTeam($gamedata["match"][11]))?$langLeague->getNameTeam($gamedata["match"][11]):$gamedata["match"][13]);
//$nameTeamAwayThai=(($langLeague->getNameTeam($gamedata["match"][12]))?$langLeague->getNameTeam($gamedata["match"][12]):$gamedata["match"][14]);

$datamap = new DataMap();
$newsm = new NewsM();

//type:{0:Goal, 1:Pen, 2:Own Goal, 3:Yellow, 4:Red, 5: Y to R}
$event_type = ['Goal', 'Pen', 'Own Goal', 'Yellow', 'Red', 'Y to R'];
$stats_type = array(3 => 'Shots', 4 => 'Shots on Goal', 5 => 'Fouls', 6 => 'Corner Kicks', 8 => 'Free Kicks', 9 => 'Offsides', 11 => 'Yellow Cards', 13 => 'Red Cards', 14 => 'Ball Possession %', 15 => 'Saves');

function calLong($min)
{
    $lastlong = 900;
    $maxlong = 1010;
    $fmin = explode("+", $min);
    $m = intval($min);
    if (count($fmin) == 2) {
        $m = intval($fmin[0]) + intval($fmin[1]);
    }
    $long = $m * 10;
    if ($long > $maxlong) {
        $long = $maxlong;
    }
    return $long;
}
?>
<body>
<?php //include 'header.php'; ?>
<!--index menu-->
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu.php'; ?>
<nav class="navbar navbar-menu-v2">
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/view/menu/navbar-menu-v2.php'; ?>
</nav>

<input type="hidden" id="game-data" mid="<?php echo $gamedata['match'][0] ?>" slid="<?php echo $gamedata['match'][1] ?>"
       lid="<?php echo $gamedata['match'][2] ?>" hid="<?php echo $gamedata['match'][11] ?>"
       gid="<?php echo $gamedata['match'][12] ?>"/>

<div class="wrapper-news-articles">
    <div class="containter-fluid">
        <div class="bx-head-fixtures bgtop" style="padding: 133px 0px 108px 0px;">
            <div class="hilight-cover-top"></div>
            <div class="articles-title">
                <div class="container">
                    <div class="wrap-game-live">
                        <div class="col-sm-4">
                            <div class="text-right">
                                <div class="name-team1"><?php
                                    if (!empty($gamedata["match"])) {
                                        echo $gamedata["match"][13];
                                    }
                                    ?>
                                    <img src="<?php echo $logoman->getLogo($gamedata["match"][11], 2); ?>"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <?php
                            $homescore = "";
                            $awayscore = "";
                            $scores = explode("-", $gamedata["match"][19]);
                            if (count($scores) == 2) {
                                $homescore = $scores[0];
                                $awayscore = $scores[1];
                            }
                            ?>
                            <label class="txt-score pull-left"><?php echo $homescore; ?></label>

                            <div class="bt-bet"><a href="/game.php?mid=<?php echo $mid; ?>">ทายผล</a></div>
                            <label class="txt-score pull-right"><?php echo $awayscore; ?></label>
                            <?php $dt = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $gamedata["match"][32]); ?>
                            <div style="font-size: 22px;"><?php echo $dt->addHours(8)->format("H:i"); ?></div>
                            <div class="bx-league"><?php echo $dt->format("d/m/Y"); ?></div>
                            <div><?php echo $gamedata["match"][38]; ?></div>
                        </div>
                        <div class="col-sm-4">
                            <div class="text-left">
                                <div class="name-team2"><img
                                        src="<?php echo $logoman->getLogo($gamedata["match"][12], 2); ?>">
                                    <?php
                                    if (!empty($gamedata["match"])) {
                                        echo $gamedata["match"][14];
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>
            </div>
            <!--            <div class="background-image_image3"></div>-->
        </div>
    </div>
    <div class="container" style="position: relative;">
        <div class="tab-head tab-head-top text-center">
            <ul>
                <li class="active">All</li>
                <?php if (!empty($lineups["lineup"])) { ?>
                    <li><a href="#lineup-section">Line-Ups</a></li>
                <?php } ?>
                <?php if (count($livestats['livestats']) > 1) { ?>
                    <li><a href="#stat-section">Statistics</a></li>
                <?php } ?>
                <li><a href="#h2h-section">Head-to-Head</a></li>
                <?php if(!empty($news)){ ?>
                    <li><a href="#news-section">News</a></li>
                <?php } ?>
            </ul>
        </div>
        <?php if (!empty($eventcircle["event"]) || !empty($stitutes["substitutes"])) { ?>
            <div class="rows">
                <div class="tab-head-title text-center mg-bt15">
                    <strong>Match Info</strong>
                </div>
                <div class="col-sm-12">
                    <div class="bx-line-events">
                        <div class="end-time-play">90'</div>
                        <div class="line-events team1">
                            <?php foreach ($eventcircle["event"] as $eventc) { ?>
                                <?php if ($eventc[6] == 0) { ?>
                                    <span class="events" style="left: <?php echo calLong($eventc[2]); ?>px;">
                            <img src="/images/stat/<?php echo $eventc[4]; ?>.png">

                            <div class="popover-tooltip-events">
                                <div><?php echo $eventc[2] . "'"; ?></div>
                                <?php echo $eventc[7]; ?>
                            </div>
                        </span>
                                <?php } ?>
                            <?php } ?>
                            <?php foreach ($stitutes["substitutes"] as $sub) { ?>
                                <?php if ($sub[4] == 0) { ?>
                                    <span class="events" style="left: <?php echo calLong($sub[3]); ?>px;">
                            <img src="/images/stat/swap.png">
                            <div class="popover-tooltip-events">
                                <div><?php echo $sub[3] . "'"; ?></div>
                                <?php echo $sub[5] . " => " . $sub[6]; ?>
                            </div>
                        </span>
                                <?php } ?>
                            <?php } ?>
                        </div>
                        <div class="line-events team2">
                            <?php foreach ($eventcircle["event"] as $eventc) { ?>
                                <?php if ($eventc[6] == -1) { ?>
                                    <span class="events" style="left: <?php echo calLong($eventc[2]); ?>px;">
                            <img src="/images/stat/<?php echo $eventc[4]; ?>.png">
                            <div class="popover-tooltip-events">
                                <div><?php echo $eventc[2] . "'"; ?></div>
                                <?php echo $eventc[7]; ?>
                            </div>
                        </span>
                                <?php } ?>
                            <?php } ?>

                            <?php foreach ($stitutes["substitutes"] as $sub) { ?>
                                <?php if ($sub[4] == -1) { ?>
                                    <span class="events" style="left: <?php echo calLong($sub[3]); ?>px;">
                            <img src="/images/stat/swap.png">
                            <div class="popover-tooltip-events">
                                <div><?php echo $sub[3] . "'"; ?></div>
                                <?php echo $sub[5] . " => " . $sub[6]; ?>
                            </div>
                        </span>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div>
            <div id="lineup-section" class="rows mg-bt15 <?php echo (empty($lineups["lineup"])) ? 'hide' : ''; ?>">
                <div class="tab-head-title text-center mg-bt15">
                    <strong>Line-Ups</strong>
                </div>
                <div class="bx-lineup">
                    <div class="col-sm-6 text-left">
                        <div>ผู้จัดการทีม <span>-</span></div>
                        <div>รูปแบบการเล่น
                            <span><?php echo array_key_exists(9, $gameinfo["game_info"]) ? $gameinfo["game_info"][9] : "'-'"; ?></span>
                        </div>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div><span>-</span> ผู้จัดการทีม</div>
                        <div>
                            <span><?php echo array_key_exists(10, $gameinfo["game_info"]) ? $gameinfo["game_info"][10] : "'-'"; ?></span>
                            รูปแบบการเล่น
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
                <div class="rows">
                    <div class="col-sm-12 mg-bt15">
                        <div class="box-lineup">
                            <div class="pull-left" style="width: 50%;">
                                <table>
                                    <tr>
                                        <td>
                                            <?php foreach ($lineups["lineup"] as $lineup) { ?>
                                                <?php if ($lineup[8] == "home" && $lineup[6] == 0 && $lineup[7] == 3) { ?>
                                                    <?php
                                                    $expplayer = explode(" ", $lineup[4]);
                                                    $pnumber = 0;

                                                    if (array_key_exists(0, $expplayer)) {
                                                        if (is_numeric($expplayer[0])) {
                                                            $pnumber = $expplayer[0];
                                                        }
                                                    }
                                                    ?>
                                                    <div class="wrap-box-player">
                                                        <div class="bx-player">
                                                            <div
                                                                class="label-number"><?php echo $pnumber; ?></div>
                                                            <img src="images/img-user.jpg">
                                                        </div>
                                                        <div class="popover-tooltip">
                                                            <div>Goal 30'</div>
                                                            บาโลเตลี่
                                                        </div>
                                                        <div><?php echo str_replace($pnumber, "", $lineup[4]); ?></div>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php foreach ($lineups["lineup"] as $lineup) { ?>
                                                <?php if ($lineup[8] == "home" && $lineup[6] == 1 && $lineup[7] == 3) { ?>
                                                    <?php
                                                    $expplayer = explode(" ", $lineup[4]);
                                                    $pnumber = 0;

                                                    if (array_key_exists(0, $expplayer)) {
                                                        if (is_numeric($expplayer[0])) {
                                                            $pnumber = $expplayer[0];
                                                        }
                                                    }
                                                    ?>
                                                    <div class="wrap-box-player">
                                                        <div class="bx-player">
                                                            <div
                                                                class="label-number"><?php echo $pnumber; ?></div>
                                                            <img src="images/img-user.jpg">
                                                        </div>
                                                        <div class="popover-tooltip">
                                                            <div>Goal 30'</div>
                                                            บาโลเตลี่
                                                        </div>
                                                        <div><?php echo str_replace($pnumber, "", $lineup[4]); ?></div>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div class="popover-tooltip">
                                                    <div>Goal 30'</div>
                                                    บาโลเตลี่
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div class="popover-tooltip">
                                                    <div>Goal 30'</div>
                                                    บาโลเตลี่
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div class="popover-tooltip">
                                                    <div>Goal 30'</div>
                                                    บาโลเตลี่
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div class="popover-tooltip">
                                                    <div>Goal 30'</div>
                                                    บาโลเตลี่
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                        </td>
                                        <td>
                                            <?php foreach ($lineups["lineup"] as $lineup) { ?>
                                                <?php if ($lineup[8] == "home" && $lineup[6] == 2 && $lineup[7] == 3) { ?>
                                                    <?php
                                                    $expplayer = explode(" ", $lineup[4]);
                                                    $pnumber = 0;

                                                    if (array_key_exists(0, $expplayer)) {
                                                        if (is_numeric($expplayer[0])) {
                                                            $pnumber = $expplayer[0];
                                                        }
                                                    }
                                                    ?>
                                                    <div class="wrap-box-player">
                                                        <div class="bx-player">
                                                            <div
                                                                class="label-number"><?php echo $pnumber; ?></div>
                                                            <img src="images/img-user.jpg">
                                                        </div>
                                                        <div class="popover-tooltip">
                                                            <div>Goal 30'</div>
                                                            บาโลเตลี่
                                                        </div>
                                                        <div><?php echo str_replace($pnumber, "", $lineup[4]); ?></div>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                        </td>
                                        <td>
                                            <?php foreach ($lineups["lineup"] as $lineup) { ?>
                                                <?php if ($lineup[8] == "home" && $lineup[6] == 3 && $lineup[7] == 3) { ?>
                                                    <?php
                                                    $expplayer = explode(" ", $lineup[4]);
                                                    $pnumber = 0;

                                                    if (array_key_exists(0, $expplayer)) {
                                                        if (is_numeric($expplayer[0])) {
                                                            $pnumber = $expplayer[0];
                                                        }
                                                    }
                                                    ?>
                                                    <div class="wrap-box-player">
                                                        <div class="bx-player">
                                                            <div
                                                                class="label-number"><?php echo $pnumber; ?></div>
                                                            <img src="images/img-user.jpg">
                                                        </div>
                                                        <div class="popover-tooltip">
                                                            <div>Goal 30'</div>
                                                            บาโลเตลี่
                                                        </div>
                                                        <div><?php echo str_replace($pnumber, "", $lineup[4]); ?></div>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="pull-right" style="width: 50%;">
                                <table>
                                    <tr>
                                        <td>
                                            <?php foreach ($lineups["lineup"] as $lineup) { ?>
                                                <?php if ($lineup[8] == "away" && $lineup[6] == 3 && $lineup[7] == 3) { ?>
                                                    <?php
                                                    $expplayer = explode(" ", $lineup[4]);
                                                    $pnumber = 0;

                                                    if (array_key_exists(0, $expplayer)) {
                                                        if (is_numeric($expplayer[0])) {
                                                            $pnumber = $expplayer[0];
                                                        }
                                                    }
                                                    ?>
                                                    <div class="wrap-box-player">
                                                        <div class="bx-player">
                                                            <div
                                                                class="label-number"><?php echo $pnumber; ?></div>
                                                            <img src="images/img-user.jpg">
                                                        </div>
                                                        <div class="popover-tooltip">
                                                            <div>Goal 30'</div>
                                                            บาโลเตลี่
                                                        </div>
                                                        <div><?php echo str_replace($pnumber, "", $lineup[4]); ?></div>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                        </td>
                                        <td>
                                            <?php foreach ($lineups["lineup"] as $lineup) { ?>
                                                <?php if ($lineup[8] == "away" && $lineup[6] == 2 && $lineup[7] == 3) { ?>
                                                    <?php
                                                    $expplayer = explode(" ", $lineup[4]);
                                                    $pnumber = 0;

                                                    if (array_key_exists(0, $expplayer)) {
                                                        if (is_numeric($expplayer[0])) {
                                                            $pnumber = $expplayer[0];
                                                        }
                                                    }
                                                    ?>
                                                    <div class="wrap-box-player">
                                                        <div class="bx-player">
                                                            <div
                                                                class="label-number"><?php echo $pnumber; ?></div>
                                                            <img src="images/img-user.jpg">
                                                        </div>
                                                        <div class="popover-tooltip">
                                                            <div>Goal 30'</div>
                                                            บาโลเตลี่
                                                        </div>
                                                        <div><?php echo str_replace($pnumber, "", $lineup[4]); ?></div>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                        </td>
                                        <td>
                                            <?php foreach ($lineups["lineup"] as $lineup) { ?>
                                                <?php if ($lineup[8] == "away" && $lineup[6] == 1 && $lineup[7] == 3) { ?>
                                                    <?php
                                                    $expplayer = explode(" ", $lineup[4]);
                                                    $pnumber = 0;

                                                    if (array_key_exists(0, $expplayer)) {
                                                        if (is_numeric($expplayer[0])) {
                                                            $pnumber = $expplayer[0];
                                                        }
                                                    }
                                                    ?>
                                                    <div class="wrap-box-player">
                                                        <div class="bx-player">
                                                            <div
                                                                class="label-number"><?php echo $pnumber; ?></div>
                                                            <img src="images/img-user.jpg">
                                                        </div>
                                                        <div class="popover-tooltip">
                                                            <div>Goal 30'</div>
                                                            บาโลเตลี่
                                                        </div>
                                                        <div><?php echo str_replace($pnumber, "", $lineup[4]); ?></div>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                        </td>
                                        <td>
                                            <?php foreach ($lineups["lineup"] as $lineup) { ?>
                                                <?php if ($lineup[8] == "away" && $lineup[6] == 0 && $lineup[7] == 3) { ?>
                                                    <?php
                                                    $expplayer = explode(" ", $lineup[4]);
                                                    $pnumber = 0;

                                                    if (array_key_exists(0, $expplayer)) {
                                                        if (is_numeric($expplayer[0])) {
                                                            $pnumber = $expplayer[0];
                                                        }
                                                    }
                                                    ?>
                                                    <div class="wrap-box-player">
                                                        <div class="bx-player">
                                                            <div
                                                                class="label-number"><?php echo $pnumber; ?></div>
                                                            <img src="images/img-user.jpg">
                                                        </div>
                                                        <div class="popover-tooltip">
                                                            <div>Goal 30'</div>
                                                            บาโลเตลี่
                                                        </div>
                                                        <div><?php echo str_replace($pnumber, "", $lineup[4]); ?></div>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                            <div class="wrap-box-player hide">
                                                <div class="bx-player">
                                                    <div class="label-number">12</div>
                                                    <img src="images/img-user.jpg">
                                                </div>
                                                <div>โอลซาน ซาอาน</div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <table class="table-detail-lineup">
                            <tr>
                                <td>
                                    <div><strong>รายชื่อตัวสำรอง</strong></div>
                                    <span>
                                        <?php $homeplayer = ""; ?>
                                        <?php foreach ($lineups["lineup"] as $lineup) { ?>
                                            <?php if ($lineup[8] == "home" && $lineup[7] != 3) { ?>
                                                <?php $homeplayer .= empty($homeplayer) ? $lineup[4] : ", " . $lineup[4]; ?>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php echo $homeplayer; ?>
                                    </span>
                                </td>
                                <td>
                                    <div><strong>รายชื่อตัวสำรอง</strong></div>
                                    <span>
                                        <?php $homeplayer = ""; ?>
                                        <?php foreach ($lineups["lineup"] as $lineup) { ?>
                                            <?php if ($lineup[8] == "away" && $lineup[7] != 3) { ?>
                                                <?php $homeplayer .= empty($homeplayer) ? $lineup[4] : ", " . $lineup[4]; ?>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php echo $homeplayer; ?>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div style="clear:both;"></div>
            <?php if (count($livestats['livestats']) > 1) { ?>
                <div id="stat-section" class="rows mg-bt15">
                    <div class="tab-head-title text-center mg-bt15">
                        <strong>Match Stats</strong>
                    </div>
                    <div class="table-match-stat">
                        <table>
                            <thead>
                            <tr>
                                <th><strong><?php echo $gamedata['match'][13]; ?> </strong> <img
                                        onerror="imgError(this)"
                                        src="<?php echo $logoman->getLogo($gamedata['match'][11], 0); ?>">
                                </th>
                                <th></th>
                                <th><img onerror="imgError(this)"
                                         src="<?php echo $logoman->getLogo($gamedata['match'][12], 0) ?>">
                                    <strong><?php echo $gamedata['match'][14]; ?></strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $statskey = $livestats['keylist']['livestats']; ?>
                            <?php foreach ($livestats['livestats'] as $stats) { ?>
                                <?php $rs = $datamap->mapByKey($statskey, $stats); ?>
                                <?php if (array_key_exists($rs['type'], $stats_type)) { ?>
                                    <?php
                                    $values = explode(",", $rs['value']);
                                    $hval = 0;
                                    $gval = 0;
                                    if (array_key_exists(0, $values)) {
                                        $hval = intval($values[0]);
                                    }

                                    if (array_key_exists(1, $values)) {
                                        $gval = intval($values[1]);
                                    }
                                    $hpercent = $hval / ($hval + $gval) * 100;
                                    $gpercent = $gval / ($hval + $gval) * 100;
                                    ?>
                                    <tr>
                                        <td>
                                            <table class="table-progress">
                                                <tr>
                                                    <td>
                                                        <div class="progress progress-left">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="45" aria-valuemin="0"
                                                                 aria-valuemax="100"
                                                                 style="width: <?php echo $hpercent; ?>%">
                                                                <span class="sr-only">45% Complete</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="width: 6%; text-align: right;"><span
                                                            class="<?php echo ($hval > $gval) ? 'txt-red' : ''; ?>"><?php echo $hval; ?></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td><?php echo $stats_type[$rs['type']]; ?></td>
                                        <td>
                                            <table class="table-progress">
                                                <tr>
                                                    <td style="width: 6%; text-align: left;"><span
                                                            class="<?php echo ($gval > $hval) ? 'txt-red' : ''; ?>"><?php echo $gval; ?></span>
                                                    </td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="45" aria-valuemin="0"
                                                                 aria-valuemax="100"
                                                                 style="width: <?php echo $gpercent; ?>%">
                                                                <span class="sr-only">45% Complete</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php } ?>
            <div style="clear:both;"></div>
            <div style="border:solid 0px red;">
                <div class="wrap mg-bt15">
                    <div class="tab-head text-center">
                        <ul>
                            <li onclick="changeTeam(this)" class="h2h-select active" tab="team1" tid="<?=$matchView->team_home;?>">
                                <img src="<?=$matchView->teamHomePath;?>"
                                    style="width: 25px;"> <?=$matchView->teamHomeEn;?></li>
                            <li onclick="changeTeam(this)" class="h2h-select " tab="team2" tid="<?=$matchView->team_away;?>">
                                <img src="<?=$matchView->teamAwayPath;?>"
                                    style="width: 25px;"> <?=$matchView->teamAwayEn;?></li>
                        </ul>
                    </div>
                    <div class="h2h-tabs-team1 h2h-tabs " tab="team1">
                        <div id="h2h-section" class="tab-head-title text-center mg-bt15">
                            <strong>HEAD-TO-HEAD</strong>
                        </div>
                        <div class="tab-filter">
                            <div class="pull-left h-league">
                                <img src="<?=$matchView->teamHomePath?>"> <?=$matchView->teamHomeEn;?>
                            </div>
                            <div class="pull-right">
                                <div class="dropdown" style="float:left; margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                    <a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        Last Match (<label id="h2h-h-max-match"><?php echo ((count($dataHeadToHead)>=10)?'10':count($dataHeadToHead))?></label>)
                                        <span class="caret"></span>
                                    </a>
                                    <ul id="h2h-h-select-limit" class="dropdown-menu" aria-labelledby="dLabel">
                                        <?php
                                        if(count($dataHeadToHead)>10){
                                        for ($i=1;$i<=floor(count($dataHeadToHead)/10);$i++){ ?>
                                        <li><a onclick="viewHeadToHeadHome(<?=(($i)*10)?>)">Last Match(<?=(($i)*10)?>)</a></li>
                                        <?php }} ?>
                                        <li><a onclick="viewHeadToHeadHome(<?=count($dataHeadToHead)?>)">Last Match(<?=count($dataHeadToHead)?>)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="bx-h2h">
                            <div class="col-sm-6">
                                <div class="pull-right text-right">
                                    <div id="h2hdonut-h-oresult" style="height: 180px;"></div>
                                </div>
                                <div class="pull-right">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-winrate" ><span id="oddPWin">0</span>%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-loserate"><span id="oddPLose">0</span>%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-drawrate"><span id="oddPDraw">0</span>%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <h3>Odds</h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="pull-left text-left">
                                    <div id="h2hdonut-h-result" style="height: 180px;"></div>
                                </div>
                                <div class="pull-left">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-result-winrate"><span id="resultPWin">0</span>%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-result-loserate"><span id="resultPLose">0</span>%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                                <td>
                                                    <div class="bx-percent h2hh-result-drawrate"><span id="resultPDraw">0</span>%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <h3>Result</h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="mg-bt15">
                            <table class="table-h2h table-striped">
                                <thead>
                                <tr>
                                    <th>League</th>
                                    <th>Date</th>
                                    <th colspan="3">
                                        <div class="tab-filter text-center">
                                            <div class="dropdown"
                                                 style="display:inline-block;margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                                <a data-target="#" href="http://example.com" data-toggle="dropdown"
                                                   role="button" aria-haspopup="true" aria-expanded="true">
                                                    <span class="h2h-currentside-view"><span id="status-view-h2h">Total</span> Match</span>
                                                    <span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li><a class="h2h-viewside" onclick="viewMatchHome('Total')" type="GameHistory">Total</a></li>
                                                    <li><a class="h2h-viewside" onclick="viewMatchHome('Home')" type="GameHistory">Home</a></li>
                                                    <li><a class="h2h-viewside" onclick="viewMatchHome('Away')" type="GameHistory">Away</a></li>
                                                </ul>
                                            </div>
                                            <div style="clear:both;"></div>
                                        </div>
                                    </th>
                                    <th>HDP</th>
                                    <th>Odds</th>
                                    <th>Result</th>
                                </tr>
                                </thead>
                                <tbody id="teamhome-h2h-list">
                                <?php foreach ($dataHeadToHead as $key=>$value){ ?>
                                    <tr id="guide-row-original" class="guide-row tr-h2h tr-h2h-<?=(($matchView->teamHomeEn==$value->teamHomeEn)?'home':'away')?> tr-h2h-<?=floor(($key)/10)?> <?=((floor(($key)/10)==0)?'':'hide')?>">
                                        <td class="guide-league">
                                            <img style="width:25px;" src="<?=$value->leaguePath?>">
                                            <?=$value->leagueName?>
                                        </td>
                                        <td class="guide-date"><?=date("d F Y",strtotime($value->time_match))?></td>
                                        <td class="text-right"><span class="guide-home-name"><?=$value->teamHomeEn?></span>
                                            <img class="guide-home-img" onerror="imgError(this)" src="<?=$value->teamHomePath?>">
                                        </td>
                                        <td class="text-center">
                                            <div class="bx-label-hdp guide-score bg-hdp-blue">
                                                <?=(($value->homeScore==null)?0:$value->homeScore)?> - <?=(($value->awayScore==null)?0:$value->awayScore)?>
                                            </div>
                                        </td>
                                        <td class="text-left">
                                            <img class="guide-away-img" onerror="imgError(this)" src="<?=$value->teamAwayPath?>">
                                            <span class="guide-away-name"><?=$value->teamAwayEn?></span></td>
                                        <td class="guide-odds"><?=(($value->hdp==null)?'-':$value->hdp)?></td>
                                        <td class="tr-odd">
                                            <?php if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and ((($matchView->teamAwayEn==$value->teamHomeEn and ($value->homeScore+$value->hdp)<$value->awayScore)) or ($matchView->teamHomeEn==$value->teamHomeEn and ($value->homeScore+$value->hdp)>$value->awayScore))){ ?>
                                                <img class="guide-result img-win" src="/images/icon-stat/result/0.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and ((($matchView->teamAwayEn==$value->teamHomeEn and ($value->homeScore+$value->hdp)>$value->awayScore)) or ($matchView->teamHomeEn==$value->teamHomeEn and ($value->homeScore+$value->hdp)<$value->awayScore))){?>
                                                <img class="guide-result img-lose" src="/images/icon-stat/result/2.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($value->homeScore+$value->hdp)==$value->awayScore)){?>
                                                <img class="guide-result img-draw" src="/images/icon-stat/result/1.png">
                                            <?php }else{ ?>
                                                -
                                            <?php } ?>
                                        </td>
                                        <td class="tr-result">
                                            <?php if(($value->homeScore!==null and $value->awayScore!==null) and (($matchView->teamAwayEn==$value->teamHomeEn and $value->awayScore>$value->homeScore) or ($matchView->teamHomeEn==$value->teamHomeEn and $value->homeScore>$value->awayScore))){ ?>
                                                <img class="guide-result img-win" src="/images/icon-stat/result/0.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null) and (($matchView->teamAwayEn==$value->teamHomeEn and $value->awayScore<$value->homeScore) or ($matchView->teamHomeEn==$value->teamHomeEn and $value->homeScore<$value->awayScore))){?>
                                                <img class="guide-result img-lose" src="/images/icon-stat/result/2.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null) and $value->homeScore==$value->awayScore){?>
                                                <img class="guide-result img-draw" src="/images/icon-stat/result/1.png">
                                            <?php }else{ ?>
                                                -
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="h2h-tabs-team2 h2h-tabs hide" tab="team2">
                        <div class="tab-head-title text-center mg-bt15">
                            <strong>HEAD-TO-HEAD</strong>
                        </div>
                        <div class="tab-filter">
                            <div class="pull-left h-league">
                                <img src="<?=$matchView->teamAwayPath?>"> <?=$matchView->teamAwayEn;?>
                            </div>
                            <div class="pull-right">
                                <div class="dropdown"
                                     style="float:left; margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                    <a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        Last Match <label id="h2h-g-max-match">(<label id="h2h-h-max-match"><?php echo ((count($dataHeadToHead)>=10)?'10':count($dataHeadToHead))?></label>)</label>
                                        <span class="caret"></span>
                                    </a>
                                    <ul id="h2h-g-select-limit" class="dropdown-menu" aria-labelledby="dLabel">
                                        <?php
                                        if(count($dataHeadToHead)>10){
                                            for ($i=1;$i<=floor(count($dataHeadToHead)/10);$i++){ ?>
                                                <li><a onclick="viewHeadToHeadAway(<?=(($i)*10)?>)">Last Match(<?=(($i)*10)?>)</a></li>
                                            <?php }} ?>
                                        <li><a onclick="viewHeadToHeadAway(<?=count($dataHeadToHead)?>)">Last Match(<?=count($dataHeadToHead)?>)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="bx-h2h mg-bt15">
                            <div class="col-sm-6">
                                <div class="pull-right text-right">
                                    <div id="h2hdonut-a-oresult"></div>
                                </div>
                                <div class="pull-right">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-winrate" ><span id="oddPWin">0</span>%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-loserate"><span id="oddPLose">0</span>%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-drawrate"><span id="oddPDraw">0</span>%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <h3>Odds</h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="pull-left">
                                    <div id="h2hdonut-a-result"></div>
                                </div>
                                <div class="pull-left">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-result-winrate"><span id="resultPWin">0</span>%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-result-loserate"><span id="resultPLose">0</span>%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                                <td>
                                                    <div class="bx-percent h2hh-result-drawrate"><span id="resultPDraw">0</span>%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <h3>Result</h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="mg-bt15">
                            <table class="table-h2h table-striped">
                                <thead>
                                <tr>
                                    <th>League</th>
                                    <th>Date</th>
                                    <th colspan="3">
                                        <div class="tab-filter text-center">
                                            <div class="dropdown"
                                                 style="display:inline-block;margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                                <a data-target="#" href="http://example.com" data-toggle="dropdown"
                                                   role="button" aria-haspopup="true" aria-expanded="fasle">
                                                    <span class="h2h-currentside-view"><span id="status-view-h2h">Total</span> Match</span>
                                                    <span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li><a class="h2h-viewside" onclick="viewMatchAway('Total')" side="all" type="GameHistory">Total</a></li>
                                                    <li><a class="h2h-viewside" onclick="viewMatchAway('Home')" side="home" type="GameHistory">Home</a></li>
                                                    <li><a class="h2h-viewside" onclick="viewMatchAway('Away')" side="away" type="GameHistory">Away</a></li>
                                                </ul>
                                            </div>
                                            <div style="clear:both;"></div>
                                        </div>
                                    </th>
                                    <th>HDP</th>
                                    <th>Odds</th>
                                    <th>Result</th>
                                </tr>
                                </thead>
                                <tbody id="teamaway-h2h-list">
                                <?php foreach ($dataHeadToHead as $key=>$value){ ?>
                                    <tr id="guide-row-original" class="guide-row tr-h2h tr-h2h-<?=(($matchView->teamAwayEn==$value->teamAwayEn)?'home':'away')?> tr-h2h-<?=floor(($key)/10)?> <?=((floor(($key)/10)==0)?'':'hide')?>">
                                        <td class="guide-league">
                                            <img style="width:25px;" src="<?=$value->leaguePath?>">
                                            <?=$value->leagueName?>
                                        </td>
                                        <td class="guide-date"><?=date("d F Y",strtotime($value->time_match))?></td>
                                        <td class="text-right"><span class="guide-home-name"><?=$value->teamAwayEn?></span>
                                            <img class="guide-home-img" onerror="imgError(this)" src="<?=$value->teamAwayPath?>">
                                        </td>
                                        <td class="text-center">
                                            <div class="bx-label-hdp guide-score bg-hdp-blue">
                                                <?=(($value->awayScore==null)?0:$value->awayScore)?> - <?=(($value->homeScore==null)?0:$value->homeScore)?>
                                            </div>
                                        </td>
                                        <td class="text-left">
                                            <img class="guide-away-img" onerror="imgError(this)" src="<?=$value->teamHomePath?>">
                                            <span class="guide-away-name"><?=$value->teamHomeEn?></span></td>
                                        <td class="guide-odds"><?=(($value->hdp==null)?'-':($value->hdp*-1))?></td>
                                        <td class="tr-odd">
                                            <?php if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($matchView->teamAwayEn==$value->teamHomeEn and ($value->homeScore+($value->hdp))>$value->awayScore) or ($matchView->teamAwayEn==$value->teamAwayEn and ($value->homeScore+($value->hdp))<$value->awayScore))){ ?>
                                                <img class="guide-result img-win" src="/images/icon-stat/result/0.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($matchView->teamAwayEn==$value->teamHomeEn and ($value->homeScore+($value->hdp))<$value->awayScore) or ($matchView->teamAwayEn==$value->teamAwayEn and ($value->homeScore+($value->hdp))>$value->awayScore))){?>
                                                <img class="guide-result img-lose" src="/images/icon-stat/result/2.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($value->homeScore+($value->hdp))==$value->awayScore)){?>
                                                <img class="guide-result img-draw" src="/images/icon-stat/result/1.png">
                                            <?php }else{ ?>
                                                -
                                            <?php } ?>
                                        </td>
                                        <td class="tr-result">
                                            <?php if(($value->homeScore!==null and $value->awayScore!==null) and (($matchView->teamAwayEn==$value->teamHomeEn and $value->homeScore>$value->awayScore) or ($matchView->teamAwayEn==$value->teamAwayEn and $value->awayScore>$value->homeScore))){ ?>
                                                <img class="guide-result img-win" src="/images/icon-stat/result/0.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null) and (($matchView->teamAwayEn==$value->teamHomeEn and $value->homeScore<$value->awayScore) or ($matchView->teamAwayEn==$value->teamAwayEn and $value->awayScore<$value->homeScore))){?>
                                                <img class="guide-result img-lose" src="/images/icon-stat/result/2.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null) and $value->awayScore==$value->homeScore){?>
                                                <img class="guide-result img-draw" src="/images/icon-stat/result/1.png">
                                            <?php }else{ ?>
                                                -
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>
            <div class="wrap mg-bt15">
                <div class="tab-head-title text-center mg-bt15">
                    <strong>Table League</strong>
                </div>
                <div class="tab-filter">
                    <div class="pull-left h-league">
                        <img onerror="flagNotFound(this)" src="<?php echo $matchView->path;?>"> <?php echo $matchView->name;?>
                    </div>
                    <div class="pull-right">
                        <div class="times-content">Filter by League Or Match</div>
                        <div class="dropdown" style="float:left; margin-right: 10px; border-bottom: solid 1px #0091c8;">
                            <a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown"
                               role="button" aria-haspopup="true" aria-expanded="false">
                                Filter
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                <li><a class="rank-select-view" view="all">League</a></li>
                                <li><a class="rank-select-view" view="match">Match</a></li>
                            </ul>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
                <table class="table-stat table-striped" style="margin-top: 5px;">
                    <thead>
                    <tr>
                        <th>Position</th>
                        <th>Club</th>
                        <th>Played</th>
                        <th>Win</th>
                        <th>Draw</th>
                        <th>Lost</th>
                        <th>GF</th>
                        <th>GA</th>
                        <th>GD</th>
                        <th>Points</th>
                    </tr>
                    </thead>
                    <tbody id="rank-list">
                    <?php if(!empty($tableLeague)){
                        foreach ($tableLeague as $key=>$value){ ?>
                    <tr id="rank-original-row" style="<?=((!empty($value["name_en"]) and ($value["name_en"]==$matchView->teamHomeEn || $value["name_en"]==$matchView->teamAwayEn))?'color: #ff0000':'')?> ">
                        <td class="rank-no" style="color: #000000 !important;"><?php echo $key + 1; ?></td>
                        <td>
                            <img class="rank-logo" onerror="imgError(this)" src="<?= ((!empty($value["path"])) ? "" . $value["path"] : '/images/teams_clean/team_default_32x32.png'); ?>">
                            <label class="rank-team"><?php echo $value["name_en"]; ?></label>
                        </td>
                        <td class="rank-play"><?php echo $value["PI"]; ?></td>
                        <td class="rank-win"><?php echo $value["win"]; ?></td>
                        <td class="rank-draw"><?php echo $value["draw"]; ?></td>
                        <td class="rank-lose"><?php echo $value["lose"]; ?></td>
                        <td class="rank-gf"><?php echo $value["GF"]; ?></td>
                        <td class="rank-ga"><?php echo $value["GA"]; ?></td>
                        <td class="rank-gd"><?php echo $value["GD"]; ?></td>
                        <td class="rank-pts"><?php echo $value["totalPts"]; ?></td>
                    </tr>
                    <?php }} ?>
                    </tbody>
                </table>
            </div>
            <div style="border:solid 0px red;">
                <div class="wrap mg-bt15">
                    <div class="tab-head text-center">
                        <ul>
                            <li onclick="formguideChangeTeam(this)" class="guide-select active" tab="team1" tid="<?=$matchView->team_home;?>">
                                <img src="<?=$matchView->teamHomePath;?>"
                                     style="width: 25px;"> <?=$matchView->teamHomeEn;?></li>
                            <li onclick="formguideChangeTeam(this)" class="guide-select " tab="team2" tid="<?=$matchView->team_away;?>">
                                <img src="<?=$matchView->teamAwayPath;?>"
                                     style="width: 25px;"> <?=$matchView->teamAwayEn;?></li>
                        </ul>
                    </div>
                    <div class="guide-tabs-team1 guide-tabs " tab="team1">
                        <div class="tab-head-title text-center mg-bt15">
                            <strong>Form Guide</strong>
                        </div>
                        <div class="tab-filter">
                            <div class="pull-left h-league">
                                <img src="<?=$matchView->teamHomePath?>"> <?=$matchView->teamHomeEn;?>
                            </div>
                            <div class="pull-right">
                                <div class="dropdown"
                                     style="float:left; margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                    <a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        Last Match <label id="gt-h-max-match">(<label id="formguide-h-max-match"><?php echo ((count($dataFormGuideHome)>=10)?'10':count($dataFormGuideHome))?></label>)</label>
                                        <span class="caret"></span>
                                    </a>
                                    <ul id="tg-h-select-limit" class="dropdown-menu" aria-labelledby="dLabel">
                                        <?php
                                        if(count($dataFormGuideHome)>10){
                                            for ($i=1;$i<=floor(count($dataFormGuideHome)/10);$i++){ ?>
                                                <li><a onclick="viewFormGuideHome(<?=(($i)*10)?>)">Last Match(<?=(($i)*10)?>)</a></li>
                                        <?php }} ?>
                                    </ul>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="bx-h2h">
                            <div class="col-sm-6">
                                <div class="pull-right text-right">
                                    <div id="formguidedonut-gh-oresult" style="height: 180px;"></div>
                                </div>
                                <div class="pull-right">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-winrate" ><span id="oddPWin">0</span>%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-loserate"><span id="oddPLose">0</span>%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-drawrate"><span id="oddPDraw">0</span>%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <h3>Odds</h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="pull-left text-left">
                                    <div id="formguidedonut-gh-result" style="height: 180px;"></div>
                                </div>
                                <div class="pull-left">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-result-winrate"><span id="resultPWin">0</span>%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-result-loserate"><span id="resultPLose">0</span>%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                                <td>
                                                    <div class="bx-percent h2hh-result-drawrate"><span id="resultPDraw">0</span>%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <h3>Result</h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="mg-bt15">
                            <table class="table-h2h table-striped">
                                <thead>
                                <tr>
                                    <th>League</th>
                                    <th>Date</th>
                                    <th colspan="3">
                                        <div class="tab-filter text-center">
                                            <div class="dropdown"
                                                 style="display:inline-block;margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                                <a data-target="#" href="http://example.com" data-toggle="dropdown"
                                                   role="button" aria-haspopup="true" aria-expanded="true">
                                                    <span class="tg-currentside-view"><span id="status-view-formguide">Total</span> Match</span>
                                                    <span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li><a class="tg-viewside" onclick="viewMatchFormguideHome('Total')" side="all" type="GameHistory">Total Math</a></li>
                                                    <li><a class="tg-viewside" onclick="viewMatchFormguideHome('Home')" side="home" type="GameHistory">Home</a></li>
                                                    <li><a class="tg-viewside" onclick="viewMatchFormguideHome('Away')" side="away" type="GameHistory">Away</a></li>
                                                </ul>
                                            </div>
                                            <div style="clear:both;"></div>
                                        </div>
                                    </th>
                                    <th>HDP</th>
                                    <th>Odds</th>
                                    <th>Result</th>
                                </tr>
                                </thead>
                                <tbody id="teamhome-guide-list">
                                <?php foreach ($dataFormGuideHome as $key=>$value){ ?>
                                    <tr id="guide-row-original" class="guide-row tr-formguide tr-formguide-<?=(($matchView->teamHomeEn==$value->teamHomeEn)?'home':'away')?> tr-formguide-<?=floor(($key)/10)?> <?=((floor(($key)/10)==0)?'':'hide')?>">
                                        <td class="guide-league">
                                            <img style="width:25px;" src="<?=$value->leaguePath?>">
                                            <?=$value->leagueName?>
                                        </td>
                                        <td class="guide-date"><?=date("d F Y",strtotime($value->time_match))?></td>
                                        <td class="text-right"><span class="guide-home-name"><?=$value->teamHomeEn?></span>
                                            <img class="guide-home-img" onerror="imgError(this)" src="<?=$value->teamHomePath?>">
                                        </td>
                                        <td class="text-center">
                                            <div class="bx-label-hdp guide-score bg-hdp-blue">
                                                <?=(($value->homeScore==null)?0:$value->homeScore)?> - <?=(($value->awayScore==null)?0:$value->awayScore)?>
                                            </div>
                                        </td>
                                        <td class="text-left">
                                            <img class="guide-away-img" onerror="imgError(this)" src="<?=$value->teamAwayPath?>">
                                            <span class="guide-away-name"><?=$value->teamAwayEn?></span></td>
                                        <td class="guide-odds"><?=(($value->hdp==null)?'-':($value->hdp))?></td>
                                        <td class="tr-odd">
                                            <?php if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($matchView->teamHomeEn==$value->teamAwayEn and ($value->homeScore+($value->hdp))<$value->awayScore) or ($matchView->teamHomeEn==$value->teamHomeEn and ($value->homeScore+($value->hdp))>$value->awayScore))){ ?>
                                                <img class="guide-result img-win" src="/images/icon-stat/result/0.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($matchView->teamHomeEn==$value->teamAwayEn and ($value->homeScore+($value->hdp))>$value->homeScore) or ($matchView->teamHomeEn==$value->teamHomeEn and ($value->homeScore+($value->hdp))<$value->homeScore))){?>
                                                <img class="guide-result img-lose" src="/images/icon-stat/result/2.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($value->awayScore+($value->hdp))==$value->homeScore)){?>
                                                <img class="guide-result img-draw" src="/images/icon-stat/result/1.png">
                                            <?php }else{ ?>
                                                -
                                            <?php } ?>
                                        </td>
                                        <td class="tr-result">
                                            <?php if(($value->homeScore!==null and $value->awayScore!==null) and (($matchView->teamHomeEn==$value->teamAwayEn and $value->homeScore<$value->awayScore) or ($matchView->teamHomeEn==$value->teamHomeEn and $value->homeScore>$value->awayScore))){ ?>
                                                <img class="guide-result img-win" src="/images/icon-stat/result/0.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null) and (($matchView->teamHomeEn==$value->teamAwayEn and $value->homeScore>$value->awayScore) or ($matchView->teamHomeEn==$value->teamHomeEn and $value->homeScore<$value->awayScore))){?>
                                                <img class="guide-result img-lose" src="/images/icon-stat/result/2.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null) and $value->awayScore==$value->homeScore){?>
                                                <img class="guide-result img-draw" src="/images/icon-stat/result/1.png">
                                            <?php }else{ ?>
                                                -
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="guide-tabs-team2 guide-tabs hide" tab="team2">
                        <div class="tab-head-title text-center mg-bt15">
                            <strong>Form Guide</strong>
                        </div>
                        <div class="tab-filter">
                            <div class="pull-left h-league">
                                <img src="<?=$matchView->teamAwayPath?>"> <?=$matchView->teamAwayEn;?>
                            </div>
                            <div class="pull-right">
                                <div class="dropdown"
                                     style="float:left; margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                    <a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        All Match <label id="gt-g-max-match">(<label id="formguide-h-max-match"><?php echo ((count($dataFormGuideAway)>=10)?'10':count($dataFormGuideAway))?></label>)</label>
                                        <span class="caret"></span>
                                    </a>
                                    <ul id="tg-g-select-limit" class="dropdown-menu" aria-labelledby="dLabel">
                                        <?php
                                        if(count($dataFormGuideHome)>10){
                                            for ($i=1;$i<=floor(count($dataFormGuideHome)/10);$i++){ ?>
                                                <li><a onclick="viewFormGuideAway(<?=(($i)*10)?>)">Last Match(<?=(($i)*10)?>)</a></li>
                                            <?php }} ?>
                                    </ul>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="bx-h2h mg-bt15">
                            <div class="col-sm-6">
                                <div class="pull-right text-right">
                                    <div id="formguidedonut-ga-oresult"></div>
                                </div>
                                <div class="pull-right">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-winrate" ><span id="oddPWin">0</span>%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-loserate"><span id="oddPLose">0</span>%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                                <td>
                                                    <div class="bx-percent h2hh-oresult-drawrate"><span id="oddPDraw">0</span>%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <h3>Odds</h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="pull-left">
                                    <div id="formguidedonut-ga-result"></div>
                                </div>
                                <div class="pull-left">
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="bx-percent h2hh-result-winrate"><span id="resultPWin">0</span>%</div>
                                                    <span style="color: #0091c8;"><i class="fa fa-circle"></i></span>
                                                    <label>Win</label></td>
                                                <td>
                                                    <div class="bx-percent h2hh-result-loserate"><span id="resultPLose">0</span>%</div>
                                                    <span style="color: #e40520;"><i class="fa fa-circle"></i></span>
                                                    <label>Lose</label>
                                                </td>
                                                <td>
                                                    <div class="bx-percent h2hh-result-drawrate"><span id="resultPDraw">0</span>%</div>
                                                    <span style="color: #888;"><i class="fa fa-circle"></i></span>
                                                    <label>Draw</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <h3>Result</h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="mg-bt15">
                            <table class="table-h2h table-striped">
                                <thead>
                                <tr>
                                    <th>League</th>
                                    <th>Date</th>
                                    <th colspan="3">
                                        <div class="tab-filter text-center">
                                            <div class="dropdown"
                                                 style="display:inline-block;margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                                <a data-target="#" href="http://example.com" data-toggle="dropdown"
                                                   role="button" aria-haspopup="true" aria-expanded="true">
                                                    <span class="tg-currentside-view"><span id="status-view-formguide">Total</span> Match</span>
                                                    <span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li><a class="tg-viewside" onclick="viewMatchFormguideAway('Total')" side="all" type="GameHistory">Total Math</a></li>
                                                    <li><a class="tg-viewside" onclick="viewMatchFormguideAway('Home')" side="home" type="GameHistory">Home</a></li>
                                                    <li><a class="tg-viewside" onclick="viewMatchFormguideAway('Away')" side="away" type="GameHistory">Away</a></li>
                                                </ul>
                                            </div>
                                            <div style="clear:both;"></div>
                                        </div>
                                    </th>
                                    <th>HDP</th>
                                    <th>Odds</th>
                                    <th>Result</th>
                                </tr>
                                </thead>
                                <tbody id="teamaway-guide-list">
                                <?php foreach ($dataFormGuideAway as $key=>$value){ ?>
                                    <tr id="guide-row-original" class="guide-row tr-formguide tr-formguide-<?=(($matchView->teamAwayEn==$value->teamAwayEn)?'home':'away')?> tr-formguide-<?=floor(($key)/10)?> <?=((floor(($key)/10)==0)?'':'hide')?>">
                                        <td class="guide-league">
                                            <img style="width:25px;" src="<?=$value->leaguePath?>">
                                            <?=$value->leagueName?>
                                        </td>
                                        <td class="guide-date"><?=date("d F Y",strtotime($value->time_match))?></td>
                                        <td class="text-right"><span class="guide-home-name"><?=$value->teamAwayEn?></span>
                                            <img class="guide-home-img" onerror="imgError(this)" src="<?=$value->teamAwayPath?>">
                                        </td>
                                        <td class="text-center">
                                            <div class="bx-label-hdp guide-score bg-hdp-blue">
                                                <?=(($value->awayScore==null)?0:$value->awayScore)?> - <?=(($value->homeScore==null)?0:$value->homeScore)?>
                                            </div>
                                        </td>
                                        <td class="text-left">
                                            <img class="guide-away-img" onerror="imgError(this)" src="<?=$value->teamHomePath?>">
                                            <span class="guide-away-name"><?=$value->teamHomeEn?></span></td>
                                        <td class="guide-odds"><?=(($value->hdp==null)?'-':($value->hdp*-1))?></td>
                                        <td class="tr-odd">
                                            <?php if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($matchView->teamAwayEn==$value->teamAwayEn and ($value->homeScore+($value->hdp))<$value->awayScore) or ($matchView->teamAwayEn==$value->teamHomeEn and ($value->homeScore+($value->hdp))>$value->awayScore))){ ?>
                                                <img class="guide-result img-win" src="/images/icon-stat/result/0.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and(($matchView->teamAwayEn==$value->teamAwayEn and ($value->homeScore+($value->hdp))>$value->awayScore) or ($matchView->teamAwayEn==$value->teamHomeEn and($value->homeScore+($value->hdp))<$value->awayScore))){?>
                                                <img class="guide-result img-lose" src="/images/icon-stat/result/2.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null and $value->hdp!=null) and (($value->homeScore+($value->hdp))==$value->awayScore)){?>
                                                <img class="guide-result img-draw" src="/images/icon-stat/result/1.png">
                                            <?php }else{ ?>
                                                -
                                            <?php } ?>
                                        </td>
                                        <td class="tr-result">
                                            <?php if(($value->homeScore!==null and $value->awayScore!==null) and (($matchView->teamAwayEn==$value->teamAwayEn and $value->homeScore<$value->awayScore) or ($matchView->teamAwayEn==$value->teamHomeEn and $value->homeScore>$value->awayScore))){ ?>
                                                <img class="guide-result img-win" src="/images/icon-stat/result/0.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null) and (($matchView->teamAwayEn==$value->teamAwayEn and $value->homeScore>$value->awayScore) or ($matchView->teamAwayEn==$value->teamHomeEn and $value->homeScore<$value->awayScore))){?>
                                                <img class="guide-result img-lose" src="/images/icon-stat/result/2.png">
                                            <?php }else if(($value->homeScore!==null and $value->awayScore!==null) and $value->awayScore==$value->homeScore){?>
                                                <img class="guide-result img-draw" src="/images/icon-stat/result/1.png">
                                            <?php }else{ ?>
                                                -
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>
            <div class="tab-head-title text-center mg-bt15">
                <strong>Next Match</strong>
            </div>
            <div class="bx-next-match mg-bt15">
                <div class="col-sm-6">
                    <div class="text-left">
                        <img src="<?=$matchView->teamHomePath?>">
                        <span><?=$matchView->teamHomeEn;?></span>
                    </div>
                    <div style="border-right: solid 1px #ddd;">
                        <table class="table-striped">
                            <?php foreach ($dataNextMatchHome as $value) { ?>
                                <tr>
                                    <td class="">
                                        <img style="width: 25px;" src="<?=$value->teamHomePath?>">
                                        <?php echo $value->teamHomeEn; ?>
                                    </td>
                                    <td class="text-center">
                                        <div class="bx-label-hdp bg-hdp-blue"><?php echo date("H:i",strtotime($value->time_match));?></div>
                                        <div
                                            class="times-content"><?php echo $value->leagueName . " " .date("\"d/m/Y\"",strtotime($value->time_match)); ?></div>
                                    </td>
                                    <td class="text-right">
                                        <img style="width: 25px;" src="<?=$value->teamAwayPath?>">
                                        <?php echo $value->teamAwayEn; ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="text-right">
                        <img src="<?=$matchView->teamAwayPath?>">
                        <span><?=$matchView->teamAwayEn;?></span>
                    </div>
                    <div>
                        <table class="table-striped">
                            <?php foreach ($dataNextMatchAway as $value) { ?>
                                <tr>
                                    <td class="">
                                        <img style="width: 25px;" src="<?=$value->teamHomePath?>">
                                        <?php echo $value->teamHomeEn; ?>
                                    </td>
                                    <td class="text-center">
                                        <div class="bx-label-hdp bg-hdp-blue"><?php echo date("H:i",strtotime($value->time_match));?></div>
                                        <div
                                                class="times-content"><?php echo $value->leagueName . " " .date("\"d/m/Y\"",strtotime($value->time_match)); ?></div>
                                    </td>
                                    <td class="text-right">
                                        <img style="width: 25px;" src="<?=$value->teamAwayPath?>">
                                        <?php echo $value->teamAwayEn; ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
            <?php $news = $newsm->getRelatedNews($nameLeagueThai.','.$nameTeamHomeThai . ',' . $nameTeamAwayThai, 3); ?>
            <?php if(!empty($news)){ ?>
            <div id="news-section" class="tab-head-title text-center mg-bt15">
                <strong>Related News</strong>
            </div>
            <div class="rows">
                <?php foreach ($news as $n) { ?>
                    <div class="col-sm-4">
                        <div class="bx-story-small">
                            <div class="hilight-cover"></div>
                            <div class="crop"
                                 style="background-image: url(<?php echo $n->imageLink; ?>)"></div>
                            <div class="bx-story-content">
                                <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                        class="how-long"> <?php echo $n->createDatetime; ?></span>
                                </div>
                                <a href="articles.php?newsid=<?php echo $n->newsid; ?>"
                                   class="h-content"><?php echo $n->titleTh; ?></a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<script>
    google.charts.load("current", {packages: ["corechart"]});
    $(document).ready(function () {
        viewHeadToHeadAway(10);
        viewHeadToHeadHome(10);
        viewFormGuideHome(10);
        viewFormGuideAway(10)
    });

    function changeTeam(elem) {
        var id = $(elem).attr("tab");

        $('.h2h-select').removeClass('active');
        $(elem).addClass('active');

        $('.h2h-tabs').addClass('hide');
        $('.h2h-tabs-'+id).removeClass('hide');
    }
    function formguideChangeTeam(elem) {
        var id = $(elem).attr("tab");

        $('.guide-select').removeClass('active');
        $(elem).addClass('active');

        $('.guide-tabs').addClass('hide');
        $('.guide-tabs-'+id).removeClass('hide');
    }
    function viewMatchFormguideAway(status) {
        $('.guide-tabs-team2').find('#status-view-formguide').html(status);
        viewFormGuideAway($('.guide-tabs-team2').find('#formguide-h-max-match').html());
    }
    function viewFormGuideAway(total) {
//        console.log(total);
        console.log($('.guide-tabs-team2').find('#status-view-formguide').html());
        var resultWin=0,resultLose=0,resultDraw=0,resultPWin=0,resultPLose=0,resultPDraw=0,oddWin=0,oddLose=0,oddDraw=0,oddPWin=0,oddPLose=0,oddPDraw=0;
        $('.guide-tabs-team2').find('tr.tr-formguide').addClass('hide');

        for (i = 0; i < total; i++) {
//            console.log($('tr.tr-formguide').eq(i).attr('class')+" :: "+$('tr.tr-formguide').eq(i).hasClass('tr-formguide-home'));
            if($('.guide-tabs-team2').find('#status-view-formguide').html()=="Total"){
                $('.guide-tabs-team2').find('tr.tr-formguide').eq(i).removeClass('hide');
            }else if($('.guide-tabs-team2').find('#status-view-formguide').html()=="Home" && $('.guide-tabs-team2').find('tr.tr-formguide').eq(i).hasClass('tr-formguide-home')){
                console.log("IN");
                $('.guide-tabs-team2').find('tr.tr-formguide').eq(i).removeClass('hide');
            }else if($('.guide-tabs-team2').find('#status-view-formguide').html()=="Away" && $('.guide-tabs-team2').find('tr.tr-formguide').eq(i).hasClass('tr-formguide-away')){
                console.log("out");
                $('.guide-tabs-team2').find('tr.tr-formguide').eq(i).removeClass('hide');
            }
        }

        resultWin=$('.guide-tabs-team2').find('tr.tr-formguide').not(".hide").find('td.tr-result').find('img.img-win').length;
        resultLose=$('.guide-tabs-team2').find('tr.tr-formguide').not(".hide").find('td.tr-result').find('img.img-lose').length;
        resultDraw=$('.guide-tabs-team2').find('tr.tr-formguide').not(".hide").find('td.tr-result').find('img.img-draw').length;
        oddWin=$('.guide-tabs-team2').find('tr.tr-formguide').not(".hide").find('td.tr-odd').find('img.img-win').length;
        oddLose=$('.guide-tabs-team2').find('tr.tr-formguide').not(".hide").find('td.tr-odd').find('img.img-lose').length;
        oddDraw=$('.guide-tabs-team2').find('tr.tr-formguide').not(".hide").find('td.tr-odd').find('img.img-draw').length;

        resultPWin=parseInt(100-((((resultWin+resultLose+resultDraw)-resultWin)/(resultWin+resultLose+resultDraw))*100));
        resultPLose=parseInt(100-((((resultWin+resultLose+resultDraw)-resultLose)/(resultWin+resultLose+resultDraw))*100));
        resultPDraw=parseInt(100-((((resultWin+resultLose+resultDraw)-resultDraw)/(resultWin+resultLose+resultDraw))*100));

        if(oddWin!==0 || oddLose!==0 || oddDraw!==0) {
            oddPWin = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddWin) / (oddWin + oddLose + oddDraw)) * 100));
            oddPLose = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddLose) / (oddWin + oddLose + oddDraw)) * 100));
            oddPDraw = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddDraw) / (oddWin + oddLose + oddDraw)) * 100));
        }

        $('.guide-tabs-team2').find('#resultPWin').html(resultPWin);
        $('.guide-tabs-team2').find('#resultPLose').html(resultPLose);
        $('.guide-tabs-team2').find('#resultPDraw').html(resultPDraw);

        $('.guide-tabs-team2').find('#oddPWin').html(oddPWin);
        $('.guide-tabs-team2').find('#oddPLose').html(oddPLose);
        $('.guide-tabs-team2').find('#oddPDraw').html(oddPDraw);

        google.charts.setOnLoadCallback(
            function () {
                drawDonut('formguidedonut-ga-result', [['result', 'value'], ['draw', resultPDraw], ['lose', resultPLose], ['win', resultPWin]]);
                drawDonut('formguidedonut-ga-oresult', [['result', 'value'], ['draw', oddPDraw], ['lose', oddPLose], ['win', oddPWin]]);
            }
        )

        $('.guide-tabs-team2').find('#formguide-h-max-match').html(total);
    }
    function viewMatchFormguideHome(status) {
        $('.guide-tabs-team1').find('#status-view-formguide').html(status);
        viewFormGuideHome($('.guide-tabs-team1').find('#formguide-h-max-match').html());
    }
    function viewFormGuideHome(total) {
        var resultWin=0,resultLose=0,resultDraw=0,resultPWin=0,resultPLose=0,resultPDraw=0,oddWin=0,oddLose=0,oddDraw=0,oddPWin=0,oddPLose=0,oddPDraw=0;
        $('.guide-tabs-team1').find('tr.tr-formguide').addClass('hide');

        for (i = 0; i < total; i++) {
            if($('.guide-tabs-team1').find('#status-view-formguide').html()=="Total"){
                $('.guide-tabs-team1').find('tr.tr-formguide').eq(i).removeClass('hide');
            }else if($('.guide-tabs-team1').find('#status-view-formguide').html()=="Home" && $('.guide-tabs-team1').find('tr.tr-formguide').eq(i).hasClass('tr-formguide-home')){
                $('.guide-tabs-team1').find('tr.tr-formguide').eq(i).removeClass('hide');
            }else if($('.guide-tabs-team1').find('#status-view-formguide').html()=="Away" && $('.guide-tabs-team1').find('tr.tr-formguide').eq(i).hasClass('tr-formguide-away')){
                $('.guide-tabs-team1').find('tr.tr-formguide').eq(i).removeClass('hide');
            }
        }

        resultWin=$('.guide-tabs-team1').find('tr.tr-formguide').not(".hide").find('td.tr-result').find('img.img-win').length;
        resultLose=$('.guide-tabs-team1').find('tr.tr-formguide').not(".hide").find('td.tr-result').find('img.img-lose').length;
        resultDraw=$('.guide-tabs-team1').find('tr.tr-formguide').not(".hide").find('td.tr-result').find('img.img-draw').length;
        oddWin=$('.guide-tabs-team1').find('tr.tr-formguide').not(".hide").find('td.tr-odd').find('img.img-win').length;
        oddLose=$('.guide-tabs-team1').find('tr.tr-formguide').not(".hide").find('td.tr-odd').find('img.img-lose').length;
        oddDraw=$('.guide-tabs-team1').find('tr.tr-formguide').not(".hide").find('td.tr-odd').find('img.img-draw').length;

        resultPWin=parseInt(100-((((resultWin+resultLose+resultDraw)-resultWin)/(resultWin+resultLose+resultDraw))*100));
        resultPLose=parseInt(100-((((resultWin+resultLose+resultDraw)-resultLose)/(resultWin+resultLose+resultDraw))*100));
        resultPDraw=parseInt(100-((((resultWin+resultLose+resultDraw)-resultDraw)/(resultWin+resultLose+resultDraw))*100));

        if(oddWin!==0 || oddLose!==0 || oddDraw!==0) {
            oddPWin = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddWin) / (oddWin + oddLose + oddDraw)) * 100));
            oddPLose = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddLose) / (oddWin + oddLose + oddDraw)) * 100));
            oddPDraw = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddDraw) / (oddWin + oddLose + oddDraw)) * 100));
        }

        $('.guide-tabs-team1').find('#resultPWin').html(resultPWin);
        $('.guide-tabs-team1').find('#resultPLose').html(resultPLose);
        $('.guide-tabs-team1').find('#resultPDraw').html(resultPDraw);

        $('.guide-tabs-team1').find('#oddPWin').html(oddPWin);
        $('.guide-tabs-team1').find('#oddPLose').html(oddPLose);
        $('.guide-tabs-team1').find('#oddPDraw').html(oddPDraw);

        google.charts.setOnLoadCallback(
            function () {
                drawDonut('formguidedonut-gh-result', [['result', 'value'], ['draw', resultPDraw], ['lose', resultPLose], ['win', resultPWin]]);
                drawDonut('formguidedonut-gh-oresult', [['result', 'value'], ['draw', oddPDraw], ['lose', oddPLose], ['win', oddPWin]]);
            }
        )

        $('.guide-tabs-team1').find('#formguide-h-max-match').html(total);
    }
    function viewMatchAway(status) {
        $('.h2h-tabs-team2').find('#status-view-h2h').html(status);
        viewHeadToHeadAway($('.h2h-tabs-team2').find('#h2h-h-max-match').html());
    }
    function viewHeadToHeadAway(total) {
        var resultWin=0,resultLose=0,resultDraw=0,resultPWin=0,resultPLose=0,resultPDraw=0,oddWin=0,oddLose=0,oddDraw=0,oddPWin=0,oddPLose=0,oddPDraw=0;
        $('.h2h-tabs-team2').find('tr.tr-h2h').addClass('hide');

        for (i = 0; i < total; i++) {
            if($('.h2h-tabs-team2').find('#status-view-h2h').html()=="Total"){
                $('.h2h-tabs-team2').find('tr.tr-h2h').eq(i).removeClass('hide');
            }else if($('.h2h-tabs-team2').find('#status-view-h2h').html()=="Home" && $('.h2h-tabs-team2').find('tr.tr-h2h').eq(i).hasClass('tr-h2h-home')){
                $('.h2h-tabs-team2').find('tr.tr-h2h').eq(i).removeClass('hide');
            }else if($('.h2h-tabs-team2').find('#status-view-h2h').html()=="Away" && $('.h2h-tabs-team2').find('tr.tr-h2h').eq(i).hasClass('tr-h2h-away')){
                $('.h2h-tabs-team2').find('tr.tr-h2h').eq(i).removeClass('hide');
            }
        }

        resultWin=$('.h2h-tabs-team2').find('tr.tr-h2h').not(".hide").find('td.tr-result').find('img.img-win').length;
        resultLose=$('.h2h-tabs-team2').find('tr.tr-h2h').not(".hide").find('td.tr-result').find('img.img-lose').length;
        resultDraw=$('.h2h-tabs-team2').find('tr.tr-h2h').not(".hide").find('td.tr-result').find('img.img-draw').length;
        oddWin=$('.h2h-tabs-team2').find('tr.tr-h2h').not(".hide").find('td.tr-odd').find('img.img-win').length;
        oddLose=$('.h2h-tabs-team2').find('tr.tr-h2h').not(".hide").find('td.tr-odd').find('img.img-lose').length;
        oddDraw=$('.h2h-tabs-team2').find('tr.tr-h2h').not(".hide").find('td.tr-odd').find('img.img-draw').length;

        resultPWin=parseInt(100-((((resultWin+resultLose+resultDraw)-resultWin)/(resultWin+resultLose+resultDraw))*100));
        resultPLose=parseInt(100-((((resultWin+resultLose+resultDraw)-resultLose)/(resultWin+resultLose+resultDraw))*100));
        resultPDraw=parseInt(100-((((resultWin+resultLose+resultDraw)-resultDraw)/(resultWin+resultLose+resultDraw))*100));

        if(oddWin!==0 || oddLose!==0 || oddDraw!==0) {
            oddPWin = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddWin) / (oddWin + oddLose + oddDraw)) * 100));
            oddPLose = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddLose) / (oddWin + oddLose + oddDraw)) * 100));
            oddPDraw = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddDraw) / (oddWin + oddLose + oddDraw)) * 100));
        }

        $('.h2h-tabs-team2').find('#resultPWin').html(resultPWin);
        $('.h2h-tabs-team2').find('#resultPLose').html(resultPLose);
        $('.h2h-tabs-team2').find('#resultPDraw').html(resultPDraw);

        $('.h2h-tabs-team2').find('#oddPWin').html(oddPWin);
        $('.h2h-tabs-team2').find('#oddPLose').html(oddPLose);
        $('.h2h-tabs-team2').find('#oddPDraw').html(oddPDraw);

        google.charts.setOnLoadCallback(
            function () {
                drawDonut('h2hdonut-a-result', [['result', 'value'], ['draw', resultPDraw], ['lose', resultPLose], ['win', resultPWin]]);
                drawDonut('h2hdonut-a-oresult', [['result', 'value'], ['draw', oddPDraw], ['lose', oddPLose], ['win', oddPWin]]);
            }
        )

        $('.h2h-tabs-team2').find('#h2h-h-max-match').html(total);
    }
    function viewMatchHome(status) {
        $('.h2h-tabs-team1').find('#status-view-h2h').html(status);
        viewHeadToHeadHome($('.h2h-tabs-team1').find('#h2h-h-max-match').html());
    }
    function viewHeadToHeadHome(total) {
        var resultWin=0,resultLose=0,resultDraw=0,resultPWin=0,resultPLose=0,resultPDraw=0,oddWin=0,oddLose=0,oddDraw=0,oddPWin=0,oddPLose=0,oddPDraw=0;
        $('.h2h-tabs-team1').find('tr.tr-h2h').addClass('hide');

        for (i = 0; i < total; i++) {
            if($('.h2h-tabs-team1').find('#status-view-h2h').html()=="Total"){
                $('.h2h-tabs-team1').find('tr.tr-h2h').eq(i).removeClass('hide');
            }else if($('.h2h-tabs-team1').find('#status-view-h2h').html()=="Home" && $('.h2h-tabs-team1').find('tr.tr-h2h').eq(i).hasClass('tr-h2h-home')){
                $('.h2h-tabs-team1').find('tr.tr-h2h').eq(i).removeClass('hide');
            }else if($('.h2h-tabs-team1').find('#status-view-h2h').html()=="Away" && $('.h2h-tabs-team1').find('tr.tr-h2h').eq(i).hasClass('tr-h2h-away')){
                $('.h2h-tabs-team1').find('tr.tr-h2h').eq(i).removeClass('hide');
            }
        }

        resultWin=$('.h2h-tabs-team1').find('tr.tr-h2h').not(".hide").find('td.tr-result').find('img.img-win').length;
        resultLose=$('.h2h-tabs-team1').find('tr.tr-h2h').not(".hide").find('td.tr-result').find('img.img-lose').length;
        resultDraw=$('.h2h-tabs-team1').find('tr.tr-h2h').not(".hide").find('td.tr-result').find('img.img-draw').length;
        oddWin=$('.h2h-tabs-team1').find('tr.tr-h2h').not(".hide").find('td.tr-odd').find('img.img-win').length;
        oddLose=$('.h2h-tabs-team1').find('tr.tr-h2h').not(".hide").find('td.tr-odd').find('img.img-lose').length;
        oddDraw=$('.h2h-tabs-team1').find('tr.tr-h2h').not(".hide").find('td.tr-odd').find('img.img-draw').length;

        resultPWin=parseInt(100-((((resultWin+resultLose+resultDraw)-resultWin)/(resultWin+resultLose+resultDraw))*100));
        resultPLose=parseInt(100-((((resultWin+resultLose+resultDraw)-resultLose)/(resultWin+resultLose+resultDraw))*100));
        resultPDraw=parseInt(100-((((resultWin+resultLose+resultDraw)-resultDraw)/(resultWin+resultLose+resultDraw))*100));

        if(oddWin!==0 || oddLose!==0 || oddDraw!==0) {
            oddPWin = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddWin) / (oddWin + oddLose + oddDraw)) * 100));
            oddPLose = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddLose) / (oddWin + oddLose + oddDraw)) * 100));
            oddPDraw = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddDraw) / (oddWin + oddLose + oddDraw)) * 100));
        }

        $('.h2h-tabs-team1').find('#resultPWin').html(resultPWin);
        $('.h2h-tabs-team1').find('#resultPLose').html(resultPLose);
        $('.h2h-tabs-team1').find('#resultPDraw').html(resultPDraw);

        $('.h2h-tabs-team1').find('#oddPWin').html(oddPWin);
        $('.h2h-tabs-team1').find('#oddPLose').html(oddPLose);
        $('.h2h-tabs-team1').find('#oddPDraw').html(oddPDraw);

        google.charts.setOnLoadCallback(
            function () {
                drawDonut('h2hdonut-h-result', [['result', 'value'], ['draw', resultPDraw], ['lose', resultPLose], ['win', resultPWin]]);
                drawDonut('h2hdonut-h-oresult', [['result', 'value'], ['draw', oddPDraw], ['lose', oddPLose], ['win', oddPWin]]);
            }
        )

        $('.h2h-tabs-team1').find('#h2h-h-max-match').html(total);
    }
    function drawDonut(element, data) {
        console.log(data);
        var data = google.visualization.arrayToDataTable(data);
        var options = {
            pieStartAngle: 0,
            chartArea: {top: 7, height: '90%', width: '100%'},
            height: '180',
            width: '203',
            colors: ['#888', '#e40520', '#0091c8'],
            pieHole: 0.5,
            legend: 'none',
        };

        var chart = new google.visualization.PieChart(document.getElementById(element));
        chart.draw(data, options);
    }
</script>

<?php //include 'footer.php';
include 'view/index/footer.php';
?>
<!--<script>-->
<!--    $(function() {-->
<!--        $("html").easeScroll();-->
<!--    });-->
<!--</script>-->

<script src="/js/jquery.easeScroll.js"></script>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link href="/worldcup-rov/css/worldcup2018-rov.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style-new.css">
    <!--    <link rel="stylesheet" href="/css/reset.css">-->

    <meta property="og:title" content="ทายทีมเข้ารอบ World cup 2018"/>
    <meta property="og:description" content="ทายทีมเข้ารอบเพื่อลุ้นรับรางวัลง่ายๆ แจกฟรี สกิน  Superman , The Flash และสกินยอดฮิตอีกมากกว่า 20 รางวัล"/>
    <meta property="og:image" content="https://ngoal.com/images/rov.jpg"/>

    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/mobile/js/bootstrap.js"></script>

</head>
<?php
require_once __DIR__ . "/bootstrap.php";
$isMobile = (bool)preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet' .
    '|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]' .
    '|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', $_SERVER['HTTP_USER_AGENT']);

if ($isMobile) {
    header('Location: /worldcup-rov/mobile.php');
}

$mcon = new \controller\MatchController();
$matches = $mcon->getAvailable();

?>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>

<div id="content-event-rov"  class="content-event">
    <div class="content-event-side-title">
        <div class="btn-sign-FB"></div>
    </div>
    <div class="content-fifa-world-cup">


        <!--        Semi-final-->
        <div class="columns-fifa03">
            <div class="title-columns-fifa">
                <h2>Semi-final</h2>
                <small>Thursday 10 July</small>
            </div>
            <div class="content-top-fifa-8" style="margin-top: 111px;">
                <div class="box-fifa-16 box-fifa-8">
                    <div class="check-box" mid="13">

                    </div>
                    <?php if (array_key_exists(13, $matches)) { ?>
                        <div class="pull-left box-fifa-16-haft select-team team-mark"
                             mid="<?php echo $matches[13]->mid; ?>"
                             tid="<?php echo $matches[13]->aid; ?>">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="<?php echo $matches[13]->teama->flag; ?>">
                                </li>
                                <li class="name-fifa-16">
                                    <span><?php echo strtoupper(substr($matches[13]->teama->name, 0, 3)); ?></span></li>
                                </li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft select-team team-mark"
                             mid="<?php echo $matches[13]->mid; ?>"
                             tid="<?php echo $matches[13]->bid; ?>">
                            <ul>
                                <li class="name-fifa-16">
                                    <span><?php echo strtoupper(substr($matches[13]->teamb->name, 0, 3)); ?></span></li>
                                </li>
                                <li class="flag-fifa-16">
                                    <img src="<?php echo $matches[13]->teamb->flag; ?>">
                                </li>

                            </ul>
                        </div>
                    <?php } else { ?>
                        <div class="pull-left box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-default.jpg">
                                </li>
                                <li class="name-fifa-16"><span>???</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="name-fifa-16"><span>???</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-default.jpg">
                                </li>

                            </ul>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="content-top-fifa-8" style="margin-top: 113px;">
                <div class="box-fifa-16 box-fifa-8">
                    <div class="check-box" mid="14">

                    </div>
                    <?php if (array_key_exists(14, $matches)) { ?>
                        <div class="pull-left box-fifa-16-haft select-team team-mark"
                             mid="<?php echo $matches[14]->mid; ?>"
                             tid="<?php echo $matches[14]->aid; ?>">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="<?php echo $matches[14]->teama->flag; ?>">
                                </li>
                                <li class="name-fifa-16">
                                    <span><?php echo strtoupper(substr($matches[14]->teama->name, 0, 3)); ?></span></li>
                                </li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft select-team team-mark"
                             mid="<?php echo $matches[14]->mid; ?>"
                             tid="<?php echo $matches[14]->bid; ?>">
                            <ul>
                                <li class="name-fifa-16">
                                    <span><?php echo strtoupper(substr($matches[14]->teamb->name, 0, 3)); ?></span></li>
                                </li>
                                <li class="flag-fifa-16">
                                    <img src="<?php echo $matches[14]->teamb->flag; ?>">
                                </li>

                            </ul>
                        </div>
                    <?php } else { ?>
                        <div class="pull-left box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-default.jpg">
                                </li>
                                <li class="name-fifa-16"><span>???</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="name-fifa-16"><span>???</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-default.jpg">
                                </li>
                            </ul>
                        </div>
                    <?php } ?>

                </div>
            </div>
            <div class="box-award" style="margin-top: 25px;">
                <img style="width: 124px; margin: 12px 0 0 0;" src="/worldcup-rov/img/item-01.png">
                <ul>
                    <li>สกิน</li>
                    <li>13</li>
                    <li>รางวัล</li>
                </ul>
            </div>
            <div class="btn-col save-selected">
                ยืนยันการเลือกทีม
            </div>
        </div>


        <!--        Final-->
        <div class="columns-fifa04">
            <div class="title-columns-fifa">
                <h2>Final</h2>
                <small>Thursday 14 July</small>
            </div>
            <div class="content-top-fifa-8" style="margin-top: 80px;">

                <div class="final-cup"><img src="/worldcup-pubg/img/world-cup-logo-1.png"></div>

                <div class="box-fifa-16 box-fifa-final" style="margin-top: 164px;">
                    <div class="check-box" mid="15" style="margin-top: 34px;">

                    </div>
                    <?php if (array_key_exists(15, $matches)) { ?>
                        <div class="pull-left box-fifa-16-haft select-team team-mark"
                             mid="<?php echo $matches[15]->mid; ?>"
                             tid="<?php echo $matches[15]->aid; ?>">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="<?php echo $matches[15]->teama->flag; ?>">
                                </li>
                                <li class="name-fifa-16">
                                    <span><?php echo strtoupper(substr($matches[15]->teama->name, 0, 3)); ?></span></li>
                                </li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft select-team team-mark"
                             mid="<?php echo $matches[15]->mid; ?>"
                             tid="<?php echo $matches[15]->bid; ?>">
                            <ul>
                                <li class="flag-fifa-16 text-right">
                                    <img src="<?php echo $matches[15]->teamb->flag; ?>">
                                </li>
                                <li class="name-fifa-16 pull-right">
                                    <span><?php echo strtoupper(substr($matches[15]->teamb->name, 0, 3)); ?></span></li>
                                </li>
                            </ul>
                        </div>
                    <?php } else { ?>
                        <div class="pull-left box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-default.jpg">
                                </li>
                                <li class="name-fifa-16"><span>???</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16 text-right">
                                    <img src="/images/Flag-default.jpg">
                                </li>
                                <li class="name-fifa-16 pull-right"><span>???</span></li>
                            </ul>
                        </div>
                    <?php } ?>


                </div>
            </div>

            <div class="box-award" style="margin-top: 54px;">
                <img src="/worldcup-rov/img/item-02.png" style="width: 175px;margin-left: -15px;margin-top: -17px;">
                <ul>
                    <li>สกิน</li>
                    <li>13</li>
                    <li>รางวัล</li>
                </ul>
            </div>
            <div class="btn-col save-selected">
                ยืนยันการเลือกทีม
            </div>
        </div>
    </div>
</div>
<div class="container text-center" style="padding-top: 100px;">
    <a href="#content-event-rov"><img src="/worldcup-rov/img/thopy2.png"></a>
</div>
<div class="container container-list">
    <div class="col-lg-12">
        <h3>เงื่อนไขการร่วมชิงรางวัล</h3>
        <ol>
            <li>เริ่มทายผลผ่านเว็บไซต์ ได้ตั้งแต่วันที่ 10 กรกฏาคม 2561 สิ้นสุดวันที่ 15 กรกฏาคม 2561 โดย ทายผลผ่านเว็บไซต์หมดเขตส่งคำทายเวลา 23.59 น. โดยถือเวลาที่ทายผล ครั้งสุดท้ายเป็นสำคัญ
            </li>
            <li>ผู้ที่มีสิทธิ์ลุ้นรับรางวัล ต้องทายทีมเข้ารอบถูกต้องทั้งหมดในแต่ละรอบการแข่งขัน</li>
            <li>
                กำหนดสุ่มจับรางวัล และประกาศรายชื่อผู้โชคดีแบ่งเป็น 4 รอบ ทางเว็บ ngoal.com และทางเพสเฟสบุ๊ก จบสกอร์
                <ul>
                    <li>รอบที่ 1 วันที่ 12 กรกฎาคม</li>
                    <li>รอบที่ 2 วันที่ 16 กรกฎาคม</li>
                </ul>
            </li>
            <li>ผู้ได้รับรางวัลมีสิทธิรับรางวัลที่มีมูลค่าสูงสุดเพียงรางวัลเดียวเท่านั้น</li>
            <li>คำตัดสินของคณะกรรมการให้ถือเป็นที่สุด</li>
            <li>ทางเว็บไชต์ ngoal.com ขอสงวนสิทธิในการเปลี่ยนแปลงรายละเอียดหรือเงื่อนไขโดยไม่ต้องแจ้งให้ทราบล่วงหน้า
            </li>
        </ol>
    </div>

    <div class="col-lg-12">
        <h3>รางวัลสำหรับการทายผล</h3>
        <ul>
            <li>รางวัลรอบที่ 2 สกินเกม ROV ประกอบด้วย สกินฮีโร่, แพคลูกแก้ว  มูลค่า 3,500 บาท จำนวน 12 รางวัล</li>
            <li>รางวัลรอบที่ 1 สกินเกม ROV ประกอบด้วย สกินฮีโร่, แพคลูกแก้ว  มูลค่า 3,500 บาท จำนวน 12 รางวัล</li>
        </ul>
    </div>
    <div class="col-lg-12">
        <h3>วิธีการร่วมสนุกทายผลผ่านเว็บไซต์</h3>
        <ol>
            <li>เลือกทายประเทศที่จะเข้ารอบฟุตบอลโลก 2018 ของแต่ละรอบการแข่งขันผ่านเว็บไซต์ www.ngoal.com/worldcup-rov</li>
            <li>ลงทะเบียนในเว็บไซต์ โดยการ login ด้วย facebook ของจริงที่ลงทะเบียนมาแล้วไม่ต่ำกว่า 3 เดือน</li>
            <li>ผู้ร่วมกิจกรรมไม่สามารถเปลี่ยนคำตอบได้เมื่อกดยืนยันคำตอบแล้ว
                แต่สามารถตอบเพิ่มได้ในคู่ที่ยังไม่ได้เล่นจนกว่าจะหมดเขตทายผลของแต่ละรอบ
            </li>
            <li>ผู้ร่วมกิจกรรม 1 คน มีสิทธิเพียง 1 สิทธิเท่านั้น</li>
        </ol>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="reward-board" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="exampleModalLongTitle">ประกาศรายชื่อผู้โชคดี รอบ Final</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive" src="/worldcup-rov/img/rov-final.jpg">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">
                        <span style="color: red;font-size: 18px">หมายเหตุ: ผู้ที่ได้รับรางวัลต้องติดต่อขอรับรางวัลภายในวันที่ 17 ก.ค. 2561 เวลา 18.00น. หากเลยกำหนดถือว่าสละสิทธิ์</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                <button type="button" id="dismiss-reward" class="btn btn-primary">ไม่ต้องแสดงอีก</button>
            </div>
        </div>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://connect.facebook.net/en_US/sdk.js"></script>
<script type='text/javascript'>
    var hassession = <?php echo (isset($_SESSION['login'])) ? 1 : 0; ?>;
</script>
<script src="/worldcup-rov/js/wc_rov.js"></script>
</body>
</html>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bare - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <link href="momentjs-datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
    <link href="vendor/components/font-awesome/css/fontawesome-all.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <style>
        body {
            padding-top: 54px;
        }

        @media (min-width: 992px) {
            body {
                padding-top: 56px;
            }
        }

    </style>

</head>
<?php require_once __DIR__ . "/bootstrap.php"; ?>
<?php
@session_start();
if (!$_SESSION['wcroot']) {
    header("Location:/worldcup-rov/login.php");
}
$mcon = new \controller\MatchController();
$matches = $mcon->getAll();

?>

<body>
<?php include_once __DIR__ . "/include/navbar.php" ?>

<!-- Page Content -->
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <input type="button" class="btn btn-success" value="Add" data-toggle="modal" data-target="#edit-form">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Match ID</td>
                    <td>A Team</td>
                    <td>B Team</td>
                    <td>DATE</td>
                    <td>Winner</td>
                    <td>Manage</td>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($matches as $key => $match) { ?>
                    <tr>
                        <td><?php echo $key; ?></td>
                        <td><?php echo $match->mid; ?></td>
                        <td><img src="<?php echo $match->teama->flag; ?>"><?php echo strtoupper($match->teama->name); ?>
                        </td>
                        <td><?php echo strtoupper($match->teamb->name); ?><img src="<?php echo $match->teamb->flag; ?>">
                        </td>
                        <td><?php echo $match->match_time; ?></td>
                        <?php if ($match->winner != 0) { ?>
                            <td><img src="<?php echo $match->winnerteam->flag; ?>"></td>
                        <?php } else { ?>
                            <td>wait for result</td>
                        <?php } ?>
                        <td><i mid="<?php echo $match->mid; ?>" class="fa fa-wrench edit-btn"></i>
                            <i class="fa fa-trash"></i> <i class="fa fa-calculator cal-rs"
                                                           mid="<?php echo $match->mid; ?>"></i></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="edit-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add & Edit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="current-mid" value="0">
                <div class="form-group">
                    <label for="mid">Match ID</label>
                    <input type="number" class="form-control" id="mid" aria-describedby="emailHelp"
                           placeholder="ID 1-15">

                </div>
                <div class="form-group">
                    <label for="teama-select">Team A</label>
                    <select class="form-control" id="teama-select">
                        <option></option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="teamb-select">Team B</label>
                    <select class="form-control" id="teamb-select"></select>
                </div>
                <div class="form-group">
                    <label for="winner-team">Winner</label>
                    <input type="number" class="form-control" id="winner-team" placeholder="0 if game haven't end">
                </div>
                <div class="form-group">
                    <label for="play-date">Match Date</label>
                    <input type="tex" class="form-control" id="play-date" placeholder="Play Date">
                </div>
                <div class="form-group">
                    <label for="deadline">Deadline</label>
                    <input type="tex" class="form-control" id="deadline" placeholder="Play Date">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="saveupdate" type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="vendor/moment/moment/moment.js"></script>
<script src="vendor/components/jquery/jquery.js"></script>
<script src="momentjs-datetimepicker/jquery.datetimepicker.js"></script>
<script src="vendor/twbs/bootstrap/dist/js/bootstrap.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function () {
        var teams;


        $('#play-date').datetimepicker({
            format: 'YYYY-MM-DD H:mm:ss'
        });

        $('#deadline').datetimepicker({
            format: 'YYYY-MM-DD H:mm:ss'
        });

        $.ajax({
            url: "/worldcup-rov/services/getallteam.php",
            method: "GET",
            dataType: "JSON"
        }).done(function (res) {
            teams = res;
            // console.log(teams);

            var opt = $("#teama-select").children().first();
            $("#teama-select").empty();
            $.each(teams, function (k, v) {
                var newopt = $(opt).clone();
                $(newopt).val(v.id);
                $(newopt).text(v.name + "(" + v.id + ")");
                $("#teama-select").append($(newopt));

            });

            $.each(teams, function (k, v) {
                var newopt = $(opt).clone();
                $(newopt).val(v.id);
                $(newopt).text(v.name + "(" + v.id + ")");
                $("#teamb-select").append($(newopt));
            });


        })


        // $(document).on('click',".logs", function (e) {
        //     var id = $(e.currentTarget).attr("lid");
        //     window.location="detail.php?id="+id;
        // });
        $(".edit-btn").on("click", function (e) {
            $("#edit-form").modal('show');
            var mid = $(e.currentTarget).attr('mid');
            $.ajax({
                url: "/worldcup-rov/services/getbymid.php",
                method: "GET",
                data: {mid: mid},
                dataType: "JSON"
            }).done(function (res) {
                // console.log(res);
                $("#teama-select").val(res.aid);
                $("#teamb-select").val(res.bid);
                $("#current-mid").val(res.mid);
                $("#mid").val(res.mid);
                $("#play-date").val(res.match_time);
                $("#winner-team").val(res.winner);
                $("#deadline").val(res.deadline);
            });
        })

        $("#saveupdate").on("click", function () {
            var mid = $("#mid").val();
            var aid = $("#teama-select").val();
            var bid = $("#teamb-select").val();
            var winner = $("#winner-team").val();
            var play_date = $("#play-date").val();
            var deadline = $("#deadline").val();
            $.ajax({
                url: "/worldcup-rov/services/updatematch.php",
                method: "GET",
                data: {mid: mid, aid: aid, bid: bid, winner: winner, play_date: play_date, deadline: deadline},
                dataType: "JSON"
            }).done(function (res) {
                console.log(res);
                location.reload();
            });
        })

        $(".cal-rs").on("click", function (e) {
            var mid = $(e.currentTarget).attr("mid");
            $.ajax({
                url: "/worldcup-rov/services/calrs.php",
                method: "GET",
                data: {mid: mid},
                dataType: "JSON"
            }).done(function (res) {
                swal("Update bet "+res.row+" row");
            });
        })
    });
</script>
</body>

</html>

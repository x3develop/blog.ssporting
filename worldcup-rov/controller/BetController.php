<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/27/2018
 * Time: 4:41 PM
 */

namespace controller;
use model\Bet;
use model\Match;

require_once __DIR__ . "/../bootstrap.php";

class BetController
{
    public function add($fbid, $mid, $tid)
    {
        $bet = Bet::where('fb_id', $fbid)
            ->where('mid', $mid)
            ->first();
        if (empty($bet)) {
            $bet = new Bet();
            $bet->fb_id = $fbid;
            $bet->mid = $mid;
            $bet->winner = $tid;
            $bet->save();
        }
        return $bet;
    }

    public function getByFbid($fbid)
    {
        return Bet::where('fb_id', $fbid)->with('game')->get();
    }

    public function calResult($mid)
    {
        $match = Match::where("mid", $mid)->first();
        if ($match->winner != 0) {
            Bet::where('mid', $mid)->where('winner', $match->winner)->update(['result' => 'win']);
            Bet::where('mid', $mid)->where('winner', "<>", $match->winner)->update(['result' => 'lose']);
        } else {
            Bet::where('mid', $mid)->where('winner', "<>", $match->winner)->update(['result' => null]);
        }
        return Bet::where('mid', $mid)->get()->count();
    }



}
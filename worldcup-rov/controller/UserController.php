<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/26/2018
 * Time: 4:25 PM
 */

namespace controller;

use model\User;

require_once __DIR__ . "/../bootstrap.php";

class UserController
{

    public function update($fb_id, $name, $access_token)
    {
        $user = User::firstOrCreate(['fb_id' => $fb_id]);
        $user->name = $name;
        $user->access_token = $access_token;
        $user->save();
        return $user;
    }
}
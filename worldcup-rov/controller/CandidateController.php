<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 7/2/2018
 * Time: 4:01 PM
 */

namespace controller;

use model\Bet;
use model\Candidate;

require_once __DIR__ . "/../bootstrap.php";


class CandidateController
{
    public function calCandidate($round)
    {
        $min = 1;
        $max = 8;
        Candidate::where('round', $round)->delete();
        switch ($round) {
            case 16:
                $min = 1;
                $max = 8;
                break;
            case 8:
                $min = 9;
                $max = 12;
                break;
            case 4:
                $min = 13;
                $max = 14;
                break;
            case 2:
                $min = 15;
                $max = 15;
                break;

            default:
                $min = 1;
                $max = 8;
                break;
        }

        $bets = Bet::whereBetween('mid', [$min, $max])->get();
        $candid = [];
        foreach ($bets as $bet) {
            if ($bet->result == 'win') {
                if (array_key_exists($bet->fb_id, $candid)) {
                    $candid[$bet->fb_id]++;
                } else {
                    $candid[$bet->fb_id] = 1;
                }
            }
        }

        foreach ($candid as $fb_id => $wincount) {
            if ($wincount == ($round / 2)) {
                $can = new Candidate();
                $can->fb_id = $fb_id;
                $can->round = $round;
                $can->save();
            }
        }

        return Candidate::where('round', $round)->get()->count();
    }

    public function getAllCandidate($round)
    {
        return Candidate::where("round", $round)->get();
    }

    public function getAllPlay()
    {
        return Bet::distinct()->get(['fb_id'])->count();
    }

    public function getRoundAttempt($round)
    {
        $min = 1;
        $max = 8;
        switch ($round) {
            case 16:
                $min = 1;
                $max = 8;
                break;
            case 8:
                $min = 9;
                $max = 12;
                break;
            case 4:
                $min = 13;
                $max = 14;
                break;
            case 2:
                $min = 15;
                $max = 15;
                break;

            default:
                $min = 1;
                $max = 8;
                break;
        }
        return Bet::whereBetween('mid', [$min, $max])->distinct()->get(['fb_id'])->count();
    }

    public function getRoundCandidate($round)
    {
        return Candidate::where("round", $round)->get()->count();
    }


}
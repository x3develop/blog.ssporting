<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/26/2018
 * Time: 4:14 PM
 */

require_once __DIR__ . "/../bootstrap.php";


$fb_id = $_REQUEST['fb_id'];
$access_token = $_REQUEST['access_token'];
$name = $_REQUEST['name'];

$ucon = new \controller\UserController();
$user = $ucon->update($fb_id, $name, $access_token);
header("ContentType:application/json");

echo json_encode($user);
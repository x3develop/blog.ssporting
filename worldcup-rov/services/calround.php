<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 7/2/2018
 * Time: 5:47 PM
 */

require_once __DIR__ . "/../bootstrap.php";


$round = $_REQUEST['round'];

$ccon = new \controller\CandidateController();

$rs['candidate'] = $ccon->calCandidate($round);

header("ContentType:application/json");
echo json_encode($rs);

<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/28/2018
 * Time: 4:10 PM
 */

require_once __DIR__ . "/../bootstrap.php";
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Cache\Adapter\Filesystem\FilesystemCachePool;

$mid = $_REQUEST['mid'];
$tcon = new \controller\MatchController();

$filesystemAdapter = new Local(__DIR__ . '/../');
$filesystem = new Filesystem($filesystemAdapter);
$pool = new FilesystemCachePool($filesystem);
$pool->setFolder("cache");
$item = $pool->getItem("mid" . $mid);

$rs = [];
$timeup = ["timeup" => true];
if (!$item->isHit()) {
    $rs = $tcon->getByMid($mid);
    $rs['source']="cache";
    $item->set($rs);
    $item->expiresAfter(180);
    $pool->save($item);
} else {
    $rs = $item->get();
}
$timeup['aid']=$rs['aid'];
$timeup['bid']=$rs['bid'];
$dt = \Carbon\Carbon::now();
$deadline = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $rs->deadline);
$deadline->subHours(7);
if ($dt <= $deadline && $rs->winner==0) {
    $timeup['timeup'] = false;
}

header("ContentTyp:application/json");
echo json_encode($timeup);
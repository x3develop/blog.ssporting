<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 7/6/2018
 * Time: 5:09 PM
 */
if (!isset($_SESSION)) {
    session_start();
}
$id=((isset($_GET['id']))?$_GET['id']:null);
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playBet.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
$playBet = new playBet();
$playMatch = new playMatch();

if (isset($_SESSION['login'])){
    $matchBet = $playBet->getTeamMatchById($_SESSION['login']['id'], $id);
    $matchView = $playMatch->getFirstMathById($id)[0];
    if(!empty($matchBet) and !empty($matchView)){ ?>
        <form class="form-horizontal" id="user-review-match">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มทรรศนะ</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="review[user_id]" value="<?php echo $_SESSION['login']['id'];?>">
                    <input type="hidden" name="review[match_id]" value="<?php echo $id;?>">
                    <input type="hidden" name="review[team]" value="<?php echo $matchBet?>">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ชือ</label>
                        <div class="col-sm-10">
                            <div class="clearfix">
                                <div class="box-user-img pull-left"><img src="<?= ((isset($_SESSION['login'])) ? 'https://graph.facebook.com/v2.8/' . $_SESSION['login']['fb_uid'] . '/picture' : '') ?>"></div>
                                <h4><?= ((isset($_SESSION['login'])) ? $_SESSION['login']['name'] : '') ?></h4>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ทีมที่เลือก</label>
                        <div class="col-sm-10">
                            <div class="box-player-preview">
                                <?php if($matchBet=="away"){ ?>
                                    <img src="<?php echo $matchView->teamAwayPath;?>">
                                    <span>@ <?php echo $matchView->teamAwayEn;?></span>
                                <?php }else if($matchBet=="home"){ ?>
                                    <img src="<?php echo $matchView->teamHomePath;?>">
                                    <span>@ <?php echo $matchView->teamHomeEn;?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ทรรศนะ</label>
                        <div class="col-sm-10">
                            <textarea name="review[review_text]" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่ม
                    </button>
                </div>
            </div>
        </form>
        <?php }else{ echo 'false'; } ?>
<?php }else{ echo 'false'; } ?>
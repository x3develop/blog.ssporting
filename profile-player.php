<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/profile.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="/js/flogin/src/flogin.js"></script>
    <script src="nanoscrollbar/nanoscrollbar.js"></script>
</head>
<body>
<div id="fb-root"></div>
<div id="fb-root"></div>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<?php //include 'header.php'; ?>
<!--index menu-->
<?php include '/view/index/menu.php'; ?>
<nav class="navbar navbar-menu-v2">
    <div class="container">
        <div class="nav-menutop">
            <ul class="nav nav-pills">
                <li><a href=""><strong><i class="fa fa-futbol-o" aria-hidden="true"></i> SPORTBALL</a></strong></li>
                <li><a href="">หน้าหลัก</a></li>
                <li><a href="">ไฮไลท์บอล</a></li>
                <li><a href="">ข่าวบอลต่างประเทศ</a></li>
                <li><a href="">ข่าวบอลไทย</a></li>
                <li><a href="">ข่าวกีฬาอื่นๆ</a></li>
            </ul>
        </div>
    </div>
</nav>


<div class="wrapper-news-articles">
    <div class="containter-fluid">
        <div class="bx-head-profile" style="padding: 120px 0px 120px 0px;">
            <div class="background-image_image3"></div>
        </div>
    </div>


    <div class="container">
        <div class="h-profile rows">
            <div class="bx-img-profile-player pull-left">
                <div class="img-profile-player"><img src="/images/p1.png"></div>
            </div>
            <div class="bx-line-profile pull-right" style="width: 80%;">
                <div class="pull-left profile-name">Sergio Aguero </div>
                <div style="clear:both;"></div>
            </div>
            <div style="clear:both;"></div>
        </div>
        <div class="rows" style="margin-top: 80px;">
            <div class="col-sm-3">
                <div class="section-menu-left">
                    <div class="bx-white">
                        <table class="table-history">
                            <tr>
                                <td colspan="2"><div class="text-left"><strong>Personal Details</strong></div></td>
                            </tr>
                            <tr>
                                <td>สัญชาติ</td>
                                <td>อาเจนตินา</td>
                            </tr>
                            <tr>
                                <td>วันเกิด</td>
                                <td>2 มิถุนายน 2550</td>
                            </tr>
                            <tr>
                                <td>อายุ</td>
                                <td>28 ปี</td>
                            </tr>
                        </table>
                        <div>
                            <table class="table-aptitude">
                                <tr>
                                    <td>
                                        <span>179</span>
                                        <div>ส่วนสูง</div>
                                    </td>
                                    <td>
                                        <span>79</span>
                                        <div>น้ำหนัก</div>
                                    </td>
                                    <td>
                                        <span>ทั้งสองเท้า</span>
                                        <div>เท้า</div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div>
                        <h3>นักเตะในทีม</h3>

                        <div class="h-line">ผู้รักษาประตู</div>
                        <table class="table-list-player table-striped">
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="h-line">กองหลัง</div>
                        <table class="table-list-player table-striped">
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="h-line">กองกลาง</div>
                        <table class="table-list-player table-striped">
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-9" style="margin-top: -60px;">
                <div class="mg-bt15">
                    <div class="col-sm-6">
                        <div class="bx-white" style="height: 240px;">
                            <table class="table-information-team">
                                <tr style="border-bottom: solid 1px #eee;">
                                    <td>
                                        <table>
                                            <tr>
                                                <td><img src="images/logo-team/manchester.png" style="width: 70px;"></td>
                                                <td>
                                                    <span>สโมสร</span>
                                                    <div class="txt-black"><strong>Manchester United</strong></div>
                                                    <span>ตำแหน่ง</span>
                                                    <div class="txt-black"><strong>Forward</strong></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td><div style="font-size: 3em;"><i class="fa fa-user"></i></div></td>
                                                <td>
                                                    <div style="font-size: 2em;"><strong>14</strong></div>
                                                    <div>หมายเลขเสื้อ</div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>เวลาย้ายเข้า</td>
                                    <td class="text-right">30-09-2018</td>
                                </tr>
                                <tr>
                                    <td>ย้ายมาจาก</td>
                                    <td class="text-right">แอดมาดริด</td>
                                </tr>
                            </table>
                            <div class="h-line">สโมสรที่เคยค้าแข้ง</div>
                            <div class="bx-stat-played">แอดมาดริด, ปอร์โด้, เซาเปาโล</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="bx-white">
                            <div class="h-line">สถิติการลงเล่น</div>
                            <table class="table-stat-played table-striped">
                                <tr>
                                    <td><i class="fa fa-bar-chart"></i></td>
                                    <td>ลงเล่นทั้งหมด</td>
                                    <td><span>2,459</span></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-bar-chart"></i></td>
                                    <td>ประตู</td>
                                    <td><span>364</span></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-bar-chart"></i></td>
                                    <td>แอตซิส</td>
                                    <td><span>1,003</span></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-bar-chart"></i></td>
                                    <td>ชนะ</td>
                                    <td><span>243</span></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-bar-chart"></i></td>
                                    <td>แพ้</td>
                                    <td><span>543</span></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-bar-chart"></i></td>
                                    <td>เสมอ</td>
                                    <td><span>220</span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                </div>

                <div class="rows mg-bt15">
                   <div class="col-sm-4">
                       <div class="bx-white">
                           <div class="mg-bt15"><strong>สถิติการยิงประตู</strong></div>
                           <div class="text-center"><img src="images/icon-play.png" style="width: 50%;"></div>
                           <table class="table-stat-played">
                               <tr>
                                   <td><span class="info"><i class="fa fa-circle"></i></span></td>
                                   <td>ลีก</td>
                                   <td>360</td>
                               </tr>
                               <tr>
                                   <td><span><i class="fa fa-circle"></i></span></td>
                                   <td>Cup</td>
                                   <td>60</td>
                               </tr>
                               <tr>
                                   <td><span class="down"><i class="fa fa-circle"></i></span></td>
                                   <td>Uefa</td>
                                   <td>60</td>
                               </tr>
                               <tr>
                                   <td><span class="down"><i class="fa fa-circle"></i></span></td>
                                   <td>ทีมชาติ</td>
                                   <td>30</td>
                               </tr>
                           </table>
                       </div>
                   </div>
                    <div class="col-sm-8">
                        <div class="bx-white">
                            <div class="mg-bt15"><strong>การยิงประตู</strong></div>
                            <div class="bx-wrap-row">
                             <table class="table-goals">
                                <tr class="bg-h-blue">
                                    <th>Season</th>
                                    <th>Club</th>
                                    <th>Apps(Subs)</th>
                                    <th>Goals</th>
                                </tr>
                                <tr>
                                    <td>2014/2015</td>
                                    <td><img src="images/logo-team/manchester.png"> Manchester City</td>
                                    <td>30(1)</td>
                                    <td>20</td>
                                </tr>
                                <tr>
                                    <td>2014/2015</td>
                                    <td><img src="images/logo-team/manchester.png"> Manchester City</td>
                                    <td>30(1)</td>
                                    <td>20</td>
                                </tr>
                                <tr>
                                    <td>2014/2015</td>
                                    <td><img src="images/logo-team/manchester.png"> Manchester City</td>
                                    <td>30(1)</td>
                                    <td>20</td>
                                </tr>
                                <tr>
                                    <td>2014/2015</td>
                                    <td><img src="images/logo-team/manchester.png"> Manchester City</td>
                                    <td>30(1)</td>
                                    <td>20</td>
                                </tr>
                                <tr>
                                    <td>2014/2015</td>
                                    <td><img src="images/logo-team/manchester.png"> Manchester City</td>
                                    <td>30(1)</td>
                                    <td>20</td>
                                </tr>
                                <tr>
                                    <td>2014/2015</td>
                                    <td><img src="images/logo-team/manchester.png"> Manchester City</td>
                                    <td>30(1)</td>
                                    <td>20</td>
                                </tr>
                                <tr>
                                    <td>2014/2015</td>
                                    <td><img src="images/logo-team/manchester.png"> Manchester City</td>
                                    <td>30(1)</td>
                                    <td>20</td>
                                </tr>
                            </table>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>

                <div class="rows">
                    <div class="title-articles">ข่าวที่เกี่ยวข้อง</div>
                    <div class="col-sm-4">
                        <div class="bx-story-small">
                            <div class="hilight-cover"></div>
                            <div class="crop" style="background-image: url(http://www.gudoball.com/wp-content/uploads/2016/11/hd-radamel-falcao-monaco_12j2awfbt2uvy16xckijm0c338.jpg)"></div>
                            <div class="bx-story-content">
                                <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long"> 2 hour ago</span>
                                </div>
                                <a href="" class="h-content">เสือคืนป่า! ฟัลเกายิงประตู UCL รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="bx-story-small">
                            <div class="hilight-cover"></div>
                            <div class="crop" style="background-image: url(http://www.gudoball.com/wp-content/uploads/2016/11/hd-radamel-falcao-monaco_12j2awfbt2uvy16xckijm0c338.jpg)"></div>
                            <div class="bx-story-content">
                                <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long"> 2 hour ago</span>
                                </div>
                                <a href="" class="h-content">เสือคืนป่า! ฟัลเกายิงประตู UCL รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="bx-story-small">
                            <div class="hilight-cover"></div>
                            <div class="crop" style="background-image: url(http://www.gudoball.com/wp-content/uploads/2016/11/hd-radamel-falcao-monaco_12j2awfbt2uvy16xckijm0c338.jpg)"></div>
                            <div class="bx-story-content">
                                <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long"> 2 hour ago</span>
                                </div>
                                <a href="" class="h-content">เสือคืนป่า! ฟัลเกายิงประตู UCL รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both;"></div>
        </div>


    </div>
</div>
<?php //include 'footer.php';
include 'view/index/footer.php';
?>

<script>
    $(".nano").nanoScroller();
</script>

</body>
</html>
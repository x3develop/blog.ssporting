<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 6/28/2018
 * Time: 10:18 PM
 */
$id=((isset($_GET['id']))?$_GET['id']:null);
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playBet.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
$playBet = new playBet();
$playMatch = new playMatch();
$matchView=$playMatch->getFirstMathById($id)[0];
$matchResultsHome = $playMatch->getListMatchResults($matchView->team_home, 5);
$matchResultsAway = $playMatch->getListMatchResults($matchView->team_away, 5);
$userListHome = $playMatch->getUserBet($matchView->id,'home',5);
$userListAway = $playMatch->getUserBet($matchView->id,'away',5);
$percentBet = $playBet->percentBet($matchView->id);
?>
<li class="box-last-match text-right">
    <table>
        <tr class="tr-btn-results-left">
            <td class="btn-results-left"><i class="fa fa-caret-left" aria-hidden="true"></i></td>
            <?php if (!empty($matchResultsHome)) { ?>
                <?php foreach (array_reverse($matchResultsHome) as $keyResults => $valueResults) { ?>
                    <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                        <td class="li-results"><img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/draw.png"></td>
                    <?php } elseif ($valueResults->team_home == $matchView->team_home && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                        <td class="li-results"><img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/win.png"></td>
                    <?php } elseif ($valueResults->team_away == $matchView->team_home && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                        <td class="li-results"><img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/win.png"></td>
                    <?php } else { ?>
                        <td class="li-results"><img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/lose.png"></td>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
            <td>Last <span class="total-results">5</span> Match</td>
        </tr>
        <tr class="text-right">
            <td></td>
            <?php if(!empty($userListHome)){ ?>
                <td><img src="<?php echo ((!empty($userListHome[4]))?'https://graph.facebook.com/v2.8/'.$userListHome[4]->fb_uid.'/picture':'/images/avatar.png');?>"></td>
                <td><img src="<?php echo ((!empty($userListHome[3]))?'https://graph.facebook.com/v2.8/'.$userListHome[3]->fb_uid.'/picture':'/images/avatar.png');?>"></td>
                <td><img src="<?php echo ((!empty($userListHome[2]))?'https://graph.facebook.com/v2.8/'.$userListHome[2]->fb_uid.'/picture':'/images/avatar.png');?>"></td>
                <td><img src="<?php echo ((!empty($userListHome[1]))?'https://graph.facebook.com/v2.8/'.$userListHome[1]->fb_uid.'/picture':'/images/avatar.png');?>"></td>
                <td><img src="<?php echo ((!empty($userListHome[0]))?'https://graph.facebook.com/v2.8/'.$userListHome[0]->fb_uid.'/picture':'/images/avatar.png');?>"></td>
            <?php }else{ ?>
                <td><img src="/images/avatar.png"></td>
                <td><img src="/images/avatar.png"></td>
                <td><img src="/images/avatar.png"></td>
                <td><img src="/images/avatar.png"></td>
                <td><img src="/images/avatar.png"></td>
            <?php  } ?>
            <td>Player</td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="text-left"><span><?=number_format($percentBet['pHome'],2)?>%</span></td>
            <td>
                <div class="progress pull-right">
                    <div class="progress-bar pull-right" role="progressbar" aria-valuenow="<?=number_format($percentBet['pHome'],2)?>"
                         aria-valuemin="0" aria-valuemax="100" style="width: <?=number_format($percentBet['pHome'],2)?>%;">
                    </div>
                </div>
            </td>
        </tr>
    </table>
</li>
<li class="box-HDP-game text-center">
    <table>
        <tr>
            <td class="text-right"><p><?= (!empty($matchView->home_water_bill)?number_format($matchView->home_water_bill,2):'-'); ?></p></td>
            <td><h2>[<?= $matchView->handicap ?>]</h2></td>
            <td class="text-left"><p><?= (!empty($matchView->away_water_bill)?number_format($matchView->away_water_bill,2):'-'); ?></p></td>
        </tr>
    </table>
</li>
<li class="box-last-match text-left">
    <table>
        <tr class="tr-btn-results-right">
            <td>Last <span class="total-results">5</span> Match</td>
            <?php if (!empty($matchResultsAway)) { ?>
                <?php foreach ($matchResultsAway as $keyResults => $valueResults) { ?>
                    <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                        <td class="li-results"><img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/draw.png"></td>
                    <?php } elseif ($valueResults->team_home == $matchView->team_away && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                        <td class="li-results"><img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/win.png"></td>
                    <?php } elseif ($valueResults->team_away == $matchView->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                        <td class="li-results"><img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/win.png"></td>
                    <?php } else { ?>
                        <td class="li-results"><img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/lose.png"</td>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
            <td class="btn-results-right"><i class="fa fa-caret-right" aria-hidden="true"></i></td>
        </tr>
        <tr>
            <td>Player</td>
            <!--                            <td></td>-->
            <?php if(!empty($userListAway)){ ?>
                <td><img src="<?php echo ((!empty($userListAway[0]))?'https://graph.facebook.com/v2.8/'.$userListAway[0]->fb_uid.'/picture':'/images/avatar.png');?>"></td>
                <td><img src="<?php echo ((!empty($userListAway[1]))?'https://graph.facebook.com/v2.8/'.$userListAway[1]->fb_uid.'/picture':'/images/avatar.png');?>"></td>
                <td><img src="<?php echo ((!empty($userListAway[2]))?'https://graph.facebook.com/v2.8/'.$userListAway[2]->fb_uid.'/picture':'/images/avatar.png');?>"></td>
                <td><img src="<?php echo ((!empty($userListAway[3]))?'https://graph.facebook.com/v2.8/'.$userListAway[3]->fb_uid.'/picture':'/images/avatar.png');?>"></td>
                <td><img src="<?php echo ((!empty($userListAway[4]))?'https://graph.facebook.com/v2.8/'.$userListAway[4]->fb_uid.'/picture':'/images/avatar.png');?>"></td>
            <?php }else{ ?>
                <td><img src="/images/avatar.png"></td>
                <td><img src="/images/avatar.png"></td>
                <td><img src="/images/avatar.png"></td>
                <td><img src="/images/avatar.png"></td>
                <td><img src="/images/avatar.png"></td>
            <?php  } ?>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <div class="progress" style="">
                    <div class="progress-bar pull-left" role="progressbar" aria-valuenow="<?=number_format($percentBet['pAway'],2)?>"
                         aria-valuemin="0" aria-valuemax="100" style="width: <?=number_format($percentBet['pAway'],2)?>%;">
                    </div>
                </div>
            </td>
            <td><span><?=number_format($percentBet['pAway'],2)?>%</span></td>
        </tr>
    </table>
</li>
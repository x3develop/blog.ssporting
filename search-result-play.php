<?php
require('simple_html_dom.php');
function safe($value)
{
    return urlencode($value);
}
$videoid = safe($_GET['info']);
$myUrl = "https://www.youtube.com/watch?v=" . $videoid;
$html = file_get_html($myUrl);
$videos = array();
$mostViewed =  array();
$i = 1;


function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

$title = get_string_between($html, '<meta name="title" content="', '">');
$uploader = get_string_between($html, 'author":"', '"');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $title; ?></title>
    <meta name="keywords" content="คลิปวีดีโอฟุตบอล, ไฮไลท์ฟุตบอล, วีดีโอบอล, ค้นหา">
    <meta name="description" content="อยากดูคลิป <?php echo $title; ?> ต้องค้นหากับเราที่ ngoal.com ไม่ว่าจะเป็น ไฮไลท์ ไหน คลิปไหน ก็ค้นหาเจอ">

    <!--    CSS-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/search-style.css">
    <link rel="stylesheet" href="/css/swiper.min.css">
    <link rel="stylesheet" href="/css/highlight-new.css">

    <script src="/js/swiper.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.nanoscroller/0.8.7/css/nanoscroller.css">

    <!-- <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nanoscroller/0.8.7/javascripts/jquery.nanoscroller.js"></script>
    <![endif]-->
</head>
<style type="text/css">
    .content-menu-index.affix{position: relative;}
</style>
<body style="background-color: #fafafa;">
<a href="#" id="scroll" style="display: none;"><span></span></a>
<!--index menu-->
<?php include $_SERVER['DOCUMENT_ROOT'] . '/view/index/menu-all.php'; ?>

<div class="container-fluid">
    <div class="content-lg-video inline-box">
        <iframe width="100%" height="500" src="https://www.youtube.com/embed/<?php echo $_GET['info']; ?>" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen>
        </iframe>
        <div class="box-title-video full-width inline-box text-left">
            <h2><?php echo $title; ?></h2>
<!--            <span class="full-width">86,254 views</span>-->
        </div>

        <?php include("recent-onsearch.php"); ?>
    </div>
    <div class="content-list-video inline-box" data-spy="affix" data-offset-top="20">
        <div class="text-title">
            <h4><i class="fa fa-video-camera" aria-hidden="true"></i> Video ที่เกี่ยวข้อง</h4>
        </div>
        <div id="about" class="nano">
            <div class="nano-content">
                <?php
                $input = str_replace(' ','+',safe($title));
                $myUrl = "https://www.youtube.com/results?search_query=" . $input;
                $html = file_get_html($myUrl);
                $videos = array();
                $mostViewed =  array();
                $i = 1;
                foreach ($html->find('div.yt-lockup-content') as $video) {

                $title = $video->find('h3.yt-lockup-title a', 0)->plaintext;
                $title = str_replace('^',' ',$title);
                $title = str_replace('|',' ',$title);
                $url = $video->find('h3.yt-lockup-title a', 0)->href;
                $videoidIn = str_replace("/watch?v=","",$url);
                $duration = $video->find('h3.yt-lockup-title span', 0);
                $views = $video->find('ul.yt-lockup-meta-info li', 1); ?>
                    <?php if(!strpos($videoidIn, 'list')) { ?>
                        <a strops="<?php echo strpos($videoidIn, 'list'); ?>" href="/search-result-play?info=<?php echo $videoidIn; ?>" class="box-list-row inline-box full-width <?php echo (($videoidIn==$videoid)?"active":"")?>">
                            <div class="thumbnail-list-video inline-box">
                                <img src="https://i1.ytimg.com/vi/<?php echo $videoidIn; ?>/mqdefault.jpg">
                            </div>
                            <div class="content-list-video-side inline-box">
                                <h3><?php echo $title; ?></h3>
                                <span><?php echo str_replace('</li>','',str_replace('<li>','',$views));?></span>
                            </div>
                        </a>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>


<!-- Initialize Swiper -->
<script>
    var swiper2 = new Swiper('.swiper2', {
        slidesPerView: 4,
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next-recent',
            prevEl: '.swiper-button-prev-recent',
        },
    });
</script>
<script>
    var swiper3 = new Swiper('.swiper3', {
        slidesPerView: 4,
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next-highlight',
            prevEl: '.swiper-button-prev-highlight',
        },
    });
</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

<script>
    $(".nano").nanoScroller({ scroll: 'top' });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#scroll').fadeIn();
            } else {
                $('#scroll').fadeOut();
            }
        });
        $('#scroll').click(function () {
            $("html, body").animate({scrollTop: 0}, 400);
            return false;
        });
    });
</script>
</body>
</html>
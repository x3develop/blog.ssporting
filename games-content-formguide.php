<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 6/26/2018
 * Time: 10:47 AM
 */
$id = ($_REQUEST["id"]) ? $_REQUEST['id'] : null;
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
$playMatch = new playMatch();
$matchView = $playMatch->getFirstMathById($id)[0];
$dataFormGuideHome = $playMatch->getFormGuide($matchView->team_home, 50);
$dataLeagueFormGuideHome = $playMatch->getLeagueByFormGuide($matchView->team_home, 50);
$dataFormGuideAway = $playMatch->getFormGuide($matchView->team_away, 50);
$dataLeagueFormGuideAway = $playMatch->getLeagueByFormGuide($matchView->team_away, 50);
?>

<div class="content-state-game" style="width: 205px;">
    <h2>FORM GUIDE</h2>
    <div class="head-state-game"></div>
</div>
<div class="col-sm-6 formGuide-tabs-team1">
    <div class="pull-right">
        <!--            กราฟวงกลม-->
        <div class="section-state-box section-state-box-circle text-left">
            <div class="topic-section-state">
                <table>
                    <tr>
                        <td>
                            <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Chatdata Odds/ Result</h4>
                        </td>
                        <td class="text-right">

                        </td>
                    </tr>
                </table>
            </div>
            <div class="content-section-state row">

                <div class="col-sm-6">
                    <div class="box-circle-data">
                        <div id="formguidedonut-H-oresult"
                             style="z-index: 1; width: 204px; height: 174px;">
                            <img src="/images/chart-simple.png">
                        </div>
                        <div class="text-h2hdonut-h-oresult">Odds</div>
                    </div>
                    <div class="data-chart">
                        <div class="clearfix">
                            <h4 id="oddPWin">41.4</h4>
                            <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                        </div>
                        <div class="clearfix">
                            <h4 id="oddPLose">58.6</h4>
                            <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                        </div>
                        <div class="clearfix">
                            <h4 id="oddPDraw">0</h4>
                            <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box-circle-data">
                        <div id="formguidedonut-H-result"
                             style="z-index: 1; width: 204px; height: 174px;">
                            <img src="/images/chart-simple.png">
                        </div>
                        <div class="text-h2hdonut-h-oresult">Result</div>
                    </div>
                    <div class="data-chart">
                        <div class="clearfix">
                            <h4 id="resultPWin">41.4</h4>
                            <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                        </div>
                        <div class="clearfix">
                            <h4 id="resultPLose">58.6</h4>
                            <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                        </div>
                        <div class="clearfix">
                            <h4 id="resultPDraw">0</h4>
                            <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--            ตาราง-->
        <div class="section-state-box text-left">
            <div class="topic-section-state">
                <table>
                    <tr>
                        <td>
                            <h4><img src="<?= $matchView->teamHomePath ?>"> <?= $matchView->teamHomeEn ?></h4>
                        </td>
                        <td class="text-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-sm btn-default dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Last Match(<label
                                        id="formguide-h-max-match"><?php echo((count($dataFormGuideHome) >= 10) ? '10' : count($dataFormGuideHome)) ?></label>)
                                    <span class="caret"></span>
                                </button>
                                <ul id="formguide-h-select-limit" class="dropdown-menu">
                                    <?php if (count($dataFormGuideHome) >= 20) {
                                        for ($i = 1; $i <= 20; $i++) { ?>
                                            <li><a onclick="viewFormGuideHome(<?= (($i)) ?>)">Last
                                                    Match(<?= (($i)) ?>)</a></li>
                                        <?php }
                                    } else {
                                        for ($i = 1; $i <= (count($dataFormGuideHome) - 1); $i++) { ?>
                                            <li><a onclick="viewFormGuideHome(<?= (($i)) ?>)">Last
                                                    Match(<?= (($i)) ?>)</a></li>
                                        <?php }
                                    } ?>
                                    <li><a onclick="viewFormGuideHome(<?= count($dataFormGuideHome) ?>)">Last
                                            Match(<?= count($dataFormGuideHome) ?>)</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content-section-state row">
                <div class="box-table-result-state" style="padding: 0 15px;">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs  nav-pills" role="tablist">
                        <li role="presentation" class="active">
                            <a onclick="viewFormGuideHome()" href="#formguideHTotal" aria-controls="formguideHTotal"
                               role="tab" data-toggle="tab">Total</a></li>
                        <li role="presentation">
                            <a onclick="viewFormGuideHome()" href="#formguideHHome" aria-controls="formguideHHome"
                               role="tab" data-toggle="tab">Home</a></li>
                        <li role="presentation">
                            <a onclick="viewFormGuideHome()" href="#formguideHAway" aria-controls="formguideHAway"
                               role="tab" data-toggle="tab">Away</a></li>
                        <li class="pull-right">
                            <label class="checkbox-inline">
                                <input onclick="viewFormGuideHome()" type="checkbox"
                                       class="checkbox_formguide_team1_all" value="all" checked> All
                            </label>
                            <?php foreach ($dataLeagueFormGuideHome as $key => $value) { ?>
                                <label class="checkbox-inline">
                                    <input onclick="viewFormGuideHome()" type="checkbox"
                                           class="checkbox_formguide_team1_<?= $key ?>"
                                           value="<?= $key ?>"> <?= $value ?>
                                </label>
                            <?php } ?>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="formguideHTotal">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th class="text-center mini-col">HDP</th>
                                    <th class="text-center mini-col">Odds</th>
                                    <th class="text-center mini-col">Result</th>
                                </tr>
                                </thead>
                                <?php foreach ($dataFormGuideHome as $key => $value) { ?>
                                    <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                        id="guide-row-original"
                                        class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                        <td>
                                            <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                            <p><i class="fa fa-calendar"
                                                  aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                | <i
                                                    class="fa fa-clock-o"
                                                    aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                            </p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                            <img src="<?= $value->teamHomePath ?>">
                                        </td>
                                        <td style="width: 12%;">
                                            <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="<?= $value->teamAwayPath ?>">
                                            <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                        </td>
                                        <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                        <td class="text-center tr-odd">
                                            <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamHomeEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                <img class="guide-result img-win"
                                                     src="/images/icon-stat/result/0.png">
                                            <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamHomeEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                <img class="guide-result img-lose"
                                                     src="/images/icon-stat/result/2.png">
                                            <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                <img class="guide-result img-draw"
                                                     src="/images/icon-stat/result/1.png">
                                            <?php } else { ?>
                                                -
                                            <?php } ?>
                                        </td>
                                        <td class="text-center tr-result">
                                            <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamHomeEn == $value->teamAwayEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                <img class="guide-result img-win"
                                                     src="/images/icon-stat/result/0.png">
                                            <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamHomeEn == $value->teamAwayEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                <img class="guide-result img-lose"
                                                     src="/images/icon-stat/result/2.png">
                                            <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                <img class="guide-result img-draw"
                                                     src="/images/icon-stat/result/1.png">
                                            <?php } else { ?>
                                                -
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="formguideHHome">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th class="text-center mini-col">HDP</th>
                                    <th class="text-center mini-col">Odds</th>
                                    <th class="text-center mini-col">Result</th>
                                </tr>
                                </thead>
                                <?php foreach ($dataFormGuideHome as $key => $value) { ?>
                                    <?php if ($matchView->teamHomeEn == $value->teamHomeEn) { ?>
                                        <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                            id="guide-row-original"
                                            class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                            <td>
                                                <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                <p><i class="fa fa-calendar"
                                                      aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                    | <i
                                                        class="fa fa-clock-o"
                                                        aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                </p>
                                            </td>
                                            <td class="text-right box-team-state">
                                                <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                <img src="<?= $value->teamHomePath ?>">
                                            </td>
                                            <td>
                                                <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                    - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                            </td>
                                            <td class="text-left box-team-state">
                                                <img src="<?= $value->teamAwayPath ?>">
                                                <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                            </td>
                                            <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                            <td class="text-center tr-odd">
                                                <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamHomeEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                    <img class="guide-result img-win"
                                                         src="/images/icon-stat/result/0.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamHomeEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                    <img class="guide-result img-lose"
                                                         src="/images/icon-stat/result/2.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                    <img class="guide-result img-draw"
                                                         src="/images/icon-stat/result/1.png">
                                                <?php } else { ?>
                                                    -
                                                <?php } ?>
                                            </td>
                                            <td class="text-center tr-result">
                                                <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamHomeEn == $value->teamAwayEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                    <img class="guide-result img-win"
                                                         src="/images/icon-stat/result/0.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamHomeEn == $value->teamAwayEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                    <img class="guide-result img-lose"
                                                         src="/images/icon-stat/result/2.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                    <img class="guide-result img-draw"
                                                         src="/images/icon-stat/result/1.png">
                                                <?php } else { ?>
                                                    -
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="formguideHAway">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th class="text-center mini-col">HDP</th>
                                    <th class="text-center mini-col">Odds</th>
                                    <th class="text-center mini-col">Result</th>
                                </tr>
                                </thead>
                                <?php foreach ($dataFormGuideHome as $key => $value) { ?>
                                    <?php if ($matchView->teamHomeEn != $value->teamHomeEn) { ?>
                                        <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                            id="guide-row-original"
                                            class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                            <td>
                                                <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                <p><i class="fa fa-calendar"
                                                      aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                    | <i
                                                        class="fa fa-clock-o"
                                                        aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                </p>
                                            </td>
                                            <td class="text-right box-team-state">
                                                <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                <img src="<?= $value->teamHomePath ?>">
                                            </td>
                                            <td>
                                                <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                    - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                            </td>
                                            <td class="text-left box-team-state">
                                                <img src="<?= $value->teamAwayPath ?>">
                                                <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                            </td>
                                            <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                            <td class="text-center tr-odd">
                                                <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamHomeEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                    <img class="guide-result img-win"
                                                         src="/images/icon-stat/result/0.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamHomeEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                    <img class="guide-result img-lose"
                                                         src="/images/icon-stat/result/2.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                    <img class="guide-result img-draw"
                                                         src="/images/icon-stat/result/1.png">
                                                <?php } else { ?>
                                                    -
                                                <?php } ?>
                                            </td>
                                            <td class="text-center tr-result">
                                                <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamHomeEn == $value->teamAwayEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                    <img class="guide-result img-win"
                                                         src="/images/icon-stat/result/0.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamHomeEn == $value->teamAwayEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                    <img class="guide-result img-lose"
                                                         src="/images/icon-stat/result/2.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                    <img class="guide-result img-draw"
                                                         src="/images/icon-stat/result/1.png">
                                                <?php } else { ?>
                                                    -
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="clear: both"></div>
</div>
<div class="col-sm-6 margin-top formGuide-tabs-team2" style="background-color: #f1f1f1;">
    <div class="pull-left">
        <!--            กราฟวงกลม-->
        <div class="section-state-box section-state-box-circle text-left">
            <div class="topic-section-state">
                <table>
                    <tr>
                        <td>
                            <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Chatdata Odds/ Result</h4>
                        </td>
                        <td class="text-right">

                        </td>
                    </tr>
                </table>
            </div>
            <div class="content-section-state row">
                <div class="col-sm-6">
                    <div class="box-circle-data">
                        <div id="formguidedonut-A-oresult">
                            <img src="/images/chart-simple.png">
                        </div>
                        <div class="text-h2hdonut-h-oresult">Odds</div>
                    </div>
                    <div class="data-chart">
                        <div class="clearfix">
                            <h4 id="oddPWin">41.4</h4>
                            <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                        </div>
                        <div class="clearfix">
                            <h4 id="oddPLose">58.6</h4>
                            <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                        </div>
                        <div class="clearfix">
                            <h4 id="oddPDraw">0</h4>
                            <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box-circle-data">
                        <div id="formguidedonut-A-result">
                            <img src="/images/chart-simple.png">
                        </div>
                        <div class="text-h2hdonut-h-oresult">Result</div>
                    </div>
                    <div class="data-chart">
                        <div class="clearfix">
                            <h4 id="resultPWin">41.4</h4>
                            <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                        </div>
                        <div class="clearfix">
                            <h4 id="resultPLose">58.6</h4>
                            <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                        </div>
                        <div class="clearfix">
                            <h4 id="resultPDraw">0</h4>
                            <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--            ตาราง-->
        <div class="section-state-box text-left">
            <div class="topic-section-state">
                <table>
                    <tr>
                        <td>
                            <h4><img src="<?= $matchView->teamAwayPath ?>"> <?= $matchView->teamAwayEn ?></h4>
                        </td>
                        <td class="text-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-sm btn-default dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Last Match(<label
                                        id="formguide-a-max-match"><?php echo((count($dataFormGuideAway) >= 10) ? '10' : count($dataFormGuideAway)) ?></label>)
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <?php if (count($dataFormGuideAway) >= 20) {
                                        for ($i = 1; $i <= 20; $i++) { ?>
                                            <li><a onclick="viewFormGuideAway(<?= (($i)) ?>)">Last
                                                    Match(<?= (($i)) ?>)</a></li>
                                        <?php }
                                    } else {
                                        for ($i = 1; $i <= (count($dataFormGuideAway) - 1); $i++) { ?>
                                            <li><a onclick="viewFormGuideAway(<?= (($i)) ?>)">Last
                                                    Match(<?= (($i)) ?>)</a></li>
                                        <?php }
                                    } ?>
                                    <li><a onclick="viewFormGuideAway(<?= count($dataFormGuideAway) ?>)">Last
                                            Match(<?= count($dataFormGuideAway) ?>)</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="content-section-state row">
                <div class="box-table-result-state" style="padding: 0 15px;">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-pills" role="tablist">
                        <li role="presentation" class="active">
                            <a onclick="viewFormGuideAway()" href="#formguideATotal" aria-controls="formguideATotal"
                               role="tab" data-toggle="tab">Total</a></li>
                        <li role="presentation">
                            <a onclick="viewFormGuideAway()" href="#formguideAHome" aria-controls="formguideAHome"
                               role="tab" data-toggle="tab">Home</a></li>
                        <li role="presentation">
                            <a onclick="viewFormGuideAway()" href="#formguideAAway" aria-controls="formguideAAway"
                               role="tab" data-toggle="tab">Away</a></li>
                        <li class="pull-right">
                            <label class="checkbox-inline">
                                <input onclick="viewFormGuideAway()" type="checkbox"
                                       class="checkbox_formguide_team2_all" value="all" checked> All
                            </label>
                            <?php foreach ($dataLeagueFormGuideAway as $key => $value) { ?>
                                <label class="checkbox-inline">
                                    <input onclick="viewFormGuideAway()" type="checkbox"
                                           class="checkbox_formguide_team2_<?= $key ?>"
                                           value="<?= $key ?>"> <?= $value ?>
                                </label>
                            <?php } ?>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="formguideATotal">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th class="text-center mini-col">HDP</th>
                                    <th class="text-center mini-col">Odds</th>
                                    <th class="text-center mini-col">Result</th>
                                </tr>
                                </thead>
                                <?php foreach ($dataFormGuideAway as $key => $value) { ?>
                                    <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                        id="guide-row-original"
                                        class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                        <td>
                                            <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                            <p><i class="fa fa-calendar"
                                                  aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                | <i
                                                    class="fa fa-clock-o"
                                                    aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                            </p>
                                        </td>
                                        <td class="text-right box-team-state">
                                            <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                            <img src="<?= $value->teamHomePath ?>">
                                        </td>
                                        <td style="width: 12%;">
                                            <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                        </td>
                                        <td class="text-left box-team-state">
                                            <img src="<?= $value->teamAwayPath ?>">
                                            <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                        </td>
                                        <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                        <td class="text-center tr-odd">
                                            <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamAwayEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                <img class="guide-result img-win"
                                                     src="/images/icon-stat/result/0.png">
                                            <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamAwayEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                <img class="guide-result img-lose"
                                                     src="/images/icon-stat/result/2.png">
                                            <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                <img class="guide-result img-draw"
                                                     src="/images/icon-stat/result/1.png">
                                            <?php } else { ?>
                                                -
                                            <?php } ?>
                                        </td>
                                        <td class="text-center tr-result">
                                            <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->homeScore > $value->awayScore) or ($matchView->teamAwayEn == $value->teamAwayEn and $value->awayScore > $value->homeScore))) { ?>
                                                <img class="guide-result img-win"
                                                     src="/images/icon-stat/result/0.png">
                                            <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamAwayEn == $value->teamAwayEn and $value->homeScore > $value->awayScore))) { ?>
                                                <img class="guide-result img-lose"
                                                     src="/images/icon-stat/result/2.png">
                                            <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                <img class="guide-result img-draw"
                                                     src="/images/icon-stat/result/1.png">
                                            <?php } else { ?>
                                                -
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="formguideAHome">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th class="text-center mini-col">HDP</th>
                                    <th class="text-center mini-col">Odds</th>
                                    <th class="text-center mini-col">Result</th>
                                </tr>
                                </thead>
                                <?php foreach ($dataFormGuideAway as $key => $value) { ?>
                                    <?php if ($matchView->teamAwayEn != $value->teamAwayEn) { ?>
                                        <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                            id="guide-row-original"
                                            class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                            <td>
                                                <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                <p><i class="fa fa-calendar"
                                                      aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                    | <i
                                                        class="fa fa-clock-o"
                                                        aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                </p>
                                            </td>
                                            <td class="text-right box-team-state">
                                                <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                <img src="<?= $value->teamHomePath ?>">
                                            </td>
                                            <td>
                                                <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                    - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                            </td>
                                            <td class="text-left box-team-state">
                                                <img src="<?= $value->teamAwayPath ?>">
                                                <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                            </td>
                                            <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                            <td class="text-center tr-odd">
                                                <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamAwayEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                    <img class="guide-result img-win"
                                                         src="/images/icon-stat/result/0.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamAwayEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                    <img class="guide-result img-lose"
                                                         src="/images/icon-stat/result/2.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                    <img class="guide-result img-draw"
                                                         src="/images/icon-stat/result/1.png">
                                                <?php } else { ?>
                                                    -
                                                <?php } ?>
                                            </td>
                                            <td class="text-center tr-result">
                                                <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->homeScore > $value->awayScore) or ($matchView->teamAwayEn == $value->teamAwayEn and $value->awayScore > $value->homeScore))) { ?>
                                                    <img class="guide-result img-win"
                                                         src="/images/icon-stat/result/0.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamAwayEn == $value->teamAwayEn and $value->homeScore > $value->awayScore))) { ?>
                                                    <img class="guide-result img-lose"
                                                         src="/images/icon-stat/result/2.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                    <img class="guide-result img-draw"
                                                         src="/images/icon-stat/result/1.png">
                                                <?php } else { ?>
                                                    -
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="formguideAAway">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th class="text-center mini-col">HDP</th>
                                    <th class="text-center mini-col">Odds</th>
                                    <th class="text-center mini-col">Result</th>
                                </tr>
                                </thead>
                                <?php foreach ($dataFormGuideAway as $key => $value) { ?>
                                    <?php if ($matchView->teamAwayEn == $value->teamAwayEn) { ?>
                                        <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                            id="guide-row-original"
                                            class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                            <td>
                                                <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                <p><i class="fa fa-calendar"
                                                      aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                    | <i
                                                        class="fa fa-clock-o"
                                                        aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                </p>
                                            </td>
                                            <td class="text-right box-team-state">
                                                <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                <img src="<?= $value->teamHomePath ?>">
                                            </td>
                                            <td>
                                                <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                    - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                            </td>
                                            <td class="text-left box-team-state">
                                                <img src="<?= $value->teamAwayPath ?>">
                                                <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                            </td>
                                            <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                            <td class="text-center tr-odd">
                                                <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamAwayEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                    <img class="guide-result img-win"
                                                         src="/images/icon-stat/result/0.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamAwayEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                    <img class="guide-result img-lose"
                                                         src="/images/icon-stat/result/2.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                    <img class="guide-result img-draw"
                                                         src="/images/icon-stat/result/1.png">
                                                <?php } else { ?>
                                                    -
                                                <?php } ?>
                                            </td>
                                            <td class="text-center tr-result">
                                                <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->homeScore > $value->awayScore) or ($matchView->teamAwayEn == $value->teamAwayEn and $value->awayScore > $value->homeScore))) { ?>
                                                    <img class="guide-result img-win"
                                                         src="/images/icon-stat/result/0.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamAwayEn == $value->teamAwayEn and $value->homeScore > $value->awayScore))) { ?>
                                                    <img class="guide-result img-lose"
                                                         src="/images/icon-stat/result/2.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                    <img class="guide-result img-draw"
                                                         src="/images/icon-stat/result/1.png">
                                                <?php } else { ?>
                                                    -
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div style="clear: both"></div>
</div>

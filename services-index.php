<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 7/3/2018
 * Time: 10:51 AM
 */
require_once $_SERVER["DOCUMENT_ROOT"] ."/model/HomeM.php";
//require_once $_SERVER["DOCUMENT_ROOT"] ."/model/playBet.php";
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/GalleryM.php';
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
$homem = new HomeM();
//$playBet=new playBet();
$GalleryObject = new GalleryM();
$playMatch = new playMatch();
$dataReturn=array();
$match=$playMatch->getFirstMathStatusBet(30);
foreach ($match as $key=>$value){
    $percentBet= $playMatch->percentBet($value->id);
    $dataReturn["p-match-away-".$value->id]= number_format($percentBet['pAway'], 2);
    $dataReturn["p-match-home-".$value->id]= number_format($percentBet['pHome'], 2);
    $dataReturn["progress-match-away-".$value->id]= number_format($percentBet['pAway'], 2);
    $dataReturn["progress-match-home-".$value->id]= number_format($percentBet['pHome'], 2);
    $userListHome = $playMatch->getUserBet($value->id,'home',5);
    foreach ($userListHome as $keyH=>$valueH){
        if(!empty($valueH->fb_uid)) {
            $dataReturn["user-match-home-" . $value->id . "-" . $keyH] = "https://graph.facebook.com/v2.8/" . $valueH->fb_uid . "/picture";
        }
    }
    $userListAway = $playMatch->getUserBet($value->id,'away',5);
    foreach ($userListAway as $keyA=>$valueA){
        if(!empty($valueA->fb_uid)) {
            $dataReturn["user-match-away-" . $value->id . "-" . $keyA] = "https://graph.facebook.com/v2.8/" . $valueA->fb_uid . "/picture";
        }
    }
}
$popularnews = $homem->getMostPopularNews();
foreach ($popularnews as $key=>$value){
    $dataReturn["popularnews-".$value->newsid]=$value->readCount;
}
$dataGalleryAll = $GalleryObject->getAllGalleryPicture(0, 5);
foreach ($dataGalleryAll as $key=>$value){
    $dataReturn["gallery-sexy-".$value->id]=$value->view;
}
echo json_encode($dataReturn);
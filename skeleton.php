<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">

    <!--js-->


</head>
<body>
<nav class="navbar navbar-inverse navbar-top navbar-topbar">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">SPORTBALL</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="#">หน้าหลัก</a></li>
                <li><a href="#">ไฮไลท์บอล</a></li>
                <li><a href="#">ข่าวบอลตางประเทศ</a></li>
                <li><a href="#">ข่าวบอลไทย</a></li>
                <li><a href="#">livescore</a></li>
                <li><a href="#">ตารางบอล</a></li>
                <li><a href="#">วิเคราะห์บอล</a></li>
                <li><a href="#">เกมทายผลบอล</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if (isset($_SESSION['uid'])) { ?>
                    <li>
                        <a href="#" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <img style="width: 20px"
                                 src="https://graph.facebook.com/<?php echo $_SESSION['fb_uid'] ?>/picture">
                            <?php echo $_SESSION['fb_firstname'] ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">ProFile</a></li>
                            <!--                            <li><a href="#">Another action</a></li>-->
                            <!--                            <li><a href="#">Something else here</a></li>-->
                            <li role="separator" class="divider"></li>
                            <li><a href="/logout.php">logout</a></li>
                        </ul>
                    </li>
                <?php } else { ?>
                    <button id="loginbutton"><img style="margin-top: 2px;" src="/images/icon/login-face180.png">
                    </button>
                <?php } ?>
                <!--                <li><a href="#">สมัครสมาชิก</a></li>-->
                <!--                <li><a href="#">เข้าสู่ระบบ</a></li>-->
            </ul>
        </div>
        <!--/.navbar-collapse -->
    </div>
</nav>
<div class="nav-second-menu">
    <div class="container">
        <div class="col-sm-12">
            <div class="bx-headtop rows">
                <div class="col-sm-4" style="padding-left: 0px;"><h1><strong><i class="fa fa-futbol-o"
                                                                                aria-hidden="true"></i>
                            SPORTBALL</strong></h1></div>
                <div class="col-sm-6 text-center"><input class="form-control" style="width: 75%;"
                                                         placeholder="ค้นหา ข่าว ไฮไลท์ etc."></div>
                <div class="col-sm-2">
                    <button class="btn btn-purple">เกมทายผล</button>
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="wrap-box-livescore">
                <div id="liveScoreHead" style="width: 100%;overflow: hidden;height: 60px">
                    <ul class="header-game" style="margin: 0px;width: 100%">
                        <li class="" mid="0" style="width: 200px;float: left;">
                            <div class="box-livescore">
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td><img class="header-home-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-home-name"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><img class="header-guest-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-guest-name"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table>
                                                <td>
                                                    <strong style="text-align: left;" class="header-game-time"
                                                            data-datetime="" data-dateformat="hh:ii:ss"></strong>
                                                </td>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </li>
                        <li class="" mid="0" style="width: 200px;float: left;">
                            <div class="box-livescore">
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td><img class="header-home-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-home-name"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><img class="header-guest-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-guest-name"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table>
                                                <td>
                                                    <strong class="header-game-time" data-datetime=""
                                                            data-dateformat="hh:ii:ss"></strong>
                                                </td>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </li>
                        <li class="" mid="0" style="width: 200px;float: left;">
                            <div class="box-livescore">
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td><img class="header-home-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-home-name"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><img class="header-guest-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-guest-name"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table>
                                                <td>
                                                    <strong class="header-game-time" data-datetime=""
                                                            data-dateformat="hh:ii:ss"></strong>
                                                </td>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </li>
                        <li class="" mid="0" style="width: 200px;float: left;">
                            <div class="box-livescore">
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td><img class="header-home-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-home-name"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><img class="header-guest-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-guest-name"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table>
                                                <td>
                                                    <strong class="header-game-time" data-datetime=""
                                                            data-dateformat="hh:ii:ss"></strong>
                                                </td>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </li>
                        <li class="" mid="0" style="width: 200px;float: left;">
                            <div class="box-livescore">
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td><img class="header-home-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-home-name"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><img class="header-guest-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-guest-name"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table>
                                                <td>
                                                    <strong class="header-game-time" data-datetime=""
                                                            data-dateformat="hh:ii:ss"></strong>
                                                </td>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </li>
                        <li class="" mid="0" style="width: 200px;float: left;">
                            <div class="box-livescore">
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td><img class="header-home-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-home-name"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><img class="header-guest-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-guest-name"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table>
                                                <td>
                                                    <strong class="header-game-time" data-datetime=""
                                                            data-dateformat="hh:ii:ss"></strong>
                                                </td>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </li>
                        <li class="" mid="0" style="width: 200px;float: left;">
                            <div class="box-livescore">
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td><img class="header-home-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-home-name"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><img class="header-guest-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-guest-name"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table>
                                                <td>
                                                    <strong class="header-game-time" data-datetime=""
                                                            data-dateformat="hh:ii:ss"></strong>
                                                </td>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </li>
                        <li class="" mid="0" style="width: 200px;float: left;">
                            <div class="box-livescore">
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td><img class="header-home-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-home-name"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><img class="header-guest-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-guest-name"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table>
                                                <td>
                                                    <strong class="header-game-time" data-datetime=""
                                                            data-dateformat="hh:ii:ss"></strong>
                                                </td>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </li>
                        <li class="" mid="0" style="width: 200px;float: left;">
                            <div class="box-livescore">
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td><img class="header-home-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-home-name"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><img class="header-guest-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-guest-name"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table>
                                                <td>
                                                    <strong class="header-game-time" data-datetime=""
                                                            data-dateformat="hh:ii:ss"></strong>
                                                </td>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </li>
                        <li class="" mid="0" style="width: 200px;float: left;">
                            <div class="box-livescore">
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td><img class="header-home-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-home-name"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><img class="header-guest-logo" onerror="imgError(this);"
                                                             src="http://api.ssporting.com/teams_clean/team_default_32x32.png">
                                                    </td>
                                                    <td><span class="pull-left header-guest-name"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table>
                                                <td>
                                                    <strong class="header-game-time" data-datetime=""
                                                            data-dateformat="hh:ii:ss"></strong>
                                                </td>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--                <div style="clear: both;"></div>-->
            </div>

            <div class="nav-menutop">
                <ul class="nav nav-pills">
                    <li><a href="">หน้าหลัก</a></li>
                    <li><a href="">ไฮไลท์บอล</a></li>
                    <li><a href="">ข่าวบอลต่างประเทศ</a></li>
                    <li><a href="">ข่าวบอลไทย</a></li>
                    <li><a href="">ข่าวกีฬาอื่นๆ</a></li>
                </ul>
            </div>
            <div class="sport-bar">

                <marquee id="hotnews-header">How Alibaba Turns Wealthy Shopaholics Into a Marketing Squad | Yellen
                    Signals Fed Won’t Be
                    Cowed After Trump’s Election Win
                </marquee>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="wrap-content-cols">
        <div class="col-sm-6 recommend-news" recommend="1" newsid="0">
            <div class="bx-story-big">
                <div class="hilight-cover"></div>
                <div class="crop imgTest1"></div>
                <div class="bx-story-content">
                    <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long">4 hours ago</span></div>
                    <div class="h-content">กด Like เลย! นายใหญ่ "ออสซี่" ซูฮก "ช้างศึก" เล่นทีมตนซะหืดจับ</div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="rows">
                <div class="col-sm-6 recommend-news" recommend="2" newsid="0">
                    <div class="bx-story-small">
                        <div class="hilight-cover"></div>
                        <div class="crop"></div>
                        <div class="bx-story-content">
                            <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long">4 hours ago</span></div>
                            <div class="h-content">หนึ่งแต้มแด่พ่อหลวง! "ซิโก้" เสียดายสู้เต็มที่แต่เก็บชัยไม่ได้</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 recommend-news" recommend="3" newsid="0">
                    <div class="bx-story-small">
                        <div class="hilight-cover"></div>
                        <div class="crop imgTest1"></div>
                        <div class="bx-story-content">
                            <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long">4 hours ago</span></div>
                            <div class="h-content">หนึ่งแต้มแด่พ่อหลวง! "ซิโก้" เสียดายสู้เต็มที่แต่เก็บชัยไม่ได้</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 recommend-news" recommend="4" newsid="0">
                    <div class="bx-story-small">
                        <div class="hilight-cover"></div>
                        <div class="crop imgTest3"></div>
                        <div class="bx-story-content">
                            <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long">4 hours ago</span></div>
                            <div class="h-content">หนึ่งแต้มแด่พ่อหลวง! "ซิโก้" เสียดายสู้เต็มที่แต่เก็บชัยไม่ได้</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 recommend-news" recommend="5" newsid="0">
                    <div class="bx-story-small">
                        <div class="hilight-cover"></div>
                        <div class="crop"></div>
                        <div class="bx-story-content">
                            <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long">4 hours ago</span></div>
                            <div class="h-content">หนึ่งแต้มแด่พ่อหลวง! "ซิโก้" เสียดายสู้เต็มที่แต่เก็บชัยไม่ได้</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>

    <div class="wrap-content-cols">
        <div class="rows">
            <div class="wrap-box-most-popular col-sm-2">
                <h2>Most Popular</h2>

                <div class="box-popular" newsid="">
                    <span class="box-popular-title">"ช้างศึก" ลัดฟ้าสู่ฟิลิปปินส์, "ซิโก้" ย้ำต้องแชมป์เท่านั้น!</span>

                    <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long">4 hours ago</span>
                    </div>
                    <img src="/images/5.jpg">
                </div>
                <div class="box-popular" newsid="">
                    <span class="box-popular-title">"ช้างศึก" ลัดฟ้าสู่ฟิลิปปินส์, "ซิโก้" ย้ำต้องแชมป์เท่านั้น!</span>

                    <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long">4 hours ago</span>
                    </div>
                    <img src="/images/4.jpg">
                </div>
            </div>


            <div class="col-sm-10">
                <div class="rows">
                    <div class="col-sm-4 lastest-news" newsid="">
                        <div class="bx-story-medium">
                            <div class="hilight-cover"></div>
                            <div class="crop"></div>
                            <div class="bx-story-content">
                                <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long">4 hours ago</span></div>
                                <div class="h-content">หนึ่งแต้มแด่พ่อหลวง! "ซิโก้" เสียดายสู้เต็มที่แต่เก็บชัยไม่ได้
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 lastest-news" newsid="">
                        <div class="bx-story-medium">
                            <div class="hilight-cover"></div>
                            <div class="crop imgTest2"></div>
                            <div class="bx-story-content">
                                <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long">4 hours ago</span></div>
                                <div class="h-content">หนึ่งแต้มแด่พ่อหลวง! "ซิโก้" เสียดายสู้เต็มที่แต่เก็บชัยไม่ได้
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 lastest-news" newsid="">
                        <div class="bx-story-medium">
                            <div class="hilight-cover"></div>
                            <div class="crop imgTest1"></div>
                            <div class="bx-story-content">
                                <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long">4 hours ago</span></div>
                                <div class="h-content">หนึ่งแต้มแด่พ่อหลวง! "ซิโก้" เสียดายสู้เต็มที่แต่เก็บชัยไม่ได้
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="rows">
                    <div class="col-sm-6 lastest-news" newsid="">
                        <div class="bx-story-medium">
                            <div class="hilight-cover"></div>
                            <div class="crop imgTest3"></div>
                            <div class="bx-story-content">
                                <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long">4 hours ago</span></div>
                                <div class="h-content">#ทีมไทยทั้งทวีป!.. คอมเม้นท์แฟนบอลทั่วเอเชียเป็นเสียงเดียวกัน
                                    "ช้างศึกน่าชนะจิงโจ้
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 lastest-news" newsid="">
                        <div class="bx-story-medium">
                            <div class="hilight-cover"></div>
                            <div class="crop imgTest2"></div>
                            <div class="bx-story-content">
                                <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long">4 hours ago</span></div>
                                <div class="h-content">หนึ่งแต้มแด่พ่อหลวง! "ซิโก้" เสียดายสู้เต็มที่แต่เก็บชัยไม่ได้
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both;"></div>
        </div>
    </div>

    <div class="wrap-content-cols">
        <div class="wrap-box-videos">
            <div id="owl-demo" class="owl-carousel owl-theme">
                <div class="new-videos" vid="0" url="">
                    <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก 1 ฤดูกาล</div>
                    <div class="box-videos">
                        <div class="hilight-cover"></div>
                        <div class="icon-play"><i class="fa fa-play-circle-o"></i></div>
                        <img src="/images/7.jpg">
                    </div>
                </div>
                <div class="new-videos" vid="0" url="">
                    <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก 1 ฤดูกาล</div>
                    <div class="box-videos">
                        <div class="hilight-cover"></div>
                        <div class="icon-play"><i class="fa fa-play-circle-o"></i></div>
                        <img src="/images/3.jpg">
                    </div>
                </div>
                <div class="new-videos" vid="0" url="">
                    <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก 1 ฤดูกาล</div>
                    <div class="box-videos">
                        <div class="hilight-cover"></div>
                        <div class="icon-play"><i class="fa fa-play-circle-o"></i></div>
                        <img src="/images/5.jpg">
                    </div>
                </div>
                <div class="new-videos" vid="0" url="">
                    <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก 1 ฤดูกาล</div>
                    <div class="box-videos">
                        <div class="hilight-cover"></div>
                        <div class="icon-play"><i class="fa fa-play-circle-o"></i></div>
                        <img src="/images/6.jpg">
                    </div>
                </div>
                <div class="new-videos" vid="0" url="">
                    <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก 1 ฤดูกาล</div>
                    <div class="box-videos">
                        <div class="hilight-cover"></div>
                        <div class="icon-play"><i class="fa fa-play-circle-o"></i></div>
                        <img src="/images/7.jpg">
                    </div>
                </div>
                <div class="new-videos" vid="0" url="">
                    <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก 1 ฤดูกาล</div>
                    <div class="box-videos">
                        <div class="hilight-cover"></div>
                        <div class="icon-play"><i class="fa fa-play-circle-o"></i></div>
                        <img src="/images/7.jpg">
                    </div>
                </div>
                <div class="new-videos" vid="0" url="">
                    <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก 1 ฤดูกาล</div>
                    <div class="box-videos">
                        <div class="hilight-cover"></div>
                        <div class="icon-play"><i class="fa fa-play-circle-o"></i></div>
                        <img src="/images/7.jpg">
                    </div>
                </div>
                <div class="new-videos" vid="0" url="">
                    <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก 1 ฤดูกาล</div>
                    <div class="box-videos">
                        <div class="hilight-cover"></div>
                        <div class="icon-play"><i class="fa fa-play-circle-o"></i></div>
                        <img src="/images/7.jpg">
                    </div>
                </div>
            </div>

            <div style="clear: both;"></div>
        </div>
    </div>

    <div class="wrap-content-cols hide">
        <div class="rows">
            <div class="col-sm-1">
                <div class="label-vdo">Videos</div>
            </div>
            <div class="col-sm-11">
                <div class="box-video">
                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/zoHSe5TLmNE" frameborder="0"
                            allowfullscreen></iframe>
                </div>
            </div>
            <div style="clear: both;"></div>
        </div>
    </div>

    <div class="wrap-content-cols hide">
        <div class="rows">
            <div class="col-sm-3">
                <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก 1 ฤดูกาล</div>
                <div class="box-videos">
                    <div class="hilight-cover"></div>
                    <div class="icon-play"><i class="fa fa-play-circle-o"></i></div>
                    <img src="/images/1.jpg">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก 1 ฤดูกาล</div>
                <div class="box-videos">
                    <div class="hilight-cover"></div>
                    <div class="icon-play"><i class="fa fa-play-circle-o"></i></div>
                    <img src="/images/7.jpg">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก 1 ฤดูกาล</div>
                <div class="box-videos">
                    <div class="hilight-cover"></div>
                    <div class="icon-play"><i class="fa fa-play-circle-o"></i></div>
                    <img src="/images/3.jpg">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก 1 ฤดูกาล</div>
                <div class="box-videos">
                    <div class="hilight-cover"></div>
                    <div class="icon-play"><i class="fa fa-play-circle-o"></i></div>
                    <img src="/images/7.jpg">
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>


    <div class="wrap-content-cols hide">
        <div class="wrap-box-news rows">
            <h2>ข่าวต่างประเทศ</h2>

            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-tabs-league" role="tablist">
                    <li role="presentation" class="active"><a href="#Bpl" aria-controls="home" role="tab"
                                                              data-toggle="tab"><img
                                src="/images/five-leagues/English-Premier-League.png"> Premier League</a></li>
                    <li role="presentation"><a href="#Laliga" aria-controls="profile" role="tab" data-toggle="tab"><img
                                src="/images/five-leagues/La-Liga-Logo-1.png"> La Liga</a></li>
                    <li role="presentation"><a href="#Bundesliga" aria-controls="messages" role="tab" data-toggle="tab"><img
                                src="/images/five-leagues/logo_bl.gif"> Bundes Liga</a></li>
                    <li role="presentation"><a href="#Seriea" aria-controls="settings" role="tab" data-toggle="tab"><img
                                src="/images/five-leagues/seriea1.png"> Serie A</a></li>
                    <li role="presentation"><a href="#League1" aria-controls="settings" role="tab"
                                               data-toggle="tab"><img
                                src="/images/five-leagues/Ligue-1-logo-france.png"> League 1</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="Bpl">
                        <div class="wrap-box-content-news">
                            <div class="box-other-news">
                                <div class="rows">
                                    <div class="col-sm-4">
                                        <div class="tab-color"></div>
                                        <div class="times-content"><i class="fa fa-clock-o"></i> 4 hours ago</div>
                                        <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก
                                            1 ฤดูกาล
                                        </div>
                                        <div class="box-videos">
                                            <div class="hilight-cover"></div>
                                            <img src="/images/1.jpg">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="tab-color"></div>
                                        <div class="times-content"><i class="fa fa-clock-o"></i> 4 hours ago</div>
                                        <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก
                                            1 ฤดูกาล
                                        </div>
                                        <div class="box-videos">
                                            <div class="hilight-cover"></div>
                                            <img src="/images/2.jpg">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="tab-color"></div>
                                        <div class="times-content"><i class="fa fa-clock-o"></i> 4 hours ago</div>
                                        <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก
                                            1 ฤดูกาล
                                        </div>
                                        <div class="box-videos">
                                            <div class="hilight-cover"></div>
                                            <img src="/images/5.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-other-news">
                                <div class="rows">
                                    <div class="col-sm-3">
                                        <div class="tab-color"></div>
                                        <div class="times-content"><i class="fa fa-clock-o"></i> 4 hours ago</div>
                                        <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก
                                            1 ฤดูกาล
                                        </div>
                                        <div class="box-videos">
                                            <div class="hilight-cover"></div>
                                            <img src="/images/5.jpg">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="tab-color"></div>
                                        <div class="times-content"><i class="fa fa-clock-o"></i> 4 hours ago</div>
                                        <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก
                                            1 ฤดูกาล
                                        </div>
                                        <div class="box-videos">
                                            <div class="hilight-cover"></div>
                                            <img src="/images/5.jpg">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="tab-color"></div>
                                        <div class="times-content"><i class="fa fa-clock-o"></i> 4 hours ago</div>
                                        <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก
                                            1 ฤดูกาล
                                        </div>
                                        <div class="box-videos">
                                            <div class="hilight-cover"></div>
                                            <img src="/images/5.jpg">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="tab-color"></div>
                                        <div class="times-content"><i class="fa fa-clock-o"></i> 4 hours ago</div>
                                        <div class="title-owl-carousel">ตามความฝัน! "กิเลน" จ่อปล่อย "ชนาธิป" เล่นเจลีก
                                            1 ฤดูกาล
                                        </div>
                                        <div class="box-videos">
                                            <div class="hilight-cover"></div>
                                            <img src="/images/5.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="Laliga">...</div>
                    <div role="tabpanel" class="tab-pane" id="Bundesliga">...</div>
                    <div role="tabpanel" class="tab-pane" id="Seriea">...</div>
                    <div role="tabpanel" class="tab-pane" id="League1">...</div>
                </div>

            </div>

        </div>
    </div>

    <div style="clear:both;"></div>

    <div class="wrap-content-cols hide">
        <div class="wrap-box-results rows">
            <div class="col-sm-4">
                <div class="box-results">
                    <span>ดาวซัลโว</span>

                    <div class="rows">
                        <div class="col-sm-6">
                            <div class="bx-player-records">
                                <div class="h-records">Goals</div>
                                <div class="bx-top-records">
                                    <table>
                                        <tr>
                                            <td> 1
                                                <div>Alan Shearer</div>
                                                <div class="txt-point">250</span></div>
                                            </td>
                                            <td class="text-right"><img src="/images/p1.png"></td>
                                        </tr>
                                    </table>
                                </div>
                                <table class="table-list-othertop">
                                    <tr>
                                        <td>2.</td>
                                        <td>Wayne Rooney</td>
                                        <td>194</td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>Andrew Cole</td>
                                        <td>194</td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
                                        <td>Frank Lampard</td>
                                        <td>194</td>
                                    </tr>
                                    <tr>
                                        <td>5.</td>
                                        <td>Thierry Henry</td>
                                        <td>194</td>
                                    </tr>
                                    <tr>
                                        <td>6.</td>
                                        <td>Thierry Henry</td>
                                        <td>194</td>
                                    </tr>
                                </table>

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="bx-player-records">
                                <div class="bx-player-records">
                                    <div class="h-records">Apperances</div>
                                    <div class="bx-top-records">
                                        <table>
                                            <tr>
                                                <td> 1
                                                    <div>Alan Shearer</div>
                                                    <div class="txt-point">250</span></div>
                                                </td>
                                                <td class="text-right"><img src="/images/p1.png"></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <table class="table-list-othertop">
                                        <tr>
                                            <td>2.</td>
                                            <td>Wayne Rooney</td>
                                            <td>194</td>
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td>Andrew Cole</td>
                                            <td>194</td>
                                        </tr>
                                        <tr>
                                            <td>4.</td>
                                            <td>Frank Lampard</td>
                                            <td>194</td>
                                        </tr>
                                        <tr>
                                            <td>5.</td>
                                            <td>Thierry Henry</td>
                                            <td>194</td>
                                        </tr>
                                        <tr>
                                            <td>6.</td>
                                            <td>Thierry Henry</td>
                                            <td>194</td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-4">
                <div class="box-results">
                    <span>ตารางแข่งขัน</span>
                    <table class="table-live">
                        <tr>
                            <td>Manchester United <img src="/images/logo-team/manchester.png"></td>
                            <td><strong>20:34</strong></td>
                            <td><img src="/images/logo-team/Chelsea.png"> Chelsea</td>
                        </tr>
                        <tr>
                            <td>Manchester United <img src="/images/logo-team/manchester.png"></td>
                            <td><strong>20:34</strong></td>
                            <td><img src="/images/logo-team/Chelsea.png"> Chelsea</td>
                        </tr>
                        <tr>
                            <td>Manchester United <img src="/images/logo-team/manchester.png"></td>
                            <td><strong>20:34</strong></td>
                            <td><img src="/images/logo-team/Chelsea.png"> Chelsea</td>
                        </tr>
                        <tr>
                            <td>Manchester United <img src="/images/logo-team/manchester.png"></td>
                            <td><strong>20:34</strong></td>
                            <td><img src="/images/logo-team/Chelsea.png"> Chelsea</td>
                        </tr>
                        <tr>
                            <td>Manchester United <img src="/images/logo-team/manchester.png"></td>
                            <td><strong>20:34</strong></td>
                            <td><img src="/images/logo-team/Chelsea.png"> Chelsea</td>
                        </tr>
                        <tr>
                            <td>Manchester United <img src="/images/logo-team/manchester.png"></td>
                            <td><strong>20:34</strong></td>
                            <td><img src="/images/logo-team/Chelsea.png"> Chelsea</td>
                        </tr>
                        <tr>
                            <td>Manchester United <img src="/images/logo-team/manchester.png"></td>
                            <td><strong>20:34</strong></td>
                            <td><img src="/images/logo-team/Chelsea.png"> Chelsea</td>
                        </tr>
                        <tr>
                            <td>Manchester United <img src="/images/logo-team/manchester.png"></td>
                            <td><strong>20:34</strong></td>
                            <td><img src="/images/logo-team/Chelsea.png"> Chelsea</td>
                        </tr>
                        <tr>
                            <td>Manchester United <img src="/images/logo-team/manchester.png"></td>
                            <td><strong>20:34</strong></td>
                            <td><img src="/images/logo-team/Chelsea.png"> Chelsea</td>
                        </tr>
                        <tr>
                            <td>Manchester United <img src="/images/logo-team/manchester.png"></td>
                            <td><strong>20:34</strong></td>
                            <td><img src="/images/logo-team/Chelsea.png"> Chelsea</td>
                        </tr>

                    </table>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="box-results">
                    <span>ตารางคะแนน</span>
                    <table class="table-point">
                        <tr>
                            <th>Pos</th>
                            <th>Club</th>
                            <th>Pl</th>
                            <th>GD</th>
                            <th>Pts</th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td><img src="/images/logo-team/Chelsea.png"> Chelsea</td>
                            <td>9</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td><img src="/images/logo-team/manchester.png"> Manchester</td>
                            <td>9</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td><img src="/images/logo-team/manchester.png"> Manchester</td>
                            <td>9</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td><img src="/images/logo-team/manchester.png"> Manchester</td>
                            <td>9</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td><img src="/images/logo-team/manchester.png"> Manchester</td>
                            <td>9</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td><img src="/images/logo-team/manchester.png"> Manchester</td>
                            <td>9</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td><img src="/images/logo-team/Chelsea.png"> Chelsea</td>
                            <td>9</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td><img src="/images/logo-team/Chelsea.png"> Chelsea</td>
                            <td>9</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td><img src="/images/logo-team/Chelsea.png"> Chelsea</td>
                            <td>9</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td><img src="/images/logo-team/Chelsea.png"> Chelsea</td>
                            <td>9</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
    </div>


</div>

<script src="/js/jquery/dist/jquery.min.js"></script>
<script src="/js/jquery-ui/jquery-ui.min.js"></script>
<script src="/js/flogin/src/flogin.js"></script>
<script src="/css/bootstrap/js/bootstrap.min.js"></script>
<script src="js/jquery.formatDateTime/dist/jquery.formatDateTime.min.js"></script>
<script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
<script src="/js/sly/sly.js"></script>
<script src="js/jquery-timeago/jquery.timeago.js"></script>

<style>
    li.active {
        background-color: rgba(0, 0, 0, 0.1);
    }
</style>

<script>

    $(document).ready(function () {

//        $('#loginbutton').facebook_login({
//            appId: '400809029976308',   //# your facebook application id
//            endpoint: '/createSessions.php',  //# where to POST the response to
//            onSuccess: function (data) {
//                location.reload();
//            },  //# what to do on success, usually redirect
//            onError: function (data) {
//            }, //# what to do on error
//            permissions: 'read_stream'  //# what permissions you need, default is just email
//        });

//        $("#owl-demo").owlCarousel({
//
//            loop: true,
//            items: 6,
//            navigation: true, // Show next and prev buttons
//            slideSpeed: 300,
//            paginationSpeed: 400,
//            singleItem: true,
//            margin: 25,
//            responsiveClass: true,
//            responsive: {
//                350: {
//                    items: 1,
//                    nav: true
//                },
//                600: {
//                    items: 3,
//                    nav: false
//                },
//                1000: {
//                    items: 3,
//                    nav: true,
//                    loop: false
//                }
//            }
//
//            // "singleItem:true" is a shortcut for:
//            // items : 1,
//            // itemsDesktop : false,
//            // itemsDesktopSmall : false,
//            // itemsTablet: false,
//            // itemsMobile : false
//
//        });

//        $(document).ready(function () {
//
//            var owl = $("#owl-demo2");
//
//            owl.owlCarousel({
//                items: 6, //10 items above 1000px browser width
////                itemsDesktop : [1000,5], //5 items between 1000px and 901px
////                itemsDesktopSmall : [900,3], // betweem 900px and 601px
////                itemsTablet: [600,2], //2 items between 600 and 0
////                itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
//                responsiveClass: true,
//                responsive: {
//                    350: {
//                        items: 1,
//                        nav: true
//                    },
//                    600: {
//                        items: 3,
//                        nav: false
//                    },
//                    1000: {
//                        items: 6,
//                        nav: true,
//                        loop: false
//                    }
//                }
//            });
//
//        });
//
    });

</script>
<script>

    function imgError(image) {
        image.onerror = "";
        image.src = "http://api.ssporting.com/teams_clean/team_default_32x32.png";
        return true;
    }

    //    $(document).ready(function () {
    //        var options = {
    //            horizontal: 1,
    //            itemNav: 'basic',
    //            smart: 1,
    //            activateOn: 'click',
    //            mouseDragging: 1,
    //            touchDragging: 1,
    //            releaseSwing: 1,
    //            startAt: 0,
    ////            scrollBar: $wrap.find('.scrollbar'),
    //            scrollBy: 1,
    //            speed: 1000,
    //            elasticBounds: 1,
    //            easing: 'easeOutExpo',
    //            dragHandle: 1,
    //            dynamicHandle: 1,
    //            clickBar: 1,
    //
    //            // Cycling
    //            cycleBy: 'items',
    //            cycleInterval: 2000,
    //            pauseOnHover: 1,
    //        }
    //
    //        $('#liveScoreHead').sly(options);
    //    })
</script>

<script src="js/indexinit.js"></script>

</body>
</html>
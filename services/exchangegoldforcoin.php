<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 15/2/2560
 * Time: 16:13 น.
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/MatchM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/UserM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/DataMap.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/StatementM.php";

$dt = \Carbon\Carbon::now();
$fb_uid = $_REQUEST['fb_uid'];
$gold = $_REQUEST['gold'];


$matchm = new MatchM();
$userm = new UserM();
$datamap = new DataMap();
$statem = new StatementM();
$result = array('success' => false, 'desc' => 'nothing happen', 'insert_result' => 0, 'update_balance_rows' => 0, 'statement' => 0);
$config = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/system_config/file.json'), true);

$user = $userm->getUserPack($fb_uid);
$thisuser = $datamap->mapByKey($user['keylist']['user'], $user['user']);


$before_sgold = $thisuser['sgold'];
$before_scoin = $thisuser['scoin'];
$before_diamond = $thisuser['diamond'];
$sgold_income = 0;
$scoin_income = $gold * 100;
$scoin_outcome = 0;
$diamond_outcome = 0;
$sgold_outcome = $gold;
$balance_sgold = $thisuser['sgold'] - $gold;
$balance_scoin = $before_scoin + $scoin_income;

if ($thisuser['sgold'] >= $gold) {
    $step_bonus = $config['step_combo_continues'];
    $diamond_income = 0;
    $balance_diamond = $before_diamond + $diamond_income;
    $stmid = $statem->addStatement($thisuser['uid'], $thisuser['fb_uid'], 'exchange', 'sgold_to_scoin', $before_sgold, $before_scoin, $sgold_income, $scoin_income, $sgold_outcome, $scoin_outcome, $balance_sgold, $balance_scoin, 'success', "NULL", "NULL", '', $before_diamond, $diamond_income, $diamond_outcome, $balance_diamond);
    $result['statement'] = $stmid;
    if ($stmid > 0) {
        $afrow = $userm->updateXVal($thisuser['fb_uid'], 'scoin', $balance_scoin);
        $result['update_balance_rows'] = $afrow;
        if ($afrow > 0) {
            $updated = $userm->updateXVal($thisuser['fb_uid'], 'sgold', $balance_sgold);
            if ($updated > 0) {
                $result['success'] = true;
                $result['desc'] = 'แลกสำเร็จ ได้รับ ' . $scoin_income . ' coin';
            }
        }
    }
} else {
    $result['success'] = false;
    $result['desc'] = 'เหรียญทองคุณมีไม่พอ';
}

echo json_encode($result);
<?php
require_once __DIR__ . "/../../bootstrap.php";
$gid = $_REQUEST['gid'];

$mcon = new \controller\MediaController();
$rs = $mcon->getMedia($gid);

header("Content-Type: application/json");
echo json_encode($rs);

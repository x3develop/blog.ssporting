<?php
require_once __DIR__ . "/../../bootstrap.php";
$page = empty($_REQUEST['page']) ? 0 : $_REQUEST['page'];
$type = empty($_REQUEST['type']) ? 'all' : $_REQUEST['type'];
$mcon = new \controller\MediaController();
$page *= 20;
$rs = $mcon->getAllSexy($page, $type);

header("Content-Type: application/json");
echo json_encode($rs);

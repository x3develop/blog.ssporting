<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 15/2/2560
 * Time: 16:13 น.
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/MatchM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/UserM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/DataMap.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/StatementM.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/MatchCommentM.php';

$dt = \Carbon\Carbon::now();
$hid = $_REQUEST['hid'];
$gid = $_REQUEST['gid'];
$leagueId = $_REQUEST['lid'];
$kickOff = $_REQUEST['c0'];
$betDatetime = $dt->timestamp;
$betValue = $_REQUEST['betval'];
$choose = $_REQUEST['side'];
$betHdp = $_REQUEST['hdp'];
$mid = $_REQUEST['mid'];
$fb_uid = $_REQUEST['fb_uid'];
$status_id = $_REQUEST['sid'];
$bet_sgold = (!empty($_REQUEST['amount'])) ? $_REQUEST['amount'] : 100;
$oddstype = $_REQUEST['oddstype'];
$msg = $_REQUEST['msg'];

$gamestart = \Carbon\Carbon::createFromTimestamp($kickOff);
$now = \Carbon\Carbon::now();

$matchm = new MatchM();
$userm = new UserM();
$datamap = new DataMap();
$statem = new StatementM();
$commentm = new MatchCommentM();
$result = array('success' => false, 'desc' => 'nothing happen', 'insert_result' => 0, 'update_balance_rows' => 0, 'statement' => 0, 'message_id' => 0);
if ($now->diffInMinutes($gamestart,false) > 0) {
    if (is_numeric($betHdp)) {
        $user = $userm->getUserPack($fb_uid);
        $thisuser = $datamap->mapByKey($user['keylist']['user'], $user['user']);
        if ($thisuser['scoin'] > $bet_sgold) {
            $result['insert_result'] = $matchm->bet($oddstype, $hid, $gid, $leagueId, $kickOff, $betDatetime, $betValue, $choose, $betHdp, $mid, $fb_uid, $status_id, $bet_sgold);
            if ($result['insert_result'] > 0) {
                $updatebl = $userm->updateCoinBalance($thisuser['fb_uid'], 'decrease', $bet_sgold);
                $result['update_balance_rows'] = $updatebl;
                if ($result['update_balance_rows'] > 0) {
                    $result['statement'] = $statem->addStatement($thisuser['uid'], $thisuser['fb_uid'], 'ball_game', null, $thisuser['sgold'], $thisuser['scoin'], 0, 0, 0, $bet_sgold, $thisuser['sgold'], $thisuser['scoin'] - $bet_sgold, 'wait', $result['insert_result']);
                    $result['success'] = true;
                    $result['desc'] = "well done";
                }
                if (!empty($msg)) {
                    $result['message_id'] = $commentm->addComment($mid, $fb_uid, 0, $choose, $msg);
                }
            }
        }else{
            $result['desc'] = "Don't have enough coin.";
        }
    } else {
        $result['desc'] = "Can't play without hdp.";
    }
} else {
    $result['desc'] = "Time's up.";
}

echo json_encode($result);
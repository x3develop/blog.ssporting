<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 2/2/2017
 * Time: 17:10
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/model/Statistics.php";

$mid = ($_REQUEST["mid"]) ? $_REQUEST['mid'] : 0;
$type = ($_REQUEST["mid"]) ? $_REQUEST['type'] : 'game';
$date = ($_REQUEST["mid"]) ? $_REQUEST['date'] : '2000-01-01';

$statistics = new Statistics();
$history=$statistics->getGameHistory($mid,$type,$date);
$total=$statistics->getTotalGameHistory($mid,$type,$date);

echo json_encode(['history'=>$history,'total'=>$total]);
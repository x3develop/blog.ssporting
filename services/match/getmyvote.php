<?php
require_once __DIR__ . "/../../bootstrap.php";
$mid = $_REQUEST['mid'];
$fbid = $_REQUEST['fbid'];

$mcon = new \controller\MatchController();
$rs = $mcon->getMyVote($mid, $fbid);
header("Content-Type: application/json");
echo json_encode($rs);

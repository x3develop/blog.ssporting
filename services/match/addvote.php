<?php
require_once __DIR__ . "/../../bootstrap.php";
$mid = $_REQUEST['mid'];
$fbid = $_REQUEST['fb_uid'];
$side = $_REQUEST['side'];
$token = $_REQUEST['fb_token'];
$mcon = new \controller\MatchController();
$rs = $mcon->addVote($mid, $fbid, $side,$token);
header("Content-Type: application/json");
echo json_encode($rs);

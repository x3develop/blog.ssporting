<?php
require_once __DIR__ . "/../../bootstrap.php";
$mid = $_REQUEST['mid'];
$fbid = $_REQUEST['fb_uid'];

$mcon = new \controller\MatchController();
$rs = $mcon->getVote($mid, $fbid);
header("Content-Type: application/json");
echo json_encode($rs);

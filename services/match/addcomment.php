<?php
require_once __DIR__ . "/../../bootstrap.php";
$mid = $_REQUEST['mid'];
$fbid = $_REQUEST['fb_uid'];
$comment = $_REQUEST['comment'];
$mcon = new \controller\MatchController();
$rs = $mcon->addComment($mid, $fbid, $comment);
header("Content-Type: application/json");
echo json_encode($rs);

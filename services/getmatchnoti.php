<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 3/4/2560
 * Time: 17:47 น.
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/FileManager.php";

$fb_uid = $_REQUEST['fb_uid'];
$dt = \Carbon\Carbon::now();
$fileman = new FileManager();
$fileman->setPath("storage/match_noti");
$fileman->setFilename($fb_uid . ".json");
$result = array('data' => array(), 'fb_uid' => $fb_uid);
$data = array();
if ($fileman->isFile()) {
    $data = json_decode($fileman->read(),true);
}
$result['data'] = $data;
echo json_encode($result);
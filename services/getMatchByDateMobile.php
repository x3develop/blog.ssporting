<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 30/11/2559
 * Time: 14:09 น.
 */
include_once $_SERVER['DOCUMENT_ROOT'] . "/model/MatchM.php";

$day = ($_REQUEST['date']) ? $_REQUEST['date'] : date("Y-m-d");
$lang = ($_REQUEST['lang']) ? $_REQUEST['lang'] : "en";
$matchm = new MatchM();
$result = $matchm->getMatchPackMobile($day,$lang);
echo json_encode($result);
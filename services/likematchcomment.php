<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 15/2/2560
 * Time: 16:13 น.
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/DataMap.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/MatchCommentM.php';

$dt = \Carbon\Carbon::now();
$fb_uid = $_REQUEST['fb_uid'];
$comment_id = $_REQUEST['comment_id'];
$mid = $_REQUEST['mid'];
$status = $_REQUEST['status'];

$datamap = new DataMap();
$commentm = new MatchCommentM();
$result = array('success' => false, 'desc' => 'nothing happen', 'addlike' => 0);

$lastid = $commentm->addLike($mid, $comment_id, $fb_uid, $status);
$result['addlike'] = $lastid;
if ($lastid > 0) {
    if ($status == 'like') {
        $commentm->increaseLike($comment_id);
    } else {
        $commentm->decreaseLike($comment_id);
    }
    $result['success'] = true;
    $result['desc'] = 'done';
}
echo json_encode($result);
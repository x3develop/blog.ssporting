<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 30/11/2559
 * Time: 14:09 น.
 */
include_once $_SERVER['DOCUMENT_ROOT'] . "/model/MatchM.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
$nowday = \Carbon\Carbon::now();
$day = ($_REQUEST['date']) ? $_REQUEST['date'] : $nowday->toDateString();

$matchm = new MatchM();
$result = $matchm->getMatchPack($day);
echo json_encode($result);
<?php
require_once __DIR__ . "/../../bootstrap.php";

$mid = $_REQUEST['mid'];
$mcon = new \controller\MatchController();
$history = null;
if (!empty($mid)) {
    $history = $mcon->getHdpHistory($mid);
} else {
    header("Location: /");
}

header("Content-Type: application/json");
echo json_encode(['events' => $history]);

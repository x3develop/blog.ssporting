<?php
require_once __DIR__ . "/../../bootstrap.php";

$mid = $_REQUEST['mid'];
$mcon = new \controller\MatchController();
$events = null;
if (!empty($mid)) {
    $events = $mcon->getEvent($mid);
} else {
    header("Location: /");
}

header("Content-Type: application/json");
echo json_encode(['events' => $events]);

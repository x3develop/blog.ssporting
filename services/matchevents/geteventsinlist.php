<?php
require_once __DIR__ . "/../../bootstrap.php";

$midlist = $_REQUEST['midlist'];
$mcon = new \controller\MatchController();
$events = [];
if (!empty($midlist)) {
    foreach (explode(",", $midlist) as $mid) {
        $events[$mid] = $mcon->getEvent($mid);
    }
} else {
    header("Location: /");
}

header("Content-Type: application/json");
echo json_encode($events);

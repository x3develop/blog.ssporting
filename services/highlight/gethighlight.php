<?php
require_once __DIR__ . "/../../bootstrap.php";
$mid = $_REQUEST['mid'];
$mcon = new \controller\HighlightController();
$rs = $mcon->getHighlight($mid);
header("Content-Type: application/json");
echo json_encode($rs);

<?php
require_once __DIR__ . "/../../bootstrap.php";
$list = $_REQUEST['list'];
$mcon = new \controller\HighlightController();
$rs = $mcon->getHighlightList($list);

header("Content-Type: application/json");
echo json_encode($rs);

<?php
require_once __DIR__ . "/../../bootstrap.php";
$mid = $_REQUEST['mid'];
$action = $_REQUEST['action'];
$mcon = new \controller\HighlightController();
$rs = $mcon->updateHighlight($mid,$action);
header("Content-Type: application/json");
echo json_encode($rs);

<?php
require_once __DIR__ . "/../../bootstrap.php";
$day = $_REQUEST['day'];
$mcon = new \controller\HighlightController();
$rs = $mcon->getHighlightAll($day);

header("Content-Type: application/json");
echo json_encode($rs);

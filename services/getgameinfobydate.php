<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 1/2/2560
 * Time: 16:10 น.
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/model/MatchM.php";

$showdate = ($_REQUEST["date"]) ? $_REQUEST['date'] : "current_date";
$matchm = new MatchM();
$result = $matchm->getGameInfoPack($showdate);
echo json_encode($result);
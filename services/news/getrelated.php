<?php
require_once __DIR__ . "/../../bootstrap.php";
$id = empty($_REQUEST['id']) ? 0 : $_REQUEST['id'];

$mcon = new \controller\NewsController();
$rs = $mcon->getRelated($id);

header("Content-Type: application/json");
echo json_encode($rs);

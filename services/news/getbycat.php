<?php
require_once __DIR__ . "/../../bootstrap.php";
$page = empty($_REQUEST['page']) ? 0 : $_REQUEST['page'];
$cat = $_REQUEST['cat'];
$mcon = new \controller\NewsController();
$page*=20;
$rs = $mcon->getByCat($cat, $page);

header("Content-Type: application/json");
echo json_encode($rs);

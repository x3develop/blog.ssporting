<?php
require_once __DIR__ . "/../../bootstrap.php";
$id = empty($_REQUEST['id']) ? 0 : $_REQUEST['id'];
$ncon = new \controller\NewsController();
$news = $ncon->get($id);
$related = $ncon->getRelated($id);
$rs['news'] = $news;
$rs['related']=$related;
header("Content-Type: application/json");
echo json_encode($rs);

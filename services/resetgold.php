<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 15/2/2560
 * Time: 16:13 น.
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/MatchM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/UserM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/DataMap.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/StatementM.php";

$dt = \Carbon\Carbon::now();
$fb_uid = $_REQUEST['fb_uid'];


$matchm = new MatchM();
$userm = new UserM();
$datamap = new DataMap();
$statem = new StatementM();
$result = array('success' => false, 'desc' => 'nothing happen', 'insert_result' => 0, 'update_balance_rows' => 0, 'statement' => 0);


$user = $userm->getUserPack($fb_uid);
$thisuser = $datamap->mapByKey($user['keylist']['user'], $user['user']);


$before_sgold = $thisuser['sgold'];
$before_scoin = $thisuser['scoin'];
$before_diamond = $thisuser['diamond'];
$sgold_income = 0;
$scoin_income = 0;
$scoin_outcome = 0;
$diamond_outcome = 0;
$sgold_outcome = $before_sgold;
$balance_sgold = 0;
$balance_scoin = $before_scoin;
$diamond_income = 0;
$balance_diamond = $before_diamond;

$dt = \Carbon\Carbon::now();
$bf = \Carbon\Carbon::now();
if ($thisuser['last_reset_sgold_date'] != null) {
    $bf = \Carbon\Carbon::createFromFormat("Y-m-d", $thisuser['last_reset_sgold_date']);
}


if ($thisuser['last_reset_sgold_date'] == null || $dt->diffInDays($bf) >= 15) {
    $stmid = $statem->addStatement($thisuser['uid'], $thisuser['fb_uid'], 'reset_sgold', null, $before_sgold, $before_scoin, $sgold_income, $scoin_income, $sgold_outcome, $scoin_outcome, $balance_sgold, $balance_scoin, 'success', "NULL", "NULL", '', $before_diamond, $diamond_income, $diamond_outcome, $balance_diamond);
    $result['statement'] = $stmid;
    if ($stmid > 0) {
        $updated = $userm->updateXVal($thisuser['fb_uid'], 'sgold', 0);
        if ($updated > 0) {
            $updated = $userm->updateXVal($thisuser['fb_uid'], 'last_reset_sgold_date', $dt->format("Y-m-d"));
            $result['success'] = true;
            $result['desc'] = 'Reset done';
        }
    }
} else {
    $result['success'] = false;
    $result['desc'] = 'การรีเซ็ตสามารถทำได้ทุกๆ 15 วัน คุณรีเซ็ตล่าสุดเมื่อ ' . $bf->format("d M Y");
}


echo json_encode($result);
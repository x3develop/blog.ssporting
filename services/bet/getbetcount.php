<?php
require_once __DIR__ . "/../../bootstrap.php";

$midlist = $_REQUEST['midlist'];
$cont = new \controller\BetController();
$rs = null;

foreach (explode(",", $midlist) as $mid) {
    if (is_numeric($mid)) {
        $list = $cont->getByMatch($mid);
        $mcount = ["home" => 0, "away" => 0];
        foreach ($list as $row) {
            if ($row->team == "home") {
                $mcount['home']++;
            } else {
                $mcount['away']++;
            }
        }
        $rs[$mid] = $mcount;
    }
}

header("Content-Type: application/json");
echo json_encode($rs);

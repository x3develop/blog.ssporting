<?php
require_once __DIR__ . "/../../bootstrap.php";

$mid = $_REQUEST['mid'];
$fbid = $_REQUEST['fb_uid'];
$bcon = new \controller\BetController();
$ucon = new \controller\UserController();
$rs = null;

$user = $ucon->getByFbid($fbid);
//dd($user['user_data']->id);
$rs = $bcon->checkBetOnTeam($mid, $user['user_data']->id, false);

header("Content-Type: application/json");
echo json_encode($rs);

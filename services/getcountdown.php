<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 1/2/2560
 * Time: 16:10 น.
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";

$dt = \Carbon\Carbon::now();
$today = \Carbon\Carbon::now();
if ($dt->day <= 16) {
    $dt->day(16);
    if ($dt->hour == 12 && $dt->minute > 30) {
        $dt->addMonth(1);
        $dt->day(1);
    }
} else {
    $dt->addMonth(1);
    $dt->day(1);
    if ($dt->hour == 12 && $dt->minute > 30) {
        $dt->day(16);
    }
}
$dt->setTime(12, 30, 0);
$result['timestamp'] = $dt->timestamp;
$result['date_time'] = $dt->toDateTimeString();
$result['today_timestamp'] = $today->timestamp;
$result['today_date_time'] = $today->toDateTimeString();
echo json_encode($result);
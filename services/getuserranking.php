<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/RankM.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/utility/FileManager.php';

$offset = ($_REQUEST['offset']) ? $_REQUEST['offset'] : 0;
$limit = ($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
$rankm = new RankM();
$fileman = new FileManager();
$fileman->setPath('cache');
$fileman->setFilename('ranking_' . $offset . '_' . $limit . '.json');
if (!$fileman->isFile()) {
    $rank = $rankm->getUserRankPack($offset, $limit);
    $fileman->setContent(json_encode($rank));
    $fileman->write();
} else {
    if ($fileman->howOldInMinute() < 60) {
        $rank = json_decode($fileman->read(), true);
    } else {
        $rank = $rankm->getUserRankPack($offset, $limit);
        $fileman->setContent(json_encode($rank));
        $fileman->write();
    }
}
//print_r($matchlist);

echo json_encode($rank);

?>

<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 1/2/2560
 * Time: 16:10 น.
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/model/MatchM.php";

$mid = ($_REQUEST["mid"]) ? $_REQUEST['mid'] : 0;
$matchm = new MatchM();
$result = $matchm->getSubstitutesPack($mid);
echo json_encode($result);
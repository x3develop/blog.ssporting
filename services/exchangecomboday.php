<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 15/2/2560
 * Time: 16:13 น.
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/MatchM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/UserM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/DataMap.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/StatementM.php";

$dt = \Carbon\Carbon::now();
$fb_uid = $_REQUEST['fb_uid'];


$matchm = new MatchM();
$userm = new UserM();
$datamap = new DataMap();
$statem = new StatementM();
$result = array('success' => false, 'desc' => 'nothing happen', 'insert_result' => 0, 'update_balance_rows' => 0, 'statement' => 0);
$config = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/system_config/file.json'), true);

$user = $userm->getUserPack($fb_uid);
$thisuser = $datamap->mapByKey($user['keylist']['user'], $user['user']);


$before_sgold = $thisuser['sgold'];
$before_scoin = $thisuser['scoin'];
$before_diamond = $thisuser['diamond'];
$sgold_income = 0;
$scoin_income = 0;
$scoin_outcome = 0;
$diamond_outcome = 0;
$sgold_outcome = 0;
$balance_sgold = $before_sgold;
$balance_scoin = $before_scoin;

if ($thisuser['step_combo_today_count'] >= 3) {
    $step_bonus = $config['step_combo_per_day'];
    $diamond_income = $step_bonus[$thisuser['step_combo_today_count']];
    $balance_diamond = $before_diamond + $diamond_income;
    $stmid = $statem->addStatement($thisuser['uid'], $thisuser['fb_uid'], 'step_bonus', 'step_bonus_today', $before_sgold, $before_scoin, $sgold_income, $scoin_income, $sgold_outcome, $scoin_outcome, $balance_sgold, $balance_scoin, 'success', "NULL", "NULL", '', $before_diamond, $diamond_income, $diamond_outcome, $balance_diamond);
    $result['statement'] = $stmid;
    if ($stmid > 0) {
        $afrow = $userm->updateDiamondBalance($thisuser['fb_uid'], 'increase', $diamond_income);
        $result['update_balance_rows'] = $afrow;
        if ($afrow > 0) {
            $updated = $userm->updateXVal($thisuser['fb_uid'], 'step_combo_today_count', 0);
            if ($updated > 0) {
                $result['success'] = true;
                $result['desc'] = 'แลกคอมโบสำเร็จ ได้รับ ' . $diamond_income . ' diamond';
            }
        }
    }
} else {
    $result['success'] = false;
    $result['desc'] = 'ต้องมีคอมโบต่อเนื่องอย่างน้อย 3 วันขึ้นถึงจะสามารถแลกได้';
}

echo json_encode($result);
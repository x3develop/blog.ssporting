<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 28/2/2562
 * Time: 15:08 น.
 */
require_once __DIR__ . "/../../bootstrap.php";

$fbid = $_REQUEST['fb_uid'];
$name = $_REQUEST['name'];
$fb_token = $_REQUEST['fb_token'];
$email = isset($_REQUEST['email']) ? $_REQUEST['email'] : null;
$ucon = new \controller\UserController();
$rs = $ucon->updateUser($fbid, $name, $email, $fb_token);

header("Content-Type: application/json");
echo json_encode($rs);
<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 28/2/2562
 * Time: 15:08 น.
 */
require_once __DIR__ . "/../../bootstrap.php";

@session_start();
$rs = ['session_active' => false, 'user_data' => []];
if (isset($_SESSION['login'])) {
    $rs['user_data'] = $_SESSION['login'];
    $rs['session_active'] = true;
}


header("Content-Type: application/json");
echo json_encode($rs);
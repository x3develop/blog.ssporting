<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 28/2/2562
 * Time: 15:08 น.
 */
require_once __DIR__ . "/../../bootstrap.php";
$fb_uid = $_REQUEST['fb_uid'];
$ucon = new \controller\UserController();
$rs = $ucon->getByFbid($fb_uid);
header("Content-Type: application/json");
echo json_encode($rs);
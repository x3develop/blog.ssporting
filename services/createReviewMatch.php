<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 7/9/2018
 * Time: 3:52 PM
 */
include '../model/DBPDO.php';
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/UserFeed.php";
$userFeed=new UserFeed();
if (!empty($_REQUEST['review'])) {
   $dbpdo = new DBPDO();
    $return = array();
    try {
        $dbpdo->connect();
        $sql = "INSERT INTO `review_match` (`user_id`,`match_id`,`team`,`review_text`,`created_at`,`updated_at`)";
        $sql .= " VALUES ('" . $_REQUEST['review']['user_id'] . "','" . $_REQUEST['review']['match_id'] . "','" . $_REQUEST['review']['team'] . "','" . $_REQUEST['review']['review_text'] . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
        $params = array();
        $data = $dbpdo->insert($sql, $params);
        //addFeed
        $userFeed->addUserFeedMatch($_REQUEST['review']['user_id'], $_REQUEST['review']['match_id'], 'review', $_REQUEST['review']['review_text']);

        echo json_encode($_REQUEST['review']['match_id']);
    } catch (Exception $e) {
        echo $e->getMessage();
    } finally {
        $dbpdo->close();
    }
}
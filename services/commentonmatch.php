<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 15/2/2560
 * Time: 16:13 น.
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/DataMap.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/MatchCommentM.php';

$dt = \Carbon\Carbon::now();
$choose = $_REQUEST['side'];
$mid = $_REQUEST['mid'];
$fb_uid = $_REQUEST['fb_uid'];
$msg = $_REQUEST['msg'];
$parent = $_REQUEST['parent'];

$datamap = new DataMap();
$commentm = new MatchCommentM();
$result = array('success' => false, 'desc' => 'nothing happen', 'message_id' => 0);
if (!empty($msg)) {
    $result['message_id'] = $commentm->addComment($mid, $fb_uid, $parent, $choose, $msg);
    if ($result['message_id'] > 0) {
        $result['success'] = true;
        $result['desc'] = 'insert done';
    }
}
echo json_encode($result);
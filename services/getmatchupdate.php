<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 1/2/2560
 * Time: 16:10 น.
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/FileManager.php";

$zid = $_REQUEST['zid'];

$dt = \Carbon\Carbon::now();
$fileman = new FileManager();
$fileman->setPath("dist/gameupdate/" . $dt->toDateString());

$result = array('data' => array(), 'zid' => 0, 'c3' => 0);
$result['c3'] = $dt->timestamp;
$data = array();
$newzid = $zid;
$list = $fileman->listFiles(0);
foreach ($list as $key => $filename) {
    if (intval($zid) < intval($filename)) {
        $fileman->setFilename($filename);
        $updatedata = json_decode($fileman->read());
        foreach ($updatedata as $update) {
            $data[] = $update;
        }
        $newzid = intval($filename);
    }
}
//var_dump($data);
$result['zid'] = $newzid;
$result['data'] = $data;
echo json_encode($result);
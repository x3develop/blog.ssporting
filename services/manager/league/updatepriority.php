<?php
require_once __DIR__ . "/../../../bootstrap.php";

$lid = $_REQUEST['lid'];
$priority = $_REQUEST['priority'];
$cont = new \controller\LeagueController();
$rs = null;
$rs = $cont->updatePriority($lid, $priority);
header("Content-Type: application/json");
echo json_encode($rs);

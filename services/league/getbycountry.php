<?php
require_once __DIR__ . "/../../bootstrap.php";
$cid = $_REQUEST['cid'];
$mcon = new \controller\LeagueController();
$rs = $mcon->getByCountry($cid);
header("Content-Type: application/json");
echo json_encode($rs);
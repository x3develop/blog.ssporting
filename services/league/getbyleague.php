<?php
require_once __DIR__ . "/../../bootstrap.php";
$lid = $_REQUEST['lid'];
$mcon = new \controller\LeagueController();
$rs = $mcon->getByLeague($lid);
header("Content-Type: application/json");
echo json_encode($rs);
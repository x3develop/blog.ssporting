<?php
require_once __DIR__ . "/../../bootstrap.php";
$lid = $_REQUEST['lid'];
$group = empty($_REQUEST['group']) ? "N" : $_REQUEST['group'];
$mcon = new \controller\LeagueController();
$rs = null;
if ($group == "N") {
    $rs = $mcon->getStanding($lid);
} else {
    $rs = $mcon->getStandingGroup($lid);
}
header("Content-Type: application/json");
echo json_encode($rs);

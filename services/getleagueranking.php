<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 1/2/2560
 * Time: 16:10 น.
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/model/LeageM.php";

$lid = ($_REQUEST["lid"]) ? $_REQUEST['lid'] : 0;
$slid = ($_REQUEST["slid"]) ? $_REQUEST['slid'] : 0;
$leaguem = new LeageM();
$result = $leaguem->getRankingPack($lid, $slid);
echo json_encode($result);
<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 6/25/2018
 * Time: 9:52 PM
 */
$id=((isset($_GET['id']))?$_GET['id']:null);
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
$playMatch = new playMatch();
$matchResultsHome = $playMatch->getListMatchResults($id, 20);
?>
<td class="btn-results-left" number="3"><i class="fa fa-caret-left" aria-hidden="true"></i></td>
<?php if (!empty($matchResultsHome)) { ?>
    <?php foreach (array_reverse($matchResultsHome) as $keyResults => $valueResults) { ?>
        <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
            <td scoreAway="<?=$valueResults->scoreAway?>" scoreHome="<?=$valueResults->scoreHome?>" number="<?=floor($keyResults/5);?>" class="<?=((floor($keyResults/5)==3)?'':'hide')?> li-results"><img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/draw.png"></td>
        <?php } elseif ($valueResults->team_home == $matchView->team_home && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
            <td team_homeR="<?php echo $valueResults->team_home?>" team_home ="<?php echo $matchView->team_home;?>" scoreAway="<?=$valueResults->scoreAway?>" scoreHome="<?=$valueResults->scoreHome?>" number="<?=floor($keyResults/5);?>" class="<?=((floor($keyResults/5)==3)?'':'hide')?> li-results"><img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/win.png"></td>
        <?php } elseif ($valueResults->team_away == $matchView->team_home && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
            <td team_homeR="<?php echo $valueResults->team_home?>" team_home ="<?php echo $matchView->team_home;?>" scoreAway="<?=$valueResults->scoreAway?>" scoreHome="<?=$valueResults->scoreHome?>" number="<?=floor($keyResults/5);?>" class="<?=((floor($keyResults/5)==3)?'':'hide')?> li-results"><img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/win.png"></td>
        <?php } else { ?>
            <td team_homeR="<?php echo $valueResults->team_home?>" team_home ="<?php echo $matchView->team_home;?>" scoreAway="<?=$valueResults->scoreAway?>" scoreHome="<?=$valueResults->scoreHome?>" number="<?=floor($keyResults/5);?>" class="<?=((floor($keyResults/5)==3)?'':'hide')?> li-results"><img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/lose.png"></td>
        <?php } ?>
    <?php } ?>
<?php } ?>
<td>Last <span class="total-results">5</span> Match</td>

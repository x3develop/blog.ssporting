<?php
    header('Location: /sexy-football');
    exit();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ngoal | Gallery</title>
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="shortcut icon" type="images/x-icon" href="images/favicon.ico.png">


    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/view-all.css">
<!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="/js/flogin/src/flogin.js"></script>
    <script src="/js/moment.js"></script>
    <?php include 'view/index/googletagmanager.php'; ?>
</head>
<body>
<?php
require_once 'model/GalleryM.php';
$GalleryObject=new GalleryM();
$Page=(isset($_GET['Page'])?$_GET['Page']:1);
if(isset($Page)){
    $dataGalleryAll=$GalleryObject->getAllGallery(($Page-1)*12,12);
    $dataMoreRecentedGallery=$GalleryObject->getAllGallery(($Page)*12,12);
}else{
    $dataGalleryAll=$GalleryObject->getAllGallery(0,12);
    $dataMoreRecentedGallery=$GalleryObject->getAllGallery((1*12),12);
}
$maxPage=array();
$maxPage=$GalleryObject->getAllGalleryCountPage(12);
//$dataMoreRecentedGallery=$GalleryObject->getMoreRecentedGallery('น่ารัก',12);

?>
<!--index menu-->
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<!--<nav class="navbar navbar-menu-v2">-->
<!--    --><?php //include 'view/menu/navbar-menu-v2.php'; ?>
<!--</nav>-->


<div class="wrapper-news-articles">
    <div class="containter-fluid">
        <div class="bx-head-fixtures">
            <div class="hilight-cover-videos"></div>
            <div class="articles-title">
                <div class="container">
                    <div class="head-title-top pull-left">
                        <div style="font-size: 5em;">Sexy Football</div>
                    </div>
                </div>
            </div>
            <div class="background-image_image2" style="background-image: url('/images/gallery/6.jpg');"></div>
        </div>
    </div>
    <div class="container">
        <div class="wrap-content-cols">
            <div class="rows">
                <div class="col-sm-9">
                    <div class="content-articles">
                        <div class="wrap-gallery mg-bt15">
                            <?php foreach ($dataGalleryAll as $key=>$value){ ?>
                                <div class="col-sm-6" onclick="location.href='/sexy-football-picture?id=<?php echo $value->id ?>'">
                                    <div class="wrap-box-gallery">
                                        <div class="bx-type-gallery">
                                            <?php if($value->gall_type=='picture'){ ?>
                                                <i class="fa fa-picture-o"></i>
                                            <?php }else if($value->gall_type=='video'){ ?>
                                                <i class="fa fa-video-camera"></i>
                                            <?php } ?>
                                        </div>
                                        <div class="bx-gallery">
                                            <a href="/sexy-football-picture?id=<?php echo $value->id ?>">
                                                <?php if($value->gall_type=='picture'){ ?>
                                                    <div class="crop" style="background-image: url('<?php echo $value->path ?>');"></div>
                                                <?php }else if($value->gall_type=='video'){ ?>
                                                    <div class="crop" style="background-image: url('<?php echo $value->thumbnail ?>');"></div>
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <div class="tab-datetime">
                                            <span class="pull-left">
                                                <?php if(!empty($value->createDatetime)){ ?>
                                                    <?php echo date(' j/n/Y', strtotime($value->createDatetime)) ?>
                                                <?php } ?>
                                            </span>
                                            <span class="pull-right"><i class="fa fa-eye"></i> <?php echo $value->view; ?></span>
                                            <div style="clear: both;"></div>
                                        </div>
                                        <div class="bx-detail-gallery">
                                            <?php echo $value->title; ?>
                                        </div>
                                    </div>
                                </div>
                                <?php if(($key+1)%2==0){ ?>
                                    <div style="clear: both;"></div>
                            <?php }} ?>
                        </div>
                        <div style="clear: both;"></div>


                        <div class="text-center">
                        <nav aria-label="Page navigation">
                            <ul class="pagination">
                                <?php if((isset($Page) && $Page>1)){ ?>
                                    <li>
                                        <a href="/gallery?Page=<?php echo $Page-1; ?>" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php for ($i=1;$i<=$maxPage[0]->maxPage;$i++){ ?>
                                    <?php if((($Page+4>$maxPage[0]->maxPage) and $i<4) || (($i>=($Page-3) && $i<=($Page+3)) || $i>=($maxPage[0]->maxPage-3))){ ?>
                                        <li class="<?php echo ((isset($Page) && $Page==$i)?'active':'')?>" ><a href="/gallery?Page=<?php echo $i; ?>"><?php echo $i ?></a></li>
                                    <?php }else{?>
                                        <?php if((($Page+4>$maxPage[0]->maxPage) and $i==4)||(($i==$Page+4) and ($Page+4<$maxPage[0]->maxPage))){ ?>
                                            <li class="disabled"><a>...</a></li>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                                <?php if((isset($Page) && $Page<$maxPage)){ ?>
                                    <li>
                                        <a href="/gallery?Page=<?php echo $Page+1; ?>" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </nav>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="bx-publish-recommend">
                        <h3>More Recented</h3>
                        <table>
                            <?php foreach ($dataMoreRecentedGallery as $key=>$value){ ?>
                                <tr>
                                    <td>
                                        <a style="height: 60px;" href="/sexy-football-picture?id=<?php echo $value->id ?>" class="news-image"><img src="<?php echo $value->thumbnail ?>"></a>
                                    </td>
                                    <td>
                                        <a href="/sexy-football-picture?id=<?php echo $value->id ?>"><?php echo $value->title ?></a>
                                        <div class="times-content"><i class="fa fa-clock-o"></i> <span class="timeMoreRecentedGallery" date="<?php echo $value->update_at; ?>"></span></div>
                                    </td>
                                </tr>
                            <?php }?>
                        </table>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>

    </div>
</div>
<?php //include 'footer.php';
include 'view/index/footer.php';
?>

<script>
    loadMoment();
    function loadMoment() {
        $('.timeMoreRecentedGallery').each(function (index) {
            if($.isNumeric($(this).attr('date'))) {
                $(this).html(moment($(this).attr('date'), "X").fromNow())
            }else{
                $(this).html(moment($(this).attr('date'), "YYYY-MM-DD HH:mm:ss").fromNow())
            }
        })
    }
</script>

</body>
</html>
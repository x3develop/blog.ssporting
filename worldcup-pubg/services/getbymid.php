<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/25/2018
 * Time: 5:15 PM
 */
require_once __DIR__ . "/../bootstrap.php";
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Cache\Adapter\Filesystem\FilesystemCachePool;

$mid = $_REQUEST['mid'];
$cache = (isset($_REQUEST['cache'])) ? true : false;


$tcon = new \controller\MatchController();

if (!$cache) {
    $rs = $tcon->getByMid($mid);
} else {
    $filesystemAdapter = new Local(__DIR__ . '/../');
    $filesystem = new Filesystem($filesystemAdapter);
    $pool = new FilesystemCachePool($filesystem);
    $pool->setFolder("cache");
    $item = $pool->getItem("mid" . $mid);

    $rs = [];
    if (!$item->isHit()) {
        $rs = $tcon->getByMid($mid);
        $item->set($rs);
        $item->expiresAfter(180);
        $pool->save($item);
    } else {
        $rs = $item->get();
    }
}
header("ContentTyp:application/json");
echo json_encode($rs);
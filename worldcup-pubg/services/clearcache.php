<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 7/6/2018
 * Time: 11:16 AM
 */



require_once __DIR__ . "/../bootstrap.php";

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Cache\Adapter\Filesystem\FilesystemCachePool;

$filesystemAdapter = new Local(__DIR__ . '/../');
$filesystem = new Filesystem($filesystemAdapter);
$pool = new FilesystemCachePool($filesystem);
$pool->setFolder("cache");
$pool->clear();
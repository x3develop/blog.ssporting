<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 7/6/2018
 * Time: 11:07 AM
 */

require_once __DIR__ . "/../bootstrap.php";

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Cache\Adapter\Filesystem\FilesystemCachePool;

$fb_id = $_REQUEST['fbid'];
$rs = ["alert" => true];
$filesystemAdapter = new Local(__DIR__ . '/../');
$filesystem = new Filesystem($filesystemAdapter);
$pool = new FilesystemCachePool($filesystem);
$pool->setFolder("cache");
$item = $pool->getItem("dismiss_reward_alert" . $fb_id);
$rs["alert"] = false;
$rs["source"] = "cache";
if (!$item->isHit()) {
    $item->set($rs);
    $pool->save($item);
}

header("ContentType:application/json");
echo json_encode($rs);
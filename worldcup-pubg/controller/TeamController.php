<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/25/2018
 * Time: 5:16 PM
 */

namespace controller;

use model\Team;

require_once __DIR__ . "/../bootstrap.php";

class TeamController
{
    public function add($name, $flag)
    {
        $team = Team::firstOrCreate(['name' => $name], ['flag' => $flag]);
        return $team;
    }

    public function getAll()
    {
        return Team::all();
    }

    public function del($tid)
    {
        $t = Team::find($tid);
        $t->delete();
    }
}
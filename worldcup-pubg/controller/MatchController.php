<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/25/2018
 * Time: 8:38 PM
 */

namespace controller;

use Carbon\Carbon;
use model\Match;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Cache\Adapter\Filesystem\FilesystemCachePool;

require_once __DIR__ . "/../bootstrap.php";

class MatchController
{
    public function getAll()
    {
        return Match::all();
    }

    public function getByMid($mid)
    {
        return Match::where('mid', $mid)->first();
    }

    public function update($mid, $aid, $bid, $winner, $play_date, $deadline)
    {
        $match = Match::firstOrCreate(['mid' => $mid]);
        $dt  =Carbon::createFromFormat("Y-m-d H:i:s",$deadline,"Asia/Bangkok");
        $match->aid = $aid;
        $match->bid = $bid;
        $match->winner = $winner;
        $match->match_time = $play_date;
        $match->deadline = $dt->toDateTimeString();
        $match->save();
        return $match;
    }

    public function getAvailable()
    {

        $filesystemAdapter = new Local(__DIR__ . '/../');
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder("cache");
        $item = $pool->getItem("available_match");

        $rs = [];
        if (!$item->isHit()) {

            $matches = Match::whereNotNull("match_time")->with("teama")->with("teamb")->with("winnerteam")->get();
            foreach ($matches as $match) {
                $rs[$match->mid] = $match;
            }
            $item->set($rs);
            $item->expiresAfter(180);
            $pool->save($item);
        } else {
            $rs = $item->get();
        }



        return $rs;
    }

}
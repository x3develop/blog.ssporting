<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bare - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <link href="vendor/components/font-awesome/css/fontawesome-all.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <style>
        body {
            padding-top: 54px;
        }

        @media (min-width: 992px) {
            body {
                padding-top: 56px;
            }
        }

    </style>

</head>
<?php require_once __DIR__ . "/bootstrap.php"; ?>
<?php
@session_start();
if (!$_SESSION['wcroot']) {
    header("Location:/worldcup-pubg/login.php");
}
$round = isset($_REQUEST['round']) ? $_REQUEST['round'] : 16;
$ccon = new \controller\CandidateController();
$candidates = $ccon->getAllCandidate($round);
$playuser = $ccon->getAllPlay();
$roundattempt = $ccon->getRoundAttempt($round);
$roundcandiddate = $ccon->getRoundCandidate($round);
?>

<body>
<?php include_once __DIR__ . "/include/navbar.php" ?>

<!-- Page Content -->
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3">
            <form class="form-inline">
                <div class="form-group mb-2">
                    <label for="select-round">Select Round</label>
                    <select class="form-control" id="select-round">
                        <option value="16">16</option>
                        <option value="8">8</option>
                        <option value="4">4</option>
                        <option value="2">2</option>
                    </select>
                </div>
                <button type="button" id="cal-candidate" class="btn btn-primary mb-2">Cal</button>
                <a class="btn btn-success mb-2" href="/worldcup-pubg/services/downloadcsv.php?round=<?php echo $round; ?>">Download</a>
            </form>
        </div>
        <div class="col-lg-3">All Play: <?php echo $playuser; ?></div>
        <div class="col-lg-3">This Round: <?php echo $roundattempt; ?></div>
        <div class="col-lg-3">Pass : <?php echo $roundcandiddate; ?></div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Facebook ID</td>
                    <td>Facebook Name</td>
                    <td>Round</td>
                    <td>Created</td>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($candidates as $key => $candidate) { ?>
                    <tr>
                        <td><?php echo($key + 1); ?></td>
                        <td><?php echo $candidate->fb_id; ?></td>
                        <td><?php echo $candidate->user->name; ?></td>
                        <td><?php echo $candidate->round; ?></td>
                        <td><?php echo $candidate->created_at ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="vendor/moment/moment/moment.js"></script>
<script src="vendor/components/jquery/jquery.js"></script>
<script src="vendor/twbs/bootstrap/dist/js/bootstrap.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function () {
        var round = getParam("round");
        if (round == null) {
            round = 16;
        }

        $("#select-round").val(round);
        $("#select-round").on('change', function () {
            window.location = "/worldcup-pubg/candidate.php?round=" + $("#select-round").val();
        });

        function getParam(param) {
            return new URLSearchParams(window.location.search).get(param);
        }

        $("#cal-candidate").on('click',function () {
            $.ajax({
                url:"/worldcup-pubg/services/calround.php",
                data:{round:round},
                dataType:"JSON",
                method:"GET"
            }).done(function (res) {
               // swal("Pass condition "+res.candidate+" user");
               location.reload();
            });
        })
    });
</script>
</body>

</html>

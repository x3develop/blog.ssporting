<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link href="/worldcup-pubg/css/worldcup2018-pubg.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style-new.css">

    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/mobile/js/bootstrap.js"></script>
</head>
<?php
require_once __DIR__ . "/bootstrap.php";
$mcon = new \controller\MatchController();
$matches = $mcon->getAvailable();


?>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>

<div id="content-event2" class="container">
    <div class="row">
        <div class="top-container-fifa-mobile">

            <img src="/worldcup-pubg/img/side-event-pubg-mobile.png">
            <!--            <p>ทายทีมเข้ารอบตั้งแต่รอบ 8 ทีม จนถึงรอบชิงชนะเลิศ โดยแบ่งเป็น 4 ช่วง-->
            <!--                เพื่อแจกของรางวัลมูลค่ากว่า 50,000 บาท</p>-->
            <div class="btn-sign-FB"></div>
        </div>
        <div class="container-fifa-mobile">


            <!--            Semi-finals-->
            <div class="content-fifa-mobile-box" style="height: 262px;">

                <div class="fifa-mobile-box-href">

                    <div class="box-fifa-16 box-fifa-match" style="margin: 83px 0 0 10px;">
                        <div class="check-box" mid="13">

                        </div>
                        <?php if (array_key_exists(13, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[13]->mid; ?>"
                                 tid="<?php echo $matches[13]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[13]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[13]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[13]->mid; ?>"
                                 tid="<?php echo $matches[13]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[13]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[13]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="box-award-mobile">
                        <img style="width: 120px;" src="/worldcup-pubg/img/item-01.png">
                    </div>
                </div>
                <div class="fifa-mobile-box-href">
                    <div class="box-fifa-16 box-fifa-match" style="margin: 83px 15px 0;">
                        <div class="check-box" mid="14">

                        </div>
                        <?php if (array_key_exists(14, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[14]->mid; ?>"
                                 tid="<?php echo $matches[14]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[14]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[14]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[14]->mid; ?>"
                                 tid="<?php echo $matches[14]->bid; ?>">
                                <ul>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[14]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[14]->teamb->flag; ?>">
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>
                                    <li class="name-fifa-16"><span>???</span></li>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>

                    </div>
                    <div class="box-award-mobile">
                        <ul>
                            <li>สกิน</li>
                            <li>50</li>
                            <li>รางวัล</li>
                        </ul>
                        <div class="btn-col save-selected">
                            ยืนยันการเลือกทีม
                        </div>
                    </div>
                </div>


            </div>


            <!--            Final-->
            <div class="content-fifa-mobile-box" style="height: 348px;">
                <div class="content-fifa-mobile-box-final box-fifa-16 box-fifa-final" style="margin-top: 105px;">
                    <div class="box-fifa-match-final">
                        <div class="check-box" mid="15">

                        </div>

                        <?php if (array_key_exists(15, $matches)) { ?>
                            <div class="pull-left box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[15]->mid; ?>"
                                 tid="<?php echo $matches[15]->aid; ?>">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="<?php echo $matches[15]->teama->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16">
                                        <span><?php echo strtoupper(substr($matches[15]->teama->name, 0, 3)); ?></span>
                                    </li>
                                </ul>
                            </div>

                            <div class="pull-right box-fifa-16-haft select-team team-mark"
                                 mid="<?php echo $matches[15]->mid; ?>"
                                 tid="<?php echo $matches[15]->bid; ?>">
                                <ul>
                                    <li class="flag-fifa-16 text-right">
                                        <img src="<?php echo $matches[15]->teamb->flag; ?>">
                                    </li>
                                    <li class="name-fifa-16 pull-right">
                                        <span><?php echo strtoupper(substr($matches[15]->teamb->name, 0, 3)); ?></span>
                                    </li>
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="pull-left box-fifa-16-haft">
                                <ul>
                                    <li class="flag-fifa-16">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16"><span>???</span></li>
                                </ul>
                            </div>
                            <div class="pull-right box-fifa-16-haft">
                                <ul>


                                    <li class="flag-fifa-16 text-right">
                                        <img src="/images/Flag-default.jpg">
                                    </li>
                                    <li class="name-fifa-16 pull-right"><span>???</span></li>
                                </ul>
                            </div>
                        <?php } ?>


                    </div>

                </div>

                <div class="box-award-mobile">
                    <div class="box-award-mobile" style="top: -134px; bottom: 0;">
                        <img style="width: 225px;" src="/worldcup-pubg/img/item-02.png">
                    </div>
                    <ul style="margin-top: 21px!important;">
                        <li>สกิน</li>
                        <li>50</li>
                        <li>รางวัล</li>
                    </ul>
                    <div class="btn-col save-selected">
                        ยืนยันการเลือกทีม
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container text-center" style="padding-top: 20px;">
    <a href="#content-event2"><img style="width: 100%;" src="/worldcup-pubg/img/thopy1.png"></a>
</div>


<div class="container container-list">
    <div class="col-lg-12">
        <h3>เงื่อนไขการร่วมชิงรางวัล</h3>
        <ol>
            <li>เริ่มทายผลผ่านเว็บไซต์ ได้ตั้งแต่วันที่ 10 กรกฏาคม 2561 สิ้นสุดวันที่ 15 กรกฏาคม 2561 โดย ทายผลผ่านเว็บไซต์หมดเขตส่งคำทายเวลา 23.59 น. โดยถือเวลาที่ทายผล ครั้งสุดท้ายเป็นสำคัญ
            </li>
            <li>ผู้ที่มีสิทธิ์ลุ้นรับรางวัล ต้องทายทีมเข้ารอบถูกต้องทั้งหมดในแต่ละรอบการแข่งขัน</li>
            <li>
                กกำหนดสุ่มจับรางวัล และประกาศรายชื่อผู้โชคดีแบ่งเป็น 4 รอบ ทางเว็บ ngoal.com และทางเพสเฟสบุ๊ก จบสกอร์
                <ul>
                    <li>รอบที่ 1 วันที่ 12 กรกฎาคม</li>
                    <li>รอบที่ 2 วันที่ 16 กรกฎาคม</li>
                </ul>
            </li>
            <li>ผู้ได้รับรางวัลมีสิทธิรับรางวัลที่มีมูลค่าสูงสุดเพียงรางวัลเดียวเท่านั้น</li>
            <li>แจกเฉพาะ ID ที่อยู่ใน PUBG Moblie เท่านั้น</li>
            <li>คำตัดสินของคณะกรรมการให้ถือเป็นที่สุด</li>
            <li>ทางเว็บไชต์ ngoal.com ขอสงวนสิทธิในการเปลี่ยนแปลงรายละเอียดหรือเงื่อนไขโดยไม่ต้องแจ้งให้ทราบล่วงหน้า
            </li>
        </ol>
    </div>

    <div class="col-lg-12">
        <h3>รางวัลสำหรับการทายผล</h3>
        <ul>
            <li>รางวัลรอบที่ 2 สกินเกม PUBG ประกอบด้วย ปืน, ร่มชูชีพ, เครื่องแต่งกาย มูลค่า 7,000 บาท จำนวน 50 รางวัล</li>
            <li>รางวัลรอบที่ 1 สกินเกม PUBG ประกอบด้วย ปืน, ร่มชูชีพ, เครื่องแต่งกาย มูลค่า 7,000 บาท จำนวน 50 รางวัล</li>
        </ul>
    </div>
    <div class="col-lg-12">
        <h3>วิธีการร่วมสนุกทายผลผ่านเว็บไซต์</h3>
        <ol>
            <li>เลือกทายประเทศที่จะเข้ารอบฟุตบอลโลก 2018 ของแต่ละรอบการแข่งขันผ่านเว็บไซต์ www.ngoal.com/worldcup-pubg</li>
            <li>ลงทะเบียนในเว็บไซต์ โดยการ login ด้วย facebook ของจริงที่ลงทะเบียนมาแล้วไม่ต่ำกว่า 3 เดือน</li>
            <li>ผู้ร่วมกิจกรรมไม่สามารถเปลี่ยนคำตอบได้เมื่อกดยืนยันคำตอบแล้ว
                แต่สามารถตอบเพิ่มได้ในคู่ที่ยังไม่ได้เล่นจนกว่าจะหมดเขตทายผลของแต่ละรอบ
            </li>
            <li>ผู้ร่วมกิจกรรม 1 คน มีสิทธิเพียง 1 สิทธิเท่านั้น</li>
        </ol>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="reward-board" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="exampleModalLongTitle">ประกาศรายชื่อผู้โชคดี รอบ Final</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive" src="/worldcup-pubg/img/pubg-final.jpg">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive" src="/worldcup-pubg/img/pubg-final2.jpg">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">
                        <span style="color: red;font-size: 18px">หมายเหตุ: ผู้ที่ได้รับรางวัลต้องติดต่อขอรับรางวัลภายในวันที่ 17 ก.ค. 2561 เวลา 18.00น. หากเลยกำหนดถือว่าสละสิทธิ์</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                <button type="button" id="dismiss-reward" class="btn btn-primary">ไม่ต้องแสดงอีก</button>
            </div>
        </div>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://connect.facebook.net/en_US/sdk.js"></script>
<script type='text/javascript'>
    var hassession = <?php echo (isset($_SESSION['login'])) ? 1 : 0; ?>;
</script>
<script src="/worldcup-pubg/js/wc_pubg.js"></script>


</body>
</html>

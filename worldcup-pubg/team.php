<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bare - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <link href="jquery-ui-1.12.1/jquery-ui.css" rel="stylesheet">
    <link href="vendor/components/font-awesome/css/fontawesome-all.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <style>
        body {
            padding-top: 54px;
        }

        @media (min-width: 992px) {
            body {
                padding-top: 56px;
            }
        }

    </style>

</head>
<?php require_once __DIR__ . "/bootstrap.php"; ?>
<?php
@session_start();
if(!$_SESSION['wcroot']){
    header("Location:/worldcup-pubg/login.php");
}

$tcon = new \controller\TeamController();
$teams = $tcon->getAll();

?>

<body>
<?php include_once __DIR__ . "/include/navbar.php" ?>

<!-- Page Content -->
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <form class="form-inline" method="post" action="services/addteam.php">
                <div class="form-group">
                    <label for="exampleInputName2">Team </label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Team Name" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputName2">Flag </label>
                    <input type="text" class="form-control" id="flag" name="flag" placeholder="Flag url" required>
                </div>
                <button type="submit" class="btn btn-success">Add</button>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Team Name</td>
                    <td>Manage</td>

                </tr>
                </thead>
                <tbody>
                <?php foreach ($teams as $team){ ?>
                <tr>
                    <td><img src="<?php echo $team->flag; ?>"></td>
                    <td><?php echo $team->name; ?></td>
                    <td><a href="services/delteam.php?tid=<?php echo $team->id; ?>"><i class="fa fa-trash del-team" tid="<?php echo $team->id; ?>"></i></a></td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript -->
<script src="vendor/components/jquery/jquery.js"></script>
<script src="jquery-ui-1.12.1/jquery-ui.js"></script>
<script>
    $(document).ready(function () {
        $('#start').datepicker({dateFormat: 'yy-mm-dd', lang: "th"}).val();
        $('#end').datepicker({dateFormat: 'yy-mm-dd', lang: "th"}).val();

        // $(document).on('click',".logs", function (e) {
        //     var id = $(e.currentTarget).attr("lid");
        //     window.location="detail.php?id="+id;
        // });


    });
</script>
</body>

</html>

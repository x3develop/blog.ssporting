$(document).ready(function () {
    var fbid = "";
    var currect = "<img src=\"/images/check-true.png\">";
    var incurrect = "<img src=\"/images/check-false.png\">";
    var selectcircle = "<div class=\"mo-loading-semi\"><img src=\"/worldcup/images/loading-red.gif\"></div>";
    var selectlist = {};
    var playlist = {};
    checkLoginState();
    // checkPlay(fbid);
    $(document).on("click", ".select-team", function (e) {

        var mid = $(e.currentTarget).attr("mid");
        var tid = $(e.currentTarget).attr("tid");
        if (tid != 0) {
            if (isLogin()) {
                // console.log(mid, tid);
                // $(".team-mark[mid=" + mid + "]").addClass("none-select");
                // $(".team-mark[tid=" + tid + "][mid=" + mid + "]").removeClass("none-select");
                // $.each($(document).find(".team-mark[mid=" + mid + "]"), function (k, v) {
                //     if ($(v).attr('tid') == tid) {
                //         $(v).removeClass("none-select");
                //     }
                // });
                // console.log(playlist[mid])
                if (playlist[mid] != true) {
                    $.ajax({
                        url: "/worldcup-pubg/services/timeup.php",
                        method: "POST",
                        data: {mid: mid},
                        dataType: "JSON"
                    }).done(function (res) {
                        console.log(res);
                        if (res.timeup) {
                            // $(".team-mark[mid=" + mid + "]").removeClass("none-select");
                            // $(".team-mark[tid=" + tid + "]").removeClass("none-select");
                            swal("หมดเวลาทายผลคู่นี้", {icon: 'warning'});
                        } else {
                            selectlist[mid] = tid;
                            $(".team-mark[mid=" + mid + "]").addClass("none-select");
                            $(".team-mark[tid=" + tid + "][mid=" + mid + "]").removeClass("none-select");
                            $.each($(document).find(".team-mark[mid=" + mid + "]"), function (k, v) {
                                // console.log($(v)[0].outerHTML)
                                $(v).find(".mo-loading-semi").first().remove();
                            });
                            console.log(mid, tid);
                            if (mid == 15 && tid == res.bid) {
                                var css = $.parseHTML("<div class=\"mo-loading-semi\"><img src=\"/worldcup/images/loading-red.gif\"></div>");
                                $(css).css({right: "0"});
                                $(".team-mark[tid=" + tid + "][mid=" + mid + "]").children().first().find("img").first().parent().prepend($(css));
                            }
                            else {
                                $(".team-mark[tid=" + tid + "][mid=" + mid + "]").children().first().find("img").first().parent().prepend(selectcircle);
                            }
                        }
                    });
                } else {
                    swal("ทายผลคู่นี้แล้ว");
                }

            } else {
                // The person is not logged into your app or we are unable to tell.
                swal("กรุณาลงทะเบียน facebook เพื่อเข้าร่วมกิจกรรม");
            }
        }

        //console.log(selectlist);

    });

    function isLogin() {
        // console.log(fbid);
        if (hassession == 0) {
            $(".btn-sign-FB").removeClass("hide");
            return false;
        } else {
            return true;
        }
    }


    function checkLoginState() {
        FB.getLoginStatus(function (response) {
            // console.log(response);
            if (response.status === 'connected' && isLogin()) {
                // Logged into your app and Facebook.
                // testAPI();
                $(".btn-sign-FB").addClass("hide");
                updateUser(response.authResponse.accessToken);

            } else {
                $(".btn-sign-FB").removeClass("hide");
                if (hassession == 1) {
                    // FBlogin();
                    fbLogin();
                }
            }

        });
    }

    function updateUser(token) {
        FB.api('/me', function (response) {
            fbid = response.id;
            loadAlert();
            $.ajax({
                url: "/worldcup-pubg/services/updateuser.php",
                method: "POST",
                data: {fb_id: response.id, name: response.name, access_token: token},
                dataType: "JSON"
            }).done(function (res) {
                // console.log(res);
                checkPlay(fbid);
            });

        });
    }

    $(".btn-sign-FB").on("click", function () {
        // fbLogin();
        FBlogin();
    });

    function fbLogin() {
        FB.login(function (response) {
            location.reload();
        });

    }

    function checkPlay(fbid) {
        $.ajax({
            url: "/worldcup-pubg/services/getplay.php",
            method: "POST",
            data: {fbid: fbid},
            dataType: "JSON"
        }).done(function (res) {
            $.each(res, function (k, v) {
                playlist[v.mid] = true;
                // console.log(v);
                // $(".team-mark[mid=" + v.mid + "]").removeClass("select-team");
                $(".team-mark[mid=" + v.mid + "]").addClass("none-select");
                $(".team-mark[tid=" + v.winner + "][mid=" + v.mid + "]").removeClass("none-select");

                if (v.result == "win") {
                    $(".check-box[mid=" + v.mid + "]").append(currect);
                } else if (v.result == "lose") {
                    $(".check-box[mid=" + v.mid + "]").append(incurrect);
                } else {
                    $(".check-box[mid=" + v.mid + "]").empty();
                    if (v.mid == 15 && v.winner == v.game.bid) {
                        var css = $.parseHTML("<div class=\"mo-loading-semi\"><img src=\"/worldcup-pubg/images/loading-red.gif\"></div>");
                        $(css).css({right: "0"});
                        $(".team-mark[tid=" + v.winner + "][mid=" + v.mid + "]").children().first().find("img").first().parent().prepend($(css));
                    }
                    else {
                        $(".team-mark[tid=" + v.winner + "][mid=" + v.mid + "]").children().first().find("img").first().parent().prepend(selectcircle);
                    }
                }

            });


        });
    }

    $(".save-selected").on("click", function (e) {

        if (Object.keys(selectlist).length == 0) {
            swal("กรุณาเลือกทีมก่อน", {
                icon: "warning",
            });
        } else {
            save();
        }
    })

    function save() {
        swal({
            title: "Are you sure?",
            text: "หากเลือกแล้วไม่สามารถแก้ไขได้ภายหลัง",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        url: "/worldcup-pubg/services/addbet.php",
                        method: "GET",
                        data: {fbid: fbid, list: JSON.stringify(selectlist)},
                        dataType: "JSON"
                    }).done(function (response) {
                        //console.log(response);
                        $.each(response, function (k, res) {
                            playlist[res.mid] = true;
                            $(".team-mark[mid=" + res.mid + "]").removeClass("select-team");
                            $(".team-mark[mid=" + res.mid + "]").addClass("none-select");
                            $(".team-mark[tid=" + res.winner + "]").removeClass("none-select");
                        });

                        swal("Saved!", {
                            icon: "success",
                        });
                    })


                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
    }

    $(".box-fifa-16-haft").on('click', function (e) {
        if ($(e.currentTarget).hasClass("team-mark")) {

        } else {
            swal("ยังไม่มีคู่บอลให้ทายผล");
        }

    })

    function loadAlert() {
        $.ajax({
            url: "/worldcup-pubg/services/rewardalert.php",
            data: {fbid: fbid},
            method: "POST",
            dataType: "JSON"

        }).done(function (res) {
            console.log(res);
            if (res.alert) {
                $("#reward-board").modal("show");
            }
        });
    }

    $("#dismiss-reward").on("click", function () {
        $("#reward-board").modal("hide");
        $.ajax({
            url: "/worldcup-pubg/services/dismissrewardalert.php",
            data: {fbid: fbid},
            method: "POST",
            dataType: "JSON"
        }).done(function (res) {

        });
    });

});
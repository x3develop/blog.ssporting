<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 7/2/2018
 * Time: 2:47 PM
 */

namespace model;
require_once __DIR__ . "/../bootstrap.php";

use Illuminate\Database\Eloquent\Model;


class Candidate extends Model
{
    protected $table = "candidate";
    protected $fillable = ['fb_id', 'round', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->hasOne(User::class, "fb_id", "fb_id");
    }

}
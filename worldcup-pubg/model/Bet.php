<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/26/2018
 * Time: 4:09 PM
 */

namespace model;
require_once __DIR__ . "/../bootstrap.php";

use Illuminate\Database\Eloquent\Model;

class Bet extends Model
{
    protected $table = "bet";
    protected $fillable = ['mid', 'fb_id', 'winner', 'result', 'created_at', 'updated_at'];

    public function owner()
    {
        return $this->belongsTo(User::class, "fb_id", "fb_id");
    }

    public function game()
    {
        return $this->belongsTo(Match::class, "mid", "mid");
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/25/2018
 * Time: 5:04 PM
 */

namespace model;
require_once __DIR__ . "/../bootstrap.php";

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = "team";
    protected $fillable = ['name', 'flag','created_at', 'updated_at'];



}
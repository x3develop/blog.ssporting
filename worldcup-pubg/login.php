<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bare - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <link href="jquery-ui-1.12.1/jquery-ui.css" rel="stylesheet">
    <link href="vendor/components/font-awesome/css/fontawesome-all.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <style>
        body {
            padding-top: 54px;
        }

        @media (min-width: 992px) {
            body {
                padding-top: 56px;
            }
        }

        body {
            background-image: url(http://nanglidairy.com/image/login_bg.jpg);
            height: 100%;
            width: 100%;
            background-size: cover;
        }

        .login_bg {
            background: #ffffff36;
            min-height: 250px;
            margin-top: 15%;
            border-radius: 10px;
            box-shadow: 0px 0px 20px 0px #00000029;

        }

        .login_bg h2 {
            margin: 0px;
            text-align: center;
            height: 40px;
            line-height: 62px;
        }

        .login_bg .form-group {
            margin-top: 25px;
        }

        .login_bg .form-control {
            background: none;
            padding: 20px 12px;
            border: 1px solid #cad4da;

        }

        .login_bg .login_btn {
            border: 0px;
            padding: 8px;

            background: #303c67;
            font-size: 18px;
            letter-spacing: 1px;
        }



    </style>

</head>
<?php require_once __DIR__ . "/bootstrap.php"; ?>

<body>
<?php include_once __DIR__ . "/include/navbar.php" ?>

<!-- Page Content -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4 "></div>
        <div class="col-sm-4 col-sm-offset-4 login_bg">
            <form action="/worldcup/services/login.php" method="post">
                <h2>Login Panel</h2>
                <div class="form-group">
                    <input type="text" placeholder="User Email or Name" name="user" id="login_user_name"
                           class="form-control">
                </div>
                <div class="form-group">
                    <input type="password" placeholder="User Password" name="pwd" id="login_user_password"
                           class="form-control">
                </div>
                <input type="submit" name="login_btn" id="login_btn" class="btn btn-block login_btn" value="Submit"/>
            </form>
        </div>
        <div class="col-sm-4 col-sm-offset-4 "></div>
    </div>
</div>
<!-- Bootstrap core JavaScript -->
<script src="vendor/components/jquery/jquery.js"></script>
<script src="jquery-ui-1.12.1/jquery-ui.js"></script>
<script>
    $(document).ready(function () {
        $('#start').datepicker({dateFormat: 'yy-mm-dd', lang: "th"}).val();
        $('#end').datepicker({dateFormat: 'yy-mm-dd', lang: "th"}).val();

        // $(document).on('click',".logs", function (e) {
        //     var id = $(e.currentTarget).attr("lid");
        //     window.location="detail.php?id="+id;
        // });


    });
</script>
</body>

</html>

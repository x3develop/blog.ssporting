<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link href="/worldcup-pubg/css/worldcup2018-pubg.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style-new.css">

    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/mobile/js/bootstrap.js"></script>
</head>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>

<div id="content-event2" class="container">
    <div class="row">
        <div class="top-container-fifa-mobile">

            <img src="/worldcup-pubg/img/side-event-pubg-mobile.png">
            <!--            <p>ทายทีมเข้ารอบตั้งแต่รอบ 8 ทีม จนถึงรอบชิงชนะเลิศ โดยแบ่งเป็น 4 ช่วง-->
            <!--                เพื่อแจกของรางวัลมูลค่ากว่า 50,000 บาท</p>-->
            <div class="btn-sign-FB"></div>
        </div>
        <div class="container-fifa-mobile">


            <!--            Semi-finals-->
            <div class="content-fifa-mobile-box" style="height: 262px;">

                <div class="fifa-mobile-box-href">

                    <div class="box-fifa-16 box-fifa-match" style="margin: 83px 0 0 10px;">
                        <div class="check-box" mid="13">

                        </div>
                        <div class="pull-left box-fifa-16-haft select-team team-mark" mid="13" tid="5">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="http://ssl.gstatic.com/onebox/media/sports/logos/KnSUdQWiGRoy89q4x85IgA_48x48.png">
                                </li>
                                <li class="name-fifa-16">
                                    <span>FAN</span>
                                </li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft select-team team-mark" mid="13" tid="14">
                            <ul>
                                <li class="name-fifa-16">
                                    <span>BEL</span>
                                </li>
                                <li class="flag-fifa-16">
                                    <img src="http://ssl.gstatic.com/onebox/media/sports/logos/6SF7yEoB60bU5knw-M7R5Q_48x48.png">
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div class="box-award-mobile">
                        <img style="width: 120px;" src="/worldcup-pubg/img/item-01.png">
                    </div>
                </div>
                <div class="fifa-mobile-box-href">
                    <div class="box-fifa-16 box-fifa-match" style="margin: 83px 15px 0;">
                        <div class="check-box">

                        </div>
                        <div class="pull-left box-fifa-16-haft select-team team-mark" mid="14" tid="13">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="http://ssl.gstatic.com/onebox/media/sports/logos/DTqIL8Ba3KIuxGkpXw5ayA_48x48.png">
                                </li>
                                <li class="name-fifa-16">
                                    <span>ENG</span>
                                </li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft select-team team-mark" mid="14" tid="7">
                            <ul>
                                <li class="name-fifa-16">
                                    <span>CRO</span>
                                </li>
                                <li class="flag-fifa-16">
                                    <img src="http://ssl.gstatic.com/onebox/media/sports/logos/9toerdOg8xW4CRhDaZxsyw_48x48.png">
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="box-award-mobile">
                        <ul>
                            <li>สกิน</li>
                            <li>20</li>
                            <li>รางวัล</li>
                        </ul>
                        <div class="btn-col save-selected">
                            ยืนยันการเลือกทีม
                        </div>
                    </div>
                </div>


            </div>


            <!--            Final-->
            <div class="content-fifa-mobile-box" style="height: 348px;">
                <div class="content-fifa-mobile-box-final box-fifa-16 box-fifa-final" style="margin-top: 105px;">
                    <div class="box-fifa-match-final">
                        <div class="check-box" mid="15">

                        </div>

                        <div class="pull-left box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-default.jpg">
                                </li>
                                <li class="name-fifa-16"><span>???</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>


                                <li class="flag-fifa-16 text-right">
                                    <img src="/images/Flag-default.jpg">
                                </li>
                                <li class="name-fifa-16 pull-right"><span>???</span></li>
                            </ul>
                        </div>


                    </div>

                </div>

                <div class="box-award-mobile">
                    <div class="box-award-mobile" style="top: -134px; bottom: 0;">
                        <img style="width: 225px;" src="/worldcup-pubg/img/item-02.png">
                    </div>
                    <ul style="margin-top: 21px!important;">
                        <li>สกิน</li>
                        <li>30</li>
                        <li>รางวัล</li>
                    </ul>
                    <div class="btn-col save-selected">
                        ยืนยันการเลือกทีม
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container text-center" style="padding-top: 20px;">
    <a href="#content-event2"><img style="width: 100%;" src="/worldcup-pubg/img/thopy1.png"></a>
</div>

<div class="container container-list">
    <div class="col-lg-12">
        <h3>เงื่อนไขการร่วมชิงรางวัล</h3>
        <ol>
            <li>เริ่มทายผลผ่านเว็บไซต์ ได้ตั้งแต่วันที่ 10 กรกฏาคม 2561 สิ้นสุดวันที่ 15 กรกฏาคม 2561 โดย ทายผลผ่านเว็บไซต์หมดเขตส่งคำทายเวลา 23.59 น. โดยถือเวลาที่ทายผล ครั้งสุดท้ายเป็นสำคัญ
            </li>
            <li>ผู้ที่มีสิทธิ์ลุ้นรับรางวัล ต้องทายทีมเข้ารอบถูกต้องทั้งหมดในแต่ละรอบการแข่งขัน</li>
            <li>
                กกำหนดสุ่มจับรางวัล และประกาศรายชื่อผู้โชคดีแบ่งเป็น 4 รอบ ทางเว็บ ngoal.com และทางเพสเฟสบุ๊ก จบสกอร์
                <ul>
                    <li>รอบที่ 1 วันที่ 12 กรกฎาคม</li>
                    <li>รอบที่ 2 วันที่ 16 กรกฎาคม</li>
                </ul>
            </li>
            <li>ผู้ได้รับรางวัลมีสิทธิรับรางวัลที่มีมูลค่าสูงสุดเพียงรางวัลเดียวเท่านั้น</li>
            <li>แจกเฉพาะ ID ที่อยู่ใน PUBG Moblie เท่านั้น</li>
            <li>คำตัดสินของคณะกรรมการให้ถือเป็นที่สุด</li>
            <li>ทางเว็บไชต์ ngoal.com ขอสงวนสิทธิในการเปลี่ยนแปลงรายละเอียดหรือเงื่อนไขโดยไม่ต้องแจ้งให้ทราบล่วงหน้า
            </li>
        </ol>
    </div>

    <div class="col-lg-12">
        <h3>รางวัลสำหรับการทายผล</h3>
        <ul>
            <li>รางวัลรอบที่ 2 สกินเกม PUBG ประกอบด้วย ปืน, ร่มชูชีพ, เครื่องแต่งกาย มูลค่า 7,000 บาท จำนวน 50 รางวัล</li>
            <li>รางวัลรอบที่ 1 สกินเกม PUBG ประกอบด้วย ปืน, ร่มชูชีพ, เครื่องแต่งกาย มูลค่า 7,000 บาท จำนวน 50 รางวัล</li>
        </ul>
    </div>
    <div class="col-lg-12">
        <h3>วิธีการร่วมสนุกทายผลผ่านเว็บไซต์</h3>
        <ol>
            <li>เลือกทายประเทศที่จะเข้ารอบฟุตบอลโลก 2018 ของแต่ละรอบการแข่งขันผ่านเว็บไซต์ www.ngoal.com/worldcup-pubg</li>
            <li>ลงทะเบียนในเว็บไซต์ โดยการ login ด้วย facebook ของจริงที่ลงทะเบียนมาแล้วไม่ต่ำกว่า 3 เดือน</li>
            <li>ผู้ร่วมกิจกรรมไม่สามารถเปลี่ยนคำตอบได้เมื่อกดยืนยันคำตอบแล้ว
                แต่สามารถตอบเพิ่มได้ในคู่ที่ยังไม่ได้เล่นจนกว่าจะหมดเขตทายผลของแต่ละรอบ
            </li>
            <li>ผู้ร่วมกิจกรรม 1 คน มีสิทธิเพียง 1 สิทธิเท่านั้น</li>
        </ol>
    </div>
</div>
</body>
</html>

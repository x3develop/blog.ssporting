<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link href="/worldcup-pubg/css/worldcup2018-pubg.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style-new.css">
<!--    <link rel="stylesheet" href="/css/reset.css">-->

    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/mobile/js/bootstrap.js"></script>

</head>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>

<div id="content-event2" class="content-event">
    <div class="content-event-side-title">
        <div class="btn-sign-FB"></div>
    </div>
    <div class="content-fifa-world-cup">


        <!--        Semi-final-->
        <div class="columns-fifa03">
            <div class="title-columns-fifa">
                <h2>Semi-final</h2>
                <small>Thursday 10 July</small>
            </div>
            <div class="content-top-fifa-8" style="margin-top: 111px;">
                <div class="box-fifa-16 box-fifa-8">
                    <div class="check-box" mid="13">

                    </div>
                    <div class="pull-left box-fifa-16-haft">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>
                            <li class="name-fifa-16"><span>???</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>
                            <li class="name-fifa-16"><span>???</span></li>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>

                        </ul>
                    </div>

                </div>
            </div>
            <div class="content-top-fifa-8" style="margin-top: 113px;">
                <div class="box-fifa-16 box-fifa-8">
                    <div class="check-box" mid="14">

                    </div>
                    <div class="pull-left box-fifa-16-haft">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>
                            <li class="name-fifa-16"><span>???</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>
                            <li class="name-fifa-16"><span>???</span></li>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>

                        </ul>
                    </div>

                </div>
            </div>
            <div class="box-award" style="margin-top: 25px;">
                <img style="width: 124px; margin: 20px 0 18px 0;" src="/worldcup-pubg/img/item-01.png">
                <ul>
                    <li>สกิน</li>
                    <li>50</li>
                    <li>รางวัล</li>
                </ul>
            </div>
            <div class="btn-col save-selected">
                ยืนยันการเลือกทีม
            </div>
        </div>


        <!--        Final-->
        <div class="columns-fifa04">
            <div class="title-columns-fifa">
                <h2>Final</h2>
                <small>Thursday 14 July</small>
            </div>
            <div class="content-top-fifa-8" style="margin-top: 80px;">

                <div class="final-cup"><img src="/worldcup-pubg/img/world-cup-logo-1.png"></div>

                <div class="box-fifa-16 box-fifa-final" style="margin-top: 164px;">
                    <div class="check-box" mid="15" style="margin-top: 34px;">

                    </div>
                    <div class="pull-left box-fifa-16-haft">
                        <ul>
                            <li class="flag-fifa-16">
                                <img src="/images/Flag-default.jpg">
                            </li>
                            <li class="name-fifa-16"><span>???</span></li>
                        </ul>
                    </div>
                    <div class="pull-right box-fifa-16-haft">
                        <ul>


                            <li class="flag-fifa-16 text-right">
                                <img src="/images/Flag-default.jpg">
                            </li>
                            <li class="name-fifa-16 pull-right"><span>???</span></li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="box-award" style="margin-top: 54px;">
                <img src="/worldcup-pubg/img/item-02.png" style="width: 264px;margin-left: -15px;margin-top: -17px;">
                <ul>
                    <li>สกิน</li>
                    <li>50</li>
                    <li>รางวัล</li>
                </ul>
            </div>
            <div class="btn-col save-selected">
                ยืนยันการเลือกทีม
            </div>
        </div>
    </div>
</div>
<div class="container text-center" style="padding-top: 100px;">
   <a href="#content-event2"><img src="/worldcup-pubg/img/thopy1.png"></a>
</div>
<div class="container container-list">
    <div class="col-lg-12">
        <h3>เงื่อนไขการร่วมชิงรางวัล</h3>
        <ol>
            <li>เริ่มทายผลผ่านเว็บไซต์ ได้ตั้งแต่วันที่ 10 กรกฏาคม 2561 สิ้นสุดวันที่ 15 กรกฏาคม 2561 โดย ทายผลผ่านเว็บไซต์หมดเขตส่งคำทายเวลา 23.59 น. โดยถือเวลาที่ทายผล ครั้งสุดท้ายเป็นสำคัญ
            </li>
            <li>ผู้ที่มีสิทธิ์ลุ้นรับรางวัล ต้องทายทีมเข้ารอบถูกต้องทั้งหมดในแต่ละรอบการแข่งขัน</li>
            <li>
                กกำหนดสุ่มจับรางวัล และประกาศรายชื่อผู้โชคดีแบ่งเป็น 4 รอบ ทางเว็บ ngoal.com และทางเพสเฟสบุ๊ก จบสกอร์
                <ul>
                    <li>รอบที่ 1 วันที่ 12 กรกฎาคม</li>
                    <li>รอบที่ 2 วันที่ 16 กรกฎาคม</li>
                </ul>
            </li>
            <li>ผู้ได้รับรางวัลมีสิทธิรับรางวัลที่มีมูลค่าสูงสุดเพียงรางวัลเดียวเท่านั้น</li>
            <li>แจกเฉพาะ ID ที่อยู่ใน PUBG Moblie เท่านั้น</li>
            <li>คำตัดสินของคณะกรรมการให้ถือเป็นที่สุด</li>
            <li>ทางเว็บไชต์ ngoal.com ขอสงวนสิทธิในการเปลี่ยนแปลงรายละเอียดหรือเงื่อนไขโดยไม่ต้องแจ้งให้ทราบล่วงหน้า
            </li>
        </ol>
    </div>

    <div class="col-lg-12">
        <h3>รางวัลสำหรับการทายผล</h3>
        <ul>
            <li>รางวัลรอบที่ 2 สกินเกม PUBG ประกอบด้วย ปืน, ร่มชูชีพ, เครื่องแต่งกาย มูลค่า 7,000 บาท จำนวน 50 รางวัล</li>
            <li>รางวัลรอบที่ 1 สกินเกม PUBG ประกอบด้วย ปืน, ร่มชูชีพ, เครื่องแต่งกาย มูลค่า 7,000 บาท จำนวน 50 รางวัล</li>
        </ul>
    </div>
    <div class="col-lg-12">
        <h3>วิธีการร่วมสนุกทายผลผ่านเว็บไซต์</h3>
        <ol>
            <li>เลือกทายประเทศที่จะเข้ารอบฟุตบอลโลก 2018 ของแต่ละรอบการแข่งขันผ่านเว็บไซต์ www.ngoal.com/worldcup-pubg</li>
            <li>ลงทะเบียนในเว็บไซต์ โดยการ login ด้วย facebook ของจริงที่ลงทะเบียนมาแล้วไม่ต่ำกว่า 3 เดือน</li>
            <li>ผู้ร่วมกิจกรรมไม่สามารถเปลี่ยนคำตอบได้เมื่อกดยืนยันคำตอบแล้ว
                แต่สามารถตอบเพิ่มได้ในคู่ที่ยังไม่ได้เล่นจนกว่าจะหมดเขตทายผลของแต่ละรอบ
            </li>
            <li>ผู้ร่วมกิจกรรม 1 คน มีสิทธิเพียง 1 สิทธิเท่านั้น</li>
        </ol>
    </div>
</div>

</body>
</html>

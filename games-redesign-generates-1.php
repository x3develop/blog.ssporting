<?php
session_start();
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playBet.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playComment.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playReviewMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playLeague.php";
$playLeague = new playLeague();
$playMatch = new playMatch();
$playComment = new playComment();
$playReviewMatch = new playReviewMatch();
$playBet = new playBet();
$match=array();
$match = $playMatch->getFirstMathByTime();

if (!empty($match) and count($match)>1) {
    $matchView = $match[1];
}
//var_dump($matchView);
//exit;
//$matchResultsHome = $playMatch->getListMatchResults($matchView->team_home, 20);
//$matchResultsAway = $playMatch->getListMatchResults($matchView->team_away, 20);
//$userListHome = $playMatch->getUserBet($matchView->id,'home',5);
//$userListAway = $playMatch->getUserBet($matchView->id,'away',5);
//$percentBet = $playBet->percentBet($matchView->id);
//comment
$comment = $playMatch->getListComment($matchView->id);
$commentMini = array();
foreach ($comment as $key => $value) {
    if (!empty($value->countComment)) {
        $commentMini[$value->id] = $playComment->getMiniComment($matchView->id, $value->id);
    }
}

$commentHome = $playMatch->getListCommentByTeam($matchView->id, 'home');
$commentAway = $playMatch->getListCommentByTeam($matchView->id, 'away');
$heartList = $playComment->checkHeartByMatch($matchView->id);
$heartListByUser = $playComment->checkHeartByUser($matchView->id);
$reviewMatch = $playReviewMatch->getReviewAndUserMatchByMatchId($matchView->id);
$resultsReviewMatch = array();
$playReviewMatchList=array();
$reviewHome=0;
$reviewAway=0;
foreach ($reviewMatch as $key => $value) {
    if(!empty($value['user_review_id'])) {
        if ($value['team'] == "home") {
            $reviewHome++;
        } else if ($value['team'] == "away") {
            $reviewAway++;
        }
        $winRate = $playReviewMatch->getWinRate($value['user_review_id'], 30);
        if ($winRate["total"] != 0) {
            $resultsReviewMatch[$value['user_review_id']] = intval(100 - ((($winRate["total"] - $winRate["win"]) / $winRate["total"]) * 100));
        } else {
            $resultsReviewMatch[$value['user_review_id']] = 0;
        }
        $playReviewMatchList[$value['user_review_id']] = $playReviewMatch->getResultsReviewMatchByUserReview($value['user_review_id'], 15);
    }
}

if (isset($_SESSION['login'])) {
    $matchBet = $playBet->getTeamMatchById($_SESSION['login']['id'], $mid);
}

//HEAD-TO-HEAD
$dataHeadToHead = $playMatch->getHeadToHead($matchView->team_home, $matchView->team_away);
$dataLeagueByHeadToHead = $playMatch->getLeagueByHeadToHead($matchView->team_home, $matchView->team_away);
$dataFormGuideHome = $playMatch->getFormGuide($matchView->team_home, 50);
$dataLeagueFormGuideHome = $playMatch->getLeagueByFormGuide($matchView->team_home, 50);
$dataFormGuideAway = $playMatch->getFormGuide($matchView->team_away, 50);
$dataLeagueFormGuideAway = $playMatch->getLeagueByFormGuide($matchView->team_away, 50);
$dataNextMatchHome = $playMatch->getNextMatch($matchView->team_home, $matchView->id);
$dataNextMatchAway = $playMatch->getNextMatch($matchView->team_away, $matchView->id);
$tableLeague = $playLeague->checkTeamByLeague($matchView->league_id);
?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/games-redesign-generates.php'; ?>



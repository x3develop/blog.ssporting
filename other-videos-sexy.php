<?php
if(isset($_GET['galleryId'])) {
    header('Location: /sexy-football-videos?galleryId='.$_GET['galleryId']);
    exit();
}else{
    header('Location: /sexy-football-videos');
    exit();
}
?>
<?php
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/GalleryM.php';
$GalleryObject = new GalleryM();
if (isset($_GET['galleryId'])) {
    $dataVideo = array();
    $dataVideo = $GalleryObject->getMediaStoreByGalleryId($_GET['galleryId']);
    $dataRelatedVideos = $GalleryObject->getRelatedVideo($dataVideo[0]->tag, $dataVideo[0]->id, 4);
    $dataRelatedGallery = $GalleryObject->getMoreRecentedGallery($dataVideo[0]->tag, $dataVideo[0]->id, 4);
    $dataVideoMOthers = $GalleryObject->getOthersVideo(4);
    $videolistLoading = $GalleryObject->getMediaStoreByNotId($_GET['galleryId']);
} else {
    $dataVideo = array();
    $dataVideo = $GalleryObject->getOthersVideo(1);
    $dataRelatedVideos = $GalleryObject->getRelatedVideo($dataVideo[0]->video_tag, $dataVideo[0]->id, 4);
    $dataRelatedGallery = $GalleryObject->getMoreRecentedGallery($dataVideo[0]->video_tag, $dataVideo[0]->id, 4);
    $dataVideoMOthers = $GalleryObject->getOthersVideo("", 4);
    $videolistLoading = $GalleryObject->getMediaStoreByNotId($dataVideo[0]->id);
}
//var_dump($dataVideo);
//exit;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="shortcut icon" type="images/x-icon" href="images/favicon.ico.png">
    <title>Ngoal | Football sexy</title>
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="css/style-new.css">
    <link rel="stylesheet" href="css/articles.css">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/gallery.css">
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="/js/flogin/src/flogin.js"></script>
    <script src="/js/moment.js"></script>
    <meta property="fb:app_id" content="1630409860331473"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url"
          content="<?= (isset($_SERVER['HTTPS']) ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}" ?>"/>
    <meta property="og:title" content="<?php echo $dataVideo[0]->title ?>"/>
    <meta property="og:description" content="<?php echo $dataVideo[0]->desc ?>"/>
    <meta property="og:image"
          content="<?php echo((strpos($dataVideo[0]->thumbnail, "http://") === 0) ? $dataVideo[0]->thumbnail : "http://" . $_SERVER['SERVER_NAME'] . $dataVideo[0]->thumbnail); ?>"/>
    <!--    <meta property="og:video"  content="--><?php //echo $dataVideo[0]->content ?><!--" />-->
    <!--    <meta property="og:video:url"  content="--><?php //echo $dataVideo[0]->content ?><!--" />-->
    <!--    <meta property="og:video:secure_url"  content="--><?php //echo $dataVideo[0]->content ?><!--" />-->

    <!--    <meta property="og:type" content="website" />-->
    <!--    <meta property="og:image" content="http://api.ssporting.com/uploaded/gallery/1604/galleryImage_1604_19994.jpg" />-->
    <!--    <meta property="og:image:secure_url" content="http://api.ssporting.com/uploaded/gallery/1604/galleryImage_1604_19994.jpg" />-->
    <!--    <meta property="og:image:type" content="image/jpeg" />-->
    <!--    <meta property="og:image:width" content="100" />-->
    <!--    <meta property="og:image:height" content="100" />-->

    <!--    <meta property="og:url"           content="dev.ssporting.com" />-->
    <!--    <meta property="og:type"          content="website" />-->
    <!--    <meta property="og:title"         content="ssporting" />-->
    <!--    <meta property="og:description"   content="ssporting" />-->
    <!--    <meta property="og:image"         content="http://api.ssporting.com/uploaded/gallery/1604/galleryImage_1604_19994.jpg" />-->

    <?php include 'view/index/googletagmanager.php'; ?>
</head>
<body>

<style>
    div.twitter-video {
        max-height: none !important;
        max-width: none !important;
        min-width: none !important;
        min-height: none !important;
        width: 100% !important;
        height: 100% !important;
    }

    .box-large-videos {
        /*height: auto !important;*/
    }
</style>

<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
<!--index menu-->
<?php //include './view/index/menu.php'; ?>
<?php include './view/index/menu-all.php'; ?>
<?php //include 'view/menu/navbar-menu-v2-fixed.php'; ?>
<div style="clear: both;"></div>
<div id="Main-video" video_id="<?php echo $dataVideo[0]->id; ?>" style="">
    <div class="wrapper-news-articles news-articles-list mg-top-50" video_id="<?php echo $dataVideo[0]->id; ?>">

        <div class="wrap-content-cols-2">
            <div class="bg-video-sexy">
                <div class="box-large-videos-row">
                    <div class="box-large-videos">
                        <div id="box-large-videos-autoPlay">
                            <?php if ($dataVideo[0]->typeVideo == "dailymotion") { ?>
                                <iframe class="iframe_<?php echo $dataVideo[0]->id; ?>" id="large-videos-autoPlay"
                                        width="100%" height="700" data-autoplay="true"
                                        stop="<?php echo $dataVideo[0]->stopUrlIframe ?>"
                                        start="<?php echo $dataVideo[0]->urlIframe ?>"
                                        src="http://www.dailymotion.com/embed/video/<?php echo $dataVideo[0]->videokey ?>"
                                        frameborder="0"
                                        allowfullscreen></iframe>
                            <?php } elseif ($dataVideo[0]->typeVideo == "twitter") { ?>
                                <iframe class="iframe_<?php echo $dataVideo[0]->id; ?>" id="large-videos-autoPlay"
                                        data-autoplay="true"
                                        stop="<?php echo $dataVideo[0]->stopUrlIframe ?>"
                                        start="<?php echo $dataVideo[0]->urlIframe ?>"
                                        src="https://twitter.com/i/cards/tfw/v1/<?php echo $dataVideo[0]->videokey ?>"
                                        frameborder="0"
                                        allowfullscreen></iframe>
                            <?php } elseif ($dataVideo[0]->typeVideo == "facebook") { ?>
                                <iframe class="iframe_<?php echo $dataVideo[0]->id; ?>" id="large-videos-autoPlay"
                                        width="auto" height="530" data-autoplay="true"
                                        stop="<?php echo $dataVideo[0]->stopUrlIframe ?>"
                                        start="<?php echo $dataVideo[0]->urlIframe ?>"
                                        src="https://www.facebook.com/v2.3/plugins/video.php?allowfullscreen=true&container_width=800&href=<?php echo $dataVideo[0]->urlIframe ?>"
                                        frameborder="0"
                                        allowfullscreen></iframe>
                            <?php } elseif ($dataVideo[0]->typeVideo == "youtube") { ?>
                                <iframe class="iframe_<?php echo $dataVideo[0]->id; ?>" id="large-videos-autoPlay"
                                        data-autoplay="true"
                                        stop="<?php echo $dataVideo[0]->stopUrlIframe ?>"
                                        start="<?php echo $dataVideo[0]->urlIframe ?>"
                                        src="<?php echo $dataVideo[0]->urlIframe ?>"
                                        frameborder="0"
                                        allowfullscreen></iframe>
                            <?php } elseif ($dataVideo[0]->typeVideo == "streamable") { ?>
                                <iframe class="iframe_<?php echo $dataVideo[0]->id; ?>" id="large-videos-autoPlay"
                                        loop="0" data-autoplay="true"
                                        stop="<?php echo $dataVideo[0]->stopUrlIframe ?>"
                                        start="<?php echo $dataVideo[0]->urlIframe ?>"
                                        src="<?php echo $dataVideo[0]->urlIframe ?>"
                                        frameborder="0"
                                        allowfullscreen></iframe>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="load_video">
                    <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="wrap-content-cols-2">
                <div class="container">
                    <div class="content-large-videos bx-other-video-head">
                        <div class="bx-video-type"><?php echo $dataVideo[0]->video_tag ?></div>

                        <div class="contents-row">
                            <div class="col-sm-9 bx-description-videos main_articles"
                                 newsid="<?php echo $dataVideo[0]->video_id ?>"
                                 id="main_articles_<?php echo $dataVideo[0]->video_id ?>">
                                <h1><?php echo $dataVideo[0]->title ?></h1>
                                <div class="times-content mg-bt15">
                                    <span><?php echo date(' g:i A', strtotime($dataVideo[0]->create_datetime)) ?></span>
                                    <span><?php echo date(' F j, Y', strtotime($dataVideo[0]->create_datetime)) ?></span>
                                </div>
                                <?php if (!empty($dataVideo[0]->desc)) {
                                    echo $dataVideo[0]->desc ?>
                                <?php } else { ?>
                                    &nbsp;
                                <?php } ?>


                                <div class="">
                                    <?php include 'view/social/bx-social.php'; ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <img src="/images/kcap.jpg" style="width: 100%;">
                            </div>
                            <div class="col-sm-12 hide">
                                <div class="bx-publish-info">
                                    <table>
                                        <tr>
                                            <td><?php echo date(' g:i A', strtotime($dataVideo[0]->create_datetime)) ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo date(' F j, Y', strtotime($dataVideo[0]->create_datetime)) ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                    <!--                    Related-->
                    <div class="wrap-content-cols">
                        <div class="double-underline bx-tabs-related relate25">Related</div>
                        <div class="rows">
                            <div class="label-news-vertical label-vdo-vertical">Gallery</div>
                            <?php foreach ($dataRelatedGallery as $key => $value) { ?>
                                <div class="col-sm-3 related-news">
                                    <a href="/sexy-football-picture?id=<?php echo $value->id ?>">
                                    <div class="box-videos box-related box-related-sexy">
                                        <div class="bx-type-gallery">
                                            <i class="fa fa-picture-o"></i>
                                        </div>
                                        <div class="crop" style="background-image: url(<?php echo $value->thumbnail ?>)"></div>
                                    </div>
                                    <div class="title-owl-carousel title-h-videos">
                                        <a href="/sexy-football-picture?id=<?php echo $value->id ?>"><?php echo $value->title ?></a>
                                    </div>
                                        <div class="times-content"><span
                                                    class="timeMoreRecentedGallery"
                                                    date="<?php echo $value->create_datetime; ?>"></span>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                        <div style="clear: both;"></div>
                        <div class="rows">
                            <?php if (!empty($dataRelatedVideos)) { ?>
                                <div class="label-news-clip">Video</div>
                                <?php foreach ($dataRelatedVideos as $key => $value) { ?>
                                    <div class="col-sm-3">
                                        <a href="/sexy-football-videos?galleryId=<?php echo $value->id ?>">
                                            <div class="box-videos box-related box-related-sexy">
                                                <div class="bx-type-gallery">
                                                    <i class="fa fa-play"></i>
                                                </div>
                                                <div class="crop"
                                                     style="background-image: url(<?= $value->urlImg; ?>);"></div>
                                            </div>
                                            <div class="title-owl-carousel"><?php echo $value->title ?></div>
                                            <div class="times-content"><span
                                                        class="timeMoreRecentedGallery"
                                                        date="<?php echo $value->create_datetime; ?>"></span>
                                            </div>
                                        </a>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<?php //include 'footer.php';
include 'view/index/footer.php';
?>

<div id="Video-BlackUp">
    <?php foreach ($videolistLoading as $key => $value) { ?>
        <div videoId="<?php echo $value->id ?>" class="dataBlackUp" id="dataBlackUp_<?php echo $value->id ?>"
             style="display: none;"></div>
    <?php } ?>
</div>


<script>

    loadMoment();
    function loadMoment() {
        $('.timeMoreRecentedGallery').each(function (index) {
            if ($.isNumeric($(this).attr('date'))) {
                $(this).html(moment($(this).attr('date'), "X").fromNow())
            } else {
                $(this).html(moment($(this).attr('date'), "YYYY-MM-DD HH:mm:ss").fromNow())
            }
        })
    }

    $(document).ready(function () {
        var stickyNavTop = $('#navbar').offset().top;
        var countPage = 0;
        var arrayWidth = [];
        var arrayPosition = [];
        var arrayMenuBox = [];
        var arrayVideoId = [];
        var arrayMainMenuBox = [];
        arrayVideoId.push($('div#Main-video').attr('video_id'));
        $(".dataBlackUp").each(function (index) {
            arrayVideoId.push($(this).attr('videoId'));
        });

        function stickyNav() {
            var maxPage = 0;
            var windowHeight = jQuery(window).height();
            var scrollTop = $(window).scrollTop();
            if (scrollTop > stickyNavTop) {
                $('#navbarTopBar').css('top', 0);
            } else {
                $('#navbarTopBar').removeAttr('style');
            }
            $(".news-articles-list").each(function (index) {
                if (index <= countPage) {
                    var heightArticles = ($(this).height());
                    maxPage += heightArticles;
                    arrayWidth[index] = heightArticles;
//                    arrayVideoId[index]=$('#Main-video').attr('video_id');
                }
            });

            var reng = (maxPage - scrollTop) - windowHeight;
            if (reng < 100) {
                $(".dataBlackUp").each(function (index) {
                    if ($(this).html() !== "" && index == countPage) {
                        $('#Main-video').append($(this).html());
                        $(this).html("");

                        $(".dataBlackUp").each(function (index) {
                            if ((index == (countPage + 3))) {
                                var object = $(this);
                                $.ajax({
                                    url: "/sexy-football-videos-list",
                                    data: {galleryId: $(this).attr('videoId')},
                                    method: "GET",
                                    dataType: "html"
                                }).done(function (response) {
                                    object.html(response);
                                    loadMoment();
                                });
                            }
                        })
                    }
                });
            }
            if (maxPage > 0 && (scrollTop + 200) > maxPage) {
                console.log(arrayVideoId);
                if ($('.iframe_' + arrayVideoId[countPage]).length) {
                    $('.iframe_' + arrayVideoId[countPage]).attr('src', $('.iframe_' + arrayVideoId[countPage]).attr('stop'))
                }
                countPage += 1;
                if ($('.iframe_' + arrayVideoId[countPage]).length) {
                    $('.iframe_' + arrayVideoId[countPage]).attr('src', $('.iframe_' + arrayVideoId[countPage]).attr('start'))
                }
//                countPage+=1;
            } else if (maxPage > 0 && scrollTop > 0 && (scrollTop + 200) < (maxPage - arrayWidth[countPage])) {
                if ($('.iframe_' + arrayVideoId[countPage]).length) {
                    $('.iframe_' + arrayVideoId[countPage]).attr('src', $('.iframe_' + arrayVideoId[countPage]).attr('stop'))
                }
                countPage -= 1;
                if ($('.iframe_' + arrayVideoId[countPage]).length) {
                    $('.iframe_' + arrayVideoId[countPage]).attr('src', $('.iframe_' + arrayVideoId[countPage]).attr('start'))
                }
            }
            $('#loadingBar').css('width', (((((arrayWidth[countPage] - (maxPage - scrollTop)) - arrayWidth[countPage]) / arrayWidth[countPage]) * 100) + 100) + '%');
        }

        $(window).scroll(function () {
            stickyNav();
        });
    });

    $(".dataBlackUp").each(function (index) {
        if (index < 3) {
            var object = $(this);
            $.ajax({
                url: "/sexy-football-videos-list",
                data: {galleryId: $(this).attr('videoId')},
                method: "GET",
                dataType: "html"
            }).done(function (response) {
                object.html(response);
                loadMoment()
            });
        }
    })
</script>

</body>
</html>
<?php
session_start();
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playBet.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playComment.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playReviewMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playLeague.php";
$playLeague = new playLeague();
$playMatch = new playMatch();
$playComment = new playComment();
$playReviewMatch = new playReviewMatch();
$playBet = new playBet();
$mid = ($_REQUEST["mid"]) ? $_REQUEST['mid'] : 0;
$match = array();
$match = $playMatch->getFirstMathStatusBet(30);

if(!empty($mid) && file_exists($_SERVER["DOCUMENT_ROOT"]."/match/".$mid.".html") ){
    header('Location: /match/'.$mid.'.html');
    exit();
}elseif (!empty($mid)) {
    $matchView = $playMatch->getFirstMathById($mid)[0];
} elseif (!empty($match[0]->id)) {
    $matchView = $playMatch->getFirstMathById($match[0]->id)[0];
}else{
    header('Location: /');
    exit();
}
//var_dump($matchView);
//exit;
//$matchResultsHome = $playMatch->getListMatchResults($matchView->team_home, 20);
//$matchResultsAway = $playMatch->getListMatchResults($matchView->team_away, 20);

$userListHome = $playMatch->getUserBet($matchView->id, 'home', 5);
$userListAway = $playMatch->getUserBet($matchView->id, 'away', 5);
$percentBet = $playBet->percentBet($matchView->id);
//comment
//$comment = $playMatch->getListComment($matchView->id);
//$commentMini = array();
//foreach ($comment as $key => $value) {
//    if (!empty($value->countComment)) {
//        $commentMini[$value->id] = $playComment->getMiniComment($matchView->id, $value->id);
//    }
//}
//
//$commentHome = $playMatch->getListCommentByTeam($matchView->id, 'home');
//$commentAway = $playMatch->getListCommentByTeam($matchView->id, 'away');


$heartList = $playComment->checkHeartByMatch($matchView->id);
$heartListByUser = $playComment->checkHeartByUser($matchView->id);
$reviewMatch = $playReviewMatch->getReviewMatchByMatchId($matchView->id);
$resultsReviewMatch = array();
$playReviewMatchList = array();
foreach ($reviewMatch as $key => $value) {
//    $winRate = $playReviewMatch->getWinRate($value['user_review_id'], 30);
//    if ($winRate["total"] != 0) {
//        $resultsReviewMatch[$value['user_review_id']] = intval(100 - ((($winRate["total"] - $winRate["win"]) / $winRate["total"]) * 100));
//    } else {
//        $resultsReviewMatch[$value['user_review_id']] = 0;
//    }

//    $playReviewMatchList[$value['user_review_id']]=$playReviewMatch->getResultsReviewMatchByUserReview($value['user_review_id'],15);
}

if (isset($_SESSION['login'])) {
    $matchBet = $playBet->getTeamMatchById($_SESSION['login']['id'], $mid);
}

//HEAD-TO-HEAD
//$dataHeadToHead = $playMatch->getHeadToHead($matchView->team_home, $matchView->team_away);
//$dataLeagueByHeadToHead = $playMatch->getLeagueByHeadToHead($matchView->team_home, $matchView->team_away);

//$dataFormGuideHome = $playMatch->getFormGuide($matchView->team_home, 50);
//$dataLeagueFormGuideHome = $playMatch->getLeagueByFormGuide($matchView->team_home, 50);
//$dataFormGuideAway = $playMatch->getFormGuide($matchView->team_away, 50);
//$dataLeagueFormGuideAway = $playMatch->getLeagueByFormGuide($matchView->team_away, 50);

$dataNextMatchHome = $playMatch->getNextMatch($matchView->team_home, $matchView->id);
$dataNextMatchAway = $playMatch->getNextMatch($matchView->team_away, $matchView->id);
$tableLeague = $playLeague->checkTeamByLeague($matchView->league_id);
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="shortcut icon" type="images/x-icon" href="images/favicon.ico.png">
    <title>Ngoal | เกมถายผล</title>
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/livescore.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/game-redesign.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!--js-->
    <script src="js/jquery/dist/jquery.min.js"></script>
    <script src="js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="nanoscrollbar/nanoscrollbar.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/timeview.js"></script>
    <script src="js/game.js"></script>
    <script src="js/sly/sly.js"></script>
    <?php include 'view/index/googletagmanager.php'; ?>
</head>
<body>
<?php if (!empty($mid)) { ?>
    <input type="hidden" value="<?php echo $mid; ?>" id="mid">
    <input type="hidden" value="<?php echo $matchView->team_home; ?>" id="team_home">
    <input type="hidden" value="<?php echo $matchView->team_away; ?>" id="team_away">
<?php } ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div id="fade-in-index" class="box-fade">
    <div class="wrap-selected-team-play wrap-left-team">
        <p>คุณเลือกเล่นทีม</p>
        <img id="img-TeamHomePath" src="<?= $matchView->teamHomePath ?>">
        <h1 id="h1-TeamHomeEn"><?= $matchView->teamHomeEn ?></h1>
        <form id="bet-away-match">
            <input name="bet[time_match]" class="form-control" type="hidden" value="<?= $matchView->time_match; ?>">
            <input name="bet[user_id]" class="form-control" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
            <input name="bet[match_id]" class="form-control" type="hidden" value="<?= $matchView->id; ?>">
            <input name="bet[team]" type="hidden" value="home">
            <!--                <input name="bet[use_bet_coin]" type="hidden" value="750">-->
            <input name="bet[use_bet_gold]" type="hidden" value="0">
            <input name="bet[use_bet_coupon]" type="hidden" value="1">
            <input name="bet[status]" type="hidden" value="bet">

            <div class="box-commented" style="margin: 20px 0px;">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="box-input-select">
                            <textarea name="bet[comment]" class="form-control"
                                      placeholder="แสดงความคิดเห็น..."></textarea>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>

            <div class="box-button-send">
                <div class="col-sm-2"></div>
                <div class="col-sm-4">
                    <div class="box-input-select">
                        <img src="/images/coin/scoin100.png">
                        <select name="bet[use_bet_coin]">
                            <option value="1200">1200</option>
                            <option value="800">800</option>
                            <option value="400">400</option>
                            <option value="200">200</option>
                        </select>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box-button">
                        <button type="submit" class="btn btn-info btn-lg btn-block">Confirm</button>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
        </form>
        <p>เลือกจำนวน Coin ที่ใช้ในการทายผล</p>
        <div class="block-close-vote text-center">
            <div class="box-button close-tab close-tab-right">
                <button type="button" class="btn btn-default btn-lg btn-block" data-dismiss="modal">
                    ปิด
                </button>
            </div>
        </div>
    </div>
</div>
<div id="fade-in-right-index" class="box-fade">
    <!--        <div class="close-tab pull-left close-tab-left"><i class="fa fa-times" aria-hidden="true"></i></div>-->
    <div class="wrap-selected-team-play wrap-right-team">
        <p>คุณเลือกเล่นทีม</p>
        <img id="img-TeamAwayPath" src="<?= $matchView->teamAwayPath ?>">
        <h1 id="h1-TeamAwayEn"><?= $matchView->teamAwayEn ?></h1>
        <form id="bet-away-match">
            <input name="bet[time_match]" class="form-control" type="hidden" value="<?= $matchView->time_match; ?>">
            <input name="bet[user_id]" class="form-control" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
            <input name="bet[match_id]" class="form-control" type="hidden" value="<?= $matchView->id; ?>">
            <input name="bet[team]" type="hidden" value="away">
            <!--                <input name="bet[use_bet_coin]" type="hidden" value="750">-->
            <input name="bet[use_bet_gold]" type="hidden" value="0">
            <input name="bet[use_bet_coupon]" type="hidden" value="1">
            <input name="bet[status]" type="hidden" value="bet">
            <div class="box-commented" style="margin: 20px 0px;">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="box-input-select">
                            <textarea name="bet[comment]" class="form-control"
                                      placeholder="แสดงความคิดเห็น..."></textarea>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>

            <div class="box-button-send">
                <div class="col-sm-2"></div>
                <div class="col-sm-4">
                    <div class="box-input-select">
                        <img src="/images/coin/scoin100.png">
                        <select name="bet[use_bet_coin]">
                            <option value="1200">1200</option>
                            <option value="800">800</option>
                            <option value="400">400</option>
                            <option value="200">200</option>
                        </select>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box-button">
                        <button type="submit" class="btn btn-info btn-lg btn-block">Confirm</button>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
        </form>
        <p>เลือกจำนวน Coin ที่ใช้ในการทายผล</p>
        <div class="block-close-vote text-center">
            <div class="box-button close-tab close-tab-left">
                <button type="button" class="btn btn-default btn-lg btn-block" data-dismiss="modal">
                    ปิด
                </button>
            </div>
        </div>
    </div>
</div>
<div class="content-livescore-main content-game-redesign">
    <div class="container">
        <div class="row">
            <div class="content-list-livescore">
                <div class="col-md-12" id="liveScoreHead" style="width: 100%;overflow: hidden;height: 104px;">
                    <ul class="" id="ul-liveScoreHead" style="margin: 0">
                        <?php if (!empty($match)) {
                            foreach ($match as $key => $value) { ?>
                                <li data-key="<?php echo $key; ?>" data-matchId="<?= $value->id ?>"
                                    data-timematch="<?= $value->time_match ?>"
                                    class="match-slide <?= (($value->id == $mid) ? 'active' : '') ?>"
                                    style="float: left;">
                                    <a href="/games.php?mid=<?= $value->id ?>">
                                        <div class="col-md-12">
                                            <div class="thumbnail content-team-slide">
                                                <div class="pull-left">
                                                    <div class="content-team-slide-title">
                                                        <img src="<?= $value->teamHomePath ?>">
                                                        <div class="<?= ((!empty($value->handicap) && $value->handicap < 0) ? 'active' : '') ?>">
                                                            <div class="name-team-slide-top"><?= $value->teamHomeEn ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="content-team-slide-title">
                                                        <img src="<?= $value->teamAwayPath ?>">
                                                        <div class="<?= ((!empty($value->handicap) && $value->handicap > 0) ? 'active' : '') ?>">
                                                            <div class="name-team-slide-top"><?= $value->teamAwayEn ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pull-right">
                                                    <div class="time-live">
                                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        <p><?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?></p>
                                                    </div>
                                                    <div class="HDP-live">HDP</div>
                                                    <div class="num-live"><?= $value->handicap ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            <?php }
                        } ?>
                    </ul>
                </div>
            </div>
        </div>

        <?php if (!empty($matchView)) { ?>
            <div class="row" id="match-team-<?= $matchView->id ?>">

                <!--                title-->
                <div class="topic-league">
                    <h3><img src="<?= $matchView->path ?>"> <?= $matchView->name ?></h3>
                    <ul>
                        <li><i class="fa fa-clock-o"
                               aria-hidden="true"></i> <?= date("H:i", (strtotime($matchView->time_match) - (60 * 60))) ?>
                        </li>
                        <li>|</li>
                        <li><i class="fa fa-calendar"
                               aria-hidden="true"></i> <?= date("d/m/Y", (strtotime($matchView->time_match) - (60 * 60))) ?>
                        </li>
                    </ul>
                </div>

                <!--                Vote-->
                <div class="content-select-team-vote">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-xs-6 col-sm-6">
                                <div class="content-team-vote-home">
                                    <div class="box-vote-team">
                                        <div class="logo-vote-team-home">
                                            <img src="<?= $matchView->teamHomePath ?>">
                                        </div>
                                        <div class="btn-vote-team-home text-right">
                                            <?php if ($matchBet !== false && $matchBet == "away") { ?>

                                            <?php } else { ?>
                                                <a class="<?= (($matchBet !== false) ?: 'a-vote-team-home') ?>">VOTE</a>
                                            <?php } ?>
                                            <div class="bg-vote-team-home"></div>
                                        </div>
                                    </div>
                                    <div class="name-team-vote">
                                        <h3>
                                            <a style="color: #333;"
                                               class="<?php echo(($matchView->handicap < 0) ? 'text-blue' : '') ?>"><?= $matchView->teamHomeEn ?></a>
                                        </h3>
                                    </div>

                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6">
                                <div class="content-team-vote-away">

                                    <div class="box-vote-team">
                                        <div class="logo-vote-team-away">
                                            <img src="<?= $matchView->teamAwayPath ?>">
                                        </div>
                                        <div class="btn-vote-team-away">
                                            <?php if ($matchBet !== false && $matchBet == "home") { ?>
                                                <!--                                                            <a style="color: #FFFFFF" class="a-vote-team-home">VOTE</a>-->
                                            <?php } else { ?>
                                                <a class="<?= (($matchBet !== false) ?: 'a-vote-team-away') ?>">VOTE</a>
                                            <?php } ?>
                                            <div class="bg-vote-team-away"></div>
                                        </div>
                                    </div>
                                    <div class="name-team-vote">
                                        <h3>
                                            <a style="color: #333;"
                                               class="<?php echo(($matchView->handicap > 0) ? 'text-blue' : '') ?>"><?= $matchView->teamAwayEn ?></a>
                                        </h3>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>


        <!--                HDP-->
        <div class="row text-center">
            <ul class="content-HDP-game">
                <li class="box-last-match text-right">
                    <table>
                        <tr id="tr-results-left">

                        </tr>
                        <tr class="text-right">
                            <td></td>
                            <?php if (!empty($userListHome)) { ?>
                                <td>
                                    <img src="<?php echo((!empty($userListHome[4])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[4]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                                <td>
                                    <img src="<?php echo((!empty($userListHome[3])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[3]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                                <td>
                                    <img src="<?php echo((!empty($userListHome[2])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[2]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                                <td>
                                    <img src="<?php echo((!empty($userListHome[1])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[1]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                                <td>
                                    <img src="<?php echo((!empty($userListHome[0])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[0]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                            <?php } else { ?>
                                <td><img src="/images/avatar.png"></td>
                                <td><img src="/images/avatar.png"></td>
                                <td><img src="/images/avatar.png"></td>
                                <td><img src="/images/avatar.png"></td>
                                <td><img src="/images/avatar.png"></td>
                            <?php } ?>
                            <td>Player</td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td class="text-left"><span><?= number_format($percentBet['pHome'], 2) ?>%</span></td>
                            <td>
                                <div class="progress pull-right">
                                    <div class="progress-bar pull-right" role="progressbar"
                                         aria-valuenow="<?= number_format($percentBet['pHome'], 2) ?>"
                                         aria-valuemin="0" aria-valuemax="100"
                                         style="width: <?= number_format($percentBet['pHome'], 2) ?>%;">
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </li>
                <li class="box-HDP-game text-center">
                    <table>
                        <tr>
                            <td class="text-right"><p><?= $matchView->home_water_bill; ?></p></td>
                            <td><h2>[<?= $matchView->handicap ?>]</h2></td>
                            <td class="text-left"><p><?= $matchView->away_water_bill; ?></p></td>
                        </tr>
                    </table>
                </li>
                <li class="box-last-match text-left">
                    <table>
                        <tr id="tr-results-right">

                        </tr>
                        <tr>
                            <td>Player</td>
                            <?php if (!empty($userListAway)) { ?>
                                <td>
                                    <img src="<?php echo((!empty($userListAway[0])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[0]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                                <td>
                                    <img src="<?php echo((!empty($userListAway[1])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[1]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                                <td>
                                    <img src="<?php echo((!empty($userListAway[2])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[2]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                                <td>
                                    <img src="<?php echo((!empty($userListAway[3])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[3]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                                <td>
                                    <img src="<?php echo((!empty($userListAway[4])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[4]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                            <?php } else { ?>
                                <td><img src="/images/avatar.png"></td>
                                <td><img src="/images/avatar.png"></td>
                                <td><img src="/images/avatar.png"></td>
                                <td><img src="/images/avatar.png"></td>
                                <td><img src="/images/avatar.png"></td>
                            <?php } ?>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td>
                                <div class="progress" style="">
                                    <div class="progress-bar pull-left" role="progressbar"
                                         aria-valuenow="<?= number_format($percentBet['pAway'], 2) ?>"
                                         aria-valuemin="0" aria-valuemax="100"
                                         style="width: <?= number_format($percentBet['pAway'], 2) ?>%;">
                                    </div>
                                </div>
                            </td>
                            <td><span><?= number_format($percentBet['pAway'], 2) ?>%</span></td>
                        </tr>
                    </table>


                </li>
            </ul>
            <ul class="content-HDP-game content-HDP-game-responsive">
                <li class="box-HDP-game text-center">
                    <table>
                        <tbody>
                        <tr>
                            <td class="text-right">
                                <p><?= ((!empty($value->home_water_bill)) ? $value->home_water_bill : '-'); ?></p>
                            </td>
                            <td><h2>[<?= ((!empty($value->handicap)) ? $value->handicap : '-'); ?>
                                    ]</h2></td>
                            <td class="text-left">
                                <p><?= ((!empty($value->away_water_bill)) ? $value->away_water_bill : '-'); ?></p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </li>
                <li class="box-last-match text-center" style="border-right: solid 1px #092740;">
                    <table>
                        <tbody>
                        <tr>
                            <td colspan="6">Last 5 Match</td>
                        </tr>
                        <tr class="tr-btn-results-left">
                            <td class="btn-results-left" number="3"><i class="fa fa-caret-left"
                                                                       aria-hidden="true"></i></td>
                            <?php if (!empty($matchResultsHome[$value->id])) { ?>
                                <?php foreach (array_reverse($matchResultsHome[$value->id]) as $keyResults => $valueResults) { ?>
                                    <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                        <td scoreAway="<?= $valueResults->scoreAway ?>"
                                            scoreHome="<?= $valueResults->scoreHome ?>"
                                            number="<?= floor($keyResults / 5); ?>"
                                            class="<?= ((floor($keyResults / 5) == 3) ? '' : 'hide') ?> li-results">
                                            <img title="<?= $valueResults->time_match ?>"
                                                 src="/images/icon-stat/betlist_result/draw.png">
                                        </td>
                                    <?php } elseif ($valueResults->team_home == $value->team_home && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                        <td team_homeR="<?php echo $valueResults->team_home ?>"
                                            team_home="<?php echo $value->team_home; ?>"
                                            scoreAway="<?= $valueResults->scoreAway ?>"
                                            scoreHome="<?= $valueResults->scoreHome ?>"
                                            number="<?= floor($keyResults / 5); ?>"
                                            class="<?= ((floor($keyResults / 5) == 3) ? '' : 'hide') ?> li-results">
                                            <img title="<?= $valueResults->time_match ?>"
                                                 src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    <?php } elseif ($valueResults->team_away == $value->team_home && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                        <td team_homeR="<?php echo $valueResults->team_home ?>"
                                            team_home="<?php echo $value->team_home; ?>"
                                            scoreAway="<?= $valueResults->scoreAway ?>"
                                            scoreHome="<?= $valueResults->scoreHome ?>"
                                            number="<?= floor($keyResults / 5); ?>"
                                            class="<?= ((floor($keyResults / 5) == 3) ? '' : 'hide') ?> li-results">
                                            <img title="<?= $valueResults->time_match ?>"
                                                 src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    <?php } else { ?>
                                        <td team_homeR="<?php echo $valueResults->team_home ?>"
                                            team_home="<?php echo $value->team_home; ?>"
                                            scoreAway="<?= $valueResults->scoreAway ?>"
                                            scoreHome="<?= $valueResults->scoreHome ?>"
                                            number="<?= floor($keyResults / 5); ?>"
                                            class="<?= ((floor($keyResults / 5) == 3) ? '' : 'hide') ?> li-results">
                                            <img title="<?= $valueResults->time_match ?>"
                                                 src="/images/icon-stat/betlist_result/lose.png">
                                        </td>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>

                        </tr>

                        <tr>
                            <td colspan="6">Player</td>
                        </tr>
                        <tr class="text-center">
                            <td></td>
                            <?php if (!empty($userListHome[$value->id])) { ?>
                                <td>
                                    <img src="<?php echo((!empty($userListHome[$value->id][4])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[$value->id][4]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                                <td>
                                    <img src="<?php echo((!empty($userListHome[$value->id][3])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[$value->id][3]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                                <td>
                                    <img src="<?php echo((!empty($userListHome[$value->id][2])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[$value->id][2]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                                <td>
                                    <img src="<?php echo((!empty($userListHome[$value->id][1])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[$value->id][1]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                                <td>
                                    <img src="<?php echo((!empty($userListHome[$value->id][0])) ? 'https://graph.facebook.com/v2.8/' . $userListHome[$value->id][0]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                            <?php } else { ?>
                                <td><img src="/images/avatar.png"></td>
                                <td><img src="/images/avatar.png"></td>
                                <td><img src="/images/avatar.png"></td>
                                <td><img src="/images/avatar.png"></td>
                                <td><img src="/images/avatar.png"></td>
                            <?php } ?>

                        </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                        <tr>
                            <td class="text-left">
                                                    <span><?= number_format($percentBet[$value->id]['pHome'], 2) ?>
                                                        %</span></td>
                            <td>
                                <div class="progress pull-right">
                                    <div class="progress-bar pull-right" role="progressbar"
                                         aria-valuenow="<?= number_format($percentBet[$value->id]['pHome'], 2) ?>"
                                         aria-valuemin="0" aria-valuemax="100"
                                         style="width: <?= number_format($percentBet[$value->id]['pHome'], 2) ?>%;">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </li>
                <li class="box-last-match text-center">
                    <table>
                        <tbody>
                        <tr>
                            <td colspan="6">Last 5 Match</td>
                        </tr>
                        <tr class="tr-btn-results-right">
                            <?php if (!empty($matchResultsAway[$value->id])) { ?>
                                <?php foreach ($matchResultsAway[$value->id] as $keyResults => $valueResults) { ?>
                                    <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                        <td scoreAway="<?= $valueResults->scoreAway ?>"
                                            scoreHome="<?= $valueResults->scoreHome ?>"
                                            number="<?= floor($keyResults / 5); ?>"
                                            class="<?= ((floor($keyResults / 5) == 0) ? '' : 'hide') ?> li-results">
                                            <img title="<?= $valueResults->time_match ?>"
                                                 src="/images/icon-stat/betlist_result/draw.png">
                                        </td>
                                    <?php } elseif ($valueResults->team_home == $value->team_away && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                        <td scoreAway="<?= $valueResults->scoreAway ?>"
                                            scoreHome="<?= $valueResults->scoreHome ?>"
                                            number="<?= floor($keyResults / 5); ?>"
                                            class="<?= ((floor($keyResults / 5) == 0) ? '' : 'hide') ?> li-results">
                                            <img title="<?= $valueResults->time_match ?>"
                                                 src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                        <td scoreAway="<?= $valueResults->scoreAway ?>"
                                            scoreHome="<?= $valueResults->scoreHome ?>"
                                            number="<?= floor($keyResults / 5); ?>"
                                            class="<?= ((floor($keyResults / 5) == 0) ? '' : 'hide') ?> li-results">
                                            <img title="<?= $valueResults->time_match ?>"
                                                 src="/images/icon-stat/betlist_result/win.png">
                                        </td>
                                    <?php } else { ?>
                                        <td scoreAway="<?= $valueResults->scoreAway ?>"
                                            scoreHome="<?= $valueResults->scoreHome ?>"
                                            number="<?= floor($keyResults / 5); ?>"
                                            class="<?= ((floor($keyResults / 5) == 0) ? '' : 'hide') ?> li-results">
                                            <img title="<?= $valueResults->time_match ?>"
                                                 src="/images/icon-stat/betlist_result/lose.png"
                                        </td>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            <td class="btn-results-right" number="0"><i class="fa fa-caret-right"
                                                                        aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td colspan="6">Player</td>
                        </tr>
                        <tr>
                            <?php if (!empty($userListAway[$value->id])) { ?>
                                <td>
                                    <img src="<?php echo((!empty($userListAway[$value->id][0])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[$value->id][0]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                                <td>
                                    <img src="<?php echo((!empty($userListAway[$value->id][1])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[$value->id][1]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                                <td>
                                    <img src="<?php echo((!empty($userListAway[$value->id][2])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[$value->id][2]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                                <td>
                                    <img src="<?php echo((!empty($userListAway[$value->id][3])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[$value->id][3]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                                <td>
                                    <img src="<?php echo((!empty($userListAway[$value->id][4])) ? 'https://graph.facebook.com/v2.8/' . $userListAway[$value->id][4]->fb_uid . '/picture' : '/images/avatar.png'); ?>">
                                </td>
                            <?php } else { ?>
                                <td><img src="/images/avatar.png"></td>
                                <td><img src="/images/avatar.png"></td>
                                <td><img src="/images/avatar.png"></td>
                                <td><img src="/images/avatar.png"></td>
                                <td><img src="/images/avatar.png"></td>
                            <?php } ?>
                        </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <div class="progress" style="">
                                    <div class="progress-bar pull-left" role="progressbar"
                                         aria-valuenow="<?= number_format($percentBet[$value->id]['pAway'], 2) ?>"
                                         aria-valuemin="0" aria-valuemax="100"
                                         style="width: <?= number_format($percentBet[$value->id]['pAway'], 2) ?>%;">
                                    </div>
                                </div>
                            </td>
                            <td><span><?= number_format($percentBet[$value->id]['pAway'], 2) ?>
                                    %</span></td>
                        </tr>
                        </tbody>
                    </table>


                </li>
            </ul>
        </div>
    </div>
</div>
<!--ทรรศนะ-->
<?php if (!empty($reviewMatch)) { ?>
    <div class="wrap-preview-game">
        <div class="container">
            <div class="title-label-rota">REVIEW</div>
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox" style="height:240px;">
                    <div class="item active">
                        <?php foreach ($reviewMatch as $key => $value) { ?>
                            <?php if ($key <= 5) { ?>
                                <div class="col-sm-4 col-md-4">
                                    <div class="thumbnail box-preview" data-toggle="modal"
                                         data-target="#showReviewGuru">
                                        <div class="top-box-preview">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div class="box-guru-img pull-left">
                                                            <img src="<?= $value['path'] ?>">
                                                        </div>
                                                        <div class="pull-left">
                                                            <h5><?= $value['name'] ?></h5>
                                                            <div class="box-player-preview">
                                                                <?php if ($value['team'] == "home") { ?>
                                                                    <img src="<?= $value['homepath'] ?>">
                                                                    <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                                <?php } elseif ($value['team'] == "away") { ?>
                                                                    <img src="<?= $value['awaypath'] ?>">
                                                                    <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="pull-right">
                                                            <button type="button" class="btn btn-link">
                                                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                            </button>
                                                        </div>
                                                        <div class="pull-right">
                                                            <h3 class="user-review-<?php echo $value['user_review_id']; ?>">
                                                                %</h3>
                                                            <p>Win rate</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="button-box-preview">
                                            <p>
                                                <?= $value['review_text'] ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <?php if (count($reviewMatch) > 6) { ?>
                        <div class="item">
                            <?php foreach ($reviewMatch as $key => $value) { ?>
                                <?php if ($key > 5 && $key <= 11) { ?>
                                    <div class="col-sm-4 col-md-4">
                                        <div class="thumbnail box-preview" data-toggle="modal"
                                             data-target="#showReviewGuru">
                                            <div class="top-box-preview">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <div class="box-guru-img pull-left">
                                                                <img src="<?= $value['path'] ?>">
                                                            </div>
                                                            <div class="pull-left">
                                                                <h5><?= $value['name'] ?></h5>
                                                                <div class="box-player-preview">
                                                                    <?php if ($value['team'] == "home") { ?>
                                                                        <img src="<?= $value['homepath'] ?>">
                                                                        <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                                    <?php } elseif ($value['team'] == "away") { ?>
                                                                        <img src="<?= $value['awaypath'] ?>">
                                                                        <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="pull-right">
                                                                <button type="button" class="btn btn-link">
                                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                                </button>
                                                            </div>
                                                            <div class="pull-right">
                                                                <h3 class="user-review-<?php echo $value['user_review_id']; ?>">
                                                                    %</h3>
                                                                <p>Win rate</p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="button-box-preview">
                                                <p>
                                                    <?= $value['review_text'] ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php if (count($reviewMatch) > 12) { ?>
                        <div class="item">
                            <?php foreach ($reviewMatch as $key => $value) { ?>
                                <?php if ($key > 11 && $key <= 17) { ?>
                                    <div class="col-sm-4 col-md-4">
                                        <div class="thumbnail box-preview" data-toggle="modal"
                                             data-target="#showReviewGuru">
                                            <div class="top-box-preview">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <div class="box-guru-img pull-left">
                                                                <img src="<?= $value['path'] ?>">
                                                            </div>
                                                            <div class="pull-left">
                                                                <h5><?= $value['name'] ?></h5>
                                                                <div class="box-player-preview">
                                                                    <?php if ($value['team'] == "home") { ?>
                                                                        <img src="<?= $value['homepath'] ?>">
                                                                        <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                                    <?php } elseif ($value['team'] == "away") { ?>
                                                                        <img src="<?= $value['awaypath'] ?>">
                                                                        <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="pull-right">
                                                                <button type="button" class="btn btn-link">
                                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                                </button>
                                                            </div>
                                                            <div class="pull-right">
                                                                <h3 class="user-review-<?php echo $value['user_review_id']; ?>">
                                                                    %</h3>
                                                                <p>Win rate</p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="button-box-preview">
                                                <p>
                                                    <?= $value['review_text'] ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <!-- Indicators -->
                <ol class="carousel-indicators" style="bottom: 0; z-index: 0;">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>
            </div>
            <div class="box-add-review" data-toggle="modal" data-target="#addReview">
                <i class="fa fa-pencil" aria-hidden="true"></i>
            </div>
        </div>
    </div>
<?php } ?>

<div class="content-state-desktop">
    <!--สถิคิ-->
    <?php if (!empty($matchView)) { ?>
        <div class="topic-content-state" data-spy="affix" data-offset-top="600" data-offset-bottom="200">
            <ul>
                <li>
                    <div class="pull-right">
                        <h2 class="<?php echo(($matchView->handicap < 0) ? 'text-blue' : '') ?>"><?= $matchView->teamHomeEn ?></h2>
                        <img src="<?= $matchView->teamHomePath ?>">
                    </div>
                </li>
                <li>
                    <div class="text-center">
                        <h3><?= $matchView->name ?></h3>
                        <p>
                            <?= date("H:i", (strtotime($matchView->time_match) - (60 * 60))) ?>
                            | <?= date("d/m/Y", (strtotime($matchView->time_match) - (60 * 60))) ?>
                        </p>
                    </div>
                </li>
                <li>
                    <div class="pull-left">
                        <img src="<?= $matchView->teamAwayPath ?>">
                        <h2 class="<?php echo(($matchView->handicap > 0) ? 'text-blue' : '') ?>"><?= $matchView->teamAwayEn ?></h2>
                    </div>
                </li>
            </ul>
            <div class="arrow-down"></div>
        </div>
    <?php } ?>

    <!--HEAD TO HEAD-->
    <div class="content-state content-h2h col-lg-12" id="div-content-h2h">

    </div>

    <!--Form Guide-->
    <div class="content-state content-formGuide col-lg-12" id="div-content-formguide">

    </div>

    <!--Next Match-->
    <div class="content-state content-formGuide col-lg-12">
        <!--    <div class="head-state">-->
        <!--        <h2>Next Match</h2>-->
        <!--    </div>-->
        <div class="content-state-game" style="width: 181px;">
            <h2>Next Match</h2>
            <div class="head-state-game"></div>
        </div>
        <div class="col-sm-6">
            <div class="pull-right" style="width: 100%;">
                <!--            ตาราง-->
                <div class="section-next-match section-state-box text-left">
                    <div class="topic-section-state">
                        <table>
                            <tr>
                                <td>
                                    <h4><img src="<?= $matchView->teamHomePath ?>"> <?= $matchView->teamHomeEn ?></h4>
                                </td>
                                <td class="text-right">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="content-section-state row">
                        <div class="box-table-result-state" style="padding: 0 15px;">
                            <table class="table table-striped">
                                <?php foreach ($dataNextMatchHome as $value) { ?>
                                    <tr>
                                        <td>
                                            <span><img src="<?= $value->leaguePath ?>"> <?php echo $value->leagueName; ?></span>
                                        </td>
                                        <td>
                                            <p><i class="fa fa-calendar"
                                                  aria-hidden="true"></i> <?php date("d/m/Y", strtotime($value->time_match)); ?>
                                            </p>
                                        </td>
                                        <td class="text-right">
                                            <span><?php echo $value->teamHomeEn; ?> <img
                                                        src="<?= $value->teamHomePath ?>"></span>
                                        </td>
                                        <td>
                                            <div class="score-state">
                                                <?php date("H:i", strtotime($value->time_match)); ?>
                                            </div>
                                        </td>
                                        <td class="text-left">
                                            <span><img src="<?= $value->teamAweyPath ?>"> <?php echo $value->teamAweyEn; ?></span>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
        <div class="col-sm-6" style="background-color: #f1f1f1;">
            <div class="pull-left" style="width: 100%;">
                <!--            ตาราง-->
                <div class="section-next-match section-state-box text-left">
                    <div class="topic-section-state">
                        <table>
                            <tr>
                                <td>
                                    <h4><img src="<?= $matchView->teamAwayPath ?>"> <?= $matchView->teamAwayEn ?></h4>
                                </td>
                                <td class="text-right">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="content-section-state row">
                        <div class="box-table-result-state" style="padding: 0 15px;">
                            <table class="table table-striped">
                                <?php foreach ($dataNextMatchAway as $value) { ?>
                                    <tr>
                                        <td>
                                            <span><img src="<?= $value->leaguePath ?>"> <?php echo $value->leagueName; ?></span>
                                        </td>
                                        <td>
                                            <p><i class="fa fa-calendar"
                                                  aria-hidden="true"></i> <?php date("d/m/Y", strtotime($value->time_match)); ?>
                                            </p>
                                        </td>
                                        <td class="text-right">
                                            <span><?php echo $value->teamHomeEn; ?> <img
                                                        src="<?= $value->teamHomePath ?>"></span>
                                        </td>
                                        <td>
                                            <div class="score-state">
                                                <?php date("H:i", strtotime($value->time_match)); ?>
                                            </div>
                                        </td>
                                        <td class="text-left">
                                            <span><img src="<?= $value->teamAweyPath ?>"> <?php echo $value->teamAweyEn; ?></span>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>

    <!--Table-->
    <?php if (!empty($tableLeague) and count($tableLeague) <= 20) { ?>
        <div class="content-state content-formGuide col-lg-12">
            <div class="content-state-game" style="width: 123px;">
                <h2>Table</h2>
                <div class="head-state-game"></div>
            </div>
            <div class="col-sm-6">
                <div class="pull-right" style="width: 100%;">
                    <!--            ตาราง-->
                    <div class="section-table section-state-box text-left">
                        <div class="topic-section-state">
                            <table>
                                <tr>
                                    <td>
                                        <h4><img src="<?php echo $matchView->path; ?>"> <?php echo $matchView->name; ?>
                                        </h4>
                                    </td>
                                    <td class="text-right">
                                        <button onclick="showRank('home')" type="button" class="btn btn-link"><strong>Show
                                                All</strong>
                                            <span id="glyphicon-home" class="glyphicon glyphicon-triangle-bottom"
                                                  aria-hidden="true"></span>
                                        </button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="content-section-state row">
                            <div class="box-table-result-state" style="padding: 0 15px;">
                                <table class="table table-state">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Team</th>
                                        <th>P</th>
                                        <th>W</th>
                                        <th>D</th>
                                        <th>L</th>
                                        <th>GF</th>
                                        <th>GA</th>
                                        <th>GD</th>
                                        <th>PTS</th>
                                    </tr>
                                    </thead>
                                    <?php if (!empty($tableLeague)) {
                                        foreach ($tableLeague as $key => $value) {
                                            if (!empty($value)) { ?>
                                                <tr id=""
                                                    class="rank-home <?= ((!empty($value["name_en"]) and ($value["name_en"] == $matchView->teamHomeEn)) ? 'active' : '') ?> <?= ((!empty($value["name_en"]) and ($value["name_en"] == $matchView->teamHomeEn || $value["name_en"] == $matchView->teamAwayEn)) ? 'view' : 'hide') ?>">
                                                    <td class="rank-no"
                                                        style="color: #000000 !important;"><?php echo $key + 1; ?></td>
                                                    <td>
                                                        <img class="rank-logo"
                                                             src="<?= ((!empty($value["path"])) ? $value["path"] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                        <label class="rank-team"><?php echo $value["name_en"]; ?></label>
                                                    </td>
                                                    <td class="rank-play"><?php echo $value["PI"]; ?></td>
                                                    <td class="rank-win"><?php echo $value["win"]; ?></td>
                                                    <td class="rank-draw"><?php echo $value["draw"]; ?></td>
                                                    <td class="rank-lose"><?php echo $value["lose"]; ?></td>
                                                    <td class="rank-gf"><?php echo $value["GF"]; ?></td>
                                                    <td class="rank-ga"><?php echo $value["GA"]; ?></td>
                                                    <td class="rank-gd"><?php echo $value["GD"]; ?></td>
                                                    <td class="rank-pts"><?php echo $value["totalPts"]; ?></td>
                                                </tr>
                                            <?php }
                                        }
                                    } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear: both"></div>
            </div>
            <div class="col-sm-6 margin-top" style="background-color: #f1f1f1;">
                <div class="pull-left" style="width: 100%;">
                    <!--            ตาราง-->
                    <div class="section-table section-state-box text-left">
                        <div class="topic-section-state">
                            <table>
                                <tr>
                                    <td>
                                        <h4><img src="<?php echo $matchView->path; ?>"> <?php echo $matchView->name; ?>
                                        </h4>
                                    </td>
                                    <td class="text-right">
                                        <button onclick="showRank('away')" type="button" class="btn btn-link"><strong>Show
                                                All</strong>
                                            <span id="glyphicon-away" class="glyphicon glyphicon-triangle-bottom"
                                                  aria-hidden="true"></span>
                                        </button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="content-section-state row">
                            <div class="box-table-result-state" style="padding: 0 15px;">
                                <table class="table table-state">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Team</th>
                                        <th>P</th>
                                        <th>W</th>
                                        <th>D</th>
                                        <th>L</th>
                                        <th>GF</th>
                                        <th>GA</th>
                                        <th>GD</th>
                                        <th>PTS</th>
                                    </tr>
                                    </thead>
                                    <?php if (!empty($tableLeague)) {
                                        foreach ($tableLeague as $key => $value) {
                                            if (!empty($value)) { ?>
                                                <tr id=""
                                                    class="rank-away <?= ((!empty($value["name_en"]) and ($value["name_en"] == $matchView->teamAwayEn)) ? 'active' : '') ?> <?= ((!empty($value["name_en"]) and ($value["name_en"] == $matchView->teamHomeEn || $value["name_en"] == $matchView->teamAwayEn)) ? 'view' : 'hide') ?>">
                                                    <td class="rank-no"
                                                        style="color: #000000 !important;"><?php echo $key + 1; ?></td>
                                                    <td>
                                                        <img class="rank-logo"
                                                             src="<?= ((!empty($value["path"])) ? $value["path"] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                        <label class="rank-team"><?php echo $value["name_en"]; ?></label>
                                                    </td>
                                                    <td class="rank-play"><?php echo $value["PI"]; ?></td>
                                                    <td class="rank-win"><?php echo $value["win"]; ?></td>
                                                    <td class="rank-draw"><?php echo $value["draw"]; ?></td>
                                                    <td class="rank-lose"><?php echo $value["lose"]; ?></td>
                                                    <td class="rank-gf"><?php echo $value["GF"]; ?></td>
                                                    <td class="rank-ga"><?php echo $value["GA"]; ?></td>
                                                    <td class="rank-gd"><?php echo $value["GD"]; ?></td>
                                                    <td class="rank-pts"><?php echo $value["totalPts"]; ?></td>
                                                </tr>
                                            <?php }
                                        }
                                    } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear: both"></div>
            </div>
        </div>
    <?php } ?>
    <div style="clear: both;"></div>


</div>

<div class="content-state-responsive">

</div>


<!-- Modal Add -->
<div class="modal fade" id="addReview" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="width: 470px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มทรรศนะ</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ชือ</label>
                        <div class="col-sm-10">
                            <div class="clearfix">
                                <div class="box-guru-img pull-left">
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <h4>Thscore</h4>
                            </div>
                            <div class="clearfix">
                                <div class="box-user-img pull-left">
                                    <img src="/images/p1.png">
                                </div>
                                <h4>กระบี่มือหนึ่ง</h4>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ทีมที่เลือก</label>
                        <div class="col-sm-10">
                            <div class="box-player-preview">
                                <img src="/images/logo-team/manchester.png">
                                <span>@ Manchester UTD</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ทรรศนะ</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                <button type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่ม
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Preview Guru-->
<div class="modal fade" id="showReviewGuru" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">ทรรศนะทั้งหมด</h4>
            </div>
            <div class="modal-body" id="modalBody-showReviewGuru">

            </div>
        </div>
    </div>
</div>
<?php ?>
<script>
    google.charts.load("current", {packages: ["corechart"]});

    $(document).ready(function () {

        $.ajax({
            url: "games-content-h2h.php",
            method: "GET",
            data: {id: $("input#mid").val()},
            dataType: "html"
        }).done(function (response) {
            $('#div-content-h2h').html(response);
            viewHeadToHeadHome($('.h2h-tabs-team1').find('#h2h-h-max-match').html());
            viewHeadToHeadAway($('.h2h-tabs-team2').find('#h2h-a-max-match').html());
        });

        $.ajax({
            url: "games-content-formguide.php",
            method: "GET",
            data: {id: $("input#mid").val()},
            dataType: "html"
        }).done(function (response) {
            $('#div-content-formguide').html(response);
            viewFormGuideAway($('.formGuide-tabs-team2').find('#formguide-a-max-match').html());
            viewFormGuideHome($('.formGuide-tabs-team1').find('#formguide-h-max-match').html());
        });

        $("#ul-liveScoreHead").find('li.match-slide').each(function (index) {
            $(this).css('width', ($('#liveScoreHead').width() / 4));
        });

        $(window).resize(function () {
            $("#ul-liveScoreHead").find('li.match-slide').each(function (index) {
                $(this).css('width', ($('#liveScoreHead').width() / 4));
            });
        });

        var $frame = $('#liveScoreHead');
        var startAt = $("#liveScoreHead").find("li.active").first().attr('data-key');
        var frame = new Sly('#liveScoreHead', {
//        slidee:$frame.find('#ul-liveScoreHead'),
            horizontal: 1,
            itemNav: 'basic',
            smart: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 0,
//            scrollBar: $wrap.find('.scrollbar'),
            scrollBy: 1,
            speed: 2000,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,

            // Cycling
            cycleBy: 'items',
            cycleInterval: 6000,
            pauseOnHover: 1,

            // Buttons
//        forward: $wrap.find('.btnforward'),
//        backward: $wrap.find('.btnbackward'),
//        prev: $wrap.find('.btnbackward'),
//        next: $wrap.find('.btnforward'),
//        prevPage: $wrap.find('.backward'),
//        nextPage: $wrap.find('.forward'),

//        draggedClass:  'dragged', // Class for dragged elements (like SLIDEE or scrollbar handle).
            activeClass: 'active-game',  // Class for active items and pages.
//        disabledClass: 'disabled' // Class for disabled navigation elements.
        });
        frame.init();

        $.get("reviewMatch.php?id=" + $("input#mid").val(), function (data) {
            $("#modalBody-showReviewGuru").html(data);
        });

        $.get("resultsMatchHome.php?id=" + $("input#team_home").val(), function (data) {
            $("#tr-results-left").html(data);
        });

        $.get("resultsMatchAway.php?id=" + $("input#team_away").val(), function (data) {
            $("#tr-results-right").html(data);
        });

        $.ajax({
            url: "checkWinRate.php",
            method: "POST",
            dataType: "JSON"
        }).done(function (response) {
            $.each(response, function (key, val) {
                $(".user-review-" + key).html(val + "%");
                // console.log(key+" :: "+val)
            })
        });

        $(document).on('click', 'td.btn-results-left', function (e) {
            console.log(parseInt($(this).attr('number')));
            var number = parseInt($(this).attr('number'));
            if (number == 0) {
                number = 3;
            } else {
                number -= 1;
            }
            // console.log($(this).parent('tr').attr('class')+" :: "+number);
            $(this).parent('tr').find('.li-results').addClass('hide');
            $(this).parent('tr').find('[number=' + number + ']').removeClass('hide');
            $(this).parent('tr').find('span.total-results').html(20 - ((number * 5)));
            $(this).attr('number', number);
        });

        $(document).on('click', 'td.btn-results-right', function (e) {
            console.log(parseInt($(this).attr('number')));
            var number = parseInt($(this).attr('number'));
            if (number == 3) {
                number = 0;
            } else {
                number += 1;
            }
            // console.log($(this).parent('tr').attr('class')+" :: "+number);
            $(this).parent('tr').find('.li-results').addClass('hide');
            $(this).parent('tr').find('[number=' + number + ']').removeClass('hide');
            $(this).parent('tr').find('span.total-results').html(((number * 5) + 5));
            $(this).attr('number', number);
        });
    });
    function showRank(status) {
        if (status == "home") {
            if ($('span#glyphicon-home').hasClass('glyphicon-triangle-bottom')) {
                $('span#glyphicon-home').removeClass('glyphicon-triangle-bottom');
                $('span#glyphicon-home').addClass('glyphicon-triangle-top');
                $('tr.rank-home').removeClass('hide');
            } else if ($('span#glyphicon-home').hasClass('glyphicon-triangle-top')) {
                $('span#glyphicon-home').removeClass('glyphicon-triangle-top');
                $('span#glyphicon-home').addClass('glyphicon-triangle-bottom');
                $('tr.rank-home').not('.view').addClass('hide');
            }
        } else if (status == "away") {
            if ($('span#glyphicon-away').hasClass('glyphicon-triangle-bottom')) {
                $('span#glyphicon-away').removeClass('glyphicon-triangle-bottom');
                $('span#glyphicon-away').addClass('glyphicon-triangle-top');
                $('tr.rank-away').removeClass('hide');
            } else if ($('span#glyphicon-away').hasClass('glyphicon-triangle-top')) {
                $('span#glyphicon-away').removeClass('glyphicon-triangle-top');
                $('span#glyphicon-away').addClass('glyphicon-triangle-bottom');
                $('tr.rank-away').not('.view').addClass('hide');
            }
        }
    }

    function viewFormGuideAway(total=null) {
//        console.log("viewFormGuideAway");
        if (total == null) {
            total = parseInt($('#formguide-a-max-match').html());
        }
//        console.log(total);
        var resultWin = 0, resultLose = 0, resultDraw = 0, resultPWin = 0, resultPLose = 0, resultPDraw = 0, oddWin = 0,
            oddLose = 0, oddDraw = 0, oddPWin = 0, oddPLose = 0, oddPDraw = 0;
        $('.formGuide-tabs-team2').find('tr.tr-h2h').addClass('hide');

        for (i = 1; i <= total; i++) {

            if ($('input.checkbox_formguide_team2_all').is(':checked')) {
                $('.formGuide-tabs-team2').find('tr[number=' + i + ']').removeClass('hide');
            } else {
//                console.log($('.formGuide-tabs-team2').find('tr[number=' + i + ']').attr('league_id'));
                if ($('input.checkbox_formguide_team2_' + $('.formGuide-tabs-team2').find('tr[number=' + i + ']').attr('league_id')).is(':checked')) {
                    $('.formGuide-tabs-team2').find('tr[number=' + i + ']').removeClass('hide');
                }
            }
//            console.log($('.h2h-tabs-team1').find('tr[number='+i+']').length);
        }
        setTimeout(function () {
//            console.log("Total: "+$('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").length);
            resultWin = $('.formGuide-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-win').length;
            resultLose = $('.formGuide-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-lose').length;
            resultDraw = $('.formGuide-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-draw').length;
            oddWin = $('.formGuide-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-win').length;
            oddLose = $('.formGuide-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-lose').length;
            oddDraw = $('.formGuide-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-draw').length;

//            console.log(resultWin + "::" + resultLose + "::" + resultDraw + "::" + oddWin + "::" + oddLose + "::" + oddDraw);

            resultPWin = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultWin) / (resultWin + resultLose + resultDraw)) * 100));
            resultPLose = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultLose) / (resultWin + resultLose + resultDraw)) * 100));
            resultPDraw = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultDraw) / (resultWin + resultLose + resultDraw)) * 100));

            // console.log("resultPWin :: "+resultPWin+" resultPLose :: "+resultPLose+" resultPDraw: "+resultPDraw);

            if (oddWin !== 0 || oddLose !== 0 || oddDraw !== 0) {
                oddPWin = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddWin) / (oddWin + oddLose + oddDraw)) * 100));
                oddPLose = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddLose) / (oddWin + oddLose + oddDraw)) * 100));
                oddPDraw = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddDraw) / (oddWin + oddLose + oddDraw)) * 100));
            }

            $('.formGuide-tabs-team2').find('#resultPWin').html(resultWin);
            $('.formGuide-tabs-team2').find('#resultPLose').html(resultLose);
            $('.formGuide-tabs-team2').find('#resultPDraw').html(resultDraw);

            $('.formGuide-tabs-team2').find('#oddPWin').html(oddWin);
            $('.formGuide-tabs-team2').find('#oddPLose').html(oddLose);
            $('.formGuide-tabs-team2').find('#oddPDraw').html(oddDraw);

            google.charts.setOnLoadCallback(
                function () {
                    drawDonut('formguidedonut-A-result', [['result', 'value'], ['draw', resultPDraw], ['lose', resultPLose], ['win', resultPWin]]);
                    drawDonut('formguidedonut-A-oresult', [['result', 'value'], ['draw', oddPDraw], ['lose', oddPLose], ['win', oddPWin]]);
                }
            )

            $('.formGuide-tabs-team2').find('#formguide-a-max-match').html(total);
        }, 500);
    }
    function viewFormGuideHome(total=null) {
        if (total == null) {
            total = parseInt($('#formguide-h-max-match').html());
        }
//        console.log(total);
        var resultWin = 0, resultLose = 0, resultDraw = 0, resultPWin = 0, resultPLose = 0, resultPDraw = 0, oddWin = 0,
            oddLose = 0, oddDraw = 0, oddPWin = 0, oddPLose = 0, oddPDraw = 0;
        $('.formGuide-tabs-team1').find('tr.tr-h2h').addClass('hide');

        for (i = 1; i <= total; i++) {

            if ($('input.checkbox_formguide_team1_all').is(':checked')) {
                $('.formGuide-tabs-team1').find('tr[number=' + i + ']').removeClass('hide');
            } else {
                // console.log($('.formGuide-tabs-team1').find('tr[number=' + i + ']').attr('league_id'));
                if ($('input.checkbox_formguide_team1_' + $('.formGuide-tabs-team1').find('tr[number=' + i + ']').attr('league_id')).is(':checked')) {
                    $('.formGuide-tabs-team1').find('tr[number=' + i + ']').removeClass('hide');
                }
            }
//            console.log($('.h2h-tabs-team1').find('tr[number='+i+']').length);
        }
        setTimeout(function () {
//            console.log("Total: "+$('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").length);
            resultWin = $('.formGuide-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-win').length;
            resultLose = $('.formGuide-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-lose').length;
            resultDraw = $('.formGuide-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-draw').length;
            oddWin = $('.formGuide-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-win').length;
            oddLose = $('.formGuide-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-lose').length;
            oddDraw = $('.formGuide-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-draw').length;

            // console.log(resultWin + "::" + resultLose + "::" + resultDraw + "::" + oddWin + "::" + oddLose + "::" + oddDraw);

            resultPWin = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultWin) / (resultWin + resultLose + resultDraw)) * 100));
            resultPLose = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultLose) / (resultWin + resultLose + resultDraw)) * 100));
            resultPDraw = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultDraw) / (resultWin + resultLose + resultDraw)) * 100));

            if (oddWin !== 0 || oddLose !== 0 || oddDraw !== 0) {
                oddPWin = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddWin) / (oddWin + oddLose + oddDraw)) * 100));
                oddPLose = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddLose) / (oddWin + oddLose + oddDraw)) * 100));
                oddPDraw = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddDraw) / (oddWin + oddLose + oddDraw)) * 100));
            }

            $('.formGuide-tabs-team1').find('#resultPWin').html(resultWin);
            $('.formGuide-tabs-team1').find('#resultPLose').html(resultLose);
            $('.formGuide-tabs-team1').find('#resultPDraw').html(resultDraw);

            $('.formGuide-tabs-team1').find('#oddPWin').html(oddWin);
            $('.formGuide-tabs-team1').find('#oddPLose').html(oddLose);
            $('.formGuide-tabs-team1').find('#oddPDraw').html(oddDraw);

            google.charts.setOnLoadCallback(
                function () {
                    drawDonut('formguidedonut-H-result', [['result', 'value'], ['draw', resultPDraw], ['lose', resultPLose], ['win', resultPWin]]);
                    drawDonut('formguidedonut-H-oresult', [['result', 'value'], ['draw', oddPDraw], ['lose', oddPLose], ['win', oddPWin]]);
                }
            )

            $('.formGuide-tabs-team1').find('#formguide-h-max-match').html(total);
        }, 500);
    }
    function viewHeadToHeadAway(total=null) {
        if (total == null) {
            total = parseInt($('#h2h-a-max-match').html());
        }
//        console.log(total);
        var resultWin = 0, resultLose = 0, resultDraw = 0, resultPWin = 0, resultPLose = 0, resultPDraw = 0, oddWin = 0,
            oddLose = 0, oddDraw = 0, oddPWin = 0, oddPLose = 0, oddPDraw = 0;
        $('.h2h-tabs-team2').find('tr.tr-h2h').addClass('hide');

        for (i = 1; i <= total; i++) {

            if ($('input.checkbox_team2_all').is(':checked')) {
                $('.h2h-tabs-team2').find('tr[number=' + i + ']').removeClass('hide');
            } else {
//                console.log($('.h2h-tabs-team1').find('tr[number=' + i + ']').attr('league_id'));
                if ($('input.checkbox_team2_' + $('.h2h-tabs-team2').find('tr[number=' + i + ']').attr('league_id')).is(':checked')) {
                    $('.h2h-tabs-team2').find('tr[number=' + i + ']').removeClass('hide');
                }
            }
//            console.log($('.h2h-tabs-team1').find('tr[number='+i+']').length);
        }
        setTimeout(function () {
//            console.log("Total: "+$('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").length);
            resultWin = $('.h2h-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-win').length;
            resultLose = $('.h2h-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-lose').length;
            resultDraw = $('.h2h-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-draw').length;
            oddWin = $('.h2h-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-win').length;
            oddLose = $('.h2h-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-lose').length;
            oddDraw = $('.h2h-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-draw').length;

//            console.log(resultWin + "::" + resultLose + "::" + resultDraw + "::" + oddWin + "::" + oddLose + "::" + oddDraw);

            resultPWin = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultWin) / (resultWin + resultLose + resultDraw)) * 100));
            resultPLose = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultLose) / (resultWin + resultLose + resultDraw)) * 100));
            resultPDraw = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultDraw) / (resultWin + resultLose + resultDraw)) * 100));

            if (oddWin !== 0 || oddLose !== 0 || oddDraw !== 0) {
                oddPWin = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddWin) / (oddWin + oddLose + oddDraw)) * 100));
                oddPLose = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddLose) / (oddWin + oddLose + oddDraw)) * 100));
                oddPDraw = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddDraw) / (oddWin + oddLose + oddDraw)) * 100));
            }

            $('.h2h-tabs-team2').find('#resultPWin').html(resultWin);
            $('.h2h-tabs-team2').find('#resultPLose').html(resultLose);
            $('.h2h-tabs-team2').find('#resultPDraw').html(resultDraw);

            $('.h2h-tabs-team2').find('#oddPWin').html(oddWin);
            $('.h2h-tabs-team2').find('#oddPLose').html(oddLose);
            $('.h2h-tabs-team2').find('#oddPDraw').html(oddDraw);

            google.charts.setOnLoadCallback(
                function () {
                    drawDonut('h2hdonut-a-result', [['result', 'value'], ['draw', resultPDraw], ['lose', resultPLose], ['win', resultPWin]]);
                    drawDonut('h2hdonut-a-oresult', [['result', 'value'], ['draw', oddPDraw], ['lose', oddPLose], ['win', oddPWin]]);
                }
            )

            $('.h2h-tabs-team2').find('#h2h-a-max-match').html(total);
        }, 500);
    }
    function viewHeadToHeadHome(total=null) {
        if (total == null) {
            total = parseInt($('#h2h-h-max-match').html());
        }
//        console.log(total);
        var resultWin = 0, resultLose = 0, resultDraw = 0, resultPWin = 0, resultPLose = 0, resultPDraw = 0, oddWin = 0,
            oddLose = 0, oddDraw = 0, oddPWin = 0, oddPLose = 0, oddPDraw = 0;
        $('.h2h-tabs-team1').find('tr.tr-h2h').addClass('hide');

        for (i = 1; i <= total; i++) {

            if ($('input.checkbox_team1_all').is(':checked')) {
                $('.h2h-tabs-team1').find('tr[number=' + i + ']').removeClass('hide');
            } else {
//                console.log($('.h2h-tabs-team1').find('tr[number=' + i + ']').attr('league_id'));
                if ($('input.checkbox_team1_' + $('.h2h-tabs-team1').find('tr[number=' + i + ']').attr('league_id')).is(':checked')) {
                    $('.h2h-tabs-team1').find('tr[number=' + i + ']').removeClass('hide');
                }
            }
//            console.log($('.h2h-tabs-team1').find('tr[number='+i+']').length);
        }
        setTimeout(function () {
//            console.log("Total: "+$('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").length);
            resultWin = $('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-win').length;
            resultLose = $('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-lose').length;
            resultDraw = $('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-draw').length;
            oddWin = $('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-win').length;
            oddLose = $('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-lose').length;
            oddDraw = $('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-draw').length;

            console.log(resultWin + "::" + resultLose + "::" + resultDraw + "::" + oddWin + "::" + oddLose + "::" + oddDraw);

            resultPWin = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultWin) / (resultWin + resultLose + resultDraw)) * 100));
            resultPLose = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultLose) / (resultWin + resultLose + resultDraw)) * 100));
            resultPDraw = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultDraw) / (resultWin + resultLose + resultDraw)) * 100));

            if (oddWin !== 0 || oddLose !== 0 || oddDraw !== 0) {
                oddPWin = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddWin) / (oddWin + oddLose + oddDraw)) * 100));
                oddPLose = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddLose) / (oddWin + oddLose + oddDraw)) * 100));
                oddPDraw = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddDraw) / (oddWin + oddLose + oddDraw)) * 100));
            }

            $('.h2h-tabs-team1').find('#resultPWin').html(resultWin);
            $('.h2h-tabs-team1').find('#resultPLose').html(resultLose);
            $('.h2h-tabs-team1').find('#resultPDraw').html(resultDraw);

            $('.h2h-tabs-team1').find('#oddPWin').html(oddWin);
            $('.h2h-tabs-team1').find('#oddPLose').html(oddLose);
            $('.h2h-tabs-team1').find('#oddPDraw').html(oddDraw);

            google.charts.setOnLoadCallback(
                function () {
                    drawDonut('h2hdonut-h-result', [['result', 'value'], ['draw', resultPDraw], ['lose', resultPLose], ['win', resultPWin]]);
                    drawDonut('h2hdonut-h-oresult', [['result', 'value'], ['draw', oddPDraw], ['lose', oddPLose], ['win', oddPWin]]);
                }
            )

            $('.h2h-tabs-team1').find('#h2h-h-max-match').html(total);
        }, 500);
    }
    function drawDonut(element, data) {
        var data = google.visualization.arrayToDataTable(data);
        var options = {
            backgroundColor: 'transparent',
            pieStartAngle: 0,
            chartArea: {top: 7, height: '90%', width: '100%'},
            height: '180',
            width: '203',
            colors: ['#888', '#e40520', '#0091c8'],
            pieHole: 0.5,
            legend: 'none',
        };

        var chart = new google.visualization.PieChart(document.getElementById(element));
        chart.draw(data, options);
    }
</script>
<script src="/js/play/game.js"></script>
<?php //include 'footer.php';
include 'view/index/footer.php';
?>
</body>
</html>



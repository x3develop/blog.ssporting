<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="shortcut icon" type="images/x-icon" href="images/favicon.ico.png">
    <title>FIFA World Cup 2018</title>

    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/worldcup2018.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">

    <!--js-->
    <script src="js/jquery/dist/jquery.min.js"></script>
    <script src="js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>

</head>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="container">
    <div class="row">
        <div class="top-container-fifa-mobile">
            <img src="/images/top-bg-fifa-text.png">
            <p>ทายทีมเข้ารอบตั้งแต่รอบ 8 ทีม จนถึงรอบชิงชนะเลิศ โดยแบ่งเป็น 4 ช่วง
                เพื่อแจกของรางวัลมูลค่ากว่า 50,000 บาท</p>
            <div class="btn-sign-FB"></div>
        </div>
        <div class="container-fifa-mobile">


            <!--            Round 18-->
            <div class="content-fifa-mobile-box" style="height: 390px;">

                <div class="fifa-mobile-box-href">

                    <div class="box-fifa-16 box-fifa-match" style="margin: 69px 0 0 10px;">
                        <div class="check-box">
                            <img src="/images/check-false.png">
                        </div>
                        <div class="pull-left box-fifa-16-haft none-select">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-Spain.png">
                                </li>
                                <li class="name-fifa-16"><span>SPN</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="name-fifa-16"><span>POR</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Portugal-flag.png">
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div class="box-fifa-16 box-fifa-match" style="margin: 120px 0 0 10px;">
                        <div class="check-box">
                            <img src="/images/check-true.png">
                        </div>
                        <div class="pull-left box-fifa-16-haft none-select">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-Spain.png">
                                </li>
                                <li class="name-fifa-16"><span>SPN</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="name-fifa-16"><span>POR</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Portugal-flag.png">
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div class="box-fifa-16 box-fifa-match" style="margin: 178px 0 0 10px;">
                        <div class="check-box">
                            <img src="/images/check-true.png">
                        </div>
                        <div class="pull-left box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-Spain.png">
                                </li>
                                <li class="name-fifa-16"><span>SPN</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft none-select">
                            <ul>
                                <li class="name-fifa-16"><span>POR</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Portugal-flag.png">
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div class="box-fifa-16 box-fifa-match" style="margin: 233px 0 0 10px;">
                        <div class="check-box">
                            <img src="/images/check-true.png">
                        </div>
                        <div class="pull-left box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-Spain.png">
                                </li>
                                <li class="name-fifa-16"><span>SPN</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="name-fifa-16"><span>POR</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Portugal-flag.png">
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div class="box-award-mobile">
                        <img src="/images/mony_preview.png">
                    </div>
                </div>
                <div class="fifa-mobile-box-href">
                    <div class="box-fifa-16 box-fifa-match" style="margin: 69px 15px 0;">
                        <div class="check-box">
                            <img src="/images/check-false.png">
                        </div>
                        <div class="pull-left box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-Spain.png">
                                </li>
                                <li class="name-fifa-16"><span>SPN</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="name-fifa-16"><span>POR</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Portugal-flag.png">
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div class="box-fifa-16 box-fifa-match" style="margin: 120px 15px 0;">
                        <div class="check-box">
                            <img src="/images/check-true.png">
                        </div>
                        <div class="pull-left box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-Spain.png">
                                </li>
                                <li class="name-fifa-16"><span>SPN</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="name-fifa-16"><span>POR</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Portugal-flag.png">
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div class="box-fifa-16 box-fifa-match" style="margin: 178px 15px 0;">
                        <div class="check-box">
                            <img src="/images/check-true.png">
                        </div>
                        <div class="pull-left box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-Spain.png">
                                </li>
                                <li class="name-fifa-16"><span>SPN</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="name-fifa-16"><span>POR</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Portugal-flag.png">
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div class="box-fifa-16 box-fifa-match" style="margin: 233px 15px 0;">
                        <div class="check-box">
                            <img src="/images/check-true.png">
                        </div>
                        <div class="pull-left box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-Spain.png">
                                </li>
                                <li class="name-fifa-16"><span>SPN</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="name-fifa-16"><span>POR</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Portugal-flag.png">
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="box-award-mobile">
                        <h4>เงินสด 2,000 บาท</h4>
                        <div class="btn-col">
                            ยืนยันการเลือกทีม
                        </div>
                    </div>
                </div>


            </div>


            <!--            Round 8-->
            <div class="content-fifa-mobile-box" style="height: 308px;">

                <div class="fifa-mobile-box-href">

                    <div class="box-fifa-16 box-fifa-match" style="margin: 69px 0 0 10px;">
                        <div class="check-box">
                            <img src="/images/check-false.png">
                        </div>
                        <div class="pull-left box-fifa-16-haft none-select">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-Spain.png">
                                </li>
                                <li class="name-fifa-16"><span>SPN</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="name-fifa-16"><span>POR</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Portugal-flag.png">
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div class="box-fifa-16 box-fifa-match" style="margin: 120px 0 0 10px;">
                        <div class="check-box">
                            <img src="/images/check-true.png">
                        </div>
                        <div class="pull-left box-fifa-16-haft none-select">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-Spain.png">
                                </li>
                                <li class="name-fifa-16"><span>SPN</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="name-fifa-16"><span>POR</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Portugal-flag.png">
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div class="box-award-mobile">
                        <img src="/images/shirt_preview.png">
                    </div>
                </div>
                <div class="fifa-mobile-box-href">
                    <div class="box-fifa-16 box-fifa-match" style="margin: 69px 15px 0;">
                        <div class="check-box">
                            <img src="/images/check-false.png">
                        </div>
                        <div class="pull-left box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-Spain.png">
                                </li>
                                <li class="name-fifa-16"><span>SPN</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="name-fifa-16"><span>POR</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Portugal-flag.png">
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div class="box-fifa-16 box-fifa-match" style="margin: 120px 15px 0;">
                        <div class="check-box">
                            <img src="/images/check-true.png">
                        </div>
                        <div class="pull-left box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-Spain.png">
                                </li>
                                <li class="name-fifa-16"><span>SPN</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="name-fifa-16"><span>POR</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Portugal-flag.png">
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="box-award-mobile">
                        <h4>เสื้อลิขสิทธิ์แท้มูลค่า 5,000 บาท</h4>
                        <div class="btn-col">
                            ยืนยันการเลือกทีม
                        </div>
                    </div>
                </div>


            </div>


            <!--            Semi-finals-->
            <div class="content-fifa-mobile-box" style="height: 262px;">

                <div class="fifa-mobile-box-href">

                    <div class="box-fifa-16 box-fifa-match" style="margin: 69px 0 0 10px;">
                        <div class="check-box">
                            <img src="/images/check-false.png">
                        </div>
                        <div class="pull-left box-fifa-16-haft none-select">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-Spain.png">
                                </li>
                                <li class="name-fifa-16"><span>SPN</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="name-fifa-16"><span>POR</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Portugal-flag.png">
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div class="box-award-mobile">
                        <img src="/images/ps4-preview.png">
                    </div>
                </div>
                <div class="fifa-mobile-box-href">
                    <div class="box-fifa-16 box-fifa-match" style="margin: 69px 15px 0;">
                        <div class="check-box">
                            <img src="/images/check-false.png">
                        </div>
                        <div class="pull-left box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-Spain.png">
                                </li>
                                <li class="name-fifa-16"><span>SPN</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="name-fifa-16"><span>POR</span></li>
                                <li class="flag-fifa-16">
                                    <img src="/images/Portugal-flag.png">
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="box-award-mobile">
                        <h4>PS4 มูลค่า 15,000 บาท</h4>
                        <div class="btn-col">
                            ยืนยันการเลือกทีม
                        </div>
                    </div>
                </div>


            </div>


            <!--            Final-->
            <div class="content-fifa-mobile-box" style="height: 348px;">
                <div class="content-fifa-mobile-box-final box-fifa-16 box-fifa-final" style="margin-top: 98px;">
                    <div class="box-fifa-match-final">
                        <div class="check-box">
                            <img src="/images/check-true.png">
                        </div>

                        <div class="pull-left box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16">
                                    <img src="/images/Flag-default.jpg">
                                </li>
                                <li class="name-fifa-16"><span>???</span></li>
                            </ul>
                        </div>
                        <div class="pull-right box-fifa-16-haft">
                            <ul>
                                <li class="flag-fifa-16 text-right">
                                    <img src="/images/Flag-default.jpg">
                                </li>
                                <li class="name-fifa-16 pull-right"><span>???</span></li>
                            </ul>
                        </div>
                    </div>

                </div>

                <div class="box-award-mobile" style="margin-top: 100px;">
                    <img src="/images/iphone_preview.png">
                    <h4>IPhone X มูลค่า 35,000 บาท</h4>
                    <div class="btn-col">
                        ยืนยันการเลือกทีม
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
</body>
</html>
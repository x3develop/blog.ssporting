<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playBet.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playChannelMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playReviewMatch.php";
$playBet = new playBet();
$playMatch = new playMatch();
$playReviewMatch = new playReviewMatch();
$playChannelMatch = new playChannelMatch();
$matchToday = $playMatch->getByReviewToday(30);
$matchTomorrow = $playMatch->getByReviewTomorrow(30);
$matchYesterday = $playMatch->getByReviewYesterday(30);
?>
<div class="main-review">
    <div class="container-review">
        <div class="text-title">
            <h4>ทีเด็ด ทรรศนะ</h4>
            <div class="pull-right">
                <div class="date-review btn-group" role="group">
                    <button id="btn-review-Yesterday" class="btn btn-sm btn-default btn-review">Yesterday</button>
                    <button id="btn-review-Today" class="btn btn-sm btn-default btn-review">Today</button>
                    <button id="btn-review-Tomorrow" class="btn btn-sm btn-default btn-review">Tomorrow</button>
                </div>
                <div class="hamburger-review inline-box inline-box-middle" id="hamburger" onclick="toggleSidenav();"></div>
            </div>

        </div>

        <div class="div-review" id="div-review-Today">
            <?php if (!empty($matchToday)) { ?>
                <?php foreach ($matchToday as $key => $value) {
//                    if($key==0){
                    $matchPlayChannelMatch = $playChannelMatch->getChannel($value->match_7m_id);
                    $matchResultsHome = $playMatch->getListMatchResults($value->team_home, 5);
                    $matchResultsAway = $playMatch->getListMatchResults($value->team_away, 5);
                    $percentBet = $playBet->percentBet($value->match_id);
                    $reviewMatchAwayByMatchId = $playReviewMatch->getReviewAndUserMatchAwayByMatchIdAndLimit($value->match_id, 3);
                    $reviewMatchHomeByMatchId = $playReviewMatch->getReviewAndUserMatchHomeByMatchIdAndLimit($value->match_id, 3); ?>
                    <?php if ($value->status == "bet" || $value->status == "create") { ?>
                        <div class="content-review">
                            <div class="top-section">
                                <a class="link_wrap" href="/games?mid=<?php echo $value->match_id; ?>"></a>
                                <div class="home-section">
                                    <div class="vote-home-section">
                                        <a class="link_wrap" href="/games?mid=<?php echo $value->match_id; ?>"></a>
                                        VOTE
                                    </div>
                                    <div class="content-home-section text-right">
                                        <div class="topic-event"><?php echo $value->name; ?></div>
                                        <div class="detail-team-home">
                                            <div class="detail-team-home-state">
                                                <h4><?php echo $value->teamHomeEn; ?></h4>
                                                <div class="box-progress">
                                                    <span>Vote <?= number_format($percentBet['pHome']); ?>%</span>
                                                    <div class="progress">
                                                        <div class="progress-bar pull-right progress-bar-striped active"
                                                             role="progressbar"
                                                             aria-valuenow="<?= number_format($percentBet['pHome'], 2); ?>"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width: <?= number_format($percentBet['pHome'], 2); ?>%">

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="detail-team-home-logo">
                                                <img src="<?= $value->teamHomePath ?>">
                                            </div>
                                        </div>
                                        <ul class="list-state-match pull-right">
                                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                            <?php if (!empty($matchResultsHome)) { ?>
                                                <?php foreach (array_reverse($matchResultsHome) as $keyResults => $valueResults) { ?>
                                                    <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="draw-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_home == $value->team_home && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="loss-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                            <li class="text-match">Last 5 matches</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="hdp-section text-center">
                                    <a class="link_wrap" href="/games?mid=<?php echo $value->match_id; ?>"></a>
                                    <strong>HDP</strong>
                                    <h2><?php echo $value->handicap; ?></h2>
                                    <div class="box-water-text">
                                        <div><?php echo number_format($value->home_water_bill,2); ?></div>
                                        <div>|</div>
                                        <div><?php echo number_format($value->away_water_bill,2); ?></div>
                                    </div>
                                    <span class="time-match"><?php echo date("H:i", (strtotime($value->time_match) - (60 * 60))) ?></span>

                                </div>
                                <div class="away-section">
                                    <div class="content-home-section content-away-section pull-left">
                                        <div class="topic-live-on">
                                            <?php if (!empty($matchPlayChannelMatch)) { ?>
                                                <?php foreach ($matchPlayChannelMatch as $keyC => $valueC) { ?>
                                                    <img src="<?php echo $valueC->channel_path; ?>">
                                                    <?php if ($keyC != count($matchPlayChannelMatch)) { ?>
                                                        <div>|</div>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="detail-team-home detail-team-away">
                                            <div class="detail-team-home-logo">
                                                <img src="<?php echo $value->teamAwayPath ?>">
                                            </div>
                                            <div class="detail-team-home-state detail-team-away-state">
                                                <h4><?php echo $value->teamAwayEn; ?></h4>
                                                <div class="box-progress ">
                                                    <div class="progress">
                                                        <div class="progress-bar pull-left progress-bar-danger active"
                                                             role="progressbar"
                                                             aria-valuenow="<?= number_format($percentBet['pHome'], 2); ?>"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width: <?= number_format($percentBet['pHome'], 2); ?>%">
                                                        </div>
                                                    </div>
                                                    <span>Vote <?= number_format($percentBet['pHome']); ?>%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="list-state-match pull-left">
                                            <li class="text-match">Last 5 matches</li>
                                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                            <?php if (!empty($matchResultsAway)) { ?>
                                                <?php foreach ($matchResultsAway as $keyResults => $valueResults) { ?>
                                                    <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="draw-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_home == $value->team_away && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="win-ste"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="loss-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="vote-away-section">
                                        <a class="link_wrap" href="/games?mid=<?php echo $value->match_id; ?>"></a>
                                        VOTE
                                    </div>
                                </div>
                            </div>
                            <section class="slide">
                                <div class="review-section">
                                    <a class="link_wrap"
                                       href="/review-infomation.php?match_id=<?php echo $value->match_id; ?>"></a>
                                    <!-- review home-->
                                    <div class="box-review-section">
                                        <div class="home-review-section">
                                            <?php foreach ($reviewMatchHomeByMatchId as $key => $value) { ?>
                                                <div class="box-content-review">
                                                    <div class="top-content-review pull-left">
                                                        <div>
                                                            <img style="<?php echo((!empty($value['path'])) ? '' : 'width: 40px;'); ?>"
                                                                 src="<?= ((!empty($value['path'])) ? $value['path'] : 'https://graph.facebook.com/v2.8/' . $value['user_fb_uid'] . '/picture'); ?>">
                                                        </div>
                                                        <div>
                                                            <h5><?= ((!empty($value['name'])) ? $value['name'] : $value['user_name']); ?></h5>
                                                            <?php if ($value['team'] == "home") { ?>
                                                                <img src="<?= $value['homepath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                            <?php } elseif ($value['team'] == "away") { ?>
                                                                <img src="<?= $value['awaypath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="winRate-box">
                                                            <div class="bg-winRate-box">
                                                                <h3 class="user-review-<?php echo $value['user_review_id']; ?>">
                                                                    %</h3>
                                                                <p>Win Rate</p>
                                                            </div>
                                                            <div class="bg-day-box">
                                                                <h3 class="user-review-day-<?php echo $value['user_review_id']; ?>"><?php echo date("t"); ?></h3>
                                                                <p>Days</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="full-review-caption">
                                                        <p><?= $value['review_text'] ?></p>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <!-- review away-->
                                        <div class="away-review-section">
                                            <?php foreach ($reviewMatchAwayByMatchId as $key => $value) { ?>
                                                <div class="box-content-review">
                                                    <div class="top-content-review pull-left">
                                                        <div>
                                                            <img style="<?php echo((!empty($value['path'])) ? '' : 'width: 40px;'); ?>"
                                                                 src="<?= ((!empty($value['path'])) ? $value['path'] : 'https://graph.facebook.com/v2.8/' . $value['user_fb_uid'] . '/picture'); ?>">
                                                        </div>
                                                        <div>
                                                            <h5><?= ((!empty($value['name'])) ? $value['name'] : $value['user_name']); ?></h5>
                                                            <?php if ($value['team'] == "home") { ?>
                                                                <img src="<?= $value['homepath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                            <?php } elseif ($value['team'] == "away") { ?>
                                                                <img src="<?= $value['awaypath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="winRate-box">
                                                            <div class="bg-winRate-box">
                                                                <h3 class="user-review-<?php echo $value['user_review_id']; ?>">
                                                                    %</h3>
                                                                <p>Win Rate</p>
                                                            </div>
                                                            <div class="bg-day-box">
                                                                <h3 class="user-review-day-<?php echo $value['user_review_id']; ?>"><?php echo date("t"); ?></h3>
                                                                <p>Days</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="full-review-caption">
                                                        <p><?= $value['review_text'] ?></p>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="full-width text-center more-box-page">
                                        <a class="btn btn-primary"
                                           href="/review-infomation.php?match_id=<?php echo $value->match_id; ?>"
                                           role="button">อ่านทั้งหมด</a>
                                    </div>
                                </div>

                            </section>
                        </div>
                    <?php } else if ($value->status == "fullTime") { ?>
                        <div class="content-review content-review-endmatch">
                            <div class="top-section">
                                <div class="home-section">
                                    <div class="vote-home-section">
                                        <?php echo $value->home_end_time_score; ?>
                                    </div>
                                    <div class="content-home-section text-right">
                                        <div class="topic-event"><?php echo $value->name; ?></div>
                                        <div class="detail-team-home">
                                            <div class="detail-team-home-state">
                                                <h4><?php echo $value->teamHomeEn; ?></h4>
                                                <div class="box-progress">
                                                    <span>Vote <?php echo number_format($percentBet['pHome']); ?>
                                                        %</span>
                                                    <div class="progress">
                                                        <div class="progress-bar pull-right progress-bar-striped active"
                                                             role="progressbar"
                                                             aria-valuenow="<?php echo number_format($percentBet['pHome'], 2); ?>"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width: <?php echo number_format($percentBet['pHome'], 2); ?>%">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="detail-team-home-logo">
                                                <img src="<?php echo $value->teamHomePath; ?>">
                                            </div>

                                        </div>
                                        <ul class="list-state-match pull-right">
                                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                            <?php if (!empty($matchResultsHome)) { ?>
                                                <?php foreach (array_reverse($matchResultsHome) as $keyResults => $valueResults) { ?>
                                                    <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="draw-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_home == $value->team_home && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="loss-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                            <li class="text-match">Last 5 matches</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="hdp-section text-center">
                                    <a class="link_wrap" href="/games?mid=<?php echo $value->match_id; ?>"></a>
                                    <strong>HDP</strong>
                                    <h2><?php echo $value->handicap; ?></h2>
                                    <div class="box-water-text">
                                        <div><?php echo number_format($value->home_water_bill,2); ?></div>
                                        <div>|</div>
                                        <div><?php echo number_format($value->away_water_bill,2); ?></div>
                                    </div>
                                    <span class="time-match"><?php echo date("H:i", (strtotime($value->time_match) - (60 * 60))) ?></span>
                                </div>
                                <div class="away-section">
                                    <div class="content-home-section content-away-section pull-left">
                                        <div class="topic-live-on">
                                            <?php if (!empty($matchPlayChannelMatch)) { ?>
                                                <?php foreach ($matchPlayChannelMatch as $keyC => $valueC) { ?>
                                                    <img src="<?php echo $valueC->channel_path; ?>">
                                                    <?php if ($keyC != count($matchPlayChannelMatch)) { ?>
                                                        <div>|</div>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="detail-team-home detail-team-away">
                                            <div class="detail-team-home-logo">
                                                <img src="<?php echo $value->teamAwayPath ?>">
                                            </div>
                                            <div class="detail-team-home-state detail-team-away-state">
                                                <h4><?php echo $value->teamAwayEn; ?></h4>
                                                <div class="box-progress">
                                                    <div class="progress">
                                                        <div class="progress-bar pull-left progress-bar-danger active"
                                                             role="progressbar"
                                                             aria-valuenow="<?= number_format($percentBet['pAway'], 2); ?>"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width: <?= number_format($percentBet['pAway'], 2); ?>%">

                                                        </div>
                                                    </div>
                                                    <span>Vote <?= number_format($percentBet['pAway']); ?>%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="list-state-match pull-left">
                                            <li class="text-match">Last 5 matches</li>
                                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                            <?php if (!empty($matchResultsAway)) { ?>
                                                <?php foreach ($matchResultsAway as $keyResults => $valueResults) { ?>
                                                    <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="draw-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_home == $value->team_away && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="win-ste"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="loss-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="vote-away-section">
                                        <?php echo $value->away_end_time_score ?>
                                    </div>
                                </div>
                            </div>
                            <section class="slide">
                                <div class="review-section">
                                    <a class="link_wrap"
                                       href="/review-infomation.php?match_id=<?php echo $value->match_id; ?>"></a>
                                    <!-- review home-->
                                    <div class="box-review-section">
                                        <!-- review home-->
                                        <div class="home-review-section">
                                            <?php foreach ($reviewMatchHomeByMatchId as $key => $value) { ?>
                                                <div class="box-content-review">
                                                    <div class="top-content-review pull-left">
                                                        <div>
                                                            <img style="<?php echo((!empty($value['path'])) ? '' : 'width: 40px;'); ?>"
                                                                 src="<?= ((!empty($value['path'])) ? $value['path'] : 'https://graph.facebook.com/v2.8/' . $value['user_fb_uid'] . '/picture'); ?>">
                                                        </div>
                                                        <div>
                                                            <h5><?= ((!empty($value['name'])) ? $value['name'] : $value['user_name']); ?></h5>
                                                            <?php if ($value['team'] == "home") { ?>
                                                                <img src="<?= $value['homepath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                            <?php } elseif ($value['team'] == "away") { ?>
                                                                <img src="<?= $value['awaypath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="winRate-box">
                                                            <div class="bg-winRate-box Home">
                                                                <h3 class="user-review-<?php echo $value['user_review_id']; ?>">
                                                                    %</h3>
                                                                <p>Win Rate</p>
                                                            </div>
                                                            <div class="bg-day-box Home">
                                                                <h3 class="user-review-day-<?php echo $value['user_review_id']; ?>"><?php echo date("t"); ?></h3>
                                                                <p>Days</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="full-review-caption">
                                                        <p><?= $value['review_text'] ?></p>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <!-- review away-->
                                        <div class="away-review-section">
                                            <?php foreach ($reviewMatchAwayByMatchId as $key => $value) { ?>
                                                <div class="box-content-review">
                                                    <div class="top-content-review pull-left">
                                                        <div>
                                                            <img style="<?php echo((!empty($value['path'])) ? '' : 'width: 40px;'); ?>"
                                                                 src="<?= ((!empty($value['path'])) ? $value['path'] : 'https://graph.facebook.com/v2.8/' . $value['user_fb_uid'] . '/picture'); ?>">
                                                        </div>
                                                        <div>
                                                            <h5><?= ((!empty($value['name'])) ? $value['name'] : $value['user_name']); ?></h5>
                                                            <?php if ($value['team'] == "home") { ?>
                                                                <img src="<?= $value['homepath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                            <?php } elseif ($value['team'] == "away") { ?>
                                                                <img src="<?= $value['awaypath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="winRate-box">
                                                            <div class="bg-winRate-box Away">
                                                                <h3 class="user-review-<?php echo $value['user_review_id']; ?>">
                                                                    %</h3>
                                                                <p>Win Rate</p>
                                                            </div>
                                                            <div class="bg-day-box Away">
                                                                <h3 class="user-review-day-<?php echo $value['user_review_id']; ?>"><?php echo date("t"); ?></h3>
                                                                <p>Days</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="full-review-caption">
                                                        <p><?= $value['review_text'] ?></p>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="full-width text-center more-box-page">
                                        <a class="btn btn-primary"
                                           href="/review-infomation.php?match_id=<?php echo $value->match_id; ?>"
                                           role="button">อ่านทั้งหมด</a>
                                    </div>
                                </div>
                            </section>
                        </div>
                    <?php } ?>
                <?php } ?>
                <!--                --><?php //} ?>
            <?php } else { ?>
                <div style="width: 100%;"><h2 style="text-align: center;">ไม่มีข้อมูลคู่บอล</h2></div>
            <?php } ?>
        </div>
        <div class="div-review" id="div-review-Yesterday">
            <?php if (!empty($matchYesterday)) { ?>
                <?php foreach ($matchYesterday as $key => $value) {
//                    if($key==0){
                    $matchPlayChannelMatch = $playChannelMatch->getChannel($value->match_7m_id);
                    $matchResultsHome = $playMatch->getListMatchResults($value->team_home, 5);
                    $matchResultsAway = $playMatch->getListMatchResults($value->team_away, 5);
                    $percentBet = $playBet->percentBet($value->match_id);
                    $reviewMatchAwayByMatchId = $playReviewMatch->getReviewAndUserMatchAwayByMatchIdAndLimit($value->match_id, 3);
                    $reviewMatchHomeByMatchId = $playReviewMatch->getReviewAndUserMatchHomeByMatchIdAndLimit($value->match_id, 3); ?>
                    <?php if ($value->status == "bet" || $value->status == "create") { ?>
                        <div class="content-review">
                            <div class="top-section">
                                <a class="link_wrap" href="/games?mid=<?php echo $value->match_id; ?>"></a>
                                <div class="home-section">
                                    <div class="vote-home-section">
                                        <a class="link_wrap" href="/games?mid=<?php echo $value->match_id; ?>"></a>
                                        VOTE
                                    </div>
                                    <div class="content-home-section text-right">
                                        <div class="topic-event"><?php echo $value->name; ?></div>
                                        <div class="detail-team-home">
                                            <div class="detail-team-home-state">
                                                <h4><?php echo $value->teamHomeEn; ?></h4>
                                                <div class="box-progress">
                                                      <span>Vote <?= number_format($percentBet['pHome']); ?>
                                                          %</span>
                                                    <div class="progress">
                                                        <div class="progress-bar pull-right progress-bar-striped active"
                                                             role="progressbar"
                                                             aria-valuenow="<?= number_format($percentBet['pHome'], 2); ?>"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width: <?= number_format($percentBet['pHome'], 2); ?>%">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="detail-team-home-logo">
                                                <img src="<?= $value->teamHomePath ?>">
                                            </div>
                                        </div>
                                        <ul class="list-state-match pull-right">
                                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                            <?php if (!empty($matchResultsHome)) { ?>
                                                <?php foreach (array_reverse($matchResultsHome) as $keyResults => $valueResults) { ?>
                                                    <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="draw-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_home == $value->team_home && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="loss-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                            <li class="text-match">Last 5 matches</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="hdp-section text-center">
                                    <a class="link_wrap" href="/games?mid=<?php echo $value->match_id; ?>"></a>
                                    <strong>HDP</strong>
                                    <h2><?php echo $value->handicap; ?></h2>
                                    <div class="box-water-text">
                                        <div><?php echo number_format($value->home_water_bill,2); ?></div>
                                        <div>|</div>
                                        <div><?php echo number_format($value->away_water_bill,2); ?></div>
                                    </div>
                                    <span class="time-match"><?php echo date("H:i", (strtotime($value->time_match) - (60 * 60))) ?></span>
                                </div>
                                <div class="away-section">
                                    <div class="content-home-section content-away-section pull-left">
                                        <div class="topic-live-on">
                                            <?php if (!empty($matchPlayChannelMatch)) { ?>
                                                <?php foreach ($matchPlayChannelMatch as $keyC => $valueC) { ?>
                                                    <img src="<?php echo $valueC->channel_path; ?>">
                                                    <?php if ($keyC != count($matchPlayChannelMatch)) { ?>
                                                        <div>|</div>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="detail-team-home detail-team-away">
                                            <div class="detail-team-home-logo">
                                                <img src="<?php echo $value->teamAwayPath ?>">
                                            </div>
                                            <div class="detail-team-home-state detail-team-away-state">
                                                <h4><?php echo $value->teamAwayEn; ?></h4>
                                                <div class="box-progress">
                                                    <span>Vote <?= number_format($percentBet['pAway']); ?>%</span>
                                                    <div class="progress">
                                                        <div class="progress-bar pull-left progress-bar-danger active"
                                                             role="progressbar"
                                                             aria-valuenow="<?= number_format($percentBet['pAway'], 2); ?>"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width: <?= number_format($percentBet['pAway'], 2); ?>%">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="list-state-match pull-left">
                                            <li class="text-match">Last 5 matches</li>
                                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                            <?php if (!empty($matchResultsAway)) { ?>
                                                <?php foreach ($matchResultsAway as $keyResults => $valueResults) { ?>
                                                    <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="draw-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_home == $value->team_away && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="win-ste"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="loss-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="vote-away-section">
                                        <a class="link_wrap" href="/games?mid=<?php echo $value->match_id; ?>"></a>
                                        VOTE
                                    </div>
                                </div>
                            </div>
                            <section class="slide">
                                <div class="review-section">
                                    <a class="link_wrap"
                                       href="/review-infomation.php?match_id=<?php echo $value->match_id; ?>"></a>
                                    <div class="box-review-section">
                                        <!-- review home-->
                                        <div class="home-review-section">
                                            <?php foreach ($reviewMatchHomeByMatchId as $key => $value) { ?>
                                                <div class="box-content-review">
                                                    <div class="top-content-review pull-left">
                                                        <div>
                                                            <img style="<?php echo((!empty($value['path'])) ? '' : 'width: 40px;'); ?>"
                                                                 src="<?= ((!empty($value['path'])) ? $value['path'] : 'https://graph.facebook.com/v2.8/' . $value['user_fb_uid'] . '/picture'); ?>">
                                                        </div>
                                                        <div>
                                                            <h5><?= ((!empty($value['name'])) ? $value['name'] : $value['user_name']); ?></h5>
                                                            <?php if ($value['team'] == "home") { ?>
                                                                <img src="<?= $value['homepath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                            <?php } elseif ($value['team'] == "away") { ?>
                                                                <img src="<?= $value['awaypath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="winRate-box">
                                                            <div class="bg-winRate-box">
                                                                <h3 class="user-review-<?php echo $value['user_review_id']; ?>">
                                                                    %</h3>
                                                                <p>Win Rate</p>
                                                            </div>
                                                            <div class="bg-day-box">
                                                                <h3 class="user-review-day-<?php echo $value['user_review_id']; ?>"><?php echo date("t"); ?></h3>
                                                                <p>Days</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="full-review-caption">
                                                        <p><?= $value['review_text'] ?></p>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <!-- review away-->
                                        <div class="away-review-section">
                                            <?php foreach ($reviewMatchAwayByMatchId as $key => $value) { ?>
                                                <div class="box-content-review">
                                                    <div class="top-content-review pull-left">
                                                        <div>
                                                            <img style="<?php echo((!empty($value['path'])) ? '' : 'width: 40px;'); ?>"
                                                                 src="<?= ((!empty($value['path'])) ? $value['path'] : 'https://graph.facebook.com/v2.8/' . $value['user_fb_uid'] . '/picture'); ?>">
                                                        </div>
                                                        <div>
                                                            <h5><?= ((!empty($value['name'])) ? $value['name'] : $value['user_name']); ?></h5>
                                                            <?php if ($value['team'] == "home") { ?>
                                                                <img src="<?= $value['homepath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                            <?php } elseif ($value['team'] == "away") { ?>
                                                                <img src="<?= $value['awaypath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="winRate-box">
                                                            <div class="bg-winRate-box">
                                                                <h3 class="user-review-<?php echo $value['user_review_id']; ?>">
                                                                    %</h3>
                                                                <p>Win Rate</p>
                                                            </div>
                                                            <div class="bg-day-box">
                                                                <h3 class="user-review-day-<?php echo $value['user_review_id']; ?>"><?php echo date("t"); ?></h3>
                                                                <p>Days</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="full-review-caption">
                                                        <p><?= $value['review_text'] ?></p>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="full-width text-center more-box-page">
                                        <a class="btn btn-primary"
                                           href="/review-infomation.php?match_id=<?php echo $value->match_id; ?>"
                                           role="button">อ่านทั้งหมด</a>
                                    </div>
                                </div>
                            </section>
                        </div>
                    <?php } else if ($value->status == "fullTime") { ?>
                        <div class="content-review content-review-endmatch">
                            <div class="top-section">
                                <div class="home-section">
                                    <div class="vote-home-section">
                                        <?php echo $value->home_end_time_score; ?>
                                    </div>
                                    <div class="content-home-section text-right">
                                        <div class="topic-event"><?php echo $value->name; ?></div>
                                        <div class="detail-team-home">
                                            <div class="detail-team-home-state">
                                                <h4><?php echo $value->teamHomeEn; ?></h4>
                                                <div class="box-progress">
                                                    <span>Vote <?php echo number_format($percentBet['pHome']); ?>
                                                        %</span>
                                                    <div class="progress">
                                                        <div class="progress-bar pull-right progress-bar-striped active"
                                                             role="progressbar"
                                                             aria-valuenow="<?php echo number_format($percentBet['pHome'], 2); ?>"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width: <?php echo number_format($percentBet['pHome'], 2); ?>%">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="detail-team-home-logo">
                                                <img src="<?php echo $value->teamHomePath; ?>">
                                            </div>

                                        </div>
                                        <ul class="list-state-match pull-right">
                                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                            <?php if (!empty($matchResultsHome)) { ?>
                                                <?php foreach (array_reverse($matchResultsHome) as $keyResults => $valueResults) { ?>
                                                    <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="draw-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_home == $value->team_home && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="loss-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                            <li class="text-match">Last 5 matches</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="hdp-section text-center">
                                    <a class="link_wrap" href="/games?mid=<?php echo $value->match_id; ?>"></a>
                                    <strong>HDP</strong>
                                    <h2><?php echo $value->handicap; ?></h2>
                                    <div class="box-water-text">
                                        <div><?php echo number_format($value->home_water_bill,2); ?></div>
                                        <div>|</div>
                                        <div><?php echo number_format($value->away_water_bill,2); ?></div>
                                    </div>
                                    <span class="time-match"><?php echo date("H:i", (strtotime($value->time_match) - (60 * 60))) ?></span>
                                </div>
                                <div class="away-section">
                                    <div class="content-home-section content-away-section pull-left">
                                        <div class="topic-live-on">
                                            <?php if (!empty($matchPlayChannelMatch)) { ?>
                                                <?php foreach ($matchPlayChannelMatch as $keyC => $valueC) { ?>
                                                    <img src="<?php echo $valueC->channel_path; ?>">
                                                    <?php if ($keyC != count($matchPlayChannelMatch)) { ?>
                                                        <div>|</div>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="detail-team-home detail-team-away">
                                            <div class="detail-team-home-logo">
                                                <img src="<?php echo $value->teamAwayPath ?>">
                                            </div>
                                            <div class="detail-team-home-state detail-team-away-state">
                                                <h4><?php echo $value->teamAwayEn; ?></h4>
                                                <div class="box-progress">
                                                    <div class="progress">
                                                        <div class="progress-bar pull-left progress-bar-danger active"
                                                             role="progressbar"
                                                             aria-valuenow="<?= number_format($percentBet['pAway'], 2); ?>"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width: <?= number_format($percentBet['pAway'], 2); ?>%">

                                                        </div>
                                                    </div>
                                                    <span>Vote <?= number_format($percentBet['pAway']); ?>%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="list-state-match pull-left">
                                            <li class="text-match">Last 5 matches</li>
                                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                            <?php if (!empty($matchResultsAway)) { ?>
                                                <?php foreach ($matchResultsAway as $keyResults => $valueResults) { ?>
                                                    <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="draw-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_home == $value->team_away && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="win-ste"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="loss-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="vote-away-section">
                                        <?php echo $value->away_end_time_score ?>
                                    </div>
                                </div>

                            </div>
                            <section class="slide">
                                <div class="review-section">
                                    <a class="link_wrap"
                                       href="/review-infomation.php?match_id=<?php echo $value->match_id; ?>"></a>
                                    <!-- review home-->
                                    <div class="box-review-section">
                                        <!-- review home-->
                                        <div class="home-review-section">
                                            <?php foreach ($reviewMatchHomeByMatchId as $key => $value) { ?>
                                                <div class="box-content-review">
                                                    <div class="top-content-review pull-left">
                                                        <div>
                                                            <img style="<?php echo((!empty($value['path'])) ? '' : 'width: 40px;'); ?>"
                                                                 src="<?= ((!empty($value['path'])) ? $value['path'] : 'https://graph.facebook.com/v2.8/' . $value['user_fb_uid'] . '/picture'); ?>">
                                                        </div>
                                                        <div>
                                                            <h5><?= ((!empty($value['name'])) ? $value['name'] : $value['user_name']); ?></h5>
                                                            <?php if ($value['team'] == "home") { ?>
                                                                <img src="<?= $value['homepath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                            <?php } elseif ($value['team'] == "away") { ?>
                                                                <img src="<?= $value['awaypath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="winRate-box">
                                                            <div class="bg-winRate-box Home">
                                                                <h3 class="user-review-<?php echo $value['user_review_id']; ?>">
                                                                    %</h3>
                                                                <p>Win Rate</p>
                                                            </div>
                                                            <div class="bg-day-box Home">
                                                                <h3 class="user-review-day-<?php echo $value['user_review_id']; ?>"><?php echo date("t"); ?></h3>
                                                                <p>Days</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="full-review-caption">
                                                        <p><?= $value['review_text'] ?></p>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <!-- review away-->
                                        <div class="away-review-section">
                                            <?php foreach ($reviewMatchAwayByMatchId as $key => $value) { ?>
                                                <div class="box-content-review">
                                                    <div class="top-content-review pull-left">
                                                        <div>
                                                            <img style="<?php echo((!empty($value['path'])) ? '' : 'width: 40px;'); ?>"
                                                                 src="<?= ((!empty($value['path'])) ? $value['path'] : 'https://graph.facebook.com/v2.8/' . $value['user_fb_uid'] . '/picture'); ?>">
                                                        </div>
                                                        <div>
                                                            <h5><?= ((!empty($value['name'])) ? $value['name'] : $value['user_name']); ?></h5>
                                                            <?php if ($value['team'] == "home") { ?>
                                                                <img src="<?= $value['homepath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                            <?php } elseif ($value['team'] == "away") { ?>
                                                                <img src="<?= $value['awaypath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="winRate-box">
                                                            <div class="bg-winRate-box Away">
                                                                <h3 class="user-review-<?php echo $value['user_review_id']; ?>">
                                                                    %</h3>
                                                                <p>Win Rate</p>
                                                            </div>
                                                            <div class="bg-day-box Away">
                                                                <h3 class="user-review-day-<?php echo $value['user_review_id']; ?>"><?php echo date("t"); ?></h3>
                                                                <p>Days</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="full-review-caption">
                                                        <p><?= $value['review_text'] ?></p>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="full-width text-center more-box-page">
                                        <a class="btn btn-primary"
                                           href="/review-infomation.php?match_id=<?php echo $value->match_id; ?>"
                                           role="button">อ่านทั้งหมด</a>
                                    </div>
                                </div>
                            </section>
                        </div>
                    <?php } ?>
                <?php } ?>
                <!--                --><?php //} ?>
            <?php } else { ?>
                <div style="width: 100%;"><h2 style="text-align: center;">ไม่มีข้อมูลคู่บอล</h2></div>
            <?php } ?>
        </div>
        <div class="div-review" id="div-review-Tomorrow">
            <?php if (!empty($matchTomorrow)) { ?>
                <?php foreach ($matchTomorrow as $key => $value) {
//                    if($key==0){
                    $matchPlayChannelMatch = $playChannelMatch->getChannel($value->match_7m_id);
                    $matchResultsHome = $playMatch->getListMatchResults($value->team_home, 5);
                    $matchResultsAway = $playMatch->getListMatchResults($value->team_away, 5);
                    $percentBet = $playBet->percentBet($value->match_id);
                    $reviewMatchAwayByMatchId = $playReviewMatch->getReviewAndUserMatchAwayByMatchIdAndLimit($value->match_id, 3);
                    $reviewMatchHomeByMatchId = $playReviewMatch->getReviewAndUserMatchHomeByMatchIdAndLimit($value->match_id, 3); ?>
                    <?php if ($value->status == "bet" || $value->status == "create") { ?>
                        <div class="content-review">
                            <div class="top-section">
                                <a class="link_wrap" href="/games?mid=<?php echo $value->match_id; ?>"></a>
                                <div class="home-section">
                                    <div class="vote-home-section">
                                        <a class="link_wrap" href="/games?mid=<?php echo $value->match_id; ?>"></a>
                                        VOTE
                                    </div>
                                    <div class="content-home-section text-right">
                                        <div class="topic-event"><?php echo $value->name; ?></div>
                                        <div class="detail-team-home">
                                            <div class="detail-team-home-state">
                                                <h4><?php echo $value->teamHomeEn; ?></h4>
                                                <div class="box-progress">
                                                    <span>Vote <?= number_format($percentBet['pHome']); ?>%</span>
                                                <div class="progress">
                                                    <div class="progress-bar pull-right progress-bar-striped active"
                                                         role="progressbar"
                                                         aria-valuenow="<?= number_format($percentBet['pHome'], 2); ?>"
                                                         aria-valuemin="0" aria-valuemax="100"
                                                         style="width: <?= number_format($percentBet['pHome'], 2); ?>%">
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="detail-team-home-logo">
                                                <img src="<?= $value->teamHomePath ?>">
                                            </div>
                                        </div>
                                        <ul class="list-state-match pull-right">
                                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                            <?php if (!empty($matchResultsHome)) { ?>
                                                <?php foreach (array_reverse($matchResultsHome) as $keyResults => $valueResults) { ?>
                                                    <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="draw-st" aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_home == $value->team_home && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="loss-st" aria-hidden="true"></div>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                            <li class="text-match">Last 5 matches</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="hdp-section text-center">
                                    <a class="link_wrap" href="/games?mid=<?php echo $value->match_id; ?>"></a>
                                    <strong>HDP</strong>
                                    <h2><?php echo $value->handicap; ?></h2>
                                    <div class="box-water-text">
                                        <div><?php echo number_format($value->home_water_bill,2); ?></div>
                                        <div>|</div>
                                        <div><?php echo number_format($value->away_water_bill,2); ?></div>
                                    </div>
                                    <span class="time-match"><?php echo date("H:i", (strtotime($value->time_match) - (60 * 60))) ?></span>
                                </div>
                                <div class="away-section">
                                    <div class="content-home-section content-away-section pull-left">
                                        <div class="topic-live-on">
                                            <?php if (!empty($matchPlayChannelMatch)) { ?>
                                                <?php foreach ($matchPlayChannelMatch as $keyC => $valueC) { ?>
                                                    <img src="<?php echo $valueC->channel_path; ?>">
                                                    <?php if ($keyC != count($matchPlayChannelMatch)) { ?>
                                                        <div>|</div>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="detail-team-home detail-team-away">
                                            <div class="detail-team-home-logo">
                                                <img src="<?php echo $value->teamAwayPath ?>">
                                            </div>
                                            <div class="detail-team-home-state detail-team-away-state">
                                                <h4><?php echo $value->teamAwayEn; ?></h4>
                                                <div class="box-progress">
                                                <div class="progress">
                                                    <div class="progress-bar pull-left progress-bar-danger active"
                                                         role="progressbar"
                                                         aria-valuenow="<?= number_format($percentBet['pAway'], 2); ?>"
                                                         aria-valuemin="0" aria-valuemax="100"
                                                         style="width: <?= number_format($percentBet['pAway'], 2); ?>%">

                                                    </div>
                                                </div>
                                                    <span>Vote <?= number_format($percentBet['pAway']); ?>%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="list-state-match pull-left">
                                            <li class="text-match">Last 5 matches</li>
                                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                            <?php if (!empty($matchResultsAway)) { ?>
                                                <?php foreach ($matchResultsAway as $keyResults => $valueResults) { ?>
                                                    <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="draw-st" aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_home == $value->team_away && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="win-ste" aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="loss-st" aria-hidden="true"></div>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="vote-away-section">
                                        <a class="link_wrap" href="/games?mid=<?php echo $value->match_id; ?>"></a>
                                        VOTE
                                    </div>
                                </div>
                            </div>
                            <section class="slide">
                                <div class="review-section">
                                    <a class="link_wrap"
                                       href="/review-infomation.php?match_id=<?php echo $value->match_id; ?>"></a>
                                    <div class="box-review-section">
                                        <!-- review home-->
                                        <div class="home-review-section">
                                            <?php foreach ($reviewMatchHomeByMatchId as $key => $value) { ?>
                                                <div class="box-content-review">
                                                    <div class="top-content-review pull-left">
                                                        <div>
                                                            <img style="<?php echo((!empty($value['path'])) ? '' : 'width: 40px;'); ?>"
                                                                 src="<?= ((!empty($value['path'])) ? $value['path'] : 'https://graph.facebook.com/v2.8/' . $value['user_fb_uid'] . '/picture'); ?>">
                                                        </div>
                                                        <div>
                                                            <h5><?= ((!empty($value['name'])) ? $value['name'] : $value['user_name']); ?></h5>
                                                            <?php if ($value['team'] == "home") { ?>
                                                                <img src="<?= $value['homepath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                            <?php } elseif ($value['team'] == "away") { ?>
                                                                <img src="<?= $value['awaypath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="winRate-box">
                                                            <div class="bg-winRate-box">
                                                                <h3 class="user-review-<?php echo $value['user_review_id']; ?>">
                                                                    %</h3>
                                                                <p>Win Rate</p>
                                                            </div>
                                                            <div class="bg-day-box">
                                                                <h3 class="user-review-day-<?php echo $value['user_review_id']; ?>"><?php echo date("t"); ?></h3>
                                                                <p>Days</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="full-review-caption">
                                                        <p><?= $value['review_text'] ?></p>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <!-- review away-->
                                        <div class="away-review-section">
                                            <?php foreach ($reviewMatchAwayByMatchId as $key => $value) { ?>
                                                <div class="box-content-review">
                                                    <div class="top-content-review pull-left">
                                                        <div>
                                                            <img style="<?php echo((!empty($value['path'])) ? '' : 'width: 40px;'); ?>"
                                                                 src="<?= ((!empty($value['path'])) ? $value['path'] : 'https://graph.facebook.com/v2.8/' . $value['user_fb_uid'] . '/picture'); ?>">
                                                        </div>
                                                        <div>
                                                            <h5><?= ((!empty($value['name'])) ? $value['name'] : $value['user_name']); ?></h5>
                                                            <?php if ($value['team'] == "home") { ?>
                                                                <img src="<?= $value['homepath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                            <?php } elseif ($value['team'] == "away") { ?>
                                                                <img src="<?= $value['awaypath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="winRate-box">
                                                            <div class="bg-winRate-box">
                                                                <h3 class="user-review-<?php echo $value['user_review_id']; ?>">
                                                                    %</h3>
                                                                <p>Win Rate</p>
                                                            </div>
                                                            <div class="bg-day-box">
                                                                <h3 class="user-review-day-<?php echo $value['user_review_id']; ?>"><?php echo date("t"); ?></h3>
                                                                <p>Days</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="full-review-caption">
                                                        <p><?= $value['review_text'] ?></p>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="full-width text-center more-box-page">
                                        <a class="btn btn-primary"
                                           href="/review-infomation.php?match_id=<?php echo $value->match_id; ?>"
                                           role="button">อ่านทั้งหมด</a>
                                    </div>
                                </div>
                            </section>
                        </div>
                    <?php } else if ($value->status == "fullTime") { ?>
                        <div class="content-review content-review-endmatch">
                            <div class="top-section">
                                <div class="home-section">
                                    <div class="vote-home-section">
                                        <?php echo $value->home_end_time_score; ?>
                                    </div>
                                    <div class="content-home-section text-right">
                                        <div class="topic-event"><?php echo $value->name; ?></div>
                                        <div class="detail-team-home">
                                            <div class="detail-team-home-state">
                                                <h4><?php echo $value->teamHomeEn; ?></h4>
                                                <div class="box-progress">
                                                    <span>Vote <?php echo number_format($percentBet['pHome']); ?>
                                                        %</span>
                                                <div class="progress">
                                                    <div class="progress-bar pull-right progress-bar-striped active"
                                                         role="progressbar"
                                                         aria-valuenow="<?php echo number_format($percentBet['pHome'], 2); ?>"
                                                         aria-valuemin="0" aria-valuemax="100"
                                                         style="width: <?php echo number_format($percentBet['pHome'], 2); ?>%">

                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="detail-team-home-logo">
                                                <img src="<?php echo $value->teamHomePath; ?>">
                                            </div>

                                        </div>
                                        <ul class="list-state-match pull-right">
                                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                            <?php if (!empty($matchResultsHome)) { ?>
                                                <?php foreach (array_reverse($matchResultsHome) as $keyResults => $valueResults) { ?>
                                                    <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="draw-st" aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_home == $value->team_home && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="loss-st" aria-hidden="true"></div>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                            <li class="text-match">Last 5 matches</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="hdp-section text-center">
                                    <a class="link_wrap" href="/games?mid=<?php echo $value->match_id; ?>"></a>
                                    <strong>HDP</strong>
                                    <h2><?php echo $value->handicap; ?></h2>
                                    <div class="box-water-text">
                                        <div><?php echo number_format($value->home_water_bill,2); ?></div>
                                        <div>|</div>
                                        <div><?php echo number_format($value->away_water_bill,2); ?></div>
                                    </div>
                                    <span class="time-match"><?php echo date("H:i", (strtotime($value->time_match) - (60 * 60))) ?></span>
                                </div>
                                <div class="away-section">
                                    <div class="content-home-section content-away-section pull-left">
                                        <div class="topic-live-on">
                                            <?php if (!empty($matchPlayChannelMatch)) { ?>
                                                <?php foreach ($matchPlayChannelMatch as $keyC => $valueC) { ?>
                                                    <img src="<?php echo $valueC->channel_path; ?>">
                                                    <?php if ($keyC != count($matchPlayChannelMatch)) { ?>
                                                        <div>|</div>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="detail-team-home detail-team-away">
                                            <div class="detail-team-home-logo">
                                                <img src="<?php echo $value->teamAwayPath ?>">
                                            </div>
                                            <div class="detail-team-home-state detail-team-away-state">
                                                <h4><?php echo $value->teamAwayEn; ?></h4>
                                                <div class="box-progress">
                                                <div class="progress">
                                                    <div class="progress-bar pull-left progress-bar-danger active"
                                                         role="progressbar"
                                                         aria-valuenow="<?= number_format($percentBet['pAway'], 2); ?>"
                                                         aria-valuemin="0" aria-valuemax="100"
                                                         style="width: <?= number_format($percentBet['pAway'], 2); ?>%">

                                                    </div>
                                                </div>
                                                    <span>Vote <?= number_format($percentBet['pAway']); ?>%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="list-state-match pull-left">
                                            <li class="text-match">Last 5 matches</li>
                                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                            <?php if (!empty($matchResultsAway)) { ?>
                                                <?php foreach ($matchResultsAway as $keyResults => $valueResults) { ?>
                                                    <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="draw-st" aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_home == $value->team_away && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="win-ste" aria-hidden="true"></div>
                                                        </li>
                                                    <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                                 aria-hidden="true"></div>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li>
                                                            <div title="<?= $valueResults->time_match ?>"
                                                                 class="loss-st" aria-hidden="true"></div>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="vote-away-section">
                                        <?php echo $value->away_end_time_score ?>
                                    </div>
                                </div>
                            </div>
                            <section class="slide">
                                <div class="review-section">
                                    <a class="link_wrap"
                                       href="/review-infomation.php?match_id=<?php echo $value->match_id; ?>"></a>
                                    <div class="box-review-section">
                                        <!-- review home-->
                                        <div class="home-review-section">
                                            <?php foreach ($reviewMatchHomeByMatchId as $key => $value) { ?>
                                                <div class="box-content-review">
                                                    <div class="top-content-review pull-left">
                                                        <div>
                                                            <img style="<?php echo((!empty($value['path'])) ? '' : 'width: 40px;'); ?>"
                                                                 src="<?= ((!empty($value['path'])) ? $value['path'] : 'https://graph.facebook.com/v2.8/' . $value['user_fb_uid'] . '/picture'); ?>">
                                                        </div>
                                                        <div>
                                                            <h5><?= ((!empty($value['name'])) ? $value['name'] : $value['user_name']); ?></h5>
                                                            <?php if ($value['team'] == "home") { ?>
                                                                <img src="<?= $value['homepath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                            <?php } elseif ($value['team'] == "away") { ?>
                                                                <img src="<?= $value['awaypath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="winRate-box">
                                                            <div class="bg-winRate-box Home">
                                                                <h3 class="user-review-<?php echo $value['user_review_id']; ?>">
                                                                    %</h3>
                                                                <p>Win Rate</p>
                                                            </div>
                                                            <div class="bg-day-box Home">
                                                                <h3 class="user-review-day-<?php echo $value['user_review_id']; ?>"><?php echo date("t"); ?></h3>
                                                                <p>Days</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="full-review-caption">
                                                        <p><?= $value['review_text'] ?></p>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <!-- review away-->
                                        <div class="away-review-section">
                                            <?php foreach ($reviewMatchAwayByMatchId as $key => $value) { ?>
                                                <div class="box-content-review">
                                                    <div class="top-content-review pull-left">
                                                        <div>
                                                            <img style="<?php echo((!empty($value['path'])) ? '' : 'width: 40px;'); ?>"
                                                                 src="<?= ((!empty($value['path'])) ? $value['path'] : 'https://graph.facebook.com/v2.8/' . $value['user_fb_uid'] . '/picture'); ?>">
                                                        </div>
                                                        <div>
                                                            <h5><?= ((!empty($value['name'])) ? $value['name'] : $value['user_name']); ?></h5>
                                                            <?php if ($value['team'] == "home") { ?>
                                                                <img src="<?= $value['homepath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                            <?php } elseif ($value['team'] == "away") { ?>
                                                                <img src="<?= $value['awaypath'] ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="winRate-box">
                                                            <div class="bg-winRate-box Away">
                                                                <h3 class="user-review-<?php echo $value['user_review_id']; ?>">
                                                                    %</h3>
                                                                <p>Win Rate</p>
                                                            </div>
                                                            <div class="bg-day-box Away">
                                                                <h3 class="user-review-day-<?php echo $value['user_review_id']; ?>"><?php echo date("t"); ?></h3>
                                                                <p>Days</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="full-review-caption">
                                                        <p><?= $value['review_text'] ?></p>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="full-width text-center more-box-page">
                                    <a class="btn btn-primary"
                                       href="/review-infomation.php?match_id=<?php echo $value->match_id; ?>"
                                       role="button">อ่านทั้งหมด</a>
                                </div>
                            </section>
                        </div>
                    <?php } ?>
                <?php } ?>
                <!--                --><?php //} ?>
            <?php } else { ?>
                <div style="width: 100%;"><h2 style="text-align: center;">ไม่มีข้อมูลคู่บอล</h2></div>
            <?php } ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('section').readmore({maxHeight: 100});

        // active

        console.log($("#div-review-Today").find('.content-review').length);

        $(".div-review").each(function (index) {
            console.log($(this).attr('id')+"::"+$(this).find('.content-review').length);
            if(($(this).attr('id') == "div-review-Today" || $(this).attr('id') == "div-review-Tomorrow" ) && $("#div-review-Today").find('.content-review').length==0){
                $(this).addClass('hide');
            }else if(($(this).attr('id') == "div-review-Tomorrow" || $(this).attr('id') == "div-review-Yesterday") && $("#div-review-Today").find('.content-review').length>=1){
                $(this).addClass('hide');
            }
            if($("#div-review-Today").find('.content-review').length==0) {
                $(".btn-review").removeClass('active');
                $("#btn-review-Yesterday").addClass('active');
            }else {
                $(".btn-review").removeClass('active');
                $("#btn-review-Today").addClass('active');
            }
        });

        $(document).on("click", ".btn-review", function () {
            $(".btn-review").removeClass('active');
            $(".div-review").addClass('hide');
            $("#btn-review-" + $(this).html()).addClass('active');
            $("#div-review-" + $(this).html()).removeClass('hide');
        });

        $.ajax({
            url: "/json/checkWinRateReview.json",
            method: "POST",
            dataType: "JSON"
        }).done(function (response) {
            $.each(response, function (key, val) {
                $("h3.user-review-" + val.id).html(parseInt(val.winRate) + "%");
                // console.log(key+" :: "+val)
            })
        });
    });
</script>
<script>
    function openNav() {
        document.getElementById("mySidenav").style.marginRight = "0";
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main").style.marginRight = "250px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.margin = "0 auto";
    }
</script>
<script>
    function myFunction() {
        var dots = document.getElementById("dots");
        var moreText = document.getElementById("more");
        var btnText = document.getElementById("myBtn");

        if (dots.style.display === "none") {
            dots.style.display = "inline";
            btnText.innerHTML = "Read more";
            moreText.style.display = "none";
        } else {
            dots.style.display = "none";
            btnText.innerHTML = "Read less";
            moreText.style.display = "inline";
        }
    }
</script>
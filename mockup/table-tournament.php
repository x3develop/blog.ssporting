<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta http-equiv="Cache-control" content="public">
    <title>Ngoal | ตารางคะแนน</title>
    <link rel="shortcut icon" type="images/x-icon" href="/images/favicon.ico.png">
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/table-tournament.css">
    <link rel="stylesheet" href="/css/football-symbol.css">
    <link rel="stylesheet" href="/css/scrollbar.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!-- <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.carousel.css">
    <!-- <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css"> -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="/nanoscrollbar/nanoscrollbar.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <!-- <script src="/js/jquery/dist/jquery.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/js/readmore.js"></script>
    <!-- <script src="/css/bootstrap/js/bootstrap.min.js"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <!-- <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/owl.carousel.min.js"></script>
    <!-- <script src="/js/jquery-timeago/jquery.timeago.js"></script> -->
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="/js/highlight_live.js"></script>

    <script src="nanoscrollbar/nanoscrollbar.js"></script>

    <!-- <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script> -->
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118679525-1"></script> -->
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">

<?php include $_SERVER['DOCUMENT_ROOT'] . '/view/index/menu-all.php'; ?>
<style>
    body {
        position: relative;
        background-color: #f1f2f5;
    }

    .affix {
        position: relative;
        top: 0;
        width: 100%;
        z-index: 9999 !important;
    }

    .navbar {
        margin-bottom: 0;
    }


</style>
<div class="side-select" data-spy="affix" data-offset-top="60">

    <div class="calendar-box">
        <div class="month-tab full-width bt-flex">
            <div class="month-prev none-active"></div>
            <div class="month-title">
                January 2019
            </div>
            <div class="month-next text-right"></div>
        </div>
        <table>
            <thead>
            <tr>
                <th><span title="Monday">Mo</span></th>
                <th><span title="Tuesday">Tu</span></th>
                <th><span title="Wednesday">We</span></th>
                <th><span title="Thursday">Th</span></th>
                <th><span title="Friday">Fr</span></th>
                <th class="ui-datepicker-week-end"><span title="Saturday">Sa</span></th>
                <th class="ui-datepicker-week-end"><span title="Sunday">Su</span></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><a class="other-month" href="#">31</a></td>
                <td><a href="#">1</a></td>
                <td><a href="#">2</a></td>
                <td><a href="#">3</a></td>
                <td><a href="#">4</a></td>
                <td><a href="#">5</a></td>
                <td><a href="#">6</a></td>
            </tr>
            <tr>
                <td><a href="#">7</a></td>
                <td><a href="#">8</a></td>
                <td><a href="#">9</a></td>
                <td><a class="state-highlight-active" href="#">10</a></td>
                <td><a href="#">11</a></td>
                <td><a href="#">12</a></td>
                <td><a href="#">13</a></td>
            </tr>
            <tr>
                <td><a href="#">14</a></td>
                <td><a href="#">15</a></td>
                <td><a href="#">16</a></td>
                <td><a href="#">17</a></td>
                <td><a href="#">18</a></td>
                <td><a href="#">19</a></td>
                <td><a href="#">20</a></td>
            </tr>
            <tr>
                <td><a href="#">21</a></td>
                <td><a href="#">22</a></td>
                <td><a href="#">23</a></td>
                <td><a href="#">24</a></td>
                <td><a href="#">25</a></td>
                <td><a href="#">26</a></td>
                <td><a href="#">27</a></td>
            </tr>
            <tr>
                <td><a href="#">28</a></td>
                <td><a href="#">29</a></td>
                <td><a href="#">30</a></td>
                <td><a href="#">31</a></td>
                <td><a class="other-month" href="#">1</a></td>
                <td><a class="other-month" href="#">2</a></td>
                <td><a class="other-month" href="#">3</a></td>
            </tr>
            </tbody>
        </table>
    </div>

    <h4 class="title-center">
        TOP LEAGUES
    </h4>
    <nav id="nav" class="navbar nav-pills nav-stacked">
        <ul class="nav navbar-nav">
            <li class="active"><a href="#">
                    <img src="/images/five-leagues/seriea1.png">
                    <h5>กัลโช่ เซเรียอา</h5>
                </a></li>
            <li>
                <a href="#">
                    <img src="/images/five-leagues/premier-league-logo.png">
                    <h5>พรีเมียร์ลีก อังกฤษ</h5>
                </a>
            </li>
            <li><a href="#">
                    <img src="/images/five-leagues/La-Liga-Logo-1.png">
                    <h5>ลาลีกา สเปน</h5>
                </a></li>
            <li><a href="#">
                    <img src="/images/five-leagues/logo_bl.gif">
                    <h5>บุนเดสลีกา เยอรมัน</h5>
                </a></li>
            <li><a href="#">
                    <img src="/images/five-leagues/UEFA-logo.png">
                    <h5>ยูฟ่า แชมเปี้ยนส์ลีก</h5>
                </a></li>
        </ul>
    </nav>

    <h4 class="title-center">
        LEAGUES
    </h4>
    <nav id="nav" class="navbar nav-pills nav-stacked">
        <ul class="nav navbar-nav">
            <li class="active"><a href="#">
                    <img src="/images/five-leagues/seriea1.png">
                    <h5>กัลโช่ เซเรียอา</h5>
                </a></li>
            <li>
                <a href="#">
                    <img src="/images/five-leagues/premier-league-logo.png">
                    <h5>พรีเมียร์ลีก อังกฤษ</h5>
                </a>
            </li>
            <li><a href="#">
                    <img src="/images/five-leagues/La-Liga-Logo-1.png">
                    <h5>ลาลีกา สเปน</h5>
                </a></li>
            <li><a href="#">
                    <img src="/images/five-leagues/logo_bl.gif">
                    <h5>บุนเดสลีกา เยอรมัน</h5>
                </a></li>
            <li><a href="#">
                    <img src="/images/five-leagues/UEFA-logo.png">
                    <h5>ยูฟ่า แชมเปี้ยนส์ลีก</h5>
                </a></li>
        </ul>
    </nav>
</div>

<div class="content-all-table pull-right">

    <div class="content-table-league">
        <div class="topic-league inline-box full-width">

                <!-- Nav tabs -->
                <ul class="nav nav-pills" role="tablist">
                    <li role="presentation" class="active"><a href="#table-tour" role="tab"
                                                              data-toggle="tab">ตารางคะแนน</a></li>
                    <li role="presentation"><a href="#static-table" role="tab" data-toggle="tab">ตารางการแข่งขัน /
                            ดาวซัลโว</a>
                    </li>
                </ul>

        </div>

        <div class="inline-box" style="width: 100%;">
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="table-tour">
                    <div>
                        <div class="title-league">
                            <div class="logo inline-box">
                                <img src="/images/five-leagues/premier-league-logo.png">
                            </div>
                            <div class="content inline-box">
                                <h2>ตารางคะแนน Premier League</h2>
                                <ul>
                                    <li>England</li>
                                    <li>|</li>
                                    <li>2018-2019</li>
                                </ul>
                            </div>
                        </div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-table-league-detail nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#overall" role="tab" data-toggle="tab">Overall</a>
                            </li>
                            <li role="presentation"><a href="#home" role="tab" data-toggle="tab">home</a></li>
                            <li role="presentation"><a href="#away" role="tab" data-toggle="tab">Away</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content content-table-league-detail">
                            <div role="tabpanel" class="tab-pane active" id="overall">
                                <table>
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>P</th>
                                        <th>W</th>
                                        <th>D</th>
                                        <th>L</th>
                                        <th>Golds</th>
                                        <th>Last 5</th>
                                        <th>PTs</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="topic">
                                            <span class="stand-ucl">1</span>
                                            <span class="logo"><img src="/images/logo-team/Liverpool.png"></span>
                                            <h4>Liverpool</h4>
                                        </td>
                                        <td>20</td>
                                        <td>17</td>
                                        <td>3</td>
                                        <td>1</td>
                                        <td>49:10</td>
                                        <td class="text-center">
                                            <div class="static-last-play margin-center">
                                                <div class="win-status"></div>
                                                <div class="win-status"></div>
                                                <div class="win-status"></div>
                                                <div class="win-status"></div>
                                                <div class="lose-status"></div>
                                            </div>
                                        </td>
                                        <td><span>54</span></td>
                                    </tr>
                                    <tr>
                                        <td class="topic">
                                            <span class="stand-ucl">2</span>
                                            <span class="logo"><img src="/images/logo-team/mancity.png"></span>
                                            <h4>Man City</h4>
                                        </td>
                                        <td>20</td>
                                        <td>17</td>
                                        <td>3</td>
                                        <td>1</td>
                                        <td>49:10</td>
                                        <td class="text-center">
                                            <div class="static-last-play margin-center">
                                                <div class="win-status"></div>
                                                <div class="lose-status"></div>
                                                <div class="lose-status"></div>
                                                <div class="win-status"></div>
                                                <div class="win-status"></div>
                                            </div>
                                        </td>
                                        <td><span>54</span></td>
                                    </tr>
                                    <tr>
                                        <td class="topic">
                                            <span class="stand-ucl">3</span>
                                            <span class="logo"><img src="/images/logo-team/Tottenham.png"></span>
                                            <h4>Tottenham</h4>
                                        </td>
                                        <td>20</td>
                                        <td>17</td>
                                        <td>3</td>
                                        <td>1</td>
                                        <td>49:10</td>
                                        <td class="text-center">
                                            <div class="static-last-play margin-center">
                                                <div class="win-status"></div>
                                                <div class="win-status"></div>
                                                <div class="win-status"></div>
                                                <div class="lose-status"></div>
                                                <div class="win-status"></div>
                                            </div>
                                        </td>
                                        <td><span>54</span></td>
                                    </tr>
                                    <tr>
                                        <td class="topic">
                                            <span class="stand-ucl">4</span>
                                            <span class="logo"><img src="/images/logo-team/Chelsea.png"></span>
                                            <h4>Chelsea</h4>
                                        </td>
                                        <td>20</td>
                                        <td>17</td>
                                        <td>3</td>
                                        <td>1</td>
                                        <td>49:10</td>
                                        <td class="text-center">
                                            <div class="static-last-play margin-center">
                                                <div class="win-status"></div>
                                                <div class="lose-status"></div>
                                                <div class="win-status"></div>
                                                <div class="win-status"></div>
                                                <div class="draw-status"></div>
                                            </div>
                                        </td>
                                        <td><span>54</span></td>
                                    </tr>
                                    <tr>
                                        <td class="topic">
                                            <span class="stand-uefa">5</span>
                                            <span class="logo"><img src="/images/logo-team/Arsenal.png"></span>
                                            <h4>Arsenal</h4>
                                        </td>
                                        <td>20</td>
                                        <td>17</td>
                                        <td>3</td>
                                        <td>1</td>
                                        <td>49:10</td>
                                        <td class="text-center">
                                            <div class="static-last-play margin-center">
                                                <div class="lose-status"></div>
                                                <div class="win-status"></div>
                                                <div class="draw-status"></div>
                                                <div class="win-status"></div>
                                                <div class="draw-status"></div>
                                            </div>
                                        </td>
                                        <td><span>54</span></td>
                                    </tr>
                                    <tr>
                                        <td class="topic">
                                            <span>6</span>
                                            <span class="logo"><img src="/images/logo-team/manchester.png"></span>
                                            <h4>Man Utd</h4>
                                        </td>
                                        <td>20</td>
                                        <td>17</td>
                                        <td>3</td>
                                        <td>1</td>
                                        <td>49:10</td>
                                        <td class="text-center">
                                            <div class="static-last-play margin-center">
                                                <div class="lose-status"></div>
                                                <div class="win-status"></div>
                                                <div class="win-status"></div>
                                                <div class="win-status"></div>
                                                <div class="win-status"></div>
                                            </div>
                                        </td>
                                        <td><span>54</span></td>
                                    </tr>
                                    <tr>
                                        <td class="topic">
                                            <span>7</span>
                                            <span class="logo"><img src="/images/logo-team/Leicester-City.png"></span>
                                            <h4>Leicester</h4>
                                        </td>
                                        <td>20</td>
                                        <td>17</td>
                                        <td>3</td>
                                        <td>1</td>
                                        <td>49:10</td>
                                        <td class="text-center">
                                            <div class="static-last-play margin-center">
                                                <div class="lose-status"></div>
                                                <div class="win-status"></div>
                                                <div class="win-status"></div>
                                                <div class="lose-status"></div>
                                                <div class="win-status"></div>
                                            </div>
                                        </td>
                                        <td><span>54</span></td>
                                    </tr>
                                    <tr>
                                        <td class="topic">
                                            <span class="stand-fall">18</span>
                                            <span class="logo"><img src="/images/logo-team/Southampton.png"></span>
                                            <h4>Southampton</h4>
                                        </td>
                                        <td>20</td>
                                        <td>17</td>
                                        <td>3</td>
                                        <td>1</td>
                                        <td>49:10</td>
                                        <td class="text-center">
                                            <div class="static-last-play margin-center">
                                                <div class="lose-status"></div>
                                                <div class="lose-status"></div>
                                                <div class="win-status"></div>
                                                <div class="win-status"></div>
                                                <div class="draw-status"></div>
                                            </div>
                                        </td>
                                        <td><span>54</span></td>
                                    </tr>
                                    <tr>
                                        <td class="topic">
                                            <span class="stand-fall">19</span>
                                            <span class="logo"><img src="/images/logo-team/Fulham.png"></span>
                                            <h4>Fulham</h4>
                                        </td>
                                        <td>20</td>
                                        <td>17</td>
                                        <td>3</td>
                                        <td>1</td>
                                        <td>49:10</td>
                                        <td class="text-center">
                                            <div class="static-last-play margin-center">
                                                <div class="lose-status"></div>
                                                <div class="draw-status"></div>
                                                <div class="draw-status"></div>
                                                <div class="win-status"></div>
                                                <div class="lose-status"></div>
                                            </div>
                                        </td>
                                        <td><span>54</span></td>
                                    </tr>
                                    <tr>
                                        <td class="topic">
                                            <span class="stand-fall">20</span>
                                            <span class="logo"><img src="/images/logo-team/Huddersfield.png"></span>
                                            <h4>Huddersfield</h4>
                                        </td>
                                        <td>20</td>
                                        <td>17</td>
                                        <td>3</td>
                                        <td>1</td>
                                        <td>49:10</td>
                                        <td class="text-center">
                                            <div class="static-last-play margin-center">
                                                <div class="lose-status"></div>
                                                <div class="lose-status"></div>
                                                <div class="lose-status"></div>
                                                <div class="lose-status"></div>
                                                <div class="lose-status"></div>
                                            </div>
                                        </td>
                                        <td><span>54</span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="home">...</div>
                            <div role="tabpanel" class="tab-pane" id="away">...</div>
                        </div>

                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="static-table">
                    <div class="title-league">
                        <div class="logo inline-box">
                            <img src="/images/five-leagues/premier-league-logo.png">
                        </div>
                        <div class="content inline-box">
                            <h2>ตารางการแข่งขัน / ดาวซัลโว Premier League</h2>
                            <ul>
                                <li>England</li>
                                <li>|</li>
                                <li>2018-2019</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h4>ตารางการแข่งขัน</h4>
                            <div class="thumbnail">
                                <div class="box-match-tournament">
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">-</div>
                                            <div class="away-score-match">-</div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">-</div>
                                            <div class="away-score-match">-</div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <h4>อันดับดาวซัลโว</h4>
                            <div class="thumbnail">
                                <div class="table-topscore">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td><span>1</span></td>
                                            <td><img src="/images/player/image_4.jpg"></td>
                                            <td>
                                                <span>Eden Hazard</span>
                                                <img src="/images/logo-team/Chelsea.png">
                                                <p>Chelsea</p>
                                            </td>
                                            <td><img src="/images/football.png">23</td>
                                        </tr>
                                        <tr>
                                            <td><span>2</span></td>
                                            <td><img src="/images/player/image_5.jpg"></td>
                                            <td>
                                                <span>Romelu Lukaku</span>
                                                <img src="/images/logo-team/manchester.png">
                                                <p>Manchester United</p>
                                            </td>
                                            <td><img src="/images/football.png">19</td>
                                        </tr>
                                        <tr>
                                            <td><span>3</span></td>
                                            <td><img src="/images/player/image_4.jpg"></td>
                                            <td>
                                                <span>Eden Hazard</span>
                                                <img src="/images/logo-team/Chelsea.png">
                                                <p>Chelsea</p>
                                            </td>
                                            <td><img src="/images/football.png">23</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td><img src="/images/player/image_5.jpg"></td>
                                            <td>
                                                <span>Romelu Lukaku</span>
                                                <img src="/images/logo-team/manchester.png">
                                                <p>Manchester United</p>
                                            </td>
                                            <td><img src="/images/football.png">19</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td><img src="/images/player/image_4.jpg"></td>
                                            <td>
                                                <span>Eden Hazard</span>
                                                <img src="/images/logo-team/Chelsea.png">
                                                <p>Chelsea</p>
                                            </td>
                                            <td><img src="/images/football.png">23</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td><img src="/images/player/image_5.jpg"></td>
                                            <td>
                                                <span>Romelu Lukaku</span>
                                                <img src="/images/logo-team/manchester.png">
                                                <p>Manchester United</p>
                                            </td>
                                            <td><img src="/images/football.png">19</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td><img src="/images/player/image_4.jpg"></td>
                                            <td>
                                                <span>Eden Hazard</span>
                                                <img src="/images/logo-team/Chelsea.png">
                                                <p>Chelsea</p>
                                            </td>
                                            <td><img src="/images/football.png">23</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td><img src="/images/player/image_5.jpg"></td>
                                            <td>
                                                <span>Romelu Lukaku</span>
                                                <img src="/images/logo-team/manchester.png">
                                                <p>Manchester United</p>
                                            </td>
                                            <td><img src="/images/football.png">19</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td><img src="/images/player/image_4.jpg"></td>
                                            <td>
                                                <span>Eden Hazard</span>
                                                <img src="/images/logo-team/Chelsea.png">
                                                <p>Chelsea</p>
                                            </td>
                                            <td><img src="/images/football.png">23</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td><img src="/images/player/image_5.jpg"></td>
                                            <td>
                                                <span>Romelu Lukaku</span>
                                                <img src="/images/logo-team/manchester.png">
                                                <p>Manchester United</p>
                                            </td>
                                            <td><img src="/images/football.png">19</td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="side-list-video" data-spy="affix" data-offset-top="60">
        <div class="nano">
            <div class="nano-content">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#fullmatch-tab" data-toggle="tab"><span>Full Match</span></a>
                    </li>
                    <li role="presentation">
                        <a href="#allgoal-tab" data-toggle="tab"><span>All Goal</span></a>
                    </li>
                </ul>
                <div class="tab-content">
                    <!-- Tab panes -->
                    <div role="tabpanel" class="tab-pane tab-pane-full-match active" id="fullmatch-tab">
                        <div class="box-list-video active bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane tab-pane-all-goal" id="allgoal-tab">
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                        <div class="box-list-video bt-flex">
                            <div class="box-video-thumbnail">
                                <img src="/images/ca10r_first_1.jpg">
                            </div>
                            <div class="box-video-detail">
                                <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                <span class="label label-info">12/12/2019</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $('.box-list-video').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $('.box-list-video.active').removeClass('active');
            $(this).addClass('active');
        }
    });
</script>

<script>
    $("#nav ul li a[href^='#']").on('click', function (e) {

        // prevent default anchor click behavior
        e.preventDefault();

        // store hash
        var hash = this.hash;

        // animate
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 1000, function () {

            // when done, add hash to url
            // (default click behaviour)
            window.location.hash = hash;
        });

    });
</script>
</body>
</html>
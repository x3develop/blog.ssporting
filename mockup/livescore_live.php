<div id="section1" data-spy="affix" data-offset-top="197">

    <div class="content-program-soccer container-live_all">
        <div class="topic">
            <div class="logo-league-program">
                <img src="/images/five-leagues/seriea1.png">
            </div>
            <div class="title-league-program">
                <h3>กัลโช่ เซเรียอา อิตาลี (ITALY SERIE A)</h3>
                <h5>บ้านผลบอล โปรแกรมบอล ราคาบอล </h5>
            </div>
        </div>


        <div class="content">
            <div class="panel-group panel-group-row" id="accordion">
                <!--บอลสด-->
                <div class="panel panel-default livescore-panel livescore-live">
                    <div class="panel-heading">
                        <div class="hdp-float live-float">
                            <ul>
                                <li>2</li>
                                <li>
                                    <div class="progress" data-percentage="40">
				                            <span class="progress-left">
                                                <span class="progress-bar"></span>
				                            </span>
                                        <span class="progress-right">
                                                <span class="progress-bar"></span>
                                            </span>
                                        <div class="progress-value">
                                            <div>45+</div>
                                        </div>
                                    </div>
                                </li>
                                <li>1</li>
                            </ul>
                        </div>
                        <div class="left-accordion-row pull-left">
                            <div class="pull-right logo-team-ponball">
                                <img src="/images/logo-team/manchester.png">
                            </div>
                            <div class="pull-right detail-team-ponball">
                                <div class="top-detail-team-ponball text-right">
                                    <h4>Manchester United</h4>
                                </div>
                                <div class="bottom-detail-team-ponball">
                                    <div class="score-ct">60%</div>
                                    <div class="progress full-width">
                                        <div class="progress-bar pull-right" role="progressbar"
                                             aria-valuenow="60"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <ul class="status-lastMatch full-width">
                                    <li><p>Last Match</p></li>
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                </ul>
                                <ul class="status-lastMatch full-width odd">
                                    <li><p>ODD</p></li>
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                </ul>
                            </div>

                            <div class="pull-left content-col-left">
                                <div class="ct-live">
                                    <span class="label-live">Live</span>
                                </div>
                                <div class="ct-live-chanel">
                                    <img src="/images/channel/pptv.png">
                                </div>
                            </div>
                        </div>
                        <div class="right-accordion-row pull-right">
                            <div class="pull-left logo-team-ponball">
                                <img src="/images/logo-team/Chelsea.png">
                            </div>
                            <div class="pull-left detail-team-ponball">
                                <div class="top-detail-team-ponball text-left">
                                    <h4>Chelsea</h4>
                                </div>
                                <div class="bottom-detail-team-ponball">
                                    <div class="progress full-width">
                                        <div class="progress-bar progress-bar-danger" role="progressbar"
                                             aria-valuenow="20"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 20%;">
                                        </div>
                                    </div>
                                    <div class="score-ct">20%</div>
                                </div>
                            </div>
                            <div class="pull-left">
                                <ul class="status-lastMatch full-width">
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                    <li><p>Last Match</p></li>
                                </ul>
                                <ul class="status-lastMatch full-width odd">
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                    <li><p>ODD</p></li>
                                </ul>
                            </div>
                            <div class="pull-right-section">
                                <div class="intro-game">
                                    <a href="/game.php" data-toggle="tooltip" data-placement="top"
                                       title="เล่นเกมทายผล">
                                        <i class="fa fa-futbol-o" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <a data-toggle="collapse" class="review-dropdown collapsed"
                                   data-parent="#accordion"
                                   href="#collapse_livematch">
                                    <h4>Live score</h4>
                                </a>
                            </div>

                        </div>
                        <div style="clear: both"></div>
                    </div>
                    <div id="collapse_livematch" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-highlight" role="tablist">
                                <li class="active" role="presentation">
                                    <a href="#detail-tab" role="tab"
                                       data-toggle="tab">Detail</a>
                                </li>
                                <li role="presentation">
                                    <a href="#livescore-tab" role="tab"
                                       data-toggle="tab">Livescore</a></li>
                                <li role="presentation">
                                    <a href="#review" role="tab" data-toggle="tab">ทีเด็ด-ทรรศนะ</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="detail-tab">
                                    <!--            Head To Head-->
                                    <div class="section-state">

                                        <div class="thumbnail content-state content-h2h ">
                                            <div class="full-width bt-flex">
                                                <div class="content-state-home">
                                                    <div class="text-title">
                                                        <h4>HEAD TO HEAD</h4>
                                                    </div>
                                                    <div class="full-width inline-box text-center">
                                                        <div class="col-md-6">
                                                            <div class="logo-h2h full-width inline-box text-center">
                                                                <img src="/images/logo-team/manchester.png">
                                                            </div>
                                                            <div class="detail-chart margin-default inline-box">
                                                                <div class="col-detail-chart">
                                                                    <strong>41</strong>
                                                                    <small>win</small>
                                                                </div>
                                                                <div class="col-detail-chart">
                                                                    <strong>58</strong>
                                                                    <small>draw</small>
                                                                </div>
                                                                <div class="col-detail-chart">
                                                                    <strong>6</strong>
                                                                    <small>lose</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="logo-h2h full-width inline-box text-center">
                                                                <img src="/images/logo-team/Chelsea.png">
                                                            </div>
                                                            <div class="detail-chart margin-default inline-box">
                                                                <div class="col-detail-chart">
                                                                    <strong>41</strong>
                                                                    <small>win</small>
                                                                </div>
                                                                <div class="col-detail-chart">
                                                                    <strong>58</strong>
                                                                    <small>draw</small>
                                                                </div>
                                                                <div class="col-detail-chart">
                                                                    <strong>6</strong>
                                                                    <small>lose</small>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="section-state-box text-left">
                                                        <div class="content-section-state">
                                                            <div class="box-table-result-state">
                                                                <div class="box-match-tournament">
                                                                    <div class="box-match-list-topic">
                                                                        <div class="pull-left">
                                                                            <img src="/images/logo-team/fa-cup-2017.png">
                                                                            <p>FA Cup</p>
                                                                        </div>
                                                                        <div class="pull-right">
                                                                            <ul>
                                                                                <li>Odds</li>
                                                                                <li>Result</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-match-list">

                                                                        <div class="text-center">
                                                                            <div class="date-match">15 Apr 18
                                                                            </div>
                                                                            <div class="time-match">15:00</div>
                                                                        </div>
                                                                        <div>
                                                                            <div class="home-name-match team-bet-color">
                                                                                <img
                                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                                            </div>
                                                                            <div class="away-name-match hignlight-team-win">
                                                                                <img
                                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                                united
                                                                            </div>
                                                                        </div>
                                                                        <div class="match-score text-center">
                                                                            <div class="home-score-match">0
                                                                            </div>
                                                                            <div class="away-score-match">2
                                                                            </div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <div class="odds-match">
                                                                                <div class="win-status"></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <div class="result-match">
                                                                                <div class="lose-status"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-match-list">

                                                                        <div class="text-center">
                                                                            <div class="date-match">15 Apr 18
                                                                            </div>
                                                                            <div class="time-match">15:00</div>
                                                                        </div>
                                                                        <div>
                                                                            <div class="home-name-match hignlight-team-win">
                                                                                <img
                                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                                            </div>
                                                                            <div class="away-name-match team-bet-color">
                                                                                <img
                                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                                united
                                                                            </div>
                                                                        </div>
                                                                        <div class="match-score text-center">
                                                                            <div class="home-score-match">0
                                                                            </div>
                                                                            <div class="away-score-match">2
                                                                            </div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <div class="odds-match">
                                                                                <div class="draw-status"></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <div class="result-match">
                                                                                <div class="win-status"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="box-match-tournament">
                                                                    <div class="box-match-list-topic">
                                                                        <div class="pull-left">
                                                                            <img src="/images/five-leagues/premier-league-logo.png">
                                                                            <p>Premier League</p>
                                                                        </div>
                                                                        <div class="pull-right">
                                                                            <ul>
                                                                                <li>Odds</li>
                                                                                <li>Result</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-match-list">

                                                                        <div class="text-center">
                                                                            <div class="date-match">15 Apr 18
                                                                            </div>
                                                                            <div class="time-match">15:00</div>
                                                                        </div>
                                                                        <div>
                                                                            <div class="home-name-match hignlight-team-win">
                                                                                <img
                                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                                            </div>
                                                                            <div class="away-name-match"><img
                                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                                united
                                                                            </div>
                                                                        </div>
                                                                        <div class="match-score text-center">
                                                                            <div class="home-score-match">0
                                                                            </div>
                                                                            <div class="away-score-match">2
                                                                            </div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <div class="odds-match">
                                                                                <img src="/images/icon-stat/result/0.png">
                                                                            </div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <div class="result-match">
                                                                                <img src="/images/icon-stat/result/2.png">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-match-list">

                                                                        <div class="text-center">
                                                                            <div class="date-match">15 Apr 18
                                                                            </div>
                                                                            <div class="time-match">15:00</div>
                                                                        </div>
                                                                        <div>
                                                                            <div class="home-name-match hignlight-team-win">
                                                                                <img
                                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                                            </div>
                                                                            <div class="away-name-match"><img
                                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                                united
                                                                            </div>
                                                                        </div>
                                                                        <div class="match-score text-center">
                                                                            <div class="home-score-match">0
                                                                            </div>
                                                                            <div class="away-score-match">2
                                                                            </div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <div class="odds-match">
                                                                                <img src="/images/icon-stat/result/0.png">
                                                                            </div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <div class="result-match">
                                                                                <img src="/images/icon-stat/result/2.png">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="box-match-tournament">
                                                                    <div class="box-match-list-topic">
                                                                        <div class="pull-left">
                                                                            <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                                                            <p>UEFA Champions League</p>
                                                                        </div>
                                                                        <div class="pull-right">
                                                                            <ul>
                                                                                <li>Odds</li>
                                                                                <li>Result</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-match-list">

                                                                        <div class="text-center">
                                                                            <div class="date-match">15 Apr 18
                                                                            </div>
                                                                            <div class="time-match">15:00</div>
                                                                        </div>
                                                                        <div>
                                                                            <div class="home-name-match"><img
                                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                                            </div>
                                                                            <div class="away-name-match hignlight-team-win">
                                                                                <img
                                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                                united
                                                                            </div>
                                                                        </div>
                                                                        <div class="match-score text-center">
                                                                            <div class="home-score-match">0
                                                                            </div>
                                                                            <div class="away-score-match">2
                                                                            </div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <div class="odds-match">
                                                                                <img src="/images/icon-stat/result/0.png">
                                                                            </div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <div class="result-match">
                                                                                <img src="/images/icon-stat/result/2.png">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-match-list">

                                                                        <div class="text-center">
                                                                            <div class="date-match">15 Apr 18
                                                                            </div>
                                                                            <div class="time-match">15:00</div>
                                                                        </div>
                                                                        <div>
                                                                            <div class="home-name-match"><img
                                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                                            </div>
                                                                            <div class="away-name-match hignlight-team-win">
                                                                                <img
                                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                                united
                                                                            </div>
                                                                        </div>
                                                                        <div class="match-score text-center">
                                                                            <div class="home-score-match">0
                                                                            </div>
                                                                            <div class="away-score-match">2
                                                                            </div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <div class="odds-match">
                                                                                <img src="/images/icon-stat/result/0.png">
                                                                            </div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <div class="result-match">
                                                                                <img src="/images/icon-stat/result/2.png">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="content-state-away content-state-home">
                                                    <div class="text-title">
                                                        <h4>Lineups</h4>
                                                    </div>
                                                    <div class="lineUp-table">
                                                        <table class="table">
                                                            <tbody>
                                                            <tr>
                                                                <td>
                                                                    <div class="pull-right full-width text-right">
                                                                        <ul>
                                                                            <li>
                                                                                <h4>Manchester United</h4>
                                                                                <p>Coach</p>
                                                                                <p>Ole Gunnar Solskjaer</p>
                                                                            </li>
                                                                            <li>
                                                                                <img src="/images/logo-team/manchester.png">
                                                                            </li>

                                                                        </ul>
                                                                    </div>
                                                                    <div class="pull-right full-width">
                                                                        <div class="title-center full-width">
                                                                            Player
                                                                        </div>
                                                                        <div class="content-lineUp-home inline-box full-width">
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="num-player">1
                                                                                    </div>
                                                                                    <div>David De Gea</div>
                                                                                    <div>GK</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">2
                                                                                    </div>
                                                                                    <div>Victor Lindelöf</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">6
                                                                                    </div>
                                                                                    <div>Paul Pogba</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">8
                                                                                    </div>
                                                                                    <div>Juan Mata</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">9
                                                                                    </div>
                                                                                    <div>Romelu Lukaku</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">10
                                                                                    </div>
                                                                                    <div>Marcus Rashford</div>
                                                                                    <div>FT</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">11
                                                                                    </div>
                                                                                    <div>Anthony Martial</div>
                                                                                    <div>RF</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">12
                                                                                    </div>
                                                                                    <div>Chris Smalling</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">15
                                                                                    </div>
                                                                                    <div>Andreas Pereira</div>
                                                                                    <div>MD</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">12
                                                                                    </div>
                                                                                    <div>Chris Smalling</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">15
                                                                                    </div>
                                                                                    <div>Andreas Pereira</div>
                                                                                    <div>MD</div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="title-center full-width">
                                                                            Substitute
                                                                        </div>
                                                                        <div class="content-lineUp-home inline-box full-width">
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="num-player">1
                                                                                    </div>
                                                                                    <div>David De Gea</div>
                                                                                    <div>GK</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">2
                                                                                    </div>
                                                                                    <div>Victor Lindelöf</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">6
                                                                                    </div>
                                                                                    <div>Paul Pogba</div>
                                                                                    <div>CM</div>
                                                                                </li>


                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="pull-left full-width">
                                                                        <ul>
                                                                            <li>
                                                                                <img src="/images/logo-team/Chelsea.png">
                                                                            </li>
                                                                            <li>
                                                                                <h4>Chelsea</h4>
                                                                                <p>Coach</p>
                                                                                <p>Maurizio Sarri</p>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="pull-left full-width">
                                                                        <div class="title-center full-width">
                                                                            Player
                                                                        </div>
                                                                        <div class="content-lineUp-away inline-box">
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="num-player">1
                                                                                    </div>
                                                                                    <div>David De Gea</div>
                                                                                    <div>GK</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">2
                                                                                    </div>
                                                                                    <div>Victor Lindelöf</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">6
                                                                                    </div>
                                                                                    <div>Paul Pogba</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">8
                                                                                    </div>
                                                                                    <div>Juan Mata</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">9
                                                                                    </div>
                                                                                    <div>Romelu Lukaku</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">10
                                                                                    </div>
                                                                                    <div>Marcus Rashford</div>
                                                                                    <div>FT</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">11
                                                                                    </div>
                                                                                    <div>Anthony Martial</div>
                                                                                    <div>RF</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">12
                                                                                    </div>
                                                                                    <div>Chris Smalling</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">15
                                                                                    </div>
                                                                                    <div>Andreas Pereira</div>
                                                                                    <div>MD</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">12
                                                                                    </div>
                                                                                    <div>Chris Smalling</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">15
                                                                                    </div>
                                                                                    <div>Andreas Pereira</div>
                                                                                    <div>MD</div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="title-center full-width">
                                                                            Substitute
                                                                        </div>
                                                                        <div class="content-lineUp-away inline-box">
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="num-player">1
                                                                                    </div>
                                                                                    <div>David De Gea</div>
                                                                                    <div>GK</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">2
                                                                                    </div>
                                                                                    <div>Victor Lindelöf</div>
                                                                                    <div>CB</div>
                                                                                </li>

                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>


                                                            </tbody>
                                                        </table>
                                                    </div>

                                                    <div style="clear: both"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="livescore-tab">
                                    <div class="col-md-4">

                                        <div class="thumbnail">

                                            <div class="header-event-match">
                                                <ul>
                                                    <li><h3>EVENT MATCH</h3></li>
                                                    <li>
                                                            <span class="real-time-match">
                                                               45+
                                                            </span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="score-event-match">

                                                <div class="pull-right">
                                                    <img src="/images/logo-team/manchester.png">
                                                    <p>Manchester United</p>

                                                </div>
                                                <h4>2-2</h4>
                                                <div class="pull-left text-right">
                                                    <p>Chelsea</p>
                                                    <img src="/images/logo-team/Chelsea.png">
                                                </div>

                                            </div>
                                            <div class="content-event-match">

                                                <!--                                        Home ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="red-card"></div>
                                                        </div>
                                                        <div class="min-live">90+4'</div>
                                                        <div class="name-live">Willian</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home ใบเหลือง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="yellow-card"></div>
                                                        </div>
                                                        <div class="min-live">87'</div>
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home ทำประตู-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="goal-score"></div>
                                                        </div>
                                                        <div class="min-live">71'</div>
                                                        <div class="score-live">2 - 4</div>
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home เปลี่ยนตัว-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="substitute-player"></div>
                                                        </div>
                                                        <div class="min-live">69'</div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-out">out</span>
                                                            David
                                                            Luiz
                                                        </div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-in">in</span>
                                                            Victor
                                                            Moses
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Home ใบเหลือง+ใบเหลือง=ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="group-yellow-card"></div>
                                                        </div>
                                                        <div class="min-live">55'</div>
                                                        <div class="name-live">Ross Barkley</div>
                                                    </div>
                                                </div>
                                                <!--Home จุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="penalty-score"></div>
                                                        </div>
                                                        <div class="min-live">45'</div>
                                                        <div class="name-live">Gary Cahill</div>
                                                    </div>
                                                </div>
                                                <!--Home พลาดจุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="penalty-none-score"></div>
                                                        </div>
                                                        <div class="min-live">41'</div>
                                                        <div class="name-live">Álvaro Morata</div>
                                                    </div>
                                                </div>
                                                <!--Home ทำเข้าประตูตัวเอง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="own-goal"></div>
                                                        </div>
                                                        <div class="min-live">28'</div>
                                                        <div class="name-live">Álvaro Morata</div>
                                                    </div>
                                                </div>


                                                <!--                                        Away ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Willian</div>
                                                        <div class="min-live">90+4'</div>
                                                        <div class="box-football-icon">
                                                            <div class="red-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away ใบเหลือง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                        <div class="min-live">87'</div>
                                                        <div class="box-football-icon">
                                                            <div class="yellow-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away ทำประตู-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                        <div class="score-live">2 - 4</div>
                                                        <div class="min-live">71'</div>
                                                        <div class="box-football-icon">
                                                            <div class="goal-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away เปลี่ยนตัว-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live"><span
                                                                    class="substitute-player-in">in</span>
                                                            Victor
                                                            Moses
                                                        </div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-out">out</span>
                                                            David
                                                            Luiz
                                                        </div>
                                                        <div class="min-live">69'</div>
                                                        <div class="box-football-icon">
                                                            <div class="substitute-player"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away ใบเหลือง+ใบเหลือง=ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Ross Barkley</div>
                                                        <div class="min-live">55'</div>
                                                        <div class="box-football-icon">
                                                            <div class="group-yellow-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away จุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Gary Cahill</div>
                                                        <div class="min-live">45'</div>
                                                        <div class="box-football-icon">
                                                            <div class="penalty-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away พลาดจุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Álvaro Morata</div>
                                                        <div class="min-live">41'</div>
                                                        <div class="box-football-icon">
                                                            <div class="penalty-none-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away ทำเข้าประตูตัวเอง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Álvaro Morata</div>
                                                        <div class="min-live">28'</div>
                                                        <div class="box-football-icon">
                                                            <div class="own-goal"></div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 content-status-match">
                                        <div class="thumbnail">

                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#statistics-tab"
                                                                                          role="tab"
                                                                                          data-toggle="tab">Statistics</a>
                                                </li>
                                                <li role="presentation"><a href="#lineUp-tab"
                                                                           role="tab"
                                                                           data-toggle="tab">Line
                                                        Up</a>
                                                </li>

                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active"
                                                     id="statistics-tab">
                                                    <div class="table statistics-table">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <div class="pull-right">
                                                                        <h4>Manchester United
                                                                        </h4>
                                                                        <img src="/images/logo-team/manchester.png">
                                                                    </div>
                                                                </td>
                                                                <td><h3>VS</h3></td>
                                                                <td>
                                                                    <div class="pull-left">
                                                                        <img src="/images/logo-team/Chelsea.png">
                                                                        <h4>Chelsea</h4>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ยิงประตู-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">15</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 15%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ยิงประตู</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="9" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 9%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">9</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ยิงตรงกรอบ-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">5</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 5%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ยิงตรงกรอบ</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="9" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 2%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">2</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ผิดกติกา-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">13</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 13%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ผิดกติกา</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="8" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 8%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">8</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เตะมุม-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">2</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="2"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 2%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เตะมุม</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">3</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ฟรีคิก-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">12</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="12"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 12%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ฟรีคิก</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="19"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 19%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">19</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ล้ำหน้า-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">6</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="6"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 6%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ล้ำหน้า</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="4" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 4%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">4</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                จำนวนใบเหลือง-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">3</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="3"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ล้ำหน้า</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">3</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เวลาครองบอล-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">55%</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="55"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 55%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เวลาครองบอล</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="45"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 45%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">45%</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เซฟได้-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">1</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="1"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 1%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เซฟได้</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="4" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 4%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">4</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เปลี่ยนตัว-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">3</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="3"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เปลี่ยนตัว</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">3</div>
                                                                </td>
                                                            </tr>


                                                        </table>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="lineUp-tab">
                                                    <div class="lineUp-table">
                                                        <table class="table">
                                                            <tr>
                                                                <td>
                                                                    <div class="pull-right full-width text-right">
                                                                        <span>Manchester United</span>
                                                                        <img src="/images/logo-team/manchester.png">
                                                                    </div>
                                                                    <div class="pull-right full-width">
                                                                        <div class="content-lineUp-home">
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="num-player">1
                                                                                    </div>
                                                                                    <div>David De Gea</div>
                                                                                    <div>GK</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">2
                                                                                    </div>
                                                                                    <div>Victor Lindelöf</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">6
                                                                                    </div>
                                                                                    <div>Paul Pogba</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">8
                                                                                    </div>
                                                                                    <div>Juan Mata</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">9
                                                                                    </div>
                                                                                    <div>Romelu Lukaku</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">10
                                                                                    </div>
                                                                                    <div>Marcus Rashford</div>
                                                                                    <div>FT</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">11
                                                                                    </div>
                                                                                    <div>Anthony Martial</div>
                                                                                    <div>RF</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">12
                                                                                    </div>
                                                                                    <div>Chris Smalling</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">15
                                                                                    </div>
                                                                                    <div>Andreas Pereira</div>
                                                                                    <div>MD</div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="pull-left">
                                                                        <img src="/images/logo-team/Chelsea.png">
                                                                        <span>Chelsea</span>
                                                                    </div>
                                                                    <div class="pull-left full-width">
                                                                        <div class="content-lineUp-away">
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="num-player">1
                                                                                    </div>
                                                                                    <div>David De Gea</div>
                                                                                    <div>GK</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">2
                                                                                    </div>
                                                                                    <div>Victor Lindelöf</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">6
                                                                                    </div>
                                                                                    <div>Paul Pogba</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">8
                                                                                    </div>
                                                                                    <div>Juan Mata</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">9
                                                                                    </div>
                                                                                    <div>Romelu Lukaku</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">10
                                                                                    </div>
                                                                                    <div>Marcus Rashford</div>
                                                                                    <div>FT</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">11
                                                                                    </div>
                                                                                    <div>Anthony Martial</div>
                                                                                    <div>RF</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">12
                                                                                    </div>
                                                                                    <div>Chris Smalling</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">15
                                                                                    </div>
                                                                                    <div>Andreas Pereira</div>
                                                                                    <div>MD</div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>


                                                        </table>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="statistics">
                                    <div class="table statistics-table">
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="pull-right">
                                                        <img src="/images/logo-team/manchester.png">
                                                    </div>
                                                </td>
                                                <td><h3>VS</h3></td>
                                                <td>
                                                    <div class="pull-left">
                                                        <img src="/images/logo-team/Chelsea.png">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>เริ่มเขี่ยบอล</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>เปลี่ยนตัวคนแรก</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="check-team"></span></td>
                                                <td>เริ่มเขี่ยบอล</td>
                                                <td></td>
                                            </tr>
                                            <!--                                                ยิงประตู-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">15</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 15%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ยิงประตู</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="9" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 9%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">9</div>
                                                </td>
                                            </tr>
                                            <!--                                                ยิงตรงกรอบ-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">5</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 5%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ยิงตรงกรอบ</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="9" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 2%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">2</div>
                                                </td>
                                            </tr>
                                            <!--                                                ผิดกติกา-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">13</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 13%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ผิดกติกา</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="8" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 8%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">8</div>
                                                </td>
                                            </tr>
                                            <!--                                                เตะมุม-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">2</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="2"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 2%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เตะมุม</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">3</div>
                                                </td>
                                            </tr>
                                            <!--                                                ฟรีคิก-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">12</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="12"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 12%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ฟรีคิก</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="19" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 19%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">19</div>
                                                </td>
                                            </tr>
                                            <!--                                                ล้ำหน้า-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">6</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="6"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 6%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ล้ำหน้า</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="4" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 4%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">4</div>
                                                </td>
                                            </tr>
                                            <!--                                                จำนวนใบเหลือง-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">3</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="3"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 3%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ล้ำหน้า</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">3</div>
                                                </td>
                                            </tr>
                                            <!--                                                เวลาครองบอล-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">55%</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="55"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 55%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เวลาครองบอล</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="45" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 45%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">45%</div>
                                                </td>
                                            </tr>
                                            <!--                                                เซฟได้-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">1</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="1"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 1%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เซฟได้</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="4" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 4%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">4</div>
                                                </td>
                                            </tr>
                                            <!--                                                เปลี่ยนตัว-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">3</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="3"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 3%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เปลี่ยนตัว</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">3</div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="check-team"></span></td>
                                                <td>เปลี่ยนตัวคนสุดท้าย</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ใบเหลืองใบสุดท้าย</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>


                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="review">
                                    <div class="home-panel-body pull-left">
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/profess-player/sample-logo01.png">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>LOMTOE.NET</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png"</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/profess-player/sample-logo05.png">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>SMM SPORT</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png"</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    อาจจะฝืดๆหน่อย​ แต่ต้องสอย​ อย่าให้เสียชื่อทีมใหญ่
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/9.jpg">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>หมูตัวนี้ตัวใหญ่</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png">
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    ชอบความคมของตรานกแก้ว ซวยแล้วบายาโดลิด
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="away-panel-body pull-right">
                                        <div class="content-away-review full-width pull-left">
                                            <div class="pull-left">
                                                <div class="area-logoName area-logoName-away">
                                                    <div class="pull-left logo-review-user">
                                                        <img src="/images/profess-player/sample-logo12.png">
                                                    </div>
                                                    <div class="pull-left">
                                                        <h4>GOAL.IN.TH</h4>
                                                        <ul>
                                                            <li><img src="/images/logo-team/Chelsea.png"</li>
                                                            <li>@Chelsea</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-left text-review-content text-left">
                                                <p>
                                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                                </p>
                                            </div>
                                            <div class="pull-right ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="full-width text-center">
                                        <a class="btn btn-default btn-sm" href="#"
                                           role="button">อ่านทีเด็ดทรรศนะทั้งหมด</a>
                                    </div>
                                </div>
                                <div style="clear: both"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default livescore-panel livescore-live">
                    <div class="panel-heading">
                        <div class="hdp-float live-float">
                            <ul>
                                <li>2</li>
                                <li>
                                    <div class="progress" data-percentage="40">
				                            <span class="progress-left">
                                                <span class="progress-bar"></span>
				                            </span>
                                        <span class="progress-right">
                                                <span class="progress-bar"></span>
                                            </span>
                                        <div class="progress-value">
                                            <div>45+</div>
                                        </div>
                                    </div>
                                </li>
                                <li>1</li>
                            </ul>
                        </div>
                        <div class="left-accordion-row pull-left">
                            <div class="pull-right logo-team-ponball">
                                <img src="/images/logo-team/manchester.png">
                            </div>
                            <div class="pull-right detail-team-ponball">
                                <div class="top-detail-team-ponball text-right">
                                    <h4>Manchester United</h4>
                                </div>
                                <div class="bottom-detail-team-ponball">
                                    <div class="score-ct">60%</div>
                                    <div class="progress full-width">
                                        <div class="progress-bar pull-right" role="progressbar"
                                             aria-valuenow="60"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <ul class="status-lastMatch full-width">
                                    <li><p>Last Match</p></li>
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                </ul>
                                <ul class="status-lastMatch full-width odd">
                                    <li><p>ODD</p></li>
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                </ul>
                            </div>

                            <div class="pull-left content-col-left">
                                <div class="ct-live">
                                    <span class="label-live">Live</span>
                                </div>
                                <div class="ct-live-chanel">
                                    <img src="/images/channel/pptv.png">
                                </div>
                            </div>
                        </div>
                        <div class="right-accordion-row pull-right">
                            <div class="pull-left logo-team-ponball">
                                <img src="/images/logo-team/Chelsea.png">
                            </div>
                            <div class="pull-left detail-team-ponball">
                                <div class="top-detail-team-ponball text-left">
                                    <h4>Chelsea</h4>
                                </div>
                                <div class="bottom-detail-team-ponball">
                                    <div class="progress full-width">
                                        <div class="progress-bar progress-bar-danger" role="progressbar"
                                             aria-valuenow="20"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 20%;">
                                        </div>
                                    </div>
                                    <div class="score-ct">20%</div>
                                </div>
                            </div>
                            <div class="pull-left">
                                <ul class="status-lastMatch full-width">
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                    <li><p>Last Match</p></li>
                                </ul>
                                <ul class="status-lastMatch full-width odd">
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                    <li><p>ODD</p></li>
                                </ul>
                            </div>
                            <div class="pull-right-section">
                                <div class="intro-game">
                                    <a href="/game.php" data-toggle="tooltip" data-placement="top"
                                       title="เล่นเกมทายผล">
                                        <i class="fa fa-futbol-o" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <a data-toggle="collapse" class="review-dropdown collapsed"
                                   data-parent="#accordion"
                                   href="#collapse_livematch">
                                    <h4>Live score</h4>
                                </a>
                            </div>

                        </div>
                        <div style="clear: both"></div>
                    </div>
                    <div id="collapse_livematch" class="panel-collapse collapse">
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-highlight" role="tablist">
                                <li class="active" role="presentation">
                                    <a href="#livescore-tab" role="tab"
                                       data-toggle="tab">Livescore</a></li>
                                <li role="presentation">
                                    <a href="#review" role="tab" data-toggle="tab">ทีเด็ด-ทรรศนะ</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="livescore-tab">
                                    <div class="col-md-4">

                                        <div class="thumbnail">

                                            <div class="header-event-match">
                                                <ul>
                                                    <li><h3>EVENT MATCH</h3></li>
                                                    <li>
                                                            <span class="real-time-match">
                                                               45+
                                                            </span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="score-event-match">

                                                <div class="pull-right">
                                                    <img src="/images/logo-team/manchester.png">
                                                    <p>Manchester United</p>

                                                </div>
                                                <h4>2-2</h4>
                                                <div class="pull-left text-right">
                                                    <p>Chelsea</p>
                                                    <img src="/images/logo-team/Chelsea.png">
                                                </div>

                                            </div>
                                            <div class="content-event-match">

                                                <!--                                        Home ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="red-card"></div>
                                                        </div>
                                                        <div class="min-live">90+4'</div>
                                                        <div class="name-live">Willian</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home ใบเหลือง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="yellow-card"></div>
                                                        </div>
                                                        <div class="min-live">87'</div>
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home ทำประตู-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="goal-score"></div>
                                                        </div>
                                                        <div class="min-live">71'</div>
                                                        <div class="score-live">2 - 4</div>
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home เปลี่ยนตัว-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="substitute-player"></div>
                                                        </div>
                                                        <div class="min-live">69'</div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-out">out</span>
                                                            David
                                                            Luiz
                                                        </div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-in">in</span>
                                                            Victor
                                                            Moses
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Home ใบเหลือง+ใบเหลือง=ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="group-yellow-card"></div>
                                                        </div>
                                                        <div class="min-live">55'</div>
                                                        <div class="name-live">Ross Barkley</div>
                                                    </div>
                                                </div>
                                                <!--Home จุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="penalty-score"></div>
                                                        </div>
                                                        <div class="min-live">45'</div>
                                                        <div class="name-live">Gary Cahill</div>
                                                    </div>
                                                </div>
                                                <!--Home พลาดจุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="penalty-none-score"></div>
                                                        </div>
                                                        <div class="min-live">41'</div>
                                                        <div class="name-live">Álvaro Morata</div>
                                                    </div>
                                                </div>
                                                <!--Home ทำเข้าประตูตัวเอง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="own-goal"></div>
                                                        </div>
                                                        <div class="min-live">28'</div>
                                                        <div class="name-live">Álvaro Morata</div>
                                                    </div>
                                                </div>


                                                <!--                                        Away ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Willian</div>
                                                        <div class="min-live">90+4'</div>
                                                        <div class="box-football-icon">
                                                            <div class="red-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away ใบเหลือง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                        <div class="min-live">87'</div>
                                                        <div class="box-football-icon">
                                                            <div class="yellow-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away ทำประตู-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                        <div class="score-live">2 - 4</div>
                                                        <div class="min-live">71'</div>
                                                        <div class="box-football-icon">
                                                            <div class="goal-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away เปลี่ยนตัว-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live"><span
                                                                    class="substitute-player-in">in</span>
                                                            Victor
                                                            Moses
                                                        </div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-out">out</span>
                                                            David
                                                            Luiz
                                                        </div>
                                                        <div class="min-live">69'</div>
                                                        <div class="box-football-icon">
                                                            <div class="substitute-player"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away ใบเหลือง+ใบเหลือง=ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Ross Barkley</div>
                                                        <div class="min-live">55'</div>
                                                        <div class="box-football-icon">
                                                            <div class="group-yellow-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away จุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Gary Cahill</div>
                                                        <div class="min-live">45'</div>
                                                        <div class="box-football-icon">
                                                            <div class="penalty-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away พลาดจุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Álvaro Morata</div>
                                                        <div class="min-live">41'</div>
                                                        <div class="box-football-icon">
                                                            <div class="penalty-none-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away ทำเข้าประตูตัวเอง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Álvaro Morata</div>
                                                        <div class="min-live">28'</div>
                                                        <div class="box-football-icon">
                                                            <div class="own-goal"></div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 content-status-match">
                                        <div class="thumbnail">

                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#statistics-tab"
                                                                                          role="tab"
                                                                                          data-toggle="tab">Statistics</a>
                                                </li>
                                                <li role="presentation"><a href="#lineUp-tab"
                                                                           role="tab"
                                                                           data-toggle="tab">Line
                                                        Up</a>
                                                </li>

                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active"
                                                     id="statistics-tab">
                                                    <div class="table statistics-table">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <div class="pull-right">
                                                                        <h4>Manchester United
                                                                        </h4>
                                                                        <img src="/images/logo-team/manchester.png">
                                                                    </div>
                                                                </td>
                                                                <td><h3>VS</h3></td>
                                                                <td>
                                                                    <div class="pull-left">
                                                                        <img src="/images/logo-team/Chelsea.png">
                                                                        <h4>Chelsea</h4>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ยิงประตู-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">15</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 15%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ยิงประตู</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="9" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 9%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">9</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ยิงตรงกรอบ-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">5</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 5%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ยิงตรงกรอบ</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="9" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 2%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">2</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ผิดกติกา-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">13</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 13%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ผิดกติกา</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="8" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 8%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">8</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เตะมุม-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">2</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="2"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 2%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เตะมุม</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">3</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ฟรีคิก-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">12</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="12"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 12%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ฟรีคิก</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="19"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 19%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">19</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ล้ำหน้า-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">6</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="6"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 6%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ล้ำหน้า</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="4" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 4%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">4</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                จำนวนใบเหลือง-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">3</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="3"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ล้ำหน้า</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">3</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เวลาครองบอล-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">55%</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="55"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 55%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เวลาครองบอล</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="45"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 45%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">45%</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เซฟได้-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">1</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="1"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 1%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เซฟได้</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="4" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 4%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">4</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เปลี่ยนตัว-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">3</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="3"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เปลี่ยนตัว</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">3</div>
                                                                </td>
                                                            </tr>


                                                        </table>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="lineUp-tab">
                                                    <div class="lineUp-table">
                                                        <table class="table">
                                                            <tr>
                                                                <td>
                                                                    <div class="pull-right full-width text-right">
                                                                        <span>Manchester United</span>
                                                                        <img src="/images/logo-team/manchester.png">
                                                                    </div>
                                                                    <div class="pull-right full-width">
                                                                        <div class="content-lineUp-home">
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="num-player">1
                                                                                    </div>
                                                                                    <div>David De Gea</div>
                                                                                    <div>GK</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">2
                                                                                    </div>
                                                                                    <div>Victor Lindelöf</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">6
                                                                                    </div>
                                                                                    <div>Paul Pogba</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">8
                                                                                    </div>
                                                                                    <div>Juan Mata</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">9
                                                                                    </div>
                                                                                    <div>Romelu Lukaku</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">10
                                                                                    </div>
                                                                                    <div>Marcus Rashford</div>
                                                                                    <div>FT</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">11
                                                                                    </div>
                                                                                    <div>Anthony Martial</div>
                                                                                    <div>RF</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">12
                                                                                    </div>
                                                                                    <div>Chris Smalling</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">15
                                                                                    </div>
                                                                                    <div>Andreas Pereira</div>
                                                                                    <div>MD</div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="pull-left">
                                                                        <img src="/images/logo-team/Chelsea.png">
                                                                        <span>Chelsea</span>
                                                                    </div>
                                                                    <div class="pull-left full-width">
                                                                        <div class="content-lineUp-away">
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="num-player">1
                                                                                    </div>
                                                                                    <div>David De Gea</div>
                                                                                    <div>GK</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">2
                                                                                    </div>
                                                                                    <div>Victor Lindelöf</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">6
                                                                                    </div>
                                                                                    <div>Paul Pogba</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">8
                                                                                    </div>
                                                                                    <div>Juan Mata</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">9
                                                                                    </div>
                                                                                    <div>Romelu Lukaku</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">10
                                                                                    </div>
                                                                                    <div>Marcus Rashford</div>
                                                                                    <div>FT</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">11
                                                                                    </div>
                                                                                    <div>Anthony Martial</div>
                                                                                    <div>RF</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">12
                                                                                    </div>
                                                                                    <div>Chris Smalling</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">15
                                                                                    </div>
                                                                                    <div>Andreas Pereira</div>
                                                                                    <div>MD</div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>


                                                        </table>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="statistics">
                                    <div class="table statistics-table">
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="pull-right">
                                                        <img src="/images/logo-team/manchester.png">
                                                    </div>
                                                </td>
                                                <td><h3>VS</h3></td>
                                                <td>
                                                    <div class="pull-left">
                                                        <img src="/images/logo-team/Chelsea.png">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>เริ่มเขี่ยบอล</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>เปลี่ยนตัวคนแรก</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="check-team"></span></td>
                                                <td>เริ่มเขี่ยบอล</td>
                                                <td></td>
                                            </tr>
                                            <!--                                                ยิงประตู-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">15</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 15%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ยิงประตู</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="9" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 9%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">9</div>
                                                </td>
                                            </tr>
                                            <!--                                                ยิงตรงกรอบ-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">5</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 5%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ยิงตรงกรอบ</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="9" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 2%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">2</div>
                                                </td>
                                            </tr>
                                            <!--                                                ผิดกติกา-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">13</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 13%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ผิดกติกา</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="8" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 8%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">8</div>
                                                </td>
                                            </tr>
                                            <!--                                                เตะมุม-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">2</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="2"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 2%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เตะมุม</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">3</div>
                                                </td>
                                            </tr>
                                            <!--                                                ฟรีคิก-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">12</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="12"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 12%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ฟรีคิก</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="19" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 19%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">19</div>
                                                </td>
                                            </tr>
                                            <!--                                                ล้ำหน้า-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">6</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="6"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 6%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ล้ำหน้า</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="4" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 4%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">4</div>
                                                </td>
                                            </tr>
                                            <!--                                                จำนวนใบเหลือง-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">3</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="3"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 3%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ล้ำหน้า</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">3</div>
                                                </td>
                                            </tr>
                                            <!--                                                เวลาครองบอล-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">55%</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="55"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 55%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เวลาครองบอล</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="45" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 45%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">45%</div>
                                                </td>
                                            </tr>
                                            <!--                                                เซฟได้-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">1</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="1"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 1%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เซฟได้</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="4" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 4%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">4</div>
                                                </td>
                                            </tr>
                                            <!--                                                เปลี่ยนตัว-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">3</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="3"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 3%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เปลี่ยนตัว</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">3</div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="check-team"></span></td>
                                                <td>เปลี่ยนตัวคนสุดท้าย</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ใบเหลืองใบสุดท้าย</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>


                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="lineUp">

                                </div>
                                <div role="tabpanel" class="tab-pane" id="review">
                                    <div class="home-panel-body pull-left">
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/profess-player/sample-logo01.png">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>LOMTOE.NET</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png"</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/profess-player/sample-logo05.png">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>SMM SPORT</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png"</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    อาจจะฝืดๆหน่อย​ แต่ต้องสอย​ อย่าให้เสียชื่อทีมใหญ่
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/9.jpg">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>หมูตัวนี้ตัวใหญ่</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png">
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    ชอบความคมของตรานกแก้ว ซวยแล้วบายาโดลิด
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="away-panel-body pull-right">
                                        <div class="content-away-review full-width pull-left">
                                            <div class="pull-left">
                                                <div class="area-logoName area-logoName-away">
                                                    <div class="pull-left logo-review-user">
                                                        <img src="/images/profess-player/sample-logo12.png">
                                                    </div>
                                                    <div class="pull-left">
                                                        <h4>GOAL.IN.TH</h4>
                                                        <ul>
                                                            <li><img src="/images/logo-team/Chelsea.png"</li>
                                                            <li>@Chelsea</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-left text-review-content text-left">
                                                <p>
                                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                                </p>
                                            </div>
                                            <div class="pull-right ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="full-width text-center">
                                        <a class="btn btn-default btn-sm" href="#"
                                           role="button">อ่านทีเด็ดทรรศนะทั้งหมด</a>
                                    </div>
                                </div>
                                <div style="clear: both"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default livescore-panel livescore-live">
                    <div class="panel-heading">
                        <div class="hdp-float live-float">
                            <ul>
                                <li>2</li>
                                <li>
                                    <div class="progress" data-percentage="40">
				                            <span class="progress-left">
                                                <span class="progress-bar"></span>
				                            </span>
                                        <span class="progress-right">
                                                <span class="progress-bar"></span>
                                            </span>
                                        <div class="progress-value">
                                            <div>45+</div>
                                        </div>
                                    </div>
                                </li>
                                <li>1</li>
                            </ul>
                        </div>
                        <div class="left-accordion-row pull-left">
                            <div class="pull-right logo-team-ponball">
                                <img src="/images/logo-team/manchester.png">
                            </div>
                            <div class="pull-right detail-team-ponball">
                                <div class="top-detail-team-ponball text-right">
                                    <h4>Manchester United</h4>
                                </div>
                                <div class="bottom-detail-team-ponball">
                                    <div class="score-ct">60%</div>
                                    <div class="progress full-width">
                                        <div class="progress-bar pull-right" role="progressbar"
                                             aria-valuenow="60"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <ul class="status-lastMatch full-width">
                                    <li><p>Last Match</p></li>
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                </ul>
                                <ul class="status-lastMatch full-width odd">
                                    <li><p>ODD</p></li>
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                </ul>
                            </div>

                            <div class="pull-left content-col-left">
                                <div class="ct-live">
                                    <span class="label-live">Live</span>
                                </div>
                                <div class="ct-live-chanel">
                                    <img src="/images/channel/pptv.png">
                                </div>
                            </div>
                        </div>
                        <div class="right-accordion-row pull-right">
                            <div class="pull-left logo-team-ponball">
                                <img src="/images/logo-team/Chelsea.png">
                            </div>
                            <div class="pull-left detail-team-ponball">
                                <div class="top-detail-team-ponball text-left">
                                    <h4>Chelsea</h4>
                                </div>
                                <div class="bottom-detail-team-ponball">
                                    <div class="progress full-width">
                                        <div class="progress-bar progress-bar-danger" role="progressbar"
                                             aria-valuenow="20"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 20%;">
                                        </div>
                                    </div>
                                    <div class="score-ct">20%</div>
                                </div>
                            </div>
                            <div class="pull-left">
                                <ul class="status-lastMatch full-width">
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                    <li><p>Last Match</p></li>
                                </ul>
                                <ul class="status-lastMatch full-width odd">
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                    <li><p>ODD</p></li>
                                </ul>
                            </div>
                            <div class="pull-right-section">
                                <div class="intro-game">
                                    <a href="/game.php" data-toggle="tooltip" data-placement="top"
                                       title="เล่นเกมทายผล">
                                        <i class="fa fa-futbol-o" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <a data-toggle="collapse" class="review-dropdown collapsed"
                                   data-parent="#accordion"
                                   href="#collapse_livematch">
                                    <h4>Live score</h4>
                                </a>
                            </div>

                        </div>
                        <div style="clear: both"></div>
                    </div>
                    <div id="collapse_livematch" class="panel-collapse collapse">
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-highlight" role="tablist">
                                <li class="active" role="presentation">
                                    <a href="#livescore-tab" role="tab"
                                       data-toggle="tab">Livescore</a></li>
                                <li role="presentation">
                                    <a href="#review" role="tab" data-toggle="tab">ทีเด็ด-ทรรศนะ</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="livescore-tab">
                                    <div class="col-md-4">

                                        <div class="thumbnail">

                                            <div class="header-event-match">
                                                <ul>
                                                    <li><h3>EVENT MATCH</h3></li>
                                                    <li>
                                                            <span class="real-time-match">
                                                               45+
                                                            </span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="score-event-match">

                                                <div class="pull-right">
                                                    <img src="/images/logo-team/manchester.png">
                                                    <p>Manchester United</p>

                                                </div>
                                                <h4>2-2</h4>
                                                <div class="pull-left text-right">
                                                    <p>Chelsea</p>
                                                    <img src="/images/logo-team/Chelsea.png">
                                                </div>

                                            </div>
                                            <div class="content-event-match">

                                                <!--                                        Home ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="red-card"></div>
                                                        </div>
                                                        <div class="min-live">90+4'</div>
                                                        <div class="name-live">Willian</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home ใบเหลือง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="yellow-card"></div>
                                                        </div>
                                                        <div class="min-live">87'</div>
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home ทำประตู-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="goal-score"></div>
                                                        </div>
                                                        <div class="min-live">71'</div>
                                                        <div class="score-live">2 - 4</div>
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home เปลี่ยนตัว-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="substitute-player"></div>
                                                        </div>
                                                        <div class="min-live">69'</div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-out">out</span>
                                                            David
                                                            Luiz
                                                        </div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-in">in</span>
                                                            Victor
                                                            Moses
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Home ใบเหลือง+ใบเหลือง=ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="group-yellow-card"></div>
                                                        </div>
                                                        <div class="min-live">55'</div>
                                                        <div class="name-live">Ross Barkley</div>
                                                    </div>
                                                </div>
                                                <!--Home จุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="penalty-score"></div>
                                                        </div>
                                                        <div class="min-live">45'</div>
                                                        <div class="name-live">Gary Cahill</div>
                                                    </div>
                                                </div>
                                                <!--Home พลาดจุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="penalty-none-score"></div>
                                                        </div>
                                                        <div class="min-live">41'</div>
                                                        <div class="name-live">Álvaro Morata</div>
                                                    </div>
                                                </div>
                                                <!--Home ทำเข้าประตูตัวเอง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="own-goal"></div>
                                                        </div>
                                                        <div class="min-live">28'</div>
                                                        <div class="name-live">Álvaro Morata</div>
                                                    </div>
                                                </div>


                                                <!--                                        Away ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Willian</div>
                                                        <div class="min-live">90+4'</div>
                                                        <div class="box-football-icon">
                                                            <div class="red-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away ใบเหลือง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                        <div class="min-live">87'</div>
                                                        <div class="box-football-icon">
                                                            <div class="yellow-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away ทำประตู-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                        <div class="score-live">2 - 4</div>
                                                        <div class="min-live">71'</div>
                                                        <div class="box-football-icon">
                                                            <div class="goal-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away เปลี่ยนตัว-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live"><span
                                                                    class="substitute-player-in">in</span>
                                                            Victor
                                                            Moses
                                                        </div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-out">out</span>
                                                            David
                                                            Luiz
                                                        </div>
                                                        <div class="min-live">69'</div>
                                                        <div class="box-football-icon">
                                                            <div class="substitute-player"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away ใบเหลือง+ใบเหลือง=ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Ross Barkley</div>
                                                        <div class="min-live">55'</div>
                                                        <div class="box-football-icon">
                                                            <div class="group-yellow-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away จุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Gary Cahill</div>
                                                        <div class="min-live">45'</div>
                                                        <div class="box-football-icon">
                                                            <div class="penalty-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away พลาดจุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Álvaro Morata</div>
                                                        <div class="min-live">41'</div>
                                                        <div class="box-football-icon">
                                                            <div class="penalty-none-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away ทำเข้าประตูตัวเอง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Álvaro Morata</div>
                                                        <div class="min-live">28'</div>
                                                        <div class="box-football-icon">
                                                            <div class="own-goal"></div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 content-status-match">
                                        <div class="thumbnail">

                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#statistics-tab"
                                                                                          role="tab"
                                                                                          data-toggle="tab">Statistics</a>
                                                </li>
                                                <li role="presentation"><a href="#lineUp-tab"
                                                                           role="tab"
                                                                           data-toggle="tab">Line
                                                        Up</a>
                                                </li>

                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active"
                                                     id="statistics-tab">
                                                    <div class="table statistics-table">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <div class="pull-right">
                                                                        <h4>Manchester United
                                                                        </h4>
                                                                        <img src="/images/logo-team/manchester.png">
                                                                    </div>
                                                                </td>
                                                                <td><h3>VS</h3></td>
                                                                <td>
                                                                    <div class="pull-left">
                                                                        <img src="/images/logo-team/Chelsea.png">
                                                                        <h4>Chelsea</h4>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ยิงประตู-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">15</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 15%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ยิงประตู</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="9" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 9%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">9</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ยิงตรงกรอบ-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">5</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 5%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ยิงตรงกรอบ</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="9" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 2%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">2</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ผิดกติกา-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">13</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 13%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ผิดกติกา</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="8" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 8%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">8</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เตะมุม-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">2</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="2"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 2%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เตะมุม</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">3</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ฟรีคิก-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">12</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="12"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 12%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ฟรีคิก</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="19"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 19%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">19</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ล้ำหน้า-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">6</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="6"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 6%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ล้ำหน้า</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="4" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 4%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">4</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                จำนวนใบเหลือง-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">3</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="3"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ล้ำหน้า</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">3</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เวลาครองบอล-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">55%</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="55"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 55%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เวลาครองบอล</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="45"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 45%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">45%</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เซฟได้-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">1</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="1"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 1%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เซฟได้</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="4" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 4%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">4</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เปลี่ยนตัว-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">3</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="3"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เปลี่ยนตัว</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">3</div>
                                                                </td>
                                                            </tr>


                                                        </table>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="lineUp-tab">
                                                    <div class="lineUp-table">
                                                        <table class="table">
                                                            <tr>
                                                                <td>
                                                                    <div class="pull-right full-width text-right">
                                                                        <span>Manchester United</span>
                                                                        <img src="/images/logo-team/manchester.png">
                                                                    </div>
                                                                    <div class="pull-right full-width">
                                                                        <div class="content-lineUp-home">
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="num-player">1
                                                                                    </div>
                                                                                    <div>David De Gea</div>
                                                                                    <div>GK</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">2
                                                                                    </div>
                                                                                    <div>Victor Lindelöf</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">6
                                                                                    </div>
                                                                                    <div>Paul Pogba</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">8
                                                                                    </div>
                                                                                    <div>Juan Mata</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">9
                                                                                    </div>
                                                                                    <div>Romelu Lukaku</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">10
                                                                                    </div>
                                                                                    <div>Marcus Rashford</div>
                                                                                    <div>FT</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">11
                                                                                    </div>
                                                                                    <div>Anthony Martial</div>
                                                                                    <div>RF</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">12
                                                                                    </div>
                                                                                    <div>Chris Smalling</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">15
                                                                                    </div>
                                                                                    <div>Andreas Pereira</div>
                                                                                    <div>MD</div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="pull-left">
                                                                        <img src="/images/logo-team/Chelsea.png">
                                                                        <span>Chelsea</span>
                                                                    </div>
                                                                    <div class="pull-left full-width">
                                                                        <div class="content-lineUp-away">
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="num-player">1
                                                                                    </div>
                                                                                    <div>David De Gea</div>
                                                                                    <div>GK</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">2
                                                                                    </div>
                                                                                    <div>Victor Lindelöf</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">6
                                                                                    </div>
                                                                                    <div>Paul Pogba</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">8
                                                                                    </div>
                                                                                    <div>Juan Mata</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">9
                                                                                    </div>
                                                                                    <div>Romelu Lukaku</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">10
                                                                                    </div>
                                                                                    <div>Marcus Rashford</div>
                                                                                    <div>FT</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">11
                                                                                    </div>
                                                                                    <div>Anthony Martial</div>
                                                                                    <div>RF</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">12
                                                                                    </div>
                                                                                    <div>Chris Smalling</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">15
                                                                                    </div>
                                                                                    <div>Andreas Pereira</div>
                                                                                    <div>MD</div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>


                                                        </table>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="statistics">
                                    <div class="table statistics-table">
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="pull-right">
                                                        <img src="/images/logo-team/manchester.png">
                                                    </div>
                                                </td>
                                                <td><h3>VS</h3></td>
                                                <td>
                                                    <div class="pull-left">
                                                        <img src="/images/logo-team/Chelsea.png">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>เริ่มเขี่ยบอล</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>เปลี่ยนตัวคนแรก</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="check-team"></span></td>
                                                <td>เริ่มเขี่ยบอล</td>
                                                <td></td>
                                            </tr>
                                            <!--                                                ยิงประตู-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">15</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 15%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ยิงประตู</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="9" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 9%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">9</div>
                                                </td>
                                            </tr>
                                            <!--                                                ยิงตรงกรอบ-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">5</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 5%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ยิงตรงกรอบ</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="9" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 2%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">2</div>
                                                </td>
                                            </tr>
                                            <!--                                                ผิดกติกา-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">13</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 13%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ผิดกติกา</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="8" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 8%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">8</div>
                                                </td>
                                            </tr>
                                            <!--                                                เตะมุม-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">2</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="2"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 2%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เตะมุม</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">3</div>
                                                </td>
                                            </tr>
                                            <!--                                                ฟรีคิก-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">12</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="12"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 12%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ฟรีคิก</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="19" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 19%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">19</div>
                                                </td>
                                            </tr>
                                            <!--                                                ล้ำหน้า-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">6</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="6"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 6%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ล้ำหน้า</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="4" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 4%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">4</div>
                                                </td>
                                            </tr>
                                            <!--                                                จำนวนใบเหลือง-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">3</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="3"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 3%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ล้ำหน้า</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">3</div>
                                                </td>
                                            </tr>
                                            <!--                                                เวลาครองบอล-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">55%</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="55"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 55%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เวลาครองบอล</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="45" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 45%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">45%</div>
                                                </td>
                                            </tr>
                                            <!--                                                เซฟได้-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">1</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="1"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 1%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เซฟได้</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="4" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 4%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">4</div>
                                                </td>
                                            </tr>
                                            <!--                                                เปลี่ยนตัว-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">3</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="3"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 3%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เปลี่ยนตัว</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">3</div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="check-team"></span></td>
                                                <td>เปลี่ยนตัวคนสุดท้าย</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ใบเหลืองใบสุดท้าย</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>


                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="lineUp">

                                </div>
                                <div role="tabpanel" class="tab-pane" id="review">
                                    <div class="home-panel-body pull-left">
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/profess-player/sample-logo01.png">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>LOMTOE.NET</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png"</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/profess-player/sample-logo05.png">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>SMM SPORT</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png"</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    อาจจะฝืดๆหน่อย​ แต่ต้องสอย​ อย่าให้เสียชื่อทีมใหญ่
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/9.jpg">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>หมูตัวนี้ตัวใหญ่</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png">
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    ชอบความคมของตรานกแก้ว ซวยแล้วบายาโดลิด
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="away-panel-body pull-right">
                                        <div class="content-away-review full-width pull-left">
                                            <div class="pull-left">
                                                <div class="area-logoName area-logoName-away">
                                                    <div class="pull-left logo-review-user">
                                                        <img src="/images/profess-player/sample-logo12.png">
                                                    </div>
                                                    <div class="pull-left">
                                                        <h4>GOAL.IN.TH</h4>
                                                        <ul>
                                                            <li><img src="/images/logo-team/Chelsea.png"</li>
                                                            <li>@Chelsea</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-left text-review-content text-left">
                                                <p>
                                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                                </p>
                                            </div>
                                            <div class="pull-right ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="full-width text-center">
                                        <a class="btn btn-default btn-sm" href="#"
                                           role="button">อ่านทีเด็ดทรรศนะทั้งหมด</a>
                                    </div>
                                </div>
                                <div style="clear: both"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-program-soccer container-live_all">
        <div class="topic">
            <div class="logo-league-program">
                <img src="/images/five-leagues/seriea1.png">
            </div>
            <div class="title-league-program">
                <h3>กัลโช่ เซเรียอา อิตาลี (ITALY SERIE A)</h3>
                <h5>บ้านผลบอล โปรแกรมบอล ราคาบอล </h5>
            </div>
        </div>


        <div class="content">
            <div class="panel-group panel-group-row" id="accordion">
                <!--บอลสด-->
                <div class="panel panel-default livescore-panel livescore-live">
                    <div class="panel-heading">
                        <div class="hdp-float live-float">
                            <ul>
                                <li>2</li>
                                <li>
                                    <div class="progress" data-percentage="40">
				                            <span class="progress-left">
                                                <span class="progress-bar"></span>
				                            </span>
                                        <span class="progress-right">
                                                <span class="progress-bar"></span>
                                            </span>
                                        <div class="progress-value">
                                            <div>45+</div>
                                        </div>
                                    </div>
                                </li>
                                <li>1</li>
                            </ul>
                        </div>
                        <div class="left-accordion-row pull-left">
                            <div class="pull-right logo-team-ponball">
                                <img src="/images/logo-team/manchester.png">
                            </div>
                            <div class="pull-right detail-team-ponball">
                                <div class="top-detail-team-ponball text-right">
                                    <h4>Manchester United</h4>
                                </div>
                                <div class="bottom-detail-team-ponball">
                                    <div class="score-ct">60%</div>
                                    <div class="progress full-width">
                                        <div class="progress-bar pull-right" role="progressbar"
                                             aria-valuenow="60"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <ul class="status-lastMatch full-width">
                                    <li><p>Last Match</p></li>
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                </ul>
                                <ul class="status-lastMatch full-width odd">
                                    <li><p>ODD</p></li>
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                </ul>
                            </div>

                            <div class="pull-left content-col-left">
                                <div class="ct-live">
                                    <span class="label-live">Live</span>
                                </div>
                                <div class="ct-live-chanel">
                                    <img src="/images/channel/pptv.png">
                                </div>
                            </div>
                        </div>
                        <div class="right-accordion-row pull-right">
                            <div class="pull-left logo-team-ponball">
                                <img src="/images/logo-team/Chelsea.png">
                            </div>
                            <div class="pull-left detail-team-ponball">
                                <div class="top-detail-team-ponball text-left">
                                    <h4>Chelsea</h4>
                                </div>
                                <div class="bottom-detail-team-ponball">
                                    <div class="progress full-width">
                                        <div class="progress-bar progress-bar-danger" role="progressbar"
                                             aria-valuenow="20"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 20%;">
                                        </div>
                                    </div>
                                    <div class="score-ct">20%</div>
                                </div>
                            </div>
                            <div class="pull-left">
                                <ul class="status-lastMatch full-width">
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                    <li><p>Last Match</p></li>
                                </ul>
                                <ul class="status-lastMatch full-width odd">
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                    <li><p>ODD</p></li>
                                </ul>
                            </div>
                            <div class="pull-right-section">
                                <div class="intro-game">
                                    <a href="/game.php" data-toggle="tooltip" data-placement="top"
                                       title="เล่นเกมทายผล">
                                        <i class="fa fa-futbol-o" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <a data-toggle="collapse" class="review-dropdown collapsed"
                                   data-parent="#accordion"
                                   href="#collapse_livematch">
                                    <h4>Live score</h4>
                                </a>
                            </div>

                        </div>
                        <div style="clear: both"></div>
                    </div>
                    <div id="collapse_livematch" class="panel-collapse collapse">
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-highlight" role="tablist">
                                <li class="active" role="presentation">
                                    <a href="#livescore-tab" role="tab"
                                       data-toggle="tab">Livescore</a></li>
                                <li role="presentation">
                                    <a href="#review" role="tab" data-toggle="tab">ทีเด็ด-ทรรศนะ</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="livescore-tab">
                                    <div class="col-md-4">

                                        <div class="thumbnail">

                                            <div class="header-event-match">
                                                <ul>
                                                    <li><h3>EVENT MATCH</h3></li>
                                                    <li>
                                                            <span class="real-time-match">
                                                               45+
                                                            </span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="score-event-match">

                                                <div class="pull-right">
                                                    <img src="/images/logo-team/manchester.png">
                                                    <p>Manchester United</p>

                                                </div>
                                                <h4>2-2</h4>
                                                <div class="pull-left text-right">
                                                    <p>Chelsea</p>
                                                    <img src="/images/logo-team/Chelsea.png">
                                                </div>

                                            </div>
                                            <div class="content-event-match">

                                                <!--                                        Home ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="red-card"></div>
                                                        </div>
                                                        <div class="min-live">90+4'</div>
                                                        <div class="name-live">Willian</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home ใบเหลือง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="yellow-card"></div>
                                                        </div>
                                                        <div class="min-live">87'</div>
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home ทำประตู-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="goal-score"></div>
                                                        </div>
                                                        <div class="min-live">71'</div>
                                                        <div class="score-live">2 - 4</div>
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home เปลี่ยนตัว-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="substitute-player"></div>
                                                        </div>
                                                        <div class="min-live">69'</div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-out">out</span>
                                                            David
                                                            Luiz
                                                        </div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-in">in</span>
                                                            Victor
                                                            Moses
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Home ใบเหลือง+ใบเหลือง=ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="group-yellow-card"></div>
                                                        </div>
                                                        <div class="min-live">55'</div>
                                                        <div class="name-live">Ross Barkley</div>
                                                    </div>
                                                </div>
                                                <!--Home จุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="penalty-score"></div>
                                                        </div>
                                                        <div class="min-live">45'</div>
                                                        <div class="name-live">Gary Cahill</div>
                                                    </div>
                                                </div>
                                                <!--Home พลาดจุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="penalty-none-score"></div>
                                                        </div>
                                                        <div class="min-live">41'</div>
                                                        <div class="name-live">Álvaro Morata</div>
                                                    </div>
                                                </div>
                                                <!--Home ทำเข้าประตูตัวเอง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="own-goal"></div>
                                                        </div>
                                                        <div class="min-live">28'</div>
                                                        <div class="name-live">Álvaro Morata</div>
                                                    </div>
                                                </div>


                                                <!--                                        Away ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Willian</div>
                                                        <div class="min-live">90+4'</div>
                                                        <div class="box-football-icon">
                                                            <div class="red-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away ใบเหลือง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                        <div class="min-live">87'</div>
                                                        <div class="box-football-icon">
                                                            <div class="yellow-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away ทำประตู-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                        <div class="score-live">2 - 4</div>
                                                        <div class="min-live">71'</div>
                                                        <div class="box-football-icon">
                                                            <div class="goal-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away เปลี่ยนตัว-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live"><span
                                                                    class="substitute-player-in">in</span>
                                                            Victor
                                                            Moses
                                                        </div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-out">out</span>
                                                            David
                                                            Luiz
                                                        </div>
                                                        <div class="min-live">69'</div>
                                                        <div class="box-football-icon">
                                                            <div class="substitute-player"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away ใบเหลือง+ใบเหลือง=ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Ross Barkley</div>
                                                        <div class="min-live">55'</div>
                                                        <div class="box-football-icon">
                                                            <div class="group-yellow-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away จุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Gary Cahill</div>
                                                        <div class="min-live">45'</div>
                                                        <div class="box-football-icon">
                                                            <div class="penalty-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away พลาดจุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Álvaro Morata</div>
                                                        <div class="min-live">41'</div>
                                                        <div class="box-football-icon">
                                                            <div class="penalty-none-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away ทำเข้าประตูตัวเอง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Álvaro Morata</div>
                                                        <div class="min-live">28'</div>
                                                        <div class="box-football-icon">
                                                            <div class="own-goal"></div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 content-status-match">
                                        <div class="thumbnail">

                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#statistics-tab"
                                                                                          role="tab"
                                                                                          data-toggle="tab">Statistics</a>
                                                </li>
                                                <li role="presentation"><a href="#lineUp-tab"
                                                                           role="tab"
                                                                           data-toggle="tab">Line
                                                        Up</a>
                                                </li>

                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active"
                                                     id="statistics-tab">
                                                    <div class="table statistics-table">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <div class="pull-right">
                                                                        <h4>Manchester United
                                                                        </h4>
                                                                        <img src="/images/logo-team/manchester.png">
                                                                    </div>
                                                                </td>
                                                                <td><h3>VS</h3></td>
                                                                <td>
                                                                    <div class="pull-left">
                                                                        <img src="/images/logo-team/Chelsea.png">
                                                                        <h4>Chelsea</h4>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ยิงประตู-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">15</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 15%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ยิงประตู</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="9" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 9%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">9</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ยิงตรงกรอบ-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">5</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 5%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ยิงตรงกรอบ</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="9" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 2%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">2</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ผิดกติกา-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">13</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 13%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ผิดกติกา</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="8" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 8%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">8</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เตะมุม-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">2</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="2"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 2%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เตะมุม</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">3</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ฟรีคิก-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">12</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="12"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 12%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ฟรีคิก</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="19"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 19%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">19</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ล้ำหน้า-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">6</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="6"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 6%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ล้ำหน้า</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="4" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 4%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">4</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                จำนวนใบเหลือง-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">3</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="3"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ล้ำหน้า</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">3</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เวลาครองบอล-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">55%</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="55"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 55%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เวลาครองบอล</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="45"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 45%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">45%</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เซฟได้-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">1</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="1"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 1%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เซฟได้</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="4" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 4%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">4</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เปลี่ยนตัว-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">3</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="3"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เปลี่ยนตัว</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">3</div>
                                                                </td>
                                                            </tr>


                                                        </table>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="lineUp-tab">
                                                    <div class="lineUp-table">
                                                        <table class="table">
                                                            <tr>
                                                                <td>
                                                                    <div class="pull-right full-width text-right">
                                                                        <span>Manchester United</span>
                                                                        <img src="/images/logo-team/manchester.png">
                                                                    </div>
                                                                    <div class="pull-right full-width">
                                                                        <div class="content-lineUp-home">
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="num-player">1
                                                                                    </div>
                                                                                    <div>David De Gea</div>
                                                                                    <div>GK</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">2
                                                                                    </div>
                                                                                    <div>Victor Lindelöf</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">6
                                                                                    </div>
                                                                                    <div>Paul Pogba</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">8
                                                                                    </div>
                                                                                    <div>Juan Mata</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">9
                                                                                    </div>
                                                                                    <div>Romelu Lukaku</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">10
                                                                                    </div>
                                                                                    <div>Marcus Rashford</div>
                                                                                    <div>FT</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">11
                                                                                    </div>
                                                                                    <div>Anthony Martial</div>
                                                                                    <div>RF</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">12
                                                                                    </div>
                                                                                    <div>Chris Smalling</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">15
                                                                                    </div>
                                                                                    <div>Andreas Pereira</div>
                                                                                    <div>MD</div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="pull-left">
                                                                        <img src="/images/logo-team/Chelsea.png">
                                                                        <span>Chelsea</span>
                                                                    </div>
                                                                    <div class="pull-left full-width">
                                                                        <div class="content-lineUp-away">
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="num-player">1
                                                                                    </div>
                                                                                    <div>David De Gea</div>
                                                                                    <div>GK</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">2
                                                                                    </div>
                                                                                    <div>Victor Lindelöf</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">6
                                                                                    </div>
                                                                                    <div>Paul Pogba</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">8
                                                                                    </div>
                                                                                    <div>Juan Mata</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">9
                                                                                    </div>
                                                                                    <div>Romelu Lukaku</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">10
                                                                                    </div>
                                                                                    <div>Marcus Rashford</div>
                                                                                    <div>FT</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">11
                                                                                    </div>
                                                                                    <div>Anthony Martial</div>
                                                                                    <div>RF</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">12
                                                                                    </div>
                                                                                    <div>Chris Smalling</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">15
                                                                                    </div>
                                                                                    <div>Andreas Pereira</div>
                                                                                    <div>MD</div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>


                                                        </table>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="statistics">
                                    <div class="table statistics-table">
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="pull-right">
                                                        <img src="/images/logo-team/manchester.png">
                                                    </div>
                                                </td>
                                                <td><h3>VS</h3></td>
                                                <td>
                                                    <div class="pull-left">
                                                        <img src="/images/logo-team/Chelsea.png">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>เริ่มเขี่ยบอล</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>เปลี่ยนตัวคนแรก</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="check-team"></span></td>
                                                <td>เริ่มเขี่ยบอล</td>
                                                <td></td>
                                            </tr>
                                            <!--                                                ยิงประตู-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">15</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 15%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ยิงประตู</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="9" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 9%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">9</div>
                                                </td>
                                            </tr>
                                            <!--                                                ยิงตรงกรอบ-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">5</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 5%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ยิงตรงกรอบ</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="9" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 2%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">2</div>
                                                </td>
                                            </tr>
                                            <!--                                                ผิดกติกา-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">13</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 13%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ผิดกติกา</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="8" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 8%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">8</div>
                                                </td>
                                            </tr>
                                            <!--                                                เตะมุม-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">2</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="2"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 2%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เตะมุม</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">3</div>
                                                </td>
                                            </tr>
                                            <!--                                                ฟรีคิก-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">12</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="12"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 12%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ฟรีคิก</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="19" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 19%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">19</div>
                                                </td>
                                            </tr>
                                            <!--                                                ล้ำหน้า-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">6</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="6"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 6%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ล้ำหน้า</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="4" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 4%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">4</div>
                                                </td>
                                            </tr>
                                            <!--                                                จำนวนใบเหลือง-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">3</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="3"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 3%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ล้ำหน้า</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">3</div>
                                                </td>
                                            </tr>
                                            <!--                                                เวลาครองบอล-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">55%</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="55"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 55%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เวลาครองบอล</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="45" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 45%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">45%</div>
                                                </td>
                                            </tr>
                                            <!--                                                เซฟได้-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">1</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="1"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 1%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เซฟได้</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="4" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 4%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">4</div>
                                                </td>
                                            </tr>
                                            <!--                                                เปลี่ยนตัว-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">3</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="3"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 3%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เปลี่ยนตัว</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">3</div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="check-team"></span></td>
                                                <td>เปลี่ยนตัวคนสุดท้าย</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ใบเหลืองใบสุดท้าย</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>


                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="lineUp">

                                </div>
                                <div role="tabpanel" class="tab-pane" id="review">
                                    <div class="home-panel-body pull-left">
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/profess-player/sample-logo01.png">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>LOMTOE.NET</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png"</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/profess-player/sample-logo05.png">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>SMM SPORT</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png"</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    อาจจะฝืดๆหน่อย​ แต่ต้องสอย​ อย่าให้เสียชื่อทีมใหญ่
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/9.jpg">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>หมูตัวนี้ตัวใหญ่</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png">
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    ชอบความคมของตรานกแก้ว ซวยแล้วบายาโดลิด
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="away-panel-body pull-right">
                                        <div class="content-away-review full-width pull-left">
                                            <div class="pull-left">
                                                <div class="area-logoName area-logoName-away">
                                                    <div class="pull-left logo-review-user">
                                                        <img src="/images/profess-player/sample-logo12.png">
                                                    </div>
                                                    <div class="pull-left">
                                                        <h4>GOAL.IN.TH</h4>
                                                        <ul>
                                                            <li><img src="/images/logo-team/Chelsea.png"</li>
                                                            <li>@Chelsea</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-left text-review-content text-left">
                                                <p>
                                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                                </p>
                                            </div>
                                            <div class="pull-right ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="full-width text-center">
                                        <a class="btn btn-default btn-sm" href="#"
                                           role="button">อ่านทีเด็ดทรรศนะทั้งหมด</a>
                                    </div>
                                </div>
                                <div style="clear: both"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-program-soccer container-live_all">
        <div class="topic">
            <div class="logo-league-program">
                <img src="/images/five-leagues/seriea1.png">
            </div>
            <div class="title-league-program">
                <h3>กัลโช่ เซเรียอา อิตาลี (ITALY SERIE A)</h3>
                <h5>บ้านผลบอล โปรแกรมบอล ราคาบอล </h5>
            </div>
        </div>


        <div class="content">
            <div class="panel-group panel-group-row" id="accordion">
                <!--บอลสด-->
                <div class="panel panel-default livescore-panel livescore-live">
                    <div class="panel-heading">
                        <div class="hdp-float live-float">
                            <ul>
                                <li>2</li>
                                <li>
                                    <div class="progress" data-percentage="40">
				                            <span class="progress-left">
                                                <span class="progress-bar"></span>
				                            </span>
                                        <span class="progress-right">
                                                <span class="progress-bar"></span>
                                            </span>
                                        <div class="progress-value">
                                            <div>45+</div>
                                        </div>
                                    </div>
                                </li>
                                <li>1</li>
                            </ul>
                        </div>
                        <div class="left-accordion-row pull-left">
                            <div class="pull-right logo-team-ponball">
                                <img src="/images/logo-team/manchester.png">
                            </div>
                            <div class="pull-right detail-team-ponball">
                                <div class="top-detail-team-ponball text-right">
                                    <h4>Manchester United</h4>
                                </div>
                                <div class="bottom-detail-team-ponball">
                                    <div class="score-ct">60%</div>
                                    <div class="progress full-width">
                                        <div class="progress-bar pull-right" role="progressbar"
                                             aria-valuenow="60"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <ul class="status-lastMatch full-width">
                                    <li><p>Last Match</p></li>
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                </ul>
                                <ul class="status-lastMatch full-width odd">
                                    <li><p>ODD</p></li>
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                </ul>
                            </div>

                            <div class="pull-left content-col-left">
                                <div class="ct-live">
                                    <span class="label-live">Live</span>
                                </div>
                                <div class="ct-live-chanel">
                                    <img src="/images/channel/pptv.png">
                                </div>
                            </div>
                        </div>
                        <div class="right-accordion-row pull-right">
                            <div class="pull-left logo-team-ponball">
                                <img src="/images/logo-team/Chelsea.png">
                            </div>
                            <div class="pull-left detail-team-ponball">
                                <div class="top-detail-team-ponball text-left">
                                    <h4>Chelsea</h4>
                                </div>
                                <div class="bottom-detail-team-ponball">
                                    <div class="progress full-width">
                                        <div class="progress-bar progress-bar-danger" role="progressbar"
                                             aria-valuenow="20"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 20%;">
                                        </div>
                                    </div>
                                    <div class="score-ct">20%</div>
                                </div>
                            </div>
                            <div class="pull-left">
                                <ul class="status-lastMatch full-width">
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                    <li><p>Last Match</p></li>
                                </ul>
                                <ul class="status-lastMatch full-width odd">
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                    <li><p>ODD</p></li>
                                </ul>
                            </div>
                            <div class="pull-right-section">
                                <div class="intro-game">
                                    <a href="/game.php" data-toggle="tooltip" data-placement="top"
                                       title="เล่นเกมทายผล">
                                        <i class="fa fa-futbol-o" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <a data-toggle="collapse" class="review-dropdown collapsed"
                                   data-parent="#accordion"
                                   href="#collapse_livematch">
                                    <h4>Live score</h4>
                                </a>
                            </div>

                        </div>
                        <div style="clear: both"></div>
                    </div>
                    <div id="collapse_livematch" class="panel-collapse collapse">
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-highlight" role="tablist">
                                <li class="active" role="presentation">
                                    <a href="#livescore-tab" role="tab"
                                       data-toggle="tab">Livescore</a></li>
                                <li role="presentation">
                                    <a href="#review" role="tab" data-toggle="tab">ทีเด็ด-ทรรศนะ</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="livescore-tab">
                                    <div class="col-md-4">

                                        <div class="thumbnail">

                                            <div class="header-event-match">
                                                <ul>
                                                    <li><h3>EVENT MATCH</h3></li>
                                                    <li>
                                                            <span class="real-time-match">
                                                               45+
                                                            </span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="score-event-match">

                                                <div class="pull-right">
                                                    <img src="/images/logo-team/manchester.png">
                                                    <p>Manchester United</p>

                                                </div>
                                                <h4>2-2</h4>
                                                <div class="pull-left text-right">
                                                    <p>Chelsea</p>
                                                    <img src="/images/logo-team/Chelsea.png">
                                                </div>

                                            </div>
                                            <div class="content-event-match">

                                                <!--                                        Home ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="red-card"></div>
                                                        </div>
                                                        <div class="min-live">90+4'</div>
                                                        <div class="name-live">Willian</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home ใบเหลือง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="yellow-card"></div>
                                                        </div>
                                                        <div class="min-live">87'</div>
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home ทำประตู-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="goal-score"></div>
                                                        </div>
                                                        <div class="min-live">71'</div>
                                                        <div class="score-live">2 - 4</div>
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home เปลี่ยนตัว-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="substitute-player"></div>
                                                        </div>
                                                        <div class="min-live">69'</div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-out">out</span>
                                                            David
                                                            Luiz
                                                        </div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-in">in</span>
                                                            Victor
                                                            Moses
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Home ใบเหลือง+ใบเหลือง=ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="group-yellow-card"></div>
                                                        </div>
                                                        <div class="min-live">55'</div>
                                                        <div class="name-live">Ross Barkley</div>
                                                    </div>
                                                </div>
                                                <!--Home จุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="penalty-score"></div>
                                                        </div>
                                                        <div class="min-live">45'</div>
                                                        <div class="name-live">Gary Cahill</div>
                                                    </div>
                                                </div>
                                                <!--Home พลาดจุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="penalty-none-score"></div>
                                                        </div>
                                                        <div class="min-live">41'</div>
                                                        <div class="name-live">Álvaro Morata</div>
                                                    </div>
                                                </div>
                                                <!--Home ทำเข้าประตูตัวเอง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="own-goal"></div>
                                                        </div>
                                                        <div class="min-live">28'</div>
                                                        <div class="name-live">Álvaro Morata</div>
                                                    </div>
                                                </div>


                                                <!--                                        Away ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Willian</div>
                                                        <div class="min-live">90+4'</div>
                                                        <div class="box-football-icon">
                                                            <div class="red-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away ใบเหลือง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                        <div class="min-live">87'</div>
                                                        <div class="box-football-icon">
                                                            <div class="yellow-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away ทำประตู-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                        <div class="score-live">2 - 4</div>
                                                        <div class="min-live">71'</div>
                                                        <div class="box-football-icon">
                                                            <div class="goal-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away เปลี่ยนตัว-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live"><span
                                                                    class="substitute-player-in">in</span>
                                                            Victor
                                                            Moses
                                                        </div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-out">out</span>
                                                            David
                                                            Luiz
                                                        </div>
                                                        <div class="min-live">69'</div>
                                                        <div class="box-football-icon">
                                                            <div class="substitute-player"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away ใบเหลือง+ใบเหลือง=ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Ross Barkley</div>
                                                        <div class="min-live">55'</div>
                                                        <div class="box-football-icon">
                                                            <div class="group-yellow-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away จุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Gary Cahill</div>
                                                        <div class="min-live">45'</div>
                                                        <div class="box-football-icon">
                                                            <div class="penalty-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away พลาดจุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Álvaro Morata</div>
                                                        <div class="min-live">41'</div>
                                                        <div class="box-football-icon">
                                                            <div class="penalty-none-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away ทำเข้าประตูตัวเอง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Álvaro Morata</div>
                                                        <div class="min-live">28'</div>
                                                        <div class="box-football-icon">
                                                            <div class="own-goal"></div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 content-status-match">
                                        <div class="thumbnail">

                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#statistics-tab"
                                                                                          role="tab"
                                                                                          data-toggle="tab">Statistics</a>
                                                </li>
                                                <li role="presentation"><a href="#lineUp-tab"
                                                                           role="tab"
                                                                           data-toggle="tab">Line
                                                        Up</a>
                                                </li>

                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active"
                                                     id="statistics-tab">
                                                    <div class="table statistics-table">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <div class="pull-right">
                                                                        <h4>Manchester United
                                                                        </h4>
                                                                        <img src="/images/logo-team/manchester.png">
                                                                    </div>
                                                                </td>
                                                                <td><h3>VS</h3></td>
                                                                <td>
                                                                    <div class="pull-left">
                                                                        <img src="/images/logo-team/Chelsea.png">
                                                                        <h4>Chelsea</h4>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ยิงประตู-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">15</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 15%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ยิงประตู</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="9" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 9%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">9</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ยิงตรงกรอบ-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">5</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 5%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ยิงตรงกรอบ</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="9" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 2%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">2</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ผิดกติกา-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">13</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 13%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ผิดกติกา</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="8" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 8%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">8</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เตะมุม-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">2</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="2"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 2%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เตะมุม</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">3</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ฟรีคิก-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">12</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="12"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 12%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ฟรีคิก</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="19"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 19%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">19</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ล้ำหน้า-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">6</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="6"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 6%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ล้ำหน้า</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="4" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 4%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">4</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                จำนวนใบเหลือง-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">3</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="3"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ล้ำหน้า</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">3</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เวลาครองบอล-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">55%</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="55"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 55%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เวลาครองบอล</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="45"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 45%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">45%</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เซฟได้-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">1</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="1"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 1%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เซฟได้</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="4" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 4%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">4</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เปลี่ยนตัว-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">3</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="3"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เปลี่ยนตัว</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">3</div>
                                                                </td>
                                                            </tr>


                                                        </table>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="lineUp-tab">
                                                    <div class="lineUp-table">
                                                        <table class="table">
                                                            <tr>
                                                                <td>
                                                                    <div class="pull-right full-width text-right">
                                                                        <span>Manchester United</span>
                                                                        <img src="/images/logo-team/manchester.png">
                                                                    </div>
                                                                    <div class="pull-right full-width">
                                                                        <div class="content-lineUp-home">
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="num-player">1
                                                                                    </div>
                                                                                    <div>David De Gea</div>
                                                                                    <div>GK</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">2
                                                                                    </div>
                                                                                    <div>Victor Lindelöf</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">6
                                                                                    </div>
                                                                                    <div>Paul Pogba</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">8
                                                                                    </div>
                                                                                    <div>Juan Mata</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">9
                                                                                    </div>
                                                                                    <div>Romelu Lukaku</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">10
                                                                                    </div>
                                                                                    <div>Marcus Rashford</div>
                                                                                    <div>FT</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">11
                                                                                    </div>
                                                                                    <div>Anthony Martial</div>
                                                                                    <div>RF</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">12
                                                                                    </div>
                                                                                    <div>Chris Smalling</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">15
                                                                                    </div>
                                                                                    <div>Andreas Pereira</div>
                                                                                    <div>MD</div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="pull-left">
                                                                        <img src="/images/logo-team/Chelsea.png">
                                                                        <span>Chelsea</span>
                                                                    </div>
                                                                    <div class="pull-left full-width">
                                                                        <div class="content-lineUp-away">
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="num-player">1
                                                                                    </div>
                                                                                    <div>David De Gea</div>
                                                                                    <div>GK</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">2
                                                                                    </div>
                                                                                    <div>Victor Lindelöf</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">6
                                                                                    </div>
                                                                                    <div>Paul Pogba</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">8
                                                                                    </div>
                                                                                    <div>Juan Mata</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">9
                                                                                    </div>
                                                                                    <div>Romelu Lukaku</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">10
                                                                                    </div>
                                                                                    <div>Marcus Rashford</div>
                                                                                    <div>FT</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">11
                                                                                    </div>
                                                                                    <div>Anthony Martial</div>
                                                                                    <div>RF</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">12
                                                                                    </div>
                                                                                    <div>Chris Smalling</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">15
                                                                                    </div>
                                                                                    <div>Andreas Pereira</div>
                                                                                    <div>MD</div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>


                                                        </table>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="statistics">
                                    <div class="table statistics-table">
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="pull-right">
                                                        <img src="/images/logo-team/manchester.png">
                                                    </div>
                                                </td>
                                                <td><h3>VS</h3></td>
                                                <td>
                                                    <div class="pull-left">
                                                        <img src="/images/logo-team/Chelsea.png">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>เริ่มเขี่ยบอล</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>เปลี่ยนตัวคนแรก</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="check-team"></span></td>
                                                <td>เริ่มเขี่ยบอล</td>
                                                <td></td>
                                            </tr>
                                            <!--                                                ยิงประตู-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">15</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 15%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ยิงประตู</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="9" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 9%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">9</div>
                                                </td>
                                            </tr>
                                            <!--                                                ยิงตรงกรอบ-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">5</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 5%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ยิงตรงกรอบ</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="9" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 2%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">2</div>
                                                </td>
                                            </tr>
                                            <!--                                                ผิดกติกา-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">13</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 13%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ผิดกติกา</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="8" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 8%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">8</div>
                                                </td>
                                            </tr>
                                            <!--                                                เตะมุม-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">2</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="2"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 2%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เตะมุม</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">3</div>
                                                </td>
                                            </tr>
                                            <!--                                                ฟรีคิก-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">12</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="12"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 12%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ฟรีคิก</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="19" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 19%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">19</div>
                                                </td>
                                            </tr>
                                            <!--                                                ล้ำหน้า-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">6</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="6"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 6%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ล้ำหน้า</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="4" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 4%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">4</div>
                                                </td>
                                            </tr>
                                            <!--                                                จำนวนใบเหลือง-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">3</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="3"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 3%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ล้ำหน้า</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">3</div>
                                                </td>
                                            </tr>
                                            <!--                                                เวลาครองบอล-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">55%</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="55"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 55%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เวลาครองบอล</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="45" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 45%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">45%</div>
                                                </td>
                                            </tr>
                                            <!--                                                เซฟได้-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">1</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="1"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 1%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เซฟได้</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="4" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 4%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">4</div>
                                                </td>
                                            </tr>
                                            <!--                                                เปลี่ยนตัว-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">3</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="3"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 3%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เปลี่ยนตัว</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">3</div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="check-team"></span></td>
                                                <td>เปลี่ยนตัวคนสุดท้าย</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ใบเหลืองใบสุดท้าย</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>


                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="lineUp">

                                </div>
                                <div role="tabpanel" class="tab-pane" id="review">
                                    <div class="home-panel-body pull-left">
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/profess-player/sample-logo01.png">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>LOMTOE.NET</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png"</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/profess-player/sample-logo05.png">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>SMM SPORT</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png"</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    อาจจะฝืดๆหน่อย​ แต่ต้องสอย​ อย่าให้เสียชื่อทีมใหญ่
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/9.jpg">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>หมูตัวนี้ตัวใหญ่</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png">
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    ชอบความคมของตรานกแก้ว ซวยแล้วบายาโดลิด
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="away-panel-body pull-right">
                                        <div class="content-away-review full-width pull-left">
                                            <div class="pull-left">
                                                <div class="area-logoName area-logoName-away">
                                                    <div class="pull-left logo-review-user">
                                                        <img src="/images/profess-player/sample-logo12.png">
                                                    </div>
                                                    <div class="pull-left">
                                                        <h4>GOAL.IN.TH</h4>
                                                        <ul>
                                                            <li><img src="/images/logo-team/Chelsea.png"</li>
                                                            <li>@Chelsea</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-left text-review-content text-left">
                                                <p>
                                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                                </p>
                                            </div>
                                            <div class="pull-right ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="full-width text-center">
                                        <a class="btn btn-default btn-sm" href="#"
                                           role="button">อ่านทีเด็ดทรรศนะทั้งหมด</a>
                                    </div>
                                </div>
                                <div style="clear: both"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default livescore-panel livescore-live">
                    <div class="panel-heading">
                        <div class="hdp-float live-float">
                            <ul>
                                <li>2</li>
                                <li>
                                    <div class="progress" data-percentage="40">
				                            <span class="progress-left">
                                                <span class="progress-bar"></span>
				                            </span>
                                        <span class="progress-right">
                                                <span class="progress-bar"></span>
                                            </span>
                                        <div class="progress-value">
                                            <div>45+</div>
                                        </div>
                                    </div>
                                </li>
                                <li>1</li>
                            </ul>
                        </div>
                        <div class="left-accordion-row pull-left">
                            <div class="pull-right logo-team-ponball">
                                <img src="/images/logo-team/manchester.png">
                            </div>
                            <div class="pull-right detail-team-ponball">
                                <div class="top-detail-team-ponball text-right">
                                    <h4>Manchester United</h4>
                                </div>
                                <div class="bottom-detail-team-ponball">
                                    <div class="score-ct">60%</div>
                                    <div class="progress full-width">
                                        <div class="progress-bar pull-right" role="progressbar"
                                             aria-valuenow="60"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <ul class="status-lastMatch full-width">
                                    <li><p>Last Match</p></li>
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                </ul>
                                <ul class="status-lastMatch full-width odd">
                                    <li><p>ODD</p></li>
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                </ul>
                            </div>

                            <div class="pull-left content-col-left">
                                <div class="ct-live">
                                    <span class="label-live">Live</span>
                                </div>
                                <div class="ct-live-chanel">
                                    <img src="/images/channel/pptv.png">
                                </div>
                            </div>
                        </div>
                        <div class="right-accordion-row pull-right">
                            <div class="pull-left logo-team-ponball">
                                <img src="/images/logo-team/Chelsea.png">
                            </div>
                            <div class="pull-left detail-team-ponball">
                                <div class="top-detail-team-ponball text-left">
                                    <h4>Chelsea</h4>
                                </div>
                                <div class="bottom-detail-team-ponball">
                                    <div class="progress full-width">
                                        <div class="progress-bar progress-bar-danger" role="progressbar"
                                             aria-valuenow="20"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 20%;">
                                        </div>
                                    </div>
                                    <div class="score-ct">20%</div>
                                </div>
                            </div>
                            <div class="pull-left">
                                <ul class="status-lastMatch full-width">
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                    <li><p>Last Match</p></li>
                                </ul>
                                <ul class="status-lastMatch full-width odd">
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                    <li><p>ODD</p></li>
                                </ul>
                            </div>
                            <div class="pull-right-section">
                                <div class="intro-game">
                                    <a href="/game.php" data-toggle="tooltip" data-placement="top"
                                       title="เล่นเกมทายผล">
                                        <i class="fa fa-futbol-o" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <a data-toggle="collapse" class="review-dropdown collapsed"
                                   data-parent="#accordion"
                                   href="#collapse_livematch">
                                    <h4>Live score</h4>
                                </a>
                            </div>

                        </div>
                        <div style="clear: both"></div>
                    </div>
                    <div id="collapse_livematch" class="panel-collapse collapse">
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-highlight" role="tablist">
                                <li class="active" role="presentation">
                                    <a href="#livescore-tab" role="tab"
                                       data-toggle="tab">Livescore</a></li>
                                <li role="presentation">
                                    <a href="#review" role="tab" data-toggle="tab">ทีเด็ด-ทรรศนะ</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="livescore-tab">
                                    <div class="col-md-4">

                                        <div class="thumbnail">

                                            <div class="header-event-match">
                                                <ul>
                                                    <li><h3>EVENT MATCH</h3></li>
                                                    <li>
                                                            <span class="real-time-match">
                                                               45+
                                                            </span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="score-event-match">

                                                <div class="pull-right">
                                                    <img src="/images/logo-team/manchester.png">
                                                    <p>Manchester United</p>

                                                </div>
                                                <h4>2-2</h4>
                                                <div class="pull-left text-right">
                                                    <p>Chelsea</p>
                                                    <img src="/images/logo-team/Chelsea.png">
                                                </div>

                                            </div>
                                            <div class="content-event-match">

                                                <!--                                        Home ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="red-card"></div>
                                                        </div>
                                                        <div class="min-live">90+4'</div>
                                                        <div class="name-live">Willian</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home ใบเหลือง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="yellow-card"></div>
                                                        </div>
                                                        <div class="min-live">87'</div>
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home ทำประตู-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="goal-score"></div>
                                                        </div>
                                                        <div class="min-live">71'</div>
                                                        <div class="score-live">2 - 4</div>
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                    </div>
                                                </div>
                                                <!--                                        Home เปลี่ยนตัว-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="substitute-player"></div>
                                                        </div>
                                                        <div class="min-live">69'</div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-out">out</span>
                                                            David
                                                            Luiz
                                                        </div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-in">in</span>
                                                            Victor
                                                            Moses
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Home ใบเหลือง+ใบเหลือง=ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="group-yellow-card"></div>
                                                        </div>
                                                        <div class="min-live">55'</div>
                                                        <div class="name-live">Ross Barkley</div>
                                                    </div>
                                                </div>
                                                <!--Home จุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="penalty-score"></div>
                                                        </div>
                                                        <div class="min-live">45'</div>
                                                        <div class="name-live">Gary Cahill</div>
                                                    </div>
                                                </div>
                                                <!--Home พลาดจุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="penalty-none-score"></div>
                                                        </div>
                                                        <div class="min-live">41'</div>
                                                        <div class="name-live">Álvaro Morata</div>
                                                    </div>
                                                </div>
                                                <!--Home ทำเข้าประตูตัวเอง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-home-live">
                                                        <div class="box-football-icon">
                                                            <div class="own-goal"></div>
                                                        </div>
                                                        <div class="min-live">28'</div>
                                                        <div class="name-live">Álvaro Morata</div>
                                                    </div>
                                                </div>


                                                <!--                                        Away ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Willian</div>
                                                        <div class="min-live">90+4'</div>
                                                        <div class="box-football-icon">
                                                            <div class="red-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away ใบเหลือง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                        <div class="min-live">87'</div>
                                                        <div class="box-football-icon">
                                                            <div class="yellow-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away ทำประตู-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Cesc Fàbregas</div>
                                                        <div class="score-live">2 - 4</div>
                                                        <div class="min-live">71'</div>
                                                        <div class="box-football-icon">
                                                            <div class="goal-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                        Away เปลี่ยนตัว-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live"><span
                                                                    class="substitute-player-in">in</span>
                                                            Victor
                                                            Moses
                                                        </div>
                                                        <div class="name-live"><span
                                                                    class="substitute-player-out">out</span>
                                                            David
                                                            Luiz
                                                        </div>
                                                        <div class="min-live">69'</div>
                                                        <div class="box-football-icon">
                                                            <div class="substitute-player"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away ใบเหลือง+ใบเหลือง=ใบแดง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Ross Barkley</div>
                                                        <div class="min-live">55'</div>
                                                        <div class="box-football-icon">
                                                            <div class="group-yellow-card"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away จุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Gary Cahill</div>
                                                        <div class="min-live">45'</div>
                                                        <div class="box-football-icon">
                                                            <div class="penalty-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away พลาดจุดโทษ-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Álvaro Morata</div>
                                                        <div class="min-live">41'</div>
                                                        <div class="box-football-icon">
                                                            <div class="penalty-none-score"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Away ทำเข้าประตูตัวเอง-->
                                                <div class="full-width line-content-all">
                                                    <div class="col-away-live pull-right">
                                                        <div class="name-live">Álvaro Morata</div>
                                                        <div class="min-live">28'</div>
                                                        <div class="box-football-icon">
                                                            <div class="own-goal"></div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 content-status-match">
                                        <div class="thumbnail">

                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#statistics-tab"
                                                                                          role="tab"
                                                                                          data-toggle="tab">Statistics</a>
                                                </li>
                                                <li role="presentation"><a href="#lineUp-tab"
                                                                           role="tab"
                                                                           data-toggle="tab">Line
                                                        Up</a>
                                                </li>

                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active"
                                                     id="statistics-tab">
                                                    <div class="table statistics-table">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <div class="pull-right">
                                                                        <h4>Manchester United
                                                                        </h4>
                                                                        <img src="/images/logo-team/manchester.png">
                                                                    </div>
                                                                </td>
                                                                <td><h3>VS</h3></td>
                                                                <td>
                                                                    <div class="pull-left">
                                                                        <img src="/images/logo-team/Chelsea.png">
                                                                        <h4>Chelsea</h4>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ยิงประตู-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">15</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 15%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ยิงประตู</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="9" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 9%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">9</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ยิงตรงกรอบ-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">5</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 5%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ยิงตรงกรอบ</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="9" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 2%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">2</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ผิดกติกา-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">13</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="15"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 13%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ผิดกติกา</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="8" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 8%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">8</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เตะมุม-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">2</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="2"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 2%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เตะมุม</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">3</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ฟรีคิก-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">12</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="12"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 12%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ฟรีคิก</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="19"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 19%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">19</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                ล้ำหน้า-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">6</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="6"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 6%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ล้ำหน้า</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="4" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 4%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">4</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                จำนวนใบเหลือง-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">3</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="3"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>ล้ำหน้า</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">3</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เวลาครองบอล-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress active">55%</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="55"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 55%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เวลาครองบอล</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="45"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 45%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">45%</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เซฟได้-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">1</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="1"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 1%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เซฟได้</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="4" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 4%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress active">4</div>
                                                                </td>
                                                            </tr>
                                                            <!--                                                เปลี่ยนตัว-->
                                                            <tr>
                                                                <td>
                                                                    <div class="num-progress">3</div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                                             role="progressbar"
                                                                             aria-valuenow="3"
                                                                             aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>เปลี่ยนตัว</td>
                                                                <td>
                                                                    <div class="progress">
                                                                        <div class="progress-bar"
                                                                             role="progressbar"
                                                                             aria-valuenow="3" aria-valuemin="0"
                                                                             aria-valuemax="100"
                                                                             style="width: 3%;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="num-progress">3</div>
                                                                </td>
                                                            </tr>


                                                        </table>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="lineUp-tab">
                                                    <div class="lineUp-table">
                                                        <table class="table">
                                                            <tr>
                                                                <td>
                                                                    <div class="pull-right full-width text-right">
                                                                        <span>Manchester United</span>
                                                                        <img src="/images/logo-team/manchester.png">
                                                                    </div>
                                                                    <div class="pull-right full-width">
                                                                        <div class="content-lineUp-home">
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="num-player">1
                                                                                    </div>
                                                                                    <div>David De Gea</div>
                                                                                    <div>GK</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">2
                                                                                    </div>
                                                                                    <div>Victor Lindelöf</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">6
                                                                                    </div>
                                                                                    <div>Paul Pogba</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">8
                                                                                    </div>
                                                                                    <div>Juan Mata</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">9
                                                                                    </div>
                                                                                    <div>Romelu Lukaku</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">10
                                                                                    </div>
                                                                                    <div>Marcus Rashford</div>
                                                                                    <div>FT</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">11
                                                                                    </div>
                                                                                    <div>Anthony Martial</div>
                                                                                    <div>RF</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">12
                                                                                    </div>
                                                                                    <div>Chris Smalling</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">15
                                                                                    </div>
                                                                                    <div>Andreas Pereira</div>
                                                                                    <div>MD</div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="pull-left">
                                                                        <img src="/images/logo-team/Chelsea.png">
                                                                        <span>Chelsea</span>
                                                                    </div>
                                                                    <div class="pull-left full-width">
                                                                        <div class="content-lineUp-away">
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="num-player">1
                                                                                    </div>
                                                                                    <div>David De Gea</div>
                                                                                    <div>GK</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">2
                                                                                    </div>
                                                                                    <div>Victor Lindelöf</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">6
                                                                                    </div>
                                                                                    <div>Paul Pogba</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">8
                                                                                    </div>
                                                                                    <div>Juan Mata</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">9
                                                                                    </div>
                                                                                    <div>Romelu Lukaku</div>
                                                                                    <div>CM</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">10
                                                                                    </div>
                                                                                    <div>Marcus Rashford</div>
                                                                                    <div>FT</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">11
                                                                                    </div>
                                                                                    <div>Anthony Martial</div>
                                                                                    <div>RF</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">12
                                                                                    </div>
                                                                                    <div>Chris Smalling</div>
                                                                                    <div>CB</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="num-player">15
                                                                                    </div>
                                                                                    <div>Andreas Pereira</div>
                                                                                    <div>MD</div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>


                                                        </table>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="statistics">
                                    <div class="table statistics-table">
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="pull-right">
                                                        <img src="/images/logo-team/manchester.png">
                                                    </div>
                                                </td>
                                                <td><h3>VS</h3></td>
                                                <td>
                                                    <div class="pull-left">
                                                        <img src="/images/logo-team/Chelsea.png">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>เริ่มเขี่ยบอล</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>เปลี่ยนตัวคนแรก</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="check-team"></span></td>
                                                <td>เริ่มเขี่ยบอล</td>
                                                <td></td>
                                            </tr>
                                            <!--                                                ยิงประตู-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">15</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 15%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ยิงประตู</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="9" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 9%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">9</div>
                                                </td>
                                            </tr>
                                            <!--                                                ยิงตรงกรอบ-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">5</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 5%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ยิงตรงกรอบ</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="9" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 2%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">2</div>
                                                </td>
                                            </tr>
                                            <!--                                                ผิดกติกา-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">13</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="15"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 13%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ผิดกติกา</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="8" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 8%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">8</div>
                                                </td>
                                            </tr>
                                            <!--                                                เตะมุม-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">2</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="2"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 2%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เตะมุม</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">3</div>
                                                </td>
                                            </tr>
                                            <!--                                                ฟรีคิก-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">12</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="12"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 12%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ฟรีคิก</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="19" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 19%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">19</div>
                                                </td>
                                            </tr>
                                            <!--                                                ล้ำหน้า-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">6</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="6"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 6%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ล้ำหน้า</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="4" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 4%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">4</div>
                                                </td>
                                            </tr>
                                            <!--                                                จำนวนใบเหลือง-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">3</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="3"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 3%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>ล้ำหน้า</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">3</div>
                                                </td>
                                            </tr>
                                            <!--                                                เวลาครองบอล-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress active">55%</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="55"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 55%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เวลาครองบอล</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="45" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 45%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">45%</div>
                                                </td>
                                            </tr>
                                            <!--                                                เซฟได้-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">1</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="1"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 1%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เซฟได้</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="4" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 4%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress active">4</div>
                                                </td>
                                            </tr>
                                            <!--                                                เปลี่ยนตัว-->
                                            <tr>
                                                <td>
                                                    <div class="num-progress">3</div>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger pull-right"
                                                             role="progressbar" aria-valuenow="3"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100" style="width: 3%;">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>เปลี่ยนตัว</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="3" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: 3%;">
                                                        </div>
                                                    </div>
                                                    <div class="num-progress">3</div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="check-team"></span></td>
                                                <td>เปลี่ยนตัวคนสุดท้าย</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ใบเหลืองใบสุดท้าย</td>
                                                <td><span class="check-team"></span></td>
                                            </tr>


                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="lineUp">

                                </div>
                                <div role="tabpanel" class="tab-pane" id="review">
                                    <div class="home-panel-body pull-left">
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/profess-player/sample-logo01.png">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>LOMTOE.NET</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png"</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/profess-player/sample-logo05.png">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>SMM SPORT</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png"</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    อาจจะฝืดๆหน่อย​ แต่ต้องสอย​ อย่าให้เสียชื่อทีมใหญ่
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-home-review full-width pull-right">
                                            <div class="pull-right">
                                                <div class="area-logoName">
                                                    <div class="pull-right logo-review-user">
                                                        <img src="/images/9.jpg">
                                                    </div>
                                                    <div class="pull-right">
                                                        <h4>หมูตัวนี้ตัวใหญ่</h4>
                                                        <ul>
                                                            <li>@manchester</li>
                                                            <li><img src="/images/logo-team/manchester.png">
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right text-review-content text-right">
                                                <p>
                                                    ชอบความคมของตรานกแก้ว ซวยแล้วบายาโดลิด
                                                </p>
                                            </div>
                                            <div class="pull-left ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="away-panel-body pull-right">
                                        <div class="content-away-review full-width pull-left">
                                            <div class="pull-left">
                                                <div class="area-logoName area-logoName-away">
                                                    <div class="pull-left logo-review-user">
                                                        <img src="/images/profess-player/sample-logo12.png">
                                                    </div>
                                                    <div class="pull-left">
                                                        <h4>GOAL.IN.TH</h4>
                                                        <ul>
                                                            <li><img src="/images/logo-team/Chelsea.png"</li>
                                                            <li>@Chelsea</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-left text-review-content text-left">
                                                <p>
                                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                                </p>
                                            </div>
                                            <div class="pull-right ct-winrate">
                                                <div>
                                                    <h3>28%</h3>
                                                    <small>Win rate</small>
                                                </div>
                                                <div>
                                                    <h3>30</h3>
                                                    <small>Days</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="full-width text-center">
                                        <a class="btn btn-default btn-sm" href="#"
                                           role="button">อ่านทีเด็ดทรรศนะทั้งหมด</a>
                                    </div>
                                </div>
                                <div style="clear: both"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
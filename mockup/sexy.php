<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ngoal | ทีเด็ด ทรรศนะ</title>
    <link rel="shortcut icon" type="images/x-icon" href="/images/favicon.ico.png">
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/doce.css">
    <link rel="stylesheet" href="/css/livescore.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <link rel="stylesheet" href="/css/game-redesign.css">
    <link rel="stylesheet" href="/css/review-new-style.css">
    <link rel="stylesheet" href="/css/sexy.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/masonry-docs.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="js/jquery-timeago/jquery.timeago.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="/js/moment.js"></script>

    <?php include 'view/index/googletagmanager.php'; ?>
</head>
<body style="background-color: #f6f6f6;">
<!--index menu-->
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="topic-content-sexy">
    <div class="container">
        <div class="text-title">
            <h4>Sexy Football</h4>
            <div class="pull-right">
                <button class="btn btn-link active" type="submit">All</button>
                <button class="btn btn-link" type="submit" data-toggle="tooltip" data-placement="bottom" title="รูปทั้งหมด">
                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                </button>
                <button class="btn btn-link" type="submit" data-toggle="tooltip" data-placement="bottom" title="Clip Video">
                    <i class="fa fa-video-camera" aria-hidden="true"></i>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="wrap-sexy">

    <div id="grid_container">

        <div class="thumbnail-image">
            <a class="link_wrap" href="#"></a>
            <div class="icon-type-image"></div>
            <div class="textanim-hidden">
                <h3>ใส ใส</h3>
            </div>
            <img src="/images/gallery/1.jpg">

        </div>
        <div class="thumbnail-image">
            <a class="link_wrap" href="#"></a>
            <div class="icon-type-clip"></div>
            <div class="textanim-hidden">
                <h3>ชุดแดง</h3>
            </div>
            <img src="/images/gallery/2.jpg">
        </div>
        <div class="thumbnail-image">
            <a class="link_wrap" href="#"></a>
            <div class="icon-type-image"></div>
            <div class="textanim-hidden">
                <h3>ใส ใส</h3>
            </div>
            <img src="/images/gallery/3.jpg">
        </div>
        <div class="thumbnail-image">
            <a class="link_wrap" href="#"></a>
            <div class="icon-type-clip"></div>
            <div class="textanim-hidden">
                <h3>ชุดแดง</h3>
            </div>
            <img src="/images/gallery/4.jpg">
        </div>
        <div class="thumbnail-image">
            <a class="link_wrap" href="#"></a>
            <div class="icon-type-clip"></div>
            <div class="textanim-hidden">
                <h3>ชุดแดง</h3>
            </div>
            <img src="/images/gallery/5.jpg">
        </div>
        <div class="thumbnail-image">
            <a class="link_wrap" href="#"></a>
            <div class="icon-type-clip"></div>
            <div class="textanim-hidden">
                <h3>ชุดแดง</h3>
            </div>
            <img src="/images/gallery/6.jpg">
        </div>
        <div class="thumbnail-image">
            <a class="link_wrap" href="#"></a>
            <div class="icon-type-image"></div>
            <div class="textanim-hidden">
                <h3>ใส ใส</h3>
            </div>
            <img src="/images/gallery/7.jpg">
        </div>
        <div class="thumbnail-image">
            <a class="link_wrap" href="#"></a>
            <div class="icon-type-clip"></div>
            <div class="textanim-hidden">
                <h3>ชุดแดง</h3>
            </div>
            <img src="/images/gallery/8.jpg">
        </div>
        <div class="thumbnail-image">
            <a class="link_wrap" href="#"></a>
            <div class="icon-type-image"></div>
            <div class="textanim-hidden">
                <h3>ใส ใส</h3>
            </div>
            <img src="/images/gallery/9.jpg">
        </div>
        <div class="thumbnail-image">
            <a class="link_wrap" href="#"></a>
            <div class="icon-type-image"></div>
            <div class="textanim-hidden">
                <h3>ใส ใส</h3>
            </div>
            <img src="/images/gallery/10.jpg">
        </div>
    </div>

</div>
<script>
    function renderGrid() {
        var blocks = document.getElementById("grid_container").children;
        var pad = 10, cols = 5, newleft, newtop;
        for (var i = 1; i < blocks.length; i++) {
            if (i % cols == 0) {
                newtop = (blocks[i - cols].offsetTop + blocks[i - cols].offsetHeight) + pad;
                blocks[i].style.top = newtop + "px";
            } else {
                if (blocks[i - cols]) {
                    newtop = (blocks[i - cols].offsetTop + blocks[i - cols].offsetHeight) + pad;
                    blocks[i].style.top = newtop + "px";
                }
                newleft = (blocks[i - 1].offsetLeft + blocks[i - 1].offsetWidth) + pad;
                blocks[i].style.left = newleft + "px";
            }
        }
    }
    window.addEventListener("load", renderGrid, false);
    window.addEventListener("resize", renderGrid, false);

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
</body>
</html>
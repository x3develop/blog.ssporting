<html>
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="shortcut icon" type="images/x-icon" href="images/favicon.ico.png">
    <title>บ้านผลบอล | เว็บทายผลบอลอันดับ 1 ของประเทศ ข่าวฟุตบอล ไฮไลท์การแข่งขัน</title>
    <meta name="keywords" content="News Football,Highlight Football,Video Football,Football">
    <meta name="description" content="บ้านผลบอล เว็บทายผลบอลอันดับ 1 ของประเทศ ข่าวฟุตบอล ไฮไลท์การแข่งขัน">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">

    <!--Css-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/highlight-new.css">
    <link rel="stylesheet" href="/css/new-menu.css">
    <link rel="stylesheet" href="/css/swiper.min.css">
    <link rel="stylesheet" href="/css/scrollbar.css">

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <!--    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">-->

    <!--js-->

    <script src="/js/swiper.min.js"></script>
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="nanoscrollbar/nanoscrollbar.js"></script>


    <style>
        html body,
        h1, h2, h3, h4, h5 {
            font-family: "Roboto", Helvetica, Arial, sans-serif !important;
        }
        .container-new-menu .navbar-default {
            border-bottom: solid 1px #ddd;
        }

        .container-new-menu .navbar-default.affix {
            position: relative;
        }
    </style>

</head>
<body>
<?php include '../mockup/new-menu.php' ?>
<a href="#" id="scroll" style="display: none;"><span></span></a>
<div class="container-fluid-highlight full-width">
    <div class="sidebar-league" data-spy="affix" data-offset-top="60">
        <div class="nano">
            <div class="nano-content">
                <div class="sb-league">
                    <div class="small-topic">
                        <h4>Event Hot</h4>
                    </div>
                    <ul>
                        <li>
                            <img src="/images/five-leagues/uefa_nations_league.png">
                            <a href="#">UEFA Nations League</a>
                            <p>5</p>
                        </li>
                        <li>
                            <img src="/images/five-leagues/AFF_Suzuki_Cup.png">
                            <a href="#">2018 AFF Championship Suzuki Cup</a>
                            <p>2</p>
                        </li>
                    </ul>
                </div>
                <div class="sb-league">
                    <div class="small-topic">
                        <h4>Leagues</h4>
                    </div>
                    <ul>
                        <li class="active">
                            <img src="/images/five-leagues/premier-league-logo.png">
                            <a href="#">Premier League</a>
                            <p>12</p>
                        </li>
                        <li>
                            <img src="/images/five-leagues/La-Liga-Logo-1.png">
                            <a href="#">La liga</a>
                            <p>7</p>
                        </li>
                        <li>
                            <img src="/images/five-leagues/logo_bl.gif">
                            <a href="#">Bundesliga</a>
                            <p>10</p>
                        </li>
                        <li>
                            <img src="/images/five-leagues/Ligue-1-logo-france.png">
                            <a href="#">Ligue 1</a>
                            <p></p>
                        </li>
                        <li>
                            <img src="/images/five-leagues/seriea1.png">
                            <a href="#">Seriea 1</a>
                            <p></p>
                        </li>
                    </ul>
                </div>
                <div class="sb-league-information">
                    <div class="top-league-information full-width">
                        <img src="/images/five-leagues/premier-league-logo.png">
                        <h4>Premier League</h4>
                        <p>English</p>
                    </div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#table-stance" role="tab" data-toggle="tab">Table</a></li>
                        <li role="presentation"><a href="#table-top-score" role="tab" data-toggle="tab">Top score</a></li>
                        <li role="presentation"><a href="#table-fixture" role="tab" data-toggle="tab">Fixtures</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane tab-table-league active" id="table-stance">
                            <table>
                                <tr>
                                    <th class="text-right" colspan="6">Pts</th>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="stance-green">1</div>
                                    </td>
                                    <td><img src="/images/logo-team/manchester.png"></td>
                                    <td><span>Man City</span></td>
                                    <td>7</td>
                                    <td>19</td>
                                    <td>19</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="stance-green">2</div>
                                    </td>
                                    <td><img src="/images/logo-team/Chelsea.png"></td>
                                    <td><span>Liverpool</span></td>
                                    <td>7</td>
                                    <td>19</td>
                                    <td>15</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="stance-green">3</div>
                                    </td>
                                    <td><img src="/images/logo-team/manchester.png"></td>
                                    <td><span>Chelsea</span></td>
                                    <td>7</td>
                                    <td>19</td>
                                    <td>19</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="stance-green">4</div>
                                    </td>
                                    <td><img src="/images/logo-team/Chelsea.png"></td>
                                    <td><span>Tottenham</span></td>
                                    <td>7</td>
                                    <td>19</td>
                                    <td>15</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="stance-blue">5</div>
                                    </td>
                                    <td><img src="/images/logo-team/manchester.png"></td>
                                    <td><span>Arsenal</span></td>
                                    <td>7</td>
                                    <td>19</td>
                                    <td>19</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="stance-default">6</div>
                                    </td>
                                    <td><img src="/images/logo-team/Chelsea.png"></td>
                                    <td><span>Bournemouth</span></td>
                                    <td>7</td>
                                    <td>19</td>
                                    <td>15</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="stance-default">7</div>
                                    </td>
                                    <td><img src="/images/logo-team/Chelsea.png"></td>
                                    <td><span>Watford</span></td>
                                    <td>7</td>
                                    <td>19</td>
                                    <td>15</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="stance-default">8</div>
                                    </td>
                                    <td><img src="/images/logo-team/Chelsea.png"></td>
                                    <td><span>Man Utd</span></td>
                                    <td>7</td>
                                    <td>19</td>
                                    <td>15</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="stance-default">9</div>
                                    </td>
                                    <td><img src="/images/logo-team/Chelsea.png"></td>
                                    <td><span>Everton</span></td>
                                    <td>7</td>
                                    <td>19</td>
                                    <td>15</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="stance-default">10</div>
                                    </td>
                                    <td><img src="/images/logo-team/Chelsea.png"></td>
                                    <td><span>Leicester</span></td>
                                    <td>7</td>
                                    <td>19</td>
                                    <td>15</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="stance-red">11</div>
                                    </td>
                                    <td><img src="/images/logo-team/Chelsea.png"></td>
                                    <td><span>Wolverhampton</span></td>
                                    <td>7</td>
                                    <td>19</td>
                                    <td>15</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="stance-red">12</div>
                                    </td>
                                    <td><img src="/images/logo-team/Chelsea.png"></td>
                                    <td><span>Brighton</span></td>
                                    <td>7</td>
                                    <td>19</td>
                                    <td>15</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="stance-red">13</div>
                                    </td>
                                    <td><img src="/images/logo-team/Chelsea.png"></td>
                                    <td><span>Chelsea</span></td>
                                    <td>7</td>
                                    <td>19</td>
                                    <td>15</td>
                                </tr>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="table-top-score">
                            <div class="table-topscore">
                                <table>
                                    <tbody><tr>
                                        <td>1</td>
                                        <td><img src="/images/player/image_4.jpg"></td>
                                        <td>
                                            <span>Eden Hazard</span>
                                            <ul>
                                                <li><img src="/images/logo-team/Chelsea.png"></li>
                                                <li><p>Chelsea</p></li>
                                            </ul>
                                        </td>
                                        <td>23</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td><img src="/images/player/image_5.jpg"></td>
                                        <td>
                                            <span>Romelu Lukaku</span>
                                            <ul>
                                                <li> <img src="/images/logo-team/manchester.png"></li>
                                                <li> <p>Manchester United</p></li>
                                            </ul>
                                        </td>
                                        <td>19</td>
                                    </tr>

                                    </tbody></table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane tab-table-fixture" id="table-fixture">
                            <div class="box-match-tournament">
                                <div class="box-match-list-topic">
                                    <div class="pull-left">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <p>15 Apr 18</p>
                                    </div>
                                </div>
                                <div class="box-match-list">

                                    <div class="text-center">
                                        <div class="time-match">15:00</div>
                                    </div>
                                    <div>
                                        <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                        </div>
                                        <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                            united
                                        </div>
                                    </div>
                                    <div>
                                        <div class="home-score-match">0</div>
                                        <div class="away-score-match">0</div>
                                    </div>
                                </div>
                                <div class="box-match-list">

                                    <div class="text-center">
                                        <div class="time-match">15:00</div>
                                    </div>
                                    <div>
                                        <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                        </div>
                                        <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                            united
                                        </div>
                                    </div>
                                    <div>
                                        <div class="home-score-match">0</div>
                                        <div class="away-score-match">0</div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-match-tournament">
                                <div class="box-match-list-topic">
                                    <div class="pull-left">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <p>15 Apr 18</p>
                                    </div>
                                </div>
                                <div class="box-match-list">

                                    <div class="text-center">
                                        <div class="time-match">15:00</div>
                                    </div>
                                    <div>
                                        <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                        </div>
                                        <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                            united
                                        </div>
                                    </div>
                                    <div>
                                        <div class="home-score-match">0</div>
                                        <div class="away-score-match">0</div>
                                    </div>
                                </div>
                                <div class="box-match-list">

                                    <div class="text-center">
                                        <div class="time-match">15:00</div>
                                    </div>
                                    <div>
                                        <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                        </div>
                                        <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                            united
                                        </div>
                                    </div>
                                    <div>
                                        <div class="home-score-match">0</div>
                                        <div class="away-score-match">0</div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-match-tournament">
                                <div class="box-match-list-topic">
                                    <div class="pull-left">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <p>15 Apr 18</p>
                                    </div>
                                </div>
                                <div class="box-match-list">

                                    <div class="text-center">
                                        <div class="time-match">15:00</div>
                                    </div>
                                    <div>
                                        <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                        </div>
                                        <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                            united
                                        </div>
                                    </div>
                                    <div>
                                        <div class="home-score-match">0</div>
                                        <div class="away-score-match">0</div>
                                    </div>
                                </div>
                                <div class="box-match-list">

                                    <div class="text-center">
                                        <div class="time-match">15:00</div>
                                    </div>
                                    <div>
                                        <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                        </div>
                                        <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                            united
                                        </div>
                                    </div>
                                    <div>
                                        <div class="home-score-match">0</div>
                                        <div class="away-score-match">0</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <div class="main">
        <div class="main-content">
            <div class="header-highlight">
                <div class="hamburger" id="hamburger" onclick="toggleSidenav();"></div>
                <div class="bt-title-team">
                    <img src="/images/logo-team/Chelsea.png">
                    <h2>Chelsea</h2>
                </div>
                <div class="bt-title-league pull-right">
                    <img src="/images/five-leagues/premier-league-logo.png">
                    <h5>Premier League</h5>
                    <span>2018-2019</span>

                </div>
            </div>
            <!-- Swiper -->
            <div class="swiper-container swiper1 team-highlight">
                <div class="swiper-wrapper">
                    <div class="swiper-slide active">
                        <div class="logo-team-slide">

                            <img src="/images/logo-team/Chelsea.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/Arsenal.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/Bournemouth.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/Brighton.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/Burnley.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/Cardiff.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/Crystal-Palace.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/Everton.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/Fulham.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/Huddersfield.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/Leicester-City.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/Liverpool.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/manchester.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/mancity.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/Newcastle.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/Southampton.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/Tottenham.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/Watford.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/WestHam.png">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="logo-team-slide">
                            <img src="/images/logo-team/Wolverhampton.png">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid-video">
<!--            <div class="container-fluid-video" data-spy="affix" data-offset-top="200">-->
                <iframe id="large-videos-autoPlay" loop="0" width="100%" height="600" data-autoplay="true"
                        src="https://streamable.com/s/4vlqv" frameborder="0" allowfullscreen=""></iframe>
            </div>
            <div class="container-fluid-video-detail">
                <div class="bt-title-video">
                    <h2>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h2>
                <p>21-11-2018-19</p>
                </div>
                <div class="bt-title-video-right">
                    <div class="pull-right">
                        <p>350 View</p>
                        <div class="bt-share">
                            <button class="btn btn-facebook" type="button">SHARE</button>
                            <button class="btn btn-twitter" type="button">SHARE</button>
                        </div>
                    </div>
                </div>

            </div>


            <!--            Recent-->
            <div class="swiper-container swiper2 swiper-highlight swiper-container-recent">
                <div class="text-title">
                    <h4>Recent Video</h4>
                </div>
                <div class="swiper-wrapper">
                    <div class="swiper-slide box-recent-thumbnail">
                        <a href="#"></a>
                        <div class="highlight-thumbnail">
                            <img src="/images/ca10r_first_1.jpg">
                        </div>
                        <div class="caption">
                            <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                            <p>21-11-2018-19</p>
                        </div>
                    </div>
                    <div class="swiper-slide box-recent-thumbnail">
                        <a href="#"></a>
                        <div class="highlight-thumbnail">
                            <img src="/images/ca10r_first_1.jpg">
                        </div>
                        <div class="caption">
                            <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                            <p>21-11-2018-19</p>
                        </div>
                    </div>
                    <div class="swiper-slide box-recent-thumbnail">
                        <a href="#"></a>
                        <div class="highlight-thumbnail">
                            <img src="/images/ca10r_first_1.jpg">
                        </div>
                        <div class="caption">
                            <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                            <p>21-11-2018-19</p>
                        </div>
                    </div>
                    <div class="swiper-slide box-recent-thumbnail">
                        <a href="#"></a>
                        <div class="highlight-thumbnail">
                            <img src="/images/ca10r_first_1.jpg">
                        </div>
                        <div class="caption">
                            <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                            <p>21-11-2018-19</p>
                        </div>
                    </div>
                    <div class="swiper-slide box-recent-thumbnail">
                        <a href="#"></a>
                        <div class="highlight-thumbnail">
                            <img src="/images/ca10r_first_1.jpg">
                        </div>
                        <div class="caption">
                            <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                            <p>21-11-2018-19</p>
                        </div>
                    </div>
                    <div class="swiper-slide box-recent-thumbnail">
                        <a href="#"></a>
                        <div class="highlight-thumbnail">
                            <img src="/images/ca10r_first_1.jpg">
                        </div>
                        <div class="caption">
                            <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                            <p>21-11-2018-19</p>
                        </div>
                    </div>

                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-next-recent"></div>
                <div class="swiper-button-prev swiper-button-prev-recent"></div>
            </div>

            <!--            ไฮไลท์คู่บอล-->
            <div class="swiper-container swiper3 swiper-highlight swiper-container-highlight">
                <div class="text-title">
                    <h4>ไฮไลท์คู่บอล</h4>
                </div>
                <div class="swiper-wrapper">
                    <div class="swiper-slide box-recent-thumbnail">
                        <a href="#"></a>
                        <div class="highlight-thumbnail">
                            <img src="/images/ca10r_first_1.jpg">
                        </div>
                        <div class="caption">
                            <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                            <p>21-11-2018-19</p>
                        </div>
                    </div>
                    <div class="swiper-slide box-recent-thumbnail">
                        <a href="#"></a>
                        <div class="highlight-thumbnail">
                            <img src="/images/ca10r_first_1.jpg">
                        </div>
                        <div class="caption">
                            <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                            <p>21-11-2018-19</p>
                        </div>
                    </div>
                    <div class="swiper-slide box-recent-thumbnail">
                        <a href="#"></a>
                        <div class="highlight-thumbnail">
                            <img src="/images/ca10r_first_1.jpg">
                        </div>
                        <div class="caption">
                            <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                            <p>21-11-2018-19</p>
                        </div>
                    </div>
                    <div class="swiper-slide box-recent-thumbnail">
                        <a href="#"></a>
                        <div class="highlight-thumbnail">
                            <img src="/images/ca10r_first_1.jpg">
                        </div>
                        <div class="caption">
                            <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                            <p>21-11-2018-19</p>
                        </div>
                    </div>
                    <div class="swiper-slide box-recent-thumbnail">
                        <a href="#"></a>
                        <div class="highlight-thumbnail">
                            <img src="/images/ca10r_first_1.jpg">
                        </div>
                        <div class="caption">
                            <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                            <p>21-11-2018-19</p>
                        </div>
                    </div>
                    <div class="swiper-slide box-recent-thumbnail">
                        <a href="#"></a>
                        <div class="highlight-thumbnail">
                            <img src="/images/ca10r_first_1.jpg">
                        </div>
                        <div class="caption">
                            <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                            <p>21-11-2018-19</p>
                        </div>
                    </div>

                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-next-highlight"></div>
                <div class="swiper-button-prev swiper-button-prev-highlight"></div>
            </div>
        </div>
        <div class="side-list-video" data-spy="affix" data-offset-top="60">

            <div class="nano">
                <div class="nano-content">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#fullmatch-tab" data-toggle="tab"><span>Full Match</span></a>
                        </li>
                        <li role="presentation">
                            <a href="#allgoal-tab" data-toggle="tab"><span>All Goal</span></a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <!-- Tab panes -->
                        <div role="tabpanel" class="tab-pane tab-pane-full-match active" id="fullmatch-tab">
                            <div class="box-list-video active bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane tab-pane-all-goal" id="allgoal-tab">
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                            <div class="box-list-video bt-flex">
                                <div class="box-video-thumbnail">
                                    <img src="/images/ca10r_first_1.jpg">
                                </div>
                                <div class="box-video-detail">
                                    <p>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</p>
                                    <span class="label label-info">12/12/2019</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Initialize Swiper -->
<script>
    var swiper1 = new Swiper('.swiper1', {
        slidesPerView: 20,
        spaceBetween: 0
    });
</script>
<script>
    var swiper2 = new Swiper('.swiper2', {
        slidesPerView: 4,
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next-recent',
            prevEl: '.swiper-button-prev-recent',
        },
    });
</script>
<script>
    var swiper3 = new Swiper('.swiper3', {
        slidesPerView: 4,
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next-highlight',
            prevEl: '.swiper-button-prev-highlight',
        },
    });
</script>

<script>
    function toggleSidenav() {
        document.body.classList.toggle('sidenav-active');
    }
</script>
<script>
    $('.swiper-slide').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $('.swiper-slide.active').removeClass('active');
            $(this).addClass('active');
        }
    });
</script>
<script>
    $('.sb-league ul li').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $('.sb-league li.active').removeClass('active');
            $(this).addClass('active');
        }
    });
</script>
<script>
    $('.box-list-video').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $('.box-list-video.active').removeClass('active');
            $(this).addClass('active');
        }
    });
</script>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ngoal | ทีเด็ด ทรรศนะ</title>
    <link rel="shortcut icon" type="images/x-icon" href="/images/favicon.ico.png">
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <!--    <link rel="stylesheet" href="css/style.css">-->
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/game-redesign-V2.css">
    <link rel="stylesheet" href="/css/program_ball.css">
    <link rel="stylesheet" href="/css/game-redesign.css">
    <link rel="stylesheet" href="/css/circle-progress-bar.css">
    <link rel="stylesheet" href="/css/football-symbol.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="/nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/swiper.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="/js/jquery-timeago/jquery.timeago.js"></script>
    <script src="/js/swiper.min.js"></script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
    <?php include 'view/index/googletagmanager.php'; ?>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">
<?php include $_SERVER['DOCUMENT_ROOT'] . '/view/index/menu-all.php'; ?>


<div class="content-select-match content-select-match-index" style="background-color: #f5f5f5;">
    <!-- Swiper -->
    <div class="swiper-container swiper_game full-width">
        <div class="swiper-wrapper" data-toggle="buttons">
            <div class="swiper-slide">
                <div class="mt-box active" onclick="toggleVisibility('time-countdown');">
                    <div class="logo-team">
                        <img src="/images/logo-team/manchester.png">
                        <p>MUT</p>
                    </div>
                    <div class="sm-hdp">
                        <h5>-HDP-</h5>
                        <strong>-2.50</strong>
                        <ul>
                            <li>1.9</li>
                            <li>1.69</li>
                        </ul>
                        <div class="mt-time">18:00</div>
                    </div>
                    <div class="logo-team">
                        <img src="/images/logo-team/Chelsea.png">
                        <p>CHA</p>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="mt-box mt-box-full-match" onclick="toggleVisibility('time-fulltime');">
                    <div class="logo-team">
                        <div class="score-full-match-home">
                            <strong>2</strong>
                        </div>
                        <img src="/images/logo-team/manchester.png">
                        <p>MUT</p>
                    </div>
                    <div class="sm-hdp">
                        <h5>-HDP-</h5>
                        <strong>-2.50</strong>
                        <ul>
                            <li>1.9</li>
                            <li>1.69</li>
                        </ul>
                        <div class="label">Full Match</div>
                    </div>
                    <div class="logo-team">
                        <div class="score-full-match-away">
                            <strong>0</strong>
                        </div>
                        <img src="/images/logo-team/Chelsea.png">
                        <p>CHA</p>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="mt-box mt-box-live" onclick="toggleVisibility('time-live');">
                    <div class="logo-team">
                        <div class="score-full-match-home">
                            <strong>2</strong>
                        </div>
                        <img src="/images/logo-team/manchester.png">
                        <p>MUT</p>
                    </div>
                    <div class="sm-hdp">
                        <h5>-HDP-</h5>
                        <strong>-2.50</strong>
                        <ul>
                            <li>1.9</li>
                            <li>1.69</li>
                        </ul>
                        <div class="label">Live</div>
                    </div>
                    <div class="logo-team">
                        <div class="score-full-match-away">
                            <strong>0</strong>
                        </div>
                        <img src="/images/logo-team/Chelsea.png">
                        <p>CHA</p>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="mt-box">
                    <div class="logo-team">
                        <img src="/images/logo-team/manchester.png">
                        <p>MUT</p>
                    </div>
                    <div class="sm-hdp">
                        <h5>-HDP-</h5>
                        <strong>-2.50</strong>
                        <ul>
                            <li>1.9</li>
                            <li>1.69</li>
                        </ul>
                        <div class="mt-time">18:00</div>
                    </div>
                    <div class="logo-team">
                        <img src="/images/logo-team/Chelsea.png">
                        <p>CHA</p>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="mt-box">
                    <div class="logo-team">
                        <img src="/images/logo-team/manchester.png">
                        <p>MUT</p>
                    </div>
                    <div class="sm-hdp">
                        <h5>-HDP-</h5>
                        <strong>-2.50</strong>
                        <ul>
                            <li>1.9</li>
                            <li>1.69</li>
                        </ul>
                        <div class="mt-time">18:00</div>
                    </div>
                    <div class="logo-team">
                        <img src="/images/logo-team/Chelsea.png">
                        <p>CHA</p>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="mt-box">
                    <div class="logo-team">
                        <img src="/images/logo-team/manchester.png">
                        <p>MUT</p>
                    </div>
                    <div class="sm-hdp">
                        <h5>-HDP-</h5>
                        <strong>-2.50</strong>
                        <ul>
                            <li>1.9</li>
                            <li>1.69</li>
                        </ul>
                        <div class="mt-time">18:00</div>
                    </div>
                    <div class="logo-team">
                        <img src="/images/logo-team/Chelsea.png">
                        <p>CHA</p>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="mt-box">
                    <div class="logo-team">
                        <img src="/images/logo-team/manchester.png">
                        <p>MUT</p>
                    </div>
                    <div class="sm-hdp">
                        <h5>-HDP-</h5>
                        <strong>-2.50</strong>
                        <ul>
                            <li>1.9</li>
                            <li>1.69</li>
                        </ul>
                        <div class="mt-time">18:00</div>
                    </div>
                    <div class="logo-team">
                        <img src="/images/logo-team/Chelsea.png">
                        <p>CHA</p>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="mt-box">
                    <div class="logo-team">
                        <img src="/images/logo-team/manchester.png">
                        <p>MUT</p>
                    </div>
                    <div class="sm-hdp">
                        <h5>-HDP-</h5>
                        <strong>-2.50</strong>
                        <ul>
                            <li>1.9</li>
                            <li>1.69</li>
                        </ul>
                        <div class="mt-time">18:00</div>
                    </div>
                    <div class="logo-team">
                        <img src="/images/logo-team/Chelsea.png">
                        <p>CHA</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
    </div>


    <!--                        นับถอยหลัง-->
    <div class="full-width all-content-game" id="time-countdown">

        <div class="full-width topic-section text-center">
            <img src="/images/logo-team/fa-cup-2017.png">
            <h2>Spanish Primera Division</h2>
        </div>
        <div class="full-width topic-section-sec text-center">
            <div id="clockdiv">
                <div class="tm">
                    <span class="days"></span>
                    <div class="smalltext">Days</div>
                </div>
                <div class="tm">
                    <span class="hours"></span>
                    <div class="smalltext">Hours</div>
                </div>
                <div class="tm">
                    <span class="minutes"></span>
                    <div class="smalltext">Minutes</div>
                </div>
                <div class="tm">
                    <span class="seconds"></span>
                    <div class="smalltext">Seconds</div>
                </div>
            </div>
        </div>

        <div class="full-width content-countdown" style="margin-top: 20px;">
            <div class="container">
                <div class="bt-flex">
                    <!--Home-->
                    <div class="home-section-vote">
                        <div class="section-vote pull-right">
                            <div class="bt-vote-home">
                                <div class="box-vote-home" data-toggle="modal" data-target="#votehomeModal">
                                    <div class="logo-vote-home">
                                        <img src="/images/logo-team/manchester.png">
                                    </div>
                                    <span></span>
                                </div>

                            </div>
                        </div>
                        <div class="information-home text-right pull-left">
                            <h4>Manchester United</h4>
                            <!--                                <div class="head-league text-right">-->
                            <!--                                    <h5>Premier League</h5>-->
                            <!--                                    <img src="/images/five-leagues/premier-league-logo.png">-->
                            <!--                                </div>-->


                            <ul class="status-lastMatch">
                                <li>
                                    <div class="draw-status"></div>
                                </li>
                                <li>
                                    <div class="win-status"></div>
                                </li>
                                <li>
                                    <div class="win-status"></div>
                                </li>
                                <li>
                                    <div class="win-status"></div>
                                </li>
                                <li>
                                    <div class="lose-status"></div>
                                </li>
                                <li><p>: Last 5 Match</p></li>
                            </ul>
                            <ul class="vote-player">
                                <li><img src="/images/1.jpg"></li>
                                <li><img src="/images/1.jpg"></li>
                                <li><img src="/images/1.jpg"></li>
                                <li><img src="/images/1.jpg"></li>
                                <li><img src="/images/1.jpg"></li>
                                <li><p>: Player Vote</p></li>
                            </ul>
                            <div class="bt-flex">
                                <h5>60%</h5>
                                <div class="progress">
                                    <div class="progress-bar pull-right" role="progressbar"
                                         aria-valuenow="60"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!--                    HDP-->
                    <div class="hdp-section-vote text-center">
                        <div class="sm-hdp">
                            <h5>-HDP-</h5>
                            <strong>-2.50</strong>
                            <ul>
                                <li>1.9</li>
                                <li>1.69</li>
                            </ul>
                            <div class="mt-time">18:00</div>
                        </div>
                    </div>

                    <!--                    Away-->
                    <div class="away-section-vote">
                        <div class="section-vote pull-left">
                            <div class="bt-vote-away">
                                <div class="box-vote-away" data-toggle="modal" data-target="#voteawayModal">
                                    <div class="logo-vote-away">
                                        <img src="/images/logo-team/Chelsea.png">
                                    </div>
                                    <span></span>
                                </div>

                            </div>
                        </div>
                        <div class="information-away pull-right">
                            <h4>Chelsea</h4>
                            <!--                                <div class="head-league text-left">-->
                            <!--                                    <img src="/images/five-leagues/premier-league-logo.png">-->
                            <!--                                    <h5>Premier League</h5>-->
                            <!--                                </div>-->

                            <ul class="status-lastMatch">
                                <li><p>Last 5 Match :</p></li>
                                <li>
                                    <div class="draw-status"></div>
                                </li>
                                <li>
                                    <div class="win-status"></div>
                                </li>
                                <li>
                                    <div class="win-status"></div>
                                </li>
                                <li>
                                    <div class="win-status"></div>
                                </li>
                                <li>
                                    <div class="lose-status"></div>
                                </li>
                            </ul>
                            <ul class="vote-player">
                                <li><p>Player Vote :</p></li>
                                <li><img src="/images/1.jpg"></li>
                                <li><img src="/images/1.jpg"></li>
                                <li><img src="/images/1.jpg"></li>
                                <li><img src="/images/1.jpg"></li>
                                <li><img src="/images/1.jpg"></li>

                            </ul>
                            <div class="bt-flex">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-danger" role="progressbar"
                                         aria-valuenow="60"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 13%;">
                                    </div>
                                </div>
                                <h5>13%</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!--        เช็คสถิติคู่บอล-->
        <div class="text-center full-width">
            <a class="btn btn-default" href="/" role="button"><strong><i class="fa fa-area-chart" aria-hidden="true"></i> เช็คสถิติคู่บอล</strong></a>
        </div>
        <!--        ทีเด็ดทรรศนะ-->
        <div class="full-width game-livescore-panel">
            <div class="panel-body">
                <div class="container">
                    <div class="full-width">
                        <div class="title-mini-center">
                            <h3>ทีเด็ด - ทรรศนะ</h3>
                        </div>
                    </div>
                    <div class="home-panel-body pull-left">
                        <div class="content-home-review full-width pull-right">
                            <div class="pull-right">
                                <div class="area-logoName">
                                    <div class="pull-right logo-review-user">
                                        <img src="/images/profess-player/sample-logo01.png">
                                    </div>
                                    <div class="pull-right">
                                        <h4 class="professional-user">LOMTOE.NET</h4>
                                        <ul>
                                            <li>@manchester</li>
                                            <li><img src="/images/logo-team/manchester.png" <="" li="">
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right text-review-content text-right">
                                <p>
                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                </p>
                            </div>
                            <div class="pull-left ct-winrate">
                                <div>
                                    <h3>28%</h3>
                                    <small>Win rate</small>
                                </div>
                                <div>
                                    <h3>30</h3>
                                    <small>Days</small>
                                </div>
                            </div>
                        </div>
                        <div class="content-home-review full-width pull-right">
                            <div class="pull-right">
                                <div class="area-logoName">
                                    <div class="pull-right logo-review-user">
                                        <img src="/images/profess-player/sample-logo05.png">
                                    </div>
                                    <div class="pull-right">
                                        <h4 class="professional-user">SMM SPORT</h4>
                                        <ul>
                                            <li>@manchester</li>
                                            <li><img src="/images/logo-team/manchester.png" <="" li="">
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right text-review-content text-right">
                                <p>
                                    อาจจะฝืดๆหน่อย​ แต่ต้องสอย​ อย่าให้เสียชื่อทีมใหญ่
                                </p>
                            </div>
                            <div class="pull-left ct-winrate">
                                <div>
                                    <h3>28%</h3>
                                    <small>Win rate</small>
                                </div>
                                <div>
                                    <h3>30</h3>
                                    <small>Days</small>
                                </div>
                            </div>
                        </div>
                        <div class="content-home-review full-width pull-right">
                            <div class="pull-right">
                                <div class="area-logoName">
                                    <div class="pull-right logo-review-user">
                                        <img src="/images/9.jpg">
                                    </div>
                                    <div class="pull-right">
                                        <h4 class="general-user">หมูตัวนี้ตัวใหญ่ ล้มดังสุดๆ</h4>
                                        <ul>
                                            <li>@manchester</li>
                                            <li><img src="/images/logo-team/manchester.png"></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right text-review-content text-right">
                                <p>
                                    ชอบความคมของตรานกแก้ว ซวยแล้วบายาโดลิด
                                </p>
                            </div>
                            <div class="pull-left ct-winrate">
                                <div>
                                    <h3>28%</h3>
                                    <small>Win rate</small>
                                </div>
                                <div>
                                    <h3>30</h3>
                                    <small>Days</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="away-panel-body pull-right">
                        <div class="content-away-review full-width pull-left">
                            <div class="pull-left">
                                <div class="area-logoName area-logoName-away">
                                    <div class="pull-left logo-review-user">
                                        <img src="/images/profess-player/sample-logo12.png">
                                    </div>
                                    <div class="pull-left">
                                        <h4 class="professional-user">GOAL.IN.TH</h4>
                                        <ul>
                                            <li><img src="/images/logo-team/Chelsea.png" <="" li="">
                                            </li>
                                            <li>@Chelsea</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-left text-review-content text-left">
                                <p>
                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                </p>
                            </div>
                            <div class="pull-right ct-winrate">
                                <div>
                                    <h3>28%</h3>
                                    <small>Win rate</small>
                                </div>
                                <div>
                                    <h3>30</h3>
                                    <small>Days</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="full-width text-center">
                        <a class="btn btn-default btn-sm" href="#" role="button">อ่านทีเด็ด - ทรรศนะทั้งหมด</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!--                        Full Time-->
    <div class="full-width all-content-game" style="display: none;" id="time-fulltime">
        <div class="full-width topic-section text-center" style="margin-top: 15px;">
            <img src="/images/logo-team/fa-cup-2017.png">
            <h2>FA Cup</h2>
        </div>
        <div class="full-width text-center">
            <h3><span class="label label-default">Full Time</span></h3>
        </div>

        <div class="full-width content-Fulltime" style="margin-top: 20px;">
            <div class="bt-flex">
                <!--Home-->
                <div class="home-section-vote">
                    <div class="section-vote pull-right">
                        <div class="bt-vote-home">
                            <div class="active ft-box-vote-home">
                                <div class="logo-vote-home">
                                    <img src="/images/logo-team/manchester.png">
                                </div>
                                <span></span>
                            </div>

                        </div>
                    </div>
                    <div class="information-home text-right pull-left">
                        <h4>Manchester United</h4>
                        <!--                                <div class="head-league text-right">-->
                        <!--                                    <h5>Premier League</h5>-->
                        <!--                                    <img src="/images/five-leagues/premier-league-logo.png">-->
                        <!--                                </div>-->


                        <ul class="status-lastMatch">
                            <li>
                                <div class="draw-status"></div>
                            </li>
                            <li>
                                <div class="win-status"></div>
                            </li>
                            <li>
                                <div class="win-status"></div>
                            </li>
                            <li>
                                <div class="win-status"></div>
                            </li>
                            <li>
                                <div class="lose-status"></div>
                            </li>
                            <li><p>: Last 5 Match</p></li>
                        </ul>
                        <ul class="vote-player">
                            <li><img src="/images/1.jpg"></li>
                            <li><img src="/images/1.jpg"></li>
                            <li><img src="/images/1.jpg"></li>
                            <li><img src="/images/1.jpg"></li>
                            <li><img src="/images/1.jpg"></li>
                            <li><p>: Player Vote</p></li>
                        </ul>
                        <div class="bt-flex">
                            <h5>60%</h5>
                            <div class="progress">
                                <div class="progress-bar pull-right" role="progressbar"
                                     aria-valuenow="60"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!--                    HDP-->
                <div class="score-full-match text-center">
                    <ul>
                        <li><h3>0</h3></li>
                        <li><h3>-</h3></li>
                        <li><h3>2</h3></li>
                    </ul>
                    <ul>
                        <li>1.92</li>
                        <li><strong>-2.50</strong></li>
                        <li>1.50</li>
                    </ul>
                </div>

                <!--                    Away-->
                <div class="away-section-vote">
                    <div class="section-vote pull-left">
                        <div class="bt-vote-away">
                            <div class="ft-box-vote-away">
                                <div class="logo-vote-away">
                                    <img src="/images/logo-team/Chelsea.png">
                                </div>
                                <span></span>
                            </div>

                        </div>
                    </div>
                    <div class="information-away pull-right">
                        <h4>Chelsea</h4>
                        <!--                                <div class="head-league text-left">-->
                        <!--                                    <img src="/images/five-leagues/premier-league-logo.png">-->
                        <!--                                    <h5>Premier League</h5>-->
                        <!--                                </div>-->

                        <ul class="status-lastMatch">
                            <li><p>Last 5 Match :</p></li>
                            <li>
                                <div class="draw-status"></div>
                            </li>
                            <li>
                                <div class="win-status"></div>
                            </li>
                            <li>
                                <div class="win-status"></div>
                            </li>
                            <li>
                                <div class="win-status"></div>
                            </li>
                            <li>
                                <div class="lose-status"></div>
                            </li>
                        </ul>
                        <ul class="vote-player">
                            <li><p>Player Vote :</p></li>
                            <li><img src="/images/1.jpg"></li>
                            <li><img src="/images/1.jpg"></li>
                            <li><img src="/images/1.jpg"></li>
                            <li><img src="/images/1.jpg"></li>
                            <li><img src="/images/1.jpg"></li>

                        </ul>
                        <div class="bt-flex">
                            <div class="progress">
                                <div class="progress-bar progress-bar-danger" role="progressbar"
                                     aria-valuenow="60"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 13%;">
                                </div>
                            </div>
                            <h5>13%</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--        เช็คสถิติคู่บอล-->
        <div class="text-center full-width">
            <a class="btn btn-default" href="/" role="button"><strong><i class="fa fa-area-chart" aria-hidden="true"></i> เช็คสถิติคู่บอล</strong></a>
        </div>

        <!--        ทีเด็ดทรรศนะ-->
        <div class="full-width game-livescore-panel">
            <div class="panel-body">
                <div class="container">
                    <div class="full-width">
                        <div class="title-mini-center">
                            <h3>ทีเด็ด - ทรรศนะ</h3>
                        </div>
                    </div>
                    <div class="home-panel-body pull-left">
                        <div class="content-home-review full-width pull-right">
                            <div class="pull-right">
                                <div class="area-logoName">
                                    <div class="pull-right logo-review-user">
                                        <img src="/images/profess-player/sample-logo01.png">
                                    </div>
                                    <div class="pull-right">
                                        <h4 class="professional-user">LOMTOE.NET</h4>
                                        <ul>
                                            <li>@manchester</li>
                                            <li><img src="/images/logo-team/manchester.png" <="" li="">
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right text-review-content text-right">
                                <p>
                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                </p>
                            </div>
                            <div class="pull-left ct-winrate">
                                <div>
                                    <h3>28%</h3>
                                    <small>Win rate</small>
                                </div>
                                <div>
                                    <h3>30</h3>
                                    <small>Days</small>
                                </div>
                            </div>
                        </div>
                        <div class="content-home-review full-width pull-right">
                            <div class="pull-right">
                                <div class="area-logoName">
                                    <div class="pull-right logo-review-user">
                                        <img src="/images/profess-player/sample-logo05.png">
                                    </div>
                                    <div class="pull-right">
                                        <h4 class="professional-user">SMM SPORT</h4>
                                        <ul>
                                            <li>@manchester</li>
                                            <li><img src="/images/logo-team/manchester.png" <="" li="">
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right text-review-content text-right">
                                <p>
                                    อาจจะฝืดๆหน่อย​ แต่ต้องสอย​ อย่าให้เสียชื่อทีมใหญ่
                                </p>
                            </div>
                            <div class="pull-left ct-winrate">
                                <div>
                                    <h3>28%</h3>
                                    <small>Win rate</small>
                                </div>
                                <div>
                                    <h3>30</h3>
                                    <small>Days</small>
                                </div>
                            </div>
                        </div>
                        <div class="content-home-review full-width pull-right">
                            <div class="pull-right">
                                <div class="area-logoName">
                                    <div class="pull-right logo-review-user">
                                        <img src="/images/9.jpg">
                                    </div>
                                    <div class="pull-right">
                                        <h4 class="general-user">หมูตัวนี้ตัวใหญ่ ล้มดังสุดๆ</h4>
                                        <ul>
                                            <li>@manchester</li>
                                            <li><img src="/images/logo-team/manchester.png"></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right text-review-content text-right">
                                <p>
                                    ชอบความคมของตรานกแก้ว ซวยแล้วบายาโดลิด
                                </p>
                            </div>
                            <div class="pull-left ct-winrate">
                                <div>
                                    <h3>28%</h3>
                                    <small>Win rate</small>
                                </div>
                                <div>
                                    <h3>30</h3>
                                    <small>Days</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="away-panel-body pull-right">
                        <div class="content-away-review full-width pull-left">
                            <div class="pull-left">
                                <div class="area-logoName area-logoName-away">
                                    <div class="pull-left logo-review-user">
                                        <img src="/images/profess-player/sample-logo12.png">
                                    </div>
                                    <div class="pull-left">
                                        <h4 class="professional-user">GOAL.IN.TH</h4>
                                        <ul>
                                            <li><img src="/images/logo-team/Chelsea.png" <="" li="">
                                            </li>
                                            <li>@Chelsea</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-left text-review-content text-left">
                                <p>
                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                </p>
                            </div>
                            <div class="pull-right ct-winrate">
                                <div>
                                    <h3>28%</h3>
                                    <small>Win rate</small>
                                </div>
                                <div>
                                    <h3>30</h3>
                                    <small>Days</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="full-width text-center">
                        <a class="btn btn-default btn-sm" href="#" role="button">อ่านทีเด็ด - ทรรศนะทั้งหมด</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--                        Play Live-->
    <div class="full-width all-content-game" style="display: none;" id="time-live">
        <div class="full-width topic-section text-center">
            <img src="/images/five-leagues/seriea1.png">
            <h2>Italy Serie A</h2>
        </div>
        <div class="full-width text-center mt-box-live">
            <h3><span class="label label-danger">Live</span></h3>
        </div>
        <div class="full-width content-countdown" style="margin-top: 20px;">
            <div class="bt-flex">
                <!--Home-->
                <div class="home-section-vote">
                    <div class="section-vote pull-right">
                        <div class="bt-vote-home">
                            <div class="box-vote-home">
                                <div class="logo-vote-home">
                                    <img src="/images/logo-team/manchester.png">
                                </div>
                                <span></span>
                            </div>

                        </div>
                    </div>
                    <div class="information-home text-right pull-left">
                        <h4>Manchester United</h4>
                        <!--                                <div class="head-league text-right">-->
                        <!--                                    <h5>Premier League</h5>-->
                        <!--                                    <img src="/images/five-leagues/premier-league-logo.png">-->
                        <!--                                </div>-->


                        <ul class="status-lastMatch">
                            <li>
                                <div class="draw-status"></div>
                            </li>
                            <li>
                                <div class="win-status"></div>
                            </li>
                            <li>
                                <div class="win-status"></div>
                            </li>
                            <li>
                                <div class="win-status"></div>
                            </li>
                            <li>
                                <div class="lose-status"></div>
                            </li>
                            <li><p>: Last 5 Match</p></li>
                        </ul>
                        <ul class="vote-player">
                            <li><img src="/images/1.jpg"></li>
                            <li><img src="/images/1.jpg"></li>
                            <li><img src="/images/1.jpg"></li>
                            <li><img src="/images/1.jpg"></li>
                            <li><img src="/images/1.jpg"></li>
                            <li><p>: Player Vote</p></li>
                        </ul>
                        <div class="bt-flex">
                            <h5>60%</h5>
                            <div class="progress">
                                <div class="progress-bar pull-right" role="progressbar"
                                     aria-valuenow="60"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!--                    HDP-->
                <div class="hdp-section-vote text-center">
                    <div class="time-live-match text-center">
                        <div class="hdp-float live-float">
                            <ul>
                                <li>2</li>
                                <li>
                                    <div class="progress" data-percentage="40">
				                            <span class="progress-left">
                                                <span class="progress-bar"></span>
				                            </span>
                                        <span class="progress-right">
                                                <span class="progress-bar"></span>
                                            </span>
                                        <div class="progress-value">
                                            <div>45+</div>
                                        </div>
                                    </div>
                                </li>
                                <li>1</li>
                            </ul>
                        </div>
                        <div class="hdp-float-live">
                            <ul>
                                <li>1.92</li>
                                <li><strong>-2.50</strong></li>
                                <li>1.50</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <!--                    Away-->
                <div class="away-section-vote">
                    <div class="section-vote pull-left">
                        <div class="bt-vote-away">
                            <div class="box-vote-away">
                                <div class="logo-vote-away">
                                    <img src="/images/logo-team/Chelsea.png">
                                </div>
                                <span></span>
                            </div>

                        </div>
                    </div>
                    <div class="information-away pull-right">
                        <h4>Chelsea</h4>
                        <!--                                <div class="head-league text-left">-->
                        <!--                                    <img src="/images/five-leagues/premier-league-logo.png">-->
                        <!--                                    <h5>Premier League</h5>-->
                        <!--                                </div>-->

                        <ul class="status-lastMatch">
                            <li><p>Last 5 Match :</p></li>
                            <li>
                                <div class="draw-status"></div>
                            </li>
                            <li>
                                <div class="win-status"></div>
                            </li>
                            <li>
                                <div class="win-status"></div>
                            </li>
                            <li>
                                <div class="win-status"></div>
                            </li>
                            <li>
                                <div class="lose-status"></div>
                            </li>
                        </ul>
                        <ul class="vote-player">
                            <li><p>Player Vote :</p></li>
                            <li><img src="/images/1.jpg"></li>
                            <li><img src="/images/1.jpg"></li>
                            <li><img src="/images/1.jpg"></li>
                            <li><img src="/images/1.jpg"></li>
                            <li><img src="/images/1.jpg"></li>

                        </ul>
                        <div class="bt-flex">
                            <div class="progress">
                                <div class="progress-bar progress-bar-danger" role="progressbar"
                                     aria-valuenow="60"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 13%;">
                                </div>
                            </div>
                            <h5>13%</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--        เช็คสถิติคู่บอล-->
        <div class="text-center full-width">
            <a class="btn btn-default" href="/" role="button"><strong><i class="fa fa-area-chart" aria-hidden="true"></i> เช็คสถิติคู่บอล</strong></a>
        </div>
        <!--        ทีเด็ดทรรศนะ-->
        <div class="full-width game-livescore-panel">
            <div class="panel-body">
                <div class="container">
                    <div class="full-width">
                        <div class="title-mini-center">
                            <h3>ทีเด็ด - ทรรศนะ</h3>
                        </div>
                    </div>
                    <div class="home-panel-body pull-left">
                        <div class="content-home-review full-width pull-right">
                            <div class="pull-right">
                                <div class="area-logoName">
                                    <div class="pull-right logo-review-user">
                                        <img src="/images/profess-player/sample-logo01.png">
                                    </div>
                                    <div class="pull-right">
                                        <h4 class="professional-user">LOMTOE.NET</h4>
                                        <ul>
                                            <li>@manchester</li>
                                            <li><img src="/images/logo-team/manchester.png" <="" li="">
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right text-review-content text-right">
                                <p>
                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                </p>
                            </div>
                            <div class="pull-left ct-winrate">
                                <div>
                                    <h3>28%</h3>
                                    <small>Win rate</small>
                                </div>
                                <div>
                                    <h3>30</h3>
                                    <small>Days</small>
                                </div>
                            </div>
                        </div>
                        <div class="content-home-review full-width pull-right">
                            <div class="pull-right">
                                <div class="area-logoName">
                                    <div class="pull-right logo-review-user">
                                        <img src="/images/profess-player/sample-logo05.png">
                                    </div>
                                    <div class="pull-right">
                                        <h4 class="professional-user">SMM SPORT</h4>
                                        <ul>
                                            <li>@manchester</li>
                                            <li><img src="/images/logo-team/manchester.png" <="" li="">
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right text-review-content text-right">
                                <p>
                                    อาจจะฝืดๆหน่อย​ แต่ต้องสอย​ อย่าให้เสียชื่อทีมใหญ่
                                </p>
                            </div>
                            <div class="pull-left ct-winrate">
                                <div>
                                    <h3>28%</h3>
                                    <small>Win rate</small>
                                </div>
                                <div>
                                    <h3>30</h3>
                                    <small>Days</small>
                                </div>
                            </div>
                        </div>
                        <div class="content-home-review full-width pull-right">
                            <div class="pull-right">
                                <div class="area-logoName">
                                    <div class="pull-right logo-review-user">
                                        <img src="/images/9.jpg">
                                    </div>
                                    <div class="pull-right">
                                        <h4 class="general-user">หมูตัวนี้ตัวใหญ่ ล้มดังสุดๆ</h4>
                                        <ul>
                                            <li>@manchester</li>
                                            <li><img src="/images/logo-team/manchester.png"></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right text-review-content text-right">
                                <p>
                                    ชอบความคมของตรานกแก้ว ซวยแล้วบายาโดลิด
                                </p>
                            </div>
                            <div class="pull-left ct-winrate">
                                <div>
                                    <h3>28%</h3>
                                    <small>Win rate</small>
                                </div>
                                <div>
                                    <h3>30</h3>
                                    <small>Days</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="away-panel-body pull-right">
                        <div class="content-away-review full-width pull-left">
                            <div class="pull-left">
                                <div class="area-logoName area-logoName-away">
                                    <div class="pull-left logo-review-user">
                                        <img src="/images/profess-player/sample-logo12.png">
                                    </div>
                                    <div class="pull-left">
                                        <h4 class="professional-user">GOAL.IN.TH</h4>
                                        <ul>
                                            <li><img src="/images/logo-team/Chelsea.png" <="" li="">
                                            </li>
                                            <li>@Chelsea</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-left text-review-content text-left">
                                <p>
                                    อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                    นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                </p>
                            </div>
                            <div class="pull-right ct-winrate">
                                <div>
                                    <h3>28%</h3>
                                    <small>Win rate</small>
                                </div>
                                <div>
                                    <h3>30</h3>
                                    <small>Days</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="full-width text-center">
                        <a class="btn btn-default btn-sm" href="#" role="button">อ่านทีเด็ด - ทรรศนะทั้งหมด</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<!--Vote Home-->
<div class="modal fade modal-vote-home" id="votehomeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="left: 0;">
        <div class="full-width title-sidebar-vote text-center">
            <h4>คุณเลือกทีม</h4>
        </div>
        <div class="full-width header-sidebar-vote text-center">

        </div>
        <div class="full-width header-sidebar-vote text-center">
            <img src="/images/logo-team/manchester.png">
            <h3>Manchester United</h3>
        </div>
        <div class="full-width text-center">
            <div class="col-md-12">
                <textarea class="form-control" rows="3" placeholder="เขียนทรรศนะ"></textarea>
            </div>

        </div>
        <div class="full-width content-section-coin text-center">
            <h5>" เลือกจำนวน SCoin ที่ใช้ในการทายผล "</h5>
            <ul>
                <li id="about-link" class="current"></li>
                <li id="about-link"></li>
                <li id="about-link"></li>
                <li id="about-link"></li>
            </ul>
        </div>
        <div class="full-width text-center">
            <button type="submit" class="btn btn-default btn-lg">PLAY</button>
        </div>
    </div><!-- /.modal-dialog -->
</div>
<!--Vote Home-->
<div class="modal fade modal-vote-away" id="voteawayModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="full-width title-sidebar-vote text-center">
            <h4>คุณเลือกทีม</h4>
        </div>
        <div class="full-width header-sidebar-vote text-center">

        </div>
        <div class="full-width header-sidebar-vote text-center">
            <img src="/images/logo-team/Chelsea.png">
            <h3>Chelsea</h3>
        </div>
        <div class="full-width text-center">
            <div class="col-md-12">
                <textarea class="form-control" rows="3" placeholder="เขียนทรรศนะ"></textarea>
            </div>

        </div>
        <div class="full-width content-section-coin text-center">
            <h5>" เลือกจำนวน Coin ที่ใช้ในการทายผล "</h5>
            <ul>
                <li id="about-link" class="current"></li>
                <li id="about-link"></li>
                <li id="about-link"></li>
                <li id="about-link"></li>
            </ul>
        </div>
        <div class="full-width text-center">
            <button type="submit" class="btn btn-default btn-lg">PLAY</button>
        </div>
    </div><!-- /.modal-dialog -->
</div>

<!--Select Coin Vote-->
<script>
    $('ul li').click(function () {
        if ($(this).hasClass('current')) {
            $(this).removeClass('current');
        } else {
            $('li.current').removeClass('current');
            $(this).addClass('current');
        }
    });
</script>


<script>
    var divs = ["time-countdown", "time-fulltime", "time-live"];
    var visibleDivId = null;
    function toggleVisibility(divId) {
        if (visibleDivId === divId) {
            //visibleDivId = null;
        } else {
            visibleDivId = divId;
        }
        hideNonVisibleDivs();
    }
    function hideNonVisibleDivs() {
        var i, divId, div;
        for (i = 0; i < divs.length; i++) {
            divId = divs[i];
            div = document.getElementById(divId);
            if (visibleDivId === divId) {
                div.style.display = "block";
            } else {
                div.style.display = "none";
            }
        }
    }
</script>

<script>
    $('.box-vote-home').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $('.box-vote-home.active').removeClass('active');
            $(this).addClass('active');
        }
    });

</script>
<script>
    $('.box-vote-away').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $('.box-vote-away.active').removeClass('active');
            $(this).addClass('active');
        }
    });

</script>

<!--time-->
<script>
    function getTimeRemaining(endtime) {
        var t = Date.parse(endtime) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        var days = Math.floor(t / (1000 * 60 * 60 * 24));
        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        };
    }

    function initializeClock(id, endtime) {
        var clock = document.getElementById(id);
        var daysSpan = clock.querySelector('.days');
        var hoursSpan = clock.querySelector('.hours');
        var minutesSpan = clock.querySelector('.minutes');
        var secondsSpan = clock.querySelector('.seconds');

        function updateClock() {
            var t = getTimeRemaining(endtime);

            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

            if (t.total <= 0) {
                clearInterval(timeinterval);
            }
        }

        updateClock();
        var timeinterval = setInterval(updateClock, 1000);
    }

    var deadline = new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000);
    initializeClock('clockdiv', deadline);
</script>
<!--Active Slide-->
<script>
    $('.mt-box').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $('.mt-box.active').removeClass('active');
            $(this).addClass('active');
        }
    });

</script>

<!--Swiper-->
<script>
    var swiper_game = new Swiper('.swiper_game', {
        slidesPerView: 5,
        spaceBetween: 10,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
</script>

</body>
</html>
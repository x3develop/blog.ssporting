<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playUser.php";
$playUser=new playUser();
$topRankingAll=$playUser->accuracyUserAll();
?>
<div class="clearfix box-pro-view">
    <div class="text-title">
        <h4><i class="fa fa-trophy" aria-hidden="true"></i> Top Professional</h4>
        <div class="pull-right">
            <a href="/ranking.php"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
        </div>
    </div>
    <table class="table" id="table-topProfessional">

    </table>
</div>
<div class="clearfix box-player-view">
    <div class="text-title">
        <h4><i class="fa fa-trophy" aria-hidden="true"></i> Top Players</h4>
        <div class="pull-right">
            <a href="/ranking.php"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
        </div>
    </div>
    <table class="table">
        <tbody>
        <?php foreach ($topRankingAll as $key=>$value) {?>
            <tr>
                <td><?php echo ($key+1)?></td>
                <td><img src="https://graph.facebook.com/v2.8/<?php echo $value["fb_uid"]; ?>/picture?type=large"></td>
                <td><h4><a style="color:#000000;" href="/profile.php?user_id=<?php echo $value['id']; ?>"><?php echo $value['name']; ?></a></h4></td>
                <td><?php echo number_format($value['accuracy'], 2); ?>%</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $.get( "/topProfessional", function( data ) {
        $( "#table-topProfessional" ).html( data );
    });
</script>
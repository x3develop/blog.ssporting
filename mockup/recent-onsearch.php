<!--            Recent-->
<div class="swiper-container swiper2 swiper-highlight swiper-container-recent">
    <div class="text-title">
        <h4>Recent Video</h4>
    </div>
    <div class="swiper-wrapper">
        <div class="swiper-slide box-recent-thumbnail">
            <a href="#"></a>
            <div class="highlight-thumbnail">
                <img src="/images/ca10r_first_1.jpg">
            </div>
            <div class="caption">
                <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                <p>21-11-2018-19</p>
            </div>
        </div>
        <div class="swiper-slide box-recent-thumbnail">
            <a href="#"></a>
            <div class="highlight-thumbnail">
                <img src="/images/ca10r_first_1.jpg">
            </div>
            <div class="caption">
                <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                <p>21-11-2018-19</p>
            </div>
        </div>
        <div class="swiper-slide box-recent-thumbnail">
            <a href="#"></a>
            <div class="highlight-thumbnail">
                <img src="/images/ca10r_first_1.jpg">
            </div>
            <div class="caption">
                <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                <p>21-11-2018-19</p>
            </div>
        </div>
        <div class="swiper-slide box-recent-thumbnail">
            <a href="#"></a>
            <div class="highlight-thumbnail">
                <img src="/images/ca10r_first_1.jpg">
            </div>
            <div class="caption">
                <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                <p>21-11-2018-19</p>
            </div>
        </div>
        <div class="swiper-slide box-recent-thumbnail">
            <a href="#"></a>
            <div class="highlight-thumbnail">
                <img src="/images/ca10r_first_1.jpg">
            </div>
            <div class="caption">
                <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                <p>21-11-2018-19</p>
            </div>
        </div>
        <div class="swiper-slide box-recent-thumbnail">
            <a href="#"></a>
            <div class="highlight-thumbnail">
                <img src="/images/ca10r_first_1.jpg">
            </div>
            <div class="caption">
                <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                <p>21-11-2018-19</p>
            </div>
        </div>

    </div>
    <!-- Add Arrows -->
    <div class="swiper-button-next swiper-button-next-recent"></div>
    <div class="swiper-button-prev swiper-button-prev-recent"></div>
</div>

<!--            ไฮไลท์คู่บอล-->
<div class="swiper-container swiper3 swiper-highlight swiper-container-highlight">
    <div class="text-title">
        <h4>ไฮไลท์คู่บอล</h4>
    </div>
    <div class="swiper-wrapper">
        <div class="swiper-slide box-recent-thumbnail">
            <a href="#"></a>
            <div class="highlight-thumbnail">
                <img src="/images/ca10r_first_1.jpg">
            </div>
            <div class="caption">
                <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                <p>21-11-2018-19</p>
            </div>
        </div>
        <div class="swiper-slide box-recent-thumbnail">
            <a href="#"></a>
            <div class="highlight-thumbnail">
                <img src="/images/ca10r_first_1.jpg">
            </div>
            <div class="caption">
                <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                <p>21-11-2018-19</p>
            </div>
        </div>
        <div class="swiper-slide box-recent-thumbnail">
            <a href="#"></a>
            <div class="highlight-thumbnail">
                <img src="/images/ca10r_first_1.jpg">
            </div>
            <div class="caption">
                <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                <p>21-11-2018-19</p>
            </div>
        </div>
        <div class="swiper-slide box-recent-thumbnail">
            <a href="#"></a>
            <div class="highlight-thumbnail">
                <img src="/images/ca10r_first_1.jpg">
            </div>
            <div class="caption">
                <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                <p>21-11-2018-19</p>
            </div>
        </div>
        <div class="swiper-slide box-recent-thumbnail">
            <a href="#"></a>
            <div class="highlight-thumbnail">
                <img src="/images/ca10r_first_1.jpg">
            </div>
            <div class="caption">
                <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                <p>21-11-2018-19</p>
            </div>
        </div>
        <div class="swiper-slide box-recent-thumbnail">
            <a href="#"></a>
            <div class="highlight-thumbnail">
                <img src="/images/ca10r_first_1.jpg">
            </div>
            <div class="caption">
                <h4>[ไฮไลท์ยิงประตู] Philippines vs Thailand,AFF Suzuki Cup 2018,21-11-2018-19</h4>
                <p>21-11-2018-19</p>
            </div>
        </div>

    </div>
    <!-- Add Arrows -->
    <div class="swiper-button-next swiper-button-next-highlight"></div>
    <div class="swiper-button-prev swiper-button-prev-highlight"></div>
</div>


<html>
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="shortcut icon" type="images/x-icon" href="images/favicon.ico.png">
    <title>บ้านผลบอล | เว็บทายผลบอลอันดับ 1 ของประเทศ ข่าวฟุตบอล ไฮไลท์การแข่งขัน</title>
    <meta name="keywords" content="News Football,Highlight Football,Video Football,Football">
    <meta name="description" content="บ้านผลบอล เว็บทายผลบอลอันดับ 1 ของประเทศ ข่าวฟุตบอล ไฮไลท์การแข่งขัน">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">

    <!--Css-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/review-new-style.css">
    <link rel="stylesheet" href="/css/game-redesign.css">
    <link rel="stylesheet" href="/css/game-redesign-V2.css">
    <link rel="stylesheet" href="/css/new-menu.css">
    <link rel="stylesheet" href="/css/football-symbol.css">
    <link rel="stylesheet" href="/css/program_ball.css">
    <link rel="stylesheet" href="/css/circle-progress-bar.css">
    <!--    <link rel="stylesheet" href="/css/scrollbar.css">-->
    <link rel="stylesheet" href="/css/swiper.min.css">
    <link rel="stylesheet" href="/css/scrollbar.css">

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <!--    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">-->

    <!--js-->

    <script src="/js/swiper.min.js"></script>
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="nanoscrollbar/nanoscrollbar.js"></script>


    <style>
        .container-new-menu .navbar-default.affix {
            position: relative;
        }
    </style>

</head>


<body style="background-color: #fff; height: 100%;" data-spy="scroll" data-target=".navbar" data-offset="50">
<?php include '../mockup/new-menu.php' ?>
<a href="#" id="scroll" style="display: none;"><span></span></a>
<div class="wrapper-game-v2">

    <!--    Sidenav-->
    <nav class="sidenav-collapse nano" data-spy="affix" data-offset-top="60">

        <div class="nano-content">
            <div class="section-top-sr">
                <div class="header-review">
                    <h4>ทีเด็ด - ทรรศนะ</h4>
                </div>
                <div class="wrap-tab-review-side">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="box-home active">
                            <a href="#home_review" aria-controls="home" role="tab" data-toggle="tab">
                                <div class="bg-tab">
                                    <div class="box-home-review text-right">
                                        <div>(15)</div>
                                        <div class="text-left"><h4>Manchester</h4></div>
                                        <div class="img-team"><img src="/images/logo-team/manchester.png"></div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="box-away" role="presentation">
                            <a href="#away_review" aria-controls="profile" role="tab" data-toggle="tab">
                                <div class="bg-tab">
                                    <div class="box-home-review text-left">
                                        <div class="img-team"><img src="/images/logo-team/Chelsea.png"></div>
                                        <div class="text-right"><h4>Chelsea</h4></div>
                                        <div>(15)</div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="wrap-tab-review-side">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home_review">
                        <div class="review-section">
                            <div class="home-review-section">
                                <div class="box-content-review">
                                    <div class="top-content-review pull-left">

                                        <div class="pro-logo-review">
                                            <img src="/images/sample-logo01.png">
                                        </div>
                                        <div>
                                            <h5>LOMTOE.NET</h5>
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span>@Chelsea FC</span>
                                        </div>
                                        <div class="winRate-box">
                                            <div class="bg-winRate-box">
                                                <h3>20%</h3>
                                                <p>Win Rate</p>
                                            </div>
                                            <div class="bg-day-box">
                                                <h3>30</h3>
                                                <p>Days</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="full-review-caption">
                                        <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                        </p>
                                    </div>
                                </div>
                                <div class="box-content-review">
                                    <div class="top-content-review pull-left">

                                        <div class="pro-logo-review">
                                            <img src="/images/sample-logo01.png">
                                        </div>
                                        <div>
                                            <h5>LOMTOE.NET</h5>
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span>@Chelsea FC</span>
                                        </div>
                                        <div class="winRate-box">
                                            <div class="bg-winRate-box">
                                                <h3>20%</h3>
                                                <p>Win Rate</p>
                                            </div>
                                            <div class="bg-day-box">
                                                <h3>30</h3>
                                                <p>Days</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="full-review-caption">
                                        <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                        </p>
                                    </div>
                                </div>
                                <div class="box-content-review">
                                    <div class="top-content-review pull-left">

                                        <div class="pro-logo-review">
                                            <img src="/images/sample-logo01.png">
                                        </div>
                                        <div>
                                            <h5>LOMTOE.NET</h5>
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span>@Chelsea FC</span>
                                        </div>
                                        <div class="winRate-box">
                                            <div class="bg-winRate-box">
                                                <h3>20%</h3>
                                                <p>Win Rate</p>
                                            </div>
                                            <div class="bg-day-box">
                                                <h3>30</h3>
                                                <p>Days</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="full-review-caption">
                                        <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                        </p>
                                    </div>
                                </div>
                                <div class="box-content-review">
                                    <div class="top-content-review pull-left">

                                        <div class="pro-logo-review">
                                            <img src="/images/sample-logo01.png">
                                        </div>
                                        <div>
                                            <h5>LOMTOE.NET</h5>
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span>@Chelsea FC</span>
                                        </div>
                                        <div class="winRate-box">
                                            <div class="bg-winRate-box">
                                                <h3>20%</h3>
                                                <p>Win Rate</p>
                                            </div>
                                            <div class="bg-day-box">
                                                <h3>30</h3>
                                                <p>Days</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="full-review-caption">
                                        <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                        </p>
                                    </div>
                                </div>
                                <div class="box-content-review">
                                    <div class="top-content-review pull-left">

                                        <div class="pro-logo-review">
                                            <img src="/images/sample-logo01.png">
                                        </div>
                                        <div>
                                            <h5>LOMTOE.NET</h5>
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span>@Chelsea FC</span>
                                        </div>
                                        <div class="winRate-box">
                                            <div class="bg-winRate-box">
                                                <h3>20%</h3>
                                                <p>Win Rate</p>
                                            </div>
                                            <div class="bg-day-box">
                                                <h3>30</h3>
                                                <p>Days</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="full-review-caption">
                                        <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                        </p>
                                    </div>
                                </div>
                                <div class="box-content-review">
                                    <div class="top-content-review pull-left">

                                        <div class="pro-logo-review">
                                            <img src="/images/sample-logo01.png">
                                        </div>
                                        <div>
                                            <h5>LOMTOE.NET</h5>
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span>@Chelsea FC</span>
                                        </div>
                                        <div class="winRate-box">
                                            <div class="bg-winRate-box">
                                                <h3>20%</h3>
                                                <p>Win Rate</p>
                                            </div>
                                            <div class="bg-day-box">
                                                <h3>30</h3>
                                                <p>Days</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="full-review-caption">
                                        <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                        </p>
                                    </div>
                                </div>
                                <div class="box-content-review">
                                    <div class="top-content-review pull-left">

                                        <div class="pro-logo-review">
                                            <img src="/images/sample-logo01.png">
                                        </div>
                                        <div>
                                            <h5>LOMTOE.NET</h5>
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span>@Chelsea FC</span>
                                        </div>
                                        <div class="winRate-box">
                                            <div class="bg-winRate-box">
                                                <h3>20%</h3>
                                                <p>Win Rate</p>
                                            </div>
                                            <div class="bg-day-box">
                                                <h3>30</h3>
                                                <p>Days</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="full-review-caption">
                                        <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                        </p>
                                    </div>
                                </div>
                                <div class="box-content-review">
                                    <div class="top-content-review pull-left">

                                        <div class="pro-logo-review">
                                            <img src="/images/sample-logo01.png">
                                        </div>
                                        <div>
                                            <h5>LOMTOE.NET</h5>
                                            <img src="/images/logo-team/Chelsea.png">
                                            <span>@Chelsea FC</span>
                                        </div>
                                        <div class="winRate-box">
                                            <div class="bg-winRate-box">
                                                <h3>20%</h3>
                                                <p>Win Rate</p>
                                            </div>
                                            <div class="bg-day-box">
                                                <h3>30</h3>
                                                <p>Days</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="full-review-caption">
                                        <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย
                                            จะต้องดวลกับ
                                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3
                                            ส่วนทางฝั่ง
                                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด
                                            ตัวผู้เล่นของ
                                            รัสเซีย
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="away_review">...</div>
                </div>
            </div>
        </div>

        <div class="add-review-bottom">
            <button type="button" class="btn btn-lg btn-block" data-toggle="modal" data-target="#addreviewModal">
                <i class="fa fa-plus" aria-hidden="true"></i> เขียนทรรศนะ
            </button>
        </div>
    </nav>

    <div class="main">
        <div class="content-select-match">
            <div class="main-header pull-left full-width">
                <div class="hamburger" id="hamburger" onclick="toggleSidenav();">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </div>
                <h4>เกมทายผลบอล</h4>
                <span>25 คู่</span>
                <div class="pull-right">
                    <div class="btn-group date-review" data-toggle="buttons">
                        <label class="btn btn-default active">
                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yesterday
                        </label>
                        <label class="btn btn-default">
                            <input type="radio" name="options" id="option2" autocomplete="off"> Today
                        </label>
                        <label class="btn btn-default">
                            <input type="radio" name="options" id="option3" autocomplete="off"> Tomorrow
                        </label>
                    </div>
                </div>
            </div>
            <!-- Swiper -->
            <div class="swiper-container full-width">
                <div class="swiper-wrapper" data-toggle="buttons">
                    <div class="swiper-slide">
                        <div class="mt-box active" onclick="toggleVisibility('time-countdown');">
                            <div class="logo-team">
                                <img src="/images/logo-team/manchester.png">
                                <p>MUT</p>
                            </div>
                            <div class="sm-hdp">
                                <h5>-HDP-</h5>
                                <strong>-2.50</strong>
                                <ul>
                                    <li>1.9</li>
                                    <li>1.69</li>
                                </ul>
                                <div class="mt-time">18:00</div>
                            </div>
                            <div class="logo-team">
                                <img src="/images/logo-team/Chelsea.png">
                                <p>CHA</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="mt-box mt-box-full-match" onclick="toggleVisibility('time-fulltime');">
                            <div class="logo-team">
                                <div class="score-full-match-home">
                                    <strong>2</strong>
                                </div>
                                <img src="/images/logo-team/manchester.png">
                                <p>MUT</p>
                            </div>
                            <div class="sm-hdp">
                                <h5>-HDP-</h5>
                                <strong>-2.50</strong>
                                <ul>
                                    <li>1.9</li>
                                    <li>1.69</li>
                                </ul>
                                <div class="label">Full Match</div>
                            </div>
                            <div class="logo-team">
                                <div class="score-full-match-away">
                                    <strong>0</strong>
                                </div>
                                <img src="/images/logo-team/Chelsea.png">
                                <p>CHA</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="mt-box mt-box-live" onclick="toggleVisibility('time-live');">
                            <div class="logo-team">
                                <div class="score-full-match-home">
                                    <strong>2</strong>
                                </div>
                                <img src="/images/logo-team/manchester.png">
                                <p>MUT</p>
                            </div>
                            <div class="sm-hdp">
                                <h5>-HDP-</h5>
                                <strong>-2.50</strong>
                                <ul>
                                    <li>1.9</li>
                                    <li>1.69</li>
                                </ul>
                                <div class="label">Live</div>
                            </div>
                            <div class="logo-team">
                                <div class="score-full-match-away">
                                    <strong>0</strong>
                                </div>
                                <img src="/images/logo-team/Chelsea.png">
                                <p>CHA</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="mt-box">
                            <div class="logo-team">
                                <img src="/images/logo-team/manchester.png">
                                <p>MUT</p>
                            </div>
                            <div class="sm-hdp">
                                <h5>-HDP-</h5>
                                <strong>-2.50</strong>
                                <ul>
                                    <li>1.9</li>
                                    <li>1.69</li>
                                </ul>
                                <div class="mt-time">18:00</div>
                            </div>
                            <div class="logo-team">
                                <img src="/images/logo-team/Chelsea.png">
                                <p>CHA</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="mt-box">
                            <div class="logo-team">
                                <img src="/images/logo-team/manchester.png">
                                <p>MUT</p>
                            </div>
                            <div class="sm-hdp">
                                <h5>-HDP-</h5>
                                <strong>-2.50</strong>
                                <ul>
                                    <li>1.9</li>
                                    <li>1.69</li>
                                </ul>
                                <div class="mt-time">18:00</div>
                            </div>
                            <div class="logo-team">
                                <img src="/images/logo-team/Chelsea.png">
                                <p>CHA</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="mt-box">
                            <div class="logo-team">
                                <img src="/images/logo-team/manchester.png">
                                <p>MUT</p>
                            </div>
                            <div class="sm-hdp">
                                <h5>-HDP-</h5>
                                <strong>-2.50</strong>
                                <ul>
                                    <li>1.9</li>
                                    <li>1.69</li>
                                </ul>
                                <div class="mt-time">18:00</div>
                            </div>
                            <div class="logo-team">
                                <img src="/images/logo-team/Chelsea.png">
                                <p>CHA</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="mt-box">
                            <div class="logo-team">
                                <img src="/images/logo-team/manchester.png">
                                <p>MUT</p>
                            </div>
                            <div class="sm-hdp">
                                <h5>-HDP-</h5>
                                <strong>-2.50</strong>
                                <ul>
                                    <li>1.9</li>
                                    <li>1.69</li>
                                </ul>
                                <div class="mt-time">18:00</div>
                            </div>
                            <div class="logo-team">
                                <img src="/images/logo-team/Chelsea.png">
                                <p>CHA</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="mt-box">
                            <div class="logo-team">
                                <img src="/images/logo-team/manchester.png">
                                <p>MUT</p>
                            </div>
                            <div class="sm-hdp">
                                <h5>-HDP-</h5>
                                <strong>-2.50</strong>
                                <ul>
                                    <li>1.9</li>
                                    <li>1.69</li>
                                </ul>
                                <div class="mt-time">18:00</div>
                            </div>
                            <div class="logo-team">
                                <img src="/images/logo-team/Chelsea.png">
                                <p>CHA</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
            </div>


            <!--                        นับถอยหลัง-->
            <div class="full-width all-content-game" id="time-countdown">
                <div class="full-width topic-section text-center">
                    <img src="/images/logo-team/fa-cup-2017.png">
                    <h2>Spanish Primera Division</h2>
                </div>
                <div class="full-width topic-section-sec text-center">
                    <div id="clockdiv">
                        <div class="tm">
                            <span class="days"></span>
                            <div class="smalltext">Days</div>
                        </div>
                        <div class="tm">
                            <span class="hours"></span>
                            <div class="smalltext">Hours</div>
                        </div>
                        <div class="tm">
                            <span class="minutes"></span>
                            <div class="smalltext">Minutes</div>
                        </div>
                        <div class="tm">
                            <span class="seconds"></span>
                            <div class="smalltext">Seconds</div>
                        </div>
                    </div>
                </div>

                <div class="full-width content-countdown" style="margin-top: 20px;">
                    <div class="bt-flex">
                        <!--Home-->
                        <div class="home-section-vote">
                            <div class="section-vote pull-right">
                                <div class="bt-vote-home">
                                    <div class="box-vote-home" data-toggle="modal" data-target="#votehomeModal">
                                        <div class="logo-vote-home">
                                            <img src="/images/logo-team/manchester.png">
                                        </div>
                                        <span></span>
                                    </div>

                                </div>
                            </div>
                            <div class="information-home text-right pull-left">
                                <h4>Manchester United</h4>
                                <!--                                <div class="head-league text-right">-->
                                <!--                                    <h5>Premier League</h5>-->
                                <!--                                    <img src="/images/five-leagues/premier-league-logo.png">-->
                                <!--                                </div>-->


                                <ul class="status-lastMatch">
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                    <li><p>: Last 5 Match</p></li>
                                </ul>
                                <ul class="vote-player">
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><p>: Player Vote</p></li>
                                </ul>
                                <div class="bt-flex">
                                    <h5>60%</h5>
                                    <div class="progress">
                                        <div class="progress-bar pull-right" role="progressbar"
                                             aria-valuenow="60"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--                    HDP-->
                        <div class="hdp-section-vote text-center">
                            <div class="sm-hdp">
                                <h5>-HDP-</h5>
                                <strong>-2.50</strong>
                                <ul>
                                    <li>1.9</li>
                                    <li>1.69</li>
                                </ul>
                                <div class="mt-time">18:00</div>
                            </div>
                        </div>

                        <!--                    Away-->
                        <div class="away-section-vote">
                            <div class="section-vote pull-left">
                                <div class="bt-vote-away">
                                    <div class="box-vote-away" data-toggle="modal" data-target="#voteawayModal">
                                        <div class="logo-vote-away">
                                            <img src="/images/logo-team/Chelsea.png">
                                        </div>
                                        <span></span>
                                    </div>

                                </div>
                            </div>
                            <div class="information-away pull-right">
                                <h4>Chelsea</h4>
                                <!--                                <div class="head-league text-left">-->
                                <!--                                    <img src="/images/five-leagues/premier-league-logo.png">-->
                                <!--                                    <h5>Premier League</h5>-->
                                <!--                                </div>-->

                                <ul class="status-lastMatch">
                                    <li><p>Last 5 Match :</p></li>
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                </ul>
                                <ul class="vote-player">
                                    <li><p>Player Vote :</p></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>

                                </ul>
                                <div class="bt-flex">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger" role="progressbar"
                                             aria-valuenow="60"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 13%;">
                                        </div>
                                    </div>
                                    <h5>13%</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!--                        Full Time-->
            <div class="full-width all-content-game" style="display: none;" id="time-fulltime">
                <div class="full-width topic-section text-center" style="margin-top: 15px;">
                    <img src="/images/logo-team/fa-cup-2017.png">
                    <h2>FA Cup</h2>
                </div>
                <div class="full-width text-center">
                    <h3><span class="label label-default">Full Time</span></h3>
                </div>

                <div class="full-width content-Fulltime" style="margin-top: 20px;">
                    <div class="bt-flex">
                        <!--Home-->
                        <div class="home-section-vote">
                            <div class="section-vote pull-right">
                                <div class="bt-vote-home">
                                    <div class="active ft-box-vote-home">
                                        <div class="logo-vote-home">
                                            <img src="/images/logo-team/manchester.png">
                                        </div>
                                        <span></span>
                                    </div>

                                </div>
                            </div>
                            <div class="information-home text-right pull-left">
                                <h4>Manchester United</h4>
                                <!--                                <div class="head-league text-right">-->
                                <!--                                    <h5>Premier League</h5>-->
                                <!--                                    <img src="/images/five-leagues/premier-league-logo.png">-->
                                <!--                                </div>-->


                                <ul class="status-lastMatch">
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                    <li><p>: Last 5 Match</p></li>
                                </ul>
                                <ul class="vote-player">
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><p>: Player Vote</p></li>
                                </ul>
                                <div class="bt-flex">
                                    <h5>60%</h5>
                                    <div class="progress">
                                        <div class="progress-bar pull-right" role="progressbar"
                                             aria-valuenow="60"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--                    HDP-->
                        <div class="score-full-match text-center">
                            <ul>
                                <li><h3>0</h3></li>
                                <li><h3>-</h3></li>
                                <li><h3>2</h3></li>
                            </ul>
                            <ul>
                                <li>1.92</li>
                                <li><strong>-2.50</strong></li>
                                <li>1.50</li>
                            </ul>
                        </div>

                        <!--                    Away-->
                        <div class="away-section-vote">
                            <div class="section-vote pull-left">
                                <div class="bt-vote-away">
                                    <div class="ft-box-vote-away">
                                        <div class="logo-vote-away">
                                            <img src="/images/logo-team/Chelsea.png">
                                        </div>
                                        <span></span>
                                    </div>

                                </div>
                            </div>
                            <div class="information-away pull-right">
                                <h4>Chelsea</h4>
                                <!--                                <div class="head-league text-left">-->
                                <!--                                    <img src="/images/five-leagues/premier-league-logo.png">-->
                                <!--                                    <h5>Premier League</h5>-->
                                <!--                                </div>-->

                                <ul class="status-lastMatch">
                                    <li><p>Last 5 Match :</p></li>
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                </ul>
                                <ul class="vote-player">
                                    <li><p>Player Vote :</p></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>

                                </ul>
                                <div class="bt-flex">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger" role="progressbar"
                                             aria-valuenow="60"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 13%;">
                                        </div>
                                    </div>
                                    <h5>13%</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!--                        Play Live-->
            <div class="full-width all-content-game" style="display: none;" id="time-live">
                <div class="full-width topic-section text-center">
                    <img src="/images/five-leagues/seriea1.png">
                    <h2>Italy Serie A</h2>
                </div>
                <div class="full-width text-center mt-box-live">
                    <h3><span class="label label-danger">Live</span></h3>
                </div>
                <div class="full-width content-countdown" style="margin-top: 20px;">
                    <div class="bt-flex">
                        <!--Home-->
                        <div class="home-section-vote">
                            <div class="section-vote pull-right">
                                <div class="bt-vote-home">
                                    <div class="box-vote-home">
                                        <div class="logo-vote-home">
                                            <img src="/images/logo-team/manchester.png">
                                        </div>
                                        <span></span>
                                    </div>

                                </div>
                            </div>
                            <div class="information-home text-right pull-left">
                                <h4>Manchester United</h4>
                                <!--                                <div class="head-league text-right">-->
                                <!--                                    <h5>Premier League</h5>-->
                                <!--                                    <img src="/images/five-leagues/premier-league-logo.png">-->
                                <!--                                </div>-->


                                <ul class="status-lastMatch">
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                    <li><p>: Last 5 Match</p></li>
                                </ul>
                                <ul class="vote-player">
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><p>: Player Vote</p></li>
                                </ul>
                                <div class="bt-flex">
                                    <h5>60%</h5>
                                    <div class="progress">
                                        <div class="progress-bar pull-right" role="progressbar"
                                             aria-valuenow="60"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--                    HDP-->
                        <div class="hdp-section-vote text-center">
                            <div class="time-live-match text-center">
                                <div class="hdp-float live-float">
                                    <ul>
                                        <li>2</li>
                                        <li>
                                            <div class="progress" data-percentage="40">
				                            <span class="progress-left">
                                                <span class="progress-bar"></span>
				                            </span>
                                                <span class="progress-right">
                                                <span class="progress-bar"></span>
                                            </span>
                                                <div class="progress-value">
                                                    <div>45+</div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>1</li>
                                    </ul>
                                </div>
                                <div class="hdp-float-live">
                                    <ul>
                                        <li>1.92</li>
                                        <li><strong>-2.50</strong></li>
                                        <li>1.50</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!--                    Away-->
                        <div class="away-section-vote">
                            <div class="section-vote pull-left">
                                <div class="bt-vote-away">
                                    <div class="box-vote-away">
                                        <div class="logo-vote-away">
                                            <img src="/images/logo-team/Chelsea.png">
                                        </div>
                                        <span></span>
                                    </div>

                                </div>
                            </div>
                            <div class="information-away pull-right">
                                <h4>Chelsea</h4>
                                <!--                                <div class="head-league text-left">-->
                                <!--                                    <img src="/images/five-leagues/premier-league-logo.png">-->
                                <!--                                    <h5>Premier League</h5>-->
                                <!--                                </div>-->

                                <ul class="status-lastMatch">
                                    <li><p>Last 5 Match :</p></li>
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                </ul>
                                <ul class="vote-player">
                                    <li><p>Player Vote :</p></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>
                                    <li><img src="/images/1.jpg"></li>

                                </ul>
                                <div class="bt-flex">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger" role="progressbar"
                                             aria-valuenow="60"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 13%;">
                                        </div>
                                    </div>
                                    <h5>13%</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>


        <div class="wraper-content-state full-width">
            <nav id="nav" class="navbar full-width" data-spy="affix" data-offset-top="400">
                <div class="nav-home-team pull-left">
                    <img src="/images/logo-team/manchester.png">
                </div>

                <ul class="nav navbar-nav">
                    <li><a href="#section1"><span>Head 2 Head</span></a></li>
                    <li><a href="#section2"><span>Form Guide</span></a></li>
                    <li><a href="#section3"><span>Next Match</span></a></li>
                    <li><a href="#section4"><span>Table</span></a></li>
                </ul>

                <div class="nav-away-team pull-right">
                    <img src="/images/logo-team/Chelsea.png">
                </div>
            </nav>

            <!--            Head To Head-->
            <div id="section1" class="section-state container-fluid">
                <div class="title">
                    <a>Head To Head</a>
                </div>
                <div class="thumbnail content-state content-h2h ">
                    <div class="full-width bt-flex">
                        <div class="content-state-home">
                            <div class="text-title">
                                <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Chatdata Odds/ Result</h4>
                            </div>
                            <!--            กราฟวงกลม-->
                            <div class="section-state-box text-left">
                                <div class="content-section-state">
                                    <div class="full-width bt-flex">
                                        <div class="col-chart">
                                            <div class="box-chart">
                                                <label>ODD</label>
                                                <img src="/images/chart-simple.png">
                                            </div>
                                            <div class="detail-chart">
                                                <div class="col-detail-chart">
                                                    <div class="win-status pull-left"></div>
                                                    <div class="pull-left">
                                                        <strong>41</strong>
                                                        <small>win</small>
                                                    </div>

                                                </div>
                                                <div class="col-detail-chart">
                                                    <div class="draw-status pull-left"></div>
                                                    <div class="pull-left">
                                                        <strong>58</strong>
                                                        <small>draw</small>
                                                    </div>
                                                </div>
                                                <div class="col-detail-chart">
                                                    <div class="lose-status"></div>
                                                    <div class="pull-left">
                                                        <strong>6</strong>
                                                        <small>lose</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-chart">
                                            <div class="box-chart">
                                                <label>Result</label>
                                                <img src="/images/chart-simple.png">
                                            </div>
                                            <div class="detail-chart">
                                                <div class="col-detail-chart">
                                                    <div class="win-status pull-left"></div>
                                                    <div class="pull-left">
                                                        <strong>41</strong>
                                                        <small>win</small>
                                                    </div>

                                                </div>
                                                <div class="col-detail-chart">
                                                    <div class="draw-status pull-left"></div>
                                                    <div class="pull-left">
                                                        <strong>58</strong>
                                                        <small>draw</small>
                                                    </div>
                                                </div>
                                                <div class="col-detail-chart">
                                                    <div class="lose-status"></div>
                                                    <div class="pull-left">
                                                        <strong>6</strong>
                                                        <small>lose</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>
                                </div>
                            </div>

                            <!--                            Last Match-->
                            <div class="text-title">
                                <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Last Match</h4>
                                <div class="btn-group pull-right" style="margin-top: 8px;">
                                    <button type="button" class="btn btn-xs btn-primary dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Last Match (5) <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Last Match (5)</a></li>
                                        <li><a href="#">Last Match (10)</a></li>
                                        <li><a href="#">Last Match (15)</a></li>
                                        <li><a href="#">Last Match (20)</a></li>
                                        <li><a href="#">Last Match (25)</a></li>
                                        <li><a href="#">Last Match (30)</a></li>
                                        <li><a href="#">Last Match (35)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="section-state-box text-left" style="margin-top: 15px;">
                                <div class="content-section-state row">
                                    <div class="box-table-result-state" style="padding: 0 15px;">

                                        <!-- Nav tabs -->
                                        <div class="box-match-list-title-last full-width">
                                            <div class="bh-ctn text-center">
                                                <div class="pull-left form-inline-checkbox" data-toggle="buttons">
                                                    <label class="btn btn-xs btn-default active">
                                                        <input type="checkbox" autocomplete="off" checked=""> All
                                                    </label>
                                                    <label class="btn btn-xs btn-default">
                                                        <input type="checkbox" autocomplete="off"> Premier League
                                                    </label>
                                                    <label class="btn btn-xs btn-default">
                                                        <input type="checkbox" autocomplete="off"> FA Cup
                                                    </label>
                                                    <label class="btn btn-xs btn-default">
                                                        <input type="checkbox" autocomplete="off"> UEFA Champions League
                                                    </label>
                                                    <label class="btn btn-xs btn-default">
                                                        <input type="checkbox" autocomplete="off"> EFL Cup
                                                    </label>


                                                </div>
                                            </div>
                                        </div>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="h2hTotal">
                                                <div class="box-match-tournament">
                                                    <div class="box-match-list-topic">
                                                        <div class="pull-left">
                                                            <img src="/images/logo-team/fa-cup-2017.png">
                                                            <p>FA Cup</p>
                                                        </div>
                                                        <div class="pull-right">
                                                            <ul>
                                                                <li>Odds</li>
                                                                <li>Result</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match team-bet-color"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <div class="win-status"></div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <div class="lose-status"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match team-bet-color"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <div class="draw-status"></div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <div class="win-status"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-match-tournament">
                                                    <div class="box-match-list-topic">
                                                        <div class="pull-left">
                                                            <img src="/images/five-leagues/premier-league-logo.png">
                                                            <p>Premier League</p>
                                                        </div>
                                                        <div class="pull-right">
                                                            <ul>
                                                                <li>Odds</li>
                                                                <li>Result</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <img src="/images/icon-stat/result/0.png">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <img src="/images/icon-stat/result/2.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <img src="/images/icon-stat/result/0.png">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <img src="/images/icon-stat/result/2.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-match-tournament">
                                                    <div class="box-match-list-topic">
                                                        <div class="pull-left">
                                                            <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                                            <p>UEFA Champions League</p>
                                                        </div>
                                                        <div class="pull-right">
                                                            <ul>
                                                                <li>Odds</li>
                                                                <li>Result</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <img src="/images/icon-stat/result/0.png">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <img src="/images/icon-stat/result/2.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <img src="/images/icon-stat/result/0.png">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <img src="/images/icon-stat/result/2.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="h2hHome">...</div>
                                            <div role="tabpanel" class="tab-pane" id="h2hAway">...</div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div style="clear: both"></div>
                        </div>
                        <div class="content-state-away content-state-home">
                            <div class="text-title">
                                <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Chatdata Odds/ Result</h4>
                            </div>
                            <!--            กราฟวงกลม-->
                            <div class="section-state-box text-left">
                                <div class="content-section-state">
                                    <div class="full-width bt-flex">
                                        <div class="col-chart">
                                            <div class="box-chart">
                                                <label>ODD</label>
                                                <img src="/images/chart-simple.png">
                                            </div>
                                            <div class="detail-chart">
                                                <div class="col-detail-chart">
                                                    <div class="win-status pull-left"></div>
                                                    <div class="pull-left">
                                                        <strong>41</strong>
                                                        <small>win</small>
                                                    </div>

                                                </div>
                                                <div class="col-detail-chart">
                                                    <div class="draw-status pull-left"></div>
                                                    <div class="pull-left">
                                                        <strong>58</strong>
                                                        <small>draw</small>
                                                    </div>
                                                </div>
                                                <div class="col-detail-chart">
                                                    <div class="lose-status"></div>
                                                    <div class="pull-left">
                                                        <strong>6</strong>
                                                        <small>lose</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-chart">
                                            <div class="box-chart">
                                                <label>Result</label>
                                                <img src="/images/chart-simple.png">
                                            </div>
                                            <div class="detail-chart">
                                                <div class="col-detail-chart">
                                                    <div class="win-status pull-left"></div>
                                                    <div class="pull-left">
                                                        <strong>41</strong>
                                                        <small>win</small>
                                                    </div>

                                                </div>
                                                <div class="col-detail-chart">
                                                    <div class="draw-status pull-left"></div>
                                                    <div class="pull-left">
                                                        <strong>58</strong>
                                                        <small>draw</small>
                                                    </div>
                                                </div>
                                                <div class="col-detail-chart">
                                                    <div class="lose-status"></div>
                                                    <div class="pull-left">
                                                        <strong>6</strong>
                                                        <small>lose</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>
                                    <div class="col-md-6 hide">
                                        <table>
                                            <tr>
                                                <td>Odds</td>
                                                <td>
                                                    <img src="/images/chart-simple.png">
                                                </td>
                                                <td class="data-chart">
                                                    <div class="clearfix">
                                                        <h4>41.4</h4>
                                                        <i style="color: #0091c8;" class="fa fa-circle"
                                                           aria-hidden="true"></i> Win
                                                    </div>
                                                    <div class="clearfix">
                                                        <h4>58.6</h4>
                                                        <i style="color: #e40520;" class="fa fa-circle"
                                                           aria-hidden="true"></i> Lose
                                                    </div>
                                                    <div class="clearfix">
                                                        <h4>0</h4>
                                                        <i style="color: #aaaaaa;" class="fa fa-circle"
                                                           aria-hidden="true"></i> Draw
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6 hide">
                                        <table>
                                            <tr>
                                                <td>Result</td>
                                                <td>
                                                    <img src="/images/chart-simple.png">
                                                </td>
                                                <td class="data-chart">
                                                    <div class="clearfix">
                                                        <h4>41.4</h4>
                                                        <i style="color: #0091c8;" class="fa fa-circle"
                                                           aria-hidden="true"></i> Win
                                                    </div>
                                                    <div class="clearfix">
                                                        <h4>58.6</h4>
                                                        <i style="color: #e40520;" class="fa fa-circle"
                                                           aria-hidden="true"></i> Lose
                                                    </div>
                                                    <div class="clearfix">
                                                        <h4>0</h4>
                                                        <i style="color: #aaaaaa;" class="fa fa-circle"
                                                           aria-hidden="true"></i> Draw
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!--                            Last Match-->
                            <div class="text-title">
                                <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Last Match</h4>
                                <div class="btn-group pull-right" style="margin-top: 8px;">
                                    <button type="button" class="btn btn-xs btn-primary dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Last Match (5) <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Last Match (5)</a></li>
                                        <li><a href="#">Last Match (10)</a></li>
                                        <li><a href="#">Last Match (15)</a></li>
                                        <li><a href="#">Last Match (20)</a></li>
                                        <li><a href="#">Last Match (25)</a></li>
                                        <li><a href="#">Last Match (30)</a></li>
                                        <li><a href="#">Last Match (35)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="section-state-box text-left" style="margin-top: 15px;">
                                <div class="content-section-state row">
                                    <div class="box-table-result-state" style="padding: 0 15px;">

                                        <!-- Nav tabs -->
                                        <div class="box-match-list-title-last full-width">
                                            <div class="bh-ctn text-center">
                                                <div class="pull-left form-inline-checkbox" data-toggle="buttons">
                                                    <label class="btn btn-xs btn-default active">
                                                        <input type="checkbox" autocomplete="off" checked=""> All
                                                    </label>
                                                    <label class="btn btn-xs btn-default">
                                                        <input type="checkbox" autocomplete="off"> Premier League
                                                    </label>
                                                    <label class="btn btn-xs btn-default">
                                                        <input type="checkbox" autocomplete="off"> FA Cup
                                                    </label>
                                                    <label class="btn btn-xs btn-default">
                                                        <input type="checkbox" autocomplete="off"> UEFA Champions League
                                                    </label>
                                                    <label class="btn btn-xs btn-default">
                                                        <input type="checkbox" autocomplete="off"> EFL Cup
                                                    </label>


                                                </div>
                                            </div>
                                        </div>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="h2hTotal">
                                                <div class="box-match-tournament">
                                                    <div class="box-match-list-topic">
                                                        <div class="pull-left">
                                                            <img src="/images/logo-team/fa-cup-2017.png">
                                                            <p>FA Cup</p>
                                                        </div>
                                                        <div class="pull-right">
                                                            <ul>
                                                                <li>Odds</li>
                                                                <li>Result</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match team-bet-color"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <div class="win-status"></div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <div class="lose-status"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match team-bet-color"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <div class="draw-status"></div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <div class="win-status"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-match-tournament">
                                                    <div class="box-match-list-topic">
                                                        <div class="pull-left">
                                                            <img src="/images/five-leagues/premier-league-logo.png">
                                                            <p>Premier League</p>
                                                        </div>
                                                        <div class="pull-right">
                                                            <ul>
                                                                <li>Odds</li>
                                                                <li>Result</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <img src="/images/icon-stat/result/0.png">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <img src="/images/icon-stat/result/2.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <img src="/images/icon-stat/result/0.png">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <img src="/images/icon-stat/result/2.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-match-tournament">
                                                    <div class="box-match-list-topic">
                                                        <div class="pull-left">
                                                            <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                                            <p>UEFA Champions League</p>
                                                        </div>
                                                        <div class="pull-right">
                                                            <ul>
                                                                <li>Odds</li>
                                                                <li>Result</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <img src="/images/icon-stat/result/0.png">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <img src="/images/icon-stat/result/2.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <img src="/images/icon-stat/result/0.png">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <img src="/images/icon-stat/result/2.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="h2hHome">...</div>
                                            <div role="tabpanel" class="tab-pane" id="h2hAway">...</div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div style="clear: both"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--            Form Guid-->
            <div id="section2" class="section-state container-fluid">
                <div class="title">
                    <a>Form Guid</a>
                </div>
                <div class="thumbnail content-state content-h2h ">
                    <div class="full-width bt-flex">
                        <div class="content-state-home">
                            <div class="text-title">
                                <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Chatdata Odds/ Result</h4>
                            </div>
                            <!--            กราฟวงกลม-->
                            <div class="section-state-box text-left">
                                <div class="content-section-state">
                                    <div class="full-width bt-flex">
                                        <div class="col-chart">
                                            <div class="box-chart">
                                                <label>ODD</label>
                                                <img src="/images/chart-simple.png">
                                            </div>
                                            <div class="detail-chart">
                                                <div class="col-detail-chart">
                                                    <div class="win-status pull-left"></div>
                                                    <div class="pull-left">
                                                        <strong>41</strong>
                                                        <small>win</small>
                                                    </div>

                                                </div>
                                                <div class="col-detail-chart">
                                                    <div class="draw-status pull-left"></div>
                                                    <div class="pull-left">
                                                        <strong>58</strong>
                                                        <small>draw</small>
                                                    </div>
                                                </div>
                                                <div class="col-detail-chart">
                                                    <div class="lose-status"></div>
                                                    <div class="pull-left">
                                                        <strong>6</strong>
                                                        <small>lose</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-chart">
                                            <div class="box-chart">
                                                <label>Result</label>
                                                <img src="/images/chart-simple.png">
                                            </div>
                                            <div class="detail-chart">
                                                <div class="col-detail-chart">
                                                    <div class="win-status pull-left"></div>
                                                    <div class="pull-left">
                                                        <strong>41</strong>
                                                        <small>win</small>
                                                    </div>

                                                </div>
                                                <div class="col-detail-chart">
                                                    <div class="draw-status pull-left"></div>
                                                    <div class="pull-left">
                                                        <strong>58</strong>
                                                        <small>draw</small>
                                                    </div>
                                                </div>
                                                <div class="col-detail-chart">
                                                    <div class="lose-status"></div>
                                                    <div class="pull-left">
                                                        <strong>6</strong>
                                                        <small>lose</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>
                                </div>
                            </div>

                            <!--                            Last Match-->
                            <div class="text-title">
                                <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Last Match</h4>
                                <div class="btn-group pull-right" style="margin-top: 8px;">
                                    <button type="button" class="btn btn-xs btn-primary dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Last Match (5) <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Last Match (5)</a></li>
                                        <li><a href="#">Last Match (10)</a></li>
                                        <li><a href="#">Last Match (15)</a></li>
                                        <li><a href="#">Last Match (20)</a></li>
                                        <li><a href="#">Last Match (25)</a></li>
                                        <li><a href="#">Last Match (30)</a></li>
                                        <li><a href="#">Last Match (35)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="section-state-box text-left" style="margin-top: 15px;">
                                <div class="content-section-state row">
                                    <div class="box-table-result-state" style="padding: 0 15px;">

                                        <!-- Nav tabs -->
                                        <div class="box-match-list-title-last full-width">
                                            <div class="bh-ctn text-center">
                                                <div class="pull-left form-inline-checkbox" data-toggle="buttons">
                                                    <label class="btn btn-xs btn-default active">
                                                        <input type="checkbox" autocomplete="off" checked=""> All
                                                    </label>
                                                    <label class="btn btn-xs btn-default">
                                                        <input type="checkbox" autocomplete="off"> Premier League
                                                    </label>
                                                    <label class="btn btn-xs btn-default">
                                                        <input type="checkbox" autocomplete="off"> FA Cup
                                                    </label>
                                                    <label class="btn btn-xs btn-default">
                                                        <input type="checkbox" autocomplete="off"> UEFA Champions League
                                                    </label>
                                                    <label class="btn btn-xs btn-default">
                                                        <input type="checkbox" autocomplete="off"> EFL Cup
                                                    </label>


                                                </div>
                                            </div>
                                        </div>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="h2hTotal">
                                                <div class="box-match-tournament">
                                                    <div class="box-match-list-topic">
                                                        <div class="pull-left">
                                                            <img src="/images/logo-team/fa-cup-2017.png">
                                                            <p>FA Cup</p>
                                                        </div>
                                                        <div class="pull-right">
                                                            <ul>
                                                                <li>Odds</li>
                                                                <li>Result</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match team-bet-color"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <div class="win-status"></div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <div class="lose-status"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match team-bet-color"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <div class="draw-status"></div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <div class="win-status"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-match-tournament">
                                                    <div class="box-match-list-topic">
                                                        <div class="pull-left">
                                                            <img src="/images/five-leagues/premier-league-logo.png">
                                                            <p>Premier League</p>
                                                        </div>
                                                        <div class="pull-right">
                                                            <ul>
                                                                <li>Odds</li>
                                                                <li>Result</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <img src="/images/icon-stat/result/0.png">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <img src="/images/icon-stat/result/2.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <img src="/images/icon-stat/result/0.png">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <img src="/images/icon-stat/result/2.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-match-tournament">
                                                    <div class="box-match-list-topic">
                                                        <div class="pull-left">
                                                            <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                                            <p>UEFA Champions League</p>
                                                        </div>
                                                        <div class="pull-right">
                                                            <ul>
                                                                <li>Odds</li>
                                                                <li>Result</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <img src="/images/icon-stat/result/0.png">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <img src="/images/icon-stat/result/2.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <img src="/images/icon-stat/result/0.png">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <img src="/images/icon-stat/result/2.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="h2hHome">...</div>
                                            <div role="tabpanel" class="tab-pane" id="h2hAway">...</div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div style="clear: both"></div>
                        </div>
                        <div class="content-state-away content-state-home">
                            <div class="text-title">
                                <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Chatdata Odds/ Result</h4>
                            </div>
                            <!--            กราฟวงกลม-->
                            <div class="section-state-box text-left">
                                <div class="content-section-state">
                                    <div class="full-width bt-flex">
                                        <div class="col-chart">
                                            <div class="box-chart">
                                                <label>ODD</label>
                                                <img src="/images/chart-simple.png">
                                            </div>
                                            <div class="detail-chart">
                                                <div class="col-detail-chart">
                                                    <div class="win-status pull-left"></div>
                                                    <div class="pull-left">
                                                        <strong>41</strong>
                                                        <small>win</small>
                                                    </div>

                                                </div>
                                                <div class="col-detail-chart">
                                                    <div class="draw-status pull-left"></div>
                                                    <div class="pull-left">
                                                        <strong>58</strong>
                                                        <small>draw</small>
                                                    </div>
                                                </div>
                                                <div class="col-detail-chart">
                                                    <div class="lose-status"></div>
                                                    <div class="pull-left">
                                                        <strong>6</strong>
                                                        <small>lose</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-chart">
                                            <div class="box-chart">
                                                <label>Result</label>
                                                <img src="/images/chart-simple.png">
                                            </div>
                                            <div class="detail-chart">
                                                <div class="col-detail-chart">
                                                    <div class="win-status pull-left"></div>
                                                    <div class="pull-left">
                                                        <strong>41</strong>
                                                        <small>win</small>
                                                    </div>

                                                </div>
                                                <div class="col-detail-chart">
                                                    <div class="draw-status pull-left"></div>
                                                    <div class="pull-left">
                                                        <strong>58</strong>
                                                        <small>draw</small>
                                                    </div>
                                                </div>
                                                <div class="col-detail-chart">
                                                    <div class="lose-status"></div>
                                                    <div class="pull-left">
                                                        <strong>6</strong>
                                                        <small>lose</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>
                                    <div class="col-md-6 hide">
                                        <table>
                                            <tr>
                                                <td>Odds</td>
                                                <td>
                                                    <img src="/images/chart-simple.png">
                                                </td>
                                                <td class="data-chart">
                                                    <div class="clearfix">
                                                        <h4>41.4</h4>
                                                        <i style="color: #0091c8;" class="fa fa-circle"
                                                           aria-hidden="true"></i> Win
                                                    </div>
                                                    <div class="clearfix">
                                                        <h4>58.6</h4>
                                                        <i style="color: #e40520;" class="fa fa-circle"
                                                           aria-hidden="true"></i> Lose
                                                    </div>
                                                    <div class="clearfix">
                                                        <h4>0</h4>
                                                        <i style="color: #aaaaaa;" class="fa fa-circle"
                                                           aria-hidden="true"></i> Draw
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6 hide">
                                        <table>
                                            <tr>
                                                <td>Result</td>
                                                <td>
                                                    <img src="/images/chart-simple.png">
                                                </td>
                                                <td class="data-chart">
                                                    <div class="clearfix">
                                                        <h4>41.4</h4>
                                                        <i style="color: #0091c8;" class="fa fa-circle"
                                                           aria-hidden="true"></i> Win
                                                    </div>
                                                    <div class="clearfix">
                                                        <h4>58.6</h4>
                                                        <i style="color: #e40520;" class="fa fa-circle"
                                                           aria-hidden="true"></i> Lose
                                                    </div>
                                                    <div class="clearfix">
                                                        <h4>0</h4>
                                                        <i style="color: #aaaaaa;" class="fa fa-circle"
                                                           aria-hidden="true"></i> Draw
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!--                            Last Match-->
                            <div class="text-title">
                                <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Last Match</h4>
                                <div class="btn-group pull-right" style="margin-top: 8px;">
                                    <button type="button" class="btn btn-xs btn-primary dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Last Match (5) <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Last Match (5)</a></li>
                                        <li><a href="#">Last Match (10)</a></li>
                                        <li><a href="#">Last Match (15)</a></li>
                                        <li><a href="#">Last Match (20)</a></li>
                                        <li><a href="#">Last Match (25)</a></li>
                                        <li><a href="#">Last Match (30)</a></li>
                                        <li><a href="#">Last Match (35)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="section-state-box text-left" style="margin-top: 15px;">
                                <div class="content-section-state row">
                                    <div class="box-table-result-state" style="padding: 0 15px;">

                                        <!-- Nav tabs -->
                                        <div class="box-match-list-title-last full-width">
                                            <div class="bh-ctn text-center">
                                                <div class="pull-left form-inline-checkbox" data-toggle="buttons">
                                                    <label class="btn btn-xs btn-default active">
                                                        <input type="checkbox" autocomplete="off" checked=""> All
                                                    </label>
                                                    <label class="btn btn-xs btn-default">
                                                        <input type="checkbox" autocomplete="off"> Premier League
                                                    </label>
                                                    <label class="btn btn-xs btn-default">
                                                        <input type="checkbox" autocomplete="off"> FA Cup
                                                    </label>
                                                    <label class="btn btn-xs btn-default">
                                                        <input type="checkbox" autocomplete="off"> UEFA Champions League
                                                    </label>
                                                    <label class="btn btn-xs btn-default">
                                                        <input type="checkbox" autocomplete="off"> EFL Cup
                                                    </label>


                                                </div>
                                            </div>
                                        </div>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="h2hTotal">
                                                <div class="box-match-tournament">
                                                    <div class="box-match-list-topic">
                                                        <div class="pull-left">
                                                            <img src="/images/logo-team/fa-cup-2017.png">
                                                            <p>FA Cup</p>
                                                        </div>
                                                        <div class="pull-right">
                                                            <ul>
                                                                <li>Odds</li>
                                                                <li>Result</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match team-bet-color"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <div class="win-status"></div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <div class="lose-status"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match team-bet-color"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <div class="draw-status"></div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <div class="win-status"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-match-tournament">
                                                    <div class="box-match-list-topic">
                                                        <div class="pull-left">
                                                            <img src="/images/five-leagues/premier-league-logo.png">
                                                            <p>Premier League</p>
                                                        </div>
                                                        <div class="pull-right">
                                                            <ul>
                                                                <li>Odds</li>
                                                                <li>Result</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <img src="/images/icon-stat/result/0.png">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <img src="/images/icon-stat/result/2.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <img src="/images/icon-stat/result/0.png">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <img src="/images/icon-stat/result/2.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-match-tournament">
                                                    <div class="box-match-list-topic">
                                                        <div class="pull-left">
                                                            <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                                            <p>UEFA Champions League</p>
                                                        </div>
                                                        <div class="pull-right">
                                                            <ul>
                                                                <li>Odds</li>
                                                                <li>Result</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <img src="/images/icon-stat/result/0.png">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <img src="/images/icon-stat/result/2.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-match-list">

                                                        <div class="text-center">
                                                            <div class="date-match">15 Apr 18</div>
                                                            <div class="time-match">15:00</div>
                                                        </div>
                                                        <div>
                                                            <div class="home-name-match"><img
                                                                        src="/images/logo-team/Chelsea.png">Chelsea
                                                            </div>
                                                            <div class="away-name-match hignlight-team-win"><img
                                                                        src="/images/logo-team/manchester.png">Manchester
                                                                united
                                                            </div>
                                                        </div>
                                                        <div class="match-score text-center">
                                                            <div class="home-score-match">0</div>
                                                            <div class="away-score-match">2</div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="odds-match">
                                                                <img src="/images/icon-stat/result/0.png">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <div class="result-match">
                                                                <img src="/images/icon-stat/result/2.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="h2hHome">...</div>
                                            <div role="tabpanel" class="tab-pane" id="h2hAway">...</div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div style="clear: both"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--            Next Match-->
            <div id="section3" class="section-state container-fluid">

                <div class="title">
                    <a>Next Match</a>
                </div>
                <div class="thumbnail content-state content-h2h ">
                    <div class="full-width bt-flex">
                        <div class="content-state-home">
                            <div class="text-title">
                                <h4><img src="/images/logo-team/manchester.png"> Manchester united</h4>
                            </div>
                            <div class="container-table-last">

                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/logo-team/fa-cup-2017.png">
                                            <p>FA Cup</p>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/five-leagues/premier-league-logo.png">
                                            <p>Premier League</p>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>

                                    </div>
                                </div>
                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                            <p>UEFA Champions League</p>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div style="clear: both"></div>
                        </div>
                        <div class="content-state-away content-state-home">
                            <div class="text-title">
                                <h4><img src="/images/logo-team/Chelsea.png"> Chelsea</h4>
                            </div>
                            <div class="container-table-last">

                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/logo-team/fa-cup-2017.png">
                                            <p>FA Cup</p>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/five-leagues/premier-league-logo.png">
                                            <p>Premier League</p>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>

                                    </div>
                                </div>
                                <div class="box-match-tournament">
                                    <div class="box-match-list-topic">
                                        <div class="pull-left">
                                            <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                            <p>UEFA Champions League</p>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                    <div class="box-match-list">

                                        <div class="text-center">
                                            <div class="date-match">15 Apr 18</div>
                                            <div class="time-match">15:00</div>
                                        </div>
                                        <div>
                                            <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                                            </div>
                                            <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                                                united
                                            </div>
                                        </div>
                                        <div>
                                            <div class="home-score-match">0</div>
                                            <div class="away-score-match">0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="clear: both"></div>
                        </div>
                    </div>
                </div>


            </div>
            <!--            Table-->
            <div id="section4" class="section-state container-fluid">
                <div class="title">
                    <a>Table</a>
                </div>
                <div class="thumbnail content-state">
                    <div class="full-width bt-flex">
                        <div class="content-state-home">
                            <div class="text-title">
                                <h4><img src="/images/logo-team/manchester.png"> Manchester united</h4>
                            </div>
                            <div class="container-table-last">

                                <span class="pull-left"><img src="/images/five-leagues/premier-league-logo.png"> Season 18/19</span>
                                <div class="text-right">
                                    <div class="btn-group" role="group" aria-label="...">
                                        <button type="button" class="btn btn-default btn-xs active">All</button>
                                        <button type="button" class="btn btn-default btn-xs">Home</button>
                                        <button type="button" class="btn btn-default btn-xs">Away</button>
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                                <div class="content-table-standing">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <th></th>
                                            <th>Team</th>
                                            <th>P</th>
                                            <th>GD</th>
                                            <th>PTS</th>
                                        </tr>
                                        <tr class="box-highlight-team">
                                            <td>1</td>
                                            <td>
                                                <img src="/images/logo-team/manchester.png">
                                                <p>Manchester united</p>
                                            </td>
                                            <td>37</td>
                                            <td>22</td>
                                            <td>60</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>
                                                <img src="/images/logo-team/Chelsea.png">
                                                <p>Arsenal</p>
                                            </td>
                                            <td>37</td>
                                            <td>22</td>
                                            <td>60</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>
                                                <img src="/images/logo-team/Chelsea.png">
                                                <p>Chelsea</p>
                                            </td>
                                            <td>37</td>
                                            <td>19</td>
                                            <td>55</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>


                            <div style="clear: both"></div>
                        </div>
                        <div class="content-state-away content-state-home">
                            <div class="text-title">
                                <h4><img src="/images/logo-team/Chelsea.png"> Chelsea</h4>
                            </div>
                            <div class="container-table-last">

                                <span class="pull-left"><img src="/images/five-leagues/premier-league-logo.png"> Season 18/19</span>
                                <div class="text-right">
                                    <div class="btn-group" role="group" aria-label="...">
                                        <button type="button" class="btn btn-default btn-xs active">All</button>
                                        <button type="button" class="btn btn-default btn-xs">Home</button>
                                        <button type="button" class="btn btn-default btn-xs">Away</button>
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                                <div class="content-table-standing">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <th></th>
                                            <th>Team</th>
                                            <th>P</th>
                                            <th>GD</th>
                                            <th>PTS</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <img src="/images/logo-team/manchester.png">
                                                <p>Manchester united</p>
                                            </td>
                                            <td>37</td>
                                            <td>22</td>
                                            <td>60</td>
                                        </tr>
                                        <tr class="box-highlight-team">
                                            <td>2</td>
                                            <td>
                                                <img src="/images/logo-team/Chelsea.png">
                                                <p>Arsenal</p>
                                            </td>
                                            <td>37</td>
                                            <td>22</td>
                                            <td>60</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>
                                                <img src="/images/logo-team/Chelsea.png">
                                                <p>Chelsea</p>
                                            </td>
                                            <td>37</td>
                                            <td>19</td>
                                            <td>55</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                            <div style="clear: both"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
</div>

<!--เพิ่มทีเด็ด - ทรรศนะ-->
<div class="modal fade" id="addreviewModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 400px;" role="document">
        <div class="modal-content">
            <div class="modal-body modal-body-vote">
                <div class="header-review">
                    <h4>เล่นเกม - ทายผล</h4>
                </div>
                <div class="full-width content-countdown">
                    <div class="bt-flex">
                        <!--Home-->
                        <div class="home-section-vote">
                            <div class="section-vote pull-right">
                                <div class="bt-vote-home">
                                    <div class="box-vote-home">
                                        <div class="logo-vote-home">
                                            <img src="/images/logo-team/manchester.png">
                                        </div>
                                        <span></span>
                                    </div>

                                </div>
                            </div>
                            <div class="information-home text-center pull-left">
                                <h4>Manchester United</h4>
                                <ul class="status-lastMatch">
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                    <li><p>Last 5 Match</p></li>
                                </ul>
                            </div>
                        </div>
                        <!--                    HDP-->
                        <div class="hdp-section-vote text-center">
                            <div class="sm-hdp">
                                <strong>-2.50</strong>
                                <ul>
                                    <li>1.9</li>
                                    <li>1.69</li>
                                </ul>
                            </div>
                        </div>

                        <!--                    Away-->
                        <div class="away-section-vote">
                            <div class="section-vote pull-left">
                                <div class="bt-vote-away">
                                    <div class="box-vote-away">
                                        <div class="logo-vote-away">
                                            <img src="/images/logo-team/Chelsea.png">
                                        </div>
                                        <span></span>
                                    </div>

                                </div>
                            </div>
                            <div class="information-away pull-right text-center">
                                <h4>Chelsea</h4>

                                <ul class="status-lastMatch">
                                    <li>
                                        <div class="draw-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="win-status"></div>
                                    </li>
                                    <li>
                                        <div class="lose-status"></div>
                                    </li>
                                    <li><p>Last 5 Match</p></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="full-width content-coin-vote text-center">
                    <h5>" เลือกจำนวน SCoin ที่ใช้ในการทายผล "</h5>
                    <div data-toggle="buttons">
                        <div class="btn active">
                            <input type="radio" name="options" id="option1" autocomplete="off" checked>
                            <h3>200</h3>
                            <p>Scoin</p>
                        </div>
                        <div class="btn">
                            <input type="radio" name="options" id="option2" autocomplete="off">
                            <h3>400</h3>
                            <p>Scoin</p>
                        </div>
                        <div class="btn">
                            <input type="radio" name="options" id="option3" autocomplete="off">
                            <h3>800</h3>
                            <p>Scoin</p>
                        </div>
                        <div class="btn">
                            <input type="radio" name="options" id="option3" autocomplete="off">
                            <h3>1,200</h3>
                            <p>Scoin</p>
                        </div>
                    </div>
                </div>
                <div class="full-width text-center" style="padding: 15px;">
                    <textarea class="form-control" placeholder="เขียนรีวิวทรรศนะ" rows="5"></textarea>
                </div>
                <div class="col-md-12">
                    <div class="pull-right" style="padding: 0 8px;">
                        <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                        <button type="button" class="btn btn-primary">บันทึก</button>
                    </div>
                </div>
                <div style="clear: both"></div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--Vote Home-->
<div class="modal fade modal-vote-home" id="votehomeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="full-width title-sidebar-vote text-center">
            <h4>คุณเลือกทีม</h4>
        </div>
        <div class="full-width header-sidebar-vote text-center">

        </div>
        <div class="full-width header-sidebar-vote text-center">
            <img src="/images/logo-team/manchester.png">
            <h3>Manchester United</h3>
        </div>
        <div class="full-width text-center">
            <div class="col-md-12">
                <textarea class="form-control" rows="3" placeholder="เขียนทรรศนะ"></textarea>
            </div>

        </div>
        <div class="full-width content-section-coin text-center">
            <h5>" เลือกจำนวน SCoin ที่ใช้ในการทายผล "</h5>
            <ul>
                <li id="about-link" class="current"></li>
                <li id="about-link"></li>
                <li id="about-link"></li>
                <li id="about-link"></li>
            </ul>
        </div>
        <div class="full-width text-center">
            <button type="submit" class="btn btn-default btn-lg">PLAY</button>
        </div>
    </div><!-- /.modal-dialog -->
</div>
<!--Vote Home-->
<div class="modal fade modal-vote-away" id="voteawayModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="full-width title-sidebar-vote text-center">
            <h4>คุณเลือกทีม</h4>
        </div>
        <div class="full-width header-sidebar-vote text-center">

        </div>
        <div class="full-width header-sidebar-vote text-center">
            <img src="/images/logo-team/Chelsea.png">
            <h3>Chelsea</h3>
        </div>
        <div class="full-width text-center">
            <div class="col-md-12">
                <textarea class="form-control" rows="3" placeholder="เขียนทรรศนะ"></textarea>
            </div>

        </div>
        <div class="full-width content-section-coin text-center">
            <h5>" เลือกจำนวน Coin ที่ใช้ในการทายผล "</h5>
            <ul>
                <li id="about-link" class="current"></li>
                <li id="about-link"></li>
                <li id="about-link"></li>
                <li id="about-link"></li>
            </ul>
        </div>
        <div class="full-width text-center">
            <button type="submit" class="btn btn-default btn-lg">PLAY</button>
        </div>
    </div><!-- /.modal-dialog -->
</div>

<script>
    $('ul li').click(function () {
        if ($(this).hasClass('current')) {
            $(this).removeClass('current');
        } else {
            $('li.current').removeClass('current');
            $(this).addClass('current');
        }
    });
</script>

<script>
    var divs = ["time-countdown", "time-fulltime", "time-live"];
    var visibleDivId = null;
    function toggleVisibility(divId) {
        if (visibleDivId === divId) {
            //visibleDivId = null;
        } else {
            visibleDivId = divId;
        }
        hideNonVisibleDivs();
    }
    function hideNonVisibleDivs() {
        var i, divId, div;
        for (i = 0; i < divs.length; i++) {
            divId = divs[i];
            div = document.getElementById(divId);
            if (visibleDivId === divId) {
                div.style.display = "block";
            } else {
                div.style.display = "none";
            }
        }
    }
</script>

<!--Smooth Scroll-->
<script>
    $("#nav ul li a[href^='#']").on('click', function (e) {

        // prevent default anchor click behavior
        e.preventDefault();

        // store hash
        var hash = this.hash;

        // animate
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 1000, function () {

            // when done, add hash to url
            // (default click behaviour)
            window.location.hash = hash;
        });

    });
</script>

<script>
    function toggleSidenav() {
        document.body.classList.toggle('sidenav-active');
    }
</script>


<script type="text/javascript">
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#scroll').fadeIn();
            } else {
                $('#scroll').fadeOut();
            }
        });
        $('#scroll').click(function () {
            $("html, body").animate({scrollTop: 0}, 600);
            return false;
        });
    });
</script>
<!--Vote-->
<script>
    $('.mt-box').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $('.mt-box.active').removeClass('active');
            $(this).addClass('active');
        }
    });

</script>
<script>
    $('.box-vote-home').on('click', function () {
        $(this).addClass('active').siblings().removeClass('active');
    });
</script>
<script>
    $('.box-vote-away').on('click', function () {
        $(this).addClass('active').siblings().removeClass('active');
    });
</script>


<!--nanoScroller-->
<script>
    $(".nano").nanoScroller({scroll: 'top'});
</script>

<!--Swiper-->
<script>
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 4,
        spaceBetween: 10,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
</script>


<!--time-->
<script>
    function getTimeRemaining(endtime) {
        var t = Date.parse(endtime) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        var days = Math.floor(t / (1000 * 60 * 60 * 24));
        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        };
    }

    function initializeClock(id, endtime) {
        var clock = document.getElementById(id);
        var daysSpan = clock.querySelector('.days');
        var hoursSpan = clock.querySelector('.hours');
        var minutesSpan = clock.querySelector('.minutes');
        var secondsSpan = clock.querySelector('.seconds');

        function updateClock() {
            var t = getTimeRemaining(endtime);

            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

            if (t.total <= 0) {
                clearInterval(timeinterval);
            }
        }

        updateClock();
        var timeinterval = setInterval(updateClock, 1000);
    }

    var deadline = new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000);
    initializeClock('clockdiv', deadline);
</script>
</body>
</html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="shortcut icon" type="images/x-icon" href="../images/favicon.ico.png">
    <title>ข่าว Ngoal | เว็บทายผลบอลอันดับ 1 ของประเทศ ข่าวฟุตบอล ไฮไลท์การแข่งขัน</title>
    <meta name="keywords" content="News Football,Highlight Football,Video Football,Football">
    <meta name="description" content="บ้านผลบอล เว็บทายผลบอลอันดับ 1 ของประเทศ ข่าวฟุตบอล ไฮไลท์การแข่งขัน">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">

    <!--Css-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/new-menu.css">
    <link rel="stylesheet" href="/css/news-feed-style.css">

    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.nanoscroller/0.8.7/css/nanoscroller.css">

    <!-- <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!--js-->


    <script src="/js/swiper.min.js"></script>

    <script src="/js/jquery/dist/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nanoscroller/0.8.7/javascripts/jquery.nanoscroller.js"></script>


    <style>

    </style>

</head>


<body class="all-container-news" style="background-color: #fff; height: 100%;" data-spy="scroll" data-target=".navbar"
      data-offset="50">
<?php include "../mockup/new-menu.php" ?>
<a href="#" id="scroll" style="display: none;"><span></span></a>
<div class="content-feature-news full-width inline-box">
    <div class="container">
        <div class="row">
            <div class="col-md-3" style="height: 497px; position: relative;">
                <div class="nano">
                    <div class="nano-content">
                        <div class="row-news-feature">
                            <a class="link_wrap" href="#"></a>
                            <div class="thumbnail-news-feature">
                                <div class="crop-thumbnail-news-feature"
                                     style="background-image: url(/images/1.jpg)"></div>
                            </div>
                            <div class="topic-news-feature">
                                <h3>“เมื่อช่วงต้นฤดูกาลเราอาจเป็นเพียงตัวตลก แต่ว่าตอนนี้มันไม่ใช่แล้ว” : ฟิล
                                    โจนส์</h3>
                                <div class="cat-news-feature cat-premier-league">พรีเมียร์ลีก อังกฤษ</div>
                            </div>

                        </div>
                        <div class="row-news-feature">
                            <a class="link_wrap" href="#"></a>
                            <div class="thumbnail-news-feature">
                                <div class="crop-thumbnail-news-feature"
                                     style="background-image: url(/images/2.jpg)"></div>
                            </div>
                            <div class="topic-news-feature">
                                <h3>แมนยูเซ็ง!เรอัลมาดริดเล็งส่งเบลให้เชลซี</h3>
                                <div class="cat-news-feature cat-laliga">ลาลีกา สเปน</div>
                            </div>

                        </div>
                        <div class="row-news-feature">
                            <a class="link_wrap" href="#"></a>
                            <div class="thumbnail-news-feature">
                                <div class="crop-thumbnail-news-feature"
                                     style="background-image: url(/images/3.jpg)"></div>
                            </div>
                            <div class="topic-news-feature">
                                <h3>เนื้อหอมจัด!4ทีมใหญ่สนคว้าอีการ์ดี้ร่วมทัพ</h3>
                                <div class="cat-news-feature cat-serie-a">กัลโช่ เซเรีย อา อิตาลี</div>
                            </div>

                        </div>
                        <div class="row-news-feature">
                            <a class="link_wrap" href="#"></a>
                            <div class="thumbnail-news-feature">
                                <div class="crop-thumbnail-news-feature"
                                     style="background-image: url(/images/4.jpg)"></div>
                            </div>
                            <div class="topic-news-feature">
                                <h3>เปแอสเชแจงเนย์มาร์กลับบราซิลรักษาอาการบาดเจ็บ</h3>
                                <div class="cat-news-feature cat-ligue-1">ลีกเอิง ฝรั่งเศส</div>
                            </div>

                        </div>
                        <div class="row-news-feature">
                            <a class="link_wrap" href="#"></a>
                            <div class="thumbnail-news-feature">
                                <div class="crop-thumbnail-news-feature"
                                     style="background-image: url(/images/1.jpg)"></div>
                            </div>
                            <div class="topic-news-feature">
                                <h3>ฮาเมสแย้มพร้อมกลับเรอัลมาดริดหลังหมดสัญญายืม</h3>
                                <div class="cat-news-feature cat-bundesliga">บุนเดสลีกา เยอรมัน</div>
                            </div>

                        </div>
                        <div class="row-news-feature">
                            <a class="link_wrap" href="#"></a>
                            <div class="thumbnail-news-feature">
                                <div class="crop-thumbnail-news-feature"
                                     style="background-image: url(/images/2.jpg)"></div>
                            </div>
                            <div class="topic-news-feature">
                                <h3>ซาร์รี่เก้าอี้ร้อน! เชลซีปรับทัพส่ง"ชิรูด์"ซัลโวย้ำชัยมัลโม่</h3>
                                <div class="cat-news-feature cat-uefa-europa-league">ยูโรปา ลีก</div>
                            </div>

                        </div>
                        <div class="row-news-feature">
                            <a class="link_wrap" href="#"></a>
                            <div class="thumbnail-news-feature">
                                <div class="crop-thumbnail-news-feature"
                                     style="background-image: url(http://static.siamsport.co.th/size304/2019/02/21/news201902211203807.jpg)"></div>
                            </div>
                            <div class="topic-news-feature">
                                <h3>ลิเวอร์พูลเสียว!เปิดสถิติเจ้าบ้านเจ๊าไร้สกอร์นัดแรกตกรอบบาน</h3>
                                <div class="cat-news-feature cat-uefa-champions-league">ยูฟ่า แชมเปี้ยนส์ลีก</div>
                            </div>

                        </div>
                        <div class="row-news-feature">
                            <a class="link_wrap" href="#"></a>
                            <div class="thumbnail-news-feature">
                                <div class="crop-thumbnail-news-feature"
                                     style="background-image: url(/images/2.jpg)"></div>
                            </div>
                            <div class="topic-news-feature">
                                <h3>ซาร์รี่เก้าอี้ร้อน! เชลซีปรับทัพส่ง"ชิรูด์"ซัลโวย้ำชัยมัลโม่</h3>
                                <div class="cat-news-feature cat-thailand">ข่าวฟุตบอลไทย</div>
                            </div>

                        </div>
                        <div class="row-news-feature">
                            <a class="link_wrap" href="#"></a>
                            <div class="thumbnail-news-feature">
                                <div class="crop-thumbnail-news-feature"
                                     style="background-image: url(http://static.siamsport.co.th/size304/2019/02/19/news201902191132658.jpg)"></div>
                            </div>
                            <div class="topic-news-feature">
                                <h3>ไม่รอด!ทีมเซเรียซีโดนตัดสิทธิ์หลังแพ้20ลูก</h3>
                                <div class="cat-news-feature cat-otherleague">ข่าวอื่นๆ</div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row-news-feature-big row-news-feature">
                    <a class="link_wrap" href="#"></a>
                    <div class="thumbnail-news-feature">
                        <div class="crop-thumbnail-news-feature"
                             style="background-image: url(http://static.siamsport.co.th/news/2019/02/20/news201902201317101.jpg)"></div>
                    </div>
                    <div class="topic-news-feature">
                        <h3>พีพีทีวี สดจอยักษ์! "ศึกแดงเดือด แมตช์ล้างตา" รวมพล ปีศาจแดง และ หงส์แดง หน้าเดอะ สตรีท
                            รัชดา 24 ก.พ.นี้์</h3>
                        <div class="cat-news-feature cat-premier-league">พรีเมียร์ลีก อังกฤษ</div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-all-news">
    <nav id="nav" class="navbar" data-spy="affix" data-offset-top="650">
        <div class="container">
            <ul class="nav navbar-navs text-center inline-box full-width" role="tablist">
                <li>
                    <a href="#section1">
                        <img src="/images/five-leagues/premier-league-logo.png">
                        <span>Premier League</span>
                    </a>
                </li>
                <li>
                    <a href="#section2">
                        <img src="/images/five-leagues/La-Liga-Logo-1.png">
                        <span>LaLiga</span>
                    </a></li>
                <li>
                    <a href="#section3">
                        <img src="/images/five-leagues/seriea1.png">
                        <span>Serie A</span>
                    </a>
                </li>
                <li>
                    <a href="#section4">
                        <img src="/images/five-leagues/logo_bl.gif">
                        <span>Bundesliga</span>
                    </a>
                </li>
                <li>
                    <a href="#section5">
                        <img src="/images/five-leagues/Ligue-1-logo-france.png">
                        <span>Ligue 1</span>
                    </a></li>
                <li>
                    <a href="#section6">
                        <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                        <span>UEFA Champions League</span>
                    </a>
                </li>
                <li>
                    <a href="#section7">
                        <img src="/images/five-leagues/uropa.png">
                        <span>UEFA Europa League</span>
                    </a>
                </li>
                <li>
                    <a href="#section8">
                        <img src="/images/five-leagues/thai-logo.png">
                        <span>ข่าวฟุตบอลไทย</span>
                    </a>
                </li>
                <li>
                    <a href="#section10">
                        <img src="/images/football.png">
                        <span>ข่าวฟุตบอลอื่นๆ</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <div id="section1" class="container-fluid container-fluid-news">

        <div class="container">
            <div class="text-title">
                <h4>พรีเมียร์ลีก อังกฤษ</h4>
            </div>
            <div class="row">
                <div class="col-md-4 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                    <div class="inline-box cat-news-feature cat-premier-league">พรีเมียร์ลีก อังกฤษ</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                    <div class="inline-box cat-news-feature cat-premier-league">พรีเมียร์ลีก อังกฤษ</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                    <div class="inline-box cat-news-feature cat-premier-league">พรีเมียร์ลีก อังกฤษ</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                    <div class="inline-box cat-news-feature cat-premier-league">พรีเมียร์ลีก อังกฤษ</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                    <div class="inline-box cat-news-feature cat-premier-league">พรีเมียร์ลีก อังกฤษ</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                    <div class="inline-box cat-news-feature cat-premier-league">พรีเมียร์ลีก อังกฤษ</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="inline-box cat-news-feature cat-premier-league">พรีเมียร์ลีก อังกฤษ</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row text-center">
                <a class="btn btn-default">อ่านข่าวทั้งหมด</a>
            </div>
        </div>
    </div>
    <div id="section2" class="container-fluid container-fluid-news">
        <div class="container">
            <div class="text-title">
                <h4>ลาลีกา สเปน</h4>
            </div>
            <div class="row">
                <div class="col-md-4 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                    <div class="inline-box cat-news-feature cat-laliga">ลาลีกา สเปน</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                    <div class="inline-box cat-news-feature cat-laliga">ลาลีกา สเปน</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                    <div class="inline-box cat-news-feature cat-laliga">ลาลีกา สเปน</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                    <div class="inline-box cat-news-feature cat-laliga">ลาลีกา สเปน</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                    <div class="inline-box cat-news-feature cat-laliga">ลาลีกา สเปน</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                    <div class="inline-box cat-news-feature cat-laliga">ลาลีกา สเปน</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="inline-box cat-news-feature cat-laliga">ลาลีกา สเปน</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row text-center">
                <a class="btn btn-default">อ่านข่าวทั้งหมด</a>
            </div>
        </div>
    </div>
    <div id="section3" class="container-fluid container-fluid-news">
        <div class="container">
            <div class="text-title">
                <h4>ลาลีกา สเปน</h4>
            </div>
            <div class="row">
                <div class="col-md-4 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                        <div class="cat-news-feature cat-serie-a">กัลโช่ เซเรีย อา อิตาลี</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                        <div class="cat-news-feature cat-serie-a">กัลโช่ เซเรีย อา อิตาลี</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                        <div class="cat-news-feature cat-serie-a">กัลโช่ เซเรีย อา อิตาลี</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                        <div class="cat-news-feature cat-serie-a">กัลโช่ เซเรีย อา อิตาลี</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                        <div class="cat-news-feature cat-serie-a">กัลโช่ เซเรีย อา อิตาลี</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                    <div class="thumbnail inline-box">
                        <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                    </div>
                        <div class="cat-news-feature cat-serie-a">กัลโช่ เซเรีย อา อิตาลี</div>
                    <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-serie-a">กัลโช่ เซเรีย อา อิตาลี</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row text-center">
                <a class="btn btn-default">อ่านข่าวทั้งหมด</a>
            </div>
        </div>
    </div>
    <div id="section4" class="container-fluid container-fluid-news">
        <div class="container">
            <div class="text-title">
                <h4>บุนเดสลีกา เยอรมัน</h4>
            </div>
            <div class="row">
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-bundesliga">บุนเดสลีกา เยอรมัน</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-bundesliga">บุนเดสลีกา เยอรมัน</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-bundesliga">บุนเดสลีกา เยอรมัน</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-bundesliga">บุนเดสลีกา เยอรมัน</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-bundesliga">บุนเดสลีกา เยอรมัน</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-bundesliga">บุนเดสลีกา เยอรมัน</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-bundesliga">บุนเดสลีกา เยอรมัน</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row text-center">
                <a class="btn btn-default">อ่านข่าวทั้งหมด</a>
            </div>
        </div>
    </div>
    <div id="section5" class="container-fluid container-fluid-news">
        <div class="container">
            <div class="text-title">
                <h4>ลีกเอิง ฝรั่งเศส</h4>
            </div>
            <div class="row">
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-ligue-1">ลีกเอิง ฝรั่งเศส</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-ligue-1">ลีกเอิง ฝรั่งเศส</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-ligue-1">ลีกเอิง ฝรั่งเศส</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-ligue-1">ลีกเอิง ฝรั่งเศส</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-ligue-1">ลีกเอิง ฝรั่งเศส</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-ligue-1">ลีกเอิง ฝรั่งเศส</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-ligue-1">ลีกเอิง ฝรั่งเศส</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row text-center">
                <a class="btn btn-default">อ่านข่าวทั้งหมด</a>
            </div>
        </div>
    </div>
    <div id="section6" class="container-fluid container-fluid-news">
        <div class="container">
            <div class="text-title">
                <h4>ยูฟ่า แชมเปี้ยนส์ลีก</h4>
            </div>
            <div class="row">
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-uefa-champions-league">ยูฟ่า แชมเปี้ยนส์ลีก</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-uefa-champions-league">ยูฟ่า แชมเปี้ยนส์ลีก</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-uefa-champions-league">ยูฟ่า แชมเปี้ยนส์ลีก</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-uefa-champions-league">ยูฟ่า แชมเปี้ยนส์ลีก</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-uefa-champions-league">ยูฟ่า แชมเปี้ยนส์ลีก</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-uefa-champions-league">ยูฟ่า แชมเปี้ยนส์ลีก</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-uefa-champions-league">ยูฟ่า แชมเปี้ยนส์ลีก</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row text-center">
                <a class="btn btn-default">อ่านข่าวทั้งหมด</a>
            </div>
        </div>
    </div>
    <div id="section7" class="container-fluid container-fluid-news">
        <div class="container">
            <div class="text-title">
                <h4>ยูโรปา ลีก</h4>
            </div>
            <div class="row">
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-uefa-europa-league">ยูโรปา ลีก</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-uefa-europa-league">ยูโรปา ลีก</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-uefa-europa-league">ยูโรปา ลีก</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-uefa-europa-league">ยูโรปา ลีก</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-uefa-europa-league">ยูโรปา ลีก</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-uefa-europa-league">ยูโรปา ลีก</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-uefa-europa-league">ยูโรปา ลีก</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row text-center">
                <a class="btn btn-default">อ่านข่าวทั้งหมด</a>
            </div>
        </div>
    </div>
    <div id="section8" class="container-fluid container-fluid-news">
        <div class="container">
            <div class="text-title">
                <h4>ฟุตบอลไทย</h4>
            </div>
            <div class="row">
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-thailand">ข่าวฟุตบอลไทย</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-thailand">ข่าวฟุตบอลไทย</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-thailand">ข่าวฟุตบอลไทย</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-thailand">ข่าวฟุตบอลไทย</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-thailand">ข่าวฟุตบอลไทย</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-thailand">ข่าวฟุตบอลไทย</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-thailand">ข่าวฟุตบอลไทย</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row text-center">
                <a class="btn btn-default">อ่านข่าวทั้งหมด</a>
            </div>
        </div>
    </div>
    <div id="section10" class="container-fluid container-fluid-news">
        <div class="container">
            <div class="text-title">
                <h4>ข่าวฟุตบอลอื่นๆ</h4>
            </div>
            <div class="row">
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-otherleague">ข่าวอื่นๆ</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-otherleague">ข่าวอื่นๆ</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-otherleague">ข่าวอื่นๆ</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-otherleague">ข่าวอื่นๆ</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-otherleague">ข่าวอื่นๆ</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-otherleague">ข่าวอื่นๆ</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
                <div class="col-md-3 col-news">
                    <a href="#">
                        <div class="thumbnail inline-box">
                            <div class="crop" style="background-image: url(/images/1.jpg)"></div>
                        </div>
                        <div class="cat-news-feature cat-otherleague">ข่าวอื่นๆ</div>
                        <h4>เป็นลางสังหรณ์!เปเล่โพสต์มั่นใจลิเวอร์พูลซิวแชมป์ลีก</h4>
                        <p>
                            เปเล่ ตำนานดาวยิงชาวบราซิเลียน แสดงความเชื่อว่า ลิเวอร์พูล จะยังคว้าแชมป์ พรีเมียร์ลีก
                            อังกฤษ ในฤดูกาลนี้ไปครองได้อยู่ ถึงแม้ว่าล่าสุด "หงส์แดง" จะหล่นมาเป็นอันดับ 2 แล้วก็ตาม
                        </p>
                    </a>
                </div>
            </div>
            <div class="row text-center">
                <a class="btn btn-default">อ่านข่าวทั้งหมด</a>
            </div>
        </div>
    </div>
</div>





<!--Smooth Scroll-->
<script>


    $(".nano").nanoScroller()
    $(".nano-pane").css("display", "block");
    $(".nano-slider").css("display", "block");


    $("#nav ul li a[href^='#']").on('click', function (e) {

        // prevent default anchor click behavior
        e.preventDefault();

        // store hash
        var hash = this.hash;

        // animate
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 1000, function () {

            // when done, add hash to url
            // (default click behaviour)
            window.location.hash = hash;
        });

    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#scroll').fadeIn();
            } else {
                $('#scroll').fadeOut();
            }
        });
        $('#scroll').click(function () {
            $("html, body").animate({scrollTop: 0}, 600);
            return false;
        });
    });
</script>

</body>
</html>
<div class="container-new-menu">
    <nav class="navbar navbar-default"  data-spy="affix" data-offset-top="60">
            <div id="navbar" class="navbar-collapse collapse">
                <div class="pull-left">
                    <a class="logo-navbar-brand" href="/">
                        <img src="/images/logo/logo-ngoal2.png">
                    </a>
                </div>

                <ul class="nav navbar-nav navbar-nav-menu">
                    <li><a href="/">หน้าแรก</a></li>
                    <li><a href="/">วิดีโอ/ไฮไลท์บอล</a></li>
                    <li><a href="/">บอลต่างประเทศ</a></li>
                    <li><a href="/">ข่าวบอลไทย</a></li>
                    <li><a href="/">Sexy Football</a></li>
                    <li><a href="/">เกมทายผลบอล<span class="badge">4</span></a></li>
                    <li><a href="/">ทีเด็ดทรรศนะ<span class="badge">4</span></a></li>
                </ul>
                <ul class="nav navbar-nav navbar-profile navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <div class="profile-image-menu">
                                <img src="/images/2.jpg">
                            </div>
<!--                            <span class="caret"></span>-->
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="header-menu-dropdown">
                                    <div class="profile-image-menu">
                                        <img src="/images/2.jpg">
                                    </div>
                                    <div class="detail-user">
                                        <h3>Username Lastname</h3>
                                        <span>123@gmail.com</span>
                                    </div>
                                    <div style="clear: both;"></div>
                                    <table>
                                        <tr>
                                            <td>
                                                <img src="/images/coin/scoin100.png">
                                                <p>10,000</p>
                                            </td>
                                            <td>
                                                <img src="/images/coin/sgold100.png">
                                                <p>120,000</p>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </li>
                            <li><a href="#"><i class="fa fa-cogs" aria-hidden="true"></i> ตั้งค่าโปรไฟล์</a></li>
                            <li><a href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i> กติกาและเงื่อนไข</a></li>
                            <li><a href="#"><i class="fa fa-trophy" aria-hidden="true"></i> Ranking</a></li>
                            <li><a href="#"><i class="fa fa-gamepad" aria-hidden="true"></i> วิธีการเล่น</a></li>
                            <li><a href="#"><i class="fa fa-sign-out" aria-hidden="true"></i> ออกจากระบบ</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->

    </nav>

</div>
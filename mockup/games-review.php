<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ngoal | ทีเด็ด ทรรศนะ</title>
    <link rel="shortcut icon" type="images/x-icon" href="/images/favicon.ico.png">
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/doce.css">
    <link rel="stylesheet" href="/css/livescore.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <link rel="stylesheet" href="/css/game-redesign.css">
    <link rel="stylesheet" href="/css/review-new-style.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/readmore.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="js/jquery-timeago/jquery.timeago.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="/js/moment.js"></script>

    <?php include 'view/index/googletagmanager.php'; ?>

<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
</head>
<body style="background-color: #f6f6f6; min-height: 1000px;">
<!--index menu-->
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>

<div class="float-add-review">
    <div class="add-review" <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="เพิ่มทรรศนะ"></button></div>
</div>

<div class="content-livescore-main content-game-redesign">
    <div class="container">
        <div class="row">
            <div class="content-list-livescore">
                <div class="col-md-12" id="liveScoreHead" style="width: 100%;overflow: hidden;height: 104px;">
                    <ul class="" id="ul-liveScoreHead" style="margin: 0px; transform: translateZ(0px); width: 298px;">
                        <li data-key="0" data-matchid="688623" data-timematch="2018-09-26 23:30:00"
                            class="match-slide active-game" style="float: left; width: 297.5px;">
                            <a href="/games.php?mid=688623">
                                <div class="col-md-12">
                                    <div class="thumbnail content-team-slide">
                                        <div class="pull-left">
                                            <div class="content-team-slide-title">
                                                <img src="">
                                                <div class="active">
                                                    <div class="name-team-slide-top">IFK Goteborg</div>
                                                </div>
                                            </div>
                                            <div class="content-team-slide-title">
                                                <img src="">
                                                <div class="">
                                                    <div class="name-team-slide-top">Elfsborg</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-right">
                                            <div class="time-live">
                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                <p>22:30</p>
                                            </div>
                                            <div class="HDP-live">HDP</div>
                                            <div class="num-live">-0.25</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row" id="match-team-4">

            <!--                title-->
            <div class="topic-league">
                <h3><img src="/images/league/1/1_350x350.png"> English Premier League</h3>
                <ul>
                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> 22:00</li>
                    <li>|</li>
                    <li><i class="fa fa-calendar" aria-hidden="true"></i> 26/12/2017</li>
                </ul>
            </div>

            <!--                Vote-->
            <div class="content-select-team-vote">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-xs-6 col-sm-6">
                            <div class="content-team-vote-home">
                                <div class="box-vote-team">
                                    <div class="logo-vote-team-home">
                                        <img src="/images/team/12/12_256x256.png">
                                    </div>
                                    <div class="btn-vote-team-home text-right">
                                        <a class="1">VOTE</a>
                                        <div class="bg-vote-team-home"></div>
                                    </div>
                                </div>
                                <div class="name-team-vote">
                                    <h3>
                                        <a style="color: #333;" class="">Watford</a>
                                    </h3>
                                </div>

                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6">
                            <div class="content-team-vote-away">

                                <div class="box-vote-team">
                                    <div class="logo-vote-team-away">
                                        <img src="/images/team/1/1_256x256.png">
                                    </div>
                                    <div class="btn-vote-team-away">
                                        <a class="1">VOTE</a>
                                        <div class="bg-vote-team-away"></div>
                                    </div>
                                </div>
                                <div class="name-team-vote">
                                    <h3>
                                        <a style="color: #333;" class="">Leicester City</a>
                                    </h3>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--                HDP-->
        <div class="row text-center">
            <ul class="content-HDP-game">
                <li class="box-last-match text-right">
                    <table>
                        <tbody>
                        <tr id="tr-results-left">
                            <td class="btn-results-left" number="3"><i class="fa fa-caret-left" aria-hidden="true"></i>
                            </td>
                            <td team_homer="1" team_home="" scoreaway="0" scorehome="2" number="0"
                                class="hide li-results"><img title="2018-01-20 23:00:00"
                                                             src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td scoreaway="0" scorehome="0" number="0" class="hide li-results"><img
                                        title="2018-02-01 04:00:00" src="/images/icon-stat/betlist_result/draw.png">
                            </td>
                            <td team_homer="12" team_home="" scoreaway="1" scorehome="4" number="0"
                                class="hide li-results"><img title="2018-02-06 04:00:00"
                                                             src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td team_homer="7" team_home="" scoreaway="0" scorehome="2" number="0"
                                class="hide li-results"><img title="2018-02-10 23:00:00"
                                                             src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td team_homer="12" team_home="" scoreaway="0" scorehome="1" number="0"
                                class="hide li-results"><img title="2018-02-25 01:30:00"
                                                             src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td team_homer="12" team_home="" scoreaway="0" scorehome="1" number="1"
                                class="hide li-results"><img title="2018-03-03 23:00:00"
                                                             src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td team_homer="19" team_home="" scoreaway="0" scorehome="3" number="1"
                                class="hide li-results"><img title="2018-03-11 21:30:00"
                                                             src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td team_homer="20" team_home="" scoreaway="0" scorehome="5" number="1"
                                class="hide li-results"><img title="2018-03-18 01:30:00"
                                                             src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td scoreaway="2" scorehome="2" number="1" class="hide li-results"><img
                                        title="2018-03-31 22:00:00" src="/images/icon-stat/betlist_result/draw.png">
                            </td>
                            <td team_homer="12" team_home="" scoreaway="2" scorehome="1" number="1"
                                class="hide li-results"><img title="2018-04-07 22:00:00"
                                                             src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td team_homer="14" team_home="" scoreaway="0" scorehome="1" number="2"
                                class="hide li-results"><img title="2018-04-14 22:00:00"
                                                             src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td scoreaway="0" scorehome="0" number="2" class="hide li-results"><img
                                        title="2018-04-21 22:00:00" src="/images/icon-stat/betlist_result/draw.png">
                            </td>
                            <td team_homer="4" team_home="" scoreaway="0" scorehome="2" number="2"
                                class="hide li-results"><img title="2018-05-01 03:00:00"
                                                             src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td team_homer="12" team_home="" scoreaway="1" scorehome="2" number="2"
                                class="hide li-results"><img title="2018-05-05 22:00:00"
                                                             src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td team_homer="2" team_home="" scoreaway="0" scorehome="1" number="2"
                                class="hide li-results"><img title="2018-05-13 22:00:00"
                                                             src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td team_homer="12" team_home="" scoreaway="0" scorehome="2" number="3" class=" li-results">
                                <img title="2018-08-11 22:00:00" src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td team_homer="3" team_home="" scoreaway="3" scorehome="1" number="3" class=" li-results">
                                <img title="2018-08-19 20:30:00" src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td team_homer="12" team_home="" scoreaway="1" scorehome="2" number="3" class=" li-results">
                                <img title="2018-08-26 20:30:00" src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td team_homer="12" team_home="" scoreaway="1" scorehome="2" number="3" class=" li-results">
                                <img title="2018-09-02 23:00:00" src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td team_homer="12" team_home="" scoreaway="2" scorehome="1" number="3" class=" li-results">
                                <img title="2018-09-16 00:30:00" src="/images/icon-stat/betlist_result/lose.png"></td>
                            <td>Last <span class="total-results">5</span> Match</td>
                        </tr>
                        <tr class="text-right">
                            <td></td>
                            <td><img src="/images/avatar.png"></td>
                            <td><img src="/images/avatar.png"></td>
                            <td><img src="/images/avatar.png"></td>
                            <td><img src="/images/avatar.png"></td>
                            <td><img src="/images/avatar.png"></td>
                            <td>Player</td>
                        </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                        <tr>
                            <td class="text-left"><span>0.00%</span></td>
                            <td>
                                <div class="progress pull-right">
                                    <div class="progress-bar pull-right" role="progressbar" aria-valuenow="0.00"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 0.00%;">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </li>
                <li class="box-HDP-game text-center">
                    <table>
                        <tbody>
                        <tr>
                            <td class="text-right"><p></p></td>
                            <td><h2>[0]</h2></td>
                            <td class="text-left"><p></p></td>
                        </tr>
                        </tbody>
                    </table>
                </li>
                <li class="box-last-match text-left">
                    <table>
                        <tbody>
                        <tr id="tr-results-right">
                            <td>Last <span class="total-results">5</span> Match</td>
                            <td scoreaway="2" scorehome="4" number="0" class=" li-results"><img
                                        title="2018-09-15 22:00:00" src="/images/icon-stat/betlist_result/lose.png" <=""
                                td="">
                            </td>
                            <td scoreaway="2" scorehome="1" number="0" class=" li-results"><img
                                        title="2018-09-01 19:30:00" src="/images/icon-stat/betlist_result/lose.png" <=""
                                td="">
                            </td>
                            <td scoreaway="2" scorehome="1" number="0" class=" li-results"><img
                                        title="2018-08-25 22:00:00" src="/images/icon-stat/betlist_result/lose.png" <=""
                                td="">
                            </td>
                            <td scoreaway="0" scorehome="2" number="0" class=" li-results"><img
                                        title="2018-08-18 22:00:00" src="/images/icon-stat/betlist_result/lose.png" <=""
                                td="">
                            </td>
                            <td scoreaway="1" scorehome="2" number="0" class=" li-results"><img
                                        title="2018-08-11 03:00:00" src="/images/icon-stat/betlist_result/lose.png" <=""
                                td="">
                            </td>
                            <td scoreaway="4" scorehome="5" number="1" class="hide li-results"><img
                                        title="2018-05-13 22:00:00" src="/images/icon-stat/betlist_result/lose.png" <=""
                                td="">
                            </td>
                            <td scoreaway="1" scorehome="3" number="1" class="hide li-results"><img
                                        title="2018-05-10 02:45:00" src="/images/icon-stat/betlist_result/lose.png" <=""
                                td="">
                            </td>
                            <td scoreaway="2" scorehome="0" number="1" class="hide li-results"><img
                                        title="2018-05-05 22:00:00" src="/images/icon-stat/betlist_result/lose.png" <=""
                                td="">
                            </td>
                            <td scoreaway="0" scorehome="5" number="1" class="hide li-results"><img
                                        title="2018-04-28 22:00:00" src="/images/icon-stat/betlist_result/lose.png" <=""
                                td="">
                            </td>
                            <td scoreaway="0" scorehome="0" number="1" class="hide li-results"><img
                                        title="2018-04-20 02:45:00" src="/images/icon-stat/betlist_result/draw.png">
                            </td>
                            <td scoreaway="1" scorehome="2" number="2" class="hide li-results"><img
                                        title="2018-04-14 22:00:00" src="/images/icon-stat/betlist_result/lose.png" <=""
                                td="">
                            </td>
                            <td scoreaway="2" scorehome="1" number="2" class="hide li-results"><img
                                        title="2018-04-07 22:00:00" src="/images/icon-stat/betlist_result/lose.png" <=""
                                td="">
                            </td>
                            <td scoreaway="2" scorehome="0" number="2" class="hide li-results"><img
                                        title="2018-03-31 22:00:00" src="/images/icon-stat/betlist_result/lose.png" <=""
                                td="">
                            </td>
                            <td scoreaway="2" scorehome="1" number="2" class="hide li-results"><img
                                        title="2018-03-19 00:30:00" src="/images/icon-stat/betlist_result/lose.png" <=""
                                td="">
                            </td>
                            <td scoreaway="4" scorehome="1" number="2" class="hide li-results"><img
                                        title="2018-03-10 23:00:00" src="/images/icon-stat/betlist_result/lose.png" <=""
                                td="">
                            </td>
                            <td scoreaway="1" scorehome="1" number="3" class="hide li-results"><img
                                        title="2018-03-03 23:00:00" src="/images/icon-stat/betlist_result/draw.png">
                            </td>
                            <td scoreaway="1" scorehome="1" number="3" class="hide li-results"><img
                                        title="2018-02-24 20:30:00" src="/images/icon-stat/betlist_result/draw.png">
                            </td>
                            <td scoreaway="1" scorehome="5" number="3" class="hide li-results"><img
                                        title="2018-02-11 01:30:00" src="/images/icon-stat/betlist_result/lose.png" <=""
                                td="">
                            </td>
                            <td scoreaway="1" scorehome="1" number="3" class="hide li-results"><img
                                        title="2018-02-03 23:00:00" src="/images/icon-stat/betlist_result/draw.png">
                            </td>
                            <td scoreaway="1" scorehome="2" number="3" class="hide li-results"><img
                                        title="2018-02-01 03:45:00" src="/images/icon-stat/betlist_result/lose.png" <=""
                                td="">
                            </td>
                            <td class="btn-results-right" number="0"><i class="fa fa-caret-right"
                                                                        aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td>Player</td>
                            <td><img src="/images/avatar.png"></td>
                            <td><img src="/images/avatar.png"></td>
                            <td><img src="/images/avatar.png"></td>
                            <td><img src="/images/avatar.png"></td>
                            <td><img src="/images/avatar.png"></td>
                        </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <div class="progress" style="">
                                    <div class="progress-bar pull-left" role="progressbar" aria-valuenow="0.00"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 0.00%;">
                                    </div>
                                </div>
                            </td>
                            <td><span>0.00%</span></td>
                        </tr>
                        </tbody>
                    </table>


                </li>
            </ul>
            <ul class="content-HDP-game content-HDP-game-responsive">
                <li class="box-HDP-game text-center">
                    <table>
                        <tbody>
                        <tr>
                            <td class="text-right">
                                <p>-</p>
                            </td>
                            <td><h2>[-0.25 ]</h2></td>
                            <td class="text-left">
                                <p>-</p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </li>
                <li class="box-last-match text-center" style="border-right: solid 1px #092740;">
                    <table>
                        <tbody>
                        <tr>
                            <td colspan="6">Last 5 Match</td>
                        </tr>
                        <tr class="tr-btn-results-left">
                            <td class="btn-results-left" number="3"><i class="fa fa-caret-left" aria-hidden="true"></i>
                            </td>

                        </tr>

                        <tr>
                            <td colspan="6">Player</td>
                        </tr>
                        <tr class="text-center">
                            <td></td>
                            <td><img src="/images/avatar.png"></td>
                            <td><img src="/images/avatar.png"></td>
                            <td><img src="/images/avatar.png"></td>
                            <td><img src="/images/avatar.png"></td>
                            <td><img src="/images/avatar.png"></td>

                        </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                        <tr>
                            <td class="text-left">
                                <span>0.00                                                        %</span></td>
                            <td>
                                <div class="progress pull-right">
                                    <div class="progress-bar pull-right" role="progressbar" aria-valuenow="0.00"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 0.00%;">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </li>
                <li class="box-last-match text-center">
                    <table>
                        <tbody>
                        <tr>
                            <td colspan="6">Last 5 Match</td>
                        </tr>
                        <tr class="tr-btn-results-right">
                            <td class="btn-results-right" number="0"><i class="fa fa-caret-right"
                                                                        aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td colspan="6">Player</td>
                        </tr>
                        <tr>
                            <td><img src="/images/avatar.png"></td>
                            <td><img src="/images/avatar.png"></td>
                            <td><img src="/images/avatar.png"></td>
                            <td><img src="/images/avatar.png"></td>
                            <td><img src="/images/avatar.png"></td>
                        </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <div class="progress" style="">
                                    <div class="progress-bar pull-left" role="progressbar" aria-valuenow="0.00"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 0.00%;">
                                    </div>
                                </div>
                            </td>
                            <td><span>0.00                                    %</span></td>
                        </tr>
                        </tbody>
                    </table>


                </li>
            </ul>
        </div>
    </div>
</div>


<!--Review content-->
<div class="wrap-game-review">
    <div class="container">
        <div class="text-title">
            <h4>ทีเด็ด ทรรศนะ</h4>
            <div class="date-review pull-right">
                <a href="/">
                    <h5>ทรรศนะทั้งหมด <i class="fa fa-chevron-right" aria-hidden="true"></i></h5>
                </a>
            </div>
        </div>
<!--        <div class="title-mini-center text-center">-->
<!--            <h3>ทีเด็ด ทรรศนะ</h3>-->
<!---->
<!--        </div>-->

        <section class="slide">
            <div class="review-section">
                <div class="home-review-section">
                    <div class="box-content-review">
                        <div class="top-content-review pull-left">

                            <div>
                                <img src="/images/sample-logo01.png">
                            </div>
                            <div>
                                <h5>LOMTOE.NET</h5>
                                <img src="/images/logo-team/Chelsea.png">
                                <span>@Chelsea FC</span>
                            </div>
                            <div class="winRate-box">
                                <div class="bg-winRate-box">
                                    <h3>20%</h3>
                                    <p>Win Rate</p>
                                </div>
                                <div class="bg-day-box">
                                    <h3>30</h3>
                                    <p>Days</p>
                                </div>
                            </div>
                        </div>
                        <div class="full-review-caption">
                            <p> แอตเลติโก้ มาดริด ลงสนามในเกมลีกไปแล้ว 5 นัด ผลงานยังถือว่าน่าผิดหวัง ชนะ 2 เสมอ 2 แพ้ 1
                                แต่เกมล่าสุดบุกไปชนะเกตาเฟ่ 0-2 นัดนี้กลับมาเล่นในบ้านเจอกับ ฮูเอสก้า ทีมเยือน
                                ที่แพ้รวดในลีกมา 3 นัดติด นัดล่าสุดแพ้คาบ้านให้โซเซียดาด 0-1 นัดนี้ แอตฯมาดริด
                                จะยังไม่มี วีโตโล่ และ สเตฟาน ซาวิซ ที่บาดเจ็บ
                                ส่วนทีมเยือนนัดนี้ไม่มีตัวเจ็บหรือติดโทษแบน เทียบกันตามชื่อชั้นและผลงาน ทีมตราหมี
                                ยังเหนือกว่าทีมเยือนค่อนข้างเยอะ บวกกับนัดนี้ได้เล่นในบ้าน ดูแล้ว แอตเลติโก้ มาดริด
                                น่าจะเป็นฝ่ายเปิดบ้านไล่ต้อนเอาชนะทีมเยือนได้ไม่ยาก


                                ฟันธง : ต่อ แอตเลติโก้ มาดริด

                                ผลงานที่คาด : แอตเลติโก้ มาดริด 2-0 ฮูเอสก้า
                            </p>
                        </div>
                    </div>
                    <div class="box-content-review">
                        <div class="top-content-review pull-left">
                            <div>
                                <img src="/images/profess-player/sample-logo02.png">
                            </div>
                            <div>
                                <h5>Hotleague</h5>
                                <img src="/images/logo-team/Chelsea.png">
                                <span>@Chelsea FC</span>
                            </div>
                            <div class="winRate-box">
                                <div class="bg-winRate-box">
                                    <h3>20%</h3>
                                    <p>Win Rate</p>
                                </div>
                                <div class="bg-day-box">
                                    <h3>30</h3>
                                    <p>Days</p>
                                </div>
                            </div>
                        </div>
                        <div class="full-review-caption">
                            <p>แอตเลติโก้ มาดริด ไม่คิดนาน 2-0</p>
                        </div>
                    </div>
                    <div class="box-content-review">
                        <div class="top-content-review pull-left">
                            <div>
                                <img src="/images/profess-player/sample-logo03.png">
                            </div>
                            <div>
                                <h5>ทีเด็ดสปอร์ตพูล</h5>
                                <img src="/images/logo-team/Chelsea.png">
                                <span>@Chelsea FC</span>
                            </div>
                            <div class="winRate-box">
                                <div class="bg-winRate-box">
                                    <h3>20%</h3>
                                    <p>Win Rate</p>
                                </div>
                                <div class="bg-day-box">
                                    <h3>30</h3>
                                    <p>Days</p>
                                </div>
                            </div>
                        </div>
                        <div class="full-review-caption">
                            <p> ทั้งสองทีมต่างก็ลงเตะในลีกไปแล้วทีมละ 5 นัด ผลปรากฏว่าแอตฯมาดริดทำผลงานได้ดีกว่า เก็บไป8
                                คะแนน ขณะที่ฮูเอสก้าเก็บไป 4คะแนน แน่นอนว่าทีมชนะนัดนี้ย่อมเอียงไปทางแอตฯมาดริด
                                และด้วยชื่อชั้น คุณภาพทีมนักเตะที่ดีกว่า มีลุ้นบดชนะได้ไม่ยาก บวกกับเล่นในบ้านด้วย
                                ดูทรงแล้วฮูเอสก้ามาเยือนไม่น่ารอด บวกกับฟอร์มช่วงนี้แย่สุดๆด้วย แพ้รวด 3 นัดติดต่อกัน
                                ดังนั้นเกมนี้มั่นใจแอตฯมาดริดในบ้านชนะแน่ๆ และหากไม่ยิงทิ้ง
                                ยิงขว้างน่าจะมีผลชนะเกินหนึ่งประตุได้เลย ลุ้นทีมตราหมีได้เลย
                                ทีเด็ดฟุตบอลวันนี้ : แอตฯมาดริด
                            </p>
                        </div>
                    </div>
                    <div class="box-content-review">
                        <div class="top-content-review pull-left">

                            <div>
                                <img src="/images/profess-player/sample-logo04.png">
                            </div>
                            <div>
                                <h5>Siamsport</h5>
                                <img src="/images/logo-team/Chelsea.png">
                                <span>@Chelsea FC</span>
                            </div>
                            <div class="winRate-box">
                                <div class="bg-winRate-box">
                                    <h3>20%</h3>
                                    <p>Win Rate</p>
                                </div>
                                <div class="bg-day-box">
                                    <h3>30</h3>
                                    <p>Days</p>
                                </div>
                            </div>
                        </div>
                        <div class="full-review-caption">
                            <p>ฮูเอสก้า หลังจากโดนบาซ่าชนะยิงประตูไม่ได้ 2 เกมติด ขวัญกำลังย่ำแย่ ฉะนั้นเกมนี้มองแล้ว ฮูเอสก้า ไม่น่าเชียร์
                            </p>
                        </div>
                    </div>
                    <div class="box-content-review">
                        <div class="top-content-review pull-left">
                            <div>
                                <img src="/images/profess-player/sample-logo06.png">
                            </div>
                            <div>
                                <h5>SMM Sport</h5>
                                <img src="/images/logo-team/Chelsea.png">
                                <span>@Chelsea FC</span>
                            </div>
                            <div class="winRate-box">
                                <div class="bg-winRate-box">
                                    <h3>20%</h3>
                                    <p>Win Rate</p>
                                </div>
                                <div class="bg-day-box">
                                    <h3>30</h3>
                                    <p>Days</p>
                                </div>
                            </div>
                        </div>
                        <div class="full-review-caption">
                            <p></p>
                        </div>
                    </div>
                    <div class="box-content-review">
                        <div class="top-content-review pull-left">
                            <div>
                                <img src="/images/profess-player/sample-logo07.png">
                            </div>
                            <div>
                                <h5>วิเคราะห์บอลลีก</h5>
                                <img src="/images/logo-team/Chelsea.png">
                                <span>@Chelsea FC</span>
                            </div>
                            <div class="winRate-box">
                                <div class="bg-winRate-box">
                                    <h3>20%</h3>
                                    <p>Win Rate</p>
                                </div>
                                <div class="bg-day-box">
                                    <h3>30</h3>
                                    <p>Days</p>
                                </div>
                            </div>
                        </div>
                        <div class="full-review-caption">
                            <p> ตราหมีกำลังเข้าที่เข้าทาง ง้างตีนรอยิงมาแต่ไกล 2-0
                            </p>
                        </div>
                    </div>
                    <div class="box-content-review">
                        <div class="top-content-review pull-left">
                            <div class="user-review">
                                <img src="/images/1.jpg">
                            </div>
                            <div>
                                <h5>ชาวสวน วิ่งเล่นหลังบ้าน</h5>
                                <img src="/images/logo-team/Chelsea.png">
                                <span>@Chelsea FC</span>
                            </div>
                            <div class="winRate-box">
                                <div class="bg-winRate-box">
                                    <h3>20%</h3>
                                    <p>Win Rate</p>
                                </div>
                                <div class="bg-day-box">
                                    <h3>30</h3>
                                    <p>Days</p>
                                </div>
                            </div>
                        </div>
                        <div class="full-review-caption">
                            <p> ทีเด็ดแอต.มาดริดยิงสบาย
                            </p>
                        </div>
                    </div>
                </div>
                <div class="away-review-section">
                    <div class="box-content-review">
                        <div class="top-content-review pull-left">
                            <div>
                                <img src="/images/profess-player/sample-logo02.png">
                            </div>
                            <div>
                                <h5>Hotleague</h5>
                                <img src="/images/logo-team/manchester.png">
                                <span>@Manchester United</span>
                            </div>
                            <div class="winRate-box">
                                <div class="bg-winRate-box">
                                    <h3>18%</h3>
                                    <p>Win Rate</p>
                                </div>
                                <div class="bg-day-box">
                                    <h3>03</h3>
                                    <p>Days</p>
                                </div>
                            </div>
                        </div>
                        <div class="full-review-caption">
                            <p> มั่นใจมากขึ้นสำหรับแอต.มาดริด หลังจากเก็บชัยติดต่อกันมา 2 เกม แต่ก็ยังวางใจไม่ได้ในเรื่องการผลิตสกอร์ ล่าสุดชนะ 2-0 ก็เพราะคู่แข่งเหลือผู้เล่น 10 คน ไม่อย่างนั้นอาจหืดจับอีกเกม ประกอบกับ "ตราหมี" ต้องลงสนาม 4 เกมในรอบ 10 วัน จึงน่าจะเจอความอ่อนล้าเล่นงาน บวกกับเจ้าถิ่นเป็นบอลสไตล์ตีหัวเข้าบ้าน ก็คงคาดหวังให้ยิงขาดเกินราคา 1.5 ยากหน่อย ถึงแม้ชื่อชั้นอูเอสก้า จะเทียบเจ้าบ้านไม่ได้ ทว่าอาคันตุกะไม่คิดอะไรมาก นอกจากมาตั้งรับอย่างเดียว และน่าจะสร้างปัญหาให้กับบอลเชิงอย่างแอต.มาดริด พอสมควร แถมความสดเป็นรองคู่แข่ง เชื่อว่าอย่างเก่งคงได้แค่เฉือน

                                ผลบอลที่คาด : แอต.มาดริดชนะ 1-0

                                ระดับความมั่นใจ : 8/10
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
    function openNav() {
        document.getElementById("mySidenav").style.marginRight = "0";
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main").style.marginRight = "250px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.margin = "0 auto";
    }
</script>
<script>
    function myFunction() {
        var dots = document.getElementById("dots");
        var moreText = document.getElementById("more");
        var btnText = document.getElementById("myBtn");

        if (dots.style.display === "none") {
            dots.style.display = "inline";
            btnText.innerHTML = "Read more";
            moreText.style.display = "none";
        } else {
            dots.style.display = "none";
            btnText.innerHTML = "Read less";
            moreText.style.display = "inline";
        }
    }
</script>
<script>
    $('section').readmore({maxHeight: 200});
</script>

</body>
</html>
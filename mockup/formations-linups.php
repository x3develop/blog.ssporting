<div class="all-lineups-field text-center">
    <div class="box-field">
        <div class="topic-formation">
            <div class="pull-left text-left">
                <img src="/images/logo-team/manchester.png">
                <h5>3-4-3</h5>
            </div>
            <div class="pull-right text-right">
                <h5>4-4-2</h5>
                <img src="/images/logo-team/Chelsea.png">
            </div>
        </div>
        <div class="content-field">
            <div class="home-formation">

                <div class="home-gk-row-content full-width">
                    <div class="gk-row-1">
                        <div class="box-gk">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <div class="symbol-bt-right substitute-player"></div>
                                <div class="symbol-bt-bottom goal-score"></div>
                                <span>1</span>
                                <p>David De Gea</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="home-df-row-content full-width">
                    <div class="df-row-6">
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>14</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="df-row-5 hide">
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>14</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="df-row-4 hide">
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>14</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="df-row-3 hide">
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <div class="symbol-bt-right substitute-player"></div>
                                <span>12</span>
                                <p>Chris Smalling</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt red-card"></div>
                                <span>4</span>
                                <p>Phil Jones</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt group-yellow-card"></div>
                                <span>2</span>
                                <p>Victor Lindelöf</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="home-cm-row-content full-width">
                    <div class="cm-row-2">
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="cm-row-3 hide">
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>14</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="cm-row-4 hide">
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt group-yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt group-yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt group-yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt group-yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="cm-row-5 hide">
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>14</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="cm-row-6 hide">
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>14</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>14</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="home-ft-row-content full-width">
                    <div class="ft-row-4 hide">
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>14</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="ft-row-3 hide">
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>14</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="ft-row-2">
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="ft-row-1 hide">
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="away-formation">
                <div class="away-ft-row-content full-width">
                    <div class="ft-row-4 hide">
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>14</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="ft-row-3 hide">
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt-right yellow-card"></div>
                                <div class="symbol-bt goal-score">
                                    <div>x3</div>
                                </div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt penalty-score"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <span>14</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="ft-row-2">
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="ft-row-1 hide">
                        <div class="box-ft">
                            <div class="info-box-pos">
                                <div class="symbol-bt penalty-score"><div>2</div></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="away-cm-row-content full-width">
                    <div class="cm-row-2">
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="cm-row-3 hide">
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>14</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="cm-row-4 hide">
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt group-yellow-card"></div>
                                <span>7</span>
                                <p>N'Golo Kanté</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt group-yellow-card"></div>
                                <span>5</span>
                                <p>Jorginho</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt group-yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt group-yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="cm-row-5 hide">
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>14</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="cm-row-6 hide">
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>14</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-cm">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="away-df-row-content full-width">
                    <div class="df-row-6">
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>14</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="df-row-5 hide">
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>2</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>5</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>14</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <span>35</span>
                                <p>Ederson</p>
                            </div>
                        </div>
                    </div>
                    <div class="df-row-4 hide">
                        <div class="box-df">
                            <div class="info-box-pos">
                                <span>28</span>
                                <p>César Azpilicueta</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt own-goal"></div>
                                <span>2</span>
                                <p>Antonio Rüdiger</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>30</span>
                                <p>David Luiz</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <span>3</span>
                                <p>Marcos Alonso</p>
                            </div>
                        </div>
                    </div>
                    <div class="df-row-3 hide">
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>12</span>
                                <p>Chris Smalling</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt red-card"></div>
                                <span>4</span>
                                <p>Phil Jones</p>
                            </div>
                        </div>
                        <div class="box-df">
                            <div class="info-box-pos">
                                <div class="symbol-bt group-yellow-card"></div>
                                <span>2</span>
                                <p>Victor Lindelöf</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="away-gk-row-content full-width">
                    <div class="gk-row-1">
                        <div class="box-gk">
                            <div class="info-box-pos">
                                <div class="symbol-bt yellow-card"></div>
                                <span>1</span>
                                <p>David De Gea</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="content-sub-player">
                <div class="title-center">
                    Sub Player
                </div>
                <div class="sub-content bt-flex full-width">
                    <div class="home-sub-content">
                        <ul class="home-sub">
                            <li>
                                <div class="num-sub-t">
                                    <div>22</div>
                                </div>
                            </li>
                            <li>
                                <div class="name">
                                    <span>Sergio Romero</span>
                                </div>
                                <div class="detail-player">
                                    <p>GK</p>
                                </div>
                            </li>
                        </ul>
                        <ul class="home-sub">
                            <li>
                                <div class="num-sub-t">
                                    <div>12</div>
                                </div>
                            </li>
                            <li>
                                <div class="name">
                                    <span>Chris Smalling</span>
                                </div>
                                <div class="detail-player">
                                    <p>GK</p>
                                    <div class="substitute-player"></div>
                                    <span>15'</span>
                                    <p>Phil Jones</p>
                                </div>
                            </li>
                        </ul>
                        <ul class="home-sub">
                            <li>
                                <div class="num-sub-t">
                                    <div>20</div>
                                </div>
                            </li>
                            <li>
                                <div class="name">
                                    <span>Diogo Dalot</span>
                                </div>
                                <table>
                                    <tr>
                                        <td><p>DF</p></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>

                            </li>
                        </ul>
                        <ul class="home-sub">
                            <li>
                                <div class="num-sub-t">
                                    <div>12</div>
                                </div>
                            </li>
                            <li>
                                <div class="name">
                                    <span>Jesse Lingard</span>
                                </div>
                                <div class="detail-player">
                                    <p>CM</p>
                                    <div class="substitute-player"></div>
                                    <span>62'</span>
                                    <p>A. Pereira</p>
                                </div>
                            </li>
                        </ul>
                        <ul class="home-sub">
                            <li>
                                <div class="num-sub-t">
                                    <div>17</div>
                                </div>
                            </li>
                            <li>
                                <div class="name">
                                    <span>Fred</span>
                                </div>
                                <div class="detail-player">
                                    <p>CM</p>
                                </div>
                            </li>
                        </ul>
                        <ul class="home-sub">
                            <li>
                                <div class="num-sub-t">
                                    <div>21</div>
                                </div>
                            </li>
                            <li>
                                <div class="name">
                                    <span>Ander Herrera</span>
                                </div>
                                <div class="detail-player">
                                    <p>CM</p>
                                </div>
                            </li>
                        </ul>
                        <ul class="home-sub">
                            <li>
                                <div class="num-sub-t">
                                    <div>7</div>
                                </div>
                            </li>
                            <li>
                                <div class="name">
                                    <span>Alexis Sánchez</span>
                                </div>
                                <div class="detail-player">
                                    <p>FT</p>
                                    <div class="substitute-player"></div>
                                    <span>67'</span>
                                    <p>R. Lukaku</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="away-sub-content">
                        <ul class="away-sub">
                            <li>
                                <div class="name">
                                    <span>Willy Caballero</span>
                                </div>
                                <div class="detail-player">
                                    <p>GK</p>
                                </div>
                            </li>
                            <li>
                                <div class="num-sub-t">
                                    <div>13</div>
                                </div>
                            </li>
                        </ul>
                        <ul class="away-sub">
                            <li>
                                <div class="name">
                                    <span>Andreas Christensen</span>
                                </div>
                                <div class="detail-player">
                                    <p>DF</p>
                                </div>
                            </li>
                            <li>
                                <div class="num-sub-t">
                                    <div>33</div>
                                </div>
                            </li>
                        </ul>
                        <ul class="away-sub">
                            <li>
                                <div class="name">
                                    <span>Emerson Palmieri</span>
                                </div>
                                <div class="detail-player">
                                    <p>Marcos Alonso</p>
                                    <span>52'</span>
                                    <div class="substitute-player"></div>
                                    <p>DF</p>
                                </div>
                            </li>
                            <li>
                                <div class="num-sub-t">
                                    <div>17</div>
                                </div>
                            </li>
                        </ul>
                        <ul class="away-sub">
                            <li>
                                <div class="name">
                                    <span>Mateo Kovačić</span>
                                </div>
                                <div class="detail-player">
                                    <p>Ross Barkley</p>
                                    <span>17'</span>
                                    <div class="substitute-player"></div>
                                    <p>CM</p>
                                </div>
                            </li>
                            <li>
                                <div class="num-sub-t">
                                    <div>17</div>
                                </div>
                            </li>
                        </ul>
                        <ul class="away-sub">
                            <li>
                                <div class="name">
                                    <span>Ruben Loftus-Cheek</span>
                                </div>
                                <div class="detail-player">
                                    <p>Pedro</p>
                                    <span>65'</span>
                                    <div class="substitute-player"></div>
                                    <p>FT</p>
                                </div>
                            </li>
                            <li>
                                <div class="num-sub-t">
                                    <div>12</div>
                                </div>
                            </li>
                        </ul>
                        <ul class="away-sub">
                            <li>
                                <div class="name">
                                    <span>Olivier Giroud</span>
                                </div>
                                <div class="detail-player">
                                    <p>FT</p>
                                </div>
                            </li>
                            <li>
                                <div class="num-sub-t">
                                    <div>18</div>
                                </div>
                            </li>
                        </ul>
                        <ul class="away-sub">
                            <li>
                                <div class="name">
                                    <span>Willian</span>
                                </div>
                                <div class="detail-player">
                                    <p>FT</p>
                                </div>
                            </li>
                            <li>
                                <div class="num-sub-t">
                                    <div>22</div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
            <div style="clear: both;"></div>
        </div>

        <div style="clear: both;"></div>
    </div>
</div>


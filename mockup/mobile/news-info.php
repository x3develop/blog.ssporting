<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/mobile/css/all-style.css" rel="stylesheet">
    <link href="/mobile/css/article.css" rel="stylesheet">
    <link href="/mobile/css/bootstrap.css" rel="stylesheet">
    <link href="/mobile/css/docs.css" rel="stylesheet">
    <link href="/mobile/css/game.css" rel="stylesheet">
    <link href="/mobile/css/highlight.css" rel="stylesheet">
    <link href="/mobile/css/index.css" rel="stylesheet">
    <link href="/mobile/css/live-football.css" rel="stylesheet">
    <link href="/mobile/css/news.css" rel="stylesheet">
    <link href="/mobile/css/profile.css" rel="stylesheet">
    <link href="/mobile/css/ranking.css" rel="stylesheet">
    <link href="/mobile/css/review.css" rel="stylesheet">
    <link href="/mobile/css/livescore-mobile.css" rel="stylesheet">
    <link href="/mobile/css/football-symbol-mobile.css" rel="stylesheet">
    <link href="/mobile/css/swiper.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/mobile/css/font-awesome-4.7.0/css/font-awesome.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/js/moment.js"></script>
    <script src="/js/moment-with-locales.min.js"></script>
    <script src="/js/newFB.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<?php include("menu-mobile.php"); ?>

<div class="container">
    <div class="row">
        <div class="feature-news" style="background-image:url(/images/background4.jpg);"></div>
    </div>
</div>
<div class="container container-title-news-info">
    <h2>อังกฤษได้เปรียบ! เปิดกฎฟีฟ่าตัดสินแชมป์กลุ่ม จี นัดสุดท้าย</h2>

</div>
<div class="container">
    <div class="time-box box-inline">4 hours ago</div>
    <div class="vertical-line-between box-inline"></div>
    <div class="view-box box-inline">20</div>
    <div class="container-share-news box-inline pull-right">
        <span class="ft-share-box">Share</span>
        <span class="gp-share-box">Share</span>
    </div>
</div>


<div class="container container-detail-news-info">
  <p>ทีมสิงโตคำรามกุมความได้เปรียบเหนือปีศาจแดงแห่งยุโรปอยู่เล็กน้อยในเกมนัดสุดท้าย
    หลังจากที่ทั้งคู่มีทั้งแต้มและประตูได้เสียเท่ากันในเวลานี้

    ทีมชาติอังกฤษ มีความได้เปรียบเหนือกว่าทีมชาติเบลเยียมอยู่เล็กน้อย จากการที่พวกเขามีคะแนนแฟร์เพลย์ที่ดีกว่าคู่แข่ง
    ก่อนการตัดสินนัดสุดท้ายของกลุ่ม จี ในฟุตบอลโลก 2018 รอบแบ่งกลุ่ม

    ทัพสิงโตคำรามและทีมปีศาจแดงแห่งยุโรป มี 6 คะแนน จากการแข่งขัน 2 นัดเท่ากัน ทั้งยังยิงได้ 8 ประตู และเสีย 2 ประตู
    เท่ากัน ก่อนที่จะโคจรมาพบกันในเกมนัดสุดท้ายเพื่อตัดสินว่าใครจะได้เป็นแชมป์กลุ่ม

    จากสถานการณ์ดังกล่าว ทำให้หากทั้งสองทีมยังเสมอกันอีกในนัดสุดท้าย จะทำให้พวกเขาต้องใช้เงื่อนไข g) ตามกฎของที่ 32.5
    ของระเบียบการแข่งขันฟุตบอลโลก 2018 ซึ่งระบุว่าจะตัดสินกันด้วยคะแนนแฟร์เพลย์
    ทำให้อังกฤษเป็นฝ่ายได้เปรียบอยู่ในเวลานี้ เพราะมีใบเหลืองสะสมเพียง 2 ใบ (-2 คะแนน) ขณะที่เบลเยียมมีใบเหลืองสะสม 3 ใบ
    (-3 คะแนน) แม้ว่าจะยังต้องรอผลใบเหลือง-ใบแดง ในนัดสุดท้ายเพิ่มเติมอีกก็ตาม

    อย่างไรก็ดี สำหรับทีมที่เป็นแชมป์ของกลุ่ม จี นั้นมีโอกาสสูงที่จะโคจรไปพบทีมชาติบราซิลหรือเยอรมันในรอบ 8 ทีมสุดท้าย
    หลังจากที่บราซิลมีแนวโน้มว่าจะเป็นแชมป์ของกลุ่ม อี ส่วนเยอรมันอาจทำได้เพียงอันดับสองของกลุ่ม เอฟ
    หากเม็กซิโกไม่พลาดในนัดสุดท้าย ขณะที่เส้นทางของการเป็นอันดับสองของกลุ่ม น่าจะได้พบเซเนกัลในรอบ 16 ทีมสุดท้าย
    และเม็กซิโกหรือเซอร์เบีย ในรอบ 8 ทีมสุดท้ายแทน

    ที่มา: http://www.goal.com

  </p>
</div>

<div class="container">
    <div class="bx-publish-recommend">
        <div class="text-title">
            <h4>Related</h4>
        </div>
        <div class="content-recent">
            <div class="content-item-recent">
                <a class="link_wrap" href="/mobile/news-info.php"></a>
                <div class="item-recent">
                    <div><img src="/images/6.jpg"></div>
                    <div class="bx-2">
                        <h4 class="media-heading">เดชองส์ชิงซีดาน! ฟีฟาแบโผ 10 แคนดิเดตโค้ชแห่งปี</h4>
                        <div class="time-box box-inline">4 hours ago</div>
                        <div class="vertical-line-between box-inline"></div>
                        <div class="view-box box-inline">20</div>
                    </div>
                </div>
            </div>
            <div class="content-item-recent">
                <a href="/mobile/news-info.php"></a>
                <div class="item-recent">
                    <div><img src="/images/1.jpg"></div>
                    <div class="bx-2">
                        <h4 class="media-heading">ดาวดังคับคั่ง! ฟีฟ่าแบโผ 10 นักเตะลุ้นคว้าแข้งแห่งปี</h4>
                        <div class="time-box box-inline">4 hours ago</div>
                        <div class="vertical-line-between box-inline"></div>
                        <div class="view-box box-inline">20</div>
                    </div>
                </div>
            </div>
            <div class="content-item-recent">
                <a href="/mobile/news-info.php"></a>
                <div class="item-recent">
                    <div><img src="/images/2.jpg"></div>
                    <div class="bx-2">
                        <h4 class="media-heading">VARดวลกล้องสโมสร!</h4>
                        <div class="time-box box-inline">4 hours ago</div>
                        <div class="vertical-line-between box-inline"></div>
                        <div class="view-box box-inline">20</div>
                    </div>
                </div>
            </div>
            <div class="content-item-recent">
                <a href="/mobile/news-info.php"></a>
                <div class="item-recent">
                    <div><img src="/images/3.jpg"></div>
                    <div class="bx-2">
                        <h4 class="media-heading">ก่อน2ศึกใหญ่! ส.บอลวางแพลน “ช้างศึก” อุ่นเครื่อง 4 แมตช์</h4>
                        <div class="time-box box-inline">4 hours ago</div>
                        <div class="vertical-line-between box-inline"></div>
                        <div class="view-box box-inline">20</div>
                    </div>
                </div>
            </div>
            <div class="content-item-recent">
                <a href="/mobile/news-info.php"></a>
                <div class="item-recent">
                    <div><img src="/images/5.jpg"></div>
                    <div class="bx-2">
                        <h4 class="media-heading">เพื่อความปลอดภัย!กิเลนลดเดินขอบคุณแฟนหลังเหตุปาธงสารัช!</h4>
                        <div class="time-box box-inline">4 hours ago</div>
                        <div class="vertical-line-between box-inline"></div>
                        <div class="view-box box-inline">20</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include("footer.php"); ?>
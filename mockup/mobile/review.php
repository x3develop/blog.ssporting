<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/mobile/css/all-style.css" rel="stylesheet">
    <link href="/mobile/css/article.css" rel="stylesheet">
    <link href="/mobile/css/bootstrap.css" rel="stylesheet">
    <link href="/mobile/css/docs.css" rel="stylesheet">
    <link href="/mobile/css/game.css" rel="stylesheet">
    <link href="/mobile/css/highlight.css" rel="stylesheet">
    <link href="/mobile/css/index.css" rel="stylesheet">
    <link href="/mobile/css/live-football.css" rel="stylesheet">
    <link href="/mobile/css/news.css" rel="stylesheet">
    <link href="/mobile/css/profile.css" rel="stylesheet">
    <link href="/mobile/css/ranking.css" rel="stylesheet">
    <link href="/mobile/css/review.css" rel="stylesheet">
    <link href="/mobile/css/livescore-mobile.css" rel="stylesheet">
    <link href="/mobile/css/football-symbol-mobile.css" rel="stylesheet">
    <link href="/mobile/css/swiper.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/mobile/css/font-awesome-4.7.0/css/font-awesome.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/js/moment.js"></script>
    <script src="/js/moment-with-locales.min.js"></script>
    <script src="/js/newFB.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>


<?php include("menu-mobile.php"); ?>
<div id="Sidenav_ranking" class="sidenav-review" style="width: 250px;">
    <div class="container-sidenav-review">
        <div class="title-sidenav-review">
            <h3>Ranking</h3>
            <a href="javascript:void(0)" class="closebtn" onclick="closeNavSide()">&times;</a>
        </div>
        <!--Top Ranking-->
        <div class="row-sidenav-review">
            <div class="text-title">
                <a class="link_wrap" href="ranking-all.php"></a>
                <h4><img src="/images/trophy/300150.png">Top Ranking</h4>
                <p>ทั้งหมด</p>
            </div>
            <div class="container-top-ranking">

                <!-- Nav tabs -->
                <!--        nav-pills-->
                <ul class="nav nav-pills" role="tablist">
                    <li role="presentation" class="active"><a href="#allTime_topRanking" role="tab" data-toggle="tab">All
                            Time</a></li>
                    <li role="presentation"><a href="#now_topRanking" role="tab" data-toggle="tab">Now</a></li>
                    <li role="presentation"><a href="#lastChamp_topRanking" aria-controls="messages" role="tab"
                                               data-toggle="tab">Last Champ</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="allTime_topRanking">
                        <table class="table table-striped">
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>PTA</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td><img src="/images/p1.png"></td>
                                <td><p>Rittirong Nimcharone</p></td>
                                <td>100.00</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td><img src="/images/bg-tophead.jpg"></td>
                                <td><p>esdakron Bounlong</p></td>
                                <td>95.00</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td><img src="/images/9.jpg"></td>
                                <td><p>Pataradanai Suanti</p></td>
                                <td>72.00</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td><img src="/images/2.jpg"></td>
                                <td><p>พัชรี ชูชาติ</p></td>
                                <td>63.00</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td><img src="/images/6.jpg"></td>
                                <td><p>Natchanon Karkingphai</p></td>
                                <td>60.00</td>
                            </tr>

                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="now_topRanking">
                        <table class="table table-striped">
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>PTA</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td><img src="/images/p1.png"></td>
                                <td><p>Rittirong Nimcharone</p></td>
                                <td>100.00</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td><img src="/images/bg-tophead.jpg"></td>
                                <td><p>esdakron Bounlong</p></td>
                                <td>95.00</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td><img src="/images/9.jpg"></td>
                                <td><p>Pataradanai Suanti</p></td>
                                <td>72.00</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td><img src="/images/2.jpg"></td>
                                <td><p>พัชรี ชูชาติ</p></td>
                                <td>63.00</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td><img src="/images/6.jpg"></td>
                                <td><p>Natchanon Karkingphai</p></td>
                                <td>60.00</td>
                            </tr>

                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="lastChamp_topRanking">
                        <table class="table table-striped">
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>PTA</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td><img src="/images/p1.png"></td>
                                <td><p>Rittirong Nimcharone</p></td>
                                <td>100.00</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td><img src="/images/bg-tophead.jpg"></td>
                                <td><p>esdakron Bounlong</p></td>
                                <td>95.00</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td><img src="/images/9.jpg"></td>
                                <td><p>Pataradanai Suanti</p></td>
                                <td>72.00</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td><img src="/images/2.jpg"></td>
                                <td><p>พัชรี ชูชาติ</p></td>
                                <td>63.00</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td><img src="/images/6.jpg"></td>
                                <td><p>Natchanon Karkingphai</p></td>
                                <td>60.00</td>
                            </tr>

                        </table>
                    </div>
                </div>

            </div>
        </div>

        <!--Top Sgold-->
        <div class="row-sidenav-review" style="margin: 20px 0;">
            <div class="text-title">
                <a class="link_wrap" href="ranking-all.php"></a>
                <h4><img src="/images/trophy/000150.png">Top Sgold</h4>
                <p>ทั้งหมด</p>
            </div>
            <div class="container-top-ranking">

                <!-- Nav tabs -->
                <!--        nav-pills-->
                <ul class="nav nav-pills" role="tablist">
                    <li role="presentation" class="active"><a href="#allTime_topRanking" role="tab" data-toggle="tab">All
                            Time</a></li>
                    <li role="presentation"><a href="#now_topRanking" role="tab" data-toggle="tab">Now</a></li>
                    <li role="presentation"><a href="#lastChamp_topRanking" aria-controls="messages" role="tab"
                                               data-toggle="tab">Last Champ</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="allTime_topRanking">
                        <table class="table table-striped">
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>PTA</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td><img src="/images/p1.png"></td>
                                <td><p>Rittirong Nimcharone</p></td>
                                <td>3,687.00</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td><img src="/images/bg-tophead.jpg"></td>
                                <td><p>esdakron Bounlong</p></td>
                                <td>3,011.00</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td><img src="/images/9.jpg"></td>
                                <td><p>Pataradanai Suanti</p></td>
                                <td>3,011.00</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td><img src="/images/2.jpg"></td>
                                <td><p>พัชรี ชูชาติ</p></td>
                                <td>2,608.00</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td><img src="/images/6.jpg"></td>
                                <td><p>Natchanon Karkingphai</p></td>
                                <td>2,448.00</td>
                            </tr>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="now_topRanking">
                        <table class="table table-striped">
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>PTA</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td><img src="/images/p1.png"></td>
                                <td><p>Rittirong Nimcharone</p></td>
                                <td>3,687.00</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td><img src="/images/bg-tophead.jpg"></td>
                                <td><p>esdakron Bounlong</p></td>
                                <td>3,011.00</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td><img src="/images/9.jpg"></td>
                                <td><p>Pataradanai Suanti</p></td>
                                <td>3,011.00</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td><img src="/images/2.jpg"></td>
                                <td><p>พัชรี ชูชาติ</p></td>
                                <td>2,608.00</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td><img src="/images/6.jpg"></td>
                                <td><p>Natchanon Karkingphai</p></td>
                                <td>2,448.00</td>
                            </tr>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="lastChamp_topRanking">
                        <table class="table table-striped">
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>PTA</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td><img src="/images/p1.png"></td>
                                <td><p>Rittirong Nimcharone</p></td>
                                <td>3,687.00</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td><img src="/images/bg-tophead.jpg"></td>
                                <td><p>esdakron Bounlong</p></td>
                                <td>3,011.00</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td><img src="/images/9.jpg"></td>
                                <td><p>Pataradanai Suanti</p></td>
                                <td>3,011.00</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td><img src="/images/2.jpg"></td>
                                <td><p>พัชรี ชูชาติ</p></td>
                                <td>2,608.00</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td><img src="/images/6.jpg"></td>
                                <td><p>Natchanon Karkingphai</p></td>
                                <td>2,448.00</td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>
        </div>

        <!--Top ทรรศนะ-->
        <div class="row-sidenav-review" style="margin-bottom: 20px;">
            <div class="text-title">
                <a class="link_wrap" href="ranking-all.php"></a>
                <h4><img src="/images/logo/fav-ngoal.png">Top ทรรศนะ</h4>
                <p>ทั้งหมด</p>
            </div>
            <div class="container-top-ranking">

                <!-- Nav tabs -->
                <!--        nav-pills-->
                <ul class="nav nav-pills" role="tablist">
                    <li role="presentation" class="active"><a href="#Top_Professional" role="tab" data-toggle="tab">Top
                            Professional</a></li>
                    <li role="presentation"><a href="#Top_Players" role="tab" data-toggle="tab">Top Players</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="Top_Professional">
                        <table class="table table-striped">
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>PTA</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td><img src="/images/p1.png"></td>
                                <td><p>Rittirong Nimcharone</p></td>
                                <td>60.00%</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td><img src="/images/bg-tophead.jpg"></td>
                                <td><p>esdakron Bounlong</p></td>
                                <td>52.00%</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td><img src="/images/9.jpg"></td>
                                <td><p>Pataradanai Suanti</p></td>
                                <td>50.00%</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td><img src="/images/2.jpg"></td>
                                <td><p>พัชรี ชูชาติ</p></td>
                                <td>47.00%</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td><img src="/images/6.jpg"></td>
                                <td><p>Natchanon Karkingphai</p></td>
                                <td>40.00%</td>
                            </tr>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="Top_Players">
                        <table class="table table-striped">
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>PTA</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td><img src="/images/p1.png"></td>
                                <td><p>Rittirong Nimcharone</p></td>
                                <td>60.00%</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td><img src="/images/bg-tophead.jpg"></td>
                                <td><p>esdakron Bounlong</p></td>
                                <td>52.00%</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td><img src="/images/9.jpg"></td>
                                <td><p>Pataradanai Suanti</p></td>
                                <td>50.00%</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td><img src="/images/2.jpg"></td>
                                <td><p>พัชรี ชูชาติ</p></td>
                                <td>47.00%</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td><img src="/images/6.jpg"></td>
                                <td><p>Natchanon Karkingphai</p></td>
                                <td>40.00%</td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="main-review">
    <?php include("header.php"); ?>
    <!--        Review-->
    <div class="container">
        <div class="text-title">
            <a href="#">
                <h4>ทีเด็ด ทรรศนะ</h4>
                <p onclick="openNavSide()">Ranking</p>
            </a>
        </div>
        <div class="thumbnail-review thumbnail-review-index">
            <div class="top-section">
                <a class="link_wrap" href="/mobile/game.php"></a>
                <div class="topic-review">
                    <div class="topic-review-title">
                        <h4>UEFA Champions League</h4>
                        <div class="date-box">03/07/2018</div>
                    </div>

                    <div class="more-box">
                        <h6>เล่นเกมทายผล <i class="fa fa-chevron-right" aria-hidden="true"></i></h6>
                    </div>
                </div>
                <div class="content-top-review">


                    <div class="home-review-box">
                        <div class="logo"><img src="/images/logo-team/manchester.png"></div>
                        <div class="name">Man United</div>
                        <div class="mobile-vote-team active">
                            <h4>Voted</h4>
                            <div class="bg-mobile-vote-team"></div>
                        </div>
                        <ul class="last-match">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                        </ul>
                    </div>
                    <div class="HDP-box-center">
                        <!--                        <div class="time-box">01:00</div>-->
                        <strong>HDP</strong>
                        <h2>-2.50</h2>
                        <div class="box-water-text">
                            <div>1.9</div>
                            <div>1.69</div>
                        </div>
                        <div class="box-chanel">
                            <ul>
                                <li>Live On</li>
                                <li><img src="/images/channel/True-Sport-1-1.png"></li>
                            </ul>
                        </div>
                    </div>


                    <div class="away-review-box">
                        <div class="logo"><img src="/images/logo-team/Chelsea.png"></div>
                        <div class="name">Chelsea</div>
                        <div class="mobile-vote-team">
                            <h4>Vote</h4>
                            <div class="bg-mobile-vote-team"></div>
                        </div>
                        <ul class="last-match">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                        </ul>
                    </div>

                </div>
                <div class="progress">
                    <div class="progress-bar" style="width: 35%">
                        <span>35%</span>
                    </div>
                    <div class="progress-bar progress-bar-danger" style="width: 65%">
                        <span>65%</span>
                    </div>
                </div>

            </div>
            <div class="box-review-list">
                <a class="link_wrap" href="/mobile/review-info.php"></a>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo01.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo02.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo03.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div class="user-bet"><img src="/images/3.jpg"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo04.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
        <div class="thumbnail-review thumbnail-review-index">
            <div class="top-section">
                <a class="link_wrap" href="/mobile/game.php"></a>
                <div class="topic-review">
                    <div class="topic-review-title">
                        <h4>UEFA Champions League</h4>
                        <div class="date-box">03/07/2018</div>
                    </div>

                    <div class="more-box">
                        <h6>เล่นเกมทายผล <i class="fa fa-chevron-right" aria-hidden="true"></i></h6>
                    </div>
                </div>
                <div class="content-top-review">


                    <div class="home-review-box">
                        <div class="logo"><img src="/images/logo-team/manchester.png"></div>
                        <div class="name">Man United</div>
                        <div class="mobile-vote-team active">
                            <h4>Voted</h4>
                            <div class="bg-mobile-vote-team"></div>
                        </div>
                        <ul class="last-match">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                        </ul>
                    </div>
                    <div class="HDP-box-center">
                        <!--                        <div class="time-box">01:00</div>-->
                        <strong>HDP</strong>
                        <h2>-2.50</h2>
                        <div class="box-water-text">
                            <div>1.9</div>
                            <div>1.69</div>
                        </div>
                        <div class="box-chanel">
                            <ul>
                                <li>Live On</li>
                                <li><img src="/images/channel/True-Sport-1-1.png"></li>
                            </ul>
                        </div>
                    </div>


                    <div class="away-review-box">
                        <div class="logo"><img src="/images/logo-team/Chelsea.png"></div>
                        <div class="name">Chelsea</div>
                        <div class="mobile-vote-team">
                            <h4>Vote</h4>
                            <div class="bg-mobile-vote-team"></div>
                        </div>
                        <ul class="last-match">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                        </ul>
                    </div>

                </div>
                <div class="progress">
                    <div class="progress-bar" style="width: 35%">
                        <span>35%</span>
                    </div>
                    <div class="progress-bar progress-bar-danger" style="width: 65%">
                        <span>65%</span>
                    </div>
                </div>

            </div>
            <div class="box-review-list">
                <a class="link_wrap" href="/mobile/review-info.php"></a>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo01.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo02.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo03.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div class="user-bet"><img src="/images/3.jpg"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo04.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
        <div class="thumbnail-review thumbnail-review-index">
            <div class="top-section">
                <a class="link_wrap" href="/mobile/game.php"></a>
                <div class="topic-review">
                    <div class="topic-review-title">
                        <h4>UEFA Champions League</h4>
                        <div class="date-box">03/07/2018</div>
                    </div>

                    <div class="more-box">
                        <h6>เล่นเกมทายผล <i class="fa fa-chevron-right" aria-hidden="true"></i></h6>
                    </div>
                </div>
                <div class="content-top-review">


                    <div class="home-review-box">
                        <div class="logo"><img src="/images/logo-team/manchester.png"></div>
                        <div class="name">Man United</div>
                        <div class="mobile-vote-team active">
                            <h4>Voted</h4>
                            <div class="bg-mobile-vote-team"></div>
                        </div>
                        <ul class="last-match">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                        </ul>
                    </div>
                    <div class="HDP-box-center">
                        <!--                        <div class="time-box">01:00</div>-->
                        <strong>HDP</strong>
                        <h2>-2.50</h2>
                        <div class="box-water-text">
                            <div>1.9</div>
                            <div>1.69</div>
                        </div>
                        <div class="box-chanel">
                            <ul>
                                <li>Live On</li>
                                <li><img src="/images/channel/True-Sport-1-1.png"></li>
                            </ul>
                        </div>
                    </div>


                    <div class="away-review-box">
                        <div class="logo"><img src="/images/logo-team/Chelsea.png"></div>
                        <div class="name">Chelsea</div>
                        <div class="mobile-vote-team">
                            <h4>Vote</h4>
                            <div class="bg-mobile-vote-team"></div>
                        </div>
                        <ul class="last-match">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                        </ul>
                    </div>

                </div>
                <div class="progress">
                    <div class="progress-bar" style="width: 35%">
                        <span>35%</span>
                    </div>
                    <div class="progress-bar progress-bar-danger" style="width: 65%">
                        <span>65%</span>
                    </div>
                </div>

            </div>
            <div class="box-review-list">
                <a class="link_wrap" href="/mobile/review-info.php"></a>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo01.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo02.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo03.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div class="user-bet"><img src="/images/3.jpg"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo04.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
        <div class="thumbnail-review thumbnail-review-index">
            <div class="top-section">
                <a class="link_wrap" href="/mobile/game.php"></a>
                <div class="topic-review">
                    <div class="topic-review-title">
                        <h4>UEFA Champions League</h4>
                        <div class="date-box">03/07/2018</div>
                    </div>

                    <div class="more-box">
                        <h6>เล่นเกมทายผล <i class="fa fa-chevron-right" aria-hidden="true"></i></h6>
                    </div>
                </div>
                <div class="content-top-review">


                    <div class="home-review-box">
                        <div class="logo"><img src="/images/logo-team/manchester.png"></div>
                        <div class="name">Man United</div>
                        <div class="mobile-vote-team active">
                            <h4>Voted</h4>
                            <div class="bg-mobile-vote-team"></div>
                        </div>
                        <ul class="last-match">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                        </ul>
                    </div>
                    <div class="HDP-box-center">
                        <!--                        <div class="time-box">01:00</div>-->
                        <strong>HDP</strong>
                        <h2>-2.50</h2>
                        <div class="box-water-text">
                            <div>1.9</div>
                            <div>1.69</div>
                        </div>
                        <div class="box-chanel">
                            <ul>
                                <li>Live On</li>
                                <li><img src="/images/channel/True-Sport-1-1.png"></li>
                            </ul>
                        </div>
                    </div>


                    <div class="away-review-box">
                        <div class="logo"><img src="/images/logo-team/Chelsea.png"></div>
                        <div class="name">Chelsea</div>
                        <div class="mobile-vote-team">
                            <h4>Vote</h4>
                            <div class="bg-mobile-vote-team"></div>
                        </div>
                        <ul class="last-match">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                        </ul>
                    </div>

                </div>
                <div class="progress">
                    <div class="progress-bar" style="width: 35%">
                        <span>35%</span>
                    </div>
                    <div class="progress-bar progress-bar-danger" style="width: 65%">
                        <span>65%</span>
                    </div>
                </div>

            </div>
            <div class="box-review-list">
                <a class="link_wrap" href="/mobile/review-info.php"></a>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo01.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo02.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo03.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div class="user-bet"><img src="/images/3.jpg"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo04.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
        <div class="thumbnail-review thumbnail-review-index">
            <div class="top-section">
                <a class="link_wrap" href="/mobile/game.php"></a>
                <div class="topic-review">
                    <div class="topic-review-title">
                        <h4>UEFA Champions League</h4>
                        <div class="date-box">03/07/2018</div>
                    </div>

                    <div class="more-box">
                        <h6>เล่นเกมทายผล <i class="fa fa-chevron-right" aria-hidden="true"></i></h6>
                    </div>
                </div>
                <div class="content-top-review">


                    <div class="home-review-box">
                        <div class="logo"><img src="/images/logo-team/manchester.png"></div>
                        <div class="name">Man United</div>
                        <div class="mobile-vote-team active">
                            <h4>Voted</h4>
                            <div class="bg-mobile-vote-team"></div>
                        </div>
                        <ul class="last-match">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                        </ul>
                    </div>
                    <div class="HDP-box-center">
                        <!--                        <div class="time-box">01:00</div>-->
                        <strong>HDP</strong>
                        <h2>-2.50</h2>
                        <div class="box-water-text">
                            <div>1.9</div>
                            <div>1.69</div>
                        </div>
                        <div class="box-chanel">
                            <ul>
                                <li>Live On</li>
                                <li><img src="/images/channel/True-Sport-1-1.png"></li>
                            </ul>
                        </div>
                    </div>


                    <div class="away-review-box">
                        <div class="logo"><img src="/images/logo-team/Chelsea.png"></div>
                        <div class="name">Chelsea</div>
                        <div class="mobile-vote-team">
                            <h4>Vote</h4>
                            <div class="bg-mobile-vote-team"></div>
                        </div>
                        <ul class="last-match">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                        </ul>
                    </div>

                </div>
                <div class="progress">
                    <div class="progress-bar" style="width: 35%">
                        <span>35%</span>
                    </div>
                    <div class="progress-bar progress-bar-danger" style="width: 65%">
                        <span>65%</span>
                    </div>
                </div>

            </div>
            <div class="box-review-list">
                <a class="link_wrap" href="/mobile/review-info.php"></a>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo01.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo02.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo03.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div class="user-bet"><img src="/images/3.jpg"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo04.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
        <div class="thumbnail-review thumbnail-review-index">
            <div class="top-section">
                <a class="link_wrap" href="/mobile/game.php"></a>
                <div class="topic-review">
                    <div class="topic-review-title">
                        <h4>UEFA Champions League</h4>
                        <div class="date-box">03/07/2018</div>
                    </div>

                    <div class="more-box">
                        <h6>เล่นเกมทายผล <i class="fa fa-chevron-right" aria-hidden="true"></i></h6>
                    </div>
                </div>
                <div class="content-top-review">


                    <div class="home-review-box">
                        <div class="logo"><img src="/images/logo-team/manchester.png"></div>
                        <div class="name">Man United</div>
                        <div class="mobile-vote-team active">
                            <h4>Voted</h4>
                            <div class="bg-mobile-vote-team"></div>
                        </div>
                        <ul class="last-match">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                        </ul>
                    </div>
                    <div class="HDP-box-center">
                        <!--                        <div class="time-box">01:00</div>-->
                        <strong>HDP</strong>
                        <h2>-2.50</h2>
                        <div class="box-water-text">
                            <div>1.9</div>
                            <div>1.69</div>
                        </div>
                        <div class="box-chanel">
                            <ul>
                                <li>Live On</li>
                                <li><img src="/images/channel/True-Sport-1-1.png"></li>
                            </ul>
                        </div>
                    </div>


                    <div class="away-review-box">
                        <div class="logo"><img src="/images/logo-team/Chelsea.png"></div>
                        <div class="name">Chelsea</div>
                        <div class="mobile-vote-team">
                            <h4>Vote</h4>
                            <div class="bg-mobile-vote-team"></div>
                        </div>
                        <ul class="last-match">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: #aaaaaa;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: red;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                            <li style="color: dodgerblue;"><i class="fa fa-circle" aria-hidden="true"></i></li>
                        </ul>
                    </div>

                </div>
                <div class="progress">
                    <div class="progress-bar" style="width: 35%">
                        <span>35%</span>
                    </div>
                    <div class="progress-bar progress-bar-danger" style="width: 65%">
                        <span>65%</span>
                    </div>
                </div>

            </div>
            <div class="box-review-list">
                <a class="link_wrap" href="/mobile/review-info.php"></a>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo01.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo02.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo03.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div class="user-bet"><img src="/images/3.jpg"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/Chelsea.png">@Chelsea</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
                <div class="box-review-list-row">
                    <div><img src="/images/profess-player/sample-logo04.png"></div>
                    <div class="text-add-team">
                        <span><img src="/images/logo-team/manchester.png">@Manchester United</span>
                        <div class="detail-review">
                            "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                            ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                            ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                            ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                            ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                            ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                            รัสเซีย
                            ชุดนี้ผสมผสนานระหว่างตัวเก๋ากับดาวรุ่ง มีตัวทีเด็ดอย่าง เฟดอร์ สโมลอฟ
                            กองหน้าตัวเก่ง
                            และ อเล็กซานเดอร์ โกโลวิน ตัวรุกดาวรุ่งน่าจับตา ส่วน ซาอุดีอาระเบีย
                            ชุดนี้มีตัวดังๆ
                            ที่ค้าแข้งในลาลีกาสเปนอย่าง ซาเล็ม อัล ดอว์ซารี่ และ ฟาฮัด อัล มูวัลลัด เจ้าภาพ
                            รัสเซีย ผลงานที่ผ่านมาไม่ค่อยดีเลย แต่ในฐานะเจ้าถิ่นน่าจะพยายามเน้นเต็มที่
                            แถมคู่แข่งที่เจออย่าง ซาอุดีอาระเบีย ชื่อชั้นยังถือว่าเป็นรอง เชื่อว่าเกมนี้
                            รัสเซีย
                            จะเป็นคว้าชัยชนะไปครองได้สำเร็จ แต่สกอร์ไม่น่าขาด เพราะ รัสเซีย
                            ผลงานที่ผ่านมายังทำได้ไม่ดีนัก

                            ฟันธง : รอง ซาอุดีอาระเบีย"
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>
</div>

<?php include("footer.php"); ?>

<script>
    function openNavSide() {
        document.getElementById("Sidenav_ranking").style.width = "250px";
    }

    function closeNavSide() {
        document.getElementById("Sidenav_ranking").style.width = "0";
    }
</script>

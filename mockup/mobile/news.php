


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/mobile/css/all-style.css" rel="stylesheet">
    <link href="/mobile/css/article.css" rel="stylesheet">
    <link href="/mobile/css/bootstrap.css" rel="stylesheet">
    <link href="/mobile/css/docs.css" rel="stylesheet">
    <link href="/mobile/css/game.css" rel="stylesheet">
    <link href="/mobile/css/highlight.css" rel="stylesheet">
    <link href="/mobile/css/index.css" rel="stylesheet">
    <link href="/mobile/css/live-football.css" rel="stylesheet">
    <link href="/mobile/css/news.css" rel="stylesheet">
    <link href="/mobile/css/profile.css" rel="stylesheet">
    <link href="/mobile/css/ranking.css" rel="stylesheet">
    <link href="/mobile/css/review.css" rel="stylesheet">
    <link href="/mobile/css/livescore-mobile.css" rel="stylesheet">
    <link href="/mobile/css/football-symbol-mobile.css" rel="stylesheet">
    <link href="/mobile/css/swiper.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/mobile/css/font-awesome-4.7.0/css/font-awesome.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/js/moment.js"></script>
    <script src="/js/moment-with-locales.min.js"></script>
    <script src="/js/newFB.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<a href="#" id="scroll" style="display: none; bottom: 100px;"><span></span></a>
<?php include("menu-mobile.php"); ?>
<style>

    .content-nav-footer ul li.active-nf-news .nf-news:before,
    .content-nav-footer ul li.active-nf-news span {
        color: rgba(255, 255, 255, 1);
    }
</style>
<!--ข่าวฮอตประจำสัปดาห์-->
<div class="container container-carousel-hothits hide">
    <div class="row">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <a href="#">
                        <div class="swiper-slide-thumbnal">
                            <img src="/images/background4.jpg">
                        </div>
                        <div class="swiper-slide-caption">
                            <p>VARพระเอก! โปรตุเกสเจ๊าอิหร่านระทึก 1-1 ลิ่วชนจอมโหด</p>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="#">
                        <div class="swiper-slide-thumbnal">
                            <img src="/images/3.jpg">
                        </div>
                        <div class="swiper-slide-caption">
                            <p>กอดคอเข้ารอบ! อุรุกวัยแชมป์กลุ่มน็อครัสเซีย 10 คน 3-0</p>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="#">
                        <div class="swiper-slide-thumbnal">
                            <img src="/images/9.jpg">
                        </div>
                        <div class="swiper-slide-caption">
                            <p>ไม่ต้องแก้ตัว! ซาอุฯแซง น.90+4 เชือดอียิปต์ 2-1 หนีบ๊วยสำเร็จ</p>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="#">
                        <div class="swiper-slide-thumbnal">
                            <img src="/images/1.jpg">
                        </div>
                        <div class="swiper-slide-caption">
                            <p>ไฮไลท์ กระชับมิตร Atalanta VS Chiasso</p>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="#">
                        <div class="swiper-slide-thumbnal">
                            <img src="/images/2.jpg">
                        </div>
                        <div class="swiper-slide-caption">
                            <p>ไฮไลท์ กระชับมิตร Atalanta VS Chiasso</p>
                        </div>
                    </a>
                </div>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
        </div>
    </div>

</div>

<div class="tab-top tab-top-article">
    <div class="container title">
        <img src="/images/five-leagues/premier-league-logo.png">
        <h2> พรีเมียร์ลีก</h2>
        <button class="btn btn-link pull-right collapsed" type="button" data-toggle="collapse"
                data-target="#collapseCatNews" aria-expanded="false" aria-controls="collapseCatNews">
        </button>
    </div>

    <div>
        <div class="collapse" id="collapseCatNews">
            <div id="cat-news" class="container-cat-news">
                <span class="active"><img src="/images/five-leagues/premier-league-logo.png"></span>
                <span><img src="/images/five-leagues/La-Liga-Logo-1.png"></span>
                <span><img src="/images/five-leagues/Ligue-1-logo-france.png"></span>
                <span><img src="/images/five-leagues/logo_bl.gif"></span>
                <span><img src="/images/five-leagues/seriea1.png"></span>
                <span><img src="/images/five-leagues/T1_Logo2.png"></span>
                <span><img src="/images/five-leagues/UEFA-Champions-League-1.png"></span>
                <span><img src="/images/five-leagues/uropa.png"></span>
            </div>
        </div>
    </div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#news" data-toggle="tab">ข่าว</a></li>
        <li><a href="#tableStand" data-toggle="tab">อันดับ</a></li>
        <li><a href="#tablematch" data-toggle="tab">ตารางแข่ง</a></li>
        <li><a href="#topplayer" data-toggle="tab">ดาวซัลโว</a></li>
    </ul>


</div>
<div class="tab-content tab-content-article">
    <div class="tab-pane fade in active" id="news">
        <!--    Recent-->
        <div class="container">
            <div class="content-recent">
                <div class="content-item-recent">
                    <a class="link_wrap" href="/mobile/news-info.php"></a>
                    <div class="item-recent">
                        <div><img src="/images/6.jpg"></div>
                        <div class="bx-2">
                            <h4 class="media-heading">เดชองส์ชิงซีดาน! ฟีฟาแบโผ 10 แคนดิเดตโค้ชแห่งปี</h4>
                            <div class="time-box box-inline">4 hours ago</div>
                            <div class="vertical-line-between box-inline"></div>
                            <div class="view-box box-inline">20</div>
                        </div>
                    </div>
                </div>
                <div class="content-item-recent">
                    <a href="/mobile/news-info.php"></a>
                    <div class="item-recent">
                        <div><img src="/images/1.jpg"></div>
                        <div class="bx-2">
                            <h4 class="media-heading">ดาวดังคับคั่ง! ฟีฟ่าแบโผ 10 นักเตะลุ้นคว้าแข้งแห่งปี</h4>
                            <div class="time-box box-inline">4 hours ago</div>
                            <div class="vertical-line-between box-inline"></div>
                            <div class="view-box box-inline">20</div>
                        </div>
                    </div>
                </div>

                <div class="content-item-recent">
                    <a href="/mobile/news-info.php"></a>
                    <div class="item-recent">
                        <div><img src="/images/2.jpg"></div>
                        <div class="bx-2">
                            <h4 class="media-heading">VARดวลกล้องสโมสร!</h4>
                            <div class="time-box box-inline">4 hours ago</div>
                            <div class="vertical-line-between box-inline"></div>
                            <div class="view-box box-inline">20</div>
                        </div>
                    </div>
                </div>
                <div class="content-item-recent">
                    <a href="/mobile/news-info.php"></a>
                    <div class="item-recent">
                        <div><img src="/images/3.jpg"></div>
                        <div class="bx-2">
                            <h4 class="media-heading">ก่อน2ศึกใหญ่! ส.บอลวางแพลน “ช้างศึก” อุ่นเครื่อง 4 แมตช์</h4>
                            <div class="time-box box-inline">4 hours ago</div>
                            <div class="vertical-line-between box-inline"></div>
                            <div class="view-box box-inline">20</div>
                        </div>
                    </div>
                </div>
                <div class="content-item-recent">
                    <a href="/mobile/news-info.php"></a>
                    <div class="item-recent">
                        <div><img src="/images/5.jpg"></div>
                        <div class="bx-2">
                            <h4 class="media-heading">เพื่อความปลอดภัย!กิเลนลดเดินขอบคุณแฟนหลังเหตุปาธงสารัช!</h4>
                            <div class="time-box box-inline">4 hours ago</div>
                            <div class="vertical-line-between box-inline"></div>
                            <div class="view-box box-inline">20</div>
                        </div>
                    </div>
                </div>
                <div class="content-item-recent">
                    <a href="/mobile/news-info.php"></a>
                    <div class="item-recent">
                        <div><img src="/images/6.jpg"></div>
                        <div class="bx-2">
                            <h4 class="media-heading">เดชองส์ชิงซีดาน! ฟีฟาแบโผ 10 แคนดิเดตโค้ชแห่งปี</h4>
                            <div class="time-box box-inline">4 hours ago</div>
                            <div class="vertical-line-between box-inline"></div>
                            <div class="view-box box-inline">20</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="tableStand">
        <div class="container">
            <span class="pull-left">Season 18/19</span>
            <div class="text-right">
                <div class="btn-group" role="group" aria-label="...">
                    <button type="button" class="btn btn-default btn-xs active">ทั้งหมด</button>
                    <button type="button" class="btn btn-default btn-xs">บ้าน</button>
                    <button type="button" class="btn btn-default btn-xs">เยือน</button>
                </div>
            </div>
            <div style="clear: both;"></div>
            <div class="content-table-standing">
                <table>
                    <tbody>
                    <tr>
                        <th></th>
                        <th>Team</th>
                        <th>P</th>
                        <th>GD</th>
                        <th>PTS</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>
                            <img src="/images/logo-team/manchester.png">
                            <p>Manchester united</p>
                        </td>
                        <td>37</td>
                        <td>22</td>
                        <td>60</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>
                            <img src="/images/logo-team/Chelsea.png">
                            <p>Chelsea</p>
                        </td>
                        <td>37</td>
                        <td>22</td>
                        <td>60</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="tablematch">
        <div class="container">
            <div class="box-match-tournament">
                <div class="box-match-list-topic">
                    <div class="pull-left">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <p>15 Apr 18</p>
                    </div>
                </div>
                <div class="box-match-list">

                    <div class="text-center">
                        <div class="date-match">15 Apr 18</div>
                        <div class="time-match">15:00</div>
                    </div>
                    <div>
                        <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                        </div>
                        <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                            united
                        </div>
                    </div>
                    <div>
                        <div class="home-score-match">0</div>
                        <div class="away-score-match">0</div>
                    </div>
                </div>
                <div class="box-match-list">

                    <div class="text-center">
                        <div class="date-match">15 Apr 18</div>
                        <div class="time-match">15:00</div>
                    </div>
                    <div>
                        <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                        </div>
                        <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                            united
                        </div>
                    </div>
                    <div>
                        <div class="home-score-match">0</div>
                        <div class="away-score-match">0</div>
                    </div>
                </div>
            </div>
            <div class="box-match-tournament">
                <div class="box-match-list-topic">
                    <div class="pull-left">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <p>16 Apr 18</p>
                    </div>
                </div>
                <div class="box-match-list">

                    <div class="text-center">
                        <div class="date-match">15 Apr 18</div>
                        <div class="time-match">15:00</div>
                    </div>
                    <div>
                        <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                        </div>
                        <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                            united
                        </div>
                    </div>
                    <div>
                        <div class="home-score-match">0</div>
                        <div class="away-score-match">0</div>
                    </div>
                </div>
                <div class="box-match-list">

                    <div class="text-center">
                        <div class="date-match">15 Apr 18</div>
                        <div class="time-match">15:00</div>
                    </div>
                    <div>
                        <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                        </div>
                        <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                            united
                        </div>
                    </div>
                    <div>
                        <div class="home-score-match">0</div>
                        <div class="away-score-match">0</div>
                    </div>
                </div>
                <div class="box-match-list">

                    <div class="text-center">
                        <div class="date-match">15 Apr 18</div>
                        <div class="time-match">15:00</div>
                    </div>
                    <div>
                        <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                        </div>
                        <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                            united
                        </div>
                    </div>
                    <div>
                        <div class="home-score-match">0</div>
                        <div class="away-score-match">0</div>
                    </div>
                </div>
                <div class="box-match-list">

                    <div class="text-center">
                        <div class="date-match">15 Apr 18</div>
                        <div class="time-match">15:00</div>
                    </div>
                    <div>
                        <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                        </div>
                        <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                            united
                        </div>
                    </div>
                    <div>
                        <div class="home-score-match">0</div>
                        <div class="away-score-match">0</div>
                    </div>
                </div>
                <div class="box-match-list">

                    <div class="text-center">
                        <div class="date-match">15 Apr 18</div>
                        <div class="time-match">15:00</div>
                    </div>
                    <div>
                        <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                        </div>
                        <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                            united
                        </div>
                    </div>
                    <div>
                        <div class="home-score-match">0</div>
                        <div class="away-score-match">0</div>
                    </div>
                </div>
                <div class="box-match-list">

                    <div class="text-center">
                        <div class="date-match">15 Apr 18</div>
                        <div class="time-match">15:00</div>
                    </div>
                    <div>
                        <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                        </div>
                        <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                            united
                        </div>
                    </div>
                    <div>
                        <div class="home-score-match">0</div>
                        <div class="away-score-match">0</div>
                    </div>
                </div>
                <div class="box-match-list">

                    <div class="text-center">
                        <div class="date-match">15 Apr 18</div>
                        <div class="time-match">15:00</div>
                    </div>
                    <div>
                        <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                        </div>
                        <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                            united
                        </div>
                    </div>
                    <div>
                        <div class="home-score-match">0</div>
                        <div class="away-score-match">0</div>
                    </div>
                </div>
                <div class="box-match-list">

                    <div class="text-center">
                        <div class="date-match">15 Apr 18</div>
                        <div class="time-match">15:00</div>
                    </div>
                    <div>
                        <div class="home-name-match"><img src="/images/logo-team/Chelsea.png">Chelsea
                        </div>
                        <div class="away-name-match"><img src="/images/logo-team/manchester.png">Manchester
                            united
                        </div>
                    </div>
                    <div>
                        <div class="home-score-match">0</div>
                        <div class="away-score-match">0</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="topplayer">
        <div class="table-topscore">
            <table>
                <tr>
                    <td>1</td>
                    <td><img src="/images/player/image_4.jpg"></td>
                    <td>
                        <span>Eden Hazard</span>
                        <img src="/images/logo-team/Chelsea.png">
                        <p>Chelsea</p>
                    </td>
                    <td><img src="/images/football.png">23</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td><img src="/images/player/image_5.jpg"></td>
                    <td>
                        <span>Romelu Lukaku</span>
                        <img src="/images/logo-team/manchester.png">
                        <p>Manchester United</p>
                    </td>
                    <td><img src="/images/football.png">19</td>
                </tr>

            </table>
        </div>
    </div>
<<<<<<< HEAD
</div>



<?php include("footer.php"); ?>

<style>
    body {
        background-color: #f1f1f1 !important;
    }

    .active-nf-livescore .nf-livescore:before,
    .active-nf-livescore span{
        color: #fff!important;
    }
</style>





<div class="container-livescore" style="padding-bottom: 80px;">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs nav-liveScore" role="tablist" data-spy="affix" data-offset-top="60">
        <li role="presentation"><a href="#Result" aria-controls="home" role="tab" data-toggle="tab">Result</a></li>
        <li role="presentation" class="active"><a href="#Today_live" aria-controls="profile" role="tab"
                                                  data-toggle="tab">Today <span>(15)</span></a></li>
        <li role="presentation"><a class="tab-live-top" href="#Tap_live" aria-controls="messages" role="tab"
                                   data-toggle="tab">Live <span>(2)</span></a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content tab-content-livescore">
        <div role="tabpanel" class="tab-pane" id="Result">
            <div class="content-result-livescore">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-tabs-result bt-flex" role="tablist">
                    <li role="presentation" class="active"><a href="#result-today" aria-controls="home" role="tab" data-toggle="tab">Today</a></li>
                    <li role="presentation"><a href="#result-yesterday" aria-controls="profile" role="tab" data-toggle="tab">Yesterday</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="result-today">
                        <div class="container">
                            <!--                FT-->
                            <div class="inline-box full-width">
                                <div class="main-live-box full-width inline-box ">
                                    <div class="title-league inline-box full-width">
                                        <img src="/images/five-leagues/premier-league-logo.png">
                                        <h5>Premier league</h5>
                                    </div>
                                    <div class="panel-group inline-box full-width" id="accordion" role="tablist"
                                         aria-multiselectable="true">
                                        <div class="panel row-live-box">
                                            <div class="panel-heading" role="tab" id="headingOne-1">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapseOne-1" aria-expanded="true" aria-controls="collapseOne-1">
                                                    <div class="end-time">FT</div>
                                                    <div class="home-side-team">
                                                        <div class="pull-right">
                                                            <div class="red-card"></div>
                                                            <h5>Arsenal</h5>
                                                            <img src="/images/logo-team/Arsenal.png">
                                                        </div>
                                                    </div>
                                                    <div class="center-row score-live-ft">
                                                        <ul>
                                                            <li>1</li>
                                                            <li>-</li>
                                                            <li>0</li>
                                                        </ul>
                                                    </div>
                                                    <div class="away-side-team">
                                                        <div class="pull-left">
                                                            <img src="/images/logo-team/Bournemouth.png">
                                                            <h5>Bournemouth</h5>
                                                            <div class="red-card"></div>
                                                            <div class="red-card"></div>
                                                        </div>
                                                    </div>
                                                </a>

                                            </div>
                                            <div id="collapseOne-1" class="panel-collapse collapse in" role="tabpanel"
                                                 aria-labelledby="headingOne-1">
                                                <?php include("livescore-information-tab-ft.php"); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="result-yesterday">Yesterday</div>
                </div>

            </div>

        </div>
        <div role="tabpanel" class="tab-pane active" id="Today_live">
            <div class="container">
                <div class="topic-livescore bt-flex full-width">
                    <h2>Live Score
                        <small>12/12/2019</small>
                    </h2>
                    <div class="form-group">
                        <div class="pull-right">
                            <span>Odd</span>
                            <div class="checkbox checbox-switch switch-primary">
                                <label>
                                    <input type="checkbox" name="" checked=""/>
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <!--                Live Score-->
                <div class="status-live inline-box full-width">
                    <div class="main-live-box full-width inline-box ">
                        <div class="title-league inline-box full-width">
                            <img src="/images/five-leagues/premier-league-logo.png">
                            <h5>Premier league</h5>
                        </div>
                        <div class="panel-group inline-box full-width" id="accordion" role="tablist"
                             aria-multiselectable="true">
                            <div class="panel row-live-box">
                                <div class="panel-heading" role="tab" id="headingOne-1">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseOne-1" aria-expanded="true" aria-controls="collapseOne-1">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <div class="red-card"></div>
                                                <h5>Arsenal</h5>
                                                <img src="/images/logo-team/Arsenal.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>0</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Bournemouth.png">
                                                <h5>Bournemouth</h5>
                                            </div>
                                        </div>
                                    </a>

                                </div>
                                <div id="collapseOne-1" class="panel-collapse collapse in" role="tabpanel"
                                     aria-labelledby="headingOne-1">
                                    <?php include("livescore-information-tab.php"); ?>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading" role="tab" id="headingOne-2">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseOne-2" aria-expanded="true" aria-controls="collapseOne-2">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <div class="red-card"></div>
                                                <h5>Arsenal</h5>
                                                <img src="/images/logo-team/Arsenal.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>0</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Bournemouth.png">
                                                <h5>Bournemouth</h5>
                                            </div>
                                        </div>
                                    </a>

                                </div>
                                <div id="collapseOne-2" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingOne-2">
                                    <?php include("livescore-information-tab.php"); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <!--                None Live-->
                <div class="inline-box full-width">
                    <div class="main-live-box full-width inline-box ">
                        <div class="title-league inline-box full-width">
                            <img src="/images/five-leagues/premier-league-logo.png">
                            <h5>Premier league</h5>
                        </div>
                        <div class="panel-group inline-box full-width" id="accordion" role="tablist"
                             aria-multiselectable="true">
                            <div class="panel row-live-box">
                                <div class="panel-heading" role="tab" id="headingOne-3">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseOne-3" aria-expanded="true" aria-controls="collapseOne-3">
                                        <div class="time-match-default">20:30</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <h5>Arsenal</h5>
                                                <img src="/images/logo-team/Arsenal.png">
                                            </div>
                                        </div>
                                        <div class="center-row row-hdp">
                                            <div class="title-center">
                                                HDP
                                            </div>
                                            <strong>2.50</strong>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Bournemouth.png">
                                                <h5>Bournemouth</h5>
                                            </div>
                                        </div>
                                    </a>

                                </div>
                                <div id="collapseOne-1" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingOne-3">

                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading" role="tab" id="headingOne-4">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseOne-4" aria-expanded="true" aria-controls="collapseOne-4">
                                        <div class="time-match-default">20:30</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <h5>Arsenal</h5>
                                                <img src="/images/logo-team/Arsenal.png">
                                            </div>
                                        </div>
                                        <div class="center-row row-hdp">
                                            <div class="title-center">
                                                HDP
                                            </div>
                                            <strong>2.50</strong>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Bournemouth.png">
                                                <h5>Bournemouth</h5>
                                            </div>
                                        </div>
                                    </a>

                                </div>
                                <div id="collapseOne-2" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingOne-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="main-live-box full-width inline-box ">
                        <div class="title-league inline-box full-width">
                            <img src="/images/five-leagues/premier-league-logo.png">
                            <h5>Premier league</h5>
                        </div>
                        <div class="panel-group inline-box full-width" id="accordion" role="tablist"
                             aria-multiselectable="true">
                            <div class="panel row-live-box">
                                <div class="panel-heading" role="tab" id="headingOne-3">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseOne-3" aria-expanded="true" aria-controls="collapseOne-3">
                                        <div class="time-match-default">20:30</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <h5>Arsenal</h5>
                                                <img src="/images/logo-team/Arsenal.png">
                                            </div>
                                        </div>
                                        <div class="center-row row-hdp">
                                            <div class="title-center">
                                                HDP
                                            </div>
                                            <strong>2.50</strong>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Bournemouth.png">
                                                <h5>Bournemouth</h5>
                                            </div>
                                        </div>
                                    </a>

                                </div>
                                <div id="collapseOne-1" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingOne-3">

                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading" role="tab" id="headingOne-4">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseOne-4" aria-expanded="true" aria-controls="collapseOne-4">
                                        <div class="time-match-default">20:30</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <h5>Arsenal</h5>
                                                <img src="/images/logo-team/Arsenal.png">
                                            </div>
                                        </div>
                                        <div class="center-row row-hdp">
                                            <div class="title-center">
                                                HDP
                                            </div>
                                            <strong>2.50</strong>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Bournemouth.png">
                                                <h5>Bournemouth</h5>
                                            </div>
                                        </div>
                                    </a>

                                </div>
                                <div id="collapseOne-2" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingOne-4">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="Tap_live">
            <div class="container">
                <div class="topic-livescore bt-flex full-width">
                    <h2>Live Score
                        <small>12/12/2019</small>
                    </h2>
                    <div class="form-group">
                        <div class="pull-right">
                            <span>Odd</span>
                            <div class="checkbox checbox-switch switch-primary">
                                <label>
                                    <input type="checkbox" name="" checked=""/>
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <!--                Live Score-->
                <div class="status-live inline-box inline-box">
                    <div class="main-live-box full-width inline-box ">
                        <div class="title-league pull-left">
                            <img src="/images/five-leagues/premier-league-logo.png">
                            <h5>Premier league</h5>
                        </div>
                        <div class="inline-box full-width">
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <div class="red-card"></div>
                                                <h5>Arsenal</h5>
                                                <img src="/images/logo-team/Arsenal.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>0</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Bournemouth.png">
                                                <h5>Bournemouth</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <div class="red-card"></div>
                                                <div class="red-card"></div>
                                                <h5>Brighton</h5>
                                                <img src="/images/logo-team/Brighton.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>3</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Burnley.png">
                                                <h5>Burnley</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">32'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <h5>Chelsea</h5>
                                                <img src="/images/logo-team/Chelsea.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>0</li>
                                                <li>-</li>
                                                <li>3</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Everton.png">
                                                <h5>Everton</h5>
                                                <div class="red-card"></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">45+3'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <h5>Liverpool</h5>
                                                <img src="/images/logo-team/Liverpool.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>3</li>
                                                <li>-</li>
                                                <li>1</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/manchester.png">
                                                <h5>manchester</h5>
                                                <div class="red-card"></div>
                                                <div class="red-card"></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="main-live-box full-width inline-box ">
                        <div class="title-league pull-left">
                            <img src="/images/five-leagues/La-Liga-Logo-1.png">
                            <h5>La Liga</h5>
                        </div>
                        <div class="inline-box full-width">
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <div class="red-card"></div>
                                                <h5>Arsenal</h5>
                                                <img src="/images/logo-team/Arsenal.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>0</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Bournemouth.png">
                                                <h5>Bournemouth</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="panel row-live-box">
                                <div class="panel-heading">
                                    <a href="#">
                                        <div class="time-live">15'</div>
                                        <div class="home-side-team">
                                            <div class="pull-right">
                                                <div class="red-card"></div>
                                                <div class="red-card"></div>
                                                <h5>Brighton</h5>
                                                <img src="/images/logo-team/Brighton.png">
                                            </div>
                                        </div>
                                        <div class="center-row score-live">
                                            <ul>
                                                <li>1</li>
                                                <li>-</li>
                                                <li>3</li>
                                            </ul>
                                        </div>
                                        <div class="away-side-team">
                                            <div class="pull-left">
                                                <img src="/images/logo-team/Burnley.png">
                                                <h5>Burnley</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $('#accordion').on('shown.bs.collapse', function () {

        var panel = $(this).find('.in');

        $('html, body').animate({
            scrollTop: panel.offset().top
        }, 1000);

    });
</script>











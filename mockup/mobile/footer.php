<div class="content-nav-footer">
    <div class="container">
        <ul class="bt-flex">
            <li class="active-nf-livescore">
                <a href="/mobile">
                    <div class="nf-livescore"></div>
                    <span>Live Score</span>
                </a>
            </li>
            <li class="active-nf-highlight">
                <a href="/mobile/highlight-football.php">
                    <div class="nf-highlight"></div>
                    <span>Highlight</span>
                </a>
            </li>
            <li class="active-nf-news">
                <a href="/mobile/news.php">
                    <div class="nf-news"></div>
                    <span>News</span>
                </a>
            </li>
            <li class="active-nf-sexy">
                <a href="/mobile/sexy-football.php">
                    <div class="nf-sexy"></div>
                    <span>Sexy</span>
                </a>
            </li>
            <li class="active-nf-profile">
                <a href="/mobile/profile.php">
                    <div class="nf-profile"></div>
                    <span>Profile</span>
                </a>
            </li>
        </ul>
    </div>
</div>

<!--<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>-->
<script type="text/javascript" src="/mobile/js/grid-layout.js"></script>
<script type="text/javascript" src="/mobile/js/bootstrap.js"></script>
<script type="text/javascript" src="/mobile/js/swiper.min.js"></script>
<script type="text/javascript">
    refreshTime();
    function refreshTime() {
        $.each($(".how-long"), function (k, v) {
            if ($.isNumeric($(this).text())) {
                $(this).html(moment($(this).text(), "X").fromNow())
            } else {
                $(this).html(moment($(this).text(), "YYYY-MM-DD HH:mm:ss").fromNow())
            }
        })

    }
</script>
<script type="text/javascript">
    (function($) {
        var swiper = new Swiper('.swiper-container', {
            pagination: {
                el: '.swiper-pagination',
                dynamicBullets: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    })(jQuery);
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('#scroll').fadeIn();
            } else {
                $('#scroll').fadeOut();
            }
        });
        $('#scroll').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });
    });
</script>


<script>
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;
        if (prevScrollpos > currentScrollPos) {
            document.getElementById("navbar-scroll").style.top = "0";
        } else {
            document.getElementById("navbar-scroll").style.top = "-80px";
        }
        prevScrollpos = currentScrollPos;
    }

</script>


<script type="text/javascript">
    $("#selectTeams span").click(function(){
        // Remove active if this does not have active class
        if(!($(this).closest("span").hasClass("active"))){
            $(this).closest("#selectTeams").find("span.active").removeClass('active');
        }
        $(this).closest("span").addClass('active');
    });

    $("#cat-news span").click(function(){
        // Remove active if this does not have active class
        if(!($(this).closest("span").hasClass("active"))){
            $(this).closest("#cat-news").find("span.active").removeClass('active');
        }
        $(this).closest("span").addClass('active');
    });

    $("#list_game div.thumbnail").click(function(){
        // Remove active if this does not have active class
        if(!($(this).closest("span").hasClass("active"))){
            $(this).closest("#list_game").find("div.thumbnail.active").removeClass('active');
        }
        $(this).closest("div").addClass('active');
    });
</script>


<script type="text/javascript">
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main-review").style.marginRight = "250px";
        document.getElementById("main-review").style.right = "0";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main-review").style.marginRight = "0";
        document.getElementById("main-review").style.right = "0";
    }
</script>
<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "100%";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
</script>
</body>
</html>

<nav id="navbar-scroll" class="navbar navbar-mobile">
    <div class="top-navbar">
        <?php if(isset($_SESSION['login'])){ ?>
            <div class="box-profile-menu" style="display: block; width: 100%;">
                <ul class="box-coin-menu pull-left">
                    <li>
                        <img src="/images/coin/sgold100.png">
                        <p><?php echo number_format($_SESSION['login']['gold'], 2) ?></p>
                    </li>
                    <li>
                        <img src="/images/coin/scoin100.png">
                        <p><?php echo number_format($_SESSION['login']['coin'], 2) ?></p>
                    </li>
                </ul>
                <ul class="profile-image pull-right">
                    <li role="presentation" class="dropdown ">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            <img src="<?= ((isset($_SESSION['login'])) ? 'https://graph.facebook.com/v2.8/' . $_SESSION['login']['fb_uid'] . '/picture' : '') ?>">
                            <span class="glyphicon glyphicon-option-vertical" aria-hidden="true"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="/mobile/profile.php">การตั้งค่า</a></li>
                            <li><a href="/mobile/ranking.php">Ranking</a></li>
                            <li><a href="/mobile/rules.php">กติกาและเงื่อนไข</a></li>
                            <li><a href="/mobile/help.php">วิธีการเล่นเกมทายผล</a></li>
                            <li><a href="/logout.php">ออกจากระบบ</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        <?php }else{  ?>
            <div class="menu-login pull-right" style="">
                <ul class="box-login-mobile">
                    <li>
                        <a class="fb-login-button" onclick="FBlogin();">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                            <span>Login</span>
                        </a>
                    </li>
                </ul>
            </div>
        <?php } ?>
    </div>
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed hide" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
            </button>
            <span onclick="openNav()">&#9776;</span>
            <a class="navbar-brand" href="/mobile"><img src="/images/logo/logo-ngoal-white.png"></a>
        </div>
        <div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <ul class="nav sidebar-nav text-center">
                <li><a href="/mobile">หน้าแรก</a></li>
                <li class="text-event">
                    <a href="/">
                        <!--                        <img src="/images/source.gif">-->
                        <span class="blink"> แจกรางวัล ฟรี!</span>
                    </a>
                </li>
                <li><a href="/mobile/highlight-football.php">วิดีโอ/ไฮไลท์บอล</a></li>
                <li class="collapse-box hide">
                    <a class="collapsed" data-toggle="collapse" href="#collapseinterNews" aria-expanded="true"
                       aria-controls="collapseinterNews">
                        ข่าวบอลต่างประเทศ
                    </a>
                    <div class="collapse" id="collapseinterNews">
                        <div class="box-collapse">
                            <ul>
                                <li><a href="/mobile/news.php">ข่าวพรีเมียร์ลีก</a></li>
                                <li><a href="/mobile/news.php">ข่าวลาลีกา</a></li>
                                <li><a href="/mobile/news.php">ข่าวบุนเดสลีกา</a></li>
                                <li><a href="/mobile/news.php">ข่าวซีเรียอา</a></li>
                                <li><a href="/mobile/news.php">ข่าวยูฟ่าแชมเปียนลีก</a></li>
                                <li><a href="/ mobile/news.php">ข่าวยูโรป้า</a></li>
                                <li><a href="/mobile/news.php">ข่าวไทยพรีเมียลีก</a></li>
                                <li><a href="/mobile/news.php">ข่าวยูฟ่าแชมเปียนลีก</a></li>
                                <li><a href="/mobile/news.php">ข่าวทีมชาติ</a></li>
                                <li><a href="/mobile/news.php">ข่าวฟุตบอลลีกอื่น</a></li>
                            </ul>
                        </div>
                    </div>

                </li>
                <li><a href="/mobile/news.php">ข่าวบอลฟุตบอล</a></li>
                <!--                <li><a href="/mobile/news.php">ข่าวกีฬาอื่นๆ</a></li>-->
                <li><a href="/mobile/hothits-video.php">Clip HotHit</a></li>
                <li><a href="/mobile/sexy-football.php">SexyFootball</a></li>
                <li><a href="/mobile/game.php">เกมทายผล</a></li>
                <li><a href="/mobile/review.php">ทีเด็ดทรรศนะ</a></li>
                <li><a href="/mobile/live-football.php">บอลสด</a></li>

            </ul>
        </div>
        <div id="navbar" class="collapse navbar-collapse hide">
            <ul class="nav sidebar-nav">
                <li><a href="/mobile"><i class="fa fa-home" aria-hidden="true"></i> หน้าแรก</a></li>
                <li class="text-event">
                    <a href="/">
                        <img src="/images/source.gif">
                        <span class="blink"> แจกรางวัล ฟรี!</span>
                    </a>
                </li>
                <li><a href="/mobile/highlight-football.php"><i class="fa fa-play" aria-hidden="true"></i>
                        วิดีโอ/ไฮไลท์บอล</a></li>
                <li class="collapse-box">
                    <a class="collapsed" data-toggle="collapse" href="#collapseinterNews" aria-expanded="true"
                       aria-controls="collapseinterNews">
                        <i class="fa fa-newspaper-o" aria-hidden="true"></i> ข่าวบอลต่างประเทศ
                    </a>
                    <div class="collapse" id="collapseinterNews">
                        <div class="box-collapse">
                            <ul>
                                <li><a href="/mobile/news.php">ข่าวพรีเมียร์ลีก</a></li>
                                <li><a href="/mobile/news.php">ข่าวลาลีกา</a></li>
                                <li><a href="/mobile/news.php">ข่าวบุนเดสลีกา</a></li>
                                <li><a href="/mobile/news.php">ข่าวซีเรียอา</a></li>
                                <li><a href="/mobile/news.php">ข่าวยูฟ่าแชมเปียนลีก</a></li>
                                <li><a href="/mobile/news.php">ข่าวยูโรป้า</a></li>
                                <li><a href="/mobile/news.php">ข่าวไทยพรีเมียลีก</a></li>
                                <li><a href="/mobile/news.php">ข่าวยูฟ่าแชมเปียนลีก</a></li>
                                <li><a href="/mobile/news.php">ข่าวทีมชาติ</a></li>
                                <li><a href="/mobile/news.php">ข่าวฟุตบอลลีกอื่น</a></li>
                            </ul>
                        </div>
                    </div>

                </li>
                <li><a href="/mobile/news.php"><i class="fa fa-newspaper-o" aria-hidden="true"></i> ข่าวบอลไทย</a></li>
                <li><a href="/mobile/news.php"><i class="fa fa-newspaper-o" aria-hidden="true"></i> ข่าวกีฬาอื่นๆ</a>
                </li>
                <li><a href="/mobile/hothits-video.php"><i class="fa fa-fire" aria-hidden="true"></i> Clip HotHit</a>
                </li>
                <li><a href="/mobile/sexy-football.php"><i class="fa fa-female" aria-hidden="true"></i> Sexy
                        Football</a></li>
                <li><a href="/mobile/game.php"><i class="fa fa-gamepad" aria-hidden="true"></i> เกมทายผล</a></li>
                <li><a href="/mobile/review.php"><i class="fa fa-keyboard-o" aria-hidden="true"></i> ทีเด็ดทรรศนะ</a>
                </li>
                <li><a href="/mobile/live-football.php"><i style="color: red;" class="fa fa-video-camera"
                                                           aria-hidden="true"></i> บอลสด</a></li>
                <!--                <li><a href="/mobile/rules.php"><i class="fa fa-bullhorn" aria-hidden="true"></i> กติกาและเงื่อนไข</a>-->
                <!--                </li>-->
                <!--                <li><a href="/mobile/ranking.php"><i class="fa fa-trophy" aria-hidden="true"></i> Ranking</a></li>-->
                <!--                <li><a href="/mobile/help.php"><i class="fa fa-file-text-o" aria-hidden="true"></i> วิธีการเล่นเกมทายผล</a>-->
                <!--                </li>-->
            </ul>
            <footer class="text-center">
                COPYRIGHT 2018 Ngoal.com
            </footer>
        </div>
    </div>
</nav>
<<<<<<< HEAD
<?php include("header.php"); ?>
=======
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/mobile/css/all-style.css" rel="stylesheet">
    <link href="/mobile/css/article.css" rel="stylesheet">
    <link href="/mobile/css/bootstrap.css" rel="stylesheet">
    <link href="/mobile/css/docs.css" rel="stylesheet">
    <link href="/mobile/css/game.css" rel="stylesheet">
    <link href="/mobile/css/highlight.css" rel="stylesheet">
    <link href="/mobile/css/index.css" rel="stylesheet">
    <link href="/mobile/css/live-football.css" rel="stylesheet">
    <link href="/mobile/css/news.css" rel="stylesheet">
    <link href="/mobile/css/profile.css" rel="stylesheet">
    <link href="/mobile/css/ranking.css" rel="stylesheet">
    <link href="/mobile/css/review.css" rel="stylesheet">
    <link href="/mobile/css/livescore-mobile.css" rel="stylesheet">
    <link href="/mobile/css/football-symbol-mobile.css" rel="stylesheet">
    <link href="/mobile/css/swiper.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/mobile/css/font-awesome-4.7.0/css/font-awesome.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/js/moment.js"></script>
    <script src="/js/moment-with-locales.min.js"></script>
    <script src="/js/newFB.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
>>>>>>> e1fd48f084c292f14b3850b6843e93867bf53552
<?php include("menu-mobile.php"); ?>
    <div class="container">
        <div class="text-title">
            <h4>กติกาและเงื่อนไข</h4>
            <p>
                <a href="/mobile/game.php"><i class="fa fa-gamepad" aria-hidden="true"> เล่นเกม</i></a>
            </p>
        </div>
    </div>
    <div class="container container-rules">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                           aria-expanded="true" aria-controls="collapseOne">
                            กติกาและเงื่อนไข
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <ul>
                            <li>ผู้เล่นต้องสมัครเล่นเกมผ่าน ID Facebook</li>
                            <li>กรณีที่สมัครเล่นแบบปกติ ผู้เล่นต้องทำการยืนยันตัวตนผ่าน E-mail
                                ก่อนจึงจะสามารถเล่นเกมทายผลได้
                            </li>
                            <li>ผู้เล่นที่ใช้ ID Facebook สมัครเล่นเกม จะต้องเป็น ID จริงของตัวเองเท่านั้น
                                หากตรวจพบว่ามีการสร้าง ID ปลอมจะทำการตัดสิทธิ์
                            </li>
                            <li>ผู้เล่นที่สามารถรับรางวัลได้ ID Facebook ต้องมีอายุเกิน 3 เดือนขึ้นไป</li>
                            <li> ทางเว็บไซต์จะทำการสรุปผู้ที่ได้รับรางวัลทุกวันที่ 16 และ 1 ของทุกเดือน</li>
                            <li> คะแนนสะสมแต่ละประเภทจะปรับเหลือ 0 เพื่อเริ่มต้นใหม่ทุกวันที่ 16 และ 1 ของเดือน</li>
                            <li> ผู้ที่ได้รับรางวัลทุกรางวัลต้องทำการยืนยันการขอรับรางวัลภายในวันที่ 2-3</li>
                            <li> เกมทายผลสามารถเล่นได้ทุกคู่ที่มีการเปิดอัตราต่อรอง</li>
                            <li>คำตัดสินของทางเว็บ ถือเป็นที่สิ้นสุด</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Sgoal และ Scoin คืออะไร
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        <h4><img src="/images/coin/sgold100.png"> Sgoal</h4>
                        <ul>
                            <li>เหรียญที่ได้มาจากการเล่นเกมทายผล</li>
                            <li>การแลกคอมโบรายวัน</li>
                            <li>การแลกคอมโบสะสม</li>
                            <li>Sgoal ที่ได้มาจะใช้จัดอับดับแข่งขันแต่ละเดือน</li>
                            <li>Sgoal สามารถใช้แลกรางวัลของแต่ละเดือน</li>
                            <li>Sgoal จะเริ่มต้นที่ 0</li>
                            <li>Sgoal จะติดลบเมื่อผู้เล่นทายผลผิด</li>
                            <li>Sgoal ติดลบสามารถรีเช็ตให้เป็น 0 ได้ ทุกๆ 15 วัน</li>
                        </ul>
                        <h4><img src="/images/coin/scoin100.png"> Scoin</h4>
                        <ul>
                            <li>เหรียญที่ใช้เล่นเกมทายผล</li>
                            <li>ได้จากการ login ในแต่ละวัน</li>
                            <li>สมัครสมาชิกเล่นเกมครั้งแรกจะได้รับ 5,000 Scoin</li>
                            <li>Scoin สามารถใช้ Sgoal แลกได้ โดย 1 Sgoal = 50 Scoin</li>
                        </ul>


                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            การเล่นเกมทายผล
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                     aria-labelledby="headingThree">
                    <div class="panel-body">

                        <div class="alert alert-info" role="alert"><strong>Heads up!</strong>
                            ** ใช้ Scoin ในการเล่นเกมทายผล โดยจำนวน Scoin ที่ใช้แบ่งเป็น 5 ช่วงดังนี้ 200 400 600
                            800
                            1200
                            ตามลำดับ **
                        </div>
                        <div class="mini-title">
                            <h3>การจัดอันดับ<br>คะแนนมี 2 แบบ</h3>
                        </div>
                        <ul>
                            <li>W = win, w = winhaif, L = lose, l = losehaif, D = draw</li>
                            <li>
                                **ไม่สามารถแก้ไขเปลี่ยนทีมที่เลือกเล่นในแต่ละวันได้ควรตรวจสอบให้มั่นใจก่อนยืนยันการเล่นทุกครั้ง**
                            </li>
                            <li>Ranking และ Sgoal</li>
                            <li>อันดับแต่ละแบบประกอบด้วย อันดับทั้งหมด, อันดับ Now (15 วัน), อันดับ Last champ (30
                                วัน)
                            </li>
                        </ul>
                        <div class="mini-title">
                            <h3>1. อันดับความแม่นยำ<br>Ranking</h3>
                        </div>
                        <ul>
                            <li>ผลชนะเต็ม (W) 100% ความแม่น</li>
                            <li>ผลชนะครึ่ง (w) 100% ความแม่น</li>
                            <li>ผลเสมอ (D) 0% ความแม่น</li>
                            <li>ผลแพ้ครึ่ง (l) -100% ความแม่น</li>
                            <li>ผลแพ้เต็ม (L) -100% ความแม่น</li>
                            <li>ตัวอย่างการนับความแม่น เล่น 3 คู่ ชนะ 2 แพ้ 1 เท่ากับ 66.66 ความแม่นยำ</li>
                            <li>หากคะแนนความแม่นยำเท่ากันจะให้ความสำคัญกับจำนวนคู่เล่นที่มากกว่า</li>
                        </ul>
                        <div class="mini-title">
                            <h3>2. อันดับ Sgoal</h3>
                        </div>
                        <ul>
                            <li>คำนวน Sgoal จาก Sgoal = Scoin x ค่าน้ำ</li>
                            <li>**กรณี win (ตัวอย่าง เล่น 800 Scoin ค่าน้ำ 0.90 จะได้รับ Sgoal 720)</li>
                            <li>**กรณี lose ค่า Sgoal จะติดลบ (ตัวอย่าง เล่น 800 Scoin ค่าน้ำ 0.90 เสีย Sgoal 720)
                            </li>
                            <li>ผลชนะเต็ม (W) ตัวคูณค่าน้ำจะคิดเต็ม (0.90)</li>
                            <li>ผลชนะครึ่ง (w) ตัวคูณค่าน้ำจะคิดครึ่ง (0.45)</li>
                            <li> ผลเสมอ (D) ไม่นำมาคิด</li>
                            <li>ผลแพ้ครึ่ง (l) ตัวคูณค่าน้ำจะคิดครึ่ง (-0.45)</li>
                            <li> ผลแพ้เต็ม (L) ตัวคูณค่าน้ำจะคิดเต็ม (-0.90)</li>
                            <li> หากคะแนนความแม่นยำเท่ากันจะให้ความสำคัญกับจำนวนคู่เล่นที่มากกว่า</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFour">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            คอมโบรายวัน คืออะไร
                        </a>
                    </h4>
                </div>
                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel"
                     aria-labelledby="headingFour">
                    <div class="panel-body">
                        <ul>
                            <li>คอมโบรายวันหรือ Step มีสองแบบคือ win ทั้งหมด และ lose
                                ทั้งหมดของคู่บอลที่เล่นในวันนั้น
                                โดยเริ่มต้น 3 คู่ขึ้นไป
                            </li>
                            <li>รางวัลที่ได้จากคอมโบรายวันคือ Sgoal</li>
                            <li> โดยจะคิดรางวัลให้เฉพาะคู่ที่ win และ lose เท่านั้น กรณี draw
                                จะไม่คิดรางวัลให้แต่ยังอยู่ในเงื่อนไขการได้คอมโบ
                            </li>
                            <li> ตัวอย่าง เล่น 3 คู่ win 2 draw 1 คู่ที่นำมาคิดคอมโบเฉพาะคู่ที่ win (3 คู่ เล่นคู่ละ
                                400
                                win
                                2
                                ได้รับ Sgoal 800 ตามค่าน้ำที่เล่น)
                            </li>
                            <li> คอมโบจะตัดรอบเวลา 12.00 น. ของทุกวัน</li>
                            <li> รางวัลที่ได้จะส่งเข้า statement ทันที</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFive">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            คอมโบสะสม คืออะไร
                        </a>
                    </h4>
                </div>
                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel"
                     aria-labelledby="headingFour">
                    <div class="panel-body">
                        <ul>
                            <li>รางวัลใหญ่มีโอกาศได้รับ Sgoal สูงสุดถึง 300,000 Sgoal</li>
                            <li> คอมโบสะสมหรือ Step ต่อเนื่อง มีสองแบบคือ win ทั้งหมด และ lose
                                ทั้งหมดของคู่บอลที่เล่น
                            </li>
                            <li> รางวัลที่ได้จากคอมโบสะสมคือ Sgoal</li>
                            <li> ช่วงของคอมโบสะสมที่แจก Sgoal
                                <ul>
                                    <li> 4 คู่ต่อเนื่อง 5000 Sgoal</li>
                                    <li> 7 คู่ต่อเนื่อง 10,000 Sgoal</li>
                                    <li> 10 คู่ต่อเนื่อง 50,000 Sgoal</li>
                                    <li> 15 คู่ต่อเนื่อง 100,000 Sgoal</li>
                                    <li> 20 คู่ต่อเนื่อง 300,000 Sgoal</li>
                                </ul>
                            </li>
                            <li> โดยจะคิดรางวัลให้เฉพาะคู่ที่ win และ lose เท่านั้น กรณี draw
                                จะไม่คิดรางวัลให้แต่ยังอยู่ในเงื่อนไขการได้คอมโบสะสม
                            </li>
                            <li> ตัวอย่าง เล่น 3 คู่ win 2 draw 1 คู่ที่นำมาคิดคอมโบสะสมเฉพาะคู่ที่ win</li>
                            <li> คอมโบสะสมจะตัดรอบเวลา 12.00 น. ของทุกวัน</li>
                            <li> ช่วงที่กดแลก คอมโบสะสมได่คือ 12.00 - 15.00 ของทุกวัน</li>
                            <li> เมื่อกดแลกคอมโบแล้ว จะเริ่มนับหนึ่งใหม่เพื่อสะสมคอมโบ</li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>

<?php include("footer.php"); ?>
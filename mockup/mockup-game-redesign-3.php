<html>
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="shortcut icon" type="images/x-icon" href="images/favicon.ico.png">
    <title>บ้านผลบอล | เว็บทายผลบอลอันดับ 1 ของประเทศ ข่าวฟุตบอล ไฮไลท์การแข่งขัน</title>
    <meta name="keywords" content="News Football,Highlight Football,Video Football,Football">
    <meta name="description" content="บ้านผลบอล เว็บทายผลบอลอันดับ 1 ของประเทศ ข่าวฟุตบอล ไฮไลท์การแข่งขัน">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
</head>

<!--Css-->
<link rel="stylesheet" href="/css/style-new.css">
<link rel="stylesheet" href="/css/view-all.css">
<link rel="stylesheet" href="/css/reset.css">
<link rel="stylesheet" href="/css/review-new-style.css">
<link rel="stylesheet" href="/css/game-redesign.css">
<link rel="stylesheet" href="/css/game-redesign-V2.css">
<link rel="stylesheet" href="/css/new-menu.css">
<link rel="stylesheet" href="/css/football-symbol.css">
<link rel="stylesheet" href="/css/program_ball.css">
<link rel="stylesheet" href="/css/template-game.css">
<!--    <link rel="stylesheet" href="/css/scrollbar.css">-->
<link rel="stylesheet" href="/css/swiper.min.css">
<link rel="stylesheet" href="/css/scrollbar.css">

<link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
<!--    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">-->

<!--js-->

<script src="/js/swiper.min.js"></script>
<script src="/js/jquery/dist/jquery.min.js"></script>
<script src="/css/bootstrap/js/bootstrap.min.js"></script>
<script src="nanoscrollbar/nanoscrollbar.js"></script>

<style>


    #main .container-fluid {
        margin: 0;
        padding: 0;
    }

    #main .jumbotron-fluid {
        width: 100%;
        height: 100vh;
        overflow: hidden;
    }

    #main .nav {
        background: #fff;
    }

    #main .nav-item.active .nav-link {
        color: #cb356b;
    }

    #main .display-3 {
        color: #fff;
        display: block;
        margin-top: 30px;
    }

    .one {
        background: #06beb6; /* fallback for old browsers */
        background: -webkit-linear-gradient(to right, #48b1bf, #06beb6); /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to right, #48b1bf, #06beb6); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

    }

    .two {
        background: #642B73; /* fallback for old browsers */
        background: -webkit-linear-gradient(to right, #C6426E, #642B73); /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to right, #C6426E, #642B73); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

    }

    .three {
        background: #36D1DC; /* fallback for old browsers */
        background: -webkit-linear-gradient(to right, #5B86E5, #36D1DC); /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to right, #5B86E5, #36D1DC); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

    }

    .four {
        background: #CB356B; /* fallback for old browsers */
        background: -webkit-linear-gradient(to right, #BD3F32, #CB356B); /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to right, #BD3F32, #CB356B); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

    }

    .bs-example {
        position: relative;
        width: 500px;
        display: block;
        float: left;
        border: solid 1px;
        height: 300px;
        overflow-y: scroll;
    }
</style>


<body style="background-color: #fff; height: 100%;">
<?php //include '../mockup/new-menu-slide.php' ?>
<div class="wrapper-game-v2">
    <input type="checkbox" id="slide" name="" value="" />
    <div class="container">
        <label for="slide" class="toggle">☰</label>
        <nav class="sidebar-slide">
            <ul>
                <li><a href="#"> Nav1 </a></li>
                <li><a href="#"> Nav2 </a></li>
                <li><a href="#"> Nav3 </a></li>
                <li><a href="#"> Nav4 </a></li>
                <li><a href="#"> Nav5 </a></li>
            </ul>
        </nav>
    </div>
</div>


<script>
    (function($) {
        "use strict";

        var $navbar = $("#navbar"),
            y_pos = $navbar.offset().top,
            height = $navbar.height();

        $(document).scroll(function() {
            var scrollTop = $(this).scrollTop();

            if (scrollTop > y_pos + height) {
                $navbar.addClass("navbar-fixed").animate({
                    top: 0
                });
            } else if (scrollTop <= y_pos) {
                $navbar.removeClass("navbar-fixed").clearQueue().animate({
                    top: "-48px"
                }, 0);
            }
        });

    })(jQuery, undefined);
</script>

<script>
    $('.box-vote-home').on('click', function () {
        $(this).addClass('active').siblings().removeClass('active');
    });
</script>
<script>
    $('.box-vote-away').on('click', function () {
        $(this).addClass('active').siblings().removeClass('active');
    });
</script>

<!--Smooth Scroll-->
<script>
    $("#nav ul li a[href^='#']").on('click', function (e) {

        // prevent default anchor click behavior
        e.preventDefault();

        // store hash
        var hash = this.hash;

        // animate
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 1000, function () {

            // when done, add hash to url
            // (default click behaviour)
            window.location.hash = hash;
        });

    });
</script>

<!--nanoScroller-->
<script>
    $(".nano").nanoScroller({scroll: 'top'});
</script>

<!--Swiper-->
<script>
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 4,
        spaceBetween: 10,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
</script>

<!--isOpen-->
<script>
    $(document).ready(function () {
        $('.button').on('click', function () {
            $('.content').toggleClass('isOpen');
        });
    });
</script>

<!--time-->
<script>
    function getTimeRemaining(endtime) {
        var t = Date.parse(endtime) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        var days = Math.floor(t / (1000 * 60 * 60 * 24));
        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        };
    }

    function initializeClock(id, endtime) {
        var clock = document.getElementById(id);
        var daysSpan = clock.querySelector('.days');
        var hoursSpan = clock.querySelector('.hours');
        var minutesSpan = clock.querySelector('.minutes');
        var secondsSpan = clock.querySelector('.seconds');

        function updateClock() {
            var t = getTimeRemaining(endtime);

            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

            if (t.total <= 0) {
                clearInterval(timeinterval);
            }
        }

        updateClock();
        var timeinterval = setInterval(updateClock, 1000);
    }

    var deadline = new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000);
    initializeClock('clockdiv', deadline);
</script>
</body>
</html>
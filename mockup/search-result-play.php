<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!--    CSS-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/search-style.css">
    <link rel="stylesheet" href="/css/swiper.min.css">
    <link rel="stylesheet" href="/css/highlight-new.css">

    <script src="/js/swiper.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.nanoscroller/0.8.7/css/nanoscroller.css">

    <!-- <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nanoscroller/0.8.7/javascripts/jquery.nanoscroller.js"></script>
    <![endif]-->
</head>
<body style="background-color: #fafafa;">
<a href="#" id="scroll" style="display: none;"><span></span></a>
<!--index menu-->
<?php include $_SERVER['DOCUMENT_ROOT'] . '/view/index/menu-all.php'; ?>

<div class="container-fluid">
    <div class="content-lg-video inline-box">
        <iframe width="100%" height="500" src="https://www.youtube.com/embed/U8ma5CmAbDk" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen>
        </iframe>
        <div class="box-title-video full-width inline-box text-left">
            <h2>ชนาธิป สกิล เอเชียนคัพ2019 (chanathip songkrasin Asian cup 2019)</h2>
            <span class="full-width">86,254 views</span>
        </div>



            <?php include("recent-onsearch.php"); ?>




    </div>
    <div class="content-list-video inline-box" data-spy="affix" data-offset-top="20">
        <div class="text-title">
            <h4>Video ที่เกี่ยวข้อง</h4>
        </div>
        <div id="about" class="nano">
            <div class="nano-content">
                <div class="box-list-row inline-box full-width">
                    <div class="thumbnail-list-video inline-box">
                        <img src="/images/1.jpg">
                    </div>
                    <div class="content-list-video-side inline-box">
                        <h3>ชนาธิป สกิล เอเชียนคัพ2019 (chanathip songkrasin Asian cup 2019)</h3>
                        <span>86,254 views</span>
                    </div>
                </div>
                <div class="box-list-row inline-box full-width active">
                    <div class="thumbnail-list-video inline-box">
                        <img src="/images/1.jpg">
                    </div>
                    <div class="content-list-video-side inline-box">
                        <h3>ชนาธิป สกิล เอเชียนคัพ2019 (chanathip songkrasin Asian cup 2019)</h3>
                        <span>86,254 views</span>
                    </div>
                </div>
                <div class="box-list-row inline-box full-width">
                    <div class="thumbnail-list-video inline-box">
                        <img src="/images/1.jpg">
                    </div>
                    <div class="content-list-video-side inline-box">
                        <h3>ชนาธิป สกิล เอเชียนคัพ2019 (chanathip songkrasin Asian cup 2019)</h3>
                        <span>86,254 views</span>
                    </div>
                </div>
                <div class="box-list-row inline-box full-width">
                    <div class="thumbnail-list-video inline-box">
                        <img src="/images/1.jpg">
                    </div>
                    <div class="content-list-video-side inline-box">
                        <h3>ชนาธิป สกิล เอเชียนคัพ2019 (chanathip songkrasin Asian cup 2019)</h3>
                        <span>86,254 views</span>
                    </div>
                </div>
                <div class="box-list-row inline-box full-width">
                    <div class="thumbnail-list-video inline-box">
                        <img src="/images/1.jpg">
                    </div>
                    <div class="content-list-video-side inline-box">
                        <h3>ชนาธิป สกิล เอเชียนคัพ2019 (chanathip songkrasin Asian cup 2019)</h3>
                        <span>86,254 views</span>
                    </div>
                </div>
                <div class="box-list-row inline-box full-width">
                    <div class="thumbnail-list-video inline-box">
                        <img src="/images/1.jpg">
                    </div>
                    <div class="content-list-video-side inline-box">
                        <h3>ชนาธิป สกิล เอเชียนคัพ2019 (chanathip songkrasin Asian cup 2019)</h3>
                        <span>86,254 views</span>
                    </div>
                </div>
                <div class="box-list-row inline-box full-width">
                    <div class="thumbnail-list-video inline-box">
                        <img src="/images/1.jpg">
                    </div>
                    <div class="content-list-video-side inline-box">
                        <h3>ชนาธิป สกิล เอเชียนคัพ2019 (chanathip songkrasin Asian cup 2019)</h3>
                        <span>86,254 views</span>
                    </div>
                </div>
                <div class="box-list-row inline-box full-width">
                    <div class="thumbnail-list-video inline-box">
                        <img src="/images/1.jpg">
                    </div>
                    <div class="content-list-video-side inline-box">
                        <h3>ชนาธิป สกิล เอเชียนคัพ2019 (chanathip songkrasin Asian cup 2019)</h3>
                        <span>86,254 views</span>
                    </div>
                </div>
                <div class="box-list-row inline-box full-width">
                    <div class="thumbnail-list-video inline-box">
                        <img src="/images/1.jpg">
                    </div>
                    <div class="content-list-video-side inline-box">
                        <h3>ชนาธิป สกิล เอเชียนคัพ2019 (chanathip songkrasin Asian cup 2019)</h3>
                        <span>86,254 views</span>
                    </div>
                </div>
                <div class="box-list-row inline-box full-width">
                    <div class="thumbnail-list-video inline-box">
                        <img src="/images/1.jpg">
                    </div>
                    <div class="content-list-video-side inline-box">
                        <h3>ชนาธิป สกิล เอเชียนคัพ2019 (chanathip songkrasin Asian cup 2019)</h3>
                        <span>86,254 views</span>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>


<!-- Initialize Swiper -->
<script>
    var swiper2 = new Swiper('.swiper2', {
        slidesPerView: 4,
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next-recent',
            prevEl: '.swiper-button-prev-recent',
        },
    });
</script>
<script>
    var swiper3 = new Swiper('.swiper3', {
        slidesPerView: 4,
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next-highlight',
            prevEl: '.swiper-button-prev-highlight',
        },
    });
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

<script>
    $(".nano").nanoScroller({ scroll: 'top' });
</script>
</body>
</html>
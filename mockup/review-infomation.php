<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ngoal | ทีเด็ด ทรรศนะ</title>
    <link rel="shortcut icon" type="images/x-icon" href="/images/favicon.ico.png">
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/doce.css">
    <link rel="stylesheet" href="/css/livescore.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <link rel="stylesheet" href="/css/game-redesign.css">
    <link rel="stylesheet" href="/css/review-new-style.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/readmore.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="js/jquery-timeago/jquery.timeago.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="/js/moment.js"></script>

    <?php include 'view/index/googletagmanager.php'; ?>
</head>
<body style="background-color: #f6f6f6;">
<!--index menu-->
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="wrap-review">
    <?php include '../mockup/sidebar-feed.php' ?>
    <div class="container" id="main">

        <div class="col-md-3 col-sm-3 sidebar-left-ranking">
            <div class="sidebar-ranking-review">
                <div class="box-match-sidebar">
                    <div class="text-title">
                        <h4 style="font-size: 16px;">คู่บอล</h4>
                        <div class="dropdown pull-right">
                            <button class="btn btn-default btn-xs dropdown-toggle" type="button" id="dropdownMenu1"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Today
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="#">Tomorrow</a></li>
                                <li><a href="#">Today</a></li>
                                <li><a href="#">Yesterday</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="content-match">
                        <div class="content-match-home">
                            <img src="/images/logo-team/Chelsea.png">
                            <span>Chelsea</span>
                        </div>
                        <div class="content-match-hdp">
                            <strong>-2.50</strong>
                            <div class="box-water-text">
                                <div>1.9</div>
                                <div>|</div>
                                <div>1.69</div>
                            </div>
                            <span class="time-match">18:30</span>
                        </div>
                        <div class="content-match-away">
                            <img src="/images/logo-team/manchester.png">
                            <span>Manchester</span>
                        </div>
                    </div>
                    <div class="content-match active">
                        <div class="content-match-home">
                            <img src="/images/logo-team/Chelsea.png">
                            <span>Chelsea</span>
                        </div>
                        <div class="content-match-hdp">
                            <strong>-2.50</strong>
                            <div class="box-water-text">
                                <div>1.9</div>
                                <div>|</div>
                                <div>1.69</div>
                            </div>
                            <span class="time-match">18:30</span>
                        </div>
                        <div class="content-match-away">
                            <img src="/images/logo-team/manchester.png">
                            <span>Manchester</span>
                        </div>
                    </div>
                    <div class="content-match">
                        <div class="content-match-home">
                            <img src="/images/logo-team/Chelsea.png">
                            <span>Chelsea</span>
                        </div>
                        <div class="content-match-hdp">
                            <strong>-2.50</strong>
                            <div class="box-water-text">
                                <div>1.9</div>
                                <div>|</div>
                                <div>1.69</div>
                            </div>
                            <span class="time-match">18:30</span>
                        </div>
                        <div class="content-match-away">
                            <img src="/images/logo-team/manchester.png">
                            <span>Manchester</span>
                        </div>
                    </div>
                    <div class="content-match">
                        <div class="content-match-home">
                            <img src="/images/logo-team/Chelsea.png">
                            <span>Chelsea</span>
                        </div>
                        <div class="content-match-hdp">
                            <strong>-2.50</strong>
                            <div class="box-water-text">
                                <div>1.9</div>
                                <div>|</div>
                                <div>1.69</div>
                            </div>
                            <span class="time-match">18:30</span>
                        </div>
                        <div class="content-match-away">
                            <img src="/images/logo-team/manchester.png">
                            <span>Manchester</span>
                        </div>
                    </div>
                    <div class="content-match">
                        <div class="content-match-home">
                            <img src="/images/logo-team/Chelsea.png">
                            <span>Chelsea</span>
                        </div>
                        <div class="content-match-hdp">
                            <strong>-2.50</strong>
                            <div class="box-water-text">
                                <div>1.9</div>
                                <div>|</div>
                                <div>1.69</div>
                            </div>
                            <span class="time-match">18:30</span>
                        </div>
                        <div class="content-match-away">
                            <img src="/images/logo-team/manchester.png">
                            <span>Manchester</span>
                        </div>
                    </div>
                    <div class="content-match">
                        <div class="content-match-home">
                            <img src="/images/logo-team/Chelsea.png">
                            <span>Chelsea</span>
                        </div>
                        <div class="content-match-hdp">
                            <strong>-2.50</strong>
                            <div class="box-water-text">
                                <div>1.9</div>
                                <div>|</div>
                                <div>1.69</div>
                            </div>
                            <span class="time-match">18:30</span>
                        </div>
                        <div class="content-match-away">
                            <img src="/images/logo-team/manchester.png">
                            <span>Manchester</span>
                        </div>
                    </div>
                </div>
                <?php include '../mockup/review-sidebar-left.php' ?>
            </div>
        </div>

        <div class="col-md-8 col-sm-8 content-review-information">
            <div class="container-review">
                <div class="text-title">
                    <h4>ทีเด็ด ทรรศนะ</h4>
                    <div class="pull-right" style="margin-top: 5px;"><i class="fa fa-calendar" aria-hidden="true"></i>
                        18 Sep 2018
                    </div>
                </div>
                <div class="content-review">
                    <div class="top-section">
                        <a class="link_wrap" href="/game.php"></a>
                        <div class="home-section">
                            <div class="vote-home-section">
                                <a class="link_wrap" href="/"></a>
                                VOTE
                            </div>
                            <div class="content-home-section text-right">
                                <div class="topic-event">UEFA Champions League</div>
                                <div class="detail-team-home">
                                    <div class="detail-team-home-state">
                                        <h4>Chelsea FC</h4>
                                        <div class="progress">
                                            <div class="progress-bar pull-right progress-bar-striped active"
                                                 role="progressbar"
                                                 aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"
                                                 style="width: 45%">
                                                <span>Vote 45%</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="detail-team-home-logo">
                                        <img src="/images/logo-team/Chelsea.png">
                                    </div>

                                </div>
                                <ul class="list-state-match pull-right">
                                    <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                    <li>
                                        <div class="draw-st"></div>
                                    </li>
                                    <li>
                                        <div class="loss-st"></div>
                                    </li>
                                    <li>
                                        <div class="win-st"></div>
                                    </li>
                                    <li>
                                        <div class="win-st"></div>
                                    </li>
                                    <li>
                                        <div class="draw-st"></div>
                                    </li>
                                    <li class="text-match">Last 5 matches</li>
                                </ul>
                            </div>
                        </div>
                        <div class="hdp-section text-center">
                            <a class="link_wrap" href="#"></a>
                            <strong>HDP</strong>
                            <h2>-2.50</h2>
                            <div class="box-water-text">
                                <div>1.9</div>
                                <div>|</div>
                                <div>1.69</div>
                            </div>
                            <span class="time-match">18:30</span>

                        </div>
                        <div class="away-section">

                            <div class="content-home-section content-away-section pull-left">
                                <div class="topic-live-on">
                                    <img src="/images/live-on.png">
                                    <div>|</div>
                                    <img src="/images/channel/pptv.png">
                                </div>
                                <div class="detail-team-home detail-team-away">
                                    <div class="detail-team-home-logo">
                                        <img src="/images/logo-team/manchester.png">
                                    </div>
                                    <div class="detail-team-home-state detail-team-away-state">
                                        <h4>Manchester United</h4>
                                        <div class="progress">
                                            <div class="progress-bar pull-left progress-bar-danger active"
                                                 role="progressbar"
                                                 aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"
                                                 style="width: 70%">
                                                <span>Vote 70%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="list-state-match pull-left">
                                    <li class="text-match">Last 5 matches</li>
                                    <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                    <li>
                                        <div class="draw-st"></div>
                                    </li>
                                    <li>
                                        <div class="loss-st"></div>
                                    </li>
                                    <li>
                                        <div class="win-st"></div>
                                    </li>
                                    <li>
                                        <div class="win-st"></div>
                                    </li>
                                    <li>
                                        <div class="draw-st"></div>
                                    </li>
                                </ul>
                            </div>
                            <div class="vote-away-section">
                                <a class="link_wrap" href="/"></a>
                                VOTE
                            </div>
                        </div>
                    </div>


                    <div class="review-section">

                        <div class="home-review-section">
                            <div class="box-content-review">
                                <div class="top-content-review pull-left">

                                    <div>
                                        <img src="/images/sample-logo01.png">
                                    </div>
                                    <div>
                                        <h5>LOMTOE.NET</h5>
                                        <img src="/images/logo-team/Chelsea.png">
                                        <span>@Chelsea FC</span>
                                    </div>
                                    <div class="winRate-box">
                                        <div class="bg-winRate-box">
                                            <h3>20%</h3>
                                            <p>Win Rate</p>
                                        </div>
                                        <div class="bg-day-box">
                                            <h3>30</h3>
                                            <p>Days</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="full-review-caption">
                                    <p>ไลป์ซิกในถิ่นแข็งแกร่งแม้ซัลซ์บวร์กจะเป็นทีมที่ดีแต่ยังเชื่อเจ้าถิ่นน่าซิวชัย
                                        2-1
                                    </p>
                                </div>
                            </div>
                            <div class="box-content-review">
                                <div class="top-content-review pull-left">
                                    <div>
                                        <img src="/images/sample-logo01.png">
                                    </div>
                                    <div>
                                        <h5>LOMTOE.NET</h5>
                                        <img src="/images/logo-team/Chelsea.png">
                                        <span>@Chelsea FC</span>
                                    </div>
                                    <div class="winRate-box">
                                        <div class="bg-winRate-box">
                                            <h3>20%</h3>
                                            <p>Win Rate</p>
                                        </div>
                                        <div class="bg-day-box">
                                            <h3>30</h3>
                                            <p>Days</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="full-review-caption">
                                    <p> ระดับความมั่นใจ – แอร์เบ ไลป์ซิก ★★★แอร์เบ ไลป์ซิกไม่แพ้ใครจากเกมสี่นัดติด
                                        แถมยังคว้าชัย3ด้วย นอกจากนี้แล้ว ในการต่อต้านกับเร้ดบูลล์
                                        ซัลซ์บวร์ก10นัดมาหลังสุด ต่างก็ไร้พ่ายแพ้หมด สภาพรวมคงที่มาก เร้ดบูลล์
                                        ซัลซ์บวร์กเป็นรองอยู่แล้ว เกมนี้ออกการเยือน คงก็หนีพ้นยากหรอก
                                        เรทเปิดมาเป็นทีมเหย้าต่อ0.5 ประกอบด้วยราคาน้ำต่ำ แอร์เบ ไลป์ซิกจะเป็นผู้ชนะ
                                        @แอร์เบ ไลป์ซิก ต่อ0.5
                                    </p>
                                </div>
                            </div>
                            <div class="box-content-review">
                                <div class="top-content-review pull-left">
                                    <div>
                                        <img src="/images/sample-logo01.png">
                                    </div>
                                    <div>
                                        <h5>LOMTOE.NET</h5>
                                        <img src="/images/logo-team/Chelsea.png">
                                        <span>@Chelsea FC</span>
                                    </div>
                                    <div class="winRate-box">
                                        <div class="bg-winRate-box">
                                            <h3>20%</h3>
                                            <p>Win Rate</p>
                                        </div>
                                        <div class="bg-day-box">
                                            <h3>30</h3>
                                            <p>Days</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="full-review-caption">
                                    <p> แอร์เบ ไลป์ซิก ติ้กไว้เลย 3-2
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="away-review-section">
                            <div class="box-content-review">
                                <div class="top-content-review pull-left">
                                    <div>
                                        <img src="/images/profess-player/sample-logo02.png">
                                    </div>
                                    <div>
                                        <h5>Hotleague</h5>
                                        <img src="/images/logo-team/manchester.png">
                                        <span>@Manchester United</span>
                                    </div>
                                    <div class="winRate-box">
                                        <div class="bg-winRate-box">
                                            <h3>18%</h3>
                                            <p>Win Rate</p>
                                        </div>
                                        <div class="bg-day-box">
                                            <h3>03</h3>
                                            <p>Days</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="full-review-caption">
                                    <p>
                                        แอร์เบ ไลป์ซิกยังไม่เคยเจอกับเร้ดบูลล์ ซัลซ์บวร์กมาก่อน
                                        เจ้าบ้านได้เล่นแค่รายการยูโรป้าลีก
                                        แต่ฝั่งทีมเยือนเคยเล่นในถ้วยใบใหญ่อย่างยูฟ่า แชมเปียนส์ลีกมาก่อน
                                        แต่ที่ต้องตกมาเล่นถ้วยใบเล็ก เพราะเสมอในบ้านที่สกอร์ 2-2
                                        ตกรอบไปเพราะยิงประตูนอกบ้านไม่ได้เลย และเล่นในบ้านก็พลาดท่าเสียไปถึงสองลูก
                                        มาเกมนี้เจ้าบ้านได้เปรียบเสียงเชียร์ และอยู่ลีกที่ดีกว่า
                                        แต่ผลงานกับเรทบังคับชนะ มองว่าทีมเยือนยังแข็งแกร่พอตัวแพ้ยาก
                                        ประสบการณ์เกมถ้วยใบใหญ่มีมากกว่า ที่ตกรอบมาก็เพราะเสมอ
                                        มองแล้วเจ้าบ้านน่าจะทำได้แค่เสมอ เพราะขุมกำลังโดยรวมยังไม่ดีพอ
                                        ที่จะเบียดเก็บสามแต้มได้

                                        อัตราต่อรอง : แอร์เบ ไลป์ซิก ต่อ0.5

                                        ทีเด็ด ฟันธง : เร้ดบูลล์ ซัลซ์บวร์ก

                                        ผลบอลสกอร์ที่คาด : 1-1 หรือ 2-2

                                        ระดับความมั่นใจ : 70%
                                    </p>
                                </div>
                            </div>
                            <div class="box-content-review">
                                <div class="top-content-review pull-left">
                                    <div>
                                        <img src="/images/profess-player/sample-logo02.png">
                                    </div>
                                    <div>
                                        <h5>Hotleague</h5>
                                        <img src="/images/logo-team/manchester.png">
                                        <span>@Manchester United</span>
                                    </div>
                                    <div class="winRate-box">
                                        <div class="bg-winRate-box">
                                            <h3>18%</h3>
                                            <p>Win Rate</p>
                                        </div>
                                        <div class="bg-day-box">
                                            <h3>03</h3>
                                            <p>Days</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="full-review-caption">
                                    <p>
                                        บอลคู่นี้ดูยังไงก็แนะนำให่ลุ้น เจ้าถิ่น RB ไลป์ซิก ไปดีกว่าเพราะว่า
                                        ผลงานการเล่นในบ้านกับรายการนี้ พวกเขาเล่นดีจริง เล่นไปสามเกมชนะได้หมดเลย
                                        แถมจากชื่อชั้นมาตรฐาน RB ไลป์ซิก นั้นดูดีกว่าเยอะ ลงเล่นในลีกของเยอรมัน
                                        ซึ่งมาตรฐานลีกนั้นสูงกว่าแน่นอน เจอกับทีมใหญ่ๆ มาก็ค่อนข้างเยอะ
                                        ประสบการณ์เพียบ บวกกับความได้เปรียบในเรื่องสภาพสนามอีก ดังนั้น มั่นใจ ยังไง
                                        RB ไลป์ซิก ต้องชนะเก็บสามคะแนนในกลุ่มได้ก่อนแน่นอน
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>


        <div class="open-btn" onclick="openNav()">
            <ul>
                <li><i class="fa fa-rss" aria-hidden="true"></i></li>
                <li>Feed</li>
                <li><i class="fa fa-chevron-up pull-right" aria-hidden="true"></i></li>
            </ul>


        </div>
    </div>
</div>
<script>
    function openNav() {
        document.getElementById("mySidenav").style.marginRight = "0";
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main").style.marginRight = "250px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.margin = "0 auto";
    }
</script>
<script>
    function myFunction() {
        var dots = document.getElementById("dots");
        var moreText = document.getElementById("more");
        var btnText = document.getElementById("myBtn");

        if (dots.style.display === "none") {
            dots.style.display = "inline";
            btnText.innerHTML = "Read more";
            moreText.style.display = "none";
        } else {
            dots.style.display = "none";
            btnText.innerHTML = "Read less";
            moreText.style.display = "inline";
        }
    }
</script>
<script>
    $('section').readmore({maxHeight: 500});
</script>

</body>
</html>
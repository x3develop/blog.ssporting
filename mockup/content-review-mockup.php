<div class="col-md-8 col-sm-8">
    <div class="container-review">
        <div class="text-title">
            <h4>ทีเด็ด ทรรศนะ</h4>
            <div class="date-review pull-right">
                <button class="btn btn-sm btn-default" type="submit">Yesterday</button>
                <button class="btn btn-sm btn-default active" type="submit">Today</button>
                <button class="btn btn-sm btn-default" type="submit">Tomorrow</button>
            </div>
        </div>
        <div class="content-review">
            <div class="top-section">
                <a class="link_wrap" href="/game.php"></a>
                <div class="home-section">
                    <div class="vote-home-section">
                        <a class="link_wrap" href="/"></a>
                        VOTE
                    </div>
                    <div class="content-home-section text-right">
                        <div class="topic-event">UEFA Champions League</div>
                        <div class="detail-team-home">
                            <div class="detail-team-home-state">
                                <h4>Chelsea FC</h4>
                                <div class="progress">
                                    <div class="progress-bar pull-right progress-bar-striped active" role="progressbar"
                                         aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                        <span>Vote 45%</span>
                                    </div>
                                </div>
                            </div>
                            <div class="detail-team-home-logo">
                                <img src="/images/logo-team/Chelsea.png">
                            </div>

                        </div>
                        <ul class="list-state-match pull-right">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li>
                                <div class="loss-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li class="text-match">Last 5 matches</li>
                        </ul>
                    </div>
                </div>
                <div class="hdp-section text-center">
                    <a class="link_wrap" href="#"></a>
                    <strong>HDP</strong>
                    <h2>-2.50</h2>
                    <div class="box-water-text">
                        <div>1.9</div>
                        <div>|</div>
                        <div>1.69</div>
                    </div>
                    <span class="time-match">18:30</span>

                </div>
                <div class="away-section">

                    <div class="content-home-section content-away-section pull-left">
                        <div class="topic-live-on">
                            <img src="/images/live-on.png">
                            <div>|</div>
                            <img src="/images/channel/pptv.png">
                        </div>
                        <div class="detail-team-home detail-team-away">
                            <div class="detail-team-home-logo">
                                <img src="/images/logo-team/manchester.png">
                            </div>
                            <div class="detail-team-home-state detail-team-away-state">
                                <h4>Manchester United</h4>
                                <div class="progress">
                                    <div class="progress-bar pull-left progress-bar-danger active" role="progressbar"
                                         aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span>Vote 70%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="list-state-match pull-left">
                            <li class="text-match">Last 5 matches</li>
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li>
                                <div class="loss-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="vote-away-section">
                        <a class="link_wrap" href="/"></a>
                        VOTE
                    </div>
                </div>
            </div>

            <section class="slide">
                <div class="review-section">
                    <a class="link_wrap" href="#"></a>
                    <div class="home-review-section">
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">

                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="away-review-section">
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/profess-player/sample-logo02.png">
                                </div>
                                <div>
                                    <h5>Hotleague</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>18%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>03</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <div class="content-review">
            <div class="top-section">
                <a class="link_wrap" href="/game.php"></a>
                <div class="home-section">
                    <div class="vote-home-section">
                        <a class="link_wrap" href="/"></a>
                        VOTE
                    </div>
                    <div class="content-home-section text-right">
                        <div class="topic-event">UEFA Champions League</div>
                        <div class="detail-team-home">
                            <div class="detail-team-home-state">
                                <h4>Chelsea FC</h4>
                                <div class="progress">
                                    <div class="progress-bar pull-right progress-bar-striped active" role="progressbar"
                                         aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                        <span>Vote 45%</span>
                                    </div>
                                </div>
                            </div>
                            <div class="detail-team-home-logo">
                                <img src="/images/logo-team/Chelsea.png">
                            </div>

                        </div>
                        <ul class="list-state-match pull-right">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li>
                                <div class="loss-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li class="text-match">Last 5 matches</li>
                        </ul>
                    </div>
                </div>
                <div class="hdp-section text-center">
                    <a class="link_wrap" href="#"></a>
                    <strong>HDP</strong>
                    <h2>-2.50</h2>
                    <div class="box-water-text">
                        <div>1.9</div>
                        <div>|</div>
                        <div>1.69</div>
                    </div>
                    <span class="time-match">18:30</span>

                </div>
                <div class="away-section">

                    <div class="content-home-section content-away-section pull-left">
                        <div class="topic-live-on">
                            <img src="/images/live-on.png">
                            <div>|</div>
                            <img src="/images/channel/pptv.png">
                        </div>
                        <div class="detail-team-home detail-team-away">
                            <div class="detail-team-home-logo">
                                <img src="/images/logo-team/manchester.png">
                            </div>
                            <div class="detail-team-home-state detail-team-away-state">
                                <h4>Manchester United</h4>
                                <div class="progress">
                                    <div class="progress-bar pull-left progress-bar-danger active" role="progressbar"
                                         aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span>Vote 70%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="list-state-match pull-left">
                            <li class="text-match">Last 5 matches</li>
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li>
                                <div class="loss-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="vote-away-section">
                        <a class="link_wrap" href="/"></a>
                        VOTE
                    </div>
                </div>
            </div>

            <section class="slide">
                <div class="review-section">
                    <a class="link_wrap" href="#"></a>
                    <div class="home-review-section">
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">

                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="away-review-section">
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/profess-player/sample-logo02.png">
                                </div>
                                <div>
                                    <h5>Hotleague</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>18%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>03</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <div class="content-review">
            <div class="top-section">
                <a class="link_wrap" href="/game.php"></a>
                <div class="home-section">
                    <div class="vote-home-section">
                        <a class="link_wrap" href="/"></a>
                        VOTE
                    </div>
                    <div class="content-home-section text-right">
                        <div class="topic-event">UEFA Champions League</div>
                        <div class="detail-team-home">
                            <div class="detail-team-home-state">
                                <h4>Chelsea FC</h4>
                                <div class="progress">
                                    <div class="progress-bar pull-right progress-bar-striped active" role="progressbar"
                                         aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                        <span>Vote 45%</span>
                                    </div>
                                </div>
                            </div>
                            <div class="detail-team-home-logo">
                                <img src="/images/logo-team/Chelsea.png">
                            </div>

                        </div>
                        <ul class="list-state-match pull-right">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li>
                                <div class="loss-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li class="text-match">Last 5 matches</li>
                        </ul>
                    </div>
                </div>
                <div class="hdp-section text-center">
                    <a class="link_wrap" href="#"></a>
                    <strong>HDP</strong>
                    <h2>-2.50</h2>
                    <div class="box-water-text">
                        <div>1.9</div>
                        <div>|</div>
                        <div>1.69</div>
                    </div>
                    <span class="time-match">18:30</span>

                </div>
                <div class="away-section">

                    <div class="content-home-section content-away-section pull-left">
                        <div class="topic-live-on">
                            <img src="/images/live-on.png">
                            <div>|</div>
                            <img src="/images/channel/pptv.png">
                        </div>
                        <div class="detail-team-home detail-team-away">
                            <div class="detail-team-home-logo">
                                <img src="/images/logo-team/manchester.png">
                            </div>
                            <div class="detail-team-home-state detail-team-away-state">
                                <h4>Manchester United</h4>
                                <div class="progress">
                                    <div class="progress-bar pull-left progress-bar-danger active" role="progressbar"
                                         aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span>Vote 70%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="list-state-match pull-left">
                            <li class="text-match">Last 5 matches</li>
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li>
                                <div class="loss-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="vote-away-section">
                        <a class="link_wrap" href="/"></a>
                        VOTE
                    </div>
                </div>
            </div>

            <section class="slide">
                <div class="review-section">
                    <a class="link_wrap" href="#"></a>
                    <div class="home-review-section">
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">

                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="away-review-section">
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/profess-player/sample-logo02.png">
                                </div>
                                <div>
                                    <h5>Hotleague</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>18%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>03</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <div class="content-review content-review-endmatch">
            <div class="top-section">
                <div class="home-section">
                    <div class="vote-home-section">
                        0
                    </div>
                    <div class="content-home-section text-right">
                        <div class="topic-event">UEFA Champions League</div>
                        <div class="detail-team-home">
                            <div class="detail-team-home-state">
                                <h4>Chelsea FC</h4>
                                <div class="progress">
                                    <div class="progress-bar pull-right progress-bar-striped active" role="progressbar"
                                         aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                        <span>Vote 45%</span>
                                    </div>
                                </div>
                            </div>
                            <div class="detail-team-home-logo">
                                <img src="/images/logo-team/Chelsea.png">
                            </div>

                        </div>
                        <ul class="list-state-match pull-right">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li>
                                <div class="loss-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li class="text-match">Last 5 matches</li>
                        </ul>
                    </div>
                </div>
                <div class="hdp-section text-center">
                    <a class="link_wrap" href="#"></a>
                    <strong>HDP</strong>
                    <h2>-2.50</h2>
                    <div class="box-water-text">
                        <div>1.9</div>
                        <div>|</div>
                        <div>1.69</div>
                    </div>
                    <span>Full Match</span>

                </div>
                <div class="away-section">

                    <div class="content-home-section content-away-section pull-left">
                        <div class="topic-live-on">
                            <img src="/images/live-on.png">
                            <div>|</div>
                            <img src="/images/channel/pptv.png">
                        </div>
                        <div class="detail-team-home detail-team-away">
                            <div class="detail-team-home-logo">
                                <img src="/images/logo-team/manchester.png">
                            </div>
                            <div class="detail-team-home-state detail-team-away-state">
                                <h4>Manchester United</h4>
                                <div class="progress">
                                    <div class="progress-bar pull-left progress-bar-danger active" role="progressbar"
                                         aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span>Vote 70%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="list-state-match pull-left">
                            <li class="text-match">Last 5 matches</li>
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li>
                                <div class="loss-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="vote-away-section">
                        10
                    </div>
                </div>

            </div>

            <section class="slide">
                <div class="review-section">
                    <a class="link_wrap" href="#"></a>
                    <div class="home-review-section">
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">

                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="away-review-section">
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/profess-player/sample-logo02.png">
                                </div>
                                <div>
                                    <h5>Hotleague</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>18%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>03</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

        </div>
        <div class="content-review content-review-endmatch">
            <div class="top-section">
                <div class="home-section">
                    <div class="vote-home-section">
                        0
                    </div>
                    <div class="content-home-section text-right">
                        <div class="topic-event">UEFA Champions League</div>
                        <div class="detail-team-home">
                            <div class="detail-team-home-state">
                                <h4>Chelsea FC</h4>
                                <div class="progress">
                                    <div class="progress-bar pull-right progress-bar-striped active" role="progressbar"
                                         aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                        <span>Vote 45%</span>
                                    </div>
                                </div>
                            </div>
                            <div class="detail-team-home-logo">
                                <img src="/images/logo-team/Chelsea.png">
                            </div>

                        </div>
                        <ul class="list-state-match pull-right">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li>
                                <div class="loss-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li class="text-match">Last 5 matches</li>
                        </ul>
                    </div>
                </div>
                <div class="hdp-section text-center">
                    <a class="link_wrap" href="#"></a>
                    <strong>HDP</strong>
                    <h2>-2.50</h2>
                    <div class="box-water-text">
                        <div>1.9</div>
                        <div>|</div>
                        <div>1.69</div>
                    </div>
                    <span>Full Match</span>

                </div>
                <div class="away-section">

                    <div class="content-home-section content-away-section pull-left">
                        <div class="topic-live-on">
                            <img src="/images/live-on.png">
                            <div>|</div>
                            <img src="/images/channel/pptv.png">
                        </div>
                        <div class="detail-team-home detail-team-away">
                            <div class="detail-team-home-logo">
                                <img src="/images/logo-team/manchester.png">
                            </div>
                            <div class="detail-team-home-state detail-team-away-state">
                                <h4>Manchester United</h4>
                                <div class="progress">
                                    <div class="progress-bar pull-left progress-bar-danger active" role="progressbar"
                                         aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span>Vote 70%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="list-state-match pull-left">
                            <li class="text-match">Last 5 matches</li>
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li>
                                <div class="loss-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="vote-away-section">
                        10
                    </div>
                </div>

            </div>

            <section class="slide">
                <div class="review-section">
                    <a class="link_wrap" href="#"></a>
                    <div class="home-review-section">
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">

                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="away-review-section">
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/profess-player/sample-logo02.png">
                                </div>
                                <div>
                                    <h5>Hotleague</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>18%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>03</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

        </div>
        <div class="content-review content-review-endmatch">
            <div class="top-section">
                <div class="home-section">
                    <div class="vote-home-section">
                        0
                    </div>
                    <div class="content-home-section text-right">
                        <div class="topic-event">UEFA Champions League</div>
                        <div class="detail-team-home">
                            <div class="detail-team-home-state">
                                <h4>Chelsea FC</h4>
                                <div class="progress">
                                    <div class="progress-bar pull-right progress-bar-striped active" role="progressbar"
                                         aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                        <span>Vote 45%</span>
                                    </div>
                                </div>
                            </div>
                            <div class="detail-team-home-logo">
                                <img src="/images/logo-team/Chelsea.png">
                            </div>

                        </div>
                        <ul class="list-state-match pull-right">
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li>
                                <div class="loss-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li class="text-match">Last 5 matches</li>
                        </ul>
                    </div>
                </div>
                <div class="hdp-section text-center">
                    <a class="link_wrap" href="#"></a>
                    <strong>HDP</strong>
                    <h2>-2.50</h2>
                    <div class="box-water-text">
                        <div>1.9</div>
                        <div>|</div>
                        <div>1.69</div>
                    </div>
                    <span>Full Match</span>

                </div>
                <div class="away-section">

                    <div class="content-home-section content-away-section pull-left">
                        <div class="topic-live-on">
                            <img src="/images/live-on.png">
                            <div>|</div>
                            <img src="/images/channel/pptv.png">
                        </div>
                        <div class="detail-team-home detail-team-away">
                            <div class="detail-team-home-logo">
                                <img src="/images/logo-team/manchester.png">
                            </div>
                            <div class="detail-team-home-state detail-team-away-state">
                                <h4>Manchester United</h4>
                                <div class="progress">
                                    <div class="progress-bar pull-left progress-bar-danger active" role="progressbar"
                                         aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span>Vote 70%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="list-state-match pull-left">
                            <li class="text-match">Last 5 matches</li>
                            <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                            <li>
                                <div class="loss-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="win-st"></div>
                            </li>
                            <li>
                                <div class="draw-st"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="vote-away-section">
                        10
                    </div>
                </div>

            </div>

            <section class="slide">
                <div class="review-section">
                    <a class="link_wrap" href="#"></a>
                    <div class="home-review-section">
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">

                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/Chelsea.png">
                                    <span>@Chelsea FC</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="away-review-section">
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/profess-player/sample-logo02.png">
                                </div>
                                <div>
                                    <h5>Hotleague</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>18%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>03</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                        <div class="box-content-review">
                            <div class="top-content-review pull-left">
                                <div>
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <div>
                                    <h5>LOMTOE.NET</h5>
                                    <img src="/images/logo-team/manchester.png">
                                    <span>@Manchester United</span>
                                </div>
                                <div class="winRate-box">
                                    <div class="bg-winRate-box">
                                        <h3>20%</h3>
                                        <p>Win Rate</p>
                                    </div>
                                    <div class="bg-day-box">
                                        <h3>30</h3>
                                        <p>Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="full-review-caption">
                                <p> "เกมเปิดสนามศึกฟุตบอลโลก 2018 เกมในกลุ่ม เอ เจ้าภาพ รัสเซีย จะต้องดวลกับ
                                    ซาอุดีอาระเบีย ตัวแทนจากเอเชีย ฟอร์มของเจ้าภาพ รัสเซีย
                                    ที่ได้เข้ารอบสุดท้ายแบบอัตโนมัติ
                                    ทำให้เกมก่อนหน้านี้ส่วนใหญ่ได้เล่นแต่เกมอุ่นเครื่อง
                                    ผลงานช่วงหลังไม่ดีสักเท่าไหร่ 5 นัดหลังสุดไม่ชนะใคร เสมอ 2 แพ้ 3 ส่วนทางฝั่ง
                                    ซาอุดีอาระเบีย ก็ผลงานไม่ดี แพ้รวดในเกมอุ่นเครื่องมา 3 เกมติด ตัวผู้เล่นของ
                                    รัสเซีย
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

        </div>

    </div>
</div>
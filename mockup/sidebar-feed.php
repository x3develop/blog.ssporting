<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/UserFeed.php";
$userFeed=new UserFeed();
$listUserFeed=$userFeed->userFeedBetAndReview(10);
?>
<div id="mySidenav" class="sidenav" data-spy="affix" data-offset-top="200">
    <div class="scroll-bar-wrap">
        <div class="sidebar-feed scroll-box">
<!--            <div id="title-feed" class="text-title">-->
            <div class="text-title">
                <h4><i class="fa fa-rss" aria-hidden="true"></i> Feed</h4>
<!--                <button class="btn btn-xs" onclick="closeNav()"><i class="fa fa-eye-slash" aria-hidden="true"></i>-->
<!--                    ซ่อน-->
<!--                    Feed-->
<!--                </button>-->
            </div>
            <div class="container-feed">
                <?php foreach ($listUserFeed as $key=>$value){ ?>
                    <div class="box-feed" user_feed_id="<?php echo $value['id'];?>">
                        <a class="link_wrap" href="/match/<?php echo $value['match_id']; ?>.html"></a>
                        <ul>
                            <li>
                                <div class="image-user">
                                    <img src="https://graph.facebook.com/v2.8/<?php echo $value['fb_uid']; ?>/picture?type=large">
                                </div>
                            </li>
                            <li>
                                <h4><?php echo $value['name']; ?></h4>
                                <span><?php echo date("D, G:i", strtotime('+5 hours',strtotime($value['created_at'])));?></span>
                                <p><?php echo $value['message']; ?></p>
                            </li>
                        </ul>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<script>
    var dayTradeLoad;
    $(document).ready(function() {
        loadFeedUser();
    });

    function loadFeedUser() {
        var first = parseInt($('.container-feed').find('.box-feed').first().attr('user_feed_id'));
        if ($('.container-feed').find('.box-feed').length == 0) {
            first = 0;
        }
        clearInterval(dayTradeLoad);
        $.getJSON("/json/userFeed.json", function (data) {
            var add = 0;
            $.each(data, function (key, val) {
                if (first < key) {
                    setTimeout(function () {
                        $('div.container-feed').prepend(val);
                        $("div.container-feed").find('.box-feed').first().hide();
                        $("div.container-feed").find('.box-feed').first().fadeIn(2000);
                    }, (add * 4000));
                    add++;
                }
            });
            dayTradeLoad = setInterval(function () {
                loadFeedUser();
            }, 10000);
        });
    }
</script>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!--    CSS-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/search-style.css">
    <link rel="stylesheet" href="/css/highlight-new.css">
    <link rel="stylesheet" href="/css/swiper.min.css">

    <script src="/js/swiper.min.js"></script>

    <!-- <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body style="background-color: #fafafa;">
<a href="#" id="scroll" style="display: none;"><span></span></a>
<!--index menu-->
<?php include $_SERVER['DOCUMENT_ROOT'] . '/view/index/menu-all.php'; ?>

<div class="jumbotron">
    <div class="container">

        <div class="content-top-search full-width inline-box text-center">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="button">ค้นหา</button>
                </span>
            </div>
        </div>
    </div>
</div>


<div class="container container-search-result">
    <h1>ผลการค้นหาเพลง "ชนาธิป" จาก Youtube</h1>
    <?php include("search-result.php"); ?>
</div>

<div class="container">

    <?php include("recent-onsearch.php"); ?>

</div>


<!-- Initialize Swiper -->
<script>
    var swiper2 = new Swiper('.swiper2', {
        slidesPerView: 5,
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next-recent',
            prevEl: '.swiper-button-prev-recent',
        },
    });
</script>
<script>
    var swiper3 = new Swiper('.swiper3', {
        slidesPerView: 5,
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next-highlight',
            prevEl: '.swiper-button-prev-highlight',
        },
    });
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
</body>
</html>
<?php
    if(isset($_GET['videoId'])) {
        header('Location: /highlight?videoId='.$_GET['videoId']);
        exit();
    }else if(isset($_GET['category']) and isset($_GET['day'])){
        header('Location: /highlight?category='.$_GET['category'].'&day='.$_GET['day']);
        exit();
    }else if(isset($_GET['category'])){
        header('Location: /highlight?category='.$_GET['category']);
        exit();
    }else{
        header('Location: /highlight');
        exit();
    }
?>
<?php
require_once 'model/VideoM.php';
require_once 'model/playLeague.php';
$VideoMObj = new VideoM();
$playLeagueObj = new playLeague();
//$dataVideoMMain = $VideoMObj->getSlideVideos("", 1,((isset($_GET['tag']))?$_GET['tag']:''));
$dataMap = ['bundesliga' => 44, 'europa' => 608, 'la liga' => 18, 'league 1' => 36, 'premier league' => 1, 'serie a' => 27, 'thai' => 580, 'ucl' => 105, 'international' => 645];
$dataTeamByLeague = Array();
if (isset($_GET['videoId'])) {
    $dataVideoMAllByGoal = array();
    $dataVideo = $VideoMObj->getVideosById($_GET['videoId']);
    $countNormal = $VideoMObj->getCountSlideVideosCategoryByStyle(((!empty($dataVideo[0]->category)) ? $dataVideo[0]->category : ''), "highlight", ((isset($_GET['tag'])) ? $_GET['tag'] : ''), 'normal');
    $countGoal = $VideoMObj->getCountSlideVideosCategoryByStyle(((!empty($dataVideo[0]->category)) ? $dataVideo[0]->category : ''), "highlight", ((isset($_GET['tag'])) ? $_GET['tag'] : ''), 'goal');
    $dataVideoMAllByNormal = $VideoMObj->getSlideVideosCategoryByStyle(((!empty($dataVideo[0]->category)) ? $dataVideo[0]->category : ''), "highlight", 10, ((isset($_GET['tag'])) ? $_GET['tag'] : ''), 'normal');
    $dataVideoMAllByGoal = $VideoMObj->getSlideVideosCategoryByStyle(((!empty($dataVideo[0]->category)) ? $dataVideo[0]->category : ''), "highlight", 10, ((isset($_GET['tag'])) ? $_GET['tag'] : ''), 'goal');

    if (!empty($dataVideoMAllByGoal)) {
        $dataVideoMHighlight = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByGoal[0]->video_id, "highlight", 7);
        $dataVideoMOthers = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByGoal[0]->video_id, "general", 7);
        $dataVideoMRelated = $VideoMObj->getRelatedVideoMore($dataVideoMAllByGoal[0]->video_id, '', 4);
    } else {
        $dataVideoMHighlight = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideo[0]->video_id, "highlight", 7);
        $dataVideoMOthers = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideo[0]->video_id, "general", 7);
        $dataVideoMRelated = $VideoMObj->getRelatedVideoMore($dataVideo[0]->video_id, '', 4);
    }
    if ($dataVideo[0]->category == "bundesliga") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(44);
    } elseif ($dataVideo[0]->category == "europa") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(608);
    } elseif ($dataVideo[0]->category == "la liga") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(18);
    } elseif ($dataVideo[0]->category == "league 1") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(36);
    } elseif ($dataVideo[0]->category == "premier league") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(1);
    } elseif ($dataVideo[0]->category == "serie a") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(27);
    } elseif ($dataVideo[0]->category == "thai") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(590);
    } elseif ($dataVideo[0]->category == "ucl") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(105);
    } elseif ($dataVideo[0]->category == "international") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(645);
    }
} else {
//    if(isset($_GET['category']) && isset($_GET['day'])){
        $countNormal = $VideoMObj->getCountSlideVideosCategoryByStyleAndDay(((isset($_GET['day'])) ? $_GET['day'] : ''), "highlight", ((isset($_GET['tag'])) ? $_GET['tag'] : ''), 'normal');
        $countGoal = $VideoMObj->getCountSlideVideosCategoryByStyleAndDay(((isset($_GET['day'])) ? $_GET['day'] : ''),((isset($_GET['category'])) ? $_GET['category'] : ''), "highlight", ((isset($_GET['tag'])) ? $_GET['tag'] : ''), 'goal');
        $dataVideoMAllByNormal = $VideoMObj->getSlideVideosCategoryByStyleAndDay(((isset($_GET['day'])) ? $_GET['day'] : ''),((isset($_GET['category'])) ? $_GET['category'] : ''), "highlight", 10, ((isset($_GET['tag'])) ? $_GET['tag'] : ''), 'normal');
        $dataVideoMAllByGoal = $VideoMObj->getSlideVideosCategoryByStyleAndDay(((isset($_GET['day'])) ? $_GET['day'] : ''),((isset($_GET['category'])) ? $_GET['category'] : ''), "highlight", 10, ((isset($_GET['tag'])) ? $_GET['tag'] : ''), 'goal');
//    }else{
//        $countNormal = $VideoMObj->getCountSlideVideosCategoryByStyle(((isset($_GET['category'])) ? $_GET['category'] : ''), "highlight", ((isset($_GET['tag'])) ? $_GET['tag'] : ''), 'normal');
//        $countGoal = $VideoMObj->getCountSlideVideosCategoryByStyle(((isset($_GET['category'])) ? $_GET['category'] : ''), "highlight", ((isset($_GET['tag'])) ? $_GET['tag'] : ''), 'goal');
//        $dataVideoMAllByNormal = $VideoMObj->getSlideVideosCategoryByStyle(((isset($_GET['category'])) ? $_GET['category'] : ''), "highlight", 10, ((isset($_GET['tag'])) ? $_GET['tag'] : ''), 'normal');
//        $dataVideoMAllByGoal = $VideoMObj->getSlideVideosCategoryByStyle(((isset($_GET['category'])) ? $_GET['category'] : ''), "highlight", 10, ((isset($_GET['tag'])) ? $_GET['tag'] : ''), 'goal');
//    }

    if(empty($dataVideoMAllByGoal)) {
        header('Location: /');
        exit;
    }

    $dataVideo = $VideoMObj->getVideosById($dataVideoMAllByGoal[0]->video_id);
    if (!empty($dataVideoMAllByGoal)) {
        $dataVideoMHighlight = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByGoal[0]->video_id, "highlight", 7);
        $dataVideoMOthers = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByGoal[0]->video_id, "general", 7);
        $dataVideoMRelated = $VideoMObj->getRelatedVideoMore($dataVideoMAllByGoal[0]->video_id, '', 4);
    } else {
        $dataVideoMHighlight = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByNormal[0]->video_id, "highlight", 7);
        $dataVideoMOthers = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByNormal[0]->video_id, "general", 7);
        $dataVideoMRelated = $VideoMObj->getRelatedVideoMore($dataVideoMAllByNormal[0]->video_id, '', 4);
    }

    if (isset($_GET['category']) and $_GET['category'] == "bundesliga") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(44);
    } elseif (isset($_GET['category']) and $_GET['category'] == "europa") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(608);
    } elseif (isset($_GET['category']) and $_GET['category'] == "la liga") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(18);
    } elseif (isset($_GET['category']) and $_GET['category'] == "league 1") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(36);
    } elseif (isset($_GET['category']) and $_GET['category'] == "premier league") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(1);
    } elseif (isset($_GET['category']) and $_GET['category'] == "serie a") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(27);
    } elseif (isset($_GET['category']) and $_GET['category'] == "thai") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(590);
    } elseif (isset($_GET['category']) and $_GET['category'] == "ucl") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(105);
    } elseif (isset($_GET['category']) and $_GET['category'] == "international") {
        $dataTeamByLeague = $playLeagueObj->checkTeamByLeague(645);
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <title>Ngoal | Video
        Highlight <?= ((isset($_GET['category'])) ? $_GET['category'] : '') ?> <?= ((!empty($dataVideo)) ? $dataVideo[0]->title : '') ?></title>
    <meta name="keywords" content="<?= ((!empty($dataVideo)) ? $dataVideo[0]->video_tag : '') ?>">
    <meta name="description" content="<?= ((!empty($dataVideo)) ? $dataVideo[0]->desc : '') ?>">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <meta property="fb:app_id" content="1630409860331473"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>"/>
    <link rel="shortcut icon" type="images/x-icon" href="/images/favicon.ico.png">
    <?php if (!empty($dataVideo)) { ?>
        <?php foreach ($dataVideo as $key => $value) { ?>
            <meta property="og:title" content="<?php echo $value->title; ?>"/>
            <meta property="og:description" content="<?php echo $value->desc; ?>"/>
            <meta property="og:image"
                  content="<?php echo((strpos($value->thumbnail, "http://") === 0) ? $value->thumbnail : "https://" . $_SERVER['SERVER_NAME'] . $value->thumbnail); ?>"/>
        <?php } ?>
    <?php } else if (!empty($dataVideoMAll)) { ?>
        <?php foreach ($dataVideoMAll as $key => $value) { ?>
            <?php if ($key == 0) { ?>
                <meta property="og:title" content="<?php echo $value->title; ?>"/>
                <meta property="og:description" content="<?php echo $value->desc; ?>"/>
                <meta property="og:image"
                      content="<?php echo((strpos($value->thumbnail, "http://") === 0) ? $value->thumbnail : "https://" . $_SERVER['SERVER_NAME'] . $value->thumbnail); ?>"/>
            <?php } ?>
        <?php } ?>
    <?php } else if ($dataVideoMAllByGoal) { ?>
        <?php foreach ($dataVideoMAllByGoal as $key => $value) { ?>
            <?php if ($key == 0) { ?>
                <meta property="og:title" content="<?php echo $value->title; ?>"/>
                <meta property="og:description" content="<?php echo $value->desc; ?>"/>
                <meta property="og:image"
                      content="<?php echo((strpos($value->thumbnail, "http://") === 0) ? $value->thumbnail : "https://" . $_SERVER['SERVER_NAME'] . $value->thumbnail); ?>"/>
            <?php } ?>
        <?php } ?>
    <?php } else if ($dataVideoMAllByNormal) { ?>
        <?php foreach ($dataVideoMAllByNormal as $key => $value) { ?>
            <?php if ($key == 0) { ?>
                <meta property="og:title" content="<?php echo $value->title; ?>"/>
                <meta property="og:description" content="<?php echo $value->desc; ?>"/>
                <meta property="og:image"
                      content="<?php echo((strpos($value->thumbnail, "http://") === 0) ? $value->thumbnail : "https://" . $_SERVER['SERVER_NAME'] . $value->thumbnail); ?>"/>
            <?php } ?>
        <?php } ?>
    <?php } ?>

    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="nanoscrollbar/nanoscrollbar.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="js/jquery-timeago/jquery.timeago.js"></script>
    <script src="/js/moment.js"></script>
    <script src="js/sly/sly.js"></script>
    <style type="text/css">
        body {
            background-color: #f1f1f1;
        }

        div.twitter-video {
            max-height: none !important;
            max-width: none !important;
            min-width: none !important;
            min-height: none !important;
            width: 100% !important;
            height: 100% !important;
        }

        .fb-share-button span {
            width: 100% !important;
            vertical-align: middle !important;
        }
        .content-menu-index.affix {
           position: relative;
        }
        .content-menu-index .container{
            width: 100%;
        }
    </style>
    <?php include 'view/index/googletagmanager.php'; ?>
</head>
<body>
<input id="input_day" type="hidden" value="<?php echo((isset($_GET['day'])) ? $_GET['day'] : ''); ?>">
<?php if (isset($_GET['videoId'])) { ?>
    <input id="input_category" type="hidden"
           value="<?php echo(!empty($dataVideo) and (!empty($dataVideo[0]->category)) ? $dataVideo[0]->category : ''); ?>">
    <input id="input_tag" type="hidden" value="<?php echo((isset($_GET['tag'])) ? $_GET['tag'] : ''); ?>">
<?php } else { ?>
    <input id="input_category" type="hidden"
           value="<?php echo((isset($_GET['category'])) ? $_GET['category'] : ''); ?>">
    <input id="input_tag" type="hidden" value="<?php echo((isset($_GET['tag'])) ? $_GET['tag'] : ''); ?>">
<?php } ?>
<div class="content-social-sharing hide">
    <ul>
        <li class="box-facebook-sharing">
            <i class="fa fa-facebook" aria-hidden="true"></i>
            <div class="box-sharing-hover">
                <div style="float: left;display: block;padding-left: 5px;width: 100%;" class="fb-share-button"
                     data-href="" data-layout="button" data-size="large" data-mobile-iframe="true">
                    <a class="fb-xfbml-parse-ignore" target="_blank" href="">
                        แชร์
                    </a>
                </div>
            </div>
        </li>
        <li class="box-g-plus-sharing">
            <i class="fa fa-google-plus" aria-hidden="true"></i>
            <div class="box-sharing-hover">
                <div style="float: left;display: block;padding-left: 5px;padding-top: 5px;">
                    <div class="g-plus" data-action="share" data-annotation="vertical-bubble" data-height="50"></div>
                </div>
            </div>
        </li>
    </ul>
</div>

<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
<!--index menu-->
<?php include $_SERVER['DOCUMENT_ROOT'] . '/view/index/menu-all.php'; ?>
<div class="container-feature-video">
    <div class="col-lg-12" style="padding: 0;">
        <div class="tab-h-content sexy-football-topic" style="margin: 0;">
            <div class="pull-left header-filter">
                <?php if ((!isset($_GET['category'])) and (!isset($_GET['videoId']))) { ?>
                    <div class="logo-header-filter">
                        <img src="/images/five-leagues/all.png">
                    </div>
                    <h3>วิดีโอ/ไฮไลท์บอล</h3>
                <?php } elseif ((isset($_GET['category']) && $_GET['category'] == "premier league") or (!empty($dataVideo[0]->category) and $dataVideo[0]->category == "premier league")) { ?>
                    <div class="logo-header-filter">
                        <img src="/images/five-leagues/premier-league-logo.png">
                    </div>
                    <h3>วิดีโอ/ไฮไลท์บอล พรีเมียร์ลีก</h3>
                    <div class="dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenu1"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            เลือกทีม
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a onclick="findTeam('all')"><img src="/images/five-leagues/premier-league-logo.png">
                                    English Premier League</a></li>
                            <?php foreach ($dataTeamByLeague as $key => $value) { ?>
                                <li><a onclick="findTeam('<?= $value['name_en'] ?>')"><img
                                                src="<?= $value['path'] ?>"> <?= $value['name_en'] ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } elseif ((isset($_GET['category']) && $_GET['category'] == "la liga") or (!empty($dataVideo[0]->category) and $dataVideo[0]->category == "la liga")) { ?>
                    <div class="logo-header-filter">
                        <img src="/images/five-leagues/La-Liga-Logo-1.png">
                    </div>
                    <h3>วิดีโอ/ไฮไลท์บอล ลาลีกา</h3>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            เลือกทีม
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a onclick="findTeam('all')"><img src="/images/five-leagues/La-Liga-Logo-1.png"> Spanish
                                    Primera Division</a></li>
                            <?php foreach ($dataTeamByLeague as $key => $value) { ?>
                                <li><a onclick="findTeam('<?= $value['name_en'] ?>')"><img
                                                src="<?= $value['path'] ?>"> <?= $value['name_en'] ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } elseif ((isset($_GET['category']) && $_GET['category'] == "bundesliga") or (!empty($dataVideo[0]->category) and $dataVideo[0]->category == "bundesliga")) { ?>
                    <div class="logo-header-filter">
                        <img src="/images/five-leagues/logo_bl.gif">
                    </div>
                    <h3>วิดีโอ/ไฮไลท์บอล บุนเดสลีกา</h3>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            เลือกทีม
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a onclick="findTeam('all')"><img src="/images/five-leagues/logo_bl.gif"> German
                                    Bundesliga</a></li>
                            <?php foreach ($dataTeamByLeague as $key => $value) { ?>
                                <li><a onclick="findTeam('<?= $value['name_en'] ?>')"><img
                                                src="<?= $value['path'] ?>"> <?= $value['name_en'] ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } elseif ((isset($_GET['category']) && $_GET['category'] == "serie a") or (!empty($dataVideo[0]->category) and $dataVideo[0]->category == "serie a")) { ?>
                    <div class="logo-header-filter">
                        <img src="/images/five-leagues/seriea1.png">
                    </div>
                    <h3>วิดีโอ/ไฮไลท์บอล เซเรียอา</h3>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            เลือกทีม
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a onclick="findTeam('all')"><img src="/images/five-leagues/seriea1.png"> Italian Serie
                                    A</a></li>
                            <?php foreach ($dataTeamByLeague as $key => $value) { ?>
                                <li><a onclick="findTeam('<?= $value['name_en'] ?>')"><img
                                                src="<?= $value['path'] ?>"> <?= $value['name_en'] ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } elseif ((isset($_GET['category']) && $_GET['category'] == "league 1") or (!empty($dataVideo[0]->category) and $dataVideo[0]->category == "league 1")) { ?>
                    <div class="logo-header-filter">
                        <img src="/images/five-leagues/Ligue-1-logo-france.png">
                    </div>
                    <h3>วิดีโอ/ไฮไลท์บอล ลีกเอิง</h3>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            เลือกทีม
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a onclick="findTeam('all')"><img src="/images/five-leagues/Ligue-1-logo-france.png">
                                    French Le Ligue 1</a></li>
                            <?php foreach ($dataTeamByLeague as $key => $value) { ?>
                                <li><a onclick="findTeam('<?= $value['name_en'] ?>')"><img
                                                src="<?= $value['path'] ?>"> <?= $value['name_en'] ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } elseif ((isset($_GET['category']) && $_GET['category'] == "thai") or (!empty($dataVideo[0]->category) and $dataVideo[0]->category == "thai")) { ?>
                    <div class="logo-header-filter">
                        <img src="/images/five-leagues/T1_Logo2.png">
                    </div>
                    <h3>วิดีโอ/ไฮไลท์บอล ไทยลีก</h3>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            เลือกทีม
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a onclick="findTeam('all')"><img src="/images/five-leagues/T1_Logo2.png"> Thai League 1</a>
                            </li>
                            <?php foreach ($dataTeamByLeague as $key => $value) { ?>
                                <li><a onclick="findTeam('<?= $value['name_en'] ?>')"><img
                                                src="<?= $value['path'] ?>"> <?= $value['name_en'] ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } elseif ((isset($_GET['category']) && $_GET['category'] == "ucl") or (!empty($dataVideo[0]->category) and $dataVideo[0]->category == "ucl")) { ?>
                    <div class="logo-header-filter">
                        <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                    </div>
                    <h3>วิดีโอ/ไฮไลท์บอล ยูฟ่าแชมเปียนส์ลีก</h3>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            เลือกทีม
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a onclick="findTeam('all')"><img
                                            src="/images/five-leagues/UEFA-Champions-League-1.png"> UEFA Champions
                                    League</a></li>
                            <?php foreach ($dataTeamByLeague as $key => $value) { ?>
                                <li><a onclick="findTeam('<?= $value['name_en'] ?>')"><img
                                                src="<?= $value['path'] ?>"> <?= $value['name_en'] ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } elseif ((isset($_GET['category']) && $_GET['category'] == "europa") or (!empty($dataVideo[0]->category) and $dataVideo[0]->category == "europa")) { ?>
                    <div class="logo-header-filter">
                        <img src="/images/five-leagues/uropa.png">
                    </div>
                    <h3>วิดีโอ/ไฮไลท์บอล ยูฟ่ายูโรปาลีก</h3>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            เลือกทีม
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a onclick="findTeam('all')"><img src="/images/five-leagues/uropa.png"> UEFA Europa
                                    League</a></li>
                            <?php foreach ($dataTeamByLeague as $key => $value) { ?>
                                <li><a onclick="findTeam('<?= $value['name_en'] ?>')"><img
                                                src="<?= $value['path'] ?>"> <?= $value['name_en'] ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } else { ?>
                    <div class="logo-header-filter">
                        <img src="/images/five-leagues/all.png">
                    </div>
                    <h3>วิดีโอ/ไฮไลท์บอล</h3>
                <?php } ?>


            </div>

            <div class="filter-list-video hide pull-right">
                <select id="select-filter-list-video" class="form-control">
                    <option value="">Filter by Date</option>
                    <option <?= (((isset($_GET['category']) && $_GET['category'] == "premier league") or (!empty($dataVideo[0]->category) and !empty($dataVideo[0]->category) == "premier league")) ? 'selected' : '') ?>
                            value="premier league">English Premier League
                    </option>
                    <option <?= (((isset($_GET['category']) && $_GET['category'] == "la liga") or (!empty($dataVideo[0]->category) and !empty($dataVideo[0]->category) == "la liga")) ? 'selected' : '') ?>
                            value="la liga">
                        Spanish Primera Division
                    </option>
                    <option <?= (((isset($_GET['category']) && $_GET['category'] == "bundesliga") or (!empty($dataVideo[0]->category) and !empty($dataVideo[0]->category) == "bundesliga")) ? 'selected' : '') ?>
                            value="bundesliga">German Bundesliga
                    </option>
                    <option <?= (((isset($_GET['category']) && $_GET['category'] == "serie a") or (!empty($dataVideo[0]->category) and !empty($dataVideo[0]->category) == "serie a")) ? 'selected' : '') ?>
                            value="serie a">
                        Italian Serie A
                    </option>
                    <option <?= (((isset($_GET['category']) && $_GET['category'] == "league 1") or (!empty($dataVideo[0]->category) and !empty($dataVideo[0]->category) == "league 1")) ? 'selected' : '') ?>
                            value="league 1">French Le Championnat Ligue 1
                    </option>
                    <option <?= (((isset($_GET['category']) && $_GET['category'] == "thai") or (!empty($dataVideo[0]->category) and !empty($dataVideo[0]->category) == "thai")) ? 'selected' : '') ?>
                            value="thai">Thai
                        Premier League
                    </option>
                    <option <?= (((isset($_GET['category']) && $_GET['category'] == "ucl") or (!empty($dataVideo[0]->category) and !empty($dataVideo[0]->category) == "ucl")) ? 'selected' : '') ?>
                            value="ucl">
                        UEFA
                        Champions League
                    </option>
                    <option <?= (((isset($_GET['category']) && $_GET['category'] == "europa") or (!empty($dataVideo[0]->category) and !empty($dataVideo[0]->category) == "europa")) ? 'selected' : '') ?>
                            value="europa">
                        UEFA Europa League
                    </option>
                </select>
            </div>
            <div class="pull-right content-filter">
                <table>
                    <tr>
                        <td>
                            <div class="select-text">เลือกลีก <i class="fa fa-angle-right" aria-hidden="true"></i></div>
                        </td>
                        <td class="<?= ((!isset($_GET['category']) and (!isset($_GET['videoId']))) ? 'active' : '') ?>">
                            <div class="logo-hightlight">
                                <a href="/videos"><img src="/images/five-leagues/all.png"></a>
                            </div>
                        </td>
                        <td class="<?= (((isset($_GET['category']) && $_GET['category'] == "premier league") or (isset($_GET['videoId']) and !empty($dataVideo[0]->category) and $dataVideo[0]->category == "premier league")) ? 'active' : '') ?>">
                            <div class="logo-hightlight">
                                <a href="/videos?category=premier league"><img
                                            src="/images/five-leagues/premier-league-logo.png"></a>
                            </div>
                        </td>
                        <td class="<?= (((isset($_GET['category']) && $_GET['category'] == "la liga") or (isset($_GET['videoId']) and !empty($dataVideo[0]->category) and $dataVideo[0]->category == "la liga")) ? 'active' : '') ?>">
                            <div class="logo-hightlight">
                                <a href="/videos?category=la liga"><img
                                            src="/images/five-leagues/La-Liga-Logo-1.png"></a>
                            </div>
                        </td>
                        <td class="<?= (((isset($_GET['category']) && $_GET['category'] == "bundesliga") or (isset($_GET['videoId']) and !empty($dataVideo[0]->category) and $dataVideo[0]->category == "bundesliga")) ? 'active' : '') ?>">
                            <div class="logo-hightlight">
                                <a href="/videos?category=bundesliga"><img
                                            src="/images/five-leagues/logo_bl.gif"></a>
                            </div>
                        </td>
                        <td class="<?= (((isset($_GET['category']) && $_GET['category'] == "serie a") or (isset($_GET['videoId']) and !empty($dataVideo[0]->category) and $dataVideo[0]->category == "serie a")) ? 'active' : '') ?>">
                            <div class="logo-hightlight">
                                <a href="/videos?category=serie a"><img src="/images/five-leagues/seriea1.png"></a>
                            </div>
                        </td>
                        <td class="<?= (((isset($_GET['category']) && $_GET['category'] == "league 1") or (isset($_GET['videoId']) and !empty($dataVideo[0]->category) and $dataVideo[0]->category == "league 1")) ? 'active' : '') ?>">
                            <div class="logo-hightlight">
                                <a href="/videos?category=league 1"><img
                                            src="/images/five-leagues/Ligue-1-logo-france.png"></a>
                            </div>
                        </td>
                        <td class="<?= (((isset($_GET['category']) && $_GET['category'] == "ucl") or (isset($_GET['videoId']) and !empty($dataVideo[0]->category) and $dataVideo[0]->category == "ucl")) ? 'active' : '') ?>">
                            <div class="logo-hightlight">
                                <a href="/videos?category=ucl"><img
                                            src="/images/five-leagues/UEFA-Champions-League-1.png"></a>
                            </div>
                        </td>
                        <td class="<?= (((isset($_GET['category']) && $_GET['category'] == "europa") or (isset($_GET['videoId']) and !empty($dataVideo[0]->category) and $dataVideo[0]->category == "europa")) ? 'active' : '') ?>">
                            <div class="logo-hightlight">
                                <a href="/videos?category=europa"><img src="/images/five-leagues/uropa.png"></a>
                            </div>
                        </td>
                        <td class="<?= (((isset($_GET['category']) && $_GET['category'] == "thai") or (isset($_GET['videoId']) and !empty($dataVideo[0]->category) and $dataVideo[0]->category == "thai")) ? 'active' : '') ?>">
                            <div class="logo-hightlight">
                                <a href="/videos?category=thai"><img src="/images/five-leagues/T1_Logo2.png"></a>
                            </div>
                        </td>
                        <td class="<?= (((isset($_GET['category']) && $_GET['category'] == "international") or (isset($_GET['videoId']) and !empty($dataVideo[0]->category) and $dataVideo[0]->category == "international")) ? 'active' : '') ?>">
                            <div class="logo-hightlight">
                                <a href="/videos?category=international"><img src="/images/world-cup-logo.png"></a>
                            </div>
                        </td>

                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="row" style="margin-right: 0;">
        <div class="bg-large-video">
            <div class="wrap-content-cols-2 col-lg-12">
                <div class="col-sm-8" style="position:static !important; margin-top: -10px; padding-left: 15px;">
                    <div class="box-large-videos" style="width: 100%; position: relative; height: 600px; z-index: 6; display: table;">
                        <div id="box-large-videos-autoPlay" style="">
                            <div style="position: absolute;display: none; left: 0px; right: 0;margin: 0 auto;" id="btn_Close_Iframe"
                                 onclick="closeIframe()">
                                <span class="pull-left"><i class="fa fa-video-camera"></i> Video</span>
                                <span class="pull-right"><i class="fa fa-close"></i></span>
                            </div>
                            <?php if (!empty($dataVideo)) { ?>
                                <?php foreach ($dataVideo as $key => $value) { ?>
                                    <?php if ($value->videosource == "dailymotion") { ?>
                                        <iframe style="position: absolute;left: 0; z-index: 6;" id="large-videos-autoPlay"
                                                width="100%"
                                                height="100%" data-autoplay="true"
                                                src="http://www.dailymotion.com/embed/video/<?php echo $value->videokey ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } elseif ($value->videosource == "twitter") { ?>
                                        <blockquote class="twitter-video" data-lang="en"
                                                    style="width:100%;height: 100%;">
                                            <a href="<?php echo $value->content ?>"></a>
                                        </blockquote>
                                    <?php } elseif ($value->videosource == "facebook") { ?>
                                        <iframe style="position: absolute;left: 0px;" id="large-videos-autoPlay"
                                                width="100%"
                                                height="100%" data-autoplay="true"
                                                src="https://www.facebook.com/v2.3/plugins/video.php?allowfullscreen=true&container_width=800&href=<?php echo $value->content ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } elseif ($value->videosource == "streamable") { ?>
                                        <?php if (!empty($value->pathFile)) { ?>
                                            <?php if (isset($_GET['videoId'])) { ?>
                                                <video id="large-videos-autoPlay" style="width: 100%;"
                                                       class="video-player-tag" controls autoplay>
                                                    <source class="mp4-source" src="<?= $value->pathFile ?>">
                                                </video>
                                            <?php } else { ?>
                                                <video id="large-videos-autoPlay" style="width: 100%;"
                                                       class="video-player-tag" controls autoplay>
                                                    <source class="mp4-source" src="<?= $value->pathFile ?>">
                                                </video>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <?php if (isset($_GET['videoId'])) { ?>
                                                <iframe id="large-videos-autoPlay"
                                                        width="100%"
                                                        height="100%"
                                                        data-autoplay="true"
                                                        src="<?php echo $value->urlIframe ?>"
                                                        frameborder="0"
                                                        allow="autoplay"
                                                        style="height: 100%;position: absolute;top: 0;left: 0"
                                                ></iframe>
                                            <?php } else { ?>
                                                <iframe id="large-videos-autoPlay"
                                                        width="100%"
                                                        height="100%"
                                                        data-autoplay="true"
                                                        src="https://streamable.com/s/<?php echo $value->videokey ?>"
                                                        frameborder="0"
                                                        allow="autoplay"
                                                        style="height: 100%;position: absolute;top: 0;left: 0"
                                                ></iframe>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <iframe style="position: absolute;left: 0px;" id="large-videos-autoPlay"
                                                width="100%"
                                                height="100%" data-autoplay="true"
                                                src="http://www.youtube.com/embed/<?php echo $value->videokey ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } ?>
                                <?php } ?>
                            <?php } else { ?>
                                <?php if (!empty($dataVideoMAllByGoal)) { ?>
                                    <?php foreach ($dataVideoMAllByGoal as $key => $value) { ?>
                                        <?php if ($key == 0) { ?>
                                            <?php if ($value->videosource == "dailymotion") { ?>
                                                <iframe style="position: absolute;left: 0px;" id="large-videos-autoPlay"
                                                        width="100%" height="100%" data-autoplay="true"
                                                        src="http://www.dailymotion.com/embed/video/<?php echo $value->videokey ?>"
                                                        frameborder="0"
                                                        allowfullscreen></iframe>
                                            <?php } elseif ($value->videosource == "twitter") { ?>
                                                <blockquote class="twitter-video" data-lang="en"
                                                            style="width:100%;height: 100%;">
                                                    <a href="<?php echo $value->content ?>"></a>
                                                </blockquote>
                                            <?php } elseif ($value->videosource == "facebook") { ?>
                                                <iframe style="position: absolute;left: 0px;" id="large-videos-autoPlay"
                                                        width="100%" height="100%" data-autoplay="true"
                                                        src="https://www.facebook.com/v2.3/plugins/video.php?allowfullscreen=true&container_width=800&href=<?php echo $value->content ?>"
                                                        frameborder="0"
                                                        allowfullscreen></iframe>
                                            <?php } elseif ($value->videosource == "streamable") { ?>
                                                <?php if (!empty($value->pathFile)) { ?>
                                                    <?php if (isset($_GET['videoId'])) { ?>
                                                        <video id="large-videos-autoPlay" style="width: 100%;"
                                                               class="video-player-tag" controls autoplay>
                                                            <source class="mp4-source" src="<?= $value->pathFile ?>">
                                                        </video>
                                                    <?php } else { ?>
                                                        <video id="large-videos-autoPlay" style="width: 100%;"
                                                               class="video-player-tag" controls>
                                                            <source class="mp4-source" src="<?= $value->pathFile ?>">
                                                        </video>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                    <?php if (isset($_GET['videoId'])) { ?>
                                                        <iframe id="large-videos-autoPlay"
                                                                width="100%"
                                                                height="100%" data-autoplay="true"
                                                                src="<?php echo $value->urlIframe ?>"
                                                                frameborder="0"
                                                                allowfullscreen></iframe>
                                                    <?php } else { ?>
                                                        <iframe id="large-videos-autoPlay"
                                                                width="100%"
                                                                height="100%" data-autoplay="true"
                                                                src="https://streamable.com/s/<?php echo $value->videokey ?>"
                                                                frameborder="0"
                                                                allowfullscreen></iframe>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <iframe style="position: absolute;left: 0px;" id="large-videos-autoPlay"
                                                        width="100%" height="100%" data-autoplay="true"
                                                        src="http://www.youtube.com/embed/<?php echo $value->videokey ?>"
                                                        frameborder="0"
                                                        allowfullscreen></iframe>
                                            <?php }
                                        } ?>
                                    <?php } ?>
                                <?php } else if (!empty($dataVideoMAllByNormal)) { ?>
                                    <?php foreach ($dataVideoMAllByNormal as $key => $value) { ?>
                                        <?php if ($key == 0) { ?>
                                            <?php if ($value->videosource == "dailymotion") { ?>
                                                <iframe style="position: absolute;left: 0px;" id="large-videos-autoPlay"
                                                        width="100%" height="100%" data-autoplay="true"
                                                        src="http://www.dailymotion.com/embed/video/<?php echo $value->videokey ?>"
                                                        frameborder="0"
                                                        allowfullscreen></iframe>
                                            <?php } elseif ($value->videosource == "twitter") { ?>
                                                <blockquote class="twitter-video" data-lang="en"
                                                            style="width:100%;height: 100%;">
                                                    <a href="<?php echo $value->content ?>"></a>
                                                </blockquote>
                                                <!--                                    <iframe style="position: absolute;left: 0px;" id="large-videos-autoPlay"-->
                                                <!--                                            width="100%" height="700" data-autoplay="true"-->
                                                <!--                                            src="https://twitter.com/i/cards/tfw/v1/--><?php //echo $value->videokey ?><!--"-->
                                                <!--                                            frameborder="0"-->
                                                <!--                                            allowfullscreen></iframe>-->
                                            <?php } elseif ($value->videosource == "facebook") { ?>
                                                <iframe style="position: absolute;left: 0px;" id="large-videos-autoPlay"
                                                        width="100%" height="100%" data-autoplay="true"
                                                        src="https://www.facebook.com/v2.3/plugins/video.php?allowfullscreen=true&container_width=800&href=<?php echo $value->content ?>"
                                                        frameborder="0"
                                                        allowfullscreen></iframe>
                                            <?php } elseif ($value->videosource == "streamable") { ?>
                                                <?php if (!empty($value->pathFile)) { ?>
                                                    <?php if (isset($_GET['videoId'])) { ?>
                                                        <video id="large-videos-autoPlay" style="width: 100%;"
                                                               class="video-player-tag" controls autoplay>
                                                            <source class="mp4-source" src="<?= $value->pathFile ?>">
                                                        </video>
                                                    <?php } else { ?>
                                                        <video id="large-videos-autoPlay" style="width: 100%;"
                                                               class="video-player-tag" controls>
                                                            <source class="mp4-source" src="<?= $value->pathFile ?>">
                                                        </video>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                    <?php if (isset($_GET['videoId'])) { ?>
                                                        <iframe id="large-videos-autoPlay"
                                                                width="100%"
                                                                height="100%" data-autoplay="true"
                                                                src="<?php echo $value->urlIframe ?>"
                                                                frameborder="0"
                                                                allowfullscreen></iframe>
                                                    <?php } else { ?>
                                                        <iframe id="large-videos-autoPlay"
                                                                width="100%"
                                                                height="100%" data-autoplay="true"
                                                                src="https://streamable.com/s/<?php echo $value->videokey ?>"
                                                                frameborder="0"
                                                                allowfullscreen></iframe>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <iframe style="position: absolute;left: 0px;" id="large-videos-autoPlay"
                                                        width="100%" height="100%" data-autoplay="true"
                                                        src="http://www.youtube.com/embed/<?php echo $value->videokey ?>"
                                                        frameborder="0"
                                                        allowfullscreen></iframe>
                                            <?php }
                                        } ?>
                                    <?php } ?>
                                <?php }
                            } ?>
                        </div>
                    </div>
                    <div class="bg-content-videos">
                        <div class="wrap-content-cols-2">
                            <div>
                                <div class="content-large-videos">
                                    <?php if (!empty($dataVideo)) { ?>
                                        <?php foreach ($dataVideo as $key => $value) { ?>
                                            <h1 id="large-videos-title"><?php echo $value->title ?></h1>
                                            <div class="contents-row rows">
                                                <div class="col-sm-3">
                                                    <div class="bx-publish-info">
                                                        <table>
                                                            <tr>
                                                                <td id="large-videos-time">
                                                                    <?php echo date(' g:i A ', strtotime($value->create_datetime)) ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="large-videos-date">
                                                                    <?php echo date(' F j, Y', strtotime($value->create_datetime)) ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="bx-social" style="width: 180px;">
                                                        <?php include 'view/social/bx-social.php'; ?>
                                                        <!--                                    <span style="color: #3B579D;"><i class="fa fa-facebook"></i> </span>-->
                                                        <!--                                    <span style="color: #4AC7F9;"><i class="fa fa-twitter"></i></span>-->
                                                        <!--                                    <span> <i class="fa fa-mail-forward"></i> </span>-->
                                                    </div>
                                                </div>
                                                <div class="col-sm-9" id="large-videos-desc">
                                                    <?php echo $value->desc ?>
                                                </div>
                                                <div style="clear: both;"></div>
                                            </div>
                                        <?php } ?>
                                    <?php } elseif (!empty($dataVideoMAll)) { ?>
                                        <?php foreach ($dataVideoMAll as $key => $value) { ?>
                                            <?php if ($key == 0) { ?>
                                                <h1 id="large-videos-title"><?php echo $value->title ?></h1>
                                                <div class="contents-row rows">
                                                    <div class="col-sm-3">
                                                        <div class="bx-publish-info">
                                                            <table>
                                                                <tr>
                                                                    <td id="large-videos-time">
                                                                        <?php echo date(' g:i A ', strtotime($value->create_datetime)) ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="large-videos-date">
                                                                        <?php echo date(' F j, Y', strtotime($value->create_datetime)) ?>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="bx-social">
                                                            <?php include 'view/social/bx-social.php'; ?>
                                                            <!--                                        <span style="color: #3B579D;"><i class="fa fa-facebook"></i> </span>-->
                                                            <!--                                        <span style="color: #4AC7F9;"><i class="fa fa-twitter"></i></span>-->
                                                            <!--                                        <span> <i class="fa fa-mail-forward"></i> </span>-->
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-9" id="large-videos-desc">
                                                        <?php echo $value->desc ?>
                                                    </div>
                                                    <div style="clear: both;"></div>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } elseif (!empty($dataVideoMAllByGoal)) { ?>
                                        <?php foreach ($dataVideoMAllByGoal as $key => $value) { ?>
                                            <?php if ($key == 0) { ?>
                                                <h1 id="large-videos-title"><?php echo $value->title ?></h1>
                                                <div class="contents-row rows">
                                                    <div class="col-sm-3">
                                                        <div class="bx-publish-info">
                                                            <table>
                                                                <tr>
                                                                    <td id="large-videos-time">
                                                                        <?php echo date(' g:i A ', strtotime($value->create_datetime)) ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="large-videos-date">
                                                                        <?php echo date(' F j, Y', strtotime($value->create_datetime)) ?>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="bx-social">
                                                            <!--                                                            --><?php //include 'view/social/bx-social.php'; ?>
                                                            <!--                                        <span style="color: #3B579D;"><i class="fa fa-facebook"></i> </span>-->
                                                            <!--                                        <span style="color: #4AC7F9;"><i class="fa fa-twitter"></i></span>-->
                                                            <!--                                        <span> <i class="fa fa-mail-forward"></i> </span>-->
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-9" id="large-videos-desc">
                                                        <?php echo $value->desc ?>
                                                    </div>
                                                    <div style="clear: both;"></div>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } elseif (!empty($dataVideoMAllByNormal)) { ?>
                                        <?php foreach ($dataVideoMAllByNormal as $key => $value) { ?>
                                            <?php if ($key == 0) { ?>
                                                <h1 id="large-videos-title"><?php echo $value->title ?></h1>
                                                <div class="contents-row rows">
                                                    <div class="col-sm-3">
                                                        <div class="bx-publish-info">
                                                            <table>
                                                                <tr>
                                                                    <td id="large-videos-time">
                                                                        <?php echo date(' g:i A ', strtotime($value->create_datetime)) ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="large-videos-date">
                                                                        <?php echo date(' F j, Y', strtotime($value->create_datetime)) ?>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="bx-social">
                                                            <?php include 'view/social/bx-social.php'; ?>
                                                            <!--                                        <span style="color: #3B579D;"><i class="fa fa-facebook"></i> </span>-->
                                                            <!--                                        <span style="color: #4AC7F9;"><i class="fa fa-twitter"></i></span>-->
                                                            <!--                                        <span> <i class="fa fa-mail-forward"></i> </span>-->
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-9" id="large-videos-desc">
                                                        <?php echo $value->desc ?>
                                                    </div>
                                                    <div style="clear: both;"></div>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box-tabs-videolist">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-video-list" role="tablist">
                            <li role="presentation"
                                class="<?= ((!empty($dataVideo[0]->videostyle) and $dataVideo[0]->videostyle == "goal") ? 'active' : '') ?>">
                                <a href="#tab-hilight-goal" aria-controls="home" role="tab" data-toggle="tab">ไฮไลท์ยิงประตู
                                    (<span id="total-video-Goal"><?= $countGoal[0]->total ?></span>)</a>
                            </li>
                            <li role="presentation"
                                class="<?= ((!empty($dataVideo[0]->videostyle) and $dataVideo[0]->videostyle == "normal") ? 'active' : '') ?>">
                                <a href="#tab-hilight-full" aria-controls="profile" role="tab"
                                   data-toggle="tab">ไฮไลท์เต็ม (<span
                                            id="total-video-Normal"><?= $countNormal[0]->total ?></span>)</a>
                            </li>
                        </ul>
                        <div id="about" class="nano box-lists">
                            <div class="content" id="div-content">
                                <!-- Tab panes -->
                                <div class="tab-content" id="div-tab-content">
                                    <div role="tabpanel"
                                         class="tab-pane <?= ((empty($dataVideo[0]->videostyle)) ? 'active' : '') ?> <?= ((!empty($dataVideo[0]->videostyle) and $dataVideo[0]->videostyle == "goal") ? 'active' : '') ?>"
                                         id="tab-hilight-goal">
                                        <div class="box-list-videos">
                                            <table>
                                                <?php foreach ($dataVideoMAllByGoal as $key => $value) { ?>
                                                    <tr tag="<?= trim($value->video_tag) ?>"
                                                        data-number="<?= ceil(($key + 1) / 7) ?>"
                                                        class="<?= ((!empty($dataVideo[0]) and $value->video_id == $dataVideo[0]->video_id) ? 'playing-box' : '') ?> tr-goal tr-<?= ceil(($key + 1) / 7) ?> <?= ((ceil(($key + 1) / 7) <= 3) ? '' : 'hide') ?>"
                                                        onclick="setIndexVideosAutoPlay('<?php echo $value->urlIframe ?>','<?php echo $value->video_tag ?>','<?php echo $value->title ?>','','<?php echo $value->create_datetime ?>','<?php echo $value->video_id ?>')">
                                                        <td>
                                                            <div class="box-list-video-side">
                                                                <div class="icon-play">
                                                                    <i class="fa fa-play"></i>
                                                                </div>
                                                                <img width="168" height="94"
                                                                     src="<?= $value->urlImg; ?>">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="box-content-list">
                                                                <?= $value->title ?>
                                                            </div>
                                                            <?php if ((!empty($dataVideo[0]) and $value->video_id == $dataVideo[0]->video_id)) { ?>
                                                                <span class="label label-danger">
                                                                    <i class="fa fa-play" aria-hidden="true"></i> กำลังเล่น
                                                                </span>
                                                            <?php } else { ?>
                                                                <span class="label">
                                                                    <?= date('d-M-Y', strtotime($value->create_datetime)); ?>
                                                                </span>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </table>
                                        </div>
                                    </div>
                                    <div role="tabpanel"
                                         class="tab-pane <?= ((!empty($dataVideo[0]->videostyle) and $dataVideo[0]->videostyle == "normal") ? 'active' : '') ?>"
                                         id="tab-hilight-full">
                                        <div class="box-list-videos">
                                            <table>
                                                <?php foreach ($dataVideoMAllByNormal as $key => $value) { ?>
                                                    <tr tag="<?= trim($value->video_tag) ?>"
                                                        data-number="<?= ceil(($key + 1) / 7) ?>"
                                                        class="<?= ((!empty($dataVideo[0]) and $value->video_id == $dataVideo[0]->video_id) ? 'playing-box' : '') ?> tr-normal tr-<?= ceil(($key + 1) / 7) ?> <?= ((ceil(($key + 1) / 7) <= 3) ? '' : 'hide') ?>"
                                                        onclick="setIndexVideosAutoPlay('<?php echo $value->urlIframe ?>','<?php echo $value->video_tag ?>','<?php echo $value->title ?>','','<?php echo $value->create_datetime ?>','<?php echo $value->video_id ?>')">
                                                        <td>
                                                            <div class="box-list-video-side">
                                                                <div class="icon-play">
                                                                    <i class="fa fa-play"></i>
                                                                </div>
                                                                <img width="168" height="94"
                                                                     src="<?= $value->urlImg; ?>">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="box-content-list">
                                                                <?= $value->title ?>
                                                            </div>
                                                            <?php if ((!empty($dataVideo[0]) and $value->video_id == $dataVideo[0]->video_id)) { ?>
                                                                <span class="label label-danger">
                                                                    <i class="fa fa-play" aria-hidden="true"></i> กำลังเล่น
                                                                </span>
                                                            <?php } else { ?>
                                                                <span class="label">
                                                                    <?= date('d-M-Y', strtotime($value->create_datetime)); ?>
                                                                </span>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pane">
                            <div class="slider"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid bg-content-video" style="display:none;">
    <div class="wrap-content-cols-2">
        <div class="container">
            <div class="content-large-videos">
                <?php if (!empty($dataVideo)) { ?>
                    <?php foreach ($dataVideo as $key => $value) { ?>
                        <h1 id="large-videos-title"><?php echo $value->title ?></h1>
                        <div class="contents-row rows">
                            <div class="col-sm-3">
                                <div class="bx-publish-info">
                                    <table>
                                        <tr>
                                            <td id="large-videos-time">
                                                <?php echo date(' g:i A ', strtotime($value->create_datetime)) ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="large-videos-date">
                                                <?php echo date(' F j, Y', strtotime($value->create_datetime)) ?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="bx-social" style="width: 180px;">
                                    <?php include 'view/social/bx-social.php'; ?>
                                    <!--                                    <span style="color: #3B579D;"><i class="fa fa-facebook"></i> </span>-->
                                    <!--                                    <span style="color: #4AC7F9;"><i class="fa fa-twitter"></i></span>-->
                                    <!--                                    <span> <i class="fa fa-mail-forward"></i> </span>-->
                                </div>
                            </div>
                            <div class="col-sm-9" id="large-videos-desc">
                                <?php echo $value->desc ?>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                    <?php } ?>
                <?php } elseif (!empty($dataVideoMAll)) { ?>
                    <?php foreach ($dataVideoMAll as $key => $value) { ?>
                        <?php if ($key == 0) { ?>
                            <h1 id="large-videos-title"><?php echo $value->title ?></h1>
                            <div class="contents-row rows">
                                <div class="col-sm-3">
                                    <div class="bx-publish-info">
                                        <table>
                                            <tr>
                                                <td id="large-videos-time">
                                                    <?php echo date(' g:i A ', strtotime($value->create_datetime)) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="large-videos-date">
                                                    <?php echo date(' F j, Y', strtotime($value->create_datetime)) ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="bx-social">
                                        <?php include 'view/social/bx-social.php'; ?>
                                        <!--                                        <span style="color: #3B579D;"><i class="fa fa-facebook"></i> </span>-->
                                        <!--                                        <span style="color: #4AC7F9;"><i class="fa fa-twitter"></i></span>-->
                                        <!--                                        <span> <i class="fa fa-mail-forward"></i> </span>-->
                                    </div>
                                </div>
                                <div class="col-sm-9" id="large-videos-desc">
                                    <?php echo $value->desc ?>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } elseif (!empty($dataVideoMAllByGoal)) { ?>
                    <?php foreach ($dataVideoMAllByGoal as $key => $value) { ?>
                        <?php if ($key == 0) { ?>
                            <h1 id="large-videos-title"><?php echo $value->title ?></h1>
                            <div class="contents-row rows">
                                <div class="col-sm-3">
                                    <div class="bx-publish-info">
                                        <table>
                                            <tr>
                                                <td id="large-videos-time">
                                                    <?php echo date(' g:i A ', strtotime($value->create_datetime)) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="large-videos-date">
                                                    <?php echo date(' F j, Y', strtotime($value->create_datetime)) ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="bx-social">
                                        <?php include 'view/social/bx-social.php'; ?>
                                        <!--                                        <span style="color: #3B579D;"><i class="fa fa-facebook"></i> </span>-->
                                        <!--                                        <span style="color: #4AC7F9;"><i class="fa fa-twitter"></i></span>-->
                                        <!--                                        <span> <i class="fa fa-mail-forward"></i> </span>-->
                                    </div>
                                </div>
                                <div class="col-sm-9" id="large-videos-desc">
                                    <?php echo $value->desc ?>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } elseif (!empty($dataVideoMAllByNormal)) { ?>
                    <?php foreach ($dataVideoMAllByNormal as $key => $value) { ?>
                        <?php if ($key == 0) { ?>
                            <h1 id="large-videos-title"><?php echo $value->title ?></h1>
                            <div class="contents-row rows">
                                <div class="col-sm-3">
                                    <div class="bx-publish-info">
                                        <table>
                                            <tr>
                                                <td id="large-videos-time">
                                                    <?php echo date(' g:i A ', strtotime($value->create_datetime)) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="large-videos-date">
                                                    <?php echo date(' F j, Y', strtotime($value->create_datetime)) ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="bx-social">
                                        <?php include 'view/social/bx-social.php'; ?>
                                        <!--                                        <span style="color: #3B579D;"><i class="fa fa-facebook"></i> </span>-->
                                        <!--                                        <span style="color: #4AC7F9;"><i class="fa fa-twitter"></i></span>-->
                                        <!--                                        <span> <i class="fa fa-mail-forward"></i> </span>-->
                                    </div>
                                </div>
                                <div class="col-sm-9" id="large-videos-desc">
                                    <?php echo $value->desc ?>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid container-fluid-resp"
     style="background-color: #fff; padding-top: 1%!important; padding-bottom: 1%!important;">
    <div class="container">

        <div class="wrap-content-cols">
            <div class="col-md-12">
                <div class="tab-h-content sexy-football-topic" style="margin: 0 0 30px 0;">
                    <div class="pull-left"><span>Recent Videos</span></div>
                </div>
            </div>
            <div class="recent-videos-top" id="related-video-mark">
                <?php foreach ($dataVideoMRelated as $key => $value) { ?>
                    <?php if ($value->videosource == "twitter") { ?>
                        <a href="/hothit-videos?videoId=<?php echo $value->video_id ?>">
                            <div class="col-sm-3 col-md-3">
                                <div class="box-videos hidden-box">
                                    <div class="icon-play"><i class="fa fa-play"></i></div>
                                    <div class="crop" style="background-image: url(<?php echo $value->urlImg ?>);"></div>
                                </div>
                                <div class="title-owl-carousel"><?php echo $value->title ?></div>
                                <div class="times-content">
                                    <span class="how-long"><?php echo $value->create_datetime ?></span></div>
                            </div>
                        </a>
                    <?php } else { ?>
                        <a href="/highlight?videoId=<?= $value->video_id ?>">
                            <div class="col-sm-3 col-md-3">
                                <div class="box-videos hidden-box">
                                    <div class="icon-play"><i class="fa fa-play"></i></div>
                                    <div class="crop" style="background-image: url(<?php echo $value->urlImg ?>);"></div>

                                </div>
                                <div class="title-owl-carousel"><?php echo $value->title ?></div>
                                <div class="times-content">
                                    <span class="how-long"><?php echo $value->create_datetime ?></span></div>
                            </div>
                        </a>
                    <?php } ?>
                <?php } ?>
                <div style="clear: both;"></div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>


<div class="container-fluid container-fluid-resp">
    <div class="container">
        <div class="wrap-content-cols-3 higthlight-box-recent">
            <div class="wrap-most-recent text-center">

<!--                ไฮไลท์คู่บอล-->
                <div class="wrap-content-cols" id="videosHighlightTab">
                    <div class="wrap-box-news">
                        <div class="col-md-12">
                            <div class="tab-h-content sexy-football-topic">
                                <div class="pull-left"><span>ไฮไลท์คู่บอล</span></div>
                                <a href="/highlight" class="pull-right more-btn">ทั้งหมด</a>
                            </div>
                        </div>
                        <div class="wrap-box-content-news recent-higtlight-match">
                            <div class="box-other-news">
                                <?php foreach ($dataVideoMHighlight as $key => $value) {
                                    if ($key < 3) { ?>
                                        <?php if ($value->videosource == "twitter") { ?>
                                            <a href="/hothit-videos?videoId=<?php echo $value->video_id ?>">
                                                <div class="col-sm-4 col-md-4">
                                                    <div class="times-content margin-default">
                                                        <time class="how-long"><?php echo $value->create_datetime ?></time>
                                                    </div>
                                                    <div
                                                            class="title-owl-carousel title-h-videos"><?php echo $value->title ?></div>
                                                    <div class="box-videos hidden-box">
                                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                                        <div class="crop" style="background-image: url(<?php echo $value->urlImg ?>);"></div>
                                                    </div>
                                                    <div style="clear: both"></div>
                                                </div>
                                            </a>
                                        <?php } else { ?>
                                            <a href="/highlight?videoId=<?= $value->video_id ?>">
                                                <div class="col-sm-4 col-md-4">
                                                    <div class="times-content margin-default">
                                                        <time class="how-long"><?php echo $value->create_datetime ?></time>
                                                    </div>
                                                    <div class="title-owl-carousel title-h-videos">
                                                        <?php echo $value->title ?>
                                                    </div>
                                                    <div class="box-videos hidden-box">
                                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                                        <div class="crop" style="background-image: url(<?php echo $value->urlImg ?>);"></div>
                                                    </div>
                                                    <div style="clear: both"></div>
                                                </div>
                                            </a>
                                        <?php } ?>
                                    <?php }
                                } ?>

                                <div style="clear: both;"></div>
                            </div>
                            <div class="box-other-news box-other-news-small">
                                <?php foreach ($dataVideoMHighlight as $key => $value) {
                                    if ($key >= 3) { ?>
                                        <?php if ($value->videosource == "twitter") { ?>
                                            <a href="/hothit-videos?videoId=<?php echo $value->video_id ?>">
                                                <div class="col-sm-3 col-md-3">
                                                    <!--                                                <div class="tab-color"></div>-->
                                                    <div class="times-content margin-default">
                                                        <time class="how-long"><?php echo $value->create_datetime ?></time>
                                                    </div>
                                                    <div
                                                            class="title-owl-carousel title-h-videos"><?php echo $value->title ?></div>
                                                    <div class="box-videos hidden-box">
                                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                                        <div class="crop" style="background-image: url(<?php echo $value->urlImg ?>);"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        <?php } else { ?>
                                            <a href="/highlight?videoId=<?= $value->video_id ?>">
                                                <div class="col-sm-3 col-md-3">
                                                 <div class="times-content margin-default">
                                                        <time class="how-long"><?php echo $value->create_datetime ?></time>
                                                    </div>
                                                    <div class="title-owl-carousel title-h-videos">
                                                        <?php echo $value->title ?>
                                                    </div>
                                                    <div class="box-videos hidden-box">
                                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                                        <div class="crop" style="background-image: url(<?php echo $value->urlImg ?>);"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        <?php } ?>
                                    <?php }
                                } ?>

                            </div>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                </div>
                <div style="clear:both;"></div>

<!--                HotHit Video-->
                <div class="wrap-content-cols" id="videosOthersTab">
                    <div class="wrap-box-news">
                        <div class="col-md-12">
                            <div class="tab-h-content sexy-football-topic">
                                <div class="pull-left"><span>HotHit Video</span></div>
                                <a href="/highlight" class="pull-right more-btn">ทั้งหมด</a>
                            </div>
                        </div>
                        <div class="wrap-box-content-news  recent-hothit-videos">
                            <div class="box-other-news">
                                <?php foreach ($dataVideoMOthers as $key => $value) {
                                    if ($key < 3) { ?>
                                        <a href="/hothit-videos?videoId=<?php echo $value->video_id ?>">
                                            <div class="col-sm-4 col-md-4">
                                                <!--                                                <div class="tab-color"></div>-->
                                                <div class="times-content margin-default">
                                                    <time class="how-long"><?php echo $value->create_datetime ?></time>
                                                </div>
                                                <div class="title-owl-carousel"><?php echo $value->title ?></div>
                                                <div class="box-videos hidden-box">
                                                    <div class="icon-play"><i class="fa fa-play"></i></div>
                                                    <div class="crop" style="background-image: url(<?php echo $value->urlImg ?>);"></div>
                                                </div>
                                            </div>
                                        </a>
                                    <?php }
                                } ?>
                                <div style="clear: both"></div>
                            </div>
                            <div class="box-other-news box-other-news-small">
                                <?php foreach ($dataVideoMOthers as $key => $value) {
                                    if ($key >= 3) {
                                        ?>
                                        <a href="/hothit-videos?videoId=<?php echo $value->video_id ?>">
                                            <div class="col-sm-3 col-md-3">
                                                <!--                                                <div class="tab-color"></div>-->
                                                <div class="times-content margin-default">
                                                    <time class="how-long"><?php echo $value->create_datetime ?></time>
                                                </div>
                                                <div class="title-owl-carousel"><?php echo $value->title ?></div>
                                                <div class="box-videos hidden-box">
                                                    <div class="icon-play"><i class="fa fa-play"></i></div>
                                                    <div class="crop" style="background-image: url(<?php echo $value->urlImg ?>);"></div>
                                                </div>
                                            </div>
                                        </a>
                                    <?php }
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear:both;"></div>

            </div>
        </div>
    </div>
</div>
<?php //include 'footer.php';
include 'view/index/footer.php';
?>
<script>
    var countFull = 10;
    var countGoal = 10;
    $(document).ready(function () {
        // console.log($('#input_category').val()+"::"+$('#input_tag').val());
        // console.log("full:: "+$('#tab-hilight-full').find('.box-list-videos').find('tr').length);
        if($('input#input_day').val()==="") {
            videosFull($('#input_category').val(), $('#input_tag').val(), 10, $('#tab-hilight-full').find('.box-list-videos').find('tr').length, "normal");
            videosGoal($('#input_category').val(), $('#input_tag').val(), 10, $('#tab-hilight-goal').find('.box-list-videos').find('tr').length, "goal");
        }
    });

    function videosFull(category, tag, limit, offset, style) {
        $.ajax({
            url: "/videolist",
            data: {category: category, tag: tag, limit: limit, offset: offset, style: style},
            method: "GET",
            dataType: "html"
        }).done(function (response) {
            $('#tab-hilight-full').find('.box-list-videos').find('tbody').append(response);
            if (countFull >= 0) {
                countFull--;
                videosFull($('#input_category').val(), $('#input_tag').val(), 10, $('#tab-hilight-full').find('.box-list-videos').find('tr').length, "normal");
            }

            $('span#total-video-Normal').html($('#tab-hilight-full').find('.box-list-videos').find('tr').length);

            // console.log(response);
            // $("div#tab-hilight-goal").html(response);
        });
    }

    function videosGoal(category, tag, limit, offset, style) {
        $.ajax({
            url: "/videolist",
            data: {category: category, tag: tag, limit: limit, offset: offset, style: style},
            method: "GET",
            dataType: "html"
        }).done(function (response) {
            $('#tab-hilight-goal').find('.box-list-videos').find('tbody').append(response);
            if (countGoal >= 0) {
                countGoal--;
                videosGoal($('#input_category').val(), $('#input_tag').val(), 10, $('#tab-hilight-goal').find('.box-list-videos').find('tr').length, "goal")
            }
            $('span#total-video-Goal').html($('#tab-hilight-goal').find('.box-list-videos').find('tr').length)
            // console.log(response);
            // $("div#tab-hilight-goal").html(response);
        });
    }

    $("div#div-content").scroll(function () {
        if ($(this).scrollTop() == ($('#div-tab-content').height() - $('#div-content').height())) {
            $('#tab-hilight-goal').find('tr.tr-' + $('#tab-hilight-goal').find('tr.hide').first().attr('data-number')).removeClass('hide');
            $('#tab-hilight-full').find('tr.tr-' + $('#tab-hilight-full').find('tr.hide').first().attr('data-number')).removeClass('hide');
//            console.log($('#tab-hilight-goal').find('tr.hide').first().attr('data-number'));
//            console.log($('#tab-hilight-full').find('tr.hide').first().attr('data-number'));
        }
//        console.log($(this).scrollTop()+"::"+($('#div-tab-content').height()-$('#div-content').height()));
    });

    //    $('#div-tab-content').scroll(function() {
    //        console.log($(this).scrollTop());
    //        if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
    //            alert("near bottom!");
    //        }
    //    });

    $(document).on('change', "#select-filter-list-video", function (e) {
        var category = $(this).val();
        window.location.href = "/highlight?category=" + category;
//        videosGoalByLeague(category);
//        videosFullByLeague(category);
    })

    var statusClose = false;

    loadsly();
    refreshTime();
    $(window).resize(function () {
        loadsly();
        miniBoxVideo();
    });

    $(window).scroll(function () {
        miniBoxVideo();
    });

    function findTeam(team) {
        var patt = new RegExp(team);

        if (team == "all") {
            $('.tr-goal').each(function (index) {
                $(this).removeClass('hide');
            });
            $('.tr-normal').each(function (index) {
                $(this).removeClass('hide');
            });
        } else {
            $('.tr-goal').each(function (index) {
                var tag = $(this).attr('tag');
                if (patt.test(tag) == false) {
                    $(this).addClass('hide');
                } else {
                    $(this).removeClass('hide');
                }
            });
            $('.tr-normal').each(function (index) {
                var tag = $(this).attr('tag');
                if (patt.test(tag) == false) {
                    $(this).addClass('hide');
                } else {
                    $(this).removeClass('hide');
                }
            });
        }
//
//        var str = "Chelsea,Tottenham Hotspur,Premier League,พรีเมียร์ลีก,เชลซี,สเปอร์";
//
//        var res = patt.test(str);
//        console.log(league);
    }

    function videosGoalByLeague(category) {
        $.ajax({
            url: "/play/videosGoalByLeague",
            data: {category: category},
            method: "GET",
            dataType: "html"
        }).done(function (response) {
            $("div#tab-hilight-goal").html(response);
        });
    }

    function videosFullByLeague(category) {

        $.ajax({
            url: "/play/videosFullByLeague",
            data: {category: category},
            method: "GET",
            dataType: "html"
        }).done(function (response) {
            $("div#tab-hilight-full").html(response);
        })
    }

    function setIndexVideosAutoPlay(link, tag, title, desc, date, id) {
        window.location.href = "/highlight?videoId=" + id;
//        console.log(id);
//        $('#large-videos-title').html(title);
//        $('#large-videos-desc').html("");
//        $('#large-videos-desc').html(desc);
//        $('#large-videos-time').html(moment(date, 'YYYY-MM-DD mm:ss').format(' mm:ss a '));
//        $('#large-videos-date').html(moment(date, 'YYYY-MM-DD mm:ss').format(' MMMM DD, Y'));
//        console.log($('#large-videos-autoPlay').parent('video').length);
//        if($('video').length>=1){
//            $("#box-large-videos-autoPlay").find('video').find('source').attr("src", link);
//            $("#box-large-videos-autoPlay").find('video').attr("autoplay", "");
//            $("#box-large-videos-autoPlay video")[0].load();
//        }else {
//            $('#large-videos-autoPlay').attr("src", link);
//        }

//        $('#large-videos-autoPlay').attr("src", link);
    }

    function closeIframe() {
        $('#large-videos-autoPlay').removeAttr('style');
        $('#large-videos-autoPlay').css('position', "absolute");
        $('#large-videos-autoPlay').css('left', 0);
        $('#btn_Close_Iframe').hide();
        statusClose = true;
    }

    function miniBoxVideo() {
        var windowHeight = jQuery(window).height();
        var windowWidth = jQuery(window).width();
        var scrollTop = $(window).scrollTop();
        var mainBox = $('#box-large-videos-autoPlay').offset().top
        var container = $('.container').width();
        var body = $('body').width();
        if (scrollTop > 500 && !statusClose) {
            $('#large-videos-autoPlay').removeAttr('style');
            $('#large-videos-autoPlay').css('position', "absolute");
//            $('#large-videos-autoPlay').css('left', 0);
            $('#large-videos-autoPlay').css('width', 300);
            $('#large-videos-autoPlay').css('height', 170);
            $('#large-videos-autoPlay').css('top', ((scrollTop - (mainBox)) + (windowHeight - 200)));
            $('#large-videos-autoPlay').css('z-index', 100);
            $('#large-videos-autoPlay').css('right', 0);
            $('#btn_Close_Iframe').css('z-index', 100);
            $('#btn_Close_Iframe').css('top', ((scrollTop - (mainBox)) + (windowHeight - 230)));
            $('#btn_Close_Iframe').css('left', 0);
            $('#btn_Close_Iframe').show();
//                $('#large-videos-autoPlay').css('height', 200);
        } else {
            console.log($('#box-large-videos-autoPlay').find('video').length);
            if ($('#box-large-videos-autoPlay').find('video').length == 1) {
                $('#large-videos-autoPlay').removeAttr('style');
                $('#large-videos-autoPlay').css('width', '100%');
            } else {
                $('#large-videos-autoPlay').removeAttr('style');
                $('#large-videos-autoPlay').css('position', "absolute");
                $('#large-videos-autoPlay').css('height', '100%');
                $('#large-videos-autoPlay').css('left', '0');
                $('#large-videos-autoPlay').css('top', '0');
            }

//            $('#large-videos-autoPlay').css('position', "absolute");
//            $('#large-videos-autoPlay').css('left', 0);
            $('#btn_Close_Iframe').hide();
        }
//            console.log((scrollTop));
    }
    function loadsly() {
        var $frame = $('#boxVideo');
        var $slidee = $frame.children('#basic-videos').eq(0);
        var $wrap = $frame.parent();
        $slidee.sly(false);
        $slidee.sly({
            horizontal: 1,
            itemNav: 'basic',
            smart: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 0,
            scrollBy: 1,
            speed: 1000,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,

            // Buttons
            forward: $wrap.find('.btnforward'),
            backward: $wrap.find('.btnbackward'),
            prev: $wrap.find('.btnbackward'),
            next: $wrap.find('.btnforward'),
            prevPage: $wrap.find('.backward'),
            nextPage: $wrap.find('.forward')
        });
    }
    function setVideosAutoPlay(status, link, tag, title, desc, date, id) {
        $('#large-videos-title').html(title);
        $('#large-videos-desc').html("");
        $('#large-videos-desc').html(desc);
        $('#large-videos-time').html(moment(date, 'YYYY-MM-DD mm:ss').format(' mm:ss a '));
        $('#large-videos-date').html(moment(date, 'YYYY-MM-DD mm:ss').format(' MMMM DD, Y'));
        if ($('.active-now-playing').length) {
            $('.active-now-playing').addClass('hilight-cover-videos');
            $('.active-now-playing').html('');
            $('.active-now-playing').removeClass('active-now-playing');
        }
        if ($('#highlight_cover_videos_' + id).attr('class') == "hilight-cover-videos") {
            $('#highlight_cover_videos_' + id).addClass('active-now-playing');
            $('#highlight_cover_videos_' + id).html('now playing');
            $('#highlight_cover_videos_' + id).removeClass('hilight-cover-videos');
        }
        console.log($('#large-videos-autoPlay').parent('video').length);
        if ($('video').length >= 1) {
            $("#box-large-videos-autoPlay").find('video').find('source').attr("src", link);
            $("#box-large-videos-autoPlay").find('video').attr("autoplay", "");
            $("#box-large-videos-autoPlay video")[0].load();
        } else {
            $('#large-videos-autoPlay').attr("src", link);
        }

        if (status) {
            getSlideVideosHead(tag, 20, "highlight", id);
        }
        getRelatedVideo(id, tag, 6);
        videosGoalByLeague(tag);
        videosFullByLeague(tag);
        statusClose = false
    }
    function refreshTime() {
//            console.log('refresh');
        $.each($(".how-long"), function (k, v) {
//                console.log(k,v);
//                var d = new Date(parseInt($(this).text()) * 1000);
//                $(this).text(jQuery.timeago(d));
            if ($.isNumeric($(this).text())) {
                $(this).html(moment($(this).text(), "X").fromNow())
            } else {
                $(this).html(moment($(this).text(), "YYYY-MM-DD HH:mm:ss").fromNow())
            }
        })

    }
    function getOthersVideo() {
        $.ajax({
            url: "/services/getvideotype",
            data: {videotype: 'general', limit: 20},
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
//                console.log(response);
            $('#SlideVideosHead').html("");
            $.each(response, function (index, value) {
//                    console.log(value);
                var textDiv = "";
                textDiv += "<li id='li_video_" + value[0] + "' style='width: 200px;'>";
                textDiv += "<a href='#box-large-videos-autoPlay'";
                textDiv += " link='" + value[3] + "'";
                textDiv += " onclick=''";
                textDiv += "<div class='bx-top-videos' style=''>";
                textDiv += "<div class='hilight-cover-videos'></div>";
                textDiv += "<table>";
                textDiv += "<tr>";
                textDiv += "<td>";
                textDiv += "<div class='img-top-videos'>";
                textDiv += "<img src='" + value[8] + "'>";
                textDiv += "</div>";
                textDiv += "</td>";
                textDiv += "</tr>";
                textDiv += "<tr>";
                textDiv += "<td>";
                textDiv += "<div class='title-videos'>" + value[2] + "</div>";
                textDiv += "</td>";
                textDiv += "</tr>";
                textDiv += "</table>";
                textDiv += "</div>";
                textDiv += "</a>";
                textDiv += "</li>";
                $('#SlideVideosHead').append(textDiv);
                $('li#li_video_' + value[0]).attr('onclick', "setVideosAutoPlay(true,'" + value[7] + "','" + value[6] + "','" + value[2] + "','" + value[7] + "','" + value[3] + "','" + value[0] + "')");
                $('#videosHighlightTab').hide();
            })
        });
    }
    function getHighlightVideo() {
        $.ajax({
            url: "/services/getvideotype",
            data: {videotype: 'highlight', limit: 20},
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
//                console.log(response);
            $('#SlideVideosHead').html("");
            $.each(response, function (index, value) {
//                    console.log(value);
                var textDiv = "";
                textDiv += "<li id='li_video_" + value[0] + "' style='width: 200px;'>";
                textDiv += "<a href='#box-large-videos-autoPlay'";
                textDiv += " link='" + value[3] + "'";
                textDiv += " onclick=''>";
                textDiv += "<div class='bx-top-videos' style=''>";
                textDiv += "<div class='hilight-cover-videos'></div>";
                textDiv += "<table>";
                textDiv += "<tr>";
                textDiv += "<td>";
                textDiv += "<div class='img-top-videos'>";
                textDiv += "<img src='" + value[8] + "'>";
                textDiv += "</div>";
                textDiv += "</td>";
                textDiv += "</tr>";
                textDiv += "<tr>";
                textDiv += "<td>";
                textDiv += "<div class='title-videos'>" + value[2] + "</div>";
                textDiv += "</td>";
                textDiv += "</tr>";
                textDiv += "</table>";
                textDiv += "</div>";
                textDiv += "</a>";
                textDiv += "</li>";
                console.log(textDiv);
                $('#SlideVideosHead').append(textDiv);
                $('li#li_video_' + value[0]).attr('onclick', "setVideosAutoPlay(true,'" + value[7] + "','" + value[9] + "','" + value[2] + "','" + value[3] + "','" + value[0] + "')");
                $('#videosOthersTab').hide();
            })
        });
    }
    function getSlideVideosHead(category, limit, type, id) {
        $.ajax({
            url: "/services/getvideoCategory",
            data: {category: category, limit: limit, type: type, id: id},
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
//                response=jQuery.parseJSON(response);
//                console.log(response);
            $('#SlideVideosHead').html("");
            $.each(response, function (index, value) {
//                console.log(index);
                var textDiv = "";
                textDiv += "<li style='width: 200px;' id='slide_videos_" + value['video_id'] + "'>";
                textDiv += "<a href='#box-large-videos-autoPlay' id='NewSlideVideosHead_" + value['video_id'] + "' link='" + value['videosource'] + "' onclick=''>";
                textDiv += "<div class='bx-top-videos' style=''>";
                textDiv += "<div class='hilight-cover-videos' id='highlight_cover_videos_" + value['video_id'] + "'></div>";
                textDiv += "<table>";
                textDiv += "<tr>";
                textDiv += "<td>";
                textDiv += "<div class='img-top-videos'>";
                textDiv += "<img src='" + value['urlImg'] + "'>";
                textDiv += "</div>";
                textDiv += "</td>";
                textDiv += "</tr>";
                textDiv += "<tr>";
                textDiv += "<td>";
                textDiv += "<div class='title-videos'>" + value['title'] + "</div>";
                textDiv += "<div id='desc-videos-" + value['video_id'] + "' style='display: none;'>" + value['desc'] + "</div>";
                textDiv += "<div class='times-content'><i class='fa fa-clock-o'></i> <span class='how-long'>" + value['create_datetime'] + "</span></div>";
                textDiv += "</td>";
                textDiv += "</tr>";
                textDiv += "</table>";
                textDiv += "</div>";
                textDiv += "</a>";
                textDiv += "</li>";
                $('#SlideVideosHead').append(textDiv);
                $('#NewSlideVideosHead_' + value['video_id']).attr('onclick', "setVideosAutoPlay(false,'" + value['urlIframe'] + "','" + value['video_tag'] + "','" + value['title'] + "','" + value['desc'] + "','" + value['create_datetime'] + "','" + value['video_id'] + "')");
            });
            loadsly();
            refreshTime();
//                $('#basic-videos').sly(false);
//                $('#basic-videos').sly(options);
        })
    }
    function getRelatedVideo(id, tag, limit) {
//            console.log(tag,limit);
        $.ajax({
            url: "/services/getrelatedvideos",
            data: {id: id, tag: tag, limit: limit, type: 'highlight'},
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
//                console.log(response);
            $('#related-video-mark').html("");
            $.each(response, function (index, value) {
//                    console.log(value);
                var textDiv = "";
                textDiv += "<a id='a_relatedvideos_" + value[0] + "' href='#box-large-videos-autoPlay'";
                textDiv += "onclick=''>";
                textDiv += "<div class='col-sm-2'>";
                textDiv += "<div class='box-videos box-related'>";
                textDiv += "<div class='icon-play'><i class='fa fa-play'></i></div>";
                textDiv += "<img src='" + value[9] + "'>";
                textDiv += "</div>";
                textDiv += "<div class='title-owl-carousel'>" + value[2] + "</div>";
                textDiv += "</div>";
                textDiv += "</a>";
                $('#related-video-mark').append(textDiv);
                $('#a_relatedvideos_' + value[0]).attr('onclick', "setVideosAutoPlay(true,'" + value[8] + "','" + value[10] + "','" + value[2] + "','" + value[7] + "','" + value[3] + "','" + value[0] + "')")

            })
            var ablock = $("#related-video-mark").find("a");
        });
    }
</script>
<!--<script>-->
<!--    $('#myAffix').affix({-->
<!--        offset: {-->
<!--            top: 100,-->
<!--            bottom: function () {-->
<!--                return (this.bottom = $('.footer').outerHeight(true))-->
<!--            }-->
<!--        }-->
<!--    })-->
<!--</script>-->

</body>
</html>
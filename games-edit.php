<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/livescore.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="js/jquery/dist/jquery.min.js"></script>
    <script src="js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="nanoscrollbar/nanoscrollbar.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/timeview.js"></script>
    <script src="js/game.js"></script>
    <script src="js/sly/sly.js"></script>
</head>

<body>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>


<div class="content-livescore-main">
    <div class="container">
        <div class="row">
            <div class="content-list-livescore">
                <div class="col-md-12">
                    <div class="col-md-3 active">
                        <div class="thumbnail content-team-slide">
                            <div class="pull-left">
                                <table>
                                    <tr>
                                        <td><img src="/images/logo-team/Chelsea.png"></td>
                                        <td>Chelsea</td>
                                    </tr>
                                    <tr>
                                        <td><img src="/images/logo-team/manchester.png"></td>
                                        <td class="active">manchester</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="pull-right">
                                <div class="time-live">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p>21:00</p>
                                </div>
                                <div class="HDP-live">HDP</div>
                                <div class="num-live">-025</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="thumbnail content-team-slide">
                            <div class="pull-left">
                                <table>
                                    <tr>
                                        <td><img src="/images/logo-team/Chelsea.png"></td>
                                        <td>Chelsea</td>
                                    </tr>
                                    <tr>
                                        <td><img src="/images/logo-team/manchester.png"></td>
                                        <td class="active">manchester</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="pull-right">
                                <div class="time-live">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p>21:00</p>
                                </div>
                                <div class="HDP-live">HDP</div>
                                <div class="num-live">-025</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="thumbnail content-team-slide">
                            <div class="pull-left">
                                <table>
                                    <tr>
                                        <td><img src="/images/logo-team/Chelsea.png"></td>
                                        <td>Chelsea</td>
                                    </tr>
                                    <tr>
                                        <td><img src="/images/logo-team/manchester.png"></td>
                                        <td class="active">manchester</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="pull-right">
                                <div class="time-live">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p>21:00</p>
                                </div>
                                <div class="HDP-live">HDP</div>
                                <div class="num-live">-025</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="thumbnail content-team-slide">
                            <div class="pull-left">
                                <table>
                                    <tr>
                                        <td><img src="/images/logo-team/Chelsea.png"></td>
                                        <td>Chelsea</td>
                                    </tr>
                                    <tr>
                                        <td><img src="/images/logo-team/manchester.png"></td>
                                        <td class="active">manchester</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="pull-right">
                                <div class="time-live">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p>21:00</p>
                                </div>
                                <div class="HDP-live">HDP</div>
                                <div class="num-live">-025</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="topic-league">
                <h3><img src="/images/logo-team/fa-cup-2017.png"> ENGLAND FA CUP</h3>
                <ul>
                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> 20:00</li>
                    <li>|</li>
                    <li><i class="fa fa-calendar" aria-hidden="true"></i> 21/07/2018</li>
                </ul>
            </div>
            <div class="content-select-team-vote">

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="pull-right">
                                    <div class="name-team-vote pull-left text-right">
                                        <h3>Chelsea</h3>
                                    </div>
                                    <div class="box-vote-team">
                                        <div class="logo-vote-team-home">
                                            <img src="/images/logo-team/Chelsea.png">
                                        </div>
                                        <div class="btn-vote-team-home">
                                            VOTE
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="pull-left">
                                    <div class="box-vote-team">
                                        <div class="logo-vote-team-away">
                                            <img src="/images/logo-team/manchester.png">
                                        </div>
                                        <div class="btn-vote-team-away">
                                            VOTE
                                        </div>
                                    </div>
                                    <div class="name-team-vote pull-left">
                                        <h3>Manchester UTD</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row content-HDP-vote">
            <ul>
                <li>1.99</li>
                <li class="text-blue">[0.5]</li>
                <li>1.88</li>
            </ul>
        </div>

    </div>
<!--    stats-->
    <div class="container content-stats-game">
        <table>
            <tr>
                <td>
                    <ul class="text-right">
                        <li><img src="/images/icon-stat/betlist_result/lose.png"></li>
                        <li><img src="/images/icon-stat/betlist_result/lose_half.png"></li>
                        <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                        <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                        <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                    </ul>
                </td>
                <td class="text-center">
                    <h3 class="label label-default">Status</h3>
                </td>
                <td>
                    <ul class="text-left">
                        <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                        <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                        <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                        <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                        <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                    </ul>
                </td>
            </tr>
        </table>
    </div>


</div>

<!--mobile-->

<div class="content-livescore-main-mobile content-livescore-main">
    <div class="container">
        <div class="row">
            <div class="content-list-livescore content-list-livescore-mobile">
                <div class="col-md-12">
                    <ul>
                        <li class="active">
                            <div class="thumbnail content-team-slide">
                                <div class="pull-left">
                                    <table>
                                        <tr>
                                            <td>Chelsea</td>
                                        </tr>
                                        <tr>
                                            <td class="active">manchester</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="pull-right">
                                    <div class="time-live">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <p>21:00</p>
                                    </div>
                                    <div class="HDP-live">HDP</div>
                                    <div class="num-live">-025</div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="thumbnail content-team-slide">
                                <div class="pull-left">
                                    <table>
                                        <tr>
                                            <td>Chelsea</td>
                                        </tr>
                                        <tr>
                                            <td class="active">manchester</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="pull-right">
                                    <div class="time-live">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <p>21:00</p>
                                    </div>
                                    <div class="HDP-live">HDP</div>
                                    <div class="num-live">-025</div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="topic-league">
                    <span class="text-center">
                        <img src="/images/logo-team/fa-cup-2017.png">
                    </span>
                <h3>ENGLAND FA CUP</h3>
                <ul>
                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> 20:00</li>
                    <li>|</li>
                    <li><i class="fa fa-calendar" aria-hidden="true"></i> 21/07/2018</li>
                </ul>
            </div>
            <div class="content-select-team-vote content-select-team-vote-mobile">
                <div class="row">
                    <table>
                        <tr>
                            <td>
                                <ul class="text-center">
                                    <li><img src="/images/logo-team/Chelsea.png"></li>
                                    <li><strong>Chelsea FC</strong></li>
                                    <li>
                                        <button type="button" class="btn btn-primary btn-lg">Vote</button>
                                    </li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-center">
                                    <li><img src="/images/logo-team/manchester.png"></li>
                                    <li><strong>Manchester UTD</strong></li>
                                    <li>
                                        <button type="button" class="btn btn-primary btn-lg">Vote</button>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row content-HDP-vote">
            <ul>
                <li>1.99</li>
                <li class="text-blue">[0.5]</li>
                <li>1.88</li>
            </ul>
        </div>
        <div class="content-stats-game">
            <table>
                <tr>
                    <td>
                        <ul class="text-right">
                            <li><img src="/images/icon-stat/betlist_result/lose.png"></li>
                            <li><img src="/images/icon-stat/betlist_result/lose_half.png"></li>
                            <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                            <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                        </ul>
                    </td>
                    <td class="text-center">
                        <h3 class="label label-default">Status</h3>
                    </td>
                    <td>
                        <ul class="text-left">
                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                            <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                            <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                            <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>

</div>


<div class="content-detail-game">
    <div class="container">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#box-view" aria-controls="box-view" role="tab"
                                                      data-toggle="tab">ทรรศนะ</a>
            </li>
            <li role="presentation"><a href="#box-comment" aria-controls="box-comment" role="tab" data-toggle="tab">คอมเม้น</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="box-view">
                <div class="row">
                    <div class="col-md-6 content-view-game-border-right">
                        <div class="content-view-game">
                            <div class="col-sm-12">
                                <span><img src="/images/sample-logo01.png"></span>
                                <ul>
                                    <li><img src="/images/icon-stat/betlist_result/lose.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/lose_half.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                </ul>
                            </div>
                            <div class="col-sm-12 content-view-game-detail">
                                <div class="well well-sm">
                                    <strong>@เอซี มิลาน</strong>
                                    <span>ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว</span>
                                </div>
                            </div>
                        </div>
                        <div class="content-view-game">
                            <div class="col-sm-12">
                                <span><img src="/images/sample-logo02.png"></span>
                                <ul>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                </ul>
                            </div>
                            <div class="col-sm-12 content-view-game-detail">
                                <div class="well well-sm">
                                    <strong>@เอซี มิลาน</strong>
                                    <span>ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว</span>
                                </div>
                            </div>
                        </div>
                        <div class="content-view-game">
                            <div class="col-sm-12">
                                <span><img src="/images/sample-logo01.png"></span>
                                <ul>
                                    <li><img src="/images/icon-stat/betlist_result/lose.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/lose_half.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                </ul>
                            </div>
                            <div class="col-sm-12 content-view-game-detail">
                                <div class="well well-sm">
                                    <strong>@เอซี มิลาน</strong>
                                    <span>ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="content-view-game">
                            <div class="col-sm-12">
                                <span><img src="/images/sample-logo01.png"></span>
                                <ul>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                </ul>
                            </div>
                            <div class="col-sm-12 content-view-game-detail">
                                <div class="well well-sm">
                                    <strong>@เอซี มิลาน</strong>
                                    <span>ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว</span>
                                </div>
                            </div>
                        </div>
                        <div class="content-view-game">
                            <div class="col-sm-12">
                                <span><img src="/images/sample-logo01.png"></span>
                                <ul>
                                    <li><img src="/images/icon-stat/betlist_result/lose.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/lose_half.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                </ul>
                            </div>
                            <div class="col-sm-12 content-view-game-detail">
                                <div class="well well-sm">
                                    <strong>@เอซี มิลาน</strong>
                                    <span>ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว</span>
                                </div>
                            </div>
                        </div>
                        <div class="content-view-game">
                            <div class="col-sm-12">
                                <span><img src="/images/sample-logo01.png"></span>
                                <ul>
                                    <li><img src="/images/icon-stat/betlist_result/lose.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/lose_half.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                </ul>
                            </div>
                            <div class="col-sm-12 content-view-game-detail">
                                <div class="well well-sm">
                                    <strong>@เอซี มิลาน</strong>
                                    <span>ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว</span>
                                </div>
                            </div>
                        </div>
                        <div class="content-view-game">
                            <div class="col-sm-12">
                                <span><img src="/images/sample-logo01.png"></span>
                                <ul>
                                    <li><img src="/images/icon-stat/betlist_result/lose.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/lose_half.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/draw.png"></li>
                                    <li><img src="/images/icon-stat/betlist_result/win_half.png"></li>
                                </ul>
                            </div>
                            <div class="col-sm-12 content-view-game-detail">
                                <div class="well well-sm">
                                    <strong>@เอซี มิลาน</strong>
                                    <span>ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว ปืนใหญ่โดนประจำ แฟนบอลทำใจแล้ว</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="box-comment">...</div>
        </div>
    </div>
</div>
<div style="clear: both;"></div>
<?php //include 'footer.php';
include 'view/index/footer.php';
?>
</body>
</html>



<?php require_once 'model/VideoM.php';
$VideoMObj = new VideoM();
if (isset($_GET['videoId'])) {
    $dataVideo = array();
    $dataVideo = $VideoMObj->getVideosById($_GET['videoId']);
    $dataRelatedVideos = $VideoMObj->getRelatedVideoByType("highlight", $dataVideo[0]->video_tag, 4);
    $dataVideoMOthers = $VideoMObj->getSlideVideosMore($_GET['videoId'], "general", 4);
} else {
    header("Location: /");
}
?>
<div class="wrapper-news-articles news-articles-list" video_id="<?php echo $dataVideo[0]->video_id; ?>"
     style="padding-top: 100px;">
    <div class="containter-fluid">
        <div class="wrap-content-cols-2">
            <div class="bg-video-sexy">
                <div class="box-large-videos-row">
                    <div class="box-large-videos">
                        <div id="box-large-videos-autoPlay">
                            <?php if ($dataVideo[0]->videosource == "dailymotion") { ?>
                                <iframe class="iframe_<?php echo $dataVideo[0]->video_id; ?>"
                                        id="large-videos-autoPlay" width="100%" height="100%" data-autoplay="true"
                                        stop="<?php echo $dataVideo[0]->stopUrlIframe ?>"
                                        start="<?php echo $dataVideo[0]->urlIframe ?>"
                                        src="http://www.dailymotion.com/embed/video/<?php echo $dataVideo[0]->videokey ?>"
                                        frameborder="0"
                                        allowfullscreen></iframe>
                            <?php } elseif ($dataVideo[0]->videosource == "twitter") { ?>
                                <iframe class="iframe_<?php echo $dataVideo[0]->video_id; ?>"
                                        id="large-videos-autoPlay" width="100%" height="100%" data-autoplay="true"
                                        stop="<?php echo $dataVideo[0]->stopUrlIframe ?>"
                                        start="<?php echo $dataVideo[0]->urlIframe ?>"
                                        src="https://twitter.com/i/cards/tfw/v1/<?php echo $dataVideo[0]->videokey ?>"
                                        frameborder="0"
                                        allowfullscreen></iframe>
                            <?php } elseif ($dataVideo[0]->videosource == "facebook") { ?>
                                <iframe class="iframe_<?php echo $dataVideo[0]->video_id; ?>"
                                        id="large-videos-autoPlay" width="100%" height="100%" data-autoplay="true"
                                        stop="<?php echo $dataVideo[0]->stopUrlIframe ?>"
                                        start="<?php echo $dataVideo[0]->urlIframe ?>"
                                        src="https://www.facebook.com/v2.3/plugins/video.php?allowfullscreen=true&container_width=800&href=<?php echo $dataVideo[0]->content ?>"
                                        frameborder="0"
                                        allowfullscreen></iframe>
                            <?php } elseif ($dataVideo[0]->videosource == "streamable") { ?>
                                <?php if (!empty($dataVideo[0]->pathFile)) { ?>
                                    <video style="width: 100%;" class="video-player-tag" controls>
                                        <source id="large-videos-autoPlay" class="mp4-source"
                                                src="<?= $dataVideo[0]->pathFile ?>">
                                    </video>
                                <?php } else { ?>
                                    <iframe
                                            class="iframe_<?php echo $dataVideo[0]->video_id; ?>"
                                            stop="<?php echo $dataVideo[0]->stopUrlIframe ?>"
                                            start="<?php echo $dataVideo[0]->urlIframe ?>"
                                            id="large-videos-autoPlay" width="100%" height="700"
                                            src="<?php echo $dataVideo[0]->stopUrlIframe ?>" frameborder="0"
                                            allowfullscreen="">
                                    </iframe>
                                <?php } ?>
                            <?php } else { ?>
                                <iframe class="iframe_<?php echo $dataVideo[0]->video_id; ?>"
                                        id="large-videos-autoPlay" width="100%" height="100%" data-autoplay="true"
                                        stop="<?php echo $dataVideo[0]->stopUrlIframe ?>"
                                        start="<?php echo $dataVideo[0]->urlIframe ?>"
                                        src="http://www.youtube.com/embed/<?php echo $dataVideo[0]->videokey ?>"
                                        frameborder="0"
                                        allowfullscreen></iframe>
                            <?php } ?>
                            <!--                            <iframe id="large-videos-autoPlay" width="100%" height="700" src="-->
                            <?php //echo $dataVideo[0]->urlIframe ?><!--" frameborder="0" allowfullscreen="" onload="resizeIframe(this)"></iframe>-->
                        </div>
                    </div>
                </div>
                <div class="load_video">
                    <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="wrap-content-cols-2">
                <div class="container">
                    <div class="content-large-videos bx-other-video-head">
                        <div class="bx-video-type"><?php echo $dataVideo[0]->video_tag ?></div>

                        <div class="contents-row rows">
                            <div class="col-sm-9 bx-description-videos main_articles"
                                 newsid="<?php echo $dataVideo[0]->video_id ?>"
                                 id="main_articles_<?php echo $dataVideo[0]->video_id ?>">
                                <h1><?php echo $dataVideo[0]->title ?></h1>
                                <div class="times-content mg-bt15">
                                    <span><?php echo date(' g:i A', strtotime($dataVideo[0]->create_datetime)) ?></span>
                                    <span><?php echo date(' F j, Y', strtotime($dataVideo[0]->create_datetime)) ?></span>
                                </div>
                                <?php if (!empty($dataVideo[0]->desc)) {
                                    echo $dataVideo[0]->desc ?>
                                <?php } else { ?>
                                    &nbsp;
                                <?php } ?>


                                <!--                                <div class="bx-social">-->
                                <!--                                    --><?php //include 'view/social/bx-social.php'; ?>
                                <!--                                </div>-->
                            </div>
                            <div class="col-sm-3">
                                <img src="/images/kcap.jpg" style="width: 100%;">
                            </div>
                            <div class="col-sm-12 hide">
                                <div class="bx-publish-info">
                                    <table>
                                        <tr>
                                            <td><?php echo date(' g:i A', strtotime($dataVideo[0]->create_datetime)) ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo date(' F j, Y', strtotime($dataVideo[0]->create_datetime)) ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                    </div>

                    <div class="wrap-content-cols wrap-content-cols-other">
                        <div class="double-underline bx-tabs-related relate25">Related</div>
                        <div class="row">
                            <?php if (!empty($dataRelatedVideos)) { ?>
                                <div class="label-news-vertical">Video</div>
                                <?php foreach ($dataRelatedVideos as $key => $value) { ?>
                                    <div class="col-sm-3">
                                        <a href="/<?php echo(($value->videotype == 'highlight') ? 'highlight' : 'hothit-videos') ?>?videoId=<?php echo $value->video_id ?>">
                                            <div class="box-videos box-related box-related-sexy">
                                                <div class="bx-type-gallery">
                                                    <i class="fa fa-play"></i>
                                                </div>
                                                <div class="crop"
                                                     style="background-image: url(<?php echo $value->urlImg; ?>)"
                                                     data-pin-nopin="true"></div>
                                            </div>
                                            <div class="title-owl-carousel">
                                                <?php echo $value->title ?>
                                            </div>
                                            <div class="times-content">
                                                    <span class="timeMoreRecentedGallery"
                                                          date="<?php echo $value->create_datetime; ?>"></span></div>

                                        </a>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                        <div style="clear: both;"></div>
                    </div>

                    <div class="wrap-content-cols wrap-content-cols-other">
                        <div class="double-underline bx-tabs-related">&nbsp;</div>
                        <div class="row">
                            <?php if (!empty($dataVideoMOthers)) { ?>
                                <div class="label-news-vertical">Other</div>
                                <?php foreach ($dataVideoMOthers as $key => $value) { ?>
                                    <div class="col-sm-3">
                                        <a href="/<?php echo(($value->videotype == 'highlight') ? 'highlight' : 'hothit-videos') ?>?videoId=<?php echo $value->video_id ?>">
                                            <div class="box-videos box-related box-related-sexy">
                                                <div class="bx-type-gallery">
                                                    <i class="fa fa-play"></i>
                                                </div>
                                                <div class="crop"
                                                     style="background-image: url(<?php echo $value->urlImg; ?>)"
                                                     data-pin-nopin="true"></div>
                                            </div>
                                            <div class="title-owl-carousel"><?php echo $value->title ?></div>
                                            <div class="times-content">
                                                    <span class="timeMoreRecentedGallery"
                                                          date="<?php echo $value->create_datetime; ?>"></span></div>
                                        </a>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                        <div style="clear: both;"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

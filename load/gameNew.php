<?php
header('Access-Control-Allow-Origin: *');
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 3/5/2018
 * Time: 20:53
 */
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playReviewMatch.php";
$webroot=$_SERVER['DOCUMENT_ROOT'];
$type=((isset($_REQUEST['type']))?$_REQUEST['type']:null);
$matchId=((isset($_REQUEST['matchId']))?$_REQUEST['matchId']:null);
$method=((isset($_REQUEST['method']))?$_REQUEST['method']:null);
$playReviewMatch= new playReviewMatch();
$playMatch = new playMatch();
//$match = $playMatch->getFirstMathStatusBet(30);
if($type=="gameid"){
//    $playMatch = new playMatch();
//    $match = $playMatch->getFirstMathByTime();
//    foreach ($match as $key=>$value){
//        if($value->id==$matchId){
            if(!file_exists($webroot.'/generates/games-redesign-generates-id-'.$matchId.'.php')) {
                $str = implode("\n", file($webroot . '/games-redesign-generates-view-id.php'));
                $fp = fopen($webroot . '/generates/games-redesign-generates-id-'.$matchId.'.php', 'w');
                $str = str_replace("'%0942980330%'", $matchId, $str);
                fwrite($fp, $str, strlen($str));
                fclose($fp);
            }
            $strFileName=$webroot.'/match/'.$matchId.'.html';
            $objFopen = fopen($strFileName, 'w');
            $stringData = getRenderedHTML($webroot.'/generates/games-redesign-generates-id-'.$matchId.'.php');
            fwrite($objFopen, $stringData);
            fclose($objFopen);
//        }
//    }
}

if($type=="game"){
    $playMatch = new playMatch();
    $match = $playMatch->getFirstMathByTime();
    foreach ($match as $key=>$value){
//        if(!file_exists($webroot.'/match/'.$matchId.'.html')) {
            if ($value->id == $matchId) {
                if (!file_exists($webroot . '/generates/games-redesign-generates-' . $key . '.php')) {
                    $str = implode("\n", file($webroot . '/games-redesign-generates-view.php'));
                    $fp = fopen($webroot . '/generates/games-redesign-generates-' . $key . '.php', 'w');
                    $str = str_replace("'%0942980330%'", $key, $str);
                    fwrite($fp, $str, strlen($str));
                    fclose($fp);
                }

                $strFileName = $webroot . '/match/' . $matchId . '.html';
                $objFopen = fopen($strFileName, 'w');
                $stringData = getRenderedHTML($webroot . '/generates/games-redesign-generates-' . $key . '.php');
                fwrite($objFopen, $stringData);
                fclose($objFopen);
            }
//        }
    }
}

if($type=="news"){
    $strFileName = $webroot."/load/headerNews.html";
    $strFileHtml = $webroot."/view/index/headerNews.php";
    if(file_exists($strFileName) and file_exists($strFileHtml)) {
        $objFopen = fopen($strFileName, 'w');
        $stringData = getRenderedHTML($webroot . "/view/index/headerNews.php");
        fwrite($objFopen, $stringData);
        fclose($objFopen);
//        echo "news1";
    }
}

if($type=="news"){
    $strFileName = $webroot."/load/mostPopular.html";
    $strFileHtml = $webroot."/view/index/mostPopular.php";
    if(file_exists($strFileName) and file_exists($strFileHtml)) {
        $objFopen = fopen($strFileName, 'w');
        $stringData = getRenderedHTML($webroot . "/view/index/mostPopular.php");
        fwrite($objFopen, $stringData);
        fclose($objFopen);
//        echo "news2";
    }
}

if($type=="video"){
    $strFileName = $webroot."/load/videoMain.html";
    $strFileHtml = $webroot."/view/index/videoMain.php";
    if(file_exists($strFileName) and file_exists($strFileHtml)) {
        $objFopen = fopen($strFileName, 'w');
        $stringData = getRenderedHTML($webroot . "/view/index/videoMain.php");
        fwrite($objFopen, $stringData);
        fclose($objFopen);
//        echo "video1";
    }
}

if($type=="video"){
    $strFileName = $webroot."/load/main-menu-index.html";
    $strFileHtml = $webroot."/view/index/main-menu-index.php";
    if(file_exists($strFileName) and file_exists($strFileHtml)) {
        $objFopen = fopen($strFileName, 'w');
        $stringData = getRenderedHTML($webroot . "/view/index/main-menu-index.php");
        fwrite($objFopen, $stringData);
        fclose($objFopen);
//        echo "video2";
    }
}

if($type=="sexy"){
    $strFileName = $webroot."/load/sexyFootball.html";
    $strFileHtml = $webroot."/view/index/sexyFootball.php";
    if(file_exists($strFileName) and file_exists($strFileHtml)) {
        $objFopen = fopen($strFileName, 'w');
        $stringData = getRenderedHTML($webroot . "/view/index/sexyFootball.php");
        fwrite($objFopen, $stringData);
        fclose($objFopen);
//        echo "sexy";
    }
}

if($type=="foreignNews"){
    $strFileName = $webroot."/load/foreignNews.html";
    $strFileHtml = $webroot."/view/index/foreignNews.php";
    if(file_exists($strFileName) and file_exists($strFileHtml)) {
        $objFopen = fopen($strFileName, 'w');
        $stringData = getRenderedHTML($webroot . "/view/index/foreignNews.php");
        fwrite($objFopen, $stringData);
        fclose($objFopen);
//        echo "news3";
    }
}

if($type=="livescore"){
    $strFileName = $webroot."/load/livescore-main-index.html";
    $strFileHtml = $webroot."/view/index/livescore-main-index.php";
    if(file_exists($strFileName) and file_exists($strFileHtml)) {
        $objFopen = fopen($strFileName, 'w');
        $stringData = getRenderedHTML($webroot . "/view/index/livescore-main-index.php");
        fwrite($objFopen, $stringData);
        fclose($objFopen);
//        echo "livescore";
    }

    if (file_exists($_SERVER['DOCUMENT_ROOT'] .'/json/checkWinRateReview.json')) {
        $dataReturn=$playReviewMatch->topRanking();
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/json/checkWinRateReview.json', json_encode($dataReturn));
    }

}

if($objFopen)
{
    if(!empty($method)){
        $result['status']=true;
        echo json_encode($result);
    }else {
        if (isset($_SERVER["HTTP_REFERER"])) {
            header("Location: " . $_SERVER["HTTP_REFERER"] . '#Success' . $type);
        }
    }
}else{
    echo "File can not write";
}

function getRenderedHTML($path)
{
    ob_start();
    include($path);
    $var=ob_get_contents();
    ob_end_clean();
    return $var;
}


function send($packet, $url) {
    $ctx = stream_context_create(
        array(
            'http'=>array(
                'header'=>"Content-type: application/x-www-form-urlencoded",
                'method'=>'POST',
                'content'=>$packet
            )
        )
    );
    return file_get_contents($url, 0, $ctx);
}

?>
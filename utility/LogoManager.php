<?php

/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 24/1/2560
 * Time: 15:12 น.
 */
class LogoManager
{

    private $dir;


    function  LogoManager()
    {
        $this->dir = "images/teams_clean";
    }

    public function getLogo($tid, $size)
    {
        $sizelist = array("32x32", "64x64", "256x256");
        $df_name = "team_default";
        $fullpath = $this->dir . "/" . $df_name . "_" . $sizelist[$size] . ".png";
        $genpath = $this->dir . "/" . $tid . "_" . $sizelist[$size] . ".png";
        if (file_exists($genpath)) {
            $fullpath = $genpath;
        }
        return $fullpath;
    }

    public function getFbProfilePix($fbuid, $type = 'small')
    {
        if (!is_numeric($fbuid)) {
            $fbuid = 0;
        }
        return "https://graph.facebook.com/v2.8/" . $fbuid . "/picture?type=" . $type;
    }

    public function getBetlistResultPix($result)
    {
        $path = $_SERVER["DOCUMENT_ROOT"] . "/images/icon-stat/betlist_result/{$result}.png";
        if (!file_exists($path)) {
            $path = "";
        } else {
            $path = "/images/icon-stat/betlist_result/{$result}.png";
        }
        return $path;
    }

    public function getStepPix($step)
    {
        $path = $_SERVER["DOCUMENT_ROOT"] . "/images/step/step{$step}.png";
        if (!file_exists($path)) {
            $path = "/images/step/step.png";
        } else {
            $path = "/images/step/step{$step}.png";
        }
        return $path;
    }

    public function getCountiesPix($cid)
    {
        $path = $_SERVER['DOCUMENT_ROOT'] . "/images/countries/$cid.png";
        if (!file_exists($path)) {
            $path = "/images/countries/0.png";
        } else {
            $path = "/images/countries/$cid.png";
        }

        return $path;
    }
}
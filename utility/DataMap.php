<?php

/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 9/2/2560
 * Time: 16:45 น.
 */
class DataMap
{
    function mapByKey($keylist, $data)
    {
        $rs = [];
        foreach ($keylist as $k => $v) {
            $rs[$k] = $data[$v];
        }
        return $rs;
    }
}
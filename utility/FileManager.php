<?php

/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 24/1/2560
 * Time: 13:32 น.
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

class FileManager
{
    private $path;
    private $filename;
    private $fullpath;
    private $content;

    public function setPath($str)
    {
        if (!is_dir($_SERVER["DOCUMENT_ROOT"] . "/" . $str)) {
            mkdir($_SERVER["DOCUMENT_ROOT"] . "/" . $str, 0777, true);
        }
        $this->path = $str;
        $this->fullpath = $_SERVER["DOCUMENT_ROOT"] . "/" . $this->path . "/" . $this->filename;
    }

    public function  getPath()
    {
        return $this->path;
    }

    public function setFilename($name)
    {
        $this->filename = $name;
        $this->fullpath = $_SERVER["DOCUMENT_ROOT"] . "/" . $this->path . "/" . $this->filename;
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function getFullPath()
    {
        return $this->fullpath;
    }

    public function setContent($str)
    {
        $this->content = $str;
    }

    public function write()
    {
        file_put_contents($this->fullpath, $this->content);
    }


    public function read()
    {
        $this->content = file_get_contents($this->fullpath);
        return $this->content;
    }

    public function isFile()
    {
        return is_file($this->fullpath);
    }

    public function howOldInMinute()
    {
        $cdt = \Carbon\Carbon::now();
        $fdt = \Carbon\Carbon::createFromTimestamp(filemtime($this->fullpath));
        return $fdt->diffInMinutes($cdt);
    }

    public function  listFiles($order = 0)
    {
        $list = array_diff(scandir($this->fullpath, $order), array('.', '..'));
        return $list;
    }

}
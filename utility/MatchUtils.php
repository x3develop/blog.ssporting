<?php

/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 24/3/2560
 * Time: 15:09 น.
 */
class MatchUtils
{

    public function calTime($sid, $c0, $c1, $c2, $cx, $c3)
    {
        $minuteat = "";
        if ($sid == 2) {
            $ap = $this->calHatftime($cx);
            $ao = intval((($c3 - $c0) - $c1) / 60);
            if (ao < 1) {
                $minuteat = "1'";
            } else if ($ao > $ap) {
                $minuteat = $ap . '+';
            } else {
                $minuteat = $ao . "'";
            }
        } else if ($sid == 4) {
            $ap = $this->calHatftime(0);
            $ao = intval((($c3 - $c0) - $c2) / 60 + $ap);
            if ($ao <= $ap) {
                $minuteat = ($ap + 1) . "'";
            } else if ($ao > $ap) {
                $ap = $ap * 2;
                if ($ao > $ap) {
                    $minuteat = $ap . "'";
                } else {
                    $minuteat = $ao . "'";
                }
            }
        }
        return $minuteat;
    }

    public function calHatftime($cx)
    {
        $rs = $cx;
        if ($cx <= 0) {
            $rs = 45;
        }
        return $rs;
    }

}
<!doctype html>
<html lang="en" prefix="op: http://media.facebook.com/op#">
<head>
    <meta charset="utf-8">
    <meta property="op:markup_version" content="v1.0">

    <!-- The URL of the web version of your article -->
    <link rel="canonical" href="https://ngoal.com/Article/article.php">

    <!-- The style to be used for this article -->
    <meta property="fb:article_style" content="myarticlestyle">


    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="shortcut icon" type="images/x-icon" href="/images/favicon.ico.png">
    <title>บ้านผลบอล | เว็บทายผลบอลอันดับ 1 ของประเทศ ข่าวฟุตบอล ไฮไลท์การแข่งขัน</title>
    <meta name="keywords" content="News Football,Highlight Football,Video Football,Football">
    <meta name="description" content="บ้านผลบอล เว็บทายผลบอลอันดับ 1 ของประเทศ ข่าวฟุตบอล ไฮไลท์การแข่งขัน">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">

    <link rel="stylesheet" href="/css/style-new.css">
    <!--    <link rel="stylesheet" href="css/style.css">-->
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/article-facebook.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="/nanoscrollbar/nanoscrollbar.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <!--    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">-->

    <!--js-->

    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    <!--    <script src="js/flogin/src/flogin.js"></script>-->
    <script src="/js/moment.js"></script>
    <script src="/js/moment-with-locales.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="/js/sly/sly.js"></script>
    <script src="/js/jquery-timeago/jquery.timeago.js"></script>
    <script src="/nanoscrollbar/nanoscrollbar.js"></script>
</head>
<body>
<?php include '../view/index/menu-all.php'; ?>
<div class="container container-article-fb">
    <ol class="breadcrumb">
        <li><a href="/">หน้าแรก</a></li>
        <li>น้ำมันหมด!ลือต่างดาวทำช็อกเคลื่อนยานดูด'หมูน้อย'</li>
    </ol>
    <div class="col-md-9">
        <article class="content-article-fb">
            <header>
                <!-- The title and subtitle shown in your Instant Article -->
                <h1>น้ำมันหมด!ลือต่างดาวทำช็อกเคลื่อนยานดูด'หมูน้อย'</h1>
                <!--            <h2>Article Subtitle</h2>-->
                <ul class="box-time-article">
                    <li>
                        <!-- The date and time when your article was originally published -->
                        <time class="op-published" datetime="2014-11-11T04:44:16Z">November 11th, 4:44 PM</time>
                    </li>
                    <li>
                        <!-- The date and time when your article was last updated -->
                        <time class="op-modified" dateTime="2014-12-11T04:44:16Z">December 11th, 4:44 PM</time>
                    </li>
                </ul>

            </header>
            <p>ข่าวชวนช็อกจากประเทศสเปนรายงานว่า บาร์เซโลน่า ต้องการตัว อัลเบร์โต้ โมเรโน่ แบ็คซ้ายบ่อน้ำมันจาก
                ลิเวอร์พูล</p>
            <!-- Article body goes here -->
            <!-- The authors of your article -->
            <figure class="box-image">
                <img src="https://ngoal.com/images/news/2018-10-09/20181009225936_620x413.jpg"/>
                <!--                    <figcaption>This image is amazing</figcaption>-->
            </figure>
            <!-- Body text for your article -->

            <p>ดาวเตะวัย 26 ปีกำลังตกเป็นสำรอง แอนดี้ โรเบิร์ตสัน
                ชนิดไม่ได้ผุดได้เกิดโดยฤดูกาลนี้เพิ่งได้ลงเล่นนัดเดียวในศึกคาราบาว คัพแถมทีมตกรอบไปแล้ว</p>
            <p>อย่างไรก็ดีมุนโด้ เดปอร์ติโบระบุว่า "ต่างดาว" กำลังมองหาแบ็คซ้ายอีกรายไปเป็นกำลังเสริมให้ จอร์ดี้ อัลบา
                เจ้าของสัมปทานเบอร์หนึ่ง</p>
            <p>บาร์ซ่า วางเป้าหมายไว้ 4 รายและหนึ่งในนั้นคือ โมเรโน่ ซึ่งจะหมดสัญญา 30 มิถุนายน 2018
                ทำให้พวกเขามีสิทธิ์ได้ตัวไปในเดือนมกราคมนี้เลยด้วยราคากำลังดี</p>
            <p>คนต่อมาคือ โฆเซ่ กาย่า ตัวเก่งจาก บาเลนเซีย ซึ่งถึงขั้นต่อสู้แย่งตำแหน่งแบ็คซ้ายตัวจริงของ "กระทิงดุ" กับ
                อัลบา ได้แบบไม่เป็นรอง</p>
            <p>กระนั้นสำหรับ กาย่า ยากมากที่ "ไอ้ค้างคาว" จะปล่อยตัวทำให้ บาร์ซ่า
                น่าจะต้องไปดำเนินการช่วงซัมเมอร์ปีหน้าแทน</p>
            <p>อีกคนคือ ฆวน เบร์นาท ซึ่ง "อาซูลกราน่า" พิจารณาซื้อตั้งแต่ซัมเมอร์ปีนี้แต่ลงท้ายถอนตัวและเป็น ปารีส
                แซงต์-แชร์กแมง ถอยจาก บาเยิร์น มิวนิค ไปในราคา 14 ล้านยูโร</p>
            <p>
                สุดท้ายคือ แฟร์กล็องด์ เมนดี้ ฟูลแบ็คเฟรนช์แมนวัย 23 ปีของ โอลิมปิก ลียง
                แต่รายนี้มุนโด้ไม่ได้ให้ข้อมูลเกี่ยวกับความสนใจจาก บาร์ซ่า มากไปกว่านี้
            </p>


            <footer>
                <!-- Credits for your article -->
                <aside>ที่มา:<a href="http://www.soccersuck.com">soccersuck.com</a></aside>

                <!-- Copyright details for your article -->
<!--                <small>Legal notes</small>-->
            </footer>
        </article>
    </div>
    <div class="col-md-3">

    </div>
</div>
<?php //include 'footer.php';
include '../view/index/footer.php';
?>
</body>
</html>
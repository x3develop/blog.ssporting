<?php
include_once __DIR__ . "/../bootstrap.php";
use Carbon\Carbon;
use controller\HighlightController;
use controller\MatchController;

$hcon = new HighlightController();
$mcon = new MatchController();
$today = Carbon::now("UTC");
$today->setTimezone("Asia/Bangkok");
$hllist = $hcon->getHighlightAllLSort($today->toDateString());
// $hllist = $hcon->getHighlightAllLSort("2018-11-29");

// dd($hllist);

?>

<div class="wrap_program_soccer">
    <div class="container">
        <div class="text-title">
            <h4>วิเคราะห์บอล ทัศนะบอล วัน<span id="hl-datemark"></span> </h4>
            <div class="pull-right"><a class="more-link" href="/live-match.php">โปรแกรมทั้งหมด</a></div>
        </div>


        <div class="content-program-soccer">
            <div class="topic">
                <div class="logo-league-program">
                    <img src="/images/five-leagues/seriea1.png">
                </div>
                <div class="title-league-program">
                    <h3>กัลโช่ เซเรียอา อิตาลี (ITALY SERIE A)</h3>
                    <h5>บ้านผลบอล โปรแกรมบอล ราคาบอล </h5>
                </div>
                <div class="dropdown pull-right">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        คู่บอลทั้งหมด
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="#">คู่บอลทั้งหมด</a></li>
                        <li><a href="#">บอลสด</a></li>
                        <li><a href="#">ทีเด็ด-ทรรศนะ</a></li>
                        <li><a href="#">คู่เตะเสร็จแล้ว</a></li>
                    </ul>
                </div>

            </div>


            <div class="content">
                <div class="panel-group panel-group-row" id="accordion">

                    <!--                    ทีเด็ด ทรรศนะ-->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="hdp-float">
                                <ul>
                                    <li>-1.9</li>
                                    <li><h3>2.50</h3></li>
                                    <li>1.9</li>
                                </ul>
                            </div>
                            <div class="left-accordion-row pull-left">
                                <div class="pull-right logo-team-ponball">
                                    <img src="/images/logo-team/manchester.png">
                                </div>
                                <div class="pull-right detail-team-ponball">
                                    <div class="top-detail-team-ponball text-right">
                                        <h4>Manchester United</h4>
                                    </div>
                                    <div class="bottom-detail-team-ponball">
                                        <div class="score-ct">60%</div>
                                        <div class="progress full-width">
                                            <div class="progress-bar pull-right" role="progressbar" aria-valuenow="60"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <ul class="status-lastMatch full-width">
                                        <li><p>Last Match</p></li>
                                        <li>
                                            <div class="draw-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="lose-status"></div>
                                        </li>
                                    </ul>
                                    <ul class="status-lastMatch full-width odd">
                                        <li><p>ODD</p></li>
                                        <li>
                                            <div class="draw-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="lose-status"></div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="pull-left content-col-left">
                                    <div class="ct-time">
                                        18:00
                                    </div>
                                    <div class="ct-live-chanel">
                                        <img src="/images/channel/pptv.png">
                                    </div>
                                </div>
                            </div>
                            <div class="right-accordion-row pull-right">
                                <div class="pull-left logo-team-ponball">
                                    <img src="/images/logo-team/Chelsea.png">
                                </div>
                                <div class="pull-left detail-team-ponball">
                                    <div class="top-detail-team-ponball text-left">
                                        <h4>Chelsea</h4>
                                    </div>
                                    <div class="bottom-detail-team-ponball">
                                        <div class="progress full-width">
                                            <div class="progress-bar progress-bar-danger" role="progressbar"
                                                 aria-valuenow="20"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 20%;">
                                            </div>
                                        </div>
                                        <div class="score-ct">20%</div>
                                    </div>
                                </div>
                                <div class="pull-left">
                                    <ul class="status-lastMatch full-width">
                                        <li>
                                            <div class="draw-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="lose-status"></div>
                                        </li>
                                        <li><p>Last Match</p></li>
                                    </ul>
                                    <ul class="status-lastMatch full-width odd">
                                        <li>
                                            <div class="draw-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="lose-status"></div>
                                        </li>
                                        <li><p>ODD</p></li>
                                    </ul>
                                </div>
                                <div class="pull-right-section">
                                    <div class="intro-game">
                                        <a href="/game.php" data-toggle="tooltip" data-placement="top"
                                           title="เล่นเกมทายผล">
                                            <i class="fa fa-futbol-o" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <a data-toggle="collapse" class="review-dropdown collapsed" data-parent="#accordion"
                                       href="#collapse-tded">
                                        <h4>ทีเด็ด ทรรศนะ</h4>
                                    </a>
                                </div>

                            </div>
                            <div style="clear: both"></div>
                        </div>
                        <div id="collapse-tded" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="home-panel-body pull-left">
                                    <div class="content-home-review full-width pull-right">
                                        <div class="pull-right">
                                            <div class="area-logoName">
                                                <div class="pull-right logo-review-user">
                                                    <img src="/images/profess-player/sample-logo01.png">
                                                </div>
                                                <div class="pull-right">
                                                    <h4>LOMTOE.NET</h4>
                                                    <ul>
                                                        <li>@manchester</li>
                                                        <li><img src="/images/logo-team/manchester.png"</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-right text-review-content text-right">
                                            <p>
                                                อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                            </p>
                                        </div>
                                        <div class="pull-left ct-winrate">
                                            <div>
                                                <h3>28%</h3>
                                                <small>Win rate</small>
                                            </div>
                                            <div>
                                                <h3>30</h3>
                                                <small>Days</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content-home-review full-width pull-right">
                                        <div class="pull-right">
                                            <div class="area-logoName">
                                                <div class="pull-right logo-review-user">
                                                    <img src="/images/profess-player/sample-logo05.png">
                                                </div>
                                                <div class="pull-right">
                                                    <h4>SMM SPORT</h4>
                                                    <ul>
                                                        <li>@manchester</li>
                                                        <li><img src="/images/logo-team/manchester.png"</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-right text-review-content text-right">
                                            <p>
                                                อาจจะฝืดๆหน่อย​ แต่ต้องสอย​ อย่าให้เสียชื่อทีมใหญ่
                                            </p>
                                        </div>
                                        <div class="pull-left ct-winrate">
                                            <div>
                                                <h3>28%</h3>
                                                <small>Win rate</small>
                                            </div>
                                            <div>
                                                <h3>30</h3>
                                                <small>Days</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content-home-review full-width pull-right">
                                        <div class="pull-right">
                                            <div class="area-logoName">
                                                <div class="pull-right logo-review-user">
                                                    <img src="/images/9.jpg">
                                                </div>
                                                <div class="pull-right">
                                                    <h4>หมูตัวนี้ตัวใหญ่</h4>
                                                    <ul>
                                                        <li>@manchester</li>
                                                        <li><img src="/images/logo-team/manchester.png"></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-right text-review-content text-right">
                                            <p>
                                                ชอบความคมของตรานกแก้ว ซวยแล้วบายาโดลิด
                                            </p>
                                        </div>
                                        <div class="pull-left ct-winrate">
                                            <div>
                                                <h3>28%</h3>
                                                <small>Win rate</small>
                                            </div>
                                            <div>
                                                <h3>30</h3>
                                                <small>Days</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="away-panel-body pull-right">
                                    <div class="content-away-review full-width pull-left">
                                        <div class="pull-left">
                                            <div class="area-logoName area-logoName-away">
                                                <div class="pull-left logo-review-user">
                                                    <img src="/images/profess-player/sample-logo12.png">
                                                </div>
                                                <div class="pull-left">
                                                    <h4>GOAL.IN.TH</h4>
                                                    <ul>
                                                        <li><img src="/images/logo-team/Chelsea.png"</li>
                                                        <li>@Chelsea</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-left text-review-content text-left">
                                            <p>
                                                อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                            </p>
                                        </div>
                                        <div class="pull-right ct-winrate">
                                            <div>
                                                <h3>28%</h3>
                                                <small>Win rate</small>
                                            </div>
                                            <div>
                                                <h3>30</h3>
                                                <small>Days</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="full-width text-center">
                                    <a class="btn btn-default btn-sm" href="#" role="button">อ่านทีเด็ดทรรศนะทั้งหมด</a>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!--บอลสด-->
                    <div class="panel panel-default livescore-panel livescore-live">
                        <div class="panel-heading">
                            <div class="hdp-float live-float">
                                <ul>
                                    <li>2</li>
                                    <li>
                                        <div class="progress" data-percentage="40">
				                            <span class="progress-left">
                                                <span class="progress-bar"></span>
				                            </span>
                                            <span class="progress-right">
                                                <span class="progress-bar"></span>
                                            </span>
                                            <div class="progress-value">
                                                <div>45+</div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>1</li>
                                </ul>
                            </div>
                            <div class="left-accordion-row pull-left">
                                <div class="pull-right logo-team-ponball">
                                    <img src="/images/logo-team/manchester.png">
                                </div>
                                <div class="pull-right detail-team-ponball">
                                    <div class="top-detail-team-ponball text-right">
                                        <h4>Manchester United</h4>
                                    </div>
                                    <div class="bottom-detail-team-ponball">
                                        <div class="score-ct">60%</div>
                                        <div class="progress full-width">
                                            <div class="progress-bar pull-right" role="progressbar" aria-valuenow="60"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <ul class="status-lastMatch full-width">
                                        <li><p>Last Match</p></li>
                                        <li>
                                            <div class="draw-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="lose-status"></div>
                                        </li>
                                    </ul>
                                    <ul class="status-lastMatch full-width odd">
                                        <li><p>ODD</p></li>
                                        <li>
                                            <div class="draw-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="lose-status"></div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="pull-left content-col-left">
                                    <div class="ct-live">
                                        <span class="label-live">Live</span>
                                    </div>
                                    <div class="ct-live-chanel">
                                        <img src="/images/channel/pptv.png">
                                    </div>
                                </div>
                            </div>
                            <div class="right-accordion-row pull-right">
                                <div class="pull-left logo-team-ponball">
                                    <img src="/images/logo-team/Chelsea.png">
                                </div>
                                <div class="pull-left detail-team-ponball">
                                    <div class="top-detail-team-ponball text-left">
                                        <h4>Chelsea</h4>
                                    </div>
                                    <div class="bottom-detail-team-ponball">
                                        <div class="progress full-width">
                                            <div class="progress-bar progress-bar-danger" role="progressbar"
                                                 aria-valuenow="20"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 20%;">
                                            </div>
                                        </div>
                                        <div class="score-ct">20%</div>
                                    </div>
                                </div>
                                <div class="pull-left">
                                    <ul class="status-lastMatch full-width">
                                        <li>
                                            <div class="draw-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="lose-status"></div>
                                        </li>
                                        <li><p>Last Match</p></li>
                                    </ul>
                                    <ul class="status-lastMatch full-width odd">
                                        <li>
                                            <div class="draw-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="lose-status"></div>
                                        </li>
                                        <li><p>ODD</p></li>
                                    </ul>
                                </div>
                                <div class="pull-right-section">
                                    <div class="intro-game">
                                        <a href="/game.php" data-toggle="tooltip" data-placement="top"
                                           title="เล่นเกมทายผล">
                                            <i class="fa fa-futbol-o" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <a data-toggle="collapse" class="review-dropdown collapsed" data-parent="#accordion"
                                       href="#collapse_livematch">
                                        <h4>Live score</h4>
                                    </a>
                                </div>

                            </div>
                            <div style="clear: both"></div>
                        </div>
                        <div id="collapse_livematch" class="panel-collapse collapse">
                            <div class="panel-body">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs nav-tabs-highlight" role="tablist">
                                    <li class="active" role="presentation">
                                        <a href="#livescore-tab" role="tab"
                                           data-toggle="tab">Livescore</a></li>
                                    <li role="presentation">
                                        <a href="#review" role="tab" data-toggle="tab">ทีเด็ด-ทรรศนะ</a></li>
                                    <li>
                                        <a href="#"><i class="fa fa-television" aria-hidden="true"></i> ดูไฮไลท์ฟุตบอล</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="livescore-tab">
                                        <div class="col-md-4">

                                            <div class="thumbnail">

                                                <div class="header-event-match">
                                                    <ul>
                                                        <li><h3>EVENT MATCH</h3></li>
                                                        <li>
                                                            <span class="real-time-match">
                                                               45+
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="score-event-match">

                                                    <div class="pull-right">
                                                        <img src="/images/logo-team/manchester.png">
                                                        <p>Manchester United</p>

                                                    </div>
                                                    <h4>2-2</h4>
                                                    <div class="pull-left text-right">
                                                        <p>Chelsea</p>
                                                        <img src="/images/logo-team/Chelsea.png">
                                                    </div>

                                                </div>
                                                <div class="content-event-match">

                                                    <!--                                        Home ใบแดง-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-home-live">
                                                            <div class="box-football-icon">
                                                                <div class="red-card"></div>
                                                            </div>
                                                            <div class="min-live">90+4'</div>
                                                            <div class="name-live">Willian</div>
                                                        </div>
                                                    </div>
                                                    <!--                                        Home ใบเหลือง-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-home-live">
                                                            <div class="box-football-icon">
                                                                <div class="yellow-card"></div>
                                                            </div>
                                                            <div class="min-live">87'</div>
                                                            <div class="name-live">Cesc Fàbregas</div>
                                                        </div>
                                                    </div>
                                                    <!--                                        Home ทำประตู-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-home-live">
                                                            <div class="box-football-icon">
                                                                <div class="goal-score"></div>
                                                            </div>
                                                            <div class="min-live">71'</div>
                                                            <div class="score-live">2 - 4</div>
                                                            <div class="name-live">Cesc Fàbregas</div>
                                                        </div>
                                                    </div>
                                                    <!--                                        Home เปลี่ยนตัว-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-home-live">
                                                            <div class="box-football-icon">
                                                                <div class="substitute-player"></div>
                                                            </div>
                                                            <div class="min-live">69'</div>
                                                            <div class="name-live"><span
                                                                        class="substitute-player-out"></span>
                                                                David
                                                                Luiz
                                                            </div>
                                                            <div class="name-live"><span
                                                                        class="substitute-player-in"></span>
                                                                Victor
                                                                Moses
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--Home ใบเหลือง+ใบเหลือง=ใบแดง-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-home-live">
                                                            <div class="box-football-icon">
                                                                <div class="group-yellow-card"></div>
                                                            </div>
                                                            <div class="min-live">55'</div>
                                                            <div class="name-live">Ross Barkley</div>
                                                        </div>
                                                    </div>
                                                    <!--Home จุดโทษ-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-home-live">
                                                            <div class="box-football-icon">
                                                                <div class="penalty-score"></div>
                                                            </div>
                                                            <div class="min-live">45'</div>
                                                            <div class="name-live">Gary Cahill</div>
                                                        </div>
                                                    </div>
                                                    <!--Home พลาดจุดโทษ-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-home-live">
                                                            <div class="box-football-icon">
                                                                <div class="penalty-none-score"></div>
                                                            </div>
                                                            <div class="min-live">41'</div>
                                                            <div class="name-live">Álvaro Morata</div>
                                                        </div>
                                                    </div>
                                                    <!--Home ทำเข้าประตูตัวเอง-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-home-live">
                                                            <div class="box-football-icon">
                                                                <div class="own-goal"></div>
                                                            </div>
                                                            <div class="min-live">28'</div>
                                                            <div class="name-live">Álvaro Morata</div>
                                                        </div>
                                                    </div>


                                                    <!--                                        Away ใบแดง-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-away-live pull-right">
                                                            <div class="name-live">Willian</div>
                                                            <div class="min-live">90+4'</div>
                                                            <div class="box-football-icon">
                                                                <div class="red-card"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--                                        Away ใบเหลือง-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-away-live pull-right">
                                                            <div class="name-live">Cesc Fàbregas</div>
                                                            <div class="min-live">87'</div>
                                                            <div class="box-football-icon">
                                                                <div class="yellow-card"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--                                        Away ทำประตู-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-away-live pull-right">
                                                            <div class="name-live">Cesc Fàbregas</div>
                                                            <div class="score-live">2 - 4</div>
                                                            <div class="min-live">71'</div>
                                                            <div class="box-football-icon">
                                                                <div class="goal-score"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--                                        Away เปลี่ยนตัว-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-away-live pull-right">
                                                            <div class="name-live"><span
                                                                        class="substitute-player-in"></span>
                                                                Victor
                                                                Moses
                                                            </div>
                                                            <div class="name-live"><span
                                                                        class="substitute-player-out"></span>
                                                                David
                                                                Luiz
                                                            </div>
                                                            <div class="min-live">69'</div>
                                                            <div class="box-football-icon">
                                                                <div class="substitute-player"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--Away ใบเหลือง+ใบเหลือง=ใบแดง-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-away-live pull-right">
                                                            <div class="name-live">Ross Barkley</div>
                                                            <div class="min-live">55'</div>
                                                            <div class="box-football-icon">
                                                                <div class="group-yellow-card"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--Away จุดโทษ-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-away-live pull-right">
                                                            <div class="name-live">Gary Cahill</div>
                                                            <div class="min-live">45'</div>
                                                            <div class="box-football-icon">
                                                                <div class="penalty-score"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--Away พลาดจุดโทษ-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-away-live pull-right">
                                                            <div class="name-live">Álvaro Morata</div>
                                                            <div class="min-live">41'</div>
                                                            <div class="box-football-icon">
                                                                <div class="penalty-none-score"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--Away ทำเข้าประตูตัวเอง-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-away-live pull-right">
                                                            <div class="name-live">Álvaro Morata</div>
                                                            <div class="min-live">28'</div>
                                                            <div class="box-football-icon">
                                                                <div class="own-goal"></div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 content-status-match">
                                            <div class="thumbnail">

                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#statistics-tab"
                                                                                              role="tab"
                                                                                              data-toggle="tab">Statistics</a>
                                                    </li>
                                                    <li role="presentation"><a href="#lineUp-tab"
                                                                               role="tab"
                                                                               data-toggle="tab">Line
                                                            Up</a>
                                                    </li>

                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="statistics-tab">
                                                        <div class="table statistics-table">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="pull-right">
                                                                            <h4>Manchester United
                                                                            </h4>
                                                                            <img src="/images/logo-team/manchester.png">
                                                                        </div>
                                                                    </td>
                                                                    <td><h3>VS</h3></td>
                                                                    <td>
                                                                        <div class="pull-left">
                                                                            <img src="/images/logo-team/Chelsea.png">
                                                                            <h4>Chelsea</h4>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                ยิงประตู-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress active">15</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="15"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 15%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>ยิงประตู</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="9" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 9%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress">9</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                ยิงตรงกรอบ-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress active">5</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="15"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100" style="width: 5%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>ยิงตรงกรอบ</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="9" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 2%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress">2</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                ผิดกติกา-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress active">13</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="15"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 13%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>ผิดกติกา</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="8" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 8%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress">8</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                เตะมุม-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress">2</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="2"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100" style="width: 2%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>เตะมุม</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="3" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 3%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress active">3</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                ฟรีคิก-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress">12</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="12"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 12%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>ฟรีคิก</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="19" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 19%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress active">19</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                ล้ำหน้า-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress active">6</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="6"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100" style="width: 6%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>ล้ำหน้า</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="4" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 4%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress">4</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                จำนวนใบเหลือง-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress">3</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="3"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100" style="width: 3%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>ล้ำหน้า</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="3" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 3%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress">3</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                เวลาครองบอล-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress active">55%</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="55"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 55%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>เวลาครองบอล</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="45" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 45%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress">45%</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                เซฟได้-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress">1</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="1"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100" style="width: 1%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>เซฟได้</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="4" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 4%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress active">4</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                เปลี่ยนตัว-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress">3</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="3"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100" style="width: 3%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>เปลี่ยนตัว</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="3" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 3%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress">3</div>
                                                                    </td>
                                                                </tr>


                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="lineUp-tab">
                                                        <div class="lineUp-table">
                                                            <table class="table">
                                                                <tr>
                                                                    <td>
                                                                        <div class="pull-right full-width text-right">
                                                                            <span>Manchester United</span>
                                                                            <img src="/images/logo-team/manchester.png">
                                                                        </div>
                                                                        <div class="pull-right full-width">
                                                                            <div class="content-lineUp-home">
                                                                                <ul>
                                                                                    <li>
                                                                                        <div class="num-player">1</div>
                                                                                        <div>David De Gea</div>
                                                                                        <div>GK</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">2</div>
                                                                                        <div>Victor Lindelöf</div>
                                                                                        <div>CB</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">6</div>
                                                                                        <div>Paul Pogba</div>
                                                                                        <div>CM</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">8</div>
                                                                                        <div>Juan Mata</div>
                                                                                        <div>CM</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">9</div>
                                                                                        <div>Romelu Lukaku</div>
                                                                                        <div>CM</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">10</div>
                                                                                        <div>Marcus Rashford</div>
                                                                                        <div>FT</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">11</div>
                                                                                        <div>Anthony Martial</div>
                                                                                        <div>RF</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">12</div>
                                                                                        <div>Chris Smalling</div>
                                                                                        <div>CB</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">15</div>
                                                                                        <div>Andreas Pereira</div>
                                                                                        <div>MD</div>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="pull-left">
                                                                            <img src="/images/logo-team/Chelsea.png">
                                                                            <span>Chelsea</span>
                                                                        </div>
                                                                        <div class="pull-left full-width">
                                                                            <div class="content-lineUp-away">
                                                                                <ul>
                                                                                    <li>
                                                                                        <div class="num-player">1</div>
                                                                                        <div>David De Gea</div>
                                                                                        <div>GK</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">2</div>
                                                                                        <div>Victor Lindelöf</div>
                                                                                        <div>CB</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">6</div>
                                                                                        <div>Paul Pogba</div>
                                                                                        <div>CM</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">8</div>
                                                                                        <div>Juan Mata</div>
                                                                                        <div>CM</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">9</div>
                                                                                        <div>Romelu Lukaku</div>
                                                                                        <div>CM</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">10</div>
                                                                                        <div>Marcus Rashford</div>
                                                                                        <div>FT</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">11</div>
                                                                                        <div>Anthony Martial</div>
                                                                                        <div>RF</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">12</div>
                                                                                        <div>Chris Smalling</div>
                                                                                        <div>CB</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">15</div>
                                                                                        <div>Andreas Pereira</div>
                                                                                        <div>MD</div>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>


                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="statistics">
                                        <div class="table statistics-table">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div class="pull-right">
                                                            <img src="/images/logo-team/manchester.png">
                                                        </div>
                                                    </td>
                                                    <td><h3>VS</h3></td>
                                                    <td>
                                                        <div class="pull-left">
                                                            <img src="/images/logo-team/Chelsea.png">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>เริ่มเขี่ยบอล</td>
                                                    <td><span class="check-team"></span></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>เปลี่ยนตัวคนแรก</td>
                                                    <td><span class="check-team"></span></td>
                                                </tr>
                                                <tr>
                                                    <td><span class="check-team"></span></td>
                                                    <td>เริ่มเขี่ยบอล</td>
                                                    <td></td>
                                                </tr>
                                                <!--                                                ยิงประตู-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress active">15</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="15" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 15%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>ยิงประตู</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="9" aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 9%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress">9</div>
                                                    </td>
                                                </tr>
                                                <!--                                                ยิงตรงกรอบ-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress active">5</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="15" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 5%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>ยิงตรงกรอบ</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="9" aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 2%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress">2</div>
                                                    </td>
                                                </tr>
                                                <!--                                                ผิดกติกา-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress active">13</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="15" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 13%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>ผิดกติกา</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="8" aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 8%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress">8</div>
                                                    </td>
                                                </tr>
                                                <!--                                                เตะมุม-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress">2</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="2" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 2%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>เตะมุม</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="3" aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 3%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress active">3</div>
                                                    </td>
                                                </tr>
                                                <!--                                                ฟรีคิก-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress">12</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="12" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 12%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>ฟรีคิก</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="19" aria-valuemin="0"
                                                                 aria-valuemax="100"
                                                                 style="width: 19%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress active">19</div>
                                                    </td>
                                                </tr>
                                                <!--                                                ล้ำหน้า-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress active">6</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="6" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 6%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>ล้ำหน้า</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="4" aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 4%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress">4</div>
                                                    </td>
                                                </tr>
                                                <!--                                                จำนวนใบเหลือง-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress">3</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="3" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 3%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>ล้ำหน้า</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="3" aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 3%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress">3</div>
                                                    </td>
                                                </tr>
                                                <!--                                                เวลาครองบอล-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress active">55%</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="55" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 55%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>เวลาครองบอล</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="45" aria-valuemin="0"
                                                                 aria-valuemax="100"
                                                                 style="width: 45%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress">45%</div>
                                                    </td>
                                                </tr>
                                                <!--                                                เซฟได้-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress">1</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="1" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 1%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>เซฟได้</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="4" aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 4%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress active">4</div>
                                                    </td>
                                                </tr>
                                                <!--                                                เปลี่ยนตัว-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress">3</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="3" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 3%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>เปลี่ยนตัว</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="3" aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 3%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress">3</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="check-team"></span></td>
                                                    <td>เปลี่ยนตัวคนสุดท้าย</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>ใบเหลืองใบสุดท้าย</td>
                                                    <td><span class="check-team"></span></td>
                                                </tr>


                                            </table>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="lineUp">

                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="review">
                                        <div class="home-panel-body pull-left">
                                            <div class="content-home-review full-width pull-right">
                                                <div class="pull-right">
                                                    <div class="area-logoName">
                                                        <div class="pull-right logo-review-user">
                                                            <img src="/images/profess-player/sample-logo01.png">
                                                        </div>
                                                        <div class="pull-right">
                                                            <h4>LOMTOE.NET</h4>
                                                            <ul>
                                                                <li>@manchester</li>
                                                                <li><img src="/images/logo-team/manchester.png"</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pull-right text-review-content text-right">
                                                    <p>
                                                        อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                        นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                                    </p>
                                                </div>
                                                <div class="pull-left ct-winrate">
                                                    <div>
                                                        <h3>28%</h3>
                                                        <small>Win rate</small>
                                                    </div>
                                                    <div>
                                                        <h3>30</h3>
                                                        <small>Days</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="content-home-review full-width pull-right">
                                                <div class="pull-right">
                                                    <div class="area-logoName">
                                                        <div class="pull-right logo-review-user">
                                                            <img src="/images/profess-player/sample-logo05.png">
                                                        </div>
                                                        <div class="pull-right">
                                                            <h4>SMM SPORT</h4>
                                                            <ul>
                                                                <li>@manchester</li>
                                                                <li><img src="/images/logo-team/manchester.png"</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pull-right text-review-content text-right">
                                                    <p>
                                                        อาจจะฝืดๆหน่อย​ แต่ต้องสอย​ อย่าให้เสียชื่อทีมใหญ่
                                                    </p>
                                                </div>
                                                <div class="pull-left ct-winrate">
                                                    <div>
                                                        <h3>28%</h3>
                                                        <small>Win rate</small>
                                                    </div>
                                                    <div>
                                                        <h3>30</h3>
                                                        <small>Days</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="content-home-review full-width pull-right">
                                                <div class="pull-right">
                                                    <div class="area-logoName">
                                                        <div class="pull-right logo-review-user">
                                                            <img src="/images/9.jpg">
                                                        </div>
                                                        <div class="pull-right">
                                                            <h4>หมูตัวนี้ตัวใหญ่</h4>
                                                            <ul>
                                                                <li>@manchester</li>
                                                                <li><img src="/images/logo-team/manchester.png"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pull-right text-review-content text-right">
                                                    <p>
                                                        ชอบความคมของตรานกแก้ว ซวยแล้วบายาโดลิด
                                                    </p>
                                                </div>
                                                <div class="pull-left ct-winrate">
                                                    <div>
                                                        <h3>28%</h3>
                                                        <small>Win rate</small>
                                                    </div>
                                                    <div>
                                                        <h3>30</h3>
                                                        <small>Days</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="away-panel-body pull-right">
                                            <div class="content-away-review full-width pull-left">
                                                <div class="pull-left">
                                                    <div class="area-logoName area-logoName-away">
                                                        <div class="pull-left logo-review-user">
                                                            <img src="/images/profess-player/sample-logo12.png">
                                                        </div>
                                                        <div class="pull-left">
                                                            <h4>GOAL.IN.TH</h4>
                                                            <ul>
                                                                <li><img src="/images/logo-team/Chelsea.png"</li>
                                                                <li>@Chelsea</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pull-left text-review-content text-left">
                                                    <p>
                                                        อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                        นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                                    </p>
                                                </div>
                                                <div class="pull-right ct-winrate">
                                                    <div>
                                                        <h3>28%</h3>
                                                        <small>Win rate</small>
                                                    </div>
                                                    <div>
                                                        <h3>30</h3>
                                                        <small>Days</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="full-width text-center">
                                            <a class="btn btn-default btn-sm" href="#" role="button">อ่านทีเด็ดทรรศนะทั้งหมด</a>
                                        </div>
                                    </div>
                                    <div style="clear: both"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--full match-->
                    <div class="panel panel-default livescore-panel livescore-full-time">
                        <div class="panel-heading">
                            <div class="hdp-float FT-float">
                                <ul>
                                    <li>2</li>
                                    <li><h3>FT</h3></li>
                                    <li>2</li>
                                </ul>
                            </div>
                            <div class="left-accordion-row pull-left">
                                <div class="pull-right logo-team-ponball">
                                    <img src="/images/logo-team/manchester.png">
                                </div>
                                <div class="pull-right detail-team-ponball">
                                    <div class="top-detail-team-ponball text-right">
                                        <h4>Manchester United</h4>
                                    </div>
                                    <div class="bottom-detail-team-ponball">
                                        <div class="score-ct">60%</div>
                                        <div class="progress full-width">
                                            <div class="progress-bar pull-right" role="progressbar" aria-valuenow="60"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <ul class="status-lastMatch full-width">
                                        <li><p>Last Match</p></li>
                                        <li>
                                            <div class="draw-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="lose-status"></div>
                                        </li>
                                    </ul>
                                    <ul class="status-lastMatch full-width odd">
                                        <li><p>ODD</p></li>
                                        <li>
                                            <div class="draw-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="lose-status"></div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="pull-left content-col-left">
                                    <div class="ct-ft">
                                        <span class="label-ft">FT</span>
                                    </div>
                                    <div class="ct-live-chanel">
                                        <img src="/images/channel/pptv.png">
                                    </div>
                                </div>
                            </div>
                            <div class="right-accordion-row pull-right">
                                <div class="pull-left logo-team-ponball">
                                    <img src="/images/logo-team/Chelsea.png">
                                </div>
                                <div class="pull-left detail-team-ponball">
                                    <div class="top-detail-team-ponball text-left">
                                        <h4>Chelsea</h4>
                                    </div>
                                    <div class="bottom-detail-team-ponball">
                                        <div class="progress full-width">
                                            <div class="progress-bar progress-bar-danger" role="progressbar"
                                                 aria-valuenow="20"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 20%;">
                                            </div>
                                        </div>
                                        <div class="score-ct">20%</div>
                                    </div>
                                </div>
                                <div class="pull-left">
                                    <ul class="status-lastMatch full-width">
                                        <li>
                                            <div class="draw-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="lose-status"></div>
                                        </li>
                                        <li><p>Last Match</p></li>
                                    </ul>
                                    <ul class="status-lastMatch full-width odd">
                                        <li>
                                            <div class="draw-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="win-status"></div>
                                        </li>
                                        <li>
                                            <div class="lose-status"></div>
                                        </li>
                                        <li><p>ODD</p></li>
                                    </ul>
                                </div>
                                <div class="pull-right-section">
                                    <div class="intro-game">
                                        <a href="/game.php" data-toggle="tooltip" data-placement="top"
                                           title="เล่นเกมทายผล">
                                            <i class="fa fa-futbol-o" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <a data-toggle="collapse" class="review-dropdown collapsed" data-parent="#accordion"
                                       href="#collapse_endmatch">
                                        <h4>Live score</h4>
                                    </a>
                                </div>

                            </div>
                            <div style="clear: both"></div>
                        </div>
                        <div id="collapse_endmatch" class="panel-collapse collapse">
                            <div class="panel-body">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs nav-tabs-highlight" role="tablist">
                                    <li class="active" role="presentation">
                                        <a href="#livescore-tab" role="tab"
                                           data-toggle="tab">Livescore</a></li>
                                    <li role="presentation">
                                        <a href="#review" role="tab" data-toggle="tab">ทีเด็ด-ทรรศนะ</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="livescore-tab">
                                        <div class="col-md-4">

                                            <div class="thumbnail">

                                                <div class="header-event-match">
                                                    <ul>
                                                        <li><h3>EVENT MATCH</h3></li>
                                                        <li><span class="end-match">End Match</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="score-event-match">

                                                    <div class="pull-right">
                                                        <img src="/images/logo-team/manchester.png">
                                                        <p>Manchester United</p>

                                                    </div>
                                                    <h4>2-2</h4>
                                                    <div class="pull-left text-right">
                                                        <p>Chelsea</p>
                                                        <img src="/images/logo-team/Chelsea.png">
                                                    </div>

                                                </div>
                                                <div class="content-event-match">

                                                    <!--                                        Home ใบแดง-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-home-live">
                                                            <div class="box-football-icon">
                                                                <div class="red-card"></div>
                                                            </div>
                                                            <div class="min-live">90+4'</div>
                                                            <div class="name-live">Willian</div>
                                                        </div>
                                                    </div>
                                                    <!--                                        Home ใบเหลือง-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-home-live">
                                                            <div class="box-football-icon">
                                                                <div class="yellow-card"></div>
                                                            </div>
                                                            <div class="min-live">87'</div>
                                                            <div class="name-live">Cesc Fàbregas</div>
                                                        </div>
                                                    </div>
                                                    <!--                                        Home ทำประตู-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-home-live">
                                                            <div class="box-football-icon">
                                                                <div class="goal-score"></div>
                                                            </div>
                                                            <div class="min-live">71'</div>
                                                            <div class="score-live">2 - 4</div>
                                                            <div class="name-live">Cesc Fàbregas</div>
                                                        </div>
                                                    </div>
                                                    <!--                                        Home เปลี่ยนตัว-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-home-live">
                                                            <div class="box-football-icon">
                                                                <div class="substitute-player"></div>
                                                            </div>
                                                            <div class="min-live">69'</div>
                                                            <div class="name-live"><span
                                                                        class="substitute-player-out"></span>
                                                                David
                                                                Luiz
                                                            </div>
                                                            <div class="name-live"><span
                                                                        class="substitute-player-in"></span>
                                                                Victor
                                                                Moses
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--Home ใบเหลือง+ใบเหลือง=ใบแดง-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-home-live">
                                                            <div class="box-football-icon">
                                                                <div class="group-yellow-card"></div>
                                                            </div>
                                                            <div class="min-live">55'</div>
                                                            <div class="name-live">Ross Barkley</div>
                                                        </div>
                                                    </div>
                                                    <!--Home จุดโทษ-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-home-live">
                                                            <div class="box-football-icon">
                                                                <div class="penalty-score"></div>
                                                            </div>
                                                            <div class="min-live">45'</div>
                                                            <div class="name-live">Gary Cahill</div>
                                                        </div>
                                                    </div>
                                                    <!--Home พลาดจุดโทษ-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-home-live">
                                                            <div class="box-football-icon">
                                                                <div class="penalty-none-score"></div>
                                                            </div>
                                                            <div class="min-live">41'</div>
                                                            <div class="name-live">Álvaro Morata</div>
                                                        </div>
                                                    </div>
                                                    <!--Home ทำเข้าประตูตัวเอง-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-home-live">
                                                            <div class="box-football-icon">
                                                                <div class="own-goal"></div>
                                                            </div>
                                                            <div class="min-live">28'</div>
                                                            <div class="name-live">Álvaro Morata</div>
                                                        </div>
                                                    </div>


                                                    <!--                                        Away ใบแดง-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-away-live pull-right">
                                                            <div class="name-live">Willian</div>
                                                            <div class="min-live">90+4'</div>
                                                            <div class="box-football-icon">
                                                                <div class="red-card"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--                                        Away ใบเหลือง-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-away-live pull-right">
                                                            <div class="name-live">Cesc Fàbregas</div>
                                                            <div class="min-live">87'</div>
                                                            <div class="box-football-icon">
                                                                <div class="yellow-card"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--                                        Away ทำประตู-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-away-live pull-right">
                                                            <div class="name-live">Cesc Fàbregas</div>
                                                            <div class="score-live">2 - 4</div>
                                                            <div class="min-live">71'</div>
                                                            <div class="box-football-icon">
                                                                <div class="goal-score"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--                                        Away เปลี่ยนตัว-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-away-live pull-right">
                                                            <div class="name-live"><span
                                                                        class="substitute-player-in"></span>
                                                                Victor
                                                                Moses
                                                            </div>
                                                            <div class="name-live"><span
                                                                        class="substitute-player-out"></span>
                                                                David
                                                                Luiz
                                                            </div>
                                                            <div class="min-live">69'</div>
                                                            <div class="box-football-icon">
                                                                <div class="substitute-player"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--Away ใบเหลือง+ใบเหลือง=ใบแดง-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-away-live pull-right">
                                                            <div class="name-live">Ross Barkley</div>
                                                            <div class="min-live">55'</div>
                                                            <div class="box-football-icon">
                                                                <div class="group-yellow-card"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--Away จุดโทษ-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-away-live pull-right">
                                                            <div class="name-live">Gary Cahill</div>
                                                            <div class="min-live">45'</div>
                                                            <div class="box-football-icon">
                                                                <div class="penalty-score"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--Away พลาดจุดโทษ-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-away-live pull-right">
                                                            <div class="name-live">Álvaro Morata</div>
                                                            <div class="min-live">41'</div>
                                                            <div class="box-football-icon">
                                                                <div class="penalty-none-score"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--Away ทำเข้าประตูตัวเอง-->
                                                    <div class="full-width line-content-all">
                                                        <div class="col-away-live pull-right">
                                                            <div class="name-live">Álvaro Morata</div>
                                                            <div class="min-live">28'</div>
                                                            <div class="box-football-icon">
                                                                <div class="own-goal"></div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 content-status-match">
                                            <div class="thumbnail">

                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#statistics-tab"
                                                                                              role="tab"
                                                                                              data-toggle="tab">Statistics</a>
                                                    </li>
                                                    <li role="presentation"><a href="#lineUp-tab"
                                                                               role="tab"
                                                                               data-toggle="tab">Line
                                                            Up</a>
                                                    </li>

                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="statistics-tab">
                                                        <div class="table statistics-table">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="pull-right">
                                                                            <h4>Manchester United
                                                                            </h4>
                                                                            <img src="/images/logo-team/manchester.png">
                                                                        </div>
                                                                    </td>
                                                                    <td><h3>VS</h3></td>
                                                                    <td>
                                                                        <div class="pull-left">
                                                                            <img src="/images/logo-team/Chelsea.png">
                                                                            <h4>Chelsea</h4>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                ยิงประตู-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress active">15</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="15"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 15%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>ยิงประตู</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="9" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 9%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress">9</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                ยิงตรงกรอบ-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress active">5</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="15"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100" style="width: 5%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>ยิงตรงกรอบ</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="9" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 2%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress">2</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                ผิดกติกา-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress active">13</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="15"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 13%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>ผิดกติกา</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="8" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 8%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress">8</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                เตะมุม-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress">2</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="2"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100" style="width: 2%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>เตะมุม</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="3" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 3%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress active">3</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                ฟรีคิก-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress">12</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="12"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 12%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>ฟรีคิก</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="19" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 19%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress active">19</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                ล้ำหน้า-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress active">6</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="6"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100" style="width: 6%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>ล้ำหน้า</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="4" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 4%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress">4</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                จำนวนใบเหลือง-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress">3</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="3"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100" style="width: 3%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>ล้ำหน้า</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="3" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 3%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress">3</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                เวลาครองบอล-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress active">55%</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="55"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 55%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>เวลาครองบอล</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="45" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 45%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress">45%</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                เซฟได้-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress">1</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="1"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100" style="width: 1%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>เซฟได้</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="4" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 4%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress active">4</div>
                                                                    </td>
                                                                </tr>
                                                                <!--                                                เปลี่ยนตัว-->
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress">3</div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="3"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="100" style="width: 3%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>เปลี่ยนตัว</td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="3" aria-valuemin="0"
                                                                                 aria-valuemax="100"
                                                                                 style="width: 3%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress">3</div>
                                                                    </td>
                                                                </tr>


                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="lineUp-tab">
                                                        <div class="lineUp-table">
                                                            <table class="table">
                                                                <tr>
                                                                    <td>
                                                                        <div class="pull-right full-width text-right">
                                                                            <span>Manchester United</span>
                                                                            <img src="/images/logo-team/manchester.png">
                                                                        </div>
                                                                        <div class="pull-right full-width">
                                                                            <div class="content-lineUp-home">
                                                                                <ul>
                                                                                    <li>
                                                                                        <div class="num-player">1</div>
                                                                                        <div>David De Gea</div>
                                                                                        <div>GK</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">2</div>
                                                                                        <div>Victor Lindelöf</div>
                                                                                        <div>CB</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">6</div>
                                                                                        <div>Paul Pogba</div>
                                                                                        <div>CM</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">8</div>
                                                                                        <div>Juan Mata</div>
                                                                                        <div>CM</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">9</div>
                                                                                        <div>Romelu Lukaku</div>
                                                                                        <div>CM</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">10</div>
                                                                                        <div>Marcus Rashford</div>
                                                                                        <div>FT</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">11</div>
                                                                                        <div>Anthony Martial</div>
                                                                                        <div>RF</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">12</div>
                                                                                        <div>Chris Smalling</div>
                                                                                        <div>CB</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">15</div>
                                                                                        <div>Andreas Pereira</div>
                                                                                        <div>MD</div>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="pull-left">
                                                                            <img src="/images/logo-team/Chelsea.png">
                                                                            <span>Chelsea</span>
                                                                        </div>
                                                                        <div class="pull-left full-width">
                                                                            <div class="content-lineUp-away">
                                                                                <ul>
                                                                                    <li>
                                                                                        <div class="num-player">1</div>
                                                                                        <div>David De Gea</div>
                                                                                        <div>GK</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">2</div>
                                                                                        <div>Victor Lindelöf</div>
                                                                                        <div>CB</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">6</div>
                                                                                        <div>Paul Pogba</div>
                                                                                        <div>CM</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">8</div>
                                                                                        <div>Juan Mata</div>
                                                                                        <div>CM</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">9</div>
                                                                                        <div>Romelu Lukaku</div>
                                                                                        <div>CM</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">10</div>
                                                                                        <div>Marcus Rashford</div>
                                                                                        <div>FT</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">11</div>
                                                                                        <div>Anthony Martial</div>
                                                                                        <div>RF</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">12</div>
                                                                                        <div>Chris Smalling</div>
                                                                                        <div>CB</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="num-player">15</div>
                                                                                        <div>Andreas Pereira</div>
                                                                                        <div>MD</div>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>


                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="statistics">
                                        <div class="table statistics-table">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div class="pull-right">
                                                            <img src="/images/logo-team/manchester.png">
                                                        </div>
                                                    </td>
                                                    <td><h3>VS</h3></td>
                                                    <td>
                                                        <div class="pull-left">
                                                            <img src="/images/logo-team/Chelsea.png">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>เริ่มเขี่ยบอล</td>
                                                    <td><span class="check-team"></span></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>เปลี่ยนตัวคนแรก</td>
                                                    <td><span class="check-team"></span></td>
                                                </tr>
                                                <tr>
                                                    <td><span class="check-team"></span></td>
                                                    <td>เริ่มเขี่ยบอล</td>
                                                    <td></td>
                                                </tr>
                                                <!--                                                ยิงประตู-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress active">15</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="15" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 15%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>ยิงประตู</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="9" aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 9%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress">9</div>
                                                    </td>
                                                </tr>
                                                <!--                                                ยิงตรงกรอบ-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress active">5</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="15" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 5%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>ยิงตรงกรอบ</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="9" aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 2%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress">2</div>
                                                    </td>
                                                </tr>
                                                <!--                                                ผิดกติกา-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress active">13</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="15" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 13%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>ผิดกติกา</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="8" aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 8%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress">8</div>
                                                    </td>
                                                </tr>
                                                <!--                                                เตะมุม-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress">2</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="2" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 2%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>เตะมุม</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="3" aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 3%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress active">3</div>
                                                    </td>
                                                </tr>
                                                <!--                                                ฟรีคิก-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress">12</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="12" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 12%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>ฟรีคิก</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="19" aria-valuemin="0"
                                                                 aria-valuemax="100"
                                                                 style="width: 19%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress active">19</div>
                                                    </td>
                                                </tr>
                                                <!--                                                ล้ำหน้า-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress active">6</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="6" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 6%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>ล้ำหน้า</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="4" aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 4%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress">4</div>
                                                    </td>
                                                </tr>
                                                <!--                                                จำนวนใบเหลือง-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress">3</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="3" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 3%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>ล้ำหน้า</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="3" aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 3%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress">3</div>
                                                    </td>
                                                </tr>
                                                <!--                                                เวลาครองบอล-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress active">55%</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="55" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 55%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>เวลาครองบอล</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="45" aria-valuemin="0"
                                                                 aria-valuemax="100"
                                                                 style="width: 45%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress">45%</div>
                                                    </td>
                                                </tr>
                                                <!--                                                เซฟได้-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress">1</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="1" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 1%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>เซฟได้</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="4" aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 4%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress active">4</div>
                                                    </td>
                                                </tr>
                                                <!--                                                เปลี่ยนตัว-->
                                                <tr>
                                                    <td>
                                                        <div class="num-progress">3</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                 role="progressbar" aria-valuenow="3" aria-valuemin="0"
                                                                 aria-valuemax="100" style="width: 3%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>เปลี่ยนตัว</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="3" aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 3%;">
                                                            </div>
                                                        </div>
                                                        <div class="num-progress">3</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="check-team"></span></td>
                                                    <td>เปลี่ยนตัวคนสุดท้าย</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>ใบเหลืองใบสุดท้าย</td>
                                                    <td><span class="check-team"></span></td>
                                                </tr>


                                            </table>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="lineUp">

                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="review">
                                        <div class="home-panel-body pull-left">
                                            <div class="content-home-review full-width pull-right">
                                                <div class="pull-right">
                                                    <div class="area-logoName">
                                                        <div class="pull-right logo-review-user">
                                                            <img src="/images/profess-player/sample-logo01.png">
                                                        </div>
                                                        <div class="pull-right">
                                                            <h4>LOMTOE.NET</h4>
                                                            <ul>
                                                                <li>@manchester</li>
                                                                <li><img src="/images/logo-team/manchester.png"</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pull-right text-review-content text-right">
                                                    <p>
                                                        อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                        นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                                    </p>
                                                </div>
                                                <div class="pull-left ct-winrate">
                                                    <div>
                                                        <h3>28%</h3>
                                                        <small>Win rate</small>
                                                    </div>
                                                    <div>
                                                        <h3>30</h3>
                                                        <small>Days</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="content-home-review full-width pull-right">
                                                <div class="pull-right">
                                                    <div class="area-logoName">
                                                        <div class="pull-right logo-review-user">
                                                            <img src="/images/profess-player/sample-logo05.png">
                                                        </div>
                                                        <div class="pull-right">
                                                            <h4>SMM SPORT</h4>
                                                            <ul>
                                                                <li>@manchester</li>
                                                                <li><img src="/images/logo-team/manchester.png"</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pull-right text-review-content text-right">
                                                    <p>
                                                        อาจจะฝืดๆหน่อย​ แต่ต้องสอย​ อย่าให้เสียชื่อทีมใหญ่
                                                    </p>
                                                </div>
                                                <div class="pull-left ct-winrate">
                                                    <div>
                                                        <h3>28%</h3>
                                                        <small>Win rate</small>
                                                    </div>
                                                    <div>
                                                        <h3>30</h3>
                                                        <small>Days</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="content-home-review full-width pull-right">
                                                <div class="pull-right">
                                                    <div class="area-logoName">
                                                        <div class="pull-right logo-review-user">
                                                            <img src="/images/9.jpg">
                                                        </div>
                                                        <div class="pull-right">
                                                            <h4>หมูตัวนี้ตัวใหญ่</h4>
                                                            <ul>
                                                                <li>@manchester</li>
                                                                <li><img src="/images/logo-team/manchester.png"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pull-right text-review-content text-right">
                                                    <p>
                                                        ชอบความคมของตรานกแก้ว ซวยแล้วบายาโดลิด
                                                    </p>
                                                </div>
                                                <div class="pull-left ct-winrate">
                                                    <div>
                                                        <h3>28%</h3>
                                                        <small>Win rate</small>
                                                    </div>
                                                    <div>
                                                        <h3>30</h3>
                                                        <small>Days</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="away-panel-body pull-right">
                                            <div class="content-away-review full-width pull-left">
                                                <div class="pull-left">
                                                    <div class="area-logoName area-logoName-away">
                                                        <div class="pull-left logo-review-user">
                                                            <img src="/images/profess-player/sample-logo12.png">
                                                        </div>
                                                        <div class="pull-left">
                                                            <h4>GOAL.IN.TH</h4>
                                                            <ul>
                                                                <li><img src="/images/logo-team/Chelsea.png"</li>
                                                                <li>@Chelsea</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pull-left text-review-content text-left">
                                                    <p>
                                                        อาจจะต่อแพงไปหน่อย แต่ด้วยประสบการณ์ที่ผ่านมา เซบีย่า
                                                        นั้นเหนือกว่ามาก คงไม่ยากเกินไปที่จะยิงถล่มทลายในนัดนี้
                                                    </p>
                                                </div>
                                                <div class="pull-right ct-winrate">
                                                    <div>
                                                        <h3>28%</h3>
                                                        <small>Win rate</small>
                                                    </div>
                                                    <div>
                                                        <h3>30</h3>
                                                        <small>Days</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="full-width text-center">
                                            <a class="btn btn-default btn-sm" href="#" role="button">อ่านทีเด็ดทรรศนะทั้งหมด</a>
                                        </div>
                                    </div>
                                    <div style="clear: both"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php foreach ($hllist as $lid => $data) {?>
        <div class="content-program-soccer">
            <div class="topic">
                <div class="logo-league-program">
                    <img src="<?php echo $data['league']->getLogo(); ?>">
                </div>
                <div class="title-league-program">
                    <h3><?php echo $data['league']->name; ?></h3>
                    <h5>บ้านผลบอล โปรแกรมบอล ราคาบอล </h5>
                </div>
                <div class="dropdown pull-right">
                </div>

            </div>


            <div class="content">
                <div class="panel-group panel-group-row" id="accordion<?php echo $lid; ?>">
                    <!--full match-->
                    <?php foreach ($data['matches'] as $hl) {?>
                    <div class="panel panel-default livescore-panel color-tape " mid="<?php echo $hl->mid; ?>">
                        <div class="panel-heading">
                            <div mid="<?php echo $hl->mid; ?>" class="hdp-float wait-float">
                                <ul>
                                    <li class="live-h-hdp" mid="<?php echo $hl->mid; ?>"><?php echo (empty($hl->match->hdp)) ? "-" : number_format($hl->match->hdp->home_water_bill, 2); ?></li>
                                    <li><h3 class="live-hdp" mid="<?php echo $hl->mid; ?>"><?php echo (empty($hl->match->hdp)) ? "-" : $hl->match->hdp->handicap; ?></h3></li>
                                    <li class="live-a-hdp" mid="<?php echo $hl->mid; ?>"><?php echo (empty($hl->match->hdp)) ? "-" : number_format($hl->match->hdp->away_water_bill, 2); ?></li>
                                </ul>
                            </div>
                            <div mid="<?php echo $hl->mid; ?>" class="hdp-float FT-float">
                                <ul>
                                    <li class="live-h-score" mid="<?php echo $hl->mid; ?>"><?php echo $hl->match->live_score_home; ?></li>
                                    <li><h3>FT</h3></li>
                                    <li class="live-a-score" mid="<?php echo $hl->mid; ?>"><?php echo $hl->match->live_score_away; ?></li>
                                </ul>
                            </div>
                            <div mid="<?php echo $hl->mid; ?>" class="hdp-float live-float">
                                <ul>
                                    <li class="live-h-score" mid="<?php echo $hl->mid; ?>"><?php echo $hl->match->live_score_home; ?></li>
                                    <li>
                                        <div class="progress minute-progress" mid="<?php echo $hl->mid; ?>" data-percentage="40">
				                            <span class="progress-left">
                                                <span class="progress-bar"></span>
				                            </span>
                                            <span class="progress-right">
                                                <span class="progress-bar"></span>
                                            </span>
                                            <div class="progress-value">
                                                <div class="live-minute" mid="<?php echo $hl->mid; ?>">45+</div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="live-a-score" mid="<?php echo $hl->mid; ?>"><?php echo $hl->match->live_score_away; ?></li>
                                </ul>
                            </div>
                            <div class="left-accordion-row pull-left">
                                <div class="pull-right logo-team-ponball">
                                    <img src="<?php echo $hl->match->teama->getLogo(); ?>">
                                </div>
                                <div class="pull-right detail-team-ponball">
                                    <div class="top-detail-team-ponball text-right">
                                        <h4><?php echo $hl->match->teama->name_en; ?></h4>
                                    </div>
                                    <div class="bottom-detail-team-ponball">
                                        <div class="score-ct beton-homep" mid="<?php echo $hl->mid; ?>">0%</div>
                                        <div class="progress full-width">
                                            <div class="progress-bar pull-right beton-home" mid="<?php echo $hl->mid; ?>" role="progressbar" aria-valuenow="0"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <ul class="status-lastMatch full-width">
                                        <li><p>Last Match</p></li>
                                        <?php foreach (explode(",", $hl->match->lastresult_home) as $rs) {?>
                                        <?php
$rsclass = "draw-status";
    switch ($rs) {
        case "w":
            $rsclass = "win-status";
            break;
        case "l":
            $rsclass = "lose-status";
            break;
        default:
            $rsclass = "draw-status";
            break;
    }

    ?>
                                        <li>
                                            <div class="<?php echo $rsclass; ?>"></div>
                                        </li>
                                        <?php }?>
                                    </ul>
                                    <ul class="status-lastMatch full-width odd">
                                        <li><p>ODD</p></li>
                                        <?php foreach (explode(",", $hl->match->lastresult_odds_home) as $rs) {?>
                                        <?php
$rsclass = "draw-status";
    switch ($rs) {
        case "w":
            $rsclass = "win-status";
            break;
        case "l":
            $rsclass = "lose-status";
            break;
        default:
            $rsclass = "draw-status";
            break;
    }

    ?>
                                        <li>
                                            <div class="<?php echo $rsclass; ?>"></div>
                                        </li>
                                        <?php }?>
                                    </ul>
                                </div>

                                <div class="pull-left content-col-left">
                                    <div class="at-minute ct-ft">
                                        <?php
$stime = Carbon::createFromFormat("Y-m-d H:i:s", $hl->match->time_match, "Asia/Singapore");
    $stime->tz = "Asia/Bangkok";
    ?>
                                        <span class="at-minute-mark label-ft" at="<?php echo $hl->match->time_match; ?>" mid="<?php echo $hl->mid; ?>" state="<?php echo $hl->match->state; ?>" ht="<?php echo $hl->match->ht_start; ?>"><?php echo $stime->format("H:i"); ?></span>
                                    </div>
                                    <div class="ct-live-chanel hide">
                                        <img src="/images/channel/pptv.png">
                                    </div>
                                </div>
                            </div>
                            <div class="right-accordion-row pull-right">
                                <div class="pull-left logo-team-ponball">
                                    <img src="<?php echo $hl->match->teamb->getLogo(); ?>">
                                </div>
                                <div class="pull-left detail-team-ponball">
                                    <div class="top-detail-team-ponball text-left">
                                        <h4><?php echo $hl->match->teamb->name_en; ?></h4>
                                    </div>
                                    <div class="bottom-detail-team-ponball">
                                        <div class="progress full-width">
                                            <div class="progress-bar progress-bar-danger beton-away" mid="<?php echo $hl->mid; ?>"  role="progressbar"
                                                 aria-valuenow="0"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                            </div>
                                        </div>
                                        <div class="score-ct beton-awayp" mid="<?php echo $hl->mid; ?>">0%</div>
                                    </div>
                                </div>
                                <div class="pull-left">
                                    <ul class="status-lastMatch full-width">
                                    <?php foreach (explode(",", $hl->match->lastresult_away) as $rs) {?>
                                        <?php
$rsclass = "draw-status";
        switch ($rs) {
            case "w":
                $rsclass = "win-status";
                break;
            case "l":
                $rsclass = "lose-status";
                break;
            default:
                $rsclass = "draw-status";
                break;
        }

        ?>
                                        <li>
                                            <div class="<?php echo $rsclass; ?>"></div>
                                        </li>
                                        <?php }?>
                                        <li><p>Last Match</p></li>
                                    </ul>
                                    <ul class="status-lastMatch full-width odd">
                                    <?php foreach (explode(",", $hl->match->lastresult_odds_away) as $rs) {?>
                                        <?php
$rsclass = "draw-status";
        switch ($rs) {
            case "w":
                $rsclass = "win-status";
                break;
            case "l":
                $rsclass = "lose-status";
                break;
            default:
                $rsclass = "draw-status";
                break;
        }

        ?>
                                        <li>
                                            <div class="<?php echo $rsclass; ?>"></div>
                                        </li>
                                        <?php }?>
                                        <li><p>ODD</p></li>
                                    </ul>
                                </div>
                                <div class="pull-right-section">
                                    <div class="intro-game">
                                        <a href="<?php echo "/match/{$hl->match->id}.html"; ?>" data-toggle="tooltip" data-placement="top"
                                           title="เล่นเกมทายผล">
                                            <i class="fa fa-futbol-o" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <a data-toggle="collapse" class="review-dropdown collapsed" data-parent="#accordion<?php echo $lid; ?>"
                                       href="#<?php echo "collapse_endmatch" . $hl->mid; ?>" mid="<?php echo $hl->mid; ?>">
                                        <h4>Live score</h4>
                                    </a>
                                </div>

                            </div>
                            <div style="clear: both"></div>
                        </div>
                        <div id="<?php echo "collapse_endmatch" . $hl->mid; ?>" class="panel-collapse collapse">
                            <div class="panel-body">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs nav-tabs-highlight" role="tablist">
                                    <li class="<?php echo ($hl->match->state != 17) ? "active" : ""; ?>" role="presentation">
                                        <a href="#<?php echo "livescore-tab" . $hl->mid; ?>" role="tab"
                                           data-toggle="tab">Livescore</a></li>
                                    <li class="<?php echo ($hl->match->state == 17) ? "active" : ""; ?>" role="presentation">
                                        <a href="#<?php echo "review" . $hl->mid; ?>" role="tab" data-toggle="tab">ทีเด็ด-ทรรศนะ</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane <?php echo ($hl->match->state != 17) ? "active" : ""; ?>" id="<?php echo "livescore-tab" . $hl->mid; ?>">
                                        <div class="col-md-4">

                                            <div class="thumbnail">

                                                <div class="header-event-match">
                                                    <ul>
                                                        <li><h3>EVENT MATCH</h3></li>
                                                        <li><span class="event-dp-button end-match" mid="<?php echo $hl->mid; ?>">End Match</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="score-event-match">

                                                    <div class="pull-right">
                                                        <img src="<?php echo $hl->match->teama->getLogo(); ?>">
                                                        <p><?php echo $hl->match->teama->name_en; ?></p>

                                                    </div>
                                                    <h4><span class="live-h-score" mid="<?php echo $hl->mid; ?>"><?php echo $hl->match->live_score_home; ?></span>-<span class="live-a-score" mid="<?php echo $hl->mid; ?>"><?php echo $hl->match->live_score_away; ?></span></h4>
                                                    <div class="pull-left text-right">
                                                        <p><?php echo $hl->match->teamb->name_en; ?></p>
                                                        <img src="<?php echo $hl->match->teamb->getLogo(); ?>">
                                                    </div>

                                                </div>
                                                <div class="content-event-match" mid="<?php echo $hl->mid; ?>">
                                                    <?php $matchevent = $mcon->getEvent($hl->mid);?>
                                                    <?php foreach ($matchevent as $event) {?>
                                                        <?php if ($event->side == "home") {?>
                                                            <div class="full-width line-content-all">
                                                                <div class="col-home-live">
                                                                    <div class="box-football-icon">
                                                                        <div class="<?php echo $event->getEventImgClass(); ?>"></div>
                                                                    </div>
                                                                    <div class="min-live"><?php echo $event->at_minute . "'"; ?></div>
                                                                    <div class="score-live"><?php echo $event->score; ?></div>
                                                                    <?php if (empty($event->assist)) {?>
                                                                    <div class="name-live"><?php echo $event->player; ?></div>
                                                                    <?php } else {?>
                                                                            <?php if ($event->event == "substutitions") {?>
                                                                                <div class="name-live">
                                                                                    <span class="substitute-player-out"></span>
                                                                                    <p><?php echo $event->player; ?></p>
                                                                                </div>
                                                                                <div class="name-live">
                                                                                    <span class="substitute-player-in"></span>
                                                                                    <p><?php echo $event->assist; ?></p>
                                                                                </div>
                                                                                <?php } else {?>
                                                                                <div class="name-live"><?php echo $event->player . " (" . $event->assist . ")"; ?></div>
                                                                            <?php }?>
                                                                    <?php }?>
                                                                </div>
                                                            </div>
                                                        <?php } else {?>
                                                            <div class="full-width line-content-all">
                                                                <div class="col-away-live pull-right">
                                                                <?php if (empty($event->assist)) {?>
                                                                    <div class="name-live"><?php echo $event->player; ?></div>
                                                                    <?php } else {?>
                                                                            <?php if ($event->event == "substutitions") {?>
                                                                                <div class="name-live">
                                                                                    <span class="substitute-player-in"></span>
                                                                                    <p><?php echo $event->assist; ?></p>
                                                                                </div>
                                                                                <div class="name-live">
                                                                                    <span class="substitute-player-out"></span>
                                                                                    <p><?php echo $event->player; ?></p>
                                                                                </div>
                                                                            <?php } else {?>
                                                                                <div class="name-live"><?php echo "(" . $event->assist . ") " . $event->player; ?></div>
                                                                            <?php }?>
                                                                    <?php }?>
                                                                    <div class="score-live"><?php echo $event->score; ?></div>
                                                                    <div class="min-live"><?php echo $event->at_minute . "'"; ?></div>
                                                                    <div class="box-football-icon">
                                                                        <div class="<?php echo $event->getEventImgClass(); ?>"></div>
                                                                    </div>
                                                            </div>
                                                    </div>

                                                            <?php }?>
                                                    <?php }?>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 content-status-match">
                                            <div class="thumbnail">

                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#<?php echo "statistics-tab" . $hl->mid; ?>"
                                                                                              role="tab"
                                                                                              data-toggle="tab">Statistics</a>
                                                    </li>
                                                    <li role="presentation"><a href="#<?php echo "lineUp-tab" . $hl->mid; ?>"
                                                                               role="tab"
                                                                               data-toggle="tab">Line
                                                            Up</a>
                                                    </li>

                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="<?php echo "statistics-tab" . $hl->mid; ?>">
                                                        <div class="table statistics-table">
                                                            <table>
                                                                <thead>
                                                                <tr>
                                                                    <td>
                                                                        <div class="pull-right">
                                                                            <h4><?php echo $hl->match->teama->name_en; ?></h4>
                                                                            <img src="<?php echo $hl->match->teama->getLogo(); ?>">
                                                                        </div>
                                                                    </td>
                                                                    <td><h3>VS</h3></td>
                                                                    <td>
                                                                        <div class="pull-left">
                                                                            <img src="<?php echo $hl->match->teamb->getLogo(); ?>">
                                                                            <h4><?php echo $hl->match->teamb->name_en; ?></h4>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                </thead>
                                                                <tbody class="statistics-mark" mid="<?php echo $hl->mid; ?>">
                                                                <?php $statlist = $mcon->getStat($hl->mid);?>
                                                                <?php foreach ($statlist as $stat) {?>
                                                                <?php $rmax = $stat->hval + $stat->aval;?>
                                                                <tr>
                                                                    <td>
                                                                        <div class="num-progress <?php echo ($stat->hval > $stat->aval) ? "active" : ""; ?>"><?php echo $stat->hval; ?></div>
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger pull-right"
                                                                                 role="progressbar" aria-valuenow="<?php echo $stat->hval; ?>"
                                                                                 aria-valuemin="0"
                                                                                 aria-valuemax="<?php echo $rmax; ?>"
                                                                                 style="width: <?php echo $stat->hval / $rmax * 100; ?>%;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td><?php echo $stat->type; ?></td>
                                                                    <td>
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar"
                                                                                 aria-valuenow="<?php echo $stat->aval; ?>" aria-valuemin="0"
                                                                                 aria-valuemax="<?php echo $rmax; ?>"
                                                                                 style="width: <?php echo $stat->aval / $rmax * 100; ?>%;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="num-progress <?php echo ($stat->hval < $stat->aval) ? "active" : ""; ?>"><?php echo $stat->aval; ?></div>
                                                                    </td>
                                                                </tr>
                                                                <?php }?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="<?php echo "lineUp-tab" . $hl->mid; ?>">
                                                        <div class="lineUp-table">
                                                            <table class="table">
                                                                <tr>
                                                                    <td>
                                                                        <div class="pull-right full-width text-right">
                                                                            <span><?php echo $hl->match->teama->name_en; ?></span>
                                                                            <img src="<?php echo $hl->match->teama->getLogo(); ?>">
                                                                        </div>
                                                                        <div class="pull-right full-width">
                                                                            <div class="content-lineUp-home">
                                                                                <ul>
                                                                                <?php
$lineups = $mcon->getLineups($hl->mid . "");
    foreach ($lineups as $lineup) {
        if ($lineup->side == "home") {
            ?>
                                                                                    <li>
                                                                                        <div class="num-player"><?php echo $lineup->number; ?></div>
                                                                                        <div><?php echo $lineup->name; ?></div>
                                                                                        <div><?php echo $lineup->position; ?></div>
                                                                                    </li>
                                                                                        <?php }?>
                                                                                    <?php }?>

                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="pull-left">
                                                                            <img src="<?php echo $hl->match->teamb->getLogo(); ?>">
                                                                            <span><?php echo $hl->match->teamb->name_en; ?></span>
                                                                        </div>
                                                                        <div class="pull-left full-width">
                                                                            <div class="content-lineUp-away">
                                                                                <ul>
                                                                                <?php
$lineups = $mcon->getLineups($hl->mid . "");
    foreach ($lineups as $lineup) {
        if ($lineup->side == "away") {
            ?>
                                                                                    <li>
                                                                                        <div class="num-player"><?php echo $lineup->number; ?></div>
                                                                                        <div><?php echo $lineup->name; ?></div>
                                                                                        <div><?php echo $lineup->position; ?></div>
                                                                                    </li>
                                                                                    <?php }?>
                                                                                    <?php }?>

                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>


                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane <?php echo ($hl->match->state == 17) ? "active" : ""; ?>" id="<?php echo "review" . $hl->mid; ?>">
                                        <div class="home-panel-body pull-left">
                                            <?php foreach ($hl->match->reviews as $review) {?>
                                            <?php if ($review->team == "home") {?>
                                            <div class="content-home-review full-width pull-right">
                                                <div class="pull-right">
                                                    <div class="area-logoName">
                                                        <div class="pull-right logo-review-user">
                                                            <img src="<?php echo $review->owner->path; ?>">
                                                        </div>
                                                        <div class="pull-right">
                                                            <h4><?php echo $review->owner->name; ?></h4>
                                                            <ul>
                                                                <li><?php echo "@" . strtolower(str_replace(" ", "", $hl->match->teama->name_en)); ?></li>
                                                                <li><img src="<?php echo $hl->match->teama->getLogo(); ?>"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pull-right text-review-content text-right">
                                                    <p>
                                                    <?php echo $review->review_text; ?>
                                                    </p>
                                                </div>
                                                <div class="pull-left ct-winrate">
                                                    <div>
                                                        <h3><?php echo number_format($review->owner->accurate, 0) . "%"; ?></h3>
                                                        <small>Win rate</small>
                                                    </div>
                                                    <div>
                                                        <h3>30</h3>
                                                        <small>Matches</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php }?>
                                            <?php }?>
                                        </div>
                                        <div class="away-panel-body pull-right">

                                        <?php foreach ($hl->match->reviews as $review) {?>
                                            <?php if ($review->team == "away") {?>
                                            <div class="content-away-review full-width pull-left">
                                                <div class="pull-left">
                                                    <div class="area-logoName area-logoName-away">
                                                        <div class="pull-left logo-review-user">
                                                            <img src="<?php echo $review->owner->path; ?>">
                                                        </div>
                                                        <div class="pull-left">
                                                            <h4><?php echo $review->owner->name; ?></h4>
                                                            <ul>
                                                                <li><img src="<?php echo $hl->match->teamb->getLogo(); ?>"></li>
                                                                <li><?php echo "@" . strtolower(str_replace(" ", "", $hl->match->teamb->name_en)); ?></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pull-left text-review-content text-left">
                                                    <p>
                                                    <?php echo $review->review_text; ?>
                                                    </p>
                                                </div>
                                                <div class="pull-right ct-winrate">
                                                    <div>
                                                        <h3><?php echo number_format($review->owner->accurate, 0) . "%"; ?></h3>
                                                        <small>Win rate</small>
                                                    </div>
                                                    <div>
                                                        <h3>30</h3>
                                                        <small>Matches</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php }?>
                                            <?php }?>
                                        </div>
                                        <div class="full-width text-center">
                                            <a class="btn btn-default btn-sm" href="/review-infomation.php?match_id=<?php echo $hl->mid; ?>" role="button">อ่านทีเด็ดทรรศนะทั้งหมด</a>
                                        </div>
                                    </div>
                                    <div style="clear: both"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }?>


                </div>
            </div>
        </div>
<?php }?>
    </div>

</div>
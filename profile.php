<?php
if (!isset($_SESSION)) {
    session_start();
}

if (!isset($_SESSION['login'])) {
    header('Location: /index.php');
    exit();
}

require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playUser.php";
$playUser = new playUser();
$dateNow = new DateTime();
$dateLast = new DateTime();

$db = new DBPDO();
$dbh = $db->connect2();
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$topRankingAll = $playUser->accuracyUserAll();
$topRankingNow = $playUser->accuracyUserNow();
$topRankingLastChamp = $playUser->accuracyUserLastChamp();
$topGoldAll = $playUser->goldUserAll();
$topGoldNow = $playUser->goldUserNow();
$topGoldLastChamp = $playUser->goldUserLastChamp();

if (isset($_REQUEST['exchange']) and isset($_SESSION['login'])) {
    $dataPlayUser = $playUser->checkUserPlay($_SESSION['login']['id']);
    if (!empty($dataPlayUser)) {
        if ($dataPlayUser['gold'] >= intval($_REQUEST['exchange']['gold'])) {
            $user_id = $_SESSION['login']['id'];
            $befor_gold = $_SESSION['login']['gold'];
            $befor_coin = $_SESSION['login']['coin'];
            $befor_coupon = $_SESSION['login']['coupon'];
            $befor_lv = $_SESSION['login']['lv'];
            $befor_star = $_SESSION['login']['star'];
            $income_derived = 'exchange';
            $income_type = 'coin';
            $income_format = 'plus';
            $after_gold = ($befor_gold - $_REQUEST['exchange']['gold']);
            $after_coin = ($befor_coin + ($_REQUEST['exchange']['gold'] * 100));
            $after_coupon = $befor_coupon;
            $after_lv = $_SESSION['login']['lv'];
            $after_star = $_SESSION['login']['star'];
            $comment = "ได้ทำการกด เเลก " . ($befor_gold - $after_gold) . " gold เป็น " . ($after_coin - $befor_coin) . "coin ";
            $sql = "INSERT INTO `log_income` (`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
            $sql .= " VALUES (" . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
            $sth = $dbh->prepare($sql)->execute();
            if ($sth) {
                $_SESSION['login']['coin'] = $after_coin;
                $_SESSION['login']['gold'] = $after_gold;
                $sql = "update `user` set coin=" . $after_coin . ",gold=" . $after_gold . " where id=" . $user_id;
                $sth = $dbh->prepare($sql)->execute();
                header('Location: /profile.php');
                exit();
            }
        }
    }
}
if (isset($_REQUEST['reset'])) {
    $dataPlayUser = $playUser->checkUserPlay($_SESSION['login']['id']);
    if (!empty($dataPlayUser)) {
        if ($dataPlayUser['gold'] >= intval($_REQUEST['exchange']['gold'])) {
            $user_id = $_SESSION['login']['id'];
            $befor_gold = $_SESSION['login']['gold'];
            $befor_coin = $_SESSION['login']['coin'];
            $befor_coupon = $_SESSION['login']['coupon'];
            $befor_lv = $_SESSION['login']['lv'];
            $befor_star = $_SESSION['login']['star'];
            $income_derived = 'reset';
            $income_type = 'gold';
            $income_format = 'plus';
            $after_gold = 0;
            $after_coin = $befor_coin;
            $after_coupon = $befor_coupon;
            $after_lv = $_SESSION['login']['lv'];
            $after_star = $_SESSION['login']['star'];
            $comment = "ได้ทำการกด เเลก Reset Gold";
            $sql = "INSERT INTO `log_income` (`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
            $sql .= " VALUES (" . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
            $sth = $dbh->prepare($sql)->execute();
            if ($sth) {
                $_SESSION['login']['coin'] = $after_coin;
                $_SESSION['login']['gold'] = $after_gold;
                $sql = "update `user` set coin=" . $after_coin . ",gold=" . $after_gold . " where id=" . $user_id;
                $sth = $dbh->prepare($sql)->execute();
                header('Location: /profile.php');
                exit();
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <title></title>
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="nanoscrollbar/nanoscrollbar.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/donutchart.js"></script>
    <script src="js/progressbar.js"></script>
    <script src="js/circleprogressbar.js"></script>
    <?php
    $user_id = ((isset($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : null);
    require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playBet.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playLogIncome.php";
    $playBet = new playBet();
    $playLogIncome = new playLogIncome();
    $dataStepIncome = false;
    $dataComboIncome = false;
    if (!empty($user_id)) {
        $dataPlayUser = $playUser->checkUserPlay($user_id);
        $dataPlayBet = $playBet->getWeekByUserId($user_id);
        $dataLogIncome = $playLogIncome->getByUser($user_id);
        $dateBetStatus = $playBet->checkStatusBetByUser($user_id);
        $dataCountStep = $playBet->checkBetByDayBefore($user_id);
        $dataBetComboContinuous = $playBet->checkBetComboContinuous($user_id);
    } else {
        $dataPlayUser = $playUser->checkUserPlay($_SESSION['login']['id']);
        $dataPlayBet = $playBet->getWeekByUserId($_SESSION['login']['id']);
        $dataLogIncome = $playLogIncome->getByUser($_SESSION['login']['id']);
        $dateBetStatus = $playBet->checkStatusBetByUser($_SESSION['login']['id']);
        $dataCountStep = $playBet->checkBetByDayBefore($_SESSION['login']['id']);
        $dataBetComboContinuous = $playBet->checkBetComboContinuous($_SESSION['login']['id']);
        $dataStepIncome = $playLogIncome->checkStepIncome($_SESSION['login']['id']);
        $dataComboIncome = $playLogIncome->checkComboIncome($_SESSION['login']['id']);
    }
    ?>
    <?php

    //    require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/DataMap.php";
    //    require_once $_SERVER["DOCUMENT_ROOT"] . "/model/RankM.php";
    //    require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/LogoManager.php";
    //    require_once $_SERVER["DOCUMENT_ROOT"] . "/model/StatementM.php";
    //
    //    $uid = $_REQUEST['fbuid'];
    //    if (empty($uid)) {
    //        header("index.php");
    //    }
    //
    //    $userm = new UserM();
    //    $datamap = new DataMap();
    //    $logoman = new LogoManager();
    //    $rankm = new RankM();
    //    $statem = new StatementM();

    //    $user = $userm->getUserPack($uid);
    //    $ranking = $rankm->getUserRankPack(0, 10);
    //    $betlist = $userm->getBetListPack($uid);
    //    $statement = $statem->getByUserPack($uid);
    //    $trophy = $userm->getTrophyPack($uid);

    //    $thisuser = $datamap->mapByKey($user['keylist']['user'], $user['user']);
    $userlegend = [5 => 'เด็กเก็บบอล', 20 => 'นักเตะฝึกหัด', 40 => 'ดาวรุ่งพุ่งแรง', 60 => 'นักเตะระดับเวิลด์คลาส', 80 => 'เทพแห่งวงการลูกหนัง', 100 => 'มหาเทพจุติ', 200 => 'นักเตะในตำนาน', 300 => 'ตำนานที่ยังมีชีวิตอยู่', 400 => 'มหาเทพในตำนาน'];
    ?>

    <?php include 'view/index/googletagmanager.php'; ?>
</head>
<body style="background-color: #f5f5f5">
<?php //include 'header.php'; ?>
<!--index menu-->
<?php include $_SERVER['DOCUMENT_ROOT'] . '/view/index/menu-all.php'; ?>
<!--<nav class="navbar navbar-menu-v2">-->
<!--    --><?php //include $_SERVER['DOCUMENT_ROOT'] . '/view/menu/navbar-menu-v2.php'; ?>
<!--</nav>-->

<div class="wrapper-news-articles">
    <div class="containter-fluid">
        <div class="bx-head-profile bgtop">
            <div class="hilight-cover-top"></div>
            <div class="articles-title">
                <div class="container">
                    <div class="head-title-profile pull-right">
                        <div>
                            <div class="bx-blue-bg">
                                <div><b class="countdown-sec">-</b></div>
                                <div>Seconds</div>
                            </div>
                            <div class="bx-blue-bg">
                                <div><b class="countdown-min">-</b></div>
                                <div>Minutes</div>
                            </div>
                            <div class="bx-blue-bg">
                                <div><b class="countdown-h">-</b></div>
                                <div>Hours</div>
                            </div>
                            <div class="bx-blue-bg">
                                <div><b class="countdown-d">-</b></div>
                                <div>Days</div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="pull-right" style="margin-top: 10px;"><strong>แจกรางวัลในอีก</strong></div>
                    </div>
                </div>
            </div>
            <!--            <div class="background-image_image3"></div>-->
        </div>
    </div>

    <div class="containter-fluid" style="max-width: 1800px;">

        <div class="col-sm-3 sidebar-profile">
            <div class="h-profile">
                <div class="bx-img-profile pull-left">
                    <div class="img-profile">
                        <?php if (!empty($dataPlayUser["fb_uid"])) { ?>
                            <img id="profile-pix"
                                 src="https://graph.facebook.com/v2.8/<?php echo $dataPlayUser["fb_uid"]; ?>/picture?type=large">
                        <?php } else if ($dataPlayUser["path"]) { ?>
                            <img id="profile-pix" src="<?php echo $dataPlayUser["path"]; ?>">
                        <?php } else { ?>
                            <img id="profile-pix" src="/images/avatar.png">
                        <?php } ?>
                    </div>
                </div>
                <div class="bx-line-profile">
                    <div class="profile-name">
                        <?php echo((!empty($dataPlayUser['name'])) ? $dataPlayUser['name'] : '-') ?>
                        <?php if (isset($_SESSION['login']) and ($dataPlayUser['id'] == $_SESSION['login']['id'])) { ?>
                            <button class="btn btn-default btn-xs"
                                    onclick="$('#myModalLabelRegister').html('Edit Profile');$('#btn_edit').removeClass('hide');$('#btn_register').addClass('hide');"
                                    data-toggle="modal" data-target="#myModalRegister"><span
                                        class="glyphicon glyphicon-pencil"></span></button>
                        <?php } ?>
                    </div>
                    <div class="pull-right">
                        <!--                        <button class="btn btn-mini"><i class="fa fa-pencil"></i> Edit Cover</button>-->
                        <!--                        <button class="btn btn-mini"><i class="fa fa-envelope-o"></i> Message</button>-->
                    </div>
                    <div style="clear:both;"></div>
                    <div class="second-name">
                        เด็กเก็บบอล
                    </div>
                    <div class="level">
                        <strong><?php echo "Level " . ((!empty($dataPlayUser['lv'])) ? $dataPlayUser['lv'] : '-'); ?></strong>
                    </div>
                </div>

                <div style="clear:both;"></div>
            </div>
            <div class="bx-white-border mg-bt15">
                <table class="table-coins">
                    <tr>
                        <td colspan="2" class="hide">
                            <table>
                                <tr>
                                    <td class="text-right"><img src="/images/coin/diamond.png"></td>
                                    <td class="text-left">
                                        <strong><?php echo number_format(((!empty($dataPlayUser['diamond'])) ? $dataPlayUser['diamond'] : 0), 2); ?></strong>
                                        <div>Diamond</div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-right:solid 1px #f5f5f5;">
                            <table>
                                <tr>
                                    <td class="text-right"><img src="/images/coin/sgold100.png"></td>
                                    <td class="text-left">
                                        <strong><?php echo number_format(((!empty($dataPlayUser['gold'])) ? $dataPlayUser['gold'] : 0), 2); ?></strong>
                                        <div>Sgold</div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td class="text-right"><img src="/images/coin/scoin100.png"></td>
                                    <td class="text-left">
                                        <strong><?php echo number_format(((!empty($dataPlayUser['coin'])) ? $dataPlayUser['coin'] : 0), 2); ?></strong>

                                        <div>Scoin</div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div class="bx-btn-change-coin">
                    <?php if (isset($_SESSION['login'])) { ?>
                        <?php if (empty($user_id) || (!empty($user_id) and $user_id == $_SESSION['login']['id'])) { ?>
                            <button class="btn btn-lg btn-primary show-exchange-box"><i class="fa fa-exchange"
                                                                                        aria-hidden="true"></i>
                                แลกเปลี่ยนเหรียญ
                            </button>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <div class="box-sidebar-ranking bx-white-border bx-raning-box mg-bt15">
                <div class="sidebar-style">
                    <h4 class="page-header">
                        <i class="fa fa-trophy" aria-hidden="true"></i> Top Ranking
                        <a href="/ranking.php" class="pull-right" style="margin-top: 10px;
    font-size: 75%;">ทั้งหมด</a>
                    </h4>
                    <div class="tab-top-rank-type">
                        <!--                            <div class="h-ranking">Top Ranking</div>-->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-top-rank" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#all-rank" aria-controls="home" role="tab"
                                   data-toggle="tab">All</a></li>
                            <li role="presentation">
                                <a href="#now-rank" aria-controls="profile" role="tab"
                                   data-toggle="tab">Now</a></li>
                            <li role="presentation">
                                <a href="#last-rank" aria-controls="messages" role="tab" data-toggle="tab">Last
                                    Champ</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="all-rank">
                                <div class="bx-list-ranking">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>Name</th>
                                            <th>PTA</th>
                                        </tr>
                                        </thead>
                                        <?php foreach ($topRankingAll as $key => $val) { ?>
                                            <tr>
                                                <td>
                                                    <div class="ranking-icon">
                                                        <span><?php echo $key + 1 ?></span>
                                                    </div>
                                                </td>
                                                <td style="width: 35px;"><img
                                                            src="https://graph.facebook.com/v2.8/<?php echo $val["fb_uid"]; ?>/picture?type=large">
                                                </td>
                                                <td>
                                                    <div
                                                            class="username-ranking"><a
                                                                style="color:#000000;"
                                                                href="/profile.php?user_id=<?php echo $val['id']; ?>"><?php echo $val['name']; ?></a>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div><?php echo number_format($val['accuracy'], 2); ?></div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="now-rank">
                                <div class="bx-list-ranking">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>Name</th>
                                            <th>PTA</th>
                                        </tr>
                                        </thead>
                                        <?php foreach ($topRankingNow as $key => $val) { ?>
                                            <tr>
                                                <td>
                                                    <div class="ranking-icon">
                                                        <span><?php echo $key + 1 ?></span></div>
                                                </td>
                                                <td style="width: 35px;"><img
                                                            src="https://graph.facebook.com/v2.8/<?php echo $val["fb_uid"]; ?>/picture?type=large">
                                                </td>
                                                <td>
                                                    <div
                                                            class="username-ranking"><a
                                                                style="color:#000000;"
                                                                href="/profile.php?user_id=<?php echo $val['id']; ?>"><?php echo $val['name']; ?></a>
                                                    </div>

                                                </td>
                                                <td>
                                                    <div><?php echo number_format($val['accuracy_half'], 2); ?></div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="last-rank">
                                <div class="bx-list-ranking">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>Name</th>
                                            <th>PTA</th>
                                        </tr>
                                        </thead>
                                        <?php foreach ($topRankingLastChamp as $key => $val) { ?>
                                            <tr>
                                                <td>
                                                    <div class="ranking-icon">
                                                        <span><?php echo $key + 1 ?></span></div>
                                                </td>
                                                <td style="width: 35px;"><img
                                                            src="https://graph.facebook.com/v2.8/<?php echo $val["fb_uid"]; ?>/picture?type=large">
                                                </td>
                                                <td>
                                                    <div
                                                            class="username-ranking"><a
                                                                style="color:#000000;"
                                                                href="/profile.php?user_id=<?php echo $val['id']; ?>"><?php echo $val['name']; ?></a>
                                                    </div>

                                                </td>
                                                <td>
                                                    <div><?php echo number_format($val['accuracy_month'], 2); ?></div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <a href="/ranking.php">
                            <div class="bt-view-all">View All Ranking <i class="fa fa-arrow-right"
                                                                         style="float:right;"></i></div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="box-sidebar-ranking bx-white-border bx-raning-box mg-bt15">
                <div class="sidebar-style">
                    <h4 class="page-header">
                        <i class="fa fa-trophy" aria-hidden="true"></i> Top Sgold
                        <a href="/ranking.php" class="pull-right" style="margin-top: 10px;
    font-size: 75%;">ทั้งหมด</a>
                    </h4>
                    <div class="tab-top-rank-type">
                        <!--                        <div class="h-ranking">Top Sgold</div>-->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-top-rank" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#all-rank-gold"
                                   aria-controls="home"
                                   role="tab"
                                   data-toggle="tab">All</a></li>
                            <li role="presentation"><a href="#now-rank-gold"
                                                       aria-controls="profile"
                                                       role="tab"
                                                       data-toggle="tab">Now</a></li>
                            <li role="presentation"><a href="#last-rank-gold"
                                                       aria-controls="messages"
                                                       role="tab" data-toggle="tab">Last
                                    Champ</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="all-rank-gold">
                                <div class="bx-list-ranking">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>Name</th>
                                            <th>PTA</th>
                                        </tr>
                                        </thead>
                                        <?php foreach ($topGoldAll as $key => $val) { ?>
                                            <tr>
                                                <td>
                                                    <div class="ranking-icon">
                                                        <span><?php echo $key + 1 ?></span></div>
                                                </td>
                                                <td style="width: 35px;"><img
                                                            src="https://graph.facebook.com/v2.8/<?php echo $val["fb_uid"]; ?>/picture?type=large">
                                                </td>
                                                <td>
                                                    <div
                                                            class="username-ranking"><a
                                                                style="color:#000000;"
                                                                href="/profile.php?user_id=<?php echo $val['id']; ?>"><?php echo $val['name']; ?></a>
                                                    </div>

                                                </td>
                                                <td>
                                                    <div><?php echo number_format($val['gold'], 2); ?></div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="now-rank-gold">
                                <div class="bx-list-ranking">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>Name</th>
                                            <th>PTA</th>
                                        </tr>
                                        </thead>
                                        <?php foreach ($topGoldNow as $key => $val) { ?>
                                            <tr>
                                                <td>
                                                    <div class="ranking-icon">
                                                        <span><?php echo $key + 1 ?></span></div>
                                                </td>
                                                <td style="width: 35px;"><img
                                                            src="https://graph.facebook.com/v2.8/<?php echo $val["fb_uid"]; ?>/picture?type=large">
                                                </td>
                                                <td>
                                                    <div
                                                            class="username-ranking"><a
                                                                style="color:#000000;"
                                                                href="/profile.php?user_id=<?php echo $val['id']; ?>"><?php echo $val['name']; ?></a>
                                                    </div>

                                                </td>
                                                <td>
                                                    <div><?php echo number_format($val['gold_half'], 2); ?></div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="last-rank-gold">
                                <div class="bx-list-ranking">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>Name</th>
                                            <th>PTA</th>
                                        </tr>
                                        </thead>
                                        <?php foreach ($topGoldLastChamp as $key => $val) { ?>
                                            <tr>
                                                <td>
                                                    <div class="ranking-icon">
                                                        <span><?php echo $key + 1 ?></span></div>
                                                </td>
                                                <td style="width: 35px;"><img
                                                            src="https://graph.facebook.com/v2.8/<?php echo $val["fb_uid"]; ?>/picture?type=large">
                                                </td>
                                                <td>
                                                    <div
                                                            class="username-ranking"><a
                                                                style="color:#000000;"
                                                                href="/profile.php?user_id=<?php echo $val['id']; ?>"><?php echo $val['name']; ?></a>
                                                    </div>

                                                </td>
                                                <td>
                                                    <div><?php echo number_format($val['gold_month'], 2); ?></div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <a href="/ranking.php">
                            <div class="bt-view-all">View All Ranking <i class="fa fa-arrow-right"
                                                                         style="float:right;"></i></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-9 content-profile">
            <div class="wrap-bx-trophy">
                <?php foreach ($trophy['trophy'] as $key => $val) { ?>
                    <?php $trop = $datamap->mapByKey($trophy['keylist']['trophy'], $val) ?>
                    <span title="<?php echo $trop['desc'] . '[' . $trop['type'] . ']'; ?>">
                                <img src="<?php echo $trop['picture']; ?>">
                                <div class="noti-trophy"><?php echo $trop['quantity']; ?></div>
                            </span>
                <?php } ?>
                <div style="clear:both;"></div>
            </div>
            <?php if (isset($_SESSION['login'])) { ?>
                <div class="section-exchange-coins hide">
                    <div class="col-md-12">
                        <h2 class="page-header"><i class="fa fa-exchange" aria-hidden="true"></i> แลกเปลี่ยนเหลียญ</h2>
                    </div>
                    <div class="col-sm-4">
                        <div class="thumbnail text-center">
                            <img src="images/icon/sgold100.png">
                            <h2><?php echo number_format(((!empty($dataPlayUser['gold'])) ? $dataPlayUser['gold'] : 0), 2); ?></h2>
                            <p>Sgold</p>
                            <!--                            box-info-coins-->

                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="thumbnail text-center">
                            <img src="images/icon/scoin100.png">
                            <h2><?php echo number_format(((!empty($dataPlayUser['coin'])) ? $dataPlayUser['coin'] : 0), 2); ?></h2>
                            <p>Scoin</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="thumbnail" style="height: 203px;">
                            <h4 class="page-header" style="margin-top: 15px;">
                                <i class="fa fa-refresh" aria-hidden="true"></i> Reset Sgold
                            </h4>
                            <p>กด Reset เพื่อเปลี่ยน Sgold ให้เป็น 0 ได้ทุกวันที่ 1, 16
                                ของทุกเดือน</p>
                            <div class="modal-footer">
                                <!--                            --><?php //if ($dateNow->format('j') == 1 || $dateNow->format('j') == 16) { ?>
                                <form id="reset-gold">
                                    <input type="hidden" name="reset[gold]" value="true">
                                    <input type="hidden" name="reset[day]" value="<?= $dateNow->format('j') ?>">
                                    <input type="hidden" name="reset[total]" value="<?= $dateNow->format('t') ?>">
                                    <button type="button" onclick="resetGold()"
                                            class="btn btn-lg btn-danger btn-reset-gold">
                                        <i class="fa fa-refresh" aria-hidden="true"></i> Reset Sgold
                                        <!--                                        <img src="images/icon/reset.png" style="width: 30px;"> Reset-->
                                    </button>
                                </form>
                                <!--                            --><?php //} ?>
                                <!--                            box-reset-coin-->

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="thumbnail">
                            <div class="tableExchanged">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>ประเภท</th>
                                        <th>เงื่อนไข</th>
                                        <th>แลกเปลี่ยน</th>
                                        <th>ได้รับ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><b>แลก Sgold เป็น Scoin</b><br>(<span>1</span> Sgold = <span>100</span>
                                            Scoin)
                                        </td>
                                        <td>
                                            <img src="images/icon/sgold30.png">
                                            <img src="images/icon/exchanged.png">
                                            <img src="images/icon/scoin30.png">
                                        </td>
                                        <td>
                                            <form>
                                                <select class="form-control" name="exchange[gold]"
                                                        id="gold-select-coin">
                                                    <!--                                                        <option value="0" selected="">เลือกจำนวน Sgold</option>-->
                                                    <option value="1">1</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="50">50</option>
                                                </select>
                                                <input onclick="confirm('ยืนยันที่ แลก Gold เป็น Coin ในอัตรา 1ต่อ100 ');this.form.submit();this.disabled=true;this.value='Loading…';"
                                                       type="submit" class="btn btn-primary" value="แลกเปลี่ยน">
                                            </form>
                                        </td>
                                        <td><img src="images/icon/scoin30.png"><b id="coin-received">0</b></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--                    กฏ กติกาการแลกรางวัล-->
                    <div class="hide col-sm-12">
                        <div class="thumbnail">
                            <h4 class="page-header">
                                <i class="fa fa-file-text" aria-hidden="true"></i> กฏ กติกาการแลกรางวัล
                            </h4>
                            <ul>
                                <li>ต้องเล่นเกมบอลเต็งก่อนจึงจะเล่นบอลสเต็ปได้</li>
                                <li>เลือกสเต็ปให้ครบ 5 คู่/วัน</li>
                                <li>**สเต็ปแตก ผลงานทั้ง 5 ตัว จะต้องเป็น W (Wตัวใหญ่หรือชนะเต็ม) เท่านั้น</li>
                                <li>**ไม่สามารถแก้ไขเปลี่ยนทีมที่เลือกเล่นในแต่ละวันได้
                                    ควรตรวจสอบให้มั่นใจก่อนจะกดบันทึก
                                </li>
                                <li>หากผู้รับรางวัลมีบัญชีธนาคารไม่ตรงกับทางเว็บ (กสิกรไทย) จะหักค่าโอนปลายทาง</li>
                            </ul>
                            <div class="detail-conditions">

                            </div>

                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="section-stat">
                <div class="mg-bt15">
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <div class="bx-white-border" style="height: 324px;">
                                <h4 class="page-header">
                                    <i class="fa fa-pie-chart" aria-hidden="true"></i> สถิติการเล่นเกมส์
                                </h4>
                                <div class="stat-games text-center">
                                    <div id="result-circle"></div>
                                </div>
                                <div class="col-md-12">
                                    <table class="table-stat-number">
                                        <tr>
                                            <td><span style="color: #0091c8;"><i class="fa fa-circle"></i></span></td>
                                            <td>จำนวนคู่ที่ชนะทั้งหมด (Win)</td>
                                            <td>
                                                <div id="data-win"
                                                     style="color: #0091c8;"><?php echo number_format(((!empty($dateBetStatus['cheap'])) ? $dateBetStatus['cheap'][0] : 0), 2); ?>
                                                    คู่
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span style="color: #e40520;"><i class="fa fa-circle"></i></span></td>
                                            <td>จำนวนคู่ที่แพ้ทั้งหมด (Lose)</td>
                                            <td>
                                                <div style="color: #e40520;"
                                                     id="data-lose"><?php echo number_format(((!empty($dateBetStatus['wrong'])) ? $dateBetStatus['wrong'][0] : 0), 2); ?>
                                                    คู่
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span style="color: #888;"><i class="fa fa-circle"></i></span></td>
                                            <td>จำนวนคู่ที่เสมอทั้งหมด (Draw)</td>
                                            <td>
                                                <div style="color: #888;"
                                                     id="data-draw"><?php echo number_format(((!empty($dateBetStatus['always'])) ? $dateBetStatus['always'][0] : 0), 2); ?>
                                                    คู่
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!--                                <table class="stat-games">-->
                                <!--                                    <tr>-->
                                <!--                                        <td style="width: 50%;">-->
                                <!--<!--                                            <div id="result-circle"></div>-->
                                <!--                                        </td>-->
                                <!--                                        <td>-->
                                <!--                                            <table class="table-stat-number">-->
                                <!--                                                <tr>-->
                                <!--                                                    <td>-->
                                <!--                                                        <div id="data-win">-->
                                <?php //echo number_format(((!empty($dateBetStatus['cheap'])) ? $dateBetStatus['cheap'][0] : 0), 2); ?><!--</div>-->
                                <!--                                                        Win-->
                                <!--                                                    </td>-->
                                <!--                                                    <td><span style="color: #0091c8;"><i-->
                                <!--                                                                    class="fa fa-circle"></i></span></td>-->
                                <!--                                                </tr>-->
                                <!--                                                <tr>-->
                                <!--                                                    <td>-->
                                <!--                                                        <div id="data-lose">-->
                                <?php //echo number_format(((!empty($dateBetStatus['wrong'])) ? $dateBetStatus['wrong'][0] : 0), 2); ?><!--</div>-->
                                <!--                                                        Lose-->
                                <!--                                                    </td>-->
                                <!--                                                    <td><span style="color: #e40520;"><i-->
                                <!--                                                                    class="fa fa-circle"></i></span></td>-->
                                <!--                                                </tr>-->
                                <!--                                                <tr>-->
                                <!--                                                    <td>-->
                                <!--                                                        <div id="data-draw">-->
                                <?php //echo number_format(((!empty($dateBetStatus['always'])) ? $dateBetStatus['always'][0] : 0), 2); ?><!--</div>-->
                                <!--                                                        Draw-->
                                <!--                                                    </td>-->
                                <!--                                                    <td><span style="color: #888;"><i-->
                                <!--                                                                    class="fa fa-circle"></i></span>-->
                                <!--                                                    </td>-->
                                <!--                                                </tr>-->
                                <!--                                            </table>-->
                                <!--                                        </td>-->
                                <!--                                    </tr>-->
                                <!--                                </table>-->
                                <div style="clear: both;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <div class="bx-white-border" style="height: 324px;">
                                <h4 class="page-header">
                                    <i class="fa fa-pie-chart" aria-hidden="true"></i> ความแม่นยำ
                                </h4>
                                <!--                                    <div class="tab-h-game text-center" style="border-bottom: solid 1px #eee;">-->
                                <!--                                        คะแนนการเล่นเกมส์-->
                                <!--                                    </div>-->
                                <div class="stat-games text-center">
                                    <div id="accuracy-rate"></div>
                                    <div
                                            class="info">
                                        <?php
                                        $totalWinLose = (((!empty($dateBetStatus['cheap'])) ? $dateBetStatus['cheap'][0] : 0) + ((!empty($dateBetStatus['wrong'])) ? $dateBetStatus['wrong'][0] : 0));
                                        $cheap = ((!empty($dateBetStatus['cheap'])) ? $dateBetStatus['cheap'][0] : 0);
                                        ?>
                                        <span class="hide"
                                              id="data-accuracy-rate"><?php echo number_format(100 - ((($totalWinLose - $cheap) / $totalWinLose) * 100)) ?></span>
                                    </div>
                                    <strong>% ความแม่นยำ</strong>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <div class="well" style="height: 324px; padding-top: 70px; margin: 0; text-align: center;">
                                <a href="/videos.php" title="เกมทายผล /ฟุตบอลไฮท์ไลท์ HD">
                                    <img style="width: 170px; margin-bottom: 20px;" src="/images/banner-hd_preview.png">
                                </a>
                                <a href="/videos.php" title="เกมทายผล /ฟุตบอลไฮท์ไลท์ HD">
                                    <img style="width: 170px;" src="/images/banner-game.png">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
                <div class="rows mg-bt15">
                    <div class="col-sm-6">
                        <div class="bx-white-border" style="background-color: #39444e; color: #fff;">
                            <!--                            <div class="tab-h-game text-center" style="border-bottom: solid 1px #202425;">-->
                            <!--                               -->
                            <!--                            </div>-->
                            <div class="page-header">
                                <h4>
                                    สถิติการ Win/Loss ต่อเนื่อง(Step)
                                </h4>
                            </div>
                            <div class="bx-level-step">
                                <?php if ($dataCountStep['countStep'] < 3) { ?>
                                    <img src="<?php echo "/images/step/step.png"; ?>">
                                <?php } else { ?>
                                    <img src="<?php echo "/images/step/step" . $dataCountStep['countStep'] . ".png"; ?>">
                                <?php } ?>
                            </div>
                            <table>
                                <tr>
                                    <td><?php echo((!empty($dataCountStep['statusStep']) ? (($dataCountStep['statusStep'] == "cheap") ? 'ชนะ' : 'แพ้') : '-')) ?>
                                        บอลชุด <?php echo(!empty($dataCountStep['countStep']) ? $dataCountStep['countStep'] : '-'); ?>
                                        ทีม
                                    </td>
                                    <td class="text-right">
                                        <?php if ($dataStepIncome and $dataCountStep['countStep'] >= 3) { ?>
                                            <a href="/play/exchangeStep.php"
                                               onclick="return confirm('ยืนยันการ รับ Step <?php echo $dataCountStep['countStep']; ?> คู่')"
                                               style="cursor: pointer;color: #FFFFFF;"
                                               class="btn-step btn-combo-step hide">แลก Step</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="bx-white-border" style="background-color: #034c8c; color: #fff;">
                            <!--                            <div class="tab-h-game text-center" style="border-bottom: solid 1px #02335c;">-->
                            <!--                               -->
                            <!--                            </div>-->
                            <div class="page-header">
                                <h4>
                                    สถิติการ Win/Loss รายวันต่อเนื่อง (Combo)
                                </h4>
                            </div>
                            <div class="bx-level-step">
                                <div class="progress" style="margin-bottom: 0; background-color: #02335c;">
                                    <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="60"
                                         aria-valuemin="0" aria-valuemax="100"
                                         style="width: <?php echo(!empty($dataBetComboContinuous['countCombo']) ? (100 - (((30 - $dataBetComboContinuous['countCombo']) / 30) * 100)) : 0); ?>%;">
                                        <span class="sr-only">60% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <table>
                                <tr>
                                    <td>
                                        <?php echo(!empty($dataBetComboContinuous['statusCombo']) ? (($dataBetComboContinuous['statusCombo'] == "cheap") ? 'ชนะ' : 'แพ้') : '-'); ?>
                                        ต่อเนื่อง <?php echo(!empty($dataBetComboContinuous['countCombo']) ? ($dataBetComboContinuous['countCombo']) : '-'); ?>
                                        คู่
                                    </td>
                                    <td>
                                        Combo แลกได้(4,7,10,15,20)
                                    </td>
                                    <td class="text-right">
                                        <span style="display: none;"><?php var_dump($dataComboIncome) ?></span>
                                        <?php if ($dataComboIncome and !empty($dataBetComboContinuous['countCombo']) and $dataBetComboContinuous['countCombo'] >= 2) { ?>
                                            <?php if ((intval(date("H")) + 7) >= 12 and (intval(date("H")) + 7) <= 15 and ($dataBetComboContinuous['countCombo'] == 4 || $dataBetComboContinuous['countCombo'] == 7 || $dataBetComboContinuous['countCombo'] == 10 || $dataBetComboContinuous['countCombo'] == 15 || $dataBetComboContinuous['countCombo'] == 20)) { ?>
                                                <a href="/play/exchangeCombo.php"
                                                   onclick="return confirm('ยืนยันการ รับ Combo <?php echo $dataBetComboContinuous['countCombo']; ?> คู่ต่อเนื่อง')"
                                                   style="cursor: pointer;color: #FFFFFF;"
                                                   class="btn-step btn-combo-day" style="background-color: #02335c;">แลก
                                                    Combo</a>
                                            <?php } ?>
                                        <?php } ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                </div>

                <div class="rows mg-bt15">
                    <div class="col-sm-12">
                        <div class="thumbnail">
                            <div class="page-header" style="padding-bottom: 0;">
                                <h4 class="pull-left">
                                    <i class="fa fa-gamepad" aria-hidden="true"></i>
                                    คู่บอลที่เล่นเกมส์
                                </h4>
                                <div class="pull-right tab-filter">
                                    <table>
                                        <tr>
                                            <td>Filter by Win & Lose</td>
                                            <td>
                                                <select id="betview-filter" class="btn btn-default btn-xs">
                                                    <option value="view-all" selected="">All</option>
                                                    <option value="view-win">Win</option>
                                                    <option value="view-lose">Lose</option>
                                                    <option value="view-draw">Draw</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="clear: both;"></div>
                            </div>


                            <!--                            <div class="tab-filter mg-bt15" style="border-bottom: solid 1px #eee;">-->
                            <!--                                <div class="pull-left">-->
                            <!--                                    <div class="tab-h-game">คู่บอลที่เล่นเกมส์</div>-->
                            <!--                                </div>-->
                            <!--                                <div class="pull-right">-->
                            <!--                                    -->
                            <!--                                </div>-->
                            <!--                                <div style="clear:both;"></div>-->
                            <!--                            </div>-->
                            <div class="bx-stat-played" style="display: none;">
                                <table class="table table-striped">
                                    <tr>
                                        <td><span class="info">Deportivo La Coruna (1.9)</span> <span>- VS -</span>
                                            <span>Espanyol(1.9)</span></td>
                                        <td>HDP [-0.25]</td>
                                        <td>เลือกเล่น @<span class="info">Espanyol</span></td>
                                        <td><span class="info">0 - 0</span></td>
                                        <td><img src="/images/icon-stat/win.png"></td>
                                    </tr>
                                    <tr>
                                        <td><span class="info">Deportivo La Coruna (1.9)</span> <span>- VS -</span>
                                            <span>Espanyol(1.9)</span></td>
                                        <td>HDP [-0.25]</td>
                                        <td>เลือกเล่น @<span class="info">Espanyol</span></td>
                                        <td><span class="info">0 - 0</span></td>
                                        <td><img src="/images/icon-stat/win.png"></td>
                                    </tr>
                                    <tr>
                                        <td><span class="info">Deportivo La Coruna (1.9)</span> <span>- VS -</span>
                                            <span>Espanyol(1.9)</span></td>
                                        <td>HDP [-0.25]</td>
                                        <td>เลือกเล่น @<span class="info">Espanyol</span></td>
                                        <td><span class="info">0 - 0</span></td>
                                        <td><img src="/images/icon-stat/win.png"></td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                                <!-- Nav tabs -->
                                <!--                                nav-tabs-games-->
                                <ul class="nav nav-tabs nav-top-rank" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#all-bet-tab" aria-controls="home" role="tab" data-toggle="tab">All</a>
                                    </li>
                                    <?php foreach ($dataPlayBet as $key => $value) { ?>
                                        <li role="presentation">
                                            <a href="#date-<?php echo $key; ?>" aria-controls="profile" role="tab"
                                               data-toggle="tab"><?php echo date("d M Y", strtotime($key)); ?>
                                                (<?php echo count($value) ?>)</a>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <!--                                <style>-->
                                <!--                                    .info {-->
                                <!--                                        font-weight: bolder;-->
                                <!--                                    }-->
                                <!--                                </style>-->
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="all-bet-tab">
                                        <div class="wr-box-result">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>คู่บอล</th>
                                                    <th>HDP</th>
                                                    <th></th>
                                                    <th>Score</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <?php foreach ($dataPlayBet as $key => $value) { ?>
                                                    <?php foreach ($value as $keyB => $valB) { ?>
                                                        <?php
                                                        $viewbet = "view-bet";
                                                        if ($valB['status'] == "cheap") {
                                                            $viewbet = "view-win";
                                                        } else if ($valB['status'] == "wrong") {
                                                            $viewbet = "view-lose";
                                                        } else {
                                                            $viewbet = "view-draw";
                                                        }
                                                        ?>
                                                        <tr class="view-bet" id="match-<?php echo $valB['match_id'] ?>"
                                                            viewtype="<?php echo $viewbet; ?>">
                                                            <td>
                                                                <div style="width: 400px;">
                                                                    <span class="<?php echo (floatval($valB['handicap']) < 0) ? 'info' : ''; ?>"><?php echo $valB['thm']; ?>
                                                                        (<?php echo $valB['home_water_bill']; ?>)</span>
                                                                    <span>- VS -</span>
                                                                    <span class="<?php echo (floatval($valB['handicap']) > 0) ? 'info' : ''; ?>"><?php echo $valB['tam']; ?>
                                                                        (<?php echo $valB['away_water_bill']; ?>)</span>
                                                                </div>
                                                            </td>
                                                            <td>HDP [<span
                                                                        class="info"><?php echo $valB['handicap']; ?></span>]
                                                            </td>
                                                            <td>เลือกเล่น @
                                                                <span class="info">
                                                                        <?php echo ($valB['team'] == 'home') ? $valB['thm'] : $valB['tam']; ?>
                                                                    </span></td>
                                                            <td>
                                                                <span class="info"><?php echo ((!empty($valB['home_end_time_score'])) ? $valB['home_end_time_score'] : '0') . " - " . ((!empty($valB['away_end_time_score'])) ? $valB['away_end_time_score'] : '0'); ?></span>
                                                            </td>
                                                            <td>
                                                                <?php if ($valB['status'] == "bet") { ?>
                                                                    <img src="/images/icon-stat/betlist_result/bet.png">
                                                                <?php } elseif ($valB['status'] == "cheap" && ((($valB['home_end_time_score'] + $valB['handicap']) - $valB['away_end_time_score']) == (0.25) || (($valB['home_end_time_score'] + $valB['handicap']) - $valB['away_end_time_score']) == (-0.25))) { ?>
                                                                    <img src="/images/icon-stat/betlist_result/win_half.png">
                                                                <?php } elseif ($valB['status'] == "cheap") { ?>
                                                                    <img src="/images/icon-stat/betlist_result/win.png">
                                                                <?php } elseif ($valB['status'] == "wrong" && ((($valB['home_end_time_score'] + $valB['handicap']) - $valB['away_end_time_score']) == (0.25) || (($valB['home_end_time_score'] + $valB['handicap']) - $valB['away_end_time_score']) == (-0.25))) { ?>
                                                                    <img src="/images/icon-stat/betlist_result/lose_half.png">
                                                                <?php } elseif ($valB['status'] == "wrong") { ?>
                                                                    <img src="/images/icon-stat/betlist_result/lose.png">
                                                                <?php } elseif ($valB['status'] == "always") { ?>
                                                                    <img src="/images/icon-stat/betlist_result/draw.png">
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                <?php } ?>
                                            </table>
                                        </div>
                                    </div>

                                    <?php foreach ($dataPlayBet as $key => $value) { ?>
                                        <div role="tabpanel" class="tab-pane" id="date-<?php echo $key; ?>">
                                            <div class="wr-box-result">
                                                <table class="table table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>คู่บอล</th>
                                                        <th>HDP</th>
                                                        <th></th>
                                                        <th>Score</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <?php foreach ($value as $keyB => $valB) { ?>
                                                        <?php
                                                        $viewbet = "view-bet";
                                                        if ($valB['status'] == "cheap") {
                                                            $viewbet = "view-win";
                                                        } else if ($valB['status'] == "wrong") {
                                                            $viewbet = "view-lose";
                                                        } else {
                                                            $viewbet = "view-draw";
                                                        }
                                                        ?>
                                                        <tr class="view-bet" id="match-<?php echo $valB['match_id'] ?>"
                                                            viewtype="<?php echo $viewbet; ?>">
                                                            <td>
                                                                <div style="width: 400px;">
                                                                    <span class="<?php echo (floatval($valB['handicap']) < 0) ? 'info' : ''; ?>"><?php echo $valB['thm']; ?>
                                                                        (<?php echo $valB['home_water_bill']; ?>)</span>
                                                                    <span>- VS -</span>
                                                                    <span class="<?php echo (floatval($valB['handicap']) > 0) ? 'info' : ''; ?>"><?php echo $valB['tam']; ?>
                                                                        (<?php echo $valB['away_water_bill']; ?>)</span>
                                                                </div>
                                                            </td>
                                                            <td>HDP [<span
                                                                        class="info"><?php echo $valB['handicap']; ?></span>]
                                                            </td>
                                                            <td>เลือกเล่น @
                                                                <span class="info">
                                                                        <?php echo ($valB['team'] == 'home') ? $valB['thm'] : $valB['tam']; ?>
                                                                    </span></td>
                                                            <td>
                                                                <span class="info"><?php echo ((!empty($valB['home_end_time_score'])) ? $valB['home_end_time_score'] : '0') . " - " . ((!empty($valB['away_end_time_score'])) ? $valB['away_end_time_score'] : '0'); ?></span>
                                                            </td>
                                                            <td>
                                                                <?php if ($valB['status'] == "bet") { ?>
                                                                    <img src="/images/icon-stat/betlist_result/bet.png">
                                                                <?php } elseif ($valB['status'] == "cheap" && ((($valB['home_end_time_score'] + $valB['handicap']) - $valB['away_end_time_score']) == (0.25) || (($valB['home_end_time_score'] + $valB['handicap']) - $valB['away_end_time_score']) == (-0.25))) { ?>
                                                                    <img src="/images/icon-stat/betlist_result/win_half.png">
                                                                <?php } elseif ($valB['status'] == "cheap") { ?>
                                                                    <img src="/images/icon-stat/betlist_result/win.png">
                                                                <?php } elseif ($valB['status'] == "wrong" && ((($valB['home_end_time_score'] + $valB['handicap']) - $valB['away_end_time_score']) == (0.25) || (($valB['home_end_time_score'] + $valB['handicap']) - $valB['away_end_time_score']) == (-0.25))) { ?>
                                                                    <img src="/images/icon-stat/betlist_result/lose_half.png">
                                                                <?php } elseif ($valB['status'] == "wrong") { ?>
                                                                    <img src="/images/icon-stat/betlist_result/lose.png">
                                                                <?php } elseif ($valB['status'] == "always") { ?>
                                                                    <img src="/images/icon-stat/betlist_result/draw.png">
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </table>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                </div>


                <div class="col-sm-12">
                    <div class="thumbnail">
                        <div class="bx-white-border">
                            <h4 class="page-header">
                                <i class="fa fa-file-text" aria-hidden="true"></i> Statement
                            </h4>
                            <!--                            <div class="tab-h-game">-->
                            <!--                                Statement-->
                            <!--                            </div>-->
                            <div class="tableStatement">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>วันที่/เวลา</th>
                                        <th>รายการ</th>
                                        <th class="text-center" colspan="2">กิจกรรม</th>
                                        <!--                                                <th colspan="2">ออก</th>-->
                                        <th class="text-center" colspan="2">คงเหลือ</th>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                        <td style="text-align: center;" class="text-Scoin"><b>Scoin</b></td>
                                        <td style="text-align: center;" class="text-Sgold"><b>Sgold</b></td>
                                        <!--                                                <td class="text-Sgold"><b>Sgold</b></td>-->
                                        <!--                                                <td class="text-Scoin"><b>Scoin</b></td>-->
                                        <td style="text-align: center;" class="text-Scoin"><b>Scoin</b></td>
                                        <td style="text-align: center;" class="text-Sgold"><b>Sgold</b></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($dataLogIncome as $key => $val) { ?>
                                        <tr class="TrLogIncome"
                                            data-income_type="<?php echo $val['income_type']; ?>"
                                            data-match_id="<?php echo $val['match_id']; ?>"
                                            data-income_format="<?php echo $val['income_format']; ?>">
                                            <td><?php echo date("d M Y", strtotime($val['date_record'])); ?></td>
                                            <td>
                                                <div style="width: 400px;"><?= $val['comment'] ?></div>
                                            </td>
                                            <td style="text-align: center;<?= ((($val['befor_coin'] - $val['after_coin']) != 0) ? (($val['income_format'] == "minus") ? 'color: #a94442;' : 'color: #3c763d;') : '') ?>">
                                                <?= ((($val['befor_coin'] - $val['after_coin']) != 0) ? ((($val['befor_coin'] - $val['after_coin']) > 0) ? '' : '+') : '') ?><?= ((($val['befor_coin'] - $val['after_coin']) == 0) ? '-' : number_format(($val['after_coin'] - $val['befor_coin']))) ?>
                                            </td>
                                            <td style="text-align: center;<?= ((($val['befor_gold'] - $val['after_gold']) != 0) ? ((($val['befor_gold'] - $val['after_gold']) > 0) ? 'color: #a94442;' : 'color: #3c763d;') : '') ?>">
                                                <?= ((($val['befor_gold'] - $val['after_gold']) != 0) ? ((($val['befor_gold'] - $val['after_gold']) > 0) ? '' : '+') : '') ?><?= ((($val['befor_gold'] - $val['after_gold']) == 0) ? '-' : number_format(($val['after_gold'] - $val['befor_gold']))) ?>
                                            </td>
                                            <td style="text-align: center;">
                                                <?= number_format($val['after_coin']) ?>
                                            </td>
                                            <td style="text-align: center;">
                                                <?= number_format($val['after_gold']) ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>

<?php
include 'view/index/footer.php';
?>

<script>
    function getCountDown() {
        $.ajax({
            url: "/services/getcountdown.php",
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
            var eventTime = response.timestamp;
            var currentTime = response.today_timestamp;
            var diffTime = eventTime - currentTime;
            var duration = moment.duration(diffTime * 1000, 'milliseconds');
            var interval = 1000;

            setInterval(function () {
                duration = moment.duration(duration - interval, 'milliseconds');
                //$('.countdown').text(duration.hours() + ":" + duration.minutes() + ":" + duration.seconds())
                //console.log(duration.days(), duration.hours() + ":" + duration.minutes() + ":" + duration.seconds());
                $('.countdown-sec').text(duration.seconds());
                $('.countdown-min').text(duration.minutes());
                $('.countdown-h').text(duration.hours());
                $('.countdown-d').text(duration.days());
            }, interval);
        });
    }

    function resetGold() {
//        ;this.form.submit();this.disabled=true;
        var day = $('form#reset-gold').find('input[name="reset[day]"]').val();
        var total = $('form#reset-gold').find('input[name="reset[total]"]').val();
        if (day == 1 || day == 16) {
            $('form#reset-gold').submit();
        } else {
            if (day > 16) {
                alert("คุณสามารถ Reset Gold ได้ในอีก " + (total - day) + " วัน")
            } else {
                alert("คุณสามารถ Reset Gold ได้ในอีก " + (16 - day) + " วัน")
            }

        }

        console.log($('form#reset-gold').find('input[name="reset[day]"]').val());
    }
    $(document).ready(function () {
        getCountDown();
        $("tr.TrLogIncome")
            .mouseover(function () {
                $('.table-striped tr').removeAttr('style');
                var match_id = $(this).attr('data-match_id');
                if ($(this).attr('data-income_type') == "coin&coupon") {
                    $('tr#match-' + match_id).css('background-color', '#ffeeba');
                } else if ($(this).attr('data-income_format') == "minus") {
                    $('tr#match-' + match_id).css('background-color', '#f2dede');
                } else if ($(this).attr('data-income_format') == "plus") {
                    $('tr#match-' + match_id).css('background-color', '#dff0d8');
                }
            })
            .mouseout(function () {
                $('.table-striped tr').removeAttr('style');
            });

        $(document).on('click', '.show-exchange-box', function (e) {
            if ($('.section-exchange-coins').hasClass('hide')) {
                $('.section-exchange-coins').removeClass('hide');
            } else {
                $('.section-exchange-coins').addClass('hide');
            }
        });

        $(document).on('change', '#betview-filter', function (e) {
            var viewtype = $(e.currentTarget).val();
            console.log(viewtype);
            if (viewtype == "view-all") {
                $(".view-bet").removeClass('hide');
            } else {
                $(".view-bet").addClass('hide');
                $(".view-bet[viewtype=" + viewtype + "]").removeClass('hide');
            }
        });

        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart(rsdata) {
            var rsdata = [["result", "value"], ["draw", parseInt($('#data-draw').html())], ["lose", parseInt($('#data-lose').html())], ["win", parseInt($('#data-win').html())]];
            var data = google.visualization.arrayToDataTable(rsdata);
            var options = {
                pieStartAngle: 0,
                chartArea: {top: 7, height: '90%', width: '100%'},
                height: '180',
                width: '203',
                colors: ['#888', '#e40520', '#0091c8'],
                pieHole: 0.5,
                legend: 'none',
            };
            var chart = new google.visualization.PieChart(document.getElementById('result-circle'));
            chart.draw(data, options);
        }

        circleProgressCustom('#accuracy-rate', parseInt($('#data-accuracy-rate').html()), '#ccc', '#0091c8', 4, 4, parseInt($('#data-accuracy-rate').html()) + '%');
    });

    $(".nano").nanoScroller();
</script>

<!--<script src="js/profile.js"></script>-->
</body>
</html>
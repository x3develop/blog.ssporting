<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="nanoscrollbar/nanoscrollbar.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/timeview.js"></script>
    <script src="js/game.js"></script>
</head>
<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/MatchM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/OddsM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/LogoManager.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/utility/DataMap.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/NewsM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/RankM.php";

$mid = ($_REQUEST["mid"]) ? $_REQUEST['mid'] : 0;
$matchm = new MatchM();
$logoman = new LogoManager();
$oddsm = new  OddsM();
$datamap = new DataMap();
$newsm = new NewsM();
$rankm = new RankM();
$gamedata = $matchm->getGameByMidPack($mid);
if (empty($gamedata["match"])) {
    header("fixtures.php");
}
@session_start();
$recommendodds = $oddsm->getRecommendOddsPack(20);
$teamguide = $matchm->getTeamGuidePack($mid);
$ranking = $rankm->getUserRankPack();
$betside = $matchm->betCountPack($mid);
$sysconfig = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/system_config/file.json'), true);
$ownbet = [];
if (!empty($_SESSION['fb_uid'])) {
    $matchbet = $matchm->getMatchBetPack($mid, $_SESSION['fb_uid']);
    $ownbet = $datamap->mapByKey($matchbet['keylist']['bet'], $matchbet['bet']);
}

?>
<body>
<!--<div id="fb-root"></div>-->
<!--<div id="fb-root"></div>-->
<!--<div id="fb-root"></div>-->
<!--<script>(function (d, s, id) {-->
<!--        var js, fjs = d.getElementsByTagName(s)[0];-->
<!--        if (d.getElementById(id)) return;-->
<!--        js = d.createElement(s);-->
<!--        js.id = id;-->
<!--        js.src = "//connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v2.8";-->
<!--        fjs.parentNode.insertBefore(js, fjs);-->
<!--    }(document, 'script', 'facebook-jssdk'));</script>-->
<?php //include 'header.php'; ?>
<!--index menu-->
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu.php'; ?>
<nav class="navbar navbar-menu-v2">
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/view/menu/navbar-menu-v2.php'; ?>
</nav>
<?php $thismatch = $datamap->mapByKey($gamedata['keylist']['match'], $gamedata['match']); ?>
<input id="game-info" type="hidden" mid="<?php echo $thismatch['mid']; ?>" lid="<?php echo $thismatch['lid']; ?>"
       hid="<?php echo $thismatch['hid']; ?>" gid="<?php echo $thismatch['gid']; ?>"
       c0="<?php echo $thismatch['c0']; ?>" sid="<?php echo $thismatch['sid']; ?>"
       choose="<?php echo (!empty($ownbet)) ? $ownbet['choose'] : ''; ?>"
       displayname="<?php echo (!empty($ownbet)) ? $ownbet['display_name'] : ''; ?>"/>

<div class="wrapper-news-articles">
    <div class="container">
        <div class="header-game">
            <div class="content-header">
                <div><strong>เล่นเกมส์ทายผลฟุตบอล จัดลำดับ</strong></div>
                <div><span>พร้อมแจกของรางวัลมากมายทุกอาทิตย์</span></div>
                <div style="margin: 10px 0px 10px 0px;">
                    <div class="bx-radius-o">
                        <div><b class="countdown-sec">-</b></div>
                        <div>Seconds</div>
                    </div>
                    <div class="bx-radius-o">
                        <div><b class="countdown-min">-</b></div>
                        <div>Minutes</div>
                    </div>
                    <div class="bx-radius-o">
                        <div><b class="countdown-h">-</b></div>
                        <div>Hours</div>
                    </div>
                    <div class="bx-radius-o">
                        <div><b class="countdown-d">-</b></div>
                        <div>Days</div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
                <div><strong>แจกรางวัลในอีก</strong></div>
            </div>
            <img src="/images/header-game.png" style="width: 100%;">
        </div>
        <div>
            <div class="rows" style="margin-top: 3px;">
                <div class="col-sm-9">
                    <table class="table-games table-games2">

                        <tr>
                            <td>
                                <div class="bx-head-team">
                                    <div
                                        class="<?php echo (!empty($ownbet) && $ownbet['choose'] == 'home') ? '' : 'hilight-cover'; ?>"></div>
                                    <?php if (!empty($ownbet) && $ownbet['choose'] == 'home') { ?>
                                        <div class="vote-already">VOTE</div>
                                    <?php } ?>
                                    <div class="crop-team"></div>
                                    <div class="bx-logo-team"><img
                                            src="<?php echo $logoman->getLogo($thismatch['hid'], 2); ?>"></div>
                                </div>
                            </td>
                            <td>
                                <div class="bx-head-team">
                                    <div
                                        class="<?php echo (!empty($ownbet) && $ownbet['choose'] == 'away') ? '' : 'hilight-cover'; ?>"></div>
                                    <?php if (!empty($ownbet) && $ownbet['choose'] == 'away') { ?>
                                        <div class="vote-already">VOTE</div>
                                    <?php } ?>
                                    <div class="crop-team imgTest2"></div>
                                    <div class="bx-logo-team"><img
                                            src="<?php echo $logoman->getLogo($thismatch['gid'], 2); ?>"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="wrap-vote">
                                    <div class="bx-hdp">
                                        HDP
                                        <?php
                                        $mhdp = "";
                                        $hhdp = "";
                                        $ghdp = "";
                                        if (!empty($gamedata["odds"])) {
                                            $mhdp = $gamedata["odds"][2];
                                            $hhdp = $gamedata["odds"][3];
                                            $ghdp = $gamedata["odds"][4];
                                        } ?>
                                        <ul>
                                            <li id="hdp_home_val">
                                                <?php
                                                if (!empty($gamedata["odds"])) {
                                                    echo $gamedata["odds"][3];
                                                }
                                                ?>
                                            </li>
                                            <?php
                                            $oddstype = "none";
                                            if (!empty($gamedata["odds"])) {
                                                $oddstype = $gamedata["odds"][1];
                                            }
                                            ?>
                                            <li id="hdp_val" oddstype="<?php echo $oddstype; ?>" class="bg-info">
                                                <?php
                                                if (!empty($gamedata["odds"])) {
                                                    echo $gamedata["odds"][2];
                                                }
                                                ?>
                                            </li>
                                            <li id="hdp_away_val">
                                                <?php
                                                if (!empty($gamedata["odds"])) {
                                                    echo $gamedata["odds"][4];
                                                }
                                                ?>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="bx-division">
                                        <?php
                                        $showdate = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $thismatch['date']);
                                        $showdate->addHours(7);
                                        ?>
                                        <?php echo $gamedata['match'][38]; ?> <label
                                            class="info"><?php echo $showdate->format("H:i"); ?></label>
                                    </div>
                                    <div class="bx-vote">
                                        <?php if (empty($ownbet)) { ?>
                                            <h3>เลือกทีมทายผล</h3>
                                        <?php } ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <style>
                                div.disabled{
                                    background-color: #c5c5c5 !important;
                                }
                            </style>
                            <td style="padding-top: 50px;">
                                <div
                                    class="name-team <?php echo (is_numeric($mhdp) && floatval($mhdp) < 0) ? 'hdp-team' : ''; ?> <?php echo ((is_numeric($mhdp) && floatval($mhdp) == 0) && floatval($hhdp) < floatval($ghdp)) ? 'hdp-team' : ''; ?>">
                                    <?php
                                    echo $thismatch['hn'];
                                    ?>
                                </div>
                                <?php if (empty($ownbet)) { ?>
                                    <div class="bt-play <?php echo (($thismatch['sid']!=1)?'disabled':'') ?>" side="home">Play Home</div>
                                <?php } ?>
                            </td>
                            <td style="padding-top: 50px;">
                                <div
                                    class="name-team <?php echo (is_numeric($mhdp) && floatval($mhdp) > 0) ? 'hdp-team' : ''; ?> <?php echo ((is_numeric($mhdp) && floatval($mhdp) == 0) && floatval($hhdp) > floatval($ghdp)) ? 'hdp-team' : ''; ?>">
                                    <?php
                                    echo $thismatch['gn'];
                                    ?>
                                </div>
                                <?php if (empty($ownbet)) { ?>
                                    <div class="bt-play <?php echo (($thismatch['sid']!=1)?'disabled':'') ?>" side="away">Play Away</div>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="0"
                                         aria-valuemin="0" aria-valuemax="100"
                                         style="width: <?php echo (count($betside['betlist']['home']) / (count($betside['betlist']['home']) + count($betside['betlist']['away']))) * 100; ?>%;">
                                        <span class="sr-only">60% Complete</span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 0px;">
                                <div style="position: relative;">
                                    <div class="bx-icon-vote" style="left: 94%; top: 0px;"><a
                                            href="/statistics.php?mid=<?php echo $thismatch['mid']; ?>"><img
                                                src="/images/icon-vote.png" style="width: 50px;"></a></div>
                                    <div>
                                    </div>
                                    <div class="bx-head">Results</div>

                                    <div class="bx-icon-stat pull-left">
                                        <?php $showrsconut = 0; ?>
                                        <?php foreach ($teamguide["guide"]["home"] as $guide) { ?>
                                            <?php $g = $datamap->mapByKey($teamguide['keylist']['guide'], $guide); ?>
                                            <?php if ($showrsconut < 6) { ?>
                                                <img src="/images/icon-stat/result/<?php echo $g['result']; ?>.png">
                                                <?php $showrsconut++; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                    <div style="clear: both;"></div>
                                </div>
                                <div>
                                    <div class="bx-head">Odds</div>

                                    <div class="bx-icon-stat pull-left">
                                        <?php $showrsconut = 0; ?>
                                        <?php foreach ($teamguide["guide"]["home"] as $guide) { ?>
                                            <?php $g = $datamap->mapByKey($teamguide['keylist']['guide'], $guide); ?>
                                            <?php if ($showrsconut < 6 && $g['odds_result'] != -1) { ?>
                                                <img
                                                    src="/images/icon-stat/oddsresult/<?php echo $g['odds_result']; ?>.png">
                                                <?php $showrsconut++; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <div class="bx-head pull-right">Results</div>

                                    <div class="bx-icon-stat pull-right"> <?php $showrsconut = 0; ?>
                                        <?php foreach ($teamguide["guide"]["away"] as $guide) { ?>
                                            <?php $g = $datamap->mapByKey($teamguide['keylist']['guide'], $guide); ?>
                                            <?php if ($showrsconut < 6) { ?>
                                                <img src="/images/icon-stat/result/<?php echo $g['result']; ?>.png">
                                                <?php $showrsconut++; ?>
                                            <?php } ?>
                                        <?php } ?></div>
                                    <div style="clear: both;"></div>
                                </div>
                                <div>
                                    <div class="bx-head  pull-right">Odds</div>

                                    <div class="bx-icon-stat pull-right"><?php $showrsconut = 0; ?>
                                        <?php foreach ($teamguide["guide"]["away"] as $guide) { ?>
                                            <?php $g = $datamap->mapByKey($teamguide['keylist']['guide'], $guide); ?>
                                            <?php if ($showrsconut < 6 && $g['odds_result'] != -1) { ?>
                                                <img
                                                    src="/images/icon-stat/oddsresult/<?php echo $g['odds_result']; ?>.png">
                                                <?php $showrsconut++; ?>
                                            <?php } ?>
                                        <?php } ?></div>
                                    <div style="clear: both;"></div>
                                </div>
                            </td>
                        </tr>
                        <tr style="border-top: solid 1px #0091c8;">
                            <td style="padding: 10px;">
                                <div style="position:relative;">
                                    <div class="bx-icon-vote">VS</div>
                                </div>
                                <div class="pull-left earlybet-home-user">
                                    <?php foreach ($betside['betlist']['home'] as $key => $val) { ?>
                                        <?php if ($key < 10) { ?>
                                            <?php $bet = $datamap->mapByKey($betside['keylist']['betlist'], $val) ?>
                                            <div class="box-img-user"><img
                                                    src="<?php echo $logoman->getFbProfilePix($bet['fb_uid']); ?>"
                                                    data-pin-nopin="true"></div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <div class="pull-right"><label
                                        class="point-team"><?php echo count($betside['betlist']['home']); ?></label>
                                </div>
                            </td>
                            <td style="padding: 10px;">
                                <div class="pull-left"><label
                                        class="point-team"><?php echo count($betside['betlist']['away']); ?></label>
                                </div>
                                <div class="pull-right earlybet-away-user">
                                    <?php foreach ($betside['betlist']['away'] as $key => $val) { ?>
                                        <?php if ($key < 10) { ?>
                                            <?php $bet = $datamap->mapByKey($betside['keylist']['betlist'], $val) ?>
                                            <div class="box-img-user"><img
                                                    src="<?php echo $logoman->getFbProfilePix($bet['fb_uid']); ?>"
                                                    data-pin-nopin="true"></div>
                                        <?php } ?>
                                    <?php } ?>

                                </div>
                            </td>
                        </tr>
                        <tr style="border-top: solid 1px #0091c8;">
                            <td style="padding: 10px;">
                                <div class="pull-left">ความคิดเห็น</div>
                            </td>
                            <td style="padding: 10px;">
                                <div class="pull-right">ดูความคิดเห็นทั้งหมด</div>
                            </td>
                        </tr>
                    </table>

                    <div class="bx-comments-games mg-bt15">
                        <div class="rows">
                            <div class="col-sm-6" style="border-right: solid 1px #ccc;">
                                <div class="box-comments">
                                    <table id="cm-home-side" class="hide">
                                        <tr>
                                            <td>
                                                <div class="box-img-user"><img src=""></div>
                                            </td>
                                            <td>
                                                <strong>Sukhumvit Set</strong> เสือคืนป่า! ฟัลเกายิงประตู UCL
                                                รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี
                                                <div class="times-content"><span>34 นาที</span> <span><i
                                                            class="fa fa-heart"></i> 2</span> <span>ตอบกลับ</span></div>
                                                <div class="bx-reply">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <div class="box-img-user"><img src="">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <strong>Sukhumvit Set</strong> เสือคืนป่า!
                                                                ฟัลเกายิงประตู UCL รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี
                                                                <div class="times-content"><span>34 นาที</span> <span><i
                                                                            class="fa fa-heart"></i> 2</span> <span>ตอบกลับ</span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="box-img-user"><img src="">
                                                                </div>
                                                            </td>
                                                            <td><input class="form-control"
                                                                       placeholder="ตอบกลับความคิดเห็น"></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="box-img-user"><img src=""></div>
                                            </td>
                                            <td>
                                                <strong>Sukhumvit Set</strong> เสือคืนป่า! ฟัลเกายิงประตู UCL
                                                รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี
                                                <div class="times-content"><span>34 นาที</span> <span><i
                                                            class="fa fa-heart"></i> 2</span> <span>ตอบกลับ</span></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="box-comments">
                                    <table id="cm-away-side" class="hide">
                                        <tr class="fullsize-cm-original">
                                            <td>
                                                <div class="box-img-user"><img class="cm-profile-pix"
                                                                               src=""></div>
                                            </td>
                                            <td>
                                                <strong class="cm-name-user">Sukhumvit Set</strong>
                                                <labal class="cm-msg" style="color: #333;"></labal>
                                                <div class="times-content"><span
                                                        class="how-long pass-long">34 นาที</span> <span><i
                                                            class="fa fa-heart like-comment" commentid="0"></i> <label
                                                            class="like-count" commentid="0">2</label></span>
                                                    <span class="reply-to" parent="0">ตอบกลับ</span></div>
                                                <div class="bx-reply">
                                                    <table class="reply-row" parent="0">
                                                        <tr class="fullsize-rp-original">
                                                            <td>
                                                                <div class="box-img-user"><img class="cm-profile-pix"
                                                                                               src="">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <strong class="cm-name-user">Sukhumvit Set</strong>
                                                                <labal class="rp-msg"></labal>
                                                                <div class="times-content"><span class="how-long pass-long">34 นาที</span> <span><i
                                                                            class="fa fa-heart like-comment"
                                                                            commentid="0"></i> <label
                                                                            class="like-count"
                                                                            commentid="0">2</label></span> <span
                                                                        class="reply-to" parent="0">ตอบกลับ</span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr class="reply-smgbox-orginal" parent="0" user="ownner">
                                                            <td>
                                                                <div class="box-img-user"><img class="active-user-pix"
                                                                                               src="">
                                                                </div>
                                                            </td>
                                                            <td><input class="form-control reply-msg-val"
                                                                       placeholder="ตอบกลับความคิดเห็น" parent="0"></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="box-img-user"><img src="/images/9.jpg"></div>
                                            </td>
                                            <td>
                                                <strong>Sukhumvit Set</strong> เสือคืนป่า! ฟัลเกายิงประตู UCL
                                                รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี
                                                <div class="times-content"><span>34 นาที</span> <span><i
                                                            class="fa fa-heart"></i> 2</span> <span>ตอบกลับ</span></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="comment-input-main">
                                    <input id="comment-inputbox" class="form-control" placeholder="แสดงความคิดเห็น">
                                    <button class="btn btn-primary pull-right" id="post-comment">Post</button>
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                    </div>

                    <div class="bx-raking mg-bt15">
                        <div>
                            <div class="pull-left"><strong>อันดับคะแนน</strong></div>
                            <div class="pull-right"><a href="ranking.php">ดูอันดับทั้งหมด</a></div>
                            <div style="clear: both;"></div>
                        </div>
                        <div class="bx-table-ranking">
                            <div class="col-sm-4">
                                <strong>Top Ranking</strong>
                                <table>
                                    <tr>
                                        <th colspan="2">อันดับ</th>
                                        <th>แต้ม</th>
                                    </tr>
                                    <?php $row = 0; ?>
                                    <?php foreach ($ranking['gp']['now'] as $key => $val) { ?>
                                        <?php $r = $datamap->mapByKey($ranking['keylist']['gp'], $val); ?>
                                        <?php if ($row < 5) { ?>
                                            <tr>
                                                <td><?php echo $r['rank']; ?></td>
                                                <td>
                                                    <div class="box-img-user bx-radius"><img
                                                            src="https://graph.facebook.com//v2.8/<?php echo $r['fb_uid']; ?>/picture">
                                                    </div>
                                                    <?php echo $r['display_name']; ?>
                                                </td>
                                                <td><?php echo number_format($r['overall_gp'], 2); ?></td>
                                            </tr>
                                            <?php $row++; ?>
                                        <?php } ?>
                                    <?php } ?>
                                </table>
                            </div>
                            <div class="col-sm-4">
                                <strong>Top Sgold</strong>
                                <table>
                                    <tr>
                                        <th colspan="2">อันดับ</th>
                                        <th>แต้ม</th>
                                    </tr>
                                    <?php $row = 0; ?>
                                    <?php foreach ($ranking['gold']['now'] as $key => $val) { ?>
                                        <?php $r = $datamap->mapByKey($ranking['keylist']['gold'], $val); ?>
                                        <?php if ($row < 5) { ?>
                                            <tr>
                                                <td><?php echo $r['rank']; ?></td>
                                                <td>
                                                    <div class="box-img-user bx-radius"><img
                                                            src="https://graph.facebook.com//v2.8/<?php echo $r['fb_uid']; ?>/picture">
                                                    </div>
                                                    <?php echo $r['display_name']; ?>
                                                </td>
                                                <td><?php echo number_format($r['overall_sgold'], 2); ?></td>
                                            </tr>
                                            <?php $row++; ?>
                                        <?php } ?>
                                    <?php } ?>
                                </table>
                            </div>
                            <div class="col-sm-4">
                                <strong>Premier League</strong>
                                <table>
                                    <tr>
                                        <th colspan="2">อันดับ</th>
                                        <th>แต้ม</th>
                                    </tr>
                                    <?php $row = 0; ?>
                                    <?php foreach ($ranking['league']['english_premier_league'] as $key => $val) { ?>
                                        <?php $r = $datamap->mapByKey($ranking['keylist']['league'], $val); ?>
                                        <?php if ($row < 5) { ?>
                                            <tr>
                                                <td><?php echo $r['current_rank']; ?></td>
                                                <td>
                                                    <div class="box-img-user bx-radius"><img
                                                            src="https://graph.facebook.com//v2.8/<?php echo $r['fb_uid']; ?>/picture">
                                                    </div>
                                                    <?php echo $r['display_name']; ?>
                                                </td>
                                                <td><?php echo number_format($r['indicator'], 2); ?></td>
                                            </tr>
                                            <?php $row++; ?>
                                        <?php } ?>
                                    <?php } ?>
                                </table>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                    <div>
                        <div class="tab-head-title text-center mg-bt15">
                            <strong>Related News</strong>
                        </div>
                        <?php $news = $newsm->getRelatedNews($gamedata['match'][38] . ',' . $gamedata['match'][13] . ',' . $gamedata['match'][14], 3); ?>
                        <div class="rows">
                            <?php foreach ($news as $n) { ?>
                                <div class="col-sm-4">
                                    <div class="bx-story-small">
                                        <div class="hilight-cover"></div>
                                        <div class="crop"
                                             style="background-image: url(<?php echo $n->imageLink; ?>)"></div>
                                        <div class="bx-story-content">
                                            <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                                    class="how-long"> <?php echo $n->createDatetime; ?></span>
                                            </div>
                                            <a href="articles.php?newsid=<?php echo $n->newsid; ?>"
                                               class="h-content"><?php echo $n->titleTh; ?></a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="h-tabs-game">เลือกทีมทายผลบอลวันนี้</div>
                    <div class="table-game-all mg-bt15">
                        <div class="wrap-game-today">
                            <div id="about" class="nano bx1">
                                <div class="content">
                                    <?php foreach ($recommendodds['match'] as $key => $val) { ?>
                                        <?php $oddsmatch = $datamap->mapByKey($recommendodds['keylist']['match'], $val); ?>
                                        <?php
                                        $dt = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $oddsmatch['date']);
                                        $dt->addHours(7);
                                        ?>
                                        <?php
                                        $mhdp = "";
                                        $hhdp = "";
                                        $ghdp = "";
                                        if (is_numeric($oddsmatch["hdp"])) {
                                            $mhdp = $oddsmatch["hdp"];
                                            $hhdp = $oddsmatch["hdp_home"];
                                            $ghdp = $oddsmatch["hdp_away"];
                                        } ?>
                                        <div
                                            class="box-results lnktogame <?php echo ($key % 2 == 0) ? 'active' : ''; ?>"
                                            mid="<?php echo $oddsmatch['mid']; ?>">
                                            <table class="table-list-games table-teams">
                                                <tr>
                                                    <td><img
                                                            src="<?php echo $logoman->getLogo($oddsmatch['hid'], 0); ?>">
                                                        <label
                                                            class="<?php echo (is_numeric($mhdp) && floatval($mhdp) < 0) ? 'hdp-team' : ''; ?> <?php echo ((is_numeric($mhdp) && floatval($mhdp) == 0) && floatval($hhdp) < floatval($ghdp)) ? 'hdp-team' : ''; ?>"><?php echo $oddsmatch['hn']; ?></label>
                                                    </td>
                                                    <td class="text-right"><?php echo $dt->format("H:i"); ?></td>
                                                </tr>
                                                <tr>
                                                    <td><img
                                                            src="<?php echo $logoman->getLogo($oddsmatch['gid'], 0); ?>">
                                                        <label
                                                            class="<?php echo (is_numeric($mhdp) && floatval($mhdp) > 0) ? 'hdp-team' : ''; ?> <?php echo ((is_numeric($mhdp) && floatval($mhdp) == 0) && floatval($hhdp) > floatval($ghdp)) ? 'hdp-team' : ''; ?>"><?php echo $oddsmatch['gn']; ?></label>
                                                    </td>
                                                    <td class="text-right"><label
                                                            class="info"><?php echo $oddsmatch['hdp'] ?></label></td>
                                                </tr>
                                            </table>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="bx-advertise mg-bt15">
                        <img src="/images/prize.png" style="width: 100%;">

                        <div class="content-ads">
                            <div><strong>แจกรางวัลทุกเดือน</strong></div>
                            <span>เล่นเกมส์ทายผลบอลฟรี สะสมเพชรไว้แลกของรางวัลมากมายจากเว็บ</span>
                        </div>
                    </div>
                    <div class="bx-advertise mg-bt15">
                        <div class="content-ads">
                            <div><strong>กติกาการเล่นเกมส์</strong></div>
                            <span>
                               <ul>
                                   <li>- ต้องเล่นเกมบอลเต็งก่อนจึงจะเล่นบอลสเต็ปได้</li>
                                   <li>- เลือกสเต็ปให้ครบ 5 คู่/วัน</li>
                                   <li>- **สเต็ปแตก ผลงานทั้ง 5 ตัว จะต้องเป็น W (Wตัวใหญ่หรือชนะเต็ม) เท่านั้น/li>
                                   <li>- **ไม่สามารถแก้ไขเปลี่ยนทีมที่เลือกเล่นในแต่ละวันได้
                                       ควรตรวจสอบให้มั่นใจก่อนจะกดบันทึก
                                   </li>
                                   <li>- หากผู้รับรางวัลมีบัญชีธนาคารไม่ตรงกับทางเว็บ (กสิกรไทย) จะหักค่าโอนปลายทาง</li>
                               </ul>
                            </span>
                        </div>
                    </div>

                    <div>
                        <div class="fb-page" data-href="https://www.facebook.com/9ssporting/?fref=ts"
                             data-tabs="timeline" data-height="220" data-width="100%" data-small-header="false"
                             data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                            <blockquote cite="https://www.facebook.com/9ssporting/?fref=ts"
                                        class="fb-xfbml-parse-ignore"><a
                                    href="https://www.facebook.com/9ssporting/?fref=ts">Ssporting.com</a></blockquote>
                        </div>
                    </div>


                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade in" role="dialog" id="myModal" tabindex="-1" aria-labelledby="myModalLabel"
     style="display: none;">
    <div class="modal-dialog" role="document" style="">
        <div class="modal-content">

            <div class="modal-body">
                <table class="table-played">
                    <tr>
                        <td>
                            <div class="button-play select-home bg-played"><span><i
                                        class="fa fa-check-square-o"></i><?php echo $thismatch['hn']; ?></span>
                            </div>
                            <div class="button-play select-away hide"><span><?php echo $thismatch['hn']; ?></span>
                            </div>
                        </td>
                        <td>
                            <div class="bx-border">
                                <div class="pull-left"><img src="/images/coin/scoin100.png"></div>
                                <select id="bet-amount">
                                    <?php foreach ($sysconfig['ball_game_scoin'] as $key => $val) { ?>
                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                    <?php } ?>
                                </select>

                                <div style="clear:both;"></div>
                            </div>
                        </td>
                        <td>
                            <div class="button-play select-home"><span><?php echo $thismatch['gn']; ?></span></div>
                            <div class="button-play select-away bg-played hide"><span><i
                                        class="fa fa-check-square-o"></i><?php echo $thismatch['gn']; ?></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center">เลือกจำนวน Coin</td>
                    </tr>
                    <tr style="border-top: solid 1px #eee; border-bottom: solid 1px #eee; padding: 5px;">
                        <td><strong><?php echo count($betside['betlist']['home']); ?></strong></td>
                        <td style="font-weight: bold;">
                            <div>HDP</div>
                            <?php
                            if (!empty($gamedata["odds"])) {
                                echo $gamedata["odds"][2];
                            }
                            ?>
                        </td>
                        <td><strong><?php echo count($betside['betlist']['away']); ?></strong></td>
                    </tr>
                    <tr>
                        <td colspan="3"><input id="bet-comment" class="form-control" placeholder="Comment.."></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <button class="btn btn-info btn-bet">ทายผล</button>
                            <img id="bet-wait-img" style="width: 60px;" class="hide" src="images/loading-spinner.gif"/>
                        </td>
                    </tr>

                </table>

            </div>

        </div>
    </div>
</div>


<?php //include 'footer.php';
include 'view/index/footer.php';
?>

<script>
    $(".nano").nanoScroller();
</script>

</body>
</html>
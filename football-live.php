<!DOCTYPE html>
<?php
require_once 'model/VideoM.php';
$VideoMObj = new VideoM();
//$dataVideoMMain = $VideoMObj->getSlideVideos("", 1,((isset($_GET['tag']))?$_GET['tag']:''));

if (isset($_GET['videoId'])) {
    $dataVideo =array();
    $dataVideo = $VideoMObj->getVideosById($_GET['videoId']);
//    $dataVideoMAll = $VideoMObj->getSlideVideosMore($dataVideo[0]->video_id, "highlight", 21, $dataVideo[0]->video_tag);
    $dataVideoMAllByNormal = $VideoMObj->getSlideVideosMoreByStyle($dataVideo[0]->video_id, "highlight", 105, $dataVideo[0]->video_tag, 'normal');
    $dataVideoMAllByGoal = $VideoMObj->getSlideVideosMoreByStyle($dataVideo[0]->video_id, "highlight", 105, $dataVideo[0]->video_tag, 'goal');

    if (!empty($dataVideoMAllByGoal)) {
        $dataVideoMHighlight = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByGoal[0]->video_id, "highlight", 7);
        $dataVideoMOthers = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByGoal[0]->video_id, "general", 7);
        $dataVideoMRelated = $VideoMObj->getRelatedVideoMore($dataVideoMAllByGoal[0]->video_id, '', 6);
    } else {
        $dataVideoMHighlight = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideo[0]->video_id, "highlight", 7);
        $dataVideoMOthers = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideo[0]->video_id, "general", 7);
        $dataVideoMRelated = $VideoMObj->getRelatedVideoMore($dataVideo[0]->video_id, '', 6);
    }
} else {

//    $dataVideoMAll = $VideoMObj->getSlideVideosCategory(((isset($_GET['category'])) ? $_GET['category'] : ''), "highlight", 21, ((isset($_GET['tag'])) ? $_GET['tag'] : ''));
    $dataVideoMAllByNormal = $VideoMObj->getSlideVideosCategoryByStyle(((isset($_GET['category'])) ? $_GET['category'] : ''), "highlight", 105, ((isset($_GET['tag'])) ? $_GET['tag'] : ''), 'normal');
    $dataVideoMAllByGoal = $VideoMObj->getSlideVideosCategoryByStyle(((isset($_GET['category'])) ? $_GET['category'] : ''), "highlight", 105, ((isset($_GET['tag'])) ? $_GET['tag'] : ''), 'goal');

    if (!empty($dataVideoMAllByGoal)) {
        $dataVideoMHighlight = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByGoal[0]->video_id, "highlight", 7);
        $dataVideoMOthers = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByGoal[0]->video_id, "general", 7);
        $dataVideoMRelated = $VideoMObj->getRelatedVideoMore($dataVideoMAllByGoal[0]->video_id, '', 6);
    } else {
        $dataVideoMHighlight = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByNormal[0]->video_id, "highlight", 7);
        $dataVideoMOthers = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByNormal[0]->video_id, "general", 7);
        $dataVideoMRelated = $VideoMObj->getRelatedVideoMore($dataVideoMAllByNormal[0]->video_id, '', 6);
    }
}

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">

    <title>Football Live | ฟุตบอลสด ถ่ายทอดสดฟุตบอล</title>
    <meta name="keywords" content="<?= ((!empty($dataVideo)) ? $dataVideo[0]->video_tag : '') ?>">
    <meta name="description" content="<?= ((!empty($dataVideo)) ? $dataVideo[0]->desc : '') ?>">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">

    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/football-live.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="nanoscrollbar/nanoscrollbar.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="js/jquery-timeago/jquery.timeago.js"></script>
    <script src="/js/moment.js"></script>
    <script src="js/sly/sly.js"></script>
    <?php if (!empty($dataVideo)) { ?>
        <?php foreach ($dataVideo as $key => $value) { ?>
            <meta property="og:title" content="<?php echo $value->title; ?>"/>
            <meta property="og:description" content="<?php echo $value->desc; ?>"/>
            <meta property="og:image" content="<?php echo $value->thumbnail; ?>"/>
        <?php } ?>
    <?php } else if (!empty($dataVideoMAll)) { ?>
        <?php foreach ($dataVideoMAll as $key => $value) { ?>
            <?php if ($key == 0) { ?>
                <meta property="og:title" content="<?php echo $value->title; ?>"/>
                <meta property="og:description" content="<?php echo $value->desc; ?>"/>
                <meta property="og:image" content="<?php echo $value->thumbnail; ?>"/>
            <?php } ?>
        <?php } ?>
    <?php } else if ($dataVideoMAllByGoal) { ?>
        <?php foreach ($dataVideoMAllByGoal as $key => $value) { ?>
            <?php if ($key == 0) { ?>
                <meta property="og:title" content="<?php echo $value->title; ?>"/>
                <meta property="og:description" content="<?php echo $value->desc; ?>"/>
                <meta property="og:image" content="<?php echo $value->thumbnail; ?>"/>
            <?php } ?>
        <?php } ?>
    <?php } else if ($dataVideoMAllByNormal) { ?>
        <?php foreach ($dataVideoMAllByNormal as $key => $value) { ?>
            <?php if ($key == 0) { ?>
                <meta property="og:title" content="<?php echo $value->title; ?>"/>
                <meta property="og:description" content="<?php echo $value->desc; ?>"/>
                <meta property="og:image" content="<?php echo $value->thumbnail; ?>"/>
            <?php } ?>
        <?php } ?>
    <?php } ?>
    <style type="text/css">
        body {
            background-color: #f1f1f1;
        }

        div.twitter-video {
            max-height: none !important;
            max-width: none !important;
            min-width: none !important;
            min-height: none !important;
            width: 100% !important;
            height: 100% !important;
        }
    </style>
</head>
<body>

<div class="content-social-sharing">
    <ul>
        <li class="box-facebook-sharing">
            <i class="fa fa-facebook" aria-hidden="true"></i>
            <div class="box-sharing-hover">
                Share Facebook
            </div>
        </li>
        <li class="box-g-plus-sharing">
            <i class="fa fa-google-plus" aria-hidden="true"></i>
            <div class="box-sharing-hover">
                Share Google+
            </div>
        </li>
    </ul>
</div>
<!--index menu-->
<?php include $_SERVER['DOCUMENT_ROOT'] . '/view/index/menu-all.php'; ?>

<div class="container-football-live container-feature-video">
    <div class="col-lg-12" style="padding: 0;">
        <div class="tab-h-content sexy-football-topic" style="margin: 0;">
            <div class="pull-left header-filter">
                <div class="logo-header-filter">
                    <img src="/images/five-leagues/all.png">
                </div>
                <h3>รายการถ่ายทอดสด ทั้งหมด</h3>
            </div>
            <div class="pull-right content-filter">
                <table>
                    <tbody>
                    <tr>
                        <td>
                            <div class="select-text">ลีก/ ฟุตบอลถ้วย <i class="fa fa-angle-right"
                                                                        aria-hidden="true"></i></div>
                        </td>
                        <td class="active">
                            <div class="logo-hightlight">
                                <a href="/videos"><img src="/images/five-leagues/all.png"></a>
                            </div>
                        </td>
                        <td class="">
                            <div class="logo-hightlight">
                                <a href="/highlight?category=premier league"><img
                                            src="/images/five-leagues/premier-league-logo.png"></a>
                            </div>
                        </td>
                        <td class="">
                            <div class="logo-hightlight">
                                <a href="/highlight?category=la liga"><img
                                            src="/images/five-leagues/La-Liga-Logo-1.png"></a>
                            </div>
                        </td>
                        <td class="">
                            <div class="logo-hightlight">
                                <a href="/highlight?category=bundesliga"><img
                                            src="/images/five-leagues/logo_bl.gif"></a>
                            </div>
                        </td>
                        <td class="">
                            <div class="logo-hightlight">
                                <a href="/highlight?category=serie a"><img src="/images/five-leagues/seriea1.png"></a>
                            </div>
                        </td>
                        <td class="">
                            <div class="logo-hightlight">
                                <a href="/highlight?category=league 1"><img
                                            src="/images/five-leagues/Ligue-1-logo-france.png"></a>
                            </div>
                        </td>
                        <td class="">
                            <div class="logo-hightlight">
                                <a href="/highlight?category=ucl"><img
                                            src="/images/five-leagues/UEFA-Champions-League-1.png"></a>
                            </div>
                        </td>
                        <td class="">
                            <div class="logo-hightlight">
                                <a href="/highlight?category=europa"><img src="/images/five-leagues/uropa.png"></a>
                            </div>
                        </td>
                        <td class="">
                            <div class="logo-hightlight">
                                <a href="/highlight?category=thai"><img src="/images/five-leagues/T1_Logo2.png"></a>
                            </div>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row" style="margin-right: 0;">
        <div class="bg-large-video">
            <div class="wrap-content-cols-2 col-lg-12">
                <div class="col-sm-8" style="position:static !important; margin-top: -10px; padding-left: 15px;">
                    <div class="box-large-videos" style="width: 100%;display: table;">
                        <div style="display: table-cell;vertical-align: middle;">
                            <img style="width: 100%" src="/images/simple-live.jpg">
                        </div>
                    </div>
                    <div class="bg-content-live">
                        <div class="col-sm-12">
                            <h1>Napoli VS Genoa</h1>
                            <p>เว็บบอลมาแรงที่สุดในเมืองไทย-ผลบอลสดและไฮไลท์บอลเร็วที่สุด ต้องที่นี่
                                กับรูปแบบการอ่านข่าว และการดูวิดีโอแบบใหม่ที่ออกแบบเพื่อคอกีฬาอย่างคุณ</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box-tabs-videolist box-tabs-livelist">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-video-list" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#tab-live-list" aria-controls="home" role="tab" data-toggle="tab">
                                    <i class="fa fa-video-camera" aria-hidden="true"></i> ถ่ายทอดสด</a>
                            </li>
                            <li role="presentation">
                                <a href="#tab-chat" aria-controls="profile" role="tab" data-toggle="tab">
                                    <i class="fa fa-commenting" aria-hidden="true"></i>
                                    แชท
                                    <span>(25)</span>
                                </a>
                            </li>
                        </ul>
                        <div class="content content-list-live">
                            <!-- Tab panes -->
                            <div class="tab-content" id="">
                                <div role="tabpanel" class="tab-pane active" id="tab-live-list">
                                    <div class="box-list-videos">
                                        <table>
                                            <tbody>
                                            <tr class="box-livefootball box-livefootball-active">
                                                <td>
                                                    <div class="box-list-video-side">
                                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                                        <img src="/images/simple-live.jpg">
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4>Napoli VS Genoa</h4>
                                                    <p class="text-league">Premier League</p>
                                                    <p class="text-time-live"><i class="fa fa-clock-o"
                                                                                 aria-hidden="true"></i> 19:00 /
                                                        12/12/2017</p>
                                                    <span class="label label-live label-danger">
                                                                <i class="fa fa-play" aria-hidden="true"></i> สด
                                                        </span>
                                                    <div class="view-live">
                                                        <i class="fa fa-eye" aria-hidden="true"></i> 300
                                                    </div>
                                                </td>
                                                <td class="chanel-list">
                                                    <ul>
                                                        <li><img src="/images/channel/True-Sport-1-1.png"></li>
                                                        <li><img src="/images/channel/True-Sport-3.png"></li>
                                                        <li><img src="/images/channel/bein-sports-1-hero.png"></li>
                                                        <li><img src="/images/channel/bein-sports-2-hero.png"></li>
                                                        <li><img src="/images/channel/FSSouth_white.png"></li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr class="box-livefootball">
                                                <td>
                                                    <div class="box-list-video-side">
                                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                                        <img src="/images/simple-live.jpg">
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4>Napoli VS Genoa</h4>
                                                    <p class="text-league">Premier League</p>
                                                    <p class="text-time-live"><i class="fa fa-clock-o"
                                                                                 aria-hidden="true"></i> 19:00 /
                                                        12/12/2017</p>
                                                    <span class="label label-live label-danger">
                                                                <i class="fa fa-play" aria-hidden="true"></i> สด
                                                        </span>
                                                    <div class="view-live">
                                                        <i class="fa fa-eye" aria-hidden="true"></i> 300
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr class="box-livefootball">
                                                <td>
                                                    <div class="box-list-video-side">
                                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                                        <img src="/images/simple-live.jpg">
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4>Napoli VS Genoa</h4>
                                                    <p class="text-league">Premier League</p>
                                                    <p class="text-time-live"><i class="fa fa-clock-o"
                                                                                 aria-hidden="true"></i> 19:00 /
                                                        12/12/2017</p>
                                                    <span class="label label-live label-danger">
                                                                <i class="fa fa-play" aria-hidden="true"></i> สด
                                                        </span>
                                                    <div class="view-live">
                                                        <i class="fa fa-eye" aria-hidden="true"></i> 300
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr class="box-livefootball">
                                                <td>
                                                    <div class="box-list-video-side">
                                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                                        <img src="/images/simple-live.jpg">
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4>Napoli VS Genoa</h4>
                                                    <p class="text-league">Premier League</p>
                                                    <p class="text-time-live"><i class="fa fa-clock-o"
                                                                                 aria-hidden="true"></i> 19:00 /
                                                        12/12/2017</p>
                                                    <span class="label label-live label-danger">
                                                                <i class="fa fa-play" aria-hidden="true"></i> สด
                                                        </span>
                                                    <div class="view-live">
                                                        <i class="fa fa-eye" aria-hidden="true"></i> 300
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr class="box-livefootball">
                                                <td>
                                                    <div class="box-list-video-side">
                                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                                        <img src="/images/simple-live.jpg">
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4>Napoli VS Genoa</h4>
                                                    <p class="text-league">Premier League</p>
                                                    <p class="text-time-live"><i class="fa fa-clock-o"
                                                                                 aria-hidden="true"></i> 19:00 /
                                                        12/12/2017</p>
                                                    <span class="label label-default">
                                                                <i class="fa fa-hourglass-start" aria-hidden="true"></i> รอถ่ายทอดสด
                                                        </span>
                                                    <div class="view-live">
                                                        <i class="fa fa-eye" aria-hidden="true"></i> -
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr class="box-livefootball">
                                                <td>
                                                    <div class="box-list-video-side">
                                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                                        <img src="/images/simple-live.jpg">
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4>Napoli VS Genoa</h4>
                                                    <p class="text-league">Premier League</p>
                                                    <p class="text-time-live"><i class="fa fa-clock-o"
                                                                                 aria-hidden="true"></i> 19:00 /
                                                        12/12/2017</p>
                                                    <span class="label label-default">
                                                                <i class="fa fa-hourglass-start" aria-hidden="true"></i> รอถ่ายทอดสด
                                                        </span>
                                                    <div class="view-live">
                                                        <i class="fa fa-eye" aria-hidden="true"></i> -
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr class="box-livefootball">
                                                <td>
                                                    <div class="box-list-video-side">
                                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                                        <img src="/images/simple-live.jpg">
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4>Napoli VS Genoa</h4>
                                                    <p class="text-league">Premier League</p>
                                                    <p class="text-time-live"><i class="fa fa-clock-o"
                                                                                 aria-hidden="true"></i> 19:00 /
                                                        12/12/2017</p>
                                                    <span class="label label-default">
                                                                <i class="fa fa-hourglass-start" aria-hidden="true"></i> รอถ่ายทอดสด
                                                        </span>
                                                    <div class="view-live">
                                                        <i class="fa fa-eye" aria-hidden="true"></i> -
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr class="box-livefootball">
                                                <td>
                                                    <div class="box-list-video-side">
                                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                                        <img src="/images/simple-live.jpg">
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4>Napoli VS Genoa</h4>
                                                    <p class="text-league">Premier League</p>
                                                    <p class="text-time-live"><i class="fa fa-clock-o"
                                                                                 aria-hidden="true"></i> 19:00 /
                                                        12/12/2017</p>
                                                    <span class="label label-default">
                                                                <i class="fa fa-hourglass-start" aria-hidden="true"></i> รอถ่ายทอดสด
                                                        </span>
                                                    <div class="view-live">
                                                        <i class="fa fa-eye" aria-hidden="true"></i> -
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr class="box-livefootball">
                                                <td>
                                                    <div class="box-list-video-side">
                                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                                        <img src="/images/simple-live.jpg">
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4>Napoli VS Genoa</h4>
                                                    <p class="text-league">Premier League</p>
                                                    <p class="text-time-live"><i class="fa fa-clock-o"
                                                                                 aria-hidden="true"></i> 19:00 /
                                                        12/12/2017</p>
                                                    <span class="label label-default">
                                                                <i class="fa fa-hourglass-start" aria-hidden="true"></i> รอถ่ายทอดสด
                                                        </span>
                                                    <div class="view-live">
                                                        <i class="fa fa-eye" aria-hidden="true"></i> -
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr class="box-livefootball">
                                                <td>
                                                    <div class="box-list-video-side">
                                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                                        <img src="/images/simple-live.jpg">
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4>Napoli VS Genoa</h4>
                                                    <p class="text-league">Premier League</p>
                                                    <p class="text-time-live"><i class="fa fa-clock-o"
                                                                                 aria-hidden="true"></i> 19:00 /
                                                        12/12/2017</p>
                                                    <span class="label label-default">
                                                                <i class="fa fa-hourglass-start" aria-hidden="true"></i> รอถ่ายทอดสด
                                                        </span>
                                                    <div class="view-live">
                                                        <i class="fa fa-eye" aria-hidden="true"></i> -
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab-chat">
                                    <div class="box-list-videos">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid" style="background-color: #fff; padding: 0 0 70px 0;">
    <div class="container">

        <div class="wrap-content-cols">
            <div class="tab-h-content sexy-football-topic" style="margin: 10px 0 30px 0;">
                <div class="pull-left"><span>Recent Videos</span></div>
            </div>
            <div id="related-video-mark">
                <?php foreach ($dataVideoMRelated as $key => $value) { ?>
                    <?php if ($value->videosource == "twitter") { ?>
                        <a href="/hothit-videos?videoId=<?php echo $value->video_id ?>">
                            <div class="col-sm-2">
                                <div class="box-videos box-related">
                                    <div class="icon-play"><i class="fa fa-play"></i></div>
                                    <img src="<?php echo $value->urlImg ?>">
                                </div>
                                <div class="title-owl-carousel"><?php echo $value->title ?></div>
                                <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                            class="how-long"><?php echo $value->create_datetime ?></span></div>
                            </div>
                        </a>
                    <?php } else { ?>
                        <a href="#box-large-videos-autoPlay"
                           onclick="setVideosAutoPlay(true,'<?php echo $value->urlIframe ?>','<?php echo $value->category ?>','<?php echo $value->title ?>','','<?php echo $value->create_datetime ?>','<?php echo $value->video_id ?>')">
                            <div class="col-sm-2">
                                <div class="box-videos box-related">
                                    <div class="icon-play"><i class="fa fa-play"></i></div>
                                    <img src="<?php echo $value->urlImg ?>">
                                </div>
                                <div class="title-owl-carousel"><?php echo $value->title ?></div>
                                <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                            class="how-long"><?php echo $value->create_datetime ?></span></div>
                            </div>
                        </a>
                    <?php } ?>
                <?php } ?>
                <div style="clear: both;"></div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>
<div class="container-fluid">
    <div class="container">
        <div class="wrap-content-cols-3">
            <div class="wrap-most-recent text-center">
                <div class="wrap-content-cols" id="videosHighlightTab">
                    <div class="wrap-box-news">
                        <!--                        <div class="tab-h-content">-->
                        <!--                            <div class="pull-left"><span>ไฮไลท์คู่บอล</span></div>-->
                        <!--                            <a class="pull-right more" href="/videos">More Highlight...</a>-->
                        <!---->
                        <!--                            <div style="clear: both;"></div>-->
                        <!--                        </div>-->
                        <div class="tab-h-content sexy-football-topic" style="margin-top: 50px;">
                            <div class="pull-left"><span>ไฮไลท์คู่บอล</span></div>
                            <a href="/videos" class="pull-right more-btn">ทั้งหมด</a>
                        </div>
                        <div class="wrap-box-content-news">
                            <div class="box-other-news">
                                <?php foreach ($dataVideoMHighlight as $key => $value) {
                                    if ($key < 3) { ?>
                                        <?php if ($value->videosource == "twitter") { ?>
                                            <a href="/hothit-videos?videoId=<?php echo $value->video_id ?>">
                                                <div class="col-sm-4">
                                                    <div class="tab-color"></div>
                                                    <div class="times-content"><i class="fa fa-clock-o"></i>
                                                        <time
                                                                class="how-long"><?php echo $value->create_datetime ?></time>
                                                    </div>
                                                    <div
                                                            class="title-owl-carousel title-h-videos"><?php echo $value->title ?></div>
                                                    <div class="box-videos img-other-news">
                                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                                        <img src="<?php echo $value->urlImg ?>">
                                                    </div>
                                                </div>
                                            </a>
                                        <?php } else { ?>
                                            <a href="#box-large-videos-autoPlay"
                                               onclick="setVideosAutoPlay(true,'<?php echo $value->urlIframe ?>','<?php echo $value->category ?>','<?php echo $value->title ?>','<?php echo $value->desc ?>','<?php echo $value->create_datetime ?>','<?php echo $value->video_id ?>')">
                                                <div class="col-sm-4">
                                                    <div class="tab-color"></div>
                                                    <div class="times-content"><i class="fa fa-clock-o"></i>
                                                        <time
                                                                class="how-long"><?php echo $value->create_datetime ?></time>
                                                    </div>
                                                    <div
                                                            class="title-owl-carousel title-h-videos"><?php echo $value->title ?></div>
                                                    <div class="box-videos img-other-news">
                                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                                        <img src="<?php echo $value->urlImg ?>">
                                                    </div>
                                                </div>
                                            </a>
                                        <?php } ?>
                                    <?php }
                                } ?>

                                <div style="clear: both;"></div>
                            </div>
                            <div class="box-other-news">
                                <?php foreach ($dataVideoMHighlight as $key => $value) {
                                    if ($key >= 3) { ?>
                                        <?php if ($value->videosource == "twitter") { ?>
                                            <a href="/hothit-videos?videoId=<?php echo $value->video_id ?>"
                                            <div class="col-sm-3">
                                                <div class="tab-color"></div>
                                                <div class="times-content"><i class="fa fa-clock-o"></i>
                                                    <time
                                                            class="how-long"><?php echo $value->create_datetime ?></time>
                                                </div>
                                                <div
                                                        class="title-owl-carousel title-h-videos"><?php echo $value->title ?></div>
                                                <div class="box-videos">
                                                    <div class="icon-play"><i class="fa fa-play"></i></div>
                                                    <img
                                                            src="<?php echo $value->urlImg ?>">
                                                </div>
                                            </div>
                                            </a>
                                        <?php } else { ?>
                                            <a href="#box-large-videos-autoPlay"
                                               onclick="setVideosAutoPlay(true,'<?php echo $value->urlIframe ?>','<?php echo $value->category ?>','<?php echo $value->title ?>','<?php echo $value->desc ?>','<?php echo $value->create_datetime ?>','<?php echo $value->video_id ?>')">
                                                <div class="col-sm-3">
                                                    <div class="tab-color"></div>
                                                    <div class="times-content"><i class="fa fa-clock-o"></i>
                                                        <time
                                                                class="how-long"><?php echo $value->create_datetime ?></time>
                                                    </div>
                                                    <div
                                                            class="title-owl-carousel title-h-videos"><?php echo $value->title ?></div>
                                                    <div class="box-videos">
                                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                                        <img
                                                                src="<?php echo $value->urlImg ?>">
                                                    </div>
                                                </div>
                                            </a>
                                        <?php } ?>
                                    <?php }
                                } ?>

                            </div>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <div class="wrap-content-cols" id="videosOthersTab">
                    <div class="wrap-box-news">

                        <!--                        <div class="tab-h-content">-->
                        <!--                            <div class="pull-left"><span>ไฮไลท์ Ngoal</span></div>-->
                        <!--                            <a class="pull-right more" href="/hothit-videos">More Others...</a>-->
                        <!---->
                        <!--                            <div style="clear: both;"></div>-->
                        <!--                        </div>-->
                        <div class="tab-h-content sexy-football-topic" style="margin-top: 50px;">
                            <div class="pull-left"><span>HotHit Video</span></div>
                            <a href="/videos" class="pull-right more-btn">ทั้งหมด</a>
                        </div>
                        <div class="wrap-box-content-news">
                            <div class="box-other-news">
                                <?php foreach ($dataVideoMOthers as $key => $value) {
                                    if ($key < 3) { ?>
                                        <a href="/hothit-videos?videoId=<?php echo $value->video_id ?>">
                                            <div class="col-sm-4">
                                                <div class="tab-color"></div>
                                                <div class="times-content"><i class="fa fa-clock-o"></i>
                                                    <time
                                                            class="how-long"><?php echo $value->create_datetime ?></time>
                                                </div>
                                                <div class="title-owl-carousel"><?php echo $value->title ?></div>
                                                <div class="box-videos img-other-news">
                                                    <div class="icon-play"><i class="fa fa-play"></i></div>
                                                    <div class="cover-hothit"><img src="/images/All_.png"></div>
                                                    <img src="<?php echo $value->urlImg ?>">
                                                </div>
                                            </div>
                                        </a>
                                    <?php }
                                } ?>
                            </div>
                            <div class="box-other-news">
                                <?php foreach ($dataVideoMOthers as $key => $value) {
                                    if ($key >= 3) {
                                        ?>
                                        <a href="/hothit-videos?videoId=<?php echo $value->video_id ?>">
                                            <div class="col-sm-3">
                                                <div class="tab-color"></div>
                                                <div class="times-content"><i class="fa fa-clock-o"></i>
                                                    <time
                                                            class="how-long"><?php echo $value->create_datetime ?></time>
                                                </div>
                                                <div class="title-owl-carousel"><?php echo $value->title ?></div>
                                                <div class="box-videos box-related-small">
                                                    <div class="icon-play"><i class="fa fa-play"></i></div>
                                                    <div class="cover-hothit"><img src="/images/All_.png"></div>
                                                    <img src="<?php echo $value->urlImg ?>">
                                                </div>
                                            </div>
                                        </a>
                                    <?php }
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear:both;"></div>

            </div>
        </div>
    </div>
</div>
<?php //include 'footer.php';
include 'view/index/footer.php';
?>
<script>
    $("div#div-content").scroll(function () {
        if ($(this).scrollTop() == ($('#div-tab-content').height() - $('#div-content').height())) {
            $('#tab-hilight-goal').find('tr.tr-' + $('#tab-hilight-goal').find('tr.hide').first().attr('data-number')).removeClass('hide');
            $('#tab-hilight-full').find('tr.tr-' + $('#tab-hilight-full').find('tr.hide').first().attr('data-number')).removeClass('hide');
//            console.log($('#tab-hilight-goal').find('tr.hide').first().attr('data-number'));
//            console.log($('#tab-hilight-full').find('tr.hide').first().attr('data-number'));
        }
//        console.log($(this).scrollTop()+"::"+($('#div-tab-content').height()-$('#div-content').height()));
    });

    //    $('#div-tab-content').scroll(function() {
    //        console.log($(this).scrollTop());
    //        if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
    //            alert("near bottom!");
    //        }
    //    });

    $(document).on('change', "#select-filter-list-video", function (e) {
        var category = $(this).val();
        window.location.href = "/highlight?category=" + category;
//        videosGoalByLeague(category);
//        videosFullByLeague(category);
    })

    var statusClose = false;

    loadsly();
    refreshTime();
    $(window).resize(function () {
        loadsly();
        miniBoxVideo();
    });

    $(window).scroll(function () {
        miniBoxVideo();
    });

    function videosGoalByLeague(category) {
        $.ajax({
            url: "/play/videosGoalByLeague",
            data: {category: category},
            method: "GET",
            dataType: "html"
        }).done(function (response) {
            $("div#tab-hilight-goal").html(response);
        });
    }

    function videosFullByLeague(category) {
        $.ajax({
            url: "/play/videosFullByLeague",
            data: {category: category},
            method: "GET",
            dataType: "html"
        }).done(function (response) {
            $("div#tab-hilight-full").html(response);
        })
    }

    function setIndexVideosAutoPlay(link, tag, title, desc, date, id) {
        window.location.href = "/highlight?videoId=" + id;
//        console.log(id);
//        $('#large-videos-title').html(title);
//        $('#large-videos-desc').html("");
//        $('#large-videos-desc').html(desc);
//        $('#large-videos-time').html(moment(date, 'YYYY-MM-DD mm:ss').format(' mm:ss a '));
//        $('#large-videos-date').html(moment(date, 'YYYY-MM-DD mm:ss').format(' MMMM DD, Y'));
//        console.log($('#large-videos-autoPlay').parent('video').length);
//        if($('video').length>=1){
//            $("#box-large-videos-autoPlay").find('video').find('source').attr("src", link);
//            $("#box-large-videos-autoPlay").find('video').attr("autoplay", "");
//            $("#box-large-videos-autoPlay video")[0].load();
//        }else {
//            $('#large-videos-autoPlay').attr("src", link);
//        }

//        $('#large-videos-autoPlay').attr("src", link);
    }

    function closeIframe() {
        $('#large-videos-autoPlay').removeAttr('style');
        $('#large-videos-autoPlay').css('position', "absolute");
        $('#large-videos-autoPlay').css('left', 0);
        $('#btn_Close_Iframe').hide();
        statusClose = true;
    }

    function miniBoxVideo() {
        var windowHeight = jQuery(window).height();
        var windowWidth = jQuery(window).width();
        var scrollTop = $(window).scrollTop();
        var mainBox = $('#box-large-videos-autoPlay').offset().top
        var container = $('.container').width();
        var body = $('body').width();
        if (scrollTop > 500 && !statusClose) {
            $('#large-videos-autoPlay').css('position', "absolute");
//            $('#large-videos-autoPlay').css('left', 0);
            $('#large-videos-autoPlay').css('width', 300);
            $('#large-videos-autoPlay').css('height', 170);
            $('#large-videos-autoPlay').css('top', ((scrollTop - (mainBox)) + (windowHeight - 200)));
            $('#large-videos-autoPlay').css('z-index', 100);
            $('#large-videos-autoPlay').css('right', 0);
            $('#btn_Close_Iframe').css('z-index', 100);
            $('#btn_Close_Iframe').css('top', ((scrollTop - (mainBox)) + (windowHeight - 230)));
            $('#btn_Close_Iframe').css('right', 0);
            $('#btn_Close_Iframe').show();
//                $('#large-videos-autoPlay').css('height', 200);
        } else {
            $('#large-videos-autoPlay').removeAttr('style');
            $('#large-videos-autoPlay').css('width', '100%');
//            $('#large-videos-autoPlay').css('position', "absolute");
//            $('#large-videos-autoPlay').css('left', 0);
            $('#btn_Close_Iframe').hide();
        }
//            console.log((scrollTop));
    }
    function loadsly() {
        var $frame = $('#boxVideo');
        var $slidee = $frame.children('#basic-videos').eq(0);
        var $wrap = $frame.parent();
        $slidee.sly(false);
        $slidee.sly({
            horizontal: 1,
            itemNav: 'basic',
            smart: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 0,
            scrollBy: 1,
            speed: 1000,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,

            // Buttons
            forward: $wrap.find('.btnforward'),
            backward: $wrap.find('.btnbackward'),
            prev: $wrap.find('.btnbackward'),
            next: $wrap.find('.btnforward'),
            prevPage: $wrap.find('.backward'),
            nextPage: $wrap.find('.forward')
        });
    }
    function setVideosAutoPlay(status, link, tag, title, desc, date, id) {
        $('#large-videos-title').html(title);
        $('#large-videos-desc').html("");
        $('#large-videos-desc').html(desc);
        $('#large-videos-time').html(moment(date, 'YYYY-MM-DD mm:ss').format(' mm:ss a '));
        $('#large-videos-date').html(moment(date, 'YYYY-MM-DD mm:ss').format(' MMMM DD, Y'));
        if ($('.active-now-playing').length) {
            $('.active-now-playing').addClass('hilight-cover-videos');
            $('.active-now-playing').html('');
            $('.active-now-playing').removeClass('active-now-playing');
        }
        if ($('#highlight_cover_videos_' + id).attr('class') == "hilight-cover-videos") {
            $('#highlight_cover_videos_' + id).addClass('active-now-playing');
            $('#highlight_cover_videos_' + id).html('now playing');
            $('#highlight_cover_videos_' + id).removeClass('hilight-cover-videos');
        }
        console.log($('#large-videos-autoPlay').parent('video').length);
        if ($('video').length >= 1) {
            $("#box-large-videos-autoPlay").find('video').find('source').attr("src", link);
            $("#box-large-videos-autoPlay").find('video').attr("autoplay", "");
            $("#box-large-videos-autoPlay video")[0].load();
        } else {
            $('#large-videos-autoPlay').attr("src", link);
        }

        if (status) {
            getSlideVideosHead(tag, 20, "highlight", id);
        }
        getRelatedVideo(id, tag, 6);
        videosGoalByLeague(tag);
        videosFullByLeague(tag);
        statusClose = false
    }
    function refreshTime() {
//            console.log('refresh');
        $.each($(".how-long"), function (k, v) {
//                console.log(k,v);
//                var d = new Date(parseInt($(this).text()) * 1000);
//                $(this).text(jQuery.timeago(d));
            if ($.isNumeric($(this).text())) {
                $(this).html(moment($(this).text(), "X").fromNow())
            } else {
                $(this).html(moment($(this).text(), "YYYY-MM-DD HH:mm:ss").fromNow())
            }
        })

    }
    function getOthersVideo() {
        $.ajax({
            url: "/services/getvideotype",
            data: {videotype: 'general', limit: 20},
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
//                console.log(response);
            $('#SlideVideosHead').html("");
            $.each(response, function (index, value) {
//                    console.log(value);
                var textDiv = "";
                textDiv += "<li id='li_video_" + value[0] + "' style='width: 200px;'>";
                textDiv += "<a href='#box-large-videos-autoPlay'";
                textDiv += " link='" + value[3] + "'";
                textDiv += " onclick=''";
                textDiv += "<div class='bx-top-videos' style=''>";
                textDiv += "<div class='hilight-cover-videos'></div>";
                textDiv += "<table>";
                textDiv += "<tr>";
                textDiv += "<td>";
                textDiv += "<div class='img-top-videos'>";
                textDiv += "<img src='" + value[8] + "'>";
                textDiv += "</div>";
                textDiv += "</td>";
                textDiv += "</tr>";
                textDiv += "<tr>";
                textDiv += "<td>";
                textDiv += "<div class='title-videos'>" + value[2] + "</div>";
                textDiv += "</td>";
                textDiv += "</tr>";
                textDiv += "</table>";
                textDiv += "</div>";
                textDiv += "</a>";
                textDiv += "</li>";
                $('#SlideVideosHead').append(textDiv);
                $('li#li_video_' + value[0]).attr('onclick', "setVideosAutoPlay(true,'" + value[7] + "','" + value[6] + "','" + value[2] + "','" + value[7] + "','" + value[3] + "','" + value[0] + "')");
                $('#videosHighlightTab').hide();
            })
        });
    }
    function getHighlightVideo() {
        $.ajax({
            url: "/services/getvideotype",
            data: {videotype: 'highlight', limit: 20},
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
//                console.log(response);
            $('#SlideVideosHead').html("");
            $.each(response, function (index, value) {
//                    console.log(value);
                var textDiv = "";
                textDiv += "<li id='li_video_" + value[0] + "' style='width: 200px;'>";
                textDiv += "<a href='#box-large-videos-autoPlay'";
                textDiv += " link='" + value[3] + "'";
                textDiv += " onclick=''>";
                textDiv += "<div class='bx-top-videos' style=''>";
                textDiv += "<div class='hilight-cover-videos'></div>";
                textDiv += "<table>";
                textDiv += "<tr>";
                textDiv += "<td>";
                textDiv += "<div class='img-top-videos'>";
                textDiv += "<img src='" + value[8] + "'>";
                textDiv += "</div>";
                textDiv += "</td>";
                textDiv += "</tr>";
                textDiv += "<tr>";
                textDiv += "<td>";
                textDiv += "<div class='title-videos'>" + value[2] + "</div>";
                textDiv += "</td>";
                textDiv += "</tr>";
                textDiv += "</table>";
                textDiv += "</div>";
                textDiv += "</a>";
                textDiv += "</li>";
                console.log(textDiv);
                $('#SlideVideosHead').append(textDiv);
                $('li#li_video_' + value[0]).attr('onclick', "setVideosAutoPlay(true,'" + value[7] + "','" + value[9] + "','" + value[2] + "','" + value[3] + "','" + value[0] + "')");
                $('#videosOthersTab').hide();
            })
        });
    }
    function getSlideVideosHead(category, limit, type, id) {
        $.ajax({
            url: "/services/getvideoCategory",
            data: {category: category, limit: limit, type: type, id: id},
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
//                response=jQuery.parseJSON(response);
//                console.log(response);
            $('#SlideVideosHead').html("");
            $.each(response, function (index, value) {
//                console.log(index);
                var textDiv = "";
                textDiv += "<li style='width: 200px;' id='slide_videos_" + value['video_id'] + "'>";
                textDiv += "<a href='#box-large-videos-autoPlay' id='NewSlideVideosHead_" + value['video_id'] + "' link='" + value['videosource'] + "' onclick=''>";
                textDiv += "<div class='bx-top-videos' style=''>";
                textDiv += "<div class='hilight-cover-videos' id='highlight_cover_videos_" + value['video_id'] + "'></div>";
                textDiv += "<table>";
                textDiv += "<tr>";
                textDiv += "<td>";
                textDiv += "<div class='img-top-videos'>";
                textDiv += "<img src='" + value['urlImg'] + "'>";
                textDiv += "</div>";
                textDiv += "</td>";
                textDiv += "</tr>";
                textDiv += "<tr>";
                textDiv += "<td>";
                textDiv += "<div class='title-videos'>" + value['title'] + "</div>";
                textDiv += "<div id='desc-videos-" + value['video_id'] + "' style='display: none;'>" + value['desc'] + "</div>";
                textDiv += "<div class='times-content'><i class='fa fa-clock-o'></i> <span class='how-long'>" + value['create_datetime'] + "</span></div>";
                textDiv += "</td>";
                textDiv += "</tr>";
                textDiv += "</table>";
                textDiv += "</div>";
                textDiv += "</a>";
                textDiv += "</li>";
                $('#SlideVideosHead').append(textDiv);
                $('#NewSlideVideosHead_' + value['video_id']).attr('onclick', "setVideosAutoPlay(false,'" + value['urlIframe'] + "','" + value['video_tag'] + "','" + value['title'] + "','" + value['desc'] + "','" + value['create_datetime'] + "','" + value['video_id'] + "')");
            });
            loadsly();
            refreshTime();
//                $('#basic-videos').sly(false);
//                $('#basic-videos').sly(options);
        })
    }
    function getRelatedVideo(id, tag, limit) {
//            console.log(tag,limit);
        $.ajax({
            url: "/services/getrelatedvideos",
            data: {id: id, tag: tag, limit: limit, type: 'highlight'},
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
//                console.log(response);
            $('#related-video-mark').html("");
            $.each(response, function (index, value) {
//                    console.log(value);
                var textDiv = "";
                textDiv += "<a id='a_relatedvideos_" + value[0] + "' href='#box-large-videos-autoPlay'";
                textDiv += "onclick=''>";
                textDiv += "<div class='col-sm-2'>";
                textDiv += "<div class='box-videos box-related'>";
                textDiv += "<div class='icon-play'><i class='fa fa-play'></i></div>";
                textDiv += "<img src='" + value[9] + "'>";
                textDiv += "</div>";
                textDiv += "<div class='title-owl-carousel'>" + value[2] + "</div>";
                textDiv += "</div>";
                textDiv += "</a>";
                $('#related-video-mark').append(textDiv);
                $('#a_relatedvideos_' + value[0]).attr('onclick', "setVideosAutoPlay(true,'" + value[8] + "','" + value[10] + "','" + value[2] + "','" + value[7] + "','" + value[3] + "','" + value[0] + "')")

            })
            var ablock = $("#related-video-mark").find("a");
        });
    }
</script>
<!--<script>-->
<!--    $('#myAffix').affix({-->
<!--        offset: {-->
<!--            top: 100,-->
<!--            bottom: function () {-->
<!--                return (this.bottom = $('.footer').outerHeight(true))-->
<!--            }-->
<!--        }-->
<!--    })-->
<!--</script>-->

</body>
</html>
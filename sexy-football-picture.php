<?php
require_once 'model/GalleryM.php';
$GalleryObject = new GalleryM();
$gid = ($_REQUEST['id']) ? $_REQUEST['id'] : 0;
$webroot=$_SERVER['DOCUMENT_ROOT'];
if (isset($_GET['id'])) {
    $dataGallery = array();
    $dataGallery = $GalleryObject->getByGalleryId($_GET['id']);
    $dataGalleryStore = $GalleryObject->getMediaStoreByGalleryId($_GET['id']);

    if(!empty($dataGalleryStore) and !empty($dataGalleryStore[0]) and !file_exists($webroot.$dataGalleryStore[0]->path)){
        header('Location: /sexy-football');
        exit();
    }

    $dataMoreRecentedGallery = $GalleryObject->getMoreRecentedAllGallery($_GET['id'], 0, 6);
    $dataRelatedGalleryId = $GalleryObject->getMoreRecentedGalleryIdByDay($_GET['id'], 7);
    $dataRelatedGallery = $GalleryObject->getMoreRecentedGallery($dataGallery[0]->tag, $dataGallery[0]->id, 4);
    $dataRelatedVideos = $GalleryObject->getRelatedVideo($dataGallery[0]->tag, $dataGallery[0]->id, 4);
    $GalleryObject->updateView($gid);
} else {
    header("Location: /gallery");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="shortcut icon" type="images/x-icon" href="images/favicon.ico.png">
    <title><?php echo ((!empty($dataGallery)) ? $dataGallery[0]->title : ''); ?> เซ็กซี่ sexy picture  หลากหลาย พริตตี้ การันตีความน่ารัก</title>
    <meta name="keywords" content="สาวเซ็กซี่, เซ็กซี่เกิร์ล, sexy girl, สาวน่ารัก, ภาพเซ็กซี่, sexy sport">
    <meta name="description" content="<?php echo ((!empty($dataGallery)) ? $dataGallery[0]->title : ''); ?> สาวน่ารัก เน็ตไอดอล แหล่งรวมรูปภาพสาวเซ็กซี่ รูปภาพ Sexy Girl เซ็กซี่เกิร์ล สาวเซ็กซี่ พริตตี้ การันตีความน่ารัก สาวเซ็กซี่หลากหลายสไตล์">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <!--    <link rel="shortcut icon" type="images/x-icon" href="images/favicon.ico.png">-->
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="/js/flogin/src/flogin.js"></script>
    <script src="/js/moment.js"></script>
    <meta property="fb:app_id" content="1630409860331473"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url"
          content="<?= (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>"/>
    <meta property="og:title" content="<?php echo $dataGallery[0]->title ?>"/>
    <meta property="og:description" content="<?php echo $dataGallery[0]->desc ?>"/>
    <meta property="og:image"
          content="<?php echo((strpos($dataGalleryStore[0]->thumbnail, "http://") === 0) ? $dataGalleryStore[0]->thumbnail : "https://" . $_SERVER['SERVER_NAME'] . $dataGalleryStore[0]->thumbnail); ?>"/>
    <?php include 'view/index/googletagmanager.php'; ?>
</head>
<body>
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button"><span
            class="glyphicon glyphicon-chevron-up"></span></a>
<!--index menu-->
<?php //include './view/index/menu.php'; ?>
<?php include './view/index/menu-all.php'; ?>
<?php //include 'view/menu/navbar-menu-v2-fixed.php'; ?>

<div id="Main-articles">
    <div class="wrapper-news-articles news-articles-list mg-top-50" gid="<?php echo $gid ?>">

        <?php if ($dataGallery[0]->gall_type == 'video') { ?>
            <!--        section video sexy-->
            <div class="bg-video-sexy main_articles" type="<?php echo $dataGallery[0]->gall_type ?>"
                 newsid="<?php echo $dataGallery[0]->id ?>">
                <div class="wrap-content-cols-2">
                    <div class="bg-video-sexy">
                        <div class="box-large-videos-row">
                            <div class="box-large-videos">
                                <?php foreach ($dataGalleryStore as $key => $value) { ?>
                                    <div id="box-large-videos-autoPlay">
                                        <?php if ($value->typeVideo == "dailymotion") { ?>
                                            <iframe id="large-videos-autoPlay-<?php echo $dataGallery[0]->id; ?>"
                                                    width="100%"
                                                    height="800" data-autoplay="true"
                                                    src="http://www.dailymotion.com/embed/video/<?php echo $value->videokey ?>"
                                                    start="<?php echo $value->urlIframe; ?>"
                                                    stop="http://www.dailymotion.com/embed/video/<?php echo $value->videokey ?>"
                                                    frameborder="0"
                                                    allowfullscreen></iframe>
                                        <?php } elseif ($value->typeVideo == "twitter") { ?>
                                            <iframe id="large-videos-autoPlay-<?php echo $dataGallery[0]->id; ?>"
                                                    width="100%"
                                                    height="800" data-autoplay="true"
                                                    src="https://twitter.com/i/cards/tfw/v1/<?php echo $value->videokey ?>"
                                                    start="<?php echo $value->urlIframe; ?>"
                                                    stop="https://twitter.com/i/cards/tfw/v1/<?php echo $value->videokey ?>"
                                                    frameborder="0"
                                                    allowfullscreen></iframe>
                                        <?php } elseif ($value->typeVideo == "facebook") { ?>
                                            <iframe id="large-videos-autoPlay-<?php echo $dataGallery[0]->id; ?>"
                                                    width="100%"
                                                    height="800" data-autoplay="true"
                                                    src="https://www.facebook.com/v2.3/plugins/video.php?allowfullscreen=true&container_width=800&href=<?php echo $value->content ?>"
                                                    start="<?php echo $value->urlIframe; ?>"
                                                    stop="https://www.facebook.com/v2.3/plugins/video.php?allowfullscreen=true&container_width=800&href=<?php echo $value->content ?>"
                                                    frameborder="0"
                                                    allowfullscreen></iframe>
                                        <?php } elseif ($value->typeVideo == "youtube") { ?>
                                            <iframe id="large-videos-autoPlay-<?php echo $dataGallery[0]->id; ?>"
                                                    width="100%"
                                                    height="800" data-autoplay="true"
                                                    src="http://www.youtube.com/embed/<?php echo $value->videokey ?>"
                                                    start="<?php echo $value->urlIframe; ?>"
                                                    stop="http://www.youtube.com/embed/<?php echo $value->videokey ?>"
                                                    frameborder="0"
                                                    allowfullscreen></iframe>
                                        <?php } else if ($value->typeVideo == "streamable") { ?>
                                            <iframe id="large-videos-autoPlay-<?php echo $dataGallery[0]->id; ?>"
                                                    width="100%"
                                                    height="800" data-autoplay="true"
                                                    src="https://streamable.com/s/<?php echo $value->videokey ?>"
                                                    frameborder="0"
                                                    allowfullscreen></iframe>
                                        <?php } ?>
                                        <!--                                    <iframe width="100%" height="700" src="https://www.youtube.com/embed/HTL732cGiP8?list=PLKxjLIHxlwd4KNEvWzmMs7y5oWDGXyl36" frameborder="0" allowfullscreen></iframe>-->
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="load_video">
                            <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                        </div>
                    </div>
                </div>
            </div>
            <!--        end-->
        <?php } else { ?>
            <div class="containter-fluid">
                <div class="bx-head-fixtures bx-head-gallery">
                    <div class="hilight-cover-videos"></div>
                    <div class="articles-title">
                        <div class="container" style="padding: 0px;">
                            <div class="head-title-top pull-left">
                                <div style="font-size: 4em;"><?php echo $dataGallery[0]->title; ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="background-image_image2"
                         style="background-image: url('<?php echo $dataGallery[0]->thumbnail; ?>');"></div>
                </div>
            </div>
            <div style="clear: both"></div>
            <div class="container">
                <div class="wrap-content-cols">
                    <div class="rows">
                        <div class="col-sm-9 main_articles" type="<?php echo $dataGallery[0]->gall_type ?>"
                             newsid="<?php echo $dataGallery[0]->id ?>"
                             id="main_articles_<?php echo $dataGallery[0]->id ?>">
                            <div class="bx-publish-info">
                                <span class="pull-left"
                                      style="padding-top: 5px;"><?php echo date(' M j, Y - H:s A', strtotime($dataGallery[0]->created_at)) ?>
                                    Updated on <?php echo date(' M j, Y - H:s A', strtotime($dataGallery[0]->update_at)) ?></span>
                                <span class="bx-social pull-right">
                            <?php include 'view/social/bx-social.php'; ?>
                        </span>
                                <div style="clear: both;"></div>
                            </div>
                            <div class="content-articles">
                                <?php echo $dataGallery[0]->desc; ?>
                                <br><br>
                                <?php foreach ($dataGalleryStore as $key => $value) { ?>
                                    <center><img src="<?php echo $value->path; ?>"></center>
                                    <!--                                    <center>--><?php //echo $value->desc; ?><!--</center>-->
                                    <br><br>
                                <?php } ?>
                                <!--                        <center><img src="/images/gallery/4.jpg"></center>-->
                            </div>
                        </div>
                        <div class="col-sm-3 menu_articles" id="menu_articles_<?php echo $dataGallery[0]->id ?>"
                             style="position: relative;">
                            <div id="menu_articles_highlight_<?php echo $dataGallery[0]->id ?>"
                                 class="bx-publish-recommend"
                                 style="position: absolute;top: 0px;">
                                <div class="title">
                                    <h3>More Recented</h3>
                                </div>
                                <table>
                                    <?php foreach ($dataMoreRecentedGallery as $key => $value) { ?>
                                        <?php if(file_exists($webroot.$value->thumbnail)){ ?>
                                        <tr>
                                            <td>
                                                <a href="/sexy-football-picture?id=<?php echo $value->id ?>">
                                                    <div class="news-image">
                                                        <div class="crop">
                                                            <img src="<?php echo $value->thumbnail ?>">
                                                        </div>
                                                    </div>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="/sexy-football-picture?id=<?php echo $value->id ?>"><?php echo $value->title ?></a>
                                                <div class="times-content">
                                                    <span class="timeMoreRecentedGallery"
                                                          date="<?php echo $value->update_at ?>"></span>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>

                <div class="wrap-content-cols">
                    <div class="double-underline bx-tabs-related relate25">Related</div>
                    <div class="row">
                        <div class="label-news-vertical label-vdo-vertical">Gallery</div>
                        <?php foreach ($dataRelatedGallery as $key => $value) { ?>
                            <div class="col-sm-3 related-news">
                                <a href="/sexy-football-picture?id=<?php echo $value->id ?>">
                                    <div class="box-videos box-related box-related-sexy">
                                        <div class="bx-type-gallery">
                                            <i class="fa fa-picture-o"></i>
                                        </div>
                                        <div class="crop" style="background-image: url(<?php echo $value->thumbnail ?>)"></div>
                                    </div>
                                    <div class="title-owl-carousel title-h-videos">
                                        <a href="/sexy-football-picture?id=<?php echo $value->id ?>"><?php echo $value->title ?></a>
                                    </div>
                                    <div class="times-content">
                                            <span class="timeMoreRecentedGallery"
                                                  date="<?php echo $value->create_datetime; ?>"></span>
                                    </div>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                    <div style="clear: both;"></div>
                    <div class="row">
                        <?php if (!empty($dataRelatedVideos)) { ?>
                            <div class="label-news-clip">Video</div>
                            <?php foreach ($dataRelatedVideos as $key => $value) { ?>
                                <div class="col-sm-3">
                                    <a href="/sexy-football-videos?galleryId=<?php echo $value->id ?>">
                                        <div class="box-videos box-related box-related-sexy">
                                            <div class="bx-type-gallery">
                                                <i class="fa fa-play"></i>
                                            </div>
                                            <div class="crop" style="background-image: url(<?= $value->urlImg; ?>)"></div>
                                        </div>
                                        <div class="title-owl-carousel"><?php echo $value->title ?></div>
                                        <div class="times-content">
                                            <span class="timeMoreRecentedGallery"
                                                  date="<?php echo $value->create_datetime; ?>"></span>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>

            </div>
        <?php } ?>
        <div style="clear: both"></div>
    </div>
</div>
<?php //include 'footer.php';
include 'view/index/footer.php';
?>

<div id="Articles-BlackUp">
    <?php foreach ($dataRelatedGalleryId as $key => $value) { ?>
        <div newsId="<?php echo $value->id ?>" class="dataBlackUp" id="dataBlackUp_<?php echo $value->id ?>"
             style="display: none;"></div>
    <?php } ?>
</div>

<?php //include 'footer.php';
//include 'view/index/footer.php';
?>

<script>
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');

    });

    $(window).ready(function () {
        loadMoment();
        var stickyNavTop = $('#navbar').offset().top;
        var countPage = 0;
        var arrayWidth = [];
        var arrayPosition = [];
        var arrayMenuBox = [];
        var arrayIdNews = [];
        var arrayMainMenuBox = [];
        var arrayTypeGallery = [];

        function loadMoment() {
            $('.timeMoreRecentedGallery').each(function (index) {
                $(this).html(moment($(this).attr('date'), "YYYY-MM-DD mm:ss").fromNow())
            })
        }

        function stickyHighlight() {
            var scrollTop = $(window).scrollTop();
//            console.log(arrayMainMenuBox);
            scrollTop += 48;
            if (scrollTop > arrayPosition[countPage] && scrollTop < ((arrayPosition[countPage] + arrayMainMenuBox[countPage]) - arrayMenuBox[countPage])) {
                $('#menu_articles_highlight_' + arrayIdNews[countPage]).css('top', ((scrollTop) - arrayPosition[countPage]));
            }
        }

        $('.main_articles').each(function (index) {
            $('#menu_articles_' + $(this).attr('newsid')).css('height', $(this).height());
//            console.log($(this).height()+" :: "+$(this).width());
            arrayMainMenuBox.push($(this).height());
            arrayMenuBox.push($('#menu_articles_highlight_' + $(this).attr('newsId')).height());
            arrayPosition.push($(this).offset().top);
            arrayIdNews.push($(this).attr('newsId'));
            arrayTypeGallery.push($(this).attr('type'));
        });


//        console.log($( ".dataBlackUp:gt(0)" ).attr('newsId'));
//        console.log($( ".dataBlackUp:gt(1)" ).attr('newsId'));
//        console.log($( ".dataBlackUp:gt(2)" ).attr('newsId'));

        $(".dataBlackUp").each(function (index) {
            if (index < 3) {
                var object = $(this);
                $.ajax({
                    url: "/sexy-football-picture-list",
                    data: {id: $(this).attr('newsId')},
                    method: "GET",
                    dataType: "html"
                }).done(function (response) {
                    object.html(response);
                    loadMoment();
                });
            }
        })

        stickyNav();
//        stickyHighlight();
        function stickyNav() {
            var maxPage = 0;
            var windowHeight = jQuery(window).height();
            var scrollTop = $(window).scrollTop();
            if (scrollTop > stickyNavTop) {
                $('#navbarTopBar').css('top', 0);
            } else {
                $('#navbarTopBar').removeAttr('style');
            }
//            scrollTop += 605;

            $(".news-articles-list").each(function (index) {
                if (index <= countPage) {

                    var heightArticles = ($(this).height() + 50);
                    maxPage += heightArticles;
//                    console.log(index+'::'+heightArticles+"::"+maxPage+"::"+countPage);
                    arrayWidth[index] = heightArticles;
//                    console.log(arrayWidth);
                }
            });

            var reng = (maxPage - scrollTop) - windowHeight;
//            console.log(maxPage);
//            console.log(reng);

            if (reng < 100) {
                $(".dataBlackUp").each(function (index) {
                    if ($(this).html() !== "" && index == countPage) {
//                        console.log('add');
                        $('#Main-articles').append($(this).html());
                        $(this).html("");
                        arrayMenuBox = [];
                        arrayPosition = [];
                        arrayIdNews = [];
                        arrayMainMenuBox = [];
                        arrayTypeGallery = [];
                        $('#Main-articles').find('.main_articles').each(function (index) {
                            if ($(this).attr('type') == "picture") {
                                $('#menu_articles_' + $(this).attr('newsid')).css('height', $(this).height());
                                arrayMenuBox.push($('#menu_articles_highlight_' + $(this).attr('newsId')).height());
                            } else {
                                arrayMenuBox.push(0);
                            }
                            arrayPosition.push($(this).offset().top);
                            arrayIdNews.push($(this).attr('newsId'));
                            arrayMainMenuBox.push($(this).height());
                            arrayTypeGallery.push($(this).attr('type'));
                        });
                        $(".dataBlackUp").each(function (index) {
                            if (index == (countPage + 3)) {
                                var object = $(this);
                                $.ajax({
                                    url: "/sexy-football-picture-list",
                                    data: {id: $(this).attr('newsId')},
                                    method: "GET",
                                    dataType: "html"
                                }).done(function (response) {
                                    object.html(response);
                                    loadMoment();
                                });
                            }
                        })
                    }
                });
            }
//            console.log(arrayIdNews);
            if (maxPage > 0 && scrollTop > maxPage) {
                if ($('#large-videos-autoPlay-' + arrayIdNews[countPage]).length) {
                    $('#large-videos-autoPlay-' + arrayIdNews[countPage]).attr('src', $('#large-videos-autoPlay-' + arrayIdNews[countPage]).attr('stop'))
                }
                countPage += 1;
                if ($('#large-videos-autoPlay-' + arrayIdNews[countPage]).length) {
                    $('#large-videos-autoPlay-' + arrayIdNews[countPage]).attr('src', $('#large-videos-autoPlay-' + arrayIdNews[countPage]).attr('start'))
                }
//                console.log(arrayTypeGallery[countPage]);
            } else if (maxPage > 0 && scrollTop > 0 && scrollTop < (maxPage - arrayWidth[countPage])) {
                if ($('#large-videos-autoPlay-' + arrayIdNews[countPage]).length) {
                    $('#large-videos-autoPlay-' + arrayIdNews[countPage]).attr('src', $('#large-videos-autoPlay-' + arrayIdNews[countPage]).attr('stop'))
                }
                countPage -= 1;
                if ($('#large-videos-autoPlay-' + arrayIdNews[countPage]).length) {
                    $('#large-videos-autoPlay-' + arrayIdNews[countPage]).attr('src', $('#large-videos-autoPlay-' + arrayIdNews[countPage]).attr('start'))
                }
//                console.log(arrayTypeGallery[countPage]);
            }
            $('#loadingBar').css('width', (((((arrayWidth[countPage] - (maxPage - scrollTop)) - arrayWidth[countPage]) / arrayWidth[countPage]) * 100) + 100) + '%');
        };

        $(window).scroll(function () {
            stickyNav();
            stickyHighlight();
        });

    });
</script>

</body>
</html>
<?php
require_once 'model/GalleryM.php';
$GalleryObject = new GalleryM();
if (isset($_GET['id'])) {
    $dataGallery = array();
    $dataGallery = $GalleryObject->getByGalleryId($_GET['id']);
    $dataGalleryStore = $GalleryObject->getMediaStoreByGalleryId($_GET['id']);
    $dataMoreRecentedGallery = $GalleryObject->getMoreRecentedAllGallery($_GET['id'], 0, 6);
    $dataRelatedGallery = $GalleryObject->getMoreRecentedGallery($dataGallery[0]->tag, $dataGallery[0]->id, 4);
    $dataRelatedVideos = $GalleryObject->getRelatedVideo($dataGallery[0]->tag, $dataGallery[0]->id, 4);
//$dataRelatedGalleryId=$GalleryObject->getMoreRecentedGalleryId($_GET['id'],6);
} else {
    echo 'false';
    exit;
//    header("Location: /gallery.php");
}
?>
<div class="wrapper-news-articles news-articles-list" style="padding-top: 50px;">

    <?php if ($dataGallery[0]->gall_type == 'video') { ?>
        <!--        section video sexy-->
        <div class="container-fluid bg-video-sexy main_articles" type="<?php echo $dataGallery[0]->gall_type ?>"
             newsid="<?php echo $dataGallery[0]->id ?>">
            <div class="wrap-content-cols-2">
                <div class="bg-video-sexy">
                    <div class="box-large-videos-row">
                        <div class="box-large-videos">
                            <?php foreach ($dataGalleryStore as $key => $value) { ?>
                                <div id="box-large-videos-autoPlay"
                                     style="margin-top: 7%;margin-bottom: 5%;height: 100%;">
                                    <?php if ($value->typeVideo == "dailymotion") { ?>
                                        <iframe id="large-videos-autoPlay-<?php echo $dataGallery[0]->id; ?>"
                                                width="100%" height="800" data-autoplay="true"
                                                src="http://www.dailymotion.com/embed/video/<?php echo $value->videokey ?>"
                                                start="<?php echo $value->urlIframe; ?>"
                                                stop="http://www.dailymotion.com/embed/video/<?php echo $value->videokey ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } elseif ($value->typeVideo == "twitter") { ?>
                                        <iframe id="large-videos-autoPlay-<?php echo $dataGallery[0]->id; ?>"
                                                width="100%" height="800" data-autoplay="true"
                                                src="https://twitter.com/i/cards/tfw/v1/<?php echo $value->videokey ?>"
                                                start="<?php echo $value->urlIframe; ?>"
                                                stop="https://twitter.com/i/cards/tfw/v1/<?php echo $value->videokey ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } elseif ($value->typeVideo == "facebook") { ?>
                                        <iframe id="large-videos-autoPlay-<?php echo $dataGallery[0]->id; ?>"
                                                width="100%" height="800" data-autoplay="true"
                                                src="https://www.facebook.com/v2.3/plugins/video.php?allowfullscreen=true&container_width=800&href=<?php echo $value->content ?>"
                                                start="<?php echo $value->urlIframe; ?>"
                                                stop="https://www.facebook.com/v2.3/plugins/video.php?allowfullscreen=true&container_width=800&href=<?php echo $value->content ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } elseif ($value->typeVideo == "youtube") { ?>
                                        <iframe id="large-videos-autoPlay-<?php echo $dataGallery[0]->id; ?>"
                                                width="100%" height="800" data-autoplay="true"
                                                src="http://www.youtube.com/embed/<?php echo $value->videokey ?>"
                                                start="<?php echo $value->urlIframe; ?>"
                                                stop="http://www.youtube.com/embed/<?php echo $value->videokey ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } else if ($value->typeVideo == "streamable") { ?>
                                        <iframe id="large-videos-autoPlay-<?php echo $dataGallery[0]->id; ?>"
                                                width="100%" height="800" data-autoplay="true"
                                                src="https://streamable.com/s/<?php echo $value->videokey ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } ?>
                                    <!--                                    <iframe width="100%" height="700" src="https://www.youtube.com/embed/HTL732cGiP8?list=PLKxjLIHxlwd4KNEvWzmMs7y5oWDGXyl36" frameborder="0" allowfullscreen></iframe>-->
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="load_video">
                        <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                    </div>
                </div>
            </div>
        </div>
        <!--        end-->
    <?php } else { ?>
        <div class="containter-fluid">
            <div class="bx-head-fixtures bx-head-gallery">
                <div class="hilight-cover-videos"></div>
                <div class="articles-title">
                    <div class="container" style="padding: 0px;">
                        <div class="head-title-top pull-left">
                            <div style="font-size: 4em;"><?php echo $dataGallery[0]->title; ?></div>
                        </div>
                    </div>
                </div>
                <div class="background-image_image2"
                     style="background-image: url('<?php echo $dataGallery[0]->thumbnail; ?>');"></div>
            </div>
        </div>
        <div style="clear: both"></div>
        <div class="container">
            <div class="wrap-content-cols" style="margin-top: 20px;">
                <div class="rows">
                    <div class="col-sm-9 main_articles" type="<?php echo $dataGallery[0]->gall_type ?>"
                         newsid="<?php echo $dataGallery[0]->id ?>"
                         id="main_articles_<?php echo $dataGallery[0]->id ?>">
                        <div class="bx-publish-info">
                            <span class="pull-left"
                                  style="padding-top: 5px;"><?php echo date(' M j, Y - H:s A', strtotime($dataGallery[0]->created_at)) ?>
                                Updated on <?php echo date(' M j, Y - H:s A', strtotime($dataGallery[0]->update_at)) ?></span>
                            <span class="bx-social pull-right">
<!--                        <span style="color: #3B579D;"><i class="fa fa-facebook"></i> </span>-->
                                <!--                        <span style="color: #4AC7F9;"><i class="fa fa-twitter"></i></span>-->
                                <!--                        <span> <i class="fa fa-mail-forward"></i> </span>-->
                    </span>

                            <div style="clear: both;"></div>
                        </div>
                        <div class="content-articles">
                            <?php echo $dataGallery[0]->desc; ?>
                            <br><br>
                            <?php foreach ($dataGalleryStore as $key => $value) { ?>
                                <center><img src="<?php echo $value->path; ?>"></center>
                                <!--                            <center>--><?php //echo $value->desc; ?><!--</center>-->
                                <br><br>
                            <?php } ?>
                            <!--                        <center><img src="/images/gallery/4.jpg"></center>-->
                        </div>
                    </div>
                    <div class="col-sm-3 menu_articles" id="menu_articles_<?php echo $dataGallery[0]->id ?>"
                         style="position: relative;">
                        <div id="menu_articles_highlight_<?php echo $dataGallery[0]->id ?>" class="bx-publish-recommend"
                             style="position: absolute;top: 0px;">
                            <div class="title">
                                <h3>More Recented</h3>
                            </div>
                            <table>
                                <?php foreach ($dataMoreRecentedGallery as $key => $value) { ?>
                                    <tr>
                                        <td>
                                            <a href="/sexy-football-picture?id=<?php echo $value->id ?>">
                                                <div class="news-image">
                                                    <div class="crop">
                                                        <img src="<?php echo $value->thumbnail ?>">
                                                    </div>
                                                </div>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="/sexy-football-picture?id=<?php echo $value->id ?>"><?php echo $value->title ?></a>
                                            <div class="times-content">
                                                <span class="timeMoreRecentedGallery"
                                                      date="<?php echo $value->update_at ?>">

                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>
            <div class="wrap-content-cols">
                <div class="double-underline bx-tabs-related relate25">Related</div>
                <div class="row">
                    <div class="label-news-vertical label-vdo-vertical">Gallery</div>
                    <?php foreach ($dataRelatedGallery as $key => $value) { ?>
                        <div class="col-sm-3 related-news">
                            <a href="/sexy-football-picture?id=<?php echo $value->id ?>">
                                <div class="box-videos box-related box-related-sexy">
                                    <div class="bx-type-gallery">
                                        <i class="fa fa-picture-o"></i>
                                    </div>
                                    <div class="crop" style="background-image: url(<?php echo $value->thumbnail ?>)"></div>
                                </div>
                                <div class="title-owl-carousel title-h-videos">
                                    <a href="/sexy-football-picture?id=<?php echo $value->id ?>"><?php echo $value->title ?></a>
                                </div>
                                <div class="times-content">
                                 <span class="timeMoreRecentedGallery"
                                       date="<?php echo $value->create_datetime; ?>">

                                        </span>
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                </div>
                <div style="clear: both;"></div>
                <div class="row">
                    <?php if (!empty($dataRelatedVideos)) { ?>
                        <div class="label-news-clip">Video</div>
                        <?php foreach ($dataRelatedVideos as $key => $value) { ?>
                            <div class="col-sm-3">
                                <a href="/sexy-football-videos?galleryId=<?php echo $value->id ?>">
                                    <div class="box-videos box-related box-related-sexy">
                                        <div class="bx-type-gallery">
                                            <i class="fa fa-play"></i>
                                        </div>
                                        <div class="crop"
                                             style="background-image: url(<?= $value->urlImg; ?>);"></div>
                                    </div>
                                    <div class="title-owl-carousel"><?php echo $value->title ?></div>
                                    <div class="times-content">
                                        <span class="timeMoreRecentedGallery"
                                              date="<?php echo $value->create_datetime; ?>">

                                        </span>
                                    </div>
                                </a>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>

        </div>
    <?php } ?>
    <div style="clear: both"></div>
</div>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <title>ผลบอลสด Livescore คู่ <?= $matchView->teamHomeEn ?> พบ <?= $matchView->teamAwayEn ?>  ลีค ฟุตบอล<?= $matchView->name ?> วันที่ <?= date("Y-m-d", (strtotime($matchView->time_match) - (60 * 60))) ?>  ผลบอล บ้านผลบอล ที่ ngoal</title>
    <meta name="keywords" content="ผลบอลสด, บอลสด, Livescore,บ้านผลบอล,ผลบอลย้อนหลัง,โปรแกรมบอลล่วงหน้า,ตารางการแข่งขัน,ผลบอลวันนี้,ราคาบอลไหล,อัตราต่อรอง">
    <meta name="description" content="ดูผลบอลสด livescore คู่ <?= $matchView->teamHomeEn ?> พบ <?= $matchView->teamAwayEn ?>  ลีค ฟุตบอล<?= $matchView->name ?> วันที่ <?= date("Y-m-d", (strtotime($matchView->time_match) - (60 * 60))) ?>  ผลบอลย้อนหลัง ผลบอลเมื่อคืน  โปรแกรมบอลล่วงหน้า อัตราต่อรอง ราคาบอลไหลที่รวดเร็ว ทุกคู่ทุกลีก รวมทั้งรายงานใบเหลืองใบแดง การทำประตู รายละเอียดครบครัน">
    <link rel="shortcut icon" type="images/x-icon" href="/images/favicon.ico.png">

    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="../css/style-new.css">
    <link rel="stylesheet" href="../css/view-all.css">
    <link rel="stylesheet" href="../css/livescore.css">
    <link rel="stylesheet" href="../css/reset.css">
    <link rel="stylesheet" href="../css/articles.css">
    <link rel="stylesheet" href="../css/fixtures.css">
    <link rel="stylesheet" href="../css/gallery.css">
    <link rel="stylesheet" href="../css/game-redesign.css">
    <link rel="stylesheet" href="../nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="../css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="../css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!--js-->
    <script src="../js/jquery/dist/jquery.min.js"></script>
    <script src="../js/jquery-ui/jquery-ui.min.js"></script>
    <script src="../css/bootstrap/js/bootstrap.min.js"></script>
    <script src="../css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="../nanoscrollbar/nanoscrollbar.js"></script>
    <script src="../js/utils.js"></script>
    <script src="../js/moment.js"></script>
    <script src="../js/timeview.js"></script>
    <script src="../js/game.js"></script>
    <script src="../js/sly/sly.js"></script>
    <?php include 'view/index/googletagmanager.php'; ?>
</head>
<body>


<link rel="stylesheet" href="/css/sidebar-menu.css">
<script src="../js/newFB.js"></script>
<script>
    $('#myAffix').affix({
        offset: {
            top: 100,
            bottom: function () {
                return (this.bottom = $('.footer').outerHeight(true))
            }
        }
    })

    $(document).ready(function () {
        var trigger = $('.hamburger'),
            overlay = $('.overlay'),
            isClosed = false;

        trigger.click(function () {
            hamburger_cross();
        });

        function hamburger_cross() {

            if (isClosed == true) {
                overlay.hide();
                trigger.removeClass('is-open');
                trigger.addClass('is-closed');
                isClosed = false;
            } else {
                overlay.show();
                trigger.removeClass('is-closed');
                trigger.addClass('is-open');
                isClosed = true;
            }
        }

        $('[data-toggle="offcanvas"]').click(function () {
            $('#wrapper').toggleClass('toggled');
        });
    });
</script>
<input type="hidden" id="statusLogin" value="false">
<input type="hidden" id="getMatchId" value="<?php echo ((!empty($matchView)?$matchView->id:''))?>">
<div class="content-menu-index content-menu-all" data-spy="affix" data-offset-top="60">
    <nav class="navbar navbar-topbar">
        <div class="navbar-header">
            <div class="pull-left">
                <a class="navbar-brand" href="#">
                    <img src="/images/logo/logo-ngoal.png">
                </a>
            </div>
            <div class="pull-right navbar-header-right">
                <ul>

                    <li>
                        <div class="box-login-FB">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                            <span>Login</span>
                        </div>
                    </li>
                    <li>
                        <div class="user-topmenu collapsed" data-toggle="collapse"
                             data-target="#userbar" aria-expanded="false" aria-controls="userbar">
                            <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                        </div>
                    </li>
                    <li>
                        <div class="collapsed collapse-menu-btn" data-toggle="collapse"
                             data-target="#navbar"
                             aria-expanded="false" aria-controls="navbar">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </div>
                    </li>
                </ul>

            </div>
        </div>
        <div id="userbar" class="userbar-collapse collapse">
            <div class="userbar-collapse-body">
                <ul>
                    <li class="user-detail text-center">
                        <img src="/images/9.jpg">
                        <span><strong>Wayne Rooney</strong></span>
                    </li>
                    <li class="text-center user-coin-box">
                        <table>
                            <tr>
                                <td><img src="/images/coin/sgold100.png"><span>2,668.00</span></td>
                            </tr>
                            <tr>
                                <td><img src="/images/coin/scoin100.png"><span>3,200.00</span></td>
                            </tr>
                        </table>
                    </li>
                    <li>
                        <button type="button" class="btn btn-primary btn-lg">Edit Profile</button>
                    </li>
                    <li>
                        <button type="button" class="btn btn-danger btn-lg">Log out</button>
                    </li>
                </ul>
            </div>
        </div>
        <!--        เมนูใน smartphone-->
        <div id="navbar" class="collapse collapse-menu">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <!--                หน้าหลัก-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="/">หน้าหลัก</a>
                        </h4>
                    </div>
                </div>
                <!--                วีดีโอ/ไฮไลท์บอล-->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                วีดีโอ/ไฮไลท์บอล
                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <ul>
                                <li><a href="/highlight?category=premier league">พรีเมียร์ลีก</a></li>
                                <li><a href="/highlight?category=la liga">ลาลีกา</a></li>
                                <li><a href="/highlight?category=bundesliga">บุนเดสลีกา</a></li>
                                <li><a href="/highlight?category=serie a">ซีเรียอา</a></li>
                                <li><a href="/highlight?category=league 1">ลีกเอิง</a></li>
                                <li><a href="/highlight?category=thai">ไทยพรีเมียลีก</a></li>
                                <li><a href="/highlight?category=ucl">ยูฟ่าแชมเปียนลีก</a></li>
                                <li><a href="/highlight?category=europa">ยูโรป้า</a></li>
                                <li><a href="/highlight?category=international">ฟุตบอลทีมชาติ</a></li>
                                <li><a href="/highlight#videosOthersTab">HotHit Video</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--                ข่าวบอลต่างประเทศ-->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                ข่าวบอลต่างประเทศ
                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingThree">
                        <div class="panel-body">
                            <ul>
                                <li><a href="/news?type=premier league&Page=1">ข่าวพรีเมียร์ลีก</a></li>
                                <li><a href="/news?type=la liga&Page=1">ข่าวลาลีกา</a></li>
                                <li><a href="/news?type=bundesliga&Page=1">ข่าวบุนเดสลีกา</a></li>
                                <li><a href="/news?type=serie a&Page=1">ข่าวซีเรียอา</a></li>
                                <li><a href="/news?type=ucl&Page=1">ข่าวยูฟ่าแชมเปียนลีก</a></li>
                                <li><a href="/news?type=europa&Page=1">ข่าวยูโรป้า</a></li>
                                <li><a href="/news?type=thai&Page=1">ข่าวฟุตบอลทีมชาติ</a></li>
                                <li><a href="/news?type=other league&Page=1">ข่าวฟุตบอลลีกอื่น</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--                ข่าวบอลไทย-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="/news?type=thai&Page=1">ข่าวบอลไทย</a>
                        </h4>
                    </div>
                </div>
                <!--                ข่าวกีฬาอื่นๆ-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="/news?type=other&Page=1">ข่าวกีฬาอื่นๆ</a>
                        </h4>
                    </div>
                </div>
                <!--                Sexy Football-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#">Sexy Football</a>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-wrapper-menu">
            <!--   แทบบนสุด-->
            <div class="topmenu">
                <div class="container">
                    <div class="pull-left contact-social">
                        <ul>
                            <li>
                                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="pull-right box-login-FB content-profile-menu">
                        <?php if (!isset($_SESSION['login'])) { ?>
                            <a class="fb-login-button" onclick="FBlogin();">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                <span>Login</span>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>


            <!--/.navbar-collapse -->
            <?php
            if (file_exists($webroot . '/load/main-menu-index.html')) {
                echo "<span style='display: none;'>/load/main-menu-index.html</span>";
                include $_SERVER["DOCUMENT_ROOT"] . '/load/main-menu-index.html';
            } else { ?>
                <div class="main-menu-index navbar-top" style="padding-left: 0;">
                    <div class="container container-wide">
                        <div class="pull-left logo-menu">
                            <a class="link_wrap" href="/"></a>
                            <img src="/images/logo/logo-ngoal-white.png">
                        </div>
                        <div class="nav-menutop nav-menutop-edit pull-left nav-menu-main">
                            <ul class="nav nav-pills">
                                <li class="none-dropdown"><a href="/index">หน้าหลัก</a></li>
                                <li class="active-dropdown">
                                    <!--                    <a href="/videos">-->
                                    <a href="javascript:void(0)">
                                        <div class="hot-icon">
                                            <i class="fa fa-circle" aria-hidden="true"></i>
                                        </div>
                                        วีดีโอ/ไฮไลท์บอล
                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                        <div class="bx-menu-dropdown-arrow">
                                            <i class="fa fa-caret-up" aria-hidden="true"></i>
                                        </div>

                                    </a>
                                    <div class="bx-menu-dropdown">
                                        <div class="container">
                                            <div class="col-sm-2 list-content-big-menu">
                                                <h3>วีดีโอ/ไฮไลท์บอล</h3>
                                                <ul>
                                                    <li><a href="/highlight">ไฮไลท์บอลฟุตบอลทั้งหมด</a></li>
                                                    <li class="active">
                                                        <a class="menu-highlight"
                                                           data-category="premier_league">พรีเมียร์ลีก</a>
                                                    </li>
                                                    <li>
                                                        <a class="menu-highlight"
                                                           data-category="la_liga">ลาลีกา</a>
                                                    </li>
                                                    <li>
                                                        <a class="menu-highlight"
                                                           data-category="bundesliga">บุนเดสลีกา</a>
                                                    </li>
                                                    <li>
                                                        <a style="cursor: pointer;" class="menu-highlight"
                                                           data-category="serie_a">ซีเรียอา</a>
                                                    </li>
                                                    <li><a style="cursor: pointer;" class="menu-highlight"
                                                           data-category="league_1">ลีกเอิง</a>
                                                    </li>
                                                    <li>
                                                        <a style="cursor: pointer;" class="menu-highlight" data-category="thai">ไทยพรีเมียลีก</a>
                                                    </li>
                                                    <li>
                                                        <a style="cursor: pointer;" class="menu-highlight"
                                                           data-category="ucl">ยูฟ่าแชมเปียนลีก</a>
                                                    </li>
                                                    <li>
                                                        <a style="cursor: pointer;" class="menu-highlight"
                                                           data-category="europa">ยูโรป้า</a>
                                                    </li>
                                                    <li>
                                                        <a style="cursor: pointer;" class="menu-highlight"
                                                           data-category="international">ฟุตบอลทีมชาติ</a>
                                                    </li>

                                                </ul>
                                            </div>
                                            <?php if (!empty($videoHighlight)) { ?>
                                                <?php foreach ($videoHighlight as $key => $value) { ?>
                                                    <div class="div-highlight col-sm-6 detail-content-big-menu <?= (($key == "premier_league") ? '' : 'hide'); ?>"
                                                         id="highlight-<?= $key ?>">
                                                        <div class="col-sm-12 topic-content-big-menu">
                                                            <h3>
                                                                <i class="fa fa-youtube-play" aria-hidden="true"></i>
                                                                <?php if ($key == "premier_league") { ?>
                                                                    ไฮไลท์ พรีเมียร์ลีก
                                                                <?php } elseif ($key == "la_liga") { ?>
                                                                    ไฮไลท์ ลาลีกา
                                                                <?php } elseif ($key == "bundesliga") { ?>
                                                                    ไฮไลท์ บุนเดสลีกา
                                                                <?php } elseif ($key == "serie_a") { ?>
                                                                    ไฮไลท์ ซีเรียอา
                                                                <?php } elseif ($key == "league_1") { ?>
                                                                    ไฮไลท์ ลีกเอิง
                                                                <?php } elseif ($key == "thai") { ?>
                                                                    ไฮไลท์ ไทยพรีเมียลีก
                                                                <?php } elseif ($key == "ucl") { ?>
                                                                    ไฮไลท์ ยูฟ่าแชมเปียนลีก
                                                                <?php } elseif ($key == "europa") { ?>
                                                                    ไฮไลท์ ยูโรป้า
                                                                <?php } elseif ($key == "international") { ?>
                                                                    ไฮไลท์ ฟุตบอลทีมชาติ
                                                                <?php } ?>
                                                            </h3>
                                                            <a class="top-readmore"
                                                               href="/highlight?category=<?= str_replace('_', ' ', $key); ?>"><span>ทั้งหมด</span></a>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="thumbnail">
                                                                        <a class="link_wrap"
                                                                           href="/highlight?videoId=<?= $value[0]->video_id ?>"></a>
                                                                        <div class="box-videos box-related box-related-sexy">
                                                                            <div class="hilight-cover"></div>
                                                                            <div class="icon-play">
                                                                                <i class="fa fa-play"></i>
                                                                            </div>
                                                                            <div class="crop">
                                                                                <img src="<?= $value[0]->thumbnail ?>">
                                                                            </div>

                                                                        </div>
                                                                        <div class="caption">
                                                                            <h3><?= $value[0]->title ?></h3>
                                                                        </div>
                                                                        <div class="btn btn-default btn-sm">
                                                                            อ่านทั้งหมด
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">

                                                                    <div class="thumbnail">
                                                                        <a class="link_wrap"
                                                                           href="/highlight?videoId=<?= $value[1]->video_id ?>"></a>
                                                                        <div class="box-videos box-related box-related-sexy">
                                                                            <div class="hilight-cover"></div>
                                                                            <div class="icon-play">
                                                                                <i class="fa fa-play"></i>
                                                                            </div>
                                                                            <div class="crop">
                                                                                <img src="<?= $value[1]->thumbnail ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="caption">
                                                                            <h3><?= $value[1]->title ?></h3>
                                                                        </div>
                                                                        <div class="btn btn-default btn-sm">
                                                                            อ่านทั้งหมด
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                            <div class="col-sm-4 detail-content-big-menu">
                                                <div class="col-sm-12 topic-content-big-menu">
                                                    <h3>
                                                        <i class="fa fa-fire" aria-hidden="true"></i> HotHit Video
                                                    </h3>
                                                    <a class="top-readmore" href="/hothit"><span>ทั้งหมด</span></a>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-sm-12">


                                                            <div class="thumbnail">
                                                                <a class="link_wrap"
                                                                   href="/hothit-videos?videoId=<?= $lastVideo[0]->video_id ?>"></a>
                                                                <div class="box-videos" style="height: 204px; overflow: hidden;">
                                                                    <!--                                                                    <div class="hilight-cover"></div>-->
                                                                    <div class="icon-play">
                                                                        <i class="fa fa-play"></i>
                                                                    </div>
                                                                    <!--                                                    <div class="cover-hothit"><img-->
                                                                    <!--                                                                src="/images/All_.png"></div>-->
                                                                    <div class="crop">
                                                                        <img src="<?= $lastVideo[0]->urlImg ?>">
                                                                    </div>

                                                                </div>
                                                                <div class="bx-detail-gallery">
                                                                    <h5><?= $lastVideo[0]->title ?></h5>
                                                                </div>
                                                                <div class="btn btn-default btn-sm">
                                                                    อ่านทั้งหมด
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="active-dropdown">
                                    <a href="javascript:void(0)">
                                        ข่าวบอลต่างประเทศ
                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                        <div class="bx-menu-dropdown-arrow">
                                            <i class="fa fa-caret-up" aria-hidden="true"></i>
                                        </div>
                                    </a>
                                    <div class="bx-menu-dropdown">
                                        <div class="container">
                                            <div class="col-sm-2 list-content-big-menu">
                                                <h3>ข่าวฟุตบอลต่างประเทศ</h3>
                                                <ul>
                                                    <li class="active">
                                                        <a style="cursor: pointer;" class="menu-videoNews"
                                                           data-category="premier_league">ข่าวพรีเมียร์ลีก</a>
                                                    </li>
                                                    <li>
                                                        <a style="cursor: pointer;" class="menu-videoNews"
                                                           data-category="la_liga">ข่าวลาลีกา</a>
                                                    </li>
                                                    <li>
                                                        <a style="cursor: pointer;" class="menu-videoNews"
                                                           data-category="bundesliga">ข่าวบุนเดสลีกา</a>
                                                    </li>
                                                    <li>
                                                        <a style="cursor: pointer;" class="menu-videoNews"
                                                           data-category="serie_a">ข่าวซีเรียอา</a>
                                                    </li>
                                                    <li>
                                                        <a style="cursor: pointer;" class="menu-videoNews"
                                                           data-category="ucl">ข่าวยูฟ่าแชมเปียนลีก</a>
                                                    </li>
                                                    <li>
                                                        <a style="cursor: pointer;" class="menu-videoNews"
                                                           data-category="europa">ข่าวยูโรป้า</a>
                                                    </li>
                                                    <li>
                                                        <a style="cursor: pointer;" class="menu-videoNews"
                                                           data-category="thai">ข่าวไทยพรีเมียลีก</a>
                                                    </li>
                                                    <li>
                                                        <a style="cursor: pointer;" class="menu-videoNews"
                                                           data-category="ucl">ข่าวยูฟ่าแชมเปียนลีก</a>
                                                    </li>
                                                    <li>
                                                        <a style="cursor: pointer;" class="menu-videoNews"
                                                           data-category="ucl">ข่าวทีมชาติ</a>
                                                    </li>
                                                    <li>
                                                        <a style="cursor: pointer;" class="menu-videoNews"
                                                           data-category="other">ข่าวฟุตบอลลีกอื่น</a>
                                                    </li>

                                                </ul>
                                            </div>
                                            <?php foreach ($videoNews as $key => $value) { ?>
                                                <div class="div-videoNews col-sm-10 detail-content-big-menu <?= (($key == "premier_league") ? '' : 'hide'); ?>"
                                                     id="videoNews-<?= $key ?>">
                                                    <div class="col-sm-12 topic-content-big-menu">
                                                        <h3>
                                                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                                                            <?php if ($key == "premier_league") { ?>
                                                                ข่าว พรีเมียร์ลีก
                                                            <?php } elseif ($key == "la_liga") { ?>
                                                                ข่าว ลาลีกา
                                                            <?php } elseif ($key == "bundesliga") { ?>
                                                                ข่าว บุนเดสลีกา
                                                            <?php } elseif ($key == "serie_a") { ?>
                                                                ข่าว ซีเรียอา
                                                            <?php } elseif ($key == "league_1") { ?>
                                                                ข่าว ลีกเอิง
                                                            <?php } elseif ($key == "thai") { ?>
                                                                ข่าว ไทยพรีเมียลีก
                                                            <?php } elseif ($key == "ucl") { ?>
                                                                ข่าว ยูฟ่าแชมเปียนลีก
                                                            <?php } elseif ($key == "europa") { ?>
                                                                ข่าว ยูโรป้า
                                                            <?php } elseif ($key == "international") { ?>
                                                                ข่าว ฟุตบอลทีมชาติ
                                                            <?php } ?>
                                                        </h3>
                                                        <a class="top-readmore"
                                                           href="/news?type=<?= str_replace('_', ' ', $key); ?>"><span>ดูทั้งหมด</span></a>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <div class="thumbnail">
                                                                    <a class="link_wrap"
                                                                       href="/articles?newsid=<?= $value[0]->newsid ?>"></a>
                                                                    <div class="wrap-box-gallery bx-gallery bx-gallery-submenu">
                                                                        <div class="hilight-cover"></div>
                                                                        <div class="crop"
                                                                             style="background-image: url(<?= $value[0]->imageLink ?>)"></div>
                                                                    </div>
                                                                    <div class="caption">
                                                                        <h3><?= $value[0]->titleTh ?></h3>
                                                                    </div>
                                                                    <div class="btn btn-default btn-sm">
                                                                        อ่านทั้งหมด
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="thumbnail">
                                                                    <a class="link_wrap"
                                                                       href="/articles?newsid=<?= $value[1]->newsid ?>"></a>
                                                                    <div class="wrap-box-gallery bx-gallery bx-gallery-submenu zoom">
                                                                        <div class="hilight-cover"></div>
                                                                        <div class="crop"
                                                                             style="background-image: url(<?= $value[1]->imageLink ?>)"></div>
                                                                    </div>
                                                                    <div class="caption">
                                                                        <h3><?= $value[1]->titleTh ?></h3>
                                                                    </div>
                                                                    <div class="btn btn-default btn-sm">
                                                                        อ่านทั้งหมด
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="thumbnail">
                                                                    <a class="link_wrap"
                                                                       href="/articles?newsid=<?= $value[2]->newsid ?>"></a>
                                                                    <div class="wrap-box-gallery bx-gallery bx-gallery-submenu zoom">
                                                                        <div class="hilight-cover"></div>
                                                                        <div class="crop"
                                                                             style="background-image: url(<?= $value[2]->imageLink ?>)"></div>
                                                                    </div>
                                                                    <div class="caption">
                                                                        <h3><?= $value[2]->titleTh ?></h3>
                                                                        <div class="btn btn-default btn-sm">
                                                                            อ่านทั้งหมด
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                </li>
                                <li class="none-dropdown"><a href="/news?type=thai&Page=1">ข่าวบอลไทย</a></li>
                                <!--                <li><a href="/news?type=other&Page=1">ข่าวกีฬาอื่นๆ</a></li>-->

                                <!--                sexy-->
                                <li class="active-dropdown">
                                    <!--                    href="/gallery"-->
                                    <a href="/sexy-football">
                                        Sexy Football
                                        <!--                        <i class="fa fa-chevron-down" aria-hidden="true"></i>-->
                                        <!--                        <div class="bx-menu-dropdown-arrow">-->
                                        <!--                            <i class="fa fa-caret-up" aria-hidden="true"></i>-->
                                        <!--                        </div>-->
                                    </a>
                                    <div class="hide bx-menu-dropdown">
                                        <div class="container">
                                            <div class="col-sm-2 list-content-big-menu">
                                                <h3>Sexy Football</h3>
                                                <ul>
                                                    <li>
                                                        <a class="menu-sexy"
                                                           data-category="picture">Sexy Picture</a>
                                                    </li>
                                                    <li class="active">
                                                        <a class="menu-sexy" data-category="video">Sexy
                                                            Video</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!--                                        Picpost-->
                                            <div class="div-sexy col-sm-10 detail-content-big-menu hide" id="sexy-picture">
                                                <div class="col-sm-12 topic-content-big-menu">
                                                    <h3>
                                                        <i class="fa fa-picture-o"></i> Sexy Picture
                                                    </h3>
                                                    <a class="top-readmore"
                                                       href="/sexy-football?type=picture"><span>ดูทั้งหมด</span></a>


                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <?php foreach ($dataSexyPicture as $key => $value) { ?>
                                                            <div class="col-sm-4">
                                                                <a class="link_wrap"
                                                                   href="/sexy-football-picture?id=<?= $value->id ?>"></a>
                                                                <div class="thumbnail">
                                                                    <div class="wrap-box-gallery bx-gallery bx-gallery-submenu">
                                                                        <div class="bx-type-gallery">
                                                                            <i class="fa fa-picture-o"></i>
                                                                        </div>
                                                                        <div class="crop"
                                                                             style="background-image: url(<?php echo $value->thumbnail ?>)"></div>
                                                                    </div>
                                                                    <div class="bx-detail-gallery">
                                                                        <h5><?= $value->title ?></h5>
                                                                    </div>
                                                                    <div class="btn btn-default btn-sm">
                                                                        อ่านทั้งหมด
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--                                        video-->
                                            <div class="div-sexy col-sm-10 detail-content-big-menu" id="sexy-video">
                                                <div class="col-sm-12 topic-content-big-menu">
                                                    <h3>
                                                        <i class="fa fa-youtube-play" aria-hidden="true"></i> Sexy Video
                                                    </h3>
                                                    <a class="top-readmore" href="/sexy-football?type=video"><span>ดูทั้งหมด</span></a>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <?php foreach ($dataSexyVideo as $key => $value) { ?>
                                                            <div class="col-sm-4">
                                                                <a class="link_wrap"
                                                                   href="/other-highlight-sexy?galleryId=<?= $value->id ?>"></a>
                                                                <div class="thumbnail">
                                                                    <div class="wrap-box-gallery bx-gallery bx-gallery-submenu">
                                                                        <div class="bx-type-gallery">
                                                                            <i class="fa fa-play"></i>
                                                                        </div>
                                                                        <div class="crop"
                                                                             style="background-image: url(<?= $value->urlImg ?>)"></div>
                                                                    </div>
                                                                    <div class="bx-detail-gallery">
                                                                        <h5><?= $value->title ?></h5>
                                                                    </div>
                                                                    <div class="btn btn-default btn-sm">
                                                                        อ่านทั้งหมด
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="active-dropdown">
                                    <!--                    <a href="/games?mid=-->
                                    <?php //echo((!empty($match[0])) ? $match[0]->id : '') ?><!--">-->
                                    <a href="javascript:void(0)">
                                        <div class="hot-icon">
                                            <i class="fa fa-circle" aria-hidden="true"></i>
                                        </div>
                                        เกมทายผลบอล
                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                        <div class="bx-menu-dropdown-arrow">
                                            <i class="fa fa-caret-up" aria-hidden="true"></i>
                                        </div>
                                    </a>
                                    <div class="bx-menu-dropdown bx-menu-dropdown-match-football">

                                        <div class="container">
                                            <div class="col-sm-2 list-content-big-menu">
                                                <h3>เกมทายผลบอล</h3>
                                                <ul>
                                                    <li class="active">
                                                        <a>เกมทายผลบอล</a>
                                                    </li>
                                                    <li>
                                                        <a href="/rules">กติกาและเงื่อนไข</a>
                                                    </li>
                                                    <li>
                                                        <a href="/ranking">Ranking</a>
                                                    </li>
                                                    <li>
                                                        <a href="/help">วิธีการเล่นเกมทายผล</a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="col-sm-6 detail-content-big-menu detail-march-content">
                                                <div class="col-sm-12 topic-content-big-menu">
                                                    <h3>
                                                        <i class="fa fa-gamepad" aria-hidden="true"></i> March Football
                                                    </h3>
                                                    <a class="top-readmore"
                                                       href="/livescore"><span>ทั้งหมด</span></a>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <?php if (!empty($match[0])) { ?>
                                                            <div class="col-sm-6"
                                                                 style="border-right: solid 1px rgba(255, 255, 255, 0.1);">
                                                                <h5><?= $match[0]->name ?></h5>
                                                                <ul class="topic">
                                                                    <li>
                                                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                                        <?= date("H:i", (strtotime($match[0]->time_match) - (60 * 60))) ?>
                                                                    </li>
                                                                    <li>|</li>
                                                                    <li><i class="fa fa-calendar" aria-hidden="true"></i>
                                                                        <?= date("d/m/Y", (strtotime($match[0]->time_match) - (60 * 60))) ?>
                                                                    </li>
                                                                </ul>

                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <img src="<?= (!empty($match[0]->teamHomePath)?$match[0]->teamHomePath:'/images/team/team_default_64x64.png'); ?>">
                                                                            <h4 class="<?= ((!empty($match[0]->handicap) && $match[0]->handicap < 0) ? 'active' : '') ?>"><?= $match[0]->teamHomeEn ?></h4>
                                                                        </td>
                                                                        <td class="box-vs">VS</td>
                                                                        <td>
                                                                            <img src="<?= (!empty($match[0]->teamAwayPath)?$match[0]->teamAwayPath:'/images/team/team_default_64x64.png'); ?>">
                                                                            <h4 class="<?= ((!empty($match[0]->handicap) && $match[0]->handicap > 0) ? 'active' : '') ?>"><?= $match[0]->teamAwayEn ?></h4>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <p class="text-center">
                                                                    <a href="/games?mid=<?= $match[0]->id ?>"
                                                                       class="btn btn-default btn-sm">ทายผลบอล</a>
                                                                </p>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if (!empty($match[1])) { ?>
                                                            <div class="col-sm-6">
                                                                <h5><?= $match[1]->name ?></h5>
                                                                <ul class="topic">
                                                                    <li>
                                                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                                        <?= date("H:i", (strtotime($match[1]->time_match) - (60 * 60))) ?>
                                                                    </li>
                                                                    <li>|</li>
                                                                    <li><i class="fa fa-calendar" aria-hidden="true"></i>
                                                                        <?= date("d/m/Y", (strtotime($match[1]->time_match) - (60 * 60))) ?>
                                                                    </li>
                                                                </ul>

                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <img src="<?= (!empty($match[1]->teamHomePath)?$match[1]->teamHomePath:'/images/team/team_default_64x64.png'); ?>">
                                                                            <h4 class="<?= ((!empty($match[1]->handicap) && $match[1]->handicap < 0) ? 'active' : '') ?>"><?= $match[1]->teamHomeEn ?></h4>
                                                                        </td>
                                                                        <td class="box-vs">VS</td>
                                                                        <td>
                                                                            <img src="<?= (!empty($match[1]->teamAwayPath)?$match[1]->teamAwayPath:'/images/team/team_default_64x64.png'); ?>">
                                                                            <h4 class="<?= ((!empty($match[1]->handicap) && $match[1]->handicap > 0) ? 'active' : '') ?>"><?= $match[1]->teamAwayEn ?></h4>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <p class="text-center">
                                                                    <a href="/games?mid=<?= $match[1]->id ?>"
                                                                       class="btn btn-default btn-sm">ทายผลบอล</a>
                                                                </p>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 detail-content-big-menu">
                                                <div class="col-sm-12 topic-content-big-menu">
                                                    <h3>
                                                        <i class="fa fa-trophy" aria-hidden="true"></i> Ranking
                                                    </h3>
                                                    <a class="top-readmore"
                                                       href="/ranking"><span>ดูทั้งหมด</span></a>
                                                </div>
                                                <div class="col-sm-12 menu-ranking-box">
                                                    <!-- Nav tabs -->
                                                    <ul class="nav nav-tabs" role="tablist">
                                                        <li role="presentation" class="active"><a href="#all-time-ranking"
                                                                                                  aria-controls="home"
                                                                                                  role="tab"
                                                                                                  data-toggle="tab">All
                                                                Time</a></li>
                                                        <li role="presentation"><a href="#now-ranking"
                                                                                   aria-controls="profile" role="tab"
                                                                                   data-toggle="tab">Now</a></li>
                                                        <li role="presentation"><a href="#Last-champ-ranking"
                                                                                   aria-controls="messages" role="tab"
                                                                                   data-toggle="tab">Last Champ</a></li>
                                                    </ul>

                                                    <!-- Tab panes -->
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane active" id="all-time-ranking">
                                                            <table>
                                                                <?php foreach ($topRankingAll as $key => $value) {
                                                                    if ($key < 5) {
                                                                        ?>
                                                                        <tr>
                                                                            <td><?= $key + 1 ?></td>
                                                                            <td>
                                                                                <img src="https://graph.facebook.com/v2.8/<?php echo $value["fb_uid"]; ?>/picture?type=large">
                                                                            </td>
                                                                            <td><?php echo $value["name"]; ?></td>
                                                                            <td><?php echo number_format($value["accuracy"], 2); ?></td>
                                                                        </tr>
                                                                    <?php }
                                                                } ?>
                                                            </table>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane" id="now-ranking">
                                                            <table>
                                                                <?php foreach ($topRankingNow as $key => $value) {
                                                                    if ($key < 5) {
                                                                        ?>
                                                                        <tr>
                                                                            <td><?= $key + 1 ?></td>
                                                                            <td>
                                                                                <img src="https://graph.facebook.com/v2.8/<?php echo $value["fb_uid"]; ?>/picture?type=large">
                                                                            </td>
                                                                            <td><?php echo $value["name"]; ?></td>
                                                                            <td><?php echo number_format($value["accuracy"], 2); ?></td>
                                                                        </tr>
                                                                    <?php }
                                                                } ?>
                                                            </table>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane" id="Last-champ-ranking">
                                                            <table>
                                                                <?php foreach ($topLastChamp as $key => $value) {
                                                                    if ($key < 5) {
                                                                        ?>
                                                                        <tr>
                                                                            <td><?= $key + 1 ?></td>
                                                                            <td>
                                                                                <img src="https://graph.facebook.com/v2.8/<?php echo $value["fb_uid"]; ?>/picture?type=large">
                                                                            </td>
                                                                            <td><?php echo $value["name"]; ?></td>
                                                                            <td><?php echo number_format($value["accuracy"], 2); ?></td>
                                                                        </tr>
                                                                    <?php }
                                                                } ?>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="none-dropdown"><a href="/review">ทีเด็ด ทรรศนะ</a></li>
                                <li class="text-event"><a href="https://ngoal.com/events"><img src="/images/source.gif"> <span
                                                class="blink"> แจกรางวัล ฟรี!</span></a></li>
                                <li class="active-dropdown hide">
                                    <a href="/worldcup">
                                        <img style="height: 23px;" src="/images/World_Cup.gif">
                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                        <div class="bx-menu-dropdown-arrow">
                                            <i class="fa fa-caret-up" aria-hidden="true"></i>
                                        </div>
                                        <div class="bx-menu-dropdown bx-menu-dropdown-content" style="">
                                            <div class="">
                                                <ul>
                                                    <li>
                                                        <a href="/worldcup">
                                                            <img style="height: 23px;" src="/images/russia-2018-logo.png">
                                                            <p>World Cup 2018</p>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="/worldcup-pubg">
                                                            <img style="height: 23px;" src="/worldcup-pubg/img/pubg-logo-style-one.png">
                                                            <p>Pubg World Cup</p>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="/worldcup-rov">
                                                            <img style="height: 23px;" src="/images/logo_en.png">
                                                            <p>ROV World Cup</p>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
        <div style="clear: both"></div>
        <div style="background-color: #003a6c; width: 100%;padding: 0px 0px;">
            <div id="loadingBar"
                 style="background: rgb(0, 188, 212); width: 0%;height: 2px; transition: transform .3s ease;"></div>
        </div>
    </nav>

</div>
<div id="wrapper">


    <div class="overlay"></div>

    <!-- Sidebar -->
    <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
        <ul class="nav sidebar-nav">
            <li><a href="/">หน้าแรก</a></li>
            <li><a href="/highlight">วิดีโอ/ไฮไลท์บอล</a></li>
            <li><a href="/news">ข่าวบอลต่างประเทศ</a></li>
            <li><a href="/news?type=thai&Page=1">ข่าวบอลไทย</a></li>
            <li><a href="/news?type=other&Page=1">ข่าวกีฬาอื่นๆ</a></li>
            <li><a href="/hothit-videos">HotHit</a></li>
            <li><a href="/other-videos-sexy">Sexy Football</a></li>
            <li><a href="/livescore">เกมทายผล</a></li>
            <li><a href="/rules">กติกาและเงื่อนไข</a></li>
            <li><a href="/ranking">Ranking</a></li>
            <li><a href="/help">วิธีการเล่นเกมทายผล</a></li>
        </ul>
    </nav>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div class="box-content-wrapper" data-spy="affix" data-offset-top="60" id="page-content-wrapper">
        <a href="/"><img src="/images/logo/logo-ngoal-white.png"></a>
        <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
            <span class="hamb-top"></span>
            <span class="hamb-middle"></span>
            <span class="hamb-bottom"></span>
        </button>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<?php
$dataDailyIncome = false;
$dataNewUserIncome = false;
?>
<?php if (isset($_SESSION['login'])) {
    if (empty($playLogIncome)) {
        require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playLogIncome.php";
        $playLogIncome = new playLogIncome();
    }
//    $dataDailyIncome = $playLogIncome->checkDailyIncome($_SESSION['login']['id']);
//    $dataNewUserIncome = $playLogIncome->checkNewUserIncome($_SESSION['login']['id']);
    ?>
    <div id="model-login-daily" class="modal fade" style="" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 70%;">
            <div class="modal-content" style="position: relative; background: none;">
                <img style="width: 100%;"
                     src="/images/login-daily/login-daily-<?php echo($_SESSION['login']['combo_login'] % 7); ?>-1.png">
                <div style="position: absolute;bottom:0;right:0;z-index: 100;width: 20%;padding: 2% 5%;font-size: 18px;font-weight: bolder;color: #fff;background-color: #0e505e;">
                    <a href="/play/dailyCoin" style="color: #fff;text-decoration: none;">รับ Coin</a>
                </div>
            </div>
        </div>
    </div>

    <div id="model-new-user" class="modal fade" style="" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="width: 50%;">
            <div class="modal-content" style="position: relative; background: none;">
                <img style="width: 100%;" src="/images/login-daily/new-user-1.png">
                <div style="position: absolute;bottom:0;right:0;z-index: 100;width: 20%;height: 20%;padding: 7% 5%;font-size: 18px;font-weight: bolder;color: #fff;">
                    <a href="/play/newUserCoin" style="color: #fff;text-decoration: none;">รับ Coin</a>
                </div>
            </div>
        </div>
    </div>

<?php } ?>

<?php //if ($dataDailyIncome) { ?>
<!--    <script>-->
<!--        $(document).ready(function () {-->
<!--            $('div#model-login-daily').modal({-->
<!--                backdrop: 'static',-->
<!--                keyboard: false-->
<!--            });-->
<!--        })-->
<!--    </script>-->
<?php //} ?>
<?php //if ($dataNewUserIncome) { ?>
<!--    <script>-->
<!--        $(document).ready(function () {-->
<!--            $('div#model-new-user').modal({-->
<!--                backdrop: 'static',-->
<!--                keyboard: false-->
<!--            });-->
<!--        })-->
<!--    </script>-->
<?php //} ?>


<div id="fade-in-index" class="box-fade">
    <div class="wrap-selected-team-play wrap-left-team">
        <p>คุณเลือกเล่นทีม</p>
        <img id="img-TeamHomePath" src="<?= (!empty($matchView->teamHomePath)?$matchView->teamHomePath:'/images/team/team_default_64x64.png'); ?>">
        <h1 id="h1-TeamHomeEn"><?= $matchView->teamHomeEn ?></h1>
        <form id="bet-away-match">
            <input name="bet[time_match]" class="form-control" type="hidden" value="<?= $matchView->time_match; ?>">
            <input name="bet[user_id]" class="form-control" type="hidden" value="">
            <input name="bet[match_id]" class="form-control" type="hidden" value="<?= $matchView->id; ?>">
            <input name="bet[team]" type="hidden" value="home">
            <!--                <input name="bet[use_bet_coin]" type="hidden" value="750">-->
            <input name="bet[use_bet_gold]" type="hidden" value="0">
            <input name="bet[use_bet_coupon]" type="hidden" value="1">
            <input name="bet[status]" type="hidden" value="bet">

            <div class="box-commented" style="margin: 20px 0px;">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="box-input-select">
                            <textarea name="bet[comment]" class="form-control"
                                      placeholder="แสดงความคิดเห็น..."></textarea>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>

            <div class="box-button-send">
                <div class="col-sm-2"></div>
                <div class="col-sm-4">
                    <div class="box-input-select">
                        <img src="/images/coin/scoin100.png">
                        <select name="bet[use_bet_coin]">
                            <option value="1200">1200</option>
                            <option value="800">800</option>
                            <option value="400">400</option>
                            <option value="200">200</option>
                        </select>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box-button">
                        <button type="submit" class="btn btn-info btn-lg btn-block">Confirm</button>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
        </form>
        <p>เลือกจำนวน Coin ที่ใช้ในการทายผล</p>
        <div class="block-close-vote text-center">
            <div class="box-button close-tab close-tab-right">
                <button type="button" class="btn btn-default btn-lg btn-block" data-dismiss="modal">
                    ปิด
                </button>
            </div>
        </div>
    </div>
</div>
<div id="fade-in-right-index" class="box-fade">
    <!--        <div class="close-tab pull-left close-tab-left"><i class="fa fa-times" aria-hidden="true"></i></div>-->
    <div class="wrap-selected-team-play wrap-right-team">
        <p>คุณเลือกเล่นทีม</p>
        <img id="img-TeamAwayPath" src="<?= (!empty($matchView->teamAwayPath)?$matchView->teamAwayPath:'/images/team/team_default_64x64.png'); ?>">
        <h1 id="h1-TeamAwayEn"><?= $matchView->teamAwayEn ?></h1>
        <form id="bet-away-match">
            <input name="bet[time_match]" class="form-control" type="hidden" value="<?= $matchView->time_match; ?>">
            <input name="bet[user_id]" class="form-control" type="hidden" value="">
            <input name="bet[match_id]" class="form-control" type="hidden" value="<?= $matchView->id; ?>">
            <input name="bet[team]" type="hidden" value="away">
            <!--                <input name="bet[use_bet_coin]" type="hidden" value="750">-->
            <input name="bet[use_bet_gold]" type="hidden" value="0">
            <input name="bet[use_bet_coupon]" type="hidden" value="1">
            <input name="bet[status]" type="hidden" value="bet">
            <div class="box-commented" style="margin: 20px 0px;">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="box-input-select">
                            <textarea name="bet[comment]" class="form-control"
                                      placeholder="แสดงความคิดเห็น..."></textarea>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>

            <div class="box-button-send">
                <div class="col-sm-2"></div>
                <div class="col-sm-4">
                    <div class="box-input-select">
                        <img src="/images/coin/scoin100.png">
                        <select name="bet[use_bet_coin]">
                            <option value="1200">1200</option>
                            <option value="800">800</option>
                            <option value="400">400</option>
                            <option value="200">200</option>
                        </select>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box-button">
                        <button type="submit" class="btn btn-info btn-lg btn-block">Confirm</button>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="clearfix"></div>
            </div>
        </form>
        <p>เลือกจำนวน Coin ที่ใช้ในการทายผล</p>
        <div class="block-close-vote text-center">
            <div class="box-button close-tab close-tab-left">
                <button type="button" class="btn btn-default btn-lg btn-block" data-dismiss="modal">
                    ปิด
                </button>
            </div>
        </div>
    </div>
</div>
<div class="content-livescore-main content-game-redesign">
    <div class="container">
        <div class="row">
            <div class="content-list-livescore">
                <div class="col-md-12" id="liveScoreHead" style="width: 100%;overflow: hidden;height: 104px;">
                    <ul class="" id="ul-liveScoreHead" style="margin: 0px">
                        <?php if (!empty($match)) {
                            foreach ($match as $key => $value) { ?>
                                <?php if($value->status=="bet"){ ?>
                                <li data-key="<?php echo $key; ?>" data-matchId="<?= $value->id ?>"
                                    data-timematch="<?= $value->time_match ?>"
                                    class="match-slide <?php echo (($value->id == $matchView->id) ? 'active' : '') ?>"
                                    style="float: left;">
                                    <a href="/games?mid=<?= $value->id ?>" class="col-md-12" style="100%;">
                                        <div class="thumbnail content-team-slide">
                                            <div class="pull-left">
                                                <table>
                                                    <tr>
                                                        <td><img src="<?= (!empty($value->teamHomePath)?$value->teamHomePath:'/images/team/team_default_64x64.png');?>"></td>
                                                        <td class="<?= ((!empty($value->handicap) && $value->handicap < 0) ? 'active' : '') ?>">
                                                            <div class="name-team-slide-top"><?= $value->teamHomeEn ?></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><img src="<?= (!empty($value->teamAwayPath)?$value->teamAwayPath:'/images/team/team_default_64x64.png');?>"></td>
                                                        <td class="<?= ((!empty($value->handicap) && $value->handicap > 0) ? 'active' : '') ?>">
                                                            <div class="name-team-slide-top"><?= $value->teamAwayEn ?></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="pull-right">
                                                <div class="time-live">
                                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                    <p><?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?></p>
                                                </div>
                                                <div class="HDP-live">HDP</div>
                                                <div class="num-live"><?= $value->handicap ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            <?php }}
                        } ?>
                    </ul>
                </div>
            </div>
        </div>
        <?php if (!empty($matchView)) { ?>
            <div class="row" id="match-team-<?= $matchView->id ?>">
                <div class="topic-league">
                    <h3><img src="<?= $matchView->path ?>"> <?= $matchView->name ?></h3>
                    <ul>
                        <li><i class="fa fa-clock-o"
                               aria-hidden="true"></i> <?= date("H:i", (strtotime($matchView->time_match) - (60 * 60))) ?>
                        </li>
                        <li>|</li>
                        <li><i class="fa fa-calendar"
                               aria-hidden="true"></i> <?= date("d/m/Y", (strtotime($matchView->time_match) - (60 * 60))) ?>
                        </li>
                    </ul>
                </div>
                <div class="content-select-team-vote">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="pull-right">
                                        <div class="name-team-vote pull-left text-right">
                                            <h3 class="<?php echo(($matchView->handicap < 0) ? 'text-blue' : '') ?>"><?= $matchView->teamHomeEn ?></h3>
                                        </div>
                                        <div class="box-vote-team">
                                            <div class="logo-vote-team-home">
                                                <img src="<?php echo (file_exists($_SERVER['DOCUMENT_ROOT']."/images/team/".$matchView->team_home."/".$matchView->team_home."_256x256.png")?"/images/team/".$matchView->team_home."/".$matchView->team_home."_256x256.png":$matchView->teamHomePath) ?>">
                                            </div>
                                            <div class="btn-vote-team-home text-right">
                                                <?php if(!empty($matchView->home_water_bill) && !empty($matchView->away_water_bill) && $matchView->status=="bet"){ ?>
                                                    <a class="a-vote-team-home">VOTE</a>
                                                <?php } ?>
                                                <div class="bg-vote-team-home"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="pull-left">
                                        <div class="box-vote-team">
                                            <div class="logo-vote-team-away">
                                                <img src="<?php echo (file_exists($_SERVER['DOCUMENT_ROOT']."/images/team/".$matchView->team_away."/".$matchView->team_away."_256x256.png")?"/images/team/".$matchView->team_away."/".$matchView->team_away."_256x256.png":$matchView->teamAwayPath); ?>">
                                            </div>
                                            <div class="btn-vote-team-away">
                                                <?php if(!empty($matchView->home_water_bill) && !empty($matchView->away_water_bill) && $matchView->status=="bet"){ ?>
                                                    <a class="a-vote-team-away">VOTE</a>
                                                <?php } ?>
                                                <div class="bg-vote-team-away"></div>
                                            </div>
                                        </div>
                                        <div class="name-team-vote pull-left">
                                            <h3 class="<?php echo(($matchView->handicap > 0) ? 'text-blue' : '') ?>"><?= $matchView->teamAwayEn ?></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="row text-center">
            <ul class="content-HDP-game" id="content-HDP-game">

            </ul>
        </div>
    </div>
</div>

<!--ทรรศนะ-->
<?php if (!empty($reviewMatch)) { ?>
    <div class="wrap-preview-game">
        <div class="container">
            <div class="title-label-rota">REVIEW</div>
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox" style="height:240px;">
                    <div class="item active">
                        <?php foreach ($reviewMatch as $key => $value) { ?>
                            <?php if ($key <= 5) { ?>
                                <div class="col-sm-4 col-md-4">
                                    <div class="thumbnail box-preview" data-toggle="modal"
                                         data-target="#showReviewGuru">
                                        <div class="top-box-preview">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div class="box-guru-img pull-left">
                                                            <img style="height: 40px;<?php echo ((!empty($value['path']))?'':'width: 40px;');?>" src="<?= ((!empty($value['path']))?$value['path']:'https://graph.facebook.com/v2.8/'.$value['user_fb_uid'].'/picture'); ?>">
                                                        </div>
                                                        <div class="pull-left">
                                                            <h5><?= ((!empty($value['name']))?$value['name']:$value['user_name']); ?></h5>
                                                            <div class="box-player-preview">
                                                                <?php if ($value['team'] == "home") { ?>
                                                                    <img src="<?= (!empty($value['homepath'])?$value['homepath']:'/images/team/team_default_64x64.png'); ?>">
                                                                    <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                                <?php } elseif ($value['team'] == "away") { ?>
                                                                    <img src="<?= (!empty($value['awaypath'])?$value['awaypath']:'/images/team/team_default_64x64.png'); ?>">
                                                                    <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="pull-right">
                                                            <button type="button" class="btn btn-link">
                                                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                            </button>
                                                        </div>
                                                        <div class="pull-right">
                                                            <h3><?= ((!empty($value['user_review_id']))?$resultsReviewMatch[$value['user_review_id']]:number_format($value['user_accuracy'])); ?>
                                                                %</h3>
                                                            <p>Win rate</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="button-box-preview" style="overflow:hidden;height: 40px;">
                                                <?= $value['review_text'] ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <?php if (count($reviewMatch) > 6) { ?>
                        <div class="item">
                            <?php foreach ($reviewMatch as $key => $value) { ?>
                                <?php if ($key > 5 && $key <= 11) { ?>
                                    <div class="col-sm-4 col-md-4">
                                        <div class="thumbnail box-preview" data-toggle="modal"
                                             data-target="#showReviewGuru">
                                            <div class="top-box-preview">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <div class="box-guru-img pull-left">
                                                                <img style="height: 40px;<?php echo ((!empty($value['path']))?'':'width: 40px;');?>" src="<?= ((!empty($value['path']))?$value['path']:'https://graph.facebook.com/v2.8/'.$value['user_fb_uid'].'/picture'); ?>">
                                                            </div>
                                                            <div class="pull-left">
                                                                <h5><?= ((!empty($value['name']))?$value['name']:$value['user_name']); ?></h5>
                                                                <div class="box-player-preview">
                                                                    <?php if ($value['team'] == "home") { ?>
                                                                        <img src="<?= (!empty($value['homepath'])?$value['homepath']:'/images/team/team_default_64x64.png'); ?>">
                                                                        <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                                    <?php } elseif ($value['team'] == "away") { ?>
                                                                        <img src="<?= (!empty($value['awaypath'])?$value['awaypath']:'/images/team/team_default_64x64.png'); ?>">
                                                                        <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="pull-right">
                                                                <button type="button" class="btn btn-link">
                                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                                </button>
                                                            </div>
                                                            <div class="pull-right">
                                                                <h3><?= ((!empty($value['user_review_id']))?$resultsReviewMatch[$value['user_review_id']]:number_format($value['user_accuracy'])); ?>
                                                                    %</h3>
                                                                <p>Win rate</p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="button-box-preview">
                                                <p style="overflow:hidden;height: 40px;">
                                                    <?= $value['review_text'] ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php if (count($reviewMatch) > 12) { ?>
                        <div class="item">
                            <?php foreach ($reviewMatch as $key => $value) { ?>
                                <?php if ($key > 11 && $key <= 17) { ?>
                                    <div class="col-sm-4 col-md-4">
                                        <div class="thumbnail box-preview" data-toggle="modal"
                                             data-target="#showReviewGuru">
                                            <div class="top-box-preview">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <div class="box-guru-img pull-left">
                                                                <img style="height: 40px;<?php echo ((!empty($value['path']))?'':'width: 40px;');?>" src="<?= ((!empty($value['path']))?$value['path']:'https://graph.facebook.com/v2.8/'.$value['user_fb_uid'].'/picture'); ?>">
                                                            </div>
                                                            <div class="pull-left">
                                                                <h5><?= ((!empty($value['name']))?$value['name']:$value['user_name']); ?></h5>
                                                                <div class="box-player-preview">
                                                                    <?php if ($value['team'] == "home") { ?>
                                                                        <img src="<?= (!empty($value['homepath'])?$value['homepath']:'/images/team/team_default_64x64.png'); ?>">
                                                                        <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                                    <?php } elseif ($value['team'] == "away") { ?>
                                                                        <img src="<?= (!empty($value['awaypath'])?$value['awaypath']:'/images/team/team_default_64x64.png'); ?>">
                                                                        <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="pull-right">
                                                                <button type="button" class="btn btn-link">
                                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                                </button>
                                                            </div>
                                                            <div class="pull-right">
                                                                <h3><?= ((!empty($value['user_review_id']))?$resultsReviewMatch[$value['user_review_id']]:number_format($value['user_accuracy'])); ?>
                                                                    %</h3>
                                                                <p>Win rate</p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="button-box-preview">
                                                <p style="overflow:hidden;height: 40px;">
                                                    <?= $value['review_text'] ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <!-- Indicators -->
                <ol class="carousel-indicators" style="bottom: 0; z-index: 0;">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>
            </div>
            <div class="box-add-review" onclick="userReviewForm();">
                <i class="fa fa-pencil" aria-hidden="true"></i>
            </div>
        </div>
    </div>
<?php } ?>
<!--สถิคิ-->
<?php if (!empty($matchView)) { ?>
    <div class="topic-content-state" data-spy="affix" data-offset-top="600" data-offset-bottom="200">
        <ul>
            <li>
                <div class="pull-right">
                    <h2 class="<?php echo(($matchView->handicap < 0) ? 'text-blue' : '') ?>"><?= $matchView->teamHomeEn ?></h2>
                    <img src="<?= (!empty($matchView->teamHomePath)?$matchView->teamHomePath:'/images/team/team_default_64x64.png'); ?>">
                </div>
            </li>
            <li>
                <div class="text-center">
                    <h3><?= $matchView->name ?></h3>
                    <p>
                        <?= date("H:i", (strtotime($matchView->time_match) - (60 * 60))) ?>
                        | <?= date("d/m/Y", (strtotime($matchView->time_match) - (60 * 60))) ?>
                    </p>
                </div>
            </li>
            <li>
                <div class="pull-left">
                    <img src="<?= (!empty($matchView->teamAwayPath)?$matchView->teamAwayPath:'/images/team/team_default_64x64.png'); ?>">
                    <h2 class="<?php echo(($matchView->handicap > 0) ? 'text-blue' : '') ?>"><?= $matchView->teamAwayEn ?></h2>
                </div>
            </li>
        </ul>
        <div class="arrow-down"></div>
    </div>
<?php } ?>
<!--HEAD TO HEAD-->
<?php if(!empty($dataHeadToHead)){ ?>
    <div class="content-state content-h2h col-lg-12">
        <!--    <div class="head-state">-->
        <!--        <h2>HEAD TO HEAD</h2>-->
        <!--    </div>-->
        <div class="content-state-game" style="width: 234px; margin-top: 40px;">
            <h2>HEAD TO HEAD</h2>
            <div class="head-state-game"></div>
        </div>
        <div class="col-sm-6 h2h-tabs-team1">
            <div class="pull-right">
                <!--            กราฟวงกลม-->
                <div class="section-state-box section-state-box-circle text-left">
                    <div class="topic-section-state">
                        <table>
                            <tr>
                                <td>
                                    <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Chatdata Odds/ Result</h4>
                                </td>
                                <td class="text-right"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="content-section-state row">
                        <div class="col-sm-6">
                            <div class="box-circle-data">
                                <div id="h2hdonut-h-oresult"
                                     style="z-index: 1; width: 204px; height: 174px;">
                                    <img src="/images/chart-simple.png">
                                </div>
                                <div class="text-h2hdonut-h-oresult">Odds</div>
                            </div>
                            <div class="data-chart">
                                <div class="clearfix">
                                    <h4 id="oddPWin">41.4</h4>
                                    <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                                </div>
                                <div class="clearfix">
                                    <h4 id="oddPLose">58.6</h4>
                                    <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                                </div>
                                <div class="clearfix">
                                    <h4 id="oddPDraw">0</h4>
                                    <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <div class="box-circle-data">
                                <div id="h2hdonut-h-result"
                                     style="z-index: 1; width: 204px; height: 174px;">
                                    <img src="/images/chart-simple.png">
                                </div>
                                <div class="text-h2hdonut-h-oresult">Result</div>
                            </div>
                            <div class="data-chart">
                                <div class="clearfix">
                                    <h4 id="resultPWin">41.4</h4>
                                    <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                                </div>
                                <div class="clearfix">
                                    <h4 id="resultPLose">58.6</h4>
                                    <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                                </div>
                                <div class="clearfix">
                                    <h4 id="resultPDraw">0</h4>
                                    <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--            ตาราง-->
                <div class="section-state-box text-left">
                    <div class="topic-section-state">
                        <table>
                            <tr>
                                <td>
                                    <h4><img src="<?= (!empty($matchView->teamHomePath)?$matchView->teamHomePath:'/images/team/team_default_64x64.png'); ?>"> <?= $matchView->teamHomeEn ?></h4>
                                </td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-default dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Last Match(<label
                                                id="h2h-h-max-match"><?php echo((count($dataHeadToHead) >= 10) ? "10" : count($dataHeadToHead)); ?></label>)
                                            <span class="caret"></span>
                                        </button>
                                        <ul id="h2h-h-select-limit" class="dropdown-menu" aria-labelledby="dLabel">
                                            <?php
                                            if (count($dataHeadToHead) >= 20) {
                                                for ($i = 1; $i <= 20; $i++) { ?>
                                                    <li><a onclick="viewHeadToHeadHome(<?= (($i)) ?>)">Last
                                                            Match(<?= (($i)) ?>)</a></li>
                                                <?php }
                                            } else {
                                                for ($i = 1; $i <= (count($dataHeadToHead) - 1); $i++) { ?>
                                                    <li><a onclick="viewHeadToHeadHome(<?= (($i)) ?>)">Last
                                                            Match(<?= (($i)) ?>)</a></li>
                                                <?php }
                                            } ?>
                                            <li><a onclick="viewHeadToHeadHome(<?= count($dataHeadToHead) ?>)">Last
                                                    Match(<?= count($dataHeadToHead) ?>)</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="content-section-state row">
                        <div class="box-table-result-state" style="padding: 0 15px;">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-pills" role="tablist">
                                <li role="presentation" class="active">
                                    <a onclick="viewHeadToHeadHome()" href="#h2hHTotal" aria-controls="h2hTotal" role="tab"
                                       data-toggle="tab">Total</a></li>
                                <li role="presentation">
                                    <a onclick="viewHeadToHeadHome()" href="#h2hHHome" aria-controls="h2hHome" role="tab"
                                       data-toggle="tab">Home</a></li>
                                <li role="presentation">
                                    <a onclick="viewHeadToHeadHome()" href="#h2hHAway" aria-controls="h2hAway" role="tab"
                                       data-toggle="tab">Away</a></li>
                                <li class="pull-right">
                                    <label class="checkbox-inline">
                                        <input onclick="viewHeadToHeadHome()" type="checkbox" class="checkbox_team1_all"
                                               value="all" checked> All
                                    </label>
                                    <?php foreach ($dataLeagueByHeadToHead as $key => $value) { ?>
                                        <label class="checkbox-inline">
                                            <input onclick="viewHeadToHeadHome()" type="checkbox"
                                                   class="checkbox_team1_<?= $key ?>" value="<?= $key ?>"> <?= $value ?>
                                        </label>
                                    <?php } ?>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active " id="h2hHTotal">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-center mini-col">HDP</th>
                                            <th class="text-center mini-col">Odds</th>
                                            <th class="text-center mini-col">Result</th>
                                        </tr>
                                        </thead>
                                        <?php foreach ($dataHeadToHead as $key => $value) { ?>
                                            <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                                id="guide-row-original"
                                                class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                                <td>
                                                    <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                    <p><i class="fa fa-calendar"
                                                          aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                        | <i
                                                            class="fa fa-clock-o"
                                                            aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                    </p>
                                                </td>
                                                <td class="text-right box-team-state">
                                                    <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                    <img src="<?= (!empty($value->teamHomePath)?$value->teamHomePath:'/images/team/team_default_64x64.png'); ?>">
                                                </td>
                                                <td style="width: 12%;">
                                                    <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                        - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                                </td>
                                                <td class="text-left box-team-state">
                                                    <img src="<?= (!empty($value->teamAwayPath)?$value->teamAwayPath:'/images/team/team_default_64x64.png'); ?>">
                                                    <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                                </td>
                                                <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                                <td class="text-center tr-odd">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center tr-result">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="h2hHHome">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-center mini-col">HDP</th>
                                            <th class="text-center mini-col">Odds</th>
                                            <th class="text-center mini-col">Result</th>
                                        </tr>
                                        </thead>
                                        <?php foreach ($dataHeadToHead as $key => $value) { ?>
                                            <?php if ($matchView->teamAwayEn == $value->teamAwayEn) { ?>
                                                <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                                    id="guide-row-original"
                                                    class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                                    <td>
                                                        <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                        <p><i class="fa fa-calendar"
                                                              aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                            | <i
                                                                class="fa fa-clock-o"
                                                                aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                        </p>
                                                    </td>
                                                    <td class="text-right box-team-state">
                                                        <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                        <img src="<?= (!empty($value->teamHomePath)?$value->teamHomePath:'/images/team/team_default_64x64.png') ?>">
                                                    </td>
                                                    <td>
                                                        <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                            - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                                    </td>
                                                    <td class="text-left box-team-state">
                                                        <img src="<?= (!empty($value->teamAwayPath)?$value->teamAwayPath:'/images/team/team_default_64x64.png'); ?>">
                                                        <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                                    </td>
                                                    <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                                    <td class="text-center tr-odd">
                                                        <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                            <img class="guide-result img-win"
                                                                 src="/images/icon-stat/result/0.png">
                                                        <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                            <img class="guide-result img-lose"
                                                                 src="/images/icon-stat/result/2.png">
                                                        <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                            <img class="guide-result img-draw"
                                                                 src="/images/icon-stat/result/1.png">
                                                        <?php } else { ?>
                                                            -
                                                        <?php } ?>
                                                    </td>
                                                    <td class="text-center tr-result">
                                                        <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                            <img class="guide-result img-win"
                                                                 src="/images/icon-stat/result/0.png">
                                                        <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                            <img class="guide-result img-lose"
                                                                 src="/images/icon-stat/result/2.png">
                                                        <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                            <img class="guide-result img-draw"
                                                                 src="/images/icon-stat/result/1.png">
                                                        <?php } else { ?>
                                                            -
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </table>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="h2hHAway">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-center mini-col">HDP</th>
                                            <th class="text-center mini-col">Odds</th>
                                            <th class="text-center mini-col">Result</th>
                                        </tr>
                                        </thead>
                                        <?php foreach ($dataHeadToHead as $key => $value) { ?>
                                            <?php if ($matchView->teamAwayEn != $value->teamAwayEn) { ?>
                                                <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                                    id="guide-row-original"
                                                    class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                                    <td>
                                                        <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                        <p><i class="fa fa-calendar"
                                                              aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                            | <i
                                                                class="fa fa-clock-o"
                                                                aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                        </p>
                                                    </td>
                                                    <td class="text-right box-team-state">
                                                        <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                        <img src="<?= (!empty($value->teamHomePath)?$value->teamHomePath:'/images/team/team_default_64x64.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                            - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                                    </td>
                                                    <td class="text-left box-team-state">
                                                        <img src="<?= (!empty($value->teamAwayPath)?$value->teamAwayPath:'/images/team/team_default_64x64.png'); ?>">
                                                        <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                                    </td>
                                                    <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                                    <td class="text-center tr-odd">
                                                        <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                            <img class="guide-result img-win"
                                                                 src="/images/icon-stat/result/0.png">
                                                        <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                            <img class="guide-result img-lose"
                                                                 src="/images/icon-stat/result/2.png">
                                                        <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                            <img class="guide-result img-draw"
                                                                 src="/images/icon-stat/result/1.png">
                                                        <?php } else { ?>
                                                            -
                                                        <?php } ?>
                                                    </td>
                                                    <td class="text-center tr-result">
                                                        <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                            <img class="guide-result img-win"
                                                                 src="/images/icon-stat/result/0.png">
                                                        <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                            <img class="guide-result img-lose"
                                                                 src="/images/icon-stat/result/2.png">
                                                        <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                            <img class="guide-result img-draw"
                                                                 src="/images/icon-stat/result/1.png">
                                                        <?php } else { ?>
                                                            -
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <div style="clear: both"></div>
        </div>
        <div class="col-sm-6 h2h-tabs-team2" style="background-color: #f1f1f1;">
            <div class="pull-left">
                <!--            กราฟวงกลม-->
                <div class="section-state-box section-state-box-circle text-left">
                    <div class="topic-section-state">
                        <table>
                            <tr>
                                <td>
                                    <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Chatdata Odds/ Result</h4>
                                </td>
                                <td class="text-right">

                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="content-section-state row">
                        <div class="col-sm-6">
                            <div class="box-circle-data">
                                <div id="h2hdonut-a-oresult" style="z-index: 1; width: 204px; height: 174px;">
                                    <img src="/images/chart-simple.png">
                                </div>
                                <div class="text-h2hdonut-h-oresult">Odds</div>
                            </div>
                            <div class="data-chart">
                                <div class="clearfix">
                                    <h4 id="oddPWin">41.4</h4>
                                    <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                                </div>
                                <div class="clearfix">
                                    <h4 id="oddPLose">58.6</h4>
                                    <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                                </div>
                                <div class="clearfix">
                                    <h4 id="oddPDraw">0</h4>
                                    <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="box-circle-data">
                                <div id="h2hdonut-a-result" style="z-index: 1; width: 204px; height: 174px;">
                                    <img src="/images/chart-simple.png">
                                </div>
                                <div class="text-h2hdonut-h-oresult">Result</div>
                            </div>
                            <div class="data-chart">
                                <div class="clearfix">
                                    <h4 id="resultPWin">41.4</h4>
                                    <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                                </div>
                                <div class="clearfix">
                                    <h4 id="resultPLose">58.6</h4>
                                    <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                                </div>
                                <div class="clearfix">
                                    <h4 id="resultPDraw">0</h4>
                                    <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--            ตาราง-->
                <div class="section-state-box text-left">
                    <div class="topic-section-state">
                        <table>
                            <tr>
                                <td>
                                    <h4><img src="<?= (!empty($matchView->teamAwayPath)?$matchView->teamAwayPath:'/images/team/team_default_64x64.png') ?>"> <?= $matchView->teamAwayEn ?></h4>
                                </td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-default dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Last Match(<label
                                                id="h2h-a-max-match"><?php echo((count($dataHeadToHead) >= 10) ? "10" : count($dataHeadToHead)) ?></label>)
                                            <span class="caret"></span>
                                        </button>
                                        <ul id="h2h-a-select-limit" class="dropdown-menu">
                                            <?php
                                            if (count($dataHeadToHead) >= 20) {
                                                for ($i = 1; $i <= 20; $i++) { ?>
                                                    <li><a onclick="viewHeadToHeadAway(<?= (($i)) ?>)">Last
                                                            Match(<?= (($i)) ?>)</a></li>
                                                <?php }
                                            } else {
                                                for ($i = 1; $i <= (count($dataHeadToHead) - 1); $i++) { ?>
                                                    <li><a onclick="viewHeadToHeadAway(<?= (($i)) ?>)">Last
                                                            Match(<?= (($i)) ?>)</a></li>
                                                <?php }
                                            } ?>
                                            <li><a onclick="viewHeadToHeadAway(<?= count($dataHeadToHead) ?>)">Last
                                                    Match(<?= count($dataHeadToHead) ?>)</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="content-section-state row">
                        <div class="box-table-result-state" style="padding: 0 15px;">

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-pills" role="tablist">
                                <li role="presentation" class="active">
                                    <a onclick="viewHeadToHeadAway()" href="#h2hATotal" aria-controls="h2hTotal" role="tab"
                                       data-toggle="tab">Total</a></li>
                                <li role="presentation">
                                    <a onclick="viewHeadToHeadAway()" href="#h2hAHome" aria-controls="h2hHome" role="tab"
                                       data-toggle="tab">Home</a></li>
                                <li role="presentation">
                                    <a onclick="viewHeadToHeadAway()" href="#h2hAAway" aria-controls="h2hAway" role="tab"
                                       data-toggle="tab">Away</a></li>
                                <li class="pull-right">
                                    <label class="checkbox-inline">
                                        <input onclick="viewHeadToHeadAway()" type="checkbox" class="checkbox_team2_all"
                                               value="all" checked> All
                                    </label>
                                    <?php foreach ($dataLeagueByHeadToHead as $key => $value) { ?>
                                        <label class="checkbox-inline">
                                            <input onclick="viewHeadToHeadAway()" type="checkbox"
                                                   class="checkbox_team2_<?= $key ?>" value="<?= $key ?>"> <?= $value ?>
                                        </label>
                                    <?php } ?>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="h2hATotal">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-center mini-col">HDP</th>
                                            <th class="text-center mini-col">Odds</th>
                                            <th class="text-center mini-col">Result</th>
                                        </tr>
                                        </thead>
                                        <?php foreach ($dataHeadToHead as $key => $value) { ?>
                                            <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                                id="guide-row-original"
                                                class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                                <td>
                                                    <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                    <p><i class="fa fa-calendar"
                                                          aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                        | <i
                                                            class="fa fa-clock-o"
                                                            aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                    </p>
                                                </td>
                                                <td class="text-right box-team-state">
                                                    <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                    <img src="<?= (!empty($value->teamHomePath)?$value->teamHomePath:'/images/team/team_default_64x64.png'); ?>">
                                                </td>
                                                <td style="width: 12%;">
                                                    <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                        - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                                </td>
                                                <td class="text-left box-team-state">
                                                    <img src="<?= (!empty($value->teamAwayPath)?$value->teamAwayPath:'/images/team/team_default_64x64.png'); ?>">
                                                    <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                                </td>
                                                <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                                <td class="text-center tr-odd">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center tr-result">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="h2hAHome">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-center mini-col">HDP</th>
                                            <th class="text-center mini-col">Odds</th>
                                            <th class="text-center mini-col">Result</th>
                                        </tr>
                                        </thead>
                                        <?php foreach ($dataHeadToHead as $key => $value) { ?>
                                            <?php if ($matchView->teamAwayEn != $value->teamAwayEn) { ?>
                                                <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                                    id="guide-row-original"
                                                    class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                                    <td>
                                                        <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                        <p><i class="fa fa-calendar"
                                                              aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                            | <i
                                                                class="fa fa-clock-o"
                                                                aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                        </p>
                                                    </td>
                                                    <td class="text-right box-team-state">
                                                        <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                        <img src="<?= (!empty($value->teamHomePath)?$value->teamHomePath:'/images/team/team_default_64x64.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                            - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                                    </td>
                                                    <td class="text-left box-team-state">
                                                        <img src="<?= (!empty($value->teamAwayPath)?$value->teamAwayPath:'/images/team/team_default_64x64.png') ?>">
                                                        <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                                    </td>
                                                    <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                                    <td class="text-center tr-odd">
                                                        <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                            <img class="guide-result img-win"
                                                                 src="/images/icon-stat/result/0.png">
                                                        <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                            <img class="guide-result img-lose"
                                                                 src="/images/icon-stat/result/2.png">
                                                        <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                            <img class="guide-result img-draw"
                                                                 src="/images/icon-stat/result/1.png">
                                                        <?php } else { ?>
                                                            -
                                                        <?php } ?>
                                                    </td>
                                                    <td class="text-center tr-result">
                                                        <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                            <img class="guide-result img-win"
                                                                 src="/images/icon-stat/result/0.png">
                                                        <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                            <img class="guide-result img-lose"
                                                                 src="/images/icon-stat/result/2.png">
                                                        <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                            <img class="guide-result img-draw"
                                                                 src="/images/icon-stat/result/1.png">
                                                        <?php } else { ?>
                                                            -
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </table>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="h2hAAway">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-center mini-col">HDP</th>
                                            <th class="text-center mini-col">Odds</th>
                                            <th class="text-center mini-col">Result</th>
                                        </tr>
                                        </thead>
                                        <?php foreach ($dataHeadToHead as $key => $value) { ?>
                                            <?php if ($matchView->teamAwayEn == $value->teamAwayEn) { ?>
                                                <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                                    id="guide-row-original"
                                                    class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                                    <td>
                                                        <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                        <p><i class="fa fa-calendar"
                                                              aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                            | <i
                                                                class="fa fa-clock-o"
                                                                aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                        </p>
                                                    </td>
                                                    <td class="text-right box-team-state">
                                                        <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                        <img src="<?= (!empty($value->teamHomePath)?$value->teamHomePath:'/images/team/team_default_64x64.png'); ?>">
                                                    </td>
                                                    <td>
                                                        <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                            - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                                    </td>
                                                    <td class="text-left box-team-state">
                                                        <img src="<?= (!empty($value->teamAwayPath)?$value->teamAwayPath:'/images/team/team_default_64x64.png'); ?>">
                                                        <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                                    </td>
                                                    <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                                    <td class="text-center tr-odd">
                                                        <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                            <img class="guide-result img-win"
                                                                 src="/images/icon-stat/result/0.png">
                                                        <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                            <img class="guide-result img-lose"
                                                                 src="/images/icon-stat/result/2.png">
                                                        <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                            <img class="guide-result img-draw"
                                                                 src="/images/icon-stat/result/1.png">
                                                        <?php } else { ?>
                                                            -
                                                        <?php } ?>
                                                    </td>
                                                    <td class="text-center tr-result">
                                                        <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                            <img class="guide-result img-win"
                                                                 src="/images/icon-stat/result/0.png">
                                                        <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                            <img class="guide-result img-lose"
                                                                 src="/images/icon-stat/result/2.png">
                                                        <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                            <img class="guide-result img-draw"
                                                                 src="/images/icon-stat/result/1.png">
                                                        <?php } else { ?>
                                                            -
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>
<?php } ?>
<!--Form Guide-->
<div class="content-state content-formGuide col-lg-12">
    <!--    <div class="head-state">-->
    <!--        <h2>FORM GUIDE</h2>-->
    <!--    </div>-->
    <div class="content-state-game" style="width: 205px;">
        <h2>FORM GUIDE</h2>
        <div class="head-state-game"></div>
    </div>
    <div class="col-sm-6 formGuide-tabs-team1">
        <div class="pull-right">
            <!--            กราฟวงกลม-->
            <div class="section-state-box section-state-box-circle text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Chatdata Odds/ Result</h4>
                            </td>
                            <td class="text-right">

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">

                    <div class="col-sm-6">
                        <div class="box-circle-data">
                            <div id="formguidedonut-H-oresult"
                                 style="z-index: 1; width: 204px; height: 174px;">
                                <img src="/images/chart-simple.png">
                            </div>
                            <div class="text-h2hdonut-h-oresult">Odds</div>
                        </div>
                        <div class="data-chart">
                            <div class="clearfix">
                                <h4 id="oddPWin">41.4</h4>
                                <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                            </div>
                            <div class="clearfix">
                                <h4 id="oddPLose">58.6</h4>
                                <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                            </div>
                            <div class="clearfix">
                                <h4 id="oddPDraw">0</h4>
                                <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-circle-data">
                            <div id="formguidedonut-H-result"
                                 style="z-index: 1; width: 204px; height: 174px;">
                                <img src="/images/chart-simple.png">
                            </div>
                            <div class="text-h2hdonut-h-oresult">Result</div>
                        </div>
                        <div class="data-chart">
                            <div class="clearfix">
                                <h4 id="resultPWin">41.4</h4>
                                <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                            </div>
                            <div class="clearfix">
                                <h4 id="resultPLose">58.6</h4>
                                <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                            </div>
                            <div class="clearfix">
                                <h4 id="resultPDraw">0</h4>
                                <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--            ตาราง-->
            <div class="section-state-box text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><img src="<?= (!empty($matchView->teamHomePath)?$matchView->teamHomePath:'/images/team/team_default_64x64.png'); ?>"> <?= $matchView->teamHomeEn ?></h4>
                            </td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-default dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Last Match(<label
                                            id="formguide-h-max-match"><?php echo((count($dataFormGuideHome) >= 10) ? '10' : count($dataFormGuideHome)) ?></label>)
                                        <span class="caret"></span>
                                    </button>
                                    <ul id="formguide-h-select-limit" class="dropdown-menu">
                                        <?php if (count($dataFormGuideHome) >= 20) {
                                            for ($i = 1; $i <= 20; $i++) { ?>
                                                <li><a onclick="viewFormGuideHome(<?= (($i)) ?>)">Last
                                                        Match(<?= (($i)) ?>)</a></li>
                                            <?php }
                                        } else {
                                            for ($i = 1; $i <= (count($dataFormGuideHome) - 1); $i++) { ?>
                                                <li><a onclick="viewFormGuideHome(<?= (($i)) ?>)">Last
                                                        Match(<?= (($i)) ?>)</a></li>
                                            <?php }
                                        } ?>
                                        <li><a onclick="viewFormGuideHome(<?= count($dataFormGuideHome) ?>)">Last
                                                Match(<?= count($dataFormGuideHome) ?>)</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="box-table-result-state" style="padding: 0 15px;">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs  nav-pills" role="tablist">
                            <li role="presentation" class="active">
                                <a onclick="viewFormGuideHome()" href="#formguideHTotal" aria-controls="formguideHTotal"
                                   role="tab" data-toggle="tab">Total</a></li>
                            <li role="presentation">
                                <a onclick="viewFormGuideHome()" href="#formguideHHome" aria-controls="formguideHHome"
                                   role="tab" data-toggle="tab">Home</a></li>
                            <li role="presentation">
                                <a onclick="viewFormGuideHome()" href="#formguideHAway" aria-controls="formguideHAway"
                                   role="tab" data-toggle="tab">Away</a></li>
                            <li class="pull-right">
                                <label class="checkbox-inline">
                                    <input onclick="viewFormGuideHome()" type="checkbox"
                                           class="checkbox_formguide_team1_all" value="all" checked> All
                                </label>
                                <?php foreach ($dataLeagueFormGuideHome as $key => $value) { ?>
                                    <label class="checkbox-inline">
                                        <input onclick="viewFormGuideHome()" type="checkbox"
                                               class="checkbox_formguide_team1_<?= $key ?>"
                                               value="<?= $key ?>"> <?= $value ?>
                                    </label>
                                <?php } ?>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="formguideHTotal">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center mini-col">HDP</th>
                                        <th class="text-center mini-col">Odds</th>
                                        <th class="text-center mini-col">Result</th>
                                    </tr>
                                    </thead>
                                    <?php foreach ($dataFormGuideHome as $key => $value) { ?>
                                        <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                            id="guide-row-original"
                                            class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                            <td>
                                                <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                <p><i class="fa fa-calendar"
                                                      aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                    | <i
                                                        class="fa fa-clock-o"
                                                        aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                </p>
                                            </td>
                                            <td class="text-right box-team-state">
                                                <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                <img src="<?= (!empty($value->teamHomePath)?$value->teamHomePath:'/images/team/team_default_64x64.png'); ?>">
                                            </td>
                                            <td style="width: 12%;">
                                                <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                    - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                            </td>
                                            <td class="text-left box-team-state">
                                                <img src="<?= (!empty($value->teamAwayPath)?$value->teamAwayPath:'/images/team/team_default_64x64.png'); ?>">
                                                <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                            </td>
                                            <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                            <td class="text-center tr-odd">
                                                <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamHomeEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                    <img class="guide-result img-win"
                                                         src="/images/icon-stat/result/0.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamHomeEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                    <img class="guide-result img-lose"
                                                         src="/images/icon-stat/result/2.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                    <img class="guide-result img-draw"
                                                         src="/images/icon-stat/result/1.png">
                                                <?php } else { ?>
                                                    -
                                                <?php } ?>
                                            </td>
                                            <td class="text-center tr-result">
                                                <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamHomeEn == $value->teamAwayEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                    <img class="guide-result img-win"
                                                         src="/images/icon-stat/result/0.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamHomeEn == $value->teamAwayEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                    <img class="guide-result img-lose"
                                                         src="/images/icon-stat/result/2.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                    <img class="guide-result img-draw"
                                                         src="/images/icon-stat/result/1.png">
                                                <?php } else { ?>
                                                    -
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="formguideHHome">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center mini-col">HDP</th>
                                        <th class="text-center mini-col">Odds</th>
                                        <th class="text-center mini-col">Result</th>
                                    </tr>
                                    </thead>
                                    <?php foreach ($dataFormGuideHome as $key => $value) { ?>
                                        <?php if ($matchView->teamHomeEn == $value->teamHomeEn) { ?>
                                            <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                                id="guide-row-original"
                                                class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                                <td>
                                                    <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                    <p><i class="fa fa-calendar"
                                                          aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                        | <i
                                                            class="fa fa-clock-o"
                                                            aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                    </p>
                                                </td>
                                                <td class="text-right box-team-state">
                                                    <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                    <img src="<?= (!empty($value->teamHomePath)?$value->teamHomePath:'/images/team/team_default_64x64.png'); ?>">
                                                </td>
                                                <td>
                                                    <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                        - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                                </td>
                                                <td class="text-left box-team-state">
                                                    <img src="<?= (!empty($value->teamAwayPath)?$value->teamAwayPath:'/images/team/team_default_64x64.png'); ?>">
                                                    <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                                </td>
                                                <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                                <td class="text-center tr-odd">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamHomeEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamHomeEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center tr-result">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamHomeEn == $value->teamAwayEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamHomeEn == $value->teamAwayEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="formguideHAway">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center mini-col">HDP</th>
                                        <th class="text-center mini-col">Odds</th>
                                        <th class="text-center mini-col">Result</th>
                                    </tr>
                                    </thead>
                                    <?php foreach ($dataFormGuideHome as $key => $value) { ?>
                                        <?php if ($matchView->teamHomeEn != $value->teamHomeEn) { ?>
                                            <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                                id="guide-row-original"
                                                class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                                <td>
                                                    <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                    <p><i class="fa fa-calendar"
                                                          aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                        | <i
                                                            class="fa fa-clock-o"
                                                            aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                    </p>
                                                </td>
                                                <td class="text-right box-team-state">
                                                    <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                    <img src="<?= (!empty($value->teamHomePath)?$value->teamHomePath:'/images/team/team_default_64x64.png'); ?>">
                                                </td>
                                                <td>
                                                    <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                        - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                                </td>
                                                <td class="text-left box-team-state">
                                                    <img src="<?= (!empty($value->teamAwayPath)?$value->teamAwayPath:'/images/team/team_default_64x64.png'); ?>">
                                                    <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                                </td>
                                                <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                                <td class="text-center tr-odd">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamHomeEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamHomeEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamHomeEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center tr-result">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamHomeEn == $value->teamAwayEn and $value->awayScore > $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore > $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamHomeEn == $value->teamAwayEn and $value->awayScore < $value->homeScore) or ($matchView->teamHomeEn == $value->teamHomeEn and $value->homeScore < $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both"></div>
    </div>
    <div class="col-sm-6 margin-top formGuide-tabs-team2" style="background-color: #f1f1f1;">
        <div class="pull-left">
            <!--            กราฟวงกลม-->
            <div class="section-state-box section-state-box-circle text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><i class="fa fa-area-chart" aria-hidden="true"></i> Chatdata Odds/ Result</h4>
                            </td>
                            <td class="text-right">

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="col-sm-6">
                        <div class="box-circle-data">
                            <div id="formguidedonut-A-oresult">
                                <img src="/images/chart-simple.png">
                            </div>
                            <div class="text-h2hdonut-h-oresult">Odds</div>
                        </div>
                        <div class="data-chart">
                            <div class="clearfix">
                                <h4 id="oddPWin">41.4</h4>
                                <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                            </div>
                            <div class="clearfix">
                                <h4 id="oddPLose">58.6</h4>
                                <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                            </div>
                            <div class="clearfix">
                                <h4 id="oddPDraw">0</h4>
                                <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-circle-data">
                            <div id="formguidedonut-A-result">
                                <img src="/images/chart-simple.png">
                            </div>
                            <div class="text-h2hdonut-h-oresult">Result</div>
                        </div>
                        <div class="data-chart">
                            <div class="clearfix">
                                <h4 id="resultPWin">41.4</h4>
                                <i style="color: #0091c8;" class="fa fa-circle" aria-hidden="true"></i> Win
                            </div>
                            <div class="clearfix">
                                <h4 id="resultPLose">58.6</h4>
                                <i style="color: #e40520;" class="fa fa-circle" aria-hidden="true"></i> Lose
                            </div>
                            <div class="clearfix">
                                <h4 id="resultPDraw">0</h4>
                                <i style="color: #aaaaaa;" class="fa fa-circle" aria-hidden="true"></i> Draw
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--            ตาราง-->
            <div class="section-state-box text-left">
                <div class="topic-section-state">
                    <table>
                        <tr>
                            <td>
                                <h4><img src="<?= (!empty($matchView->teamAwayPath)?$matchView->teamAwayPath:'/images/team/team_default_64x64.png') ?>"> <?= $matchView->teamAwayEn ?></h4>
                            </td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-default dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Last Match(<label
                                            id="formguide-a-max-match"><?php echo((count($dataFormGuideAway) >= 10) ? '10' : count($dataFormGuideAway)) ?></label>)
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <?php if (count($dataFormGuideAway) >= 20) {
                                            for ($i = 1; $i <= 20; $i++) { ?>
                                                <li><a onclick="viewFormGuideAway(<?= (($i)) ?>)">Last
                                                        Match(<?= (($i)) ?>)</a></li>
                                            <?php }
                                        } else {
                                            for ($i = 1; $i <= (count($dataFormGuideAway) - 1); $i++) { ?>
                                                <li><a onclick="viewFormGuideAway(<?= (($i)) ?>)">Last
                                                        Match(<?= (($i)) ?>)</a></li>
                                            <?php }
                                        } ?>
                                        <li><a onclick="viewFormGuideAway(<?= count($dataFormGuideAway) ?>)">Last
                                                Match(<?= count($dataFormGuideAway) ?>)</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content-section-state row">
                    <div class="box-table-result-state" style="padding: 0 15px;">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-pills" role="tablist">
                            <li role="presentation" class="active">
                                <a onclick="viewFormGuideAway()" href="#formguideATotal" aria-controls="formguideATotal"
                                   role="tab" data-toggle="tab">Total</a></li>
                            <li role="presentation">
                                <a onclick="viewFormGuideAway()" href="#formguideAHome" aria-controls="formguideAHome"
                                   role="tab" data-toggle="tab">Home</a></li>
                            <li role="presentation">
                                <a onclick="viewFormGuideAway()" href="#formguideAAway" aria-controls="formguideAAway"
                                   role="tab" data-toggle="tab">Away</a></li>
                            <li class="pull-right">
                                <label class="checkbox-inline">
                                    <input onclick="viewFormGuideAway()" type="checkbox"
                                           class="checkbox_formguide_team2_all" value="all" checked> All
                                </label>
                                <?php foreach ($dataLeagueFormGuideAway as $key => $value) { ?>
                                    <label class="checkbox-inline">
                                        <input onclick="viewFormGuideAway()" type="checkbox"
                                               class="checkbox_formguide_team2_<?= $key ?>"
                                               value="<?= $key ?>"> <?= $value ?>
                                    </label>
                                <?php } ?>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="formguideATotal">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center mini-col">HDP</th>
                                        <th class="text-center mini-col">Odds</th>
                                        <th class="text-center mini-col">Result</th>
                                    </tr>
                                    </thead>
                                    <?php foreach ($dataFormGuideAway as $key => $value) { ?>
                                        <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                            id="guide-row-original"
                                            class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                            <td>
                                                <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                <p><i class="fa fa-calendar"
                                                      aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                    | <i
                                                        class="fa fa-clock-o"
                                                        aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                </p>
                                            </td>
                                            <td class="text-right box-team-state">
                                                <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                <img src="<?= (!empty($value->teamHomePath)?$value->teamHomePath:'/images/team/team_default_64x64.png') ?>">
                                            </td>
                                            <td style="width: 12%;">
                                                <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                    - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                            </td>
                                            <td class="text-left box-team-state">
                                                <img src="<?= (!empty($value->teamAwayPath)?$value->teamAwayPath:'/images/team/team_default_64x64.png'); ?>">
                                                <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                            </td>
                                            <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                            <td class="text-center tr-odd">
                                                <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamAwayEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                    <img class="guide-result img-win"
                                                         src="/images/icon-stat/result/0.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamAwayEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                    <img class="guide-result img-lose"
                                                         src="/images/icon-stat/result/2.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                    <img class="guide-result img-draw"
                                                         src="/images/icon-stat/result/1.png">
                                                <?php } else { ?>
                                                    -
                                                <?php } ?>
                                            </td>
                                            <td class="text-center tr-result">
                                                <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->homeScore > $value->awayScore) or ($matchView->teamAwayEn == $value->teamAwayEn and $value->awayScore > $value->homeScore))) { ?>
                                                    <img class="guide-result img-win"
                                                         src="/images/icon-stat/result/0.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamAwayEn == $value->teamAwayEn and $value->homeScore > $value->awayScore))) { ?>
                                                    <img class="guide-result img-lose"
                                                         src="/images/icon-stat/result/2.png">
                                                <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                    <img class="guide-result img-draw"
                                                         src="/images/icon-stat/result/1.png">
                                                <?php } else { ?>
                                                    -
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="formguideAHome">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center mini-col">HDP</th>
                                        <th class="text-center mini-col">Odds</th>
                                        <th class="text-center mini-col">Result</th>
                                    </tr>
                                    </thead>
                                    <?php foreach ($dataFormGuideAway as $key => $value) { ?>
                                        <?php if ($matchView->teamAwayEn != $value->teamAwayEn) { ?>
                                            <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                                id="guide-row-original"
                                                class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                                <td>
                                                    <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                    <p><i class="fa fa-calendar"
                                                          aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                        | <i
                                                            class="fa fa-clock-o"
                                                            aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                    </p>
                                                </td>
                                                <td class="text-right box-team-state">
                                                    <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                    <img src="<?= (!empty($value->teamHomePath)?$value->teamHomePath:'/images/team/team_default_64x64.png'); ?>">
                                                </td>
                                                <td>
                                                    <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                        - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                                </td>
                                                <td class="text-left box-team-state">
                                                    <img src="<?= (!empty($value->teamAwayPath)?$value->teamAwayPath:'/images/team/team_default_64x64.png') ?>">
                                                    <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                                </td>
                                                <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                                <td class="text-center tr-odd">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamAwayEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamAwayEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center tr-result">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->homeScore > $value->awayScore) or ($matchView->teamAwayEn == $value->teamAwayEn and $value->awayScore > $value->homeScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamAwayEn == $value->teamAwayEn and $value->homeScore > $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="formguideAAway">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center mini-col">HDP</th>
                                        <th class="text-center mini-col">Odds</th>
                                        <th class="text-center mini-col">Result</th>
                                    </tr>
                                    </thead>
                                    <?php foreach ($dataFormGuideAway as $key => $value) { ?>
                                        <?php if ($matchView->teamAwayEn == $value->teamAwayEn) { ?>
                                            <tr league_id="<?= $value->league_id ?>" number="<?= $key + 1; ?>"
                                                id="guide-row-original"
                                                class="guide-row tr-h2h tr-h2h-<?= (($matchView->teamHomeEn == $value->teamHomeEn) ? 'home' : 'away') ?> tr-h2h-<?= floor(($key) / 10) ?> <?= ((floor(($key) / 10) == 0) ? '' : 'hide') ?>">
                                                <td>
                                                    <span><img src="<?= $value->leaguePath ?>"> <?= $value->leagueName ?></span>
                                                    <p><i class="fa fa-calendar"
                                                          aria-hidden="true"></i> <?= date("d/m/Y", strtotime($value->time_match)) ?>
                                                        | <i
                                                            class="fa fa-clock-o"
                                                            aria-hidden="true"></i> <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
                                                    </p>
                                                </td>
                                                <td class="text-right box-team-state">
                                                    <span class="name-team-state"><?= $value->teamHomeEn ?></span>
                                                    <img src="<?= (!empty($value->teamHomePath)?$value->teamHomePath:'/images/team/team_default_64x64.png'); ?>">
                                                </td>
                                                <td>
                                                    <div class="score-state"><?= (($value->homeScore == null) ? 0 : $value->homeScore) ?>
                                                        - <?= (($value->awayScore == null) ? 0 : $value->awayScore) ?></div>
                                                </td>
                                                <td class="text-left box-team-state">
                                                    <img src="<?= (!empty($value->teamAwayPath)?$value->teamAwayPath:'/images/team/team_default_64x64.png'); ?>">
                                                    <span class="name-team-state"><?= $value->teamAwayEn ?></span>
                                                </td>
                                                <td class="text-center"><?= (($value->hdp == null) ? '-' : $value->hdp) ?></td>
                                                <td class="text-center tr-odd">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) > $value->awayScore)) or ($matchView->teamAwayEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) < $value->awayScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and ((($matchView->teamAwayEn == $value->teamHomeEn and ($value->homeScore + $value->hdp) < $value->awayScore)) or ($matchView->teamAwayEn == $value->teamAwayEn and ($value->homeScore + $value->hdp) > $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null and $value->hdp != null) and (($value->homeScore + $value->hdp) == $value->awayScore)) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center tr-result">
                                                    <?php if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->homeScore > $value->awayScore) or ($matchView->teamAwayEn == $value->teamAwayEn and $value->awayScore > $value->homeScore))) { ?>
                                                        <img class="guide-result img-win"
                                                             src="/images/icon-stat/result/0.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and (($matchView->teamAwayEn == $value->teamHomeEn and $value->awayScore > $value->homeScore) or ($matchView->teamAwayEn == $value->teamAwayEn and $value->homeScore > $value->awayScore))) { ?>
                                                        <img class="guide-result img-lose"
                                                             src="/images/icon-stat/result/2.png">
                                                    <?php } else if (($value->homeScore !== null and $value->awayScore !== null) and $value->homeScore == $value->awayScore) { ?>
                                                        <img class="guide-result img-draw"
                                                             src="/images/icon-stat/result/1.png">
                                                    <?php } else { ?>
                                                        -
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both"></div>
    </div>
</div>
<?php if(!empty($dataNextMatchHome)){ ?>
    <!--Next Match-->
    <div class="content-state content-formGuide col-lg-12">
        <!--    <div class="head-state">-->
        <!--        <h2>Next Match</h2>-->
        <!--    </div>-->
        <div class="content-state-game" style="width: 181px;">
            <h2>Next Match</h2>
            <div class="head-state-game"></div>
        </div>
        <div class="col-sm-6">
            <div class="pull-right"  style="width: 100%;">
                <!--            ตาราง-->
                <div class="section-next-match section-state-box text-left">
                    <div class="topic-section-state">
                        <table>
                            <tr>
                                <td>
                                    <h4><img src="<?= (!empty($matchView->teamHomePath)?$matchView->teamHomePath:'/images/team/team_default_64x64.png'); ?>"> <?= $matchView->teamHomeEn ?></h4>
                                </td>
                                <td class="text-right">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="content-section-state row">
                        <div class="box-table-result-state" style="padding: 0 15px;">
                            <table class="table table-striped">
                                <?php foreach ($dataNextMatchHome as $value) { ?>
                                    <tr>
                                        <td>
                                            <span><img src="<?= $value->leaguePath ?>"> <?php echo $value->leagueName; ?></span>
                                        </td>
                                        <td>
                                            <p><i class="fa fa-calendar"
                                                  aria-hidden="true"></i> <?php date("d/m/Y", strtotime($value->time_match)); ?>
                                            </p>
                                        </td>
                                        <td class="text-right">
                                            <span><?php echo $value->teamHomeEn; ?> <img src="<?= (!empty($value->teamHomePath)?$value->teamHomePath:'/images/team/team_default_64x64.png'); ?>"></span>
                                        </td>
                                        <td>
                                            <div class="score-state">
                                                <?php date("H:i", strtotime($value->time_match)); ?>
                                            </div>
                                        </td>
                                        <td class="text-left">
                                            <span><img src="<?= (!empty($value->teamAweyPath)?$value->teamAweyPath:'/images/team/team_default_64x64.png'); ?>"> <?php echo $value->teamAweyEn; ?></span>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
        <div class="col-sm-6" style="background-color: #f1f1f1;">
            <div class="pull-left"  style="width: 100%;">
                <!--            ตาราง-->
                <div class="section-next-match section-state-box text-left">
                    <div class="topic-section-state">
                        <table>
                            <tr>
                                <td>
                                    <h4><img src="<?= (!empty($matchView->teamAwayPath)?$matchView->teamAwayPath:'/images/team/team_default_64x64.png'); ?>"> <?= $matchView->teamAwayEn ?></h4>
                                </td>
                                <td class="text-right">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="content-section-state row">
                        <div class="box-table-result-state" style="padding: 0 15px;">
                            <table class="table table-striped">
                                <?php foreach ($dataNextMatchAway as $value) { ?>
                                    <tr>
                                        <td>
                                            <span><img src="<?= $value->leaguePath ?>"> <?php echo $value->leagueName; ?></span>
                                        </td>
                                        <td>
                                            <p><i class="fa fa-calendar"
                                                  aria-hidden="true"></i> <?php date("d/m/Y", strtotime($value->time_match)); ?>
                                            </p>
                                        </td>
                                        <td class="text-right">
                                            <span><?php echo $value->teamHomeEn; ?> <img src="<?= (!empty($value->teamHomePath)?$value->teamHomePath:'/images/team/team_default_64x64.png'); ?>"></span>
                                        </td>
                                        <td>
                                            <div class="score-state">
                                                <?php date("H:i", strtotime($value->time_match)); ?>
                                            </div>
                                        </td>
                                        <td class="text-left">
                                            <span><img src="<?= (!empty($value->teamAweyPath)?$value->teamAweyPath:'/images/team/team_default_64x64.png'); ?>"> <?php echo $value->teamAweyEn; ?></span>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>
<?php } ?>
<!--Table-->
<?php if (!empty($tableLeague) and count($tableLeague) <= 20) { ?>
    <div class="content-state content-formGuide col-lg-12">
        <div class="content-state-game" style="width: 123px;">
            <h2>Table</h2>
            <div class="head-state-game"></div>
        </div>
        <div class="col-sm-6">
            <div class="pull-right" style="width: 100%;">
                <!--            ตาราง-->
                <div class="section-table section-state-box text-left">
                    <div class="topic-section-state">
                        <table>
                            <tr>
                                <td>
                                    <h4><img src="<?php echo $matchView->path; ?>"> <?php echo $matchView->name; ?></h4>
                                </td>
                                <td class="text-right">
                                    <button onclick="showRank('home')" type="button" class="btn btn-link"><strong>Show
                                            All</strong>
                                        <span id="glyphicon-home" class="glyphicon glyphicon-triangle-bottom"
                                              aria-hidden="true"></span>
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="content-section-state row">
                        <div class="box-table-result-state" style="padding: 0 15px;">
                            <table class="table table-state">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Team</th>
                                    <th>P</th>
                                    <th>W</th>
                                    <th>D</th>
                                    <th>L</th>
                                    <th>GF</th>
                                    <th>GA</th>
                                    <th>GD</th>
                                    <th>PTS</th>
                                </tr>
                                </thead>
                                <?php if (!empty($tableLeague)) {
                                    foreach ($tableLeague as $key => $value) {
                                        if (!empty($value)) { ?>
                                            <tr id=""
                                                class="rank-home <?= ((!empty($value["name_en"]) and ($value["name_en"] == $matchView->teamHomeEn)) ? 'active' : '') ?> <?= ((!empty($value["name_en"]) and ($value["name_en"] == $matchView->teamHomeEn || $value["name_en"] == $matchView->teamAwayEn)) ? 'view' : 'hide') ?>">
                                                <td class="rank-no"
                                                    style="color: #000000 !important;"><?php echo $key + 1; ?></td>
                                                <td>
                                                    <img class="rank-logo"
                                                         src="<?= ((!empty($value["path"])) ? $value["path"] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                    <label class="rank-team"><?php echo $value["name_en"]; ?></label>
                                                </td>
                                                <td class="rank-play"><?php echo $value["PI"]; ?></td>
                                                <td class="rank-win"><?php echo $value["win"]; ?></td>
                                                <td class="rank-draw"><?php echo $value["draw"]; ?></td>
                                                <td class="rank-lose"><?php echo $value["lose"]; ?></td>
                                                <td class="rank-gf"><?php echo $value["GF"]; ?></td>
                                                <td class="rank-ga"><?php echo $value["GA"]; ?></td>
                                                <td class="rank-gd"><?php echo $value["GD"]; ?></td>
                                                <td class="rank-pts"><?php echo $value["totalPts"]; ?></td>
                                            </tr>
                                        <?php }
                                    }
                                } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
        <div class="col-sm-6 margin-top" style="background-color: #f1f1f1;">
            <div class="pull-left"  style="width: 100%;">
                <!--            ตาราง-->
                <div class="section-table section-state-box text-left">
                    <div class="topic-section-state">
                        <table>
                            <tr>
                                <td>
                                    <h4><img src="<?php echo $matchView->path; ?>"> <?php echo $matchView->name; ?></h4>
                                </td>
                                <td class="text-right">
                                    <button onclick="showRank('away')" type="button" class="btn btn-link"><strong>Show
                                            All</strong>
                                        <span id="glyphicon-away" class="glyphicon glyphicon-triangle-bottom"
                                              aria-hidden="true"></span>
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="content-section-state row">
                        <div class="box-table-result-state" style="padding: 0 15px;">
                            <table class="table table-state">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Team</th>
                                    <th>P</th>
                                    <th>W</th>
                                    <th>D</th>
                                    <th>L</th>
                                    <th>GF</th>
                                    <th>GA</th>
                                    <th>GD</th>
                                    <th>PTS</th>
                                </tr>
                                </thead>
                                <?php if (!empty($tableLeague)) {
                                    foreach ($tableLeague as $key => $value) {
                                        if (!empty($value)) { ?>
                                            <tr id=""
                                                class="rank-away <?= ((!empty($value["name_en"]) and ($value["name_en"] == $matchView->teamAwayEn)) ? 'active' : '') ?> <?= ((!empty($value["name_en"]) and ($value["name_en"] == $matchView->teamHomeEn || $value["name_en"] == $matchView->teamAwayEn)) ? 'view' : 'hide') ?>">
                                                <td class="rank-no"
                                                    style="color: #000000 !important;"><?php echo $key + 1; ?></td>
                                                <td>
                                                    <img class="rank-logo"
                                                         src="<?= ((!empty($value["path"])) ? $value["path"] : '/images/teams_clean/team_default_32x32.png'); ?>">
                                                    <label class="rank-team"><?php echo $value["name_en"]; ?></label>
                                                </td>
                                                <td class="rank-play"><?php echo $value["PI"]; ?></td>
                                                <td class="rank-win"><?php echo $value["win"]; ?></td>
                                                <td class="rank-draw"><?php echo $value["draw"]; ?></td>
                                                <td class="rank-lose"><?php echo $value["lose"]; ?></td>
                                                <td class="rank-gf"><?php echo $value["GF"]; ?></td>
                                                <td class="rank-ga"><?php echo $value["GA"]; ?></td>
                                                <td class="rank-gd"><?php echo $value["GD"]; ?></td>
                                                <td class="rank-pts"><?php echo $value["totalPts"]; ?></td>
                                            </tr>
                                        <?php }
                                    }
                                } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>
<?php } ?>
<div style="clear: both;"></div>

<!-- Modal Add -->
<div class="modal fade" id="addReview" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="width: 470px;" id="userReviewForm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มทรรศนะ</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ชือ</label>
                        <div class="col-sm-10">
                            <div class="clearfix">
                                <div class="box-guru-img pull-left">
                                    <img src="/images/sample-logo01.png">
                                </div>
                                <h4>Thscore</h4>
                            </div>
                            <div class="clearfix">
                                <div class="box-user-img pull-left">
                                    <img src="/images/p1.png">
                                </div>
                                <h4>กระบี่มือหนึ่ง</h4>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ทีมที่เลือก</label>
                        <div class="col-sm-10">
                            <div class="box-player-preview">
                                <img src="/images/logo-team/manchester.png">
                                <span>@ Manchester UTD</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ทรรศนะ</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                <button type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่ม
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Preview Guru-->
<div class="modal fade" id="showReviewGuru" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="modalBody-showReviewGuru-<?php echo $value->match_id;?>">
            <div class="modal-header" style="padding: 0px;border-bottom: transparent;">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a style="padding: 15px 20px;" href="#home-<?php echo $id;?>" aria-controls="home" role="tab" data-toggle="tab">
                            <div style="font-size: 20px;">ทั้งหมด(<?php echo count($reviewMatch); ?>)</div></a></li>
                    <li role="presentation"><a style="padding: 15px 20px;font-size: 20px;" href="#profile-<?php echo $id;?>" aria-controls="profile" role="tab" data-toggle="tab">
                            <img style="width: 26px;" src="<?php echo (!empty($matchView->teamHomePath)?$matchView->teamHomePath:'/images/team/team_default_64x64.png'); ?>"> <?php echo $matchView->teamHomeEn;?>(<?php echo $reviewHome;?>)</a></li>
                    <li role="presentation"><a style="padding: 15px 20px;font-size: 20px;" href="#messages-<?php echo $id;?>" aria-controls="messages" role="tab" data-toggle="tab">
                            <img style="width: 26px;" src="<?php echo (!empty($matchView->teamAwayPath)?$matchView->teamAwayPath:'/images/team/team_default_64x64.png') ?>"> <?php echo $matchView->teamAwayEn;?>(<?php echo $reviewAway;?>)</a></li>
                </ul>
            </div>
            <div class="modal-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home-<?php echo $id;?>">
                        <?php if(!empty($reviewMatch)){ ?>
                            <?php foreach ($reviewMatch as $key => $value) { ?>
                                <div class="box-preview" style="padding: 15px; <?php echo (($key%2==0)?'background-color: #e4e4e4':'');?>">
                                    <div class="top-box-preview">
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="box-guru-img pull-left">
                                                        <img style="height: 40px;" src="<?= $value['path'] ?>">
                                                    </div>
                                                    <div class="pull-left">
                                                        <h5><?= $value['name'] ?></h5>
                                                        <div class="box-player-preview">
                                                            <?php if ($value['team'] == "home") { ?>
                                                                <img src="<?= (!empty($value['homepath'])?$value['homepath']:'/images/team/team_default_64x64.png'); ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                            <?php } elseif ($value['team'] == "away") { ?>
                                                                <img src="<?= (!empty($value['awaypath'])?$value['awaypath']:'/images/team/team_default_64x64.png'); ?>">
                                                                <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="pull-left">
                                                        <ul class="box-betlist">
                                                            <?php if (!empty($playReviewMatchList[$value['user_review_id']])) { ?>
                                                                <?php foreach ($playReviewMatchList[$value['user_review_id']] as $keyr => $valuer) { ?>
                                                                    <?php if ($valuer['results'] == "win") { ?>
                                                                        <li><img title="<?= $valuer['updated_at'] ?>"
                                                                                 src="/images/icon-stat/betlist_result/win.png"></li>
                                                                    <?php } else if ($valuer['results'] == "lose") { ?>
                                                                        <li><img title="<?= $valuer['updated_at'] ?>"
                                                                                 src="/images/icon-stat/betlist_result/lose.png"></li>
                                                                    <?php } else if ($valuer['results'] == "equal") { ?>
                                                                        <li><img title="<?= $valuer['updated_at'] ?>"
                                                                                 src="/images/icon-stat/betlist_result/draw.png"></li>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="pull-right">
                                                        <h3><?= $resultsReviewMatch[$value['user_review_id']]; ?>%</h3>
                                                        <p>Win rate</p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="button-box-preview">
                                        <div>
                                            <?= $value['review_text'] ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="profile-<?php echo $id;?>">
                        <?php if(!empty($reviewMatch)){ ?>
                            <?php foreach ($reviewMatch as $key => $value) { ?>
                                <?php if($value['team']=="home"){ ?>
                                    <div class="box-preview" style="padding: 15px; <?php echo (($key%2==0)?'background-color: #e4e4e4':'');?>">
                                        <div class="top-box-preview">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div class="box-guru-img pull-left">
                                                            <img style="height: 40px;" src="<?= $value['path'] ?>">
                                                        </div>
                                                        <div class="pull-left">
                                                            <h5><?= $value['name'] ?></h5>
                                                            <div class="box-player-preview">
                                                                <?php if ($value['team'] == "home") { ?>
                                                                    <img src="<?= (!empty($value['homepath'])?$value['homepath']:'/images/team/team_default_64x64.png'); ?>">
                                                                    <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                                <?php } elseif ($value['team'] == "away") { ?>
                                                                    <img src="<?= (!empty($value['awaypath'])?$value['awaypath']:'/images/team/team_default_64x64.png'); ?>">
                                                                    <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <div class="pull-left">
                                                            <ul class="box-betlist">
                                                                <?php if (!empty($playReviewMatchList[$value['user_review_id']])) { ?>
                                                                    <?php foreach ($playReviewMatchList[$value['user_review_id']] as $keyr => $valuer) { ?>
                                                                        <?php if ($valuer['results'] == "win") { ?>
                                                                            <li><img title="<?= $valuer['updated_at'] ?>"
                                                                                     src="/images/icon-stat/betlist_result/win.png"></li>
                                                                        <?php } else if ($valuer['results'] == "lose") { ?>
                                                                            <li><img title="<?= $valuer['updated_at'] ?>"
                                                                                     src="/images/icon-stat/betlist_result/lose.png"></li>
                                                                        <?php } else if ($valuer['results'] == "equal") { ?>
                                                                            <li><img title="<?= $valuer['updated_at'] ?>"
                                                                                     src="/images/icon-stat/betlist_result/draw.png"></li>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="pull-right">
                                                            <h3><?= $resultsReviewMatch[$value['user_review_id']]; ?>%</h3>
                                                            <p>Win rate</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="button-box-preview">
                                            <div>
                                                <?= $value['review_text'] ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="messages-<?php echo $id;?>">
                        <?php if(!empty($reviewMatch)){ ?>
                            <?php foreach ($reviewMatch as $key => $value) { ?>
                                <?php if($value['team']=="away"){ ?>
                                    <div class="box-preview" style="padding: 15px; <?php echo (($key%2==0)?'background-color: #e4e4e4':'');?>">
                                        <div class="top-box-preview">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div class="box-guru-img pull-left">
                                                            <img style="height: 40px;" src="<?= $value['path'] ?>">
                                                        </div>
                                                        <div class="pull-left">
                                                            <h5><?= $value['name'] ?></h5>
                                                            <div class="box-player-preview">
                                                                <?php if ($value['team'] == "home") { ?>
                                                                    <img src="<?= (!empty($value['homepath'])?$value['homepath']:'/images/team/team_default_64x64.png'); ?>">
                                                                    <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                                <?php } elseif ($value['team'] == "away") { ?>
                                                                    <img src="<?= (!empty($value['awaypath'])?$value['awaypath']:'/images/team/team_default_64x64.png'); ?>">
                                                                    <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <div class="pull-left">
                                                            <ul class="box-betlist">
                                                                <?php if (!empty($playReviewMatchList[$value['user_review_id']])) { ?>
                                                                    <?php foreach ($playReviewMatchList[$value['user_review_id']] as $keyr => $valuer) { ?>
                                                                        <?php if ($valuer['results'] == "win") { ?>
                                                                            <li><img title="<?= $valuer['updated_at'] ?>"
                                                                                     src="/images/icon-stat/betlist_result/win.png"></li>
                                                                        <?php } else if ($valuer['results'] == "lose") { ?>
                                                                            <li><img title="<?= $valuer['updated_at'] ?>"
                                                                                     src="/images/icon-stat/betlist_result/lose.png"></li>
                                                                        <?php } else if ($valuer['results'] == "equal") { ?>
                                                                            <li><img title="<?= $valuer['updated_at'] ?>"
                                                                                     src="/images/icon-stat/betlist_result/draw.png"></li>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="pull-right">
                                                            <h3><?= $resultsReviewMatch[$value['user_review_id']]; ?>%</h3>
                                                            <p>Win rate</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="button-box-preview">
                                            <div>
                                                <?= $value['review_text'] ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php ?>
<script>

    $.get( "/checkLogInUser", function( data ) {
        $(".content-profile-menu").html(data);
        console.log("id-user-bet::"+$('#id-user-bet').val());
        $('input[name="bet[user_id]"]').val($('#id-user-bet').val());
    });

    $.get( "/content-hdp-game?id="+$('input#getMatchId').val(), function( data ) {
        $("#content-HDP-game").html(data);
    });

    google.charts.load("current", {packages: ["corechart"]});

    $(document).on('submit', '#user-review-match', function (e) {
        e.preventDefault();
        $.ajax({
            url: "/services/createReviewMatch",
            data: $(this).serialize(),
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
            $.ajax({
                url: "/load/gameNew",
                data: {method:'GET',type: 'game',matchId:response},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                if (response['status']) {
                    location.reload();
                }
            });
        });
    });

    // $(document).on("click","td.btn-results-left",function() {
    //     var number=parseInt($(this).attr('number'));
    //     var total=parseInt($(this).attr('total'));
    //     var max=parseInt($(this).attr('max'));
    //     if(number==0){
    //         number=total;
    //     }else {
    //         number-=1;
    //     }
    //     console.log(max+" :: "+((number*5)));
    //     $(this).parent('tr').find('.li-results').addClass('hide');
    //     $(this).parent('tr').find('[number='+number+']').removeClass('hide');
    //     $(this).parent('tr').find('span.total-results').html(max-((number*5)));
    //     $(this).attr('number',number);
    // });
    //
    // $(document).on("click","td.btn-results-right",function() {
    //     var number=parseInt($(this).attr('number'));
    //     var total=parseInt($(this).attr('total'));
    //     var max=parseInt($(this).attr('max'));
    //     if(number==total){
    //         number=0;
    //     }else {
    //         number+=1;
    //     }
    //     // console.log($(this).parent('tr').attr('class')+" :: "+number);
    //     $(this).parent('tr').find('.li-results').addClass('hide');
    //     $(this).parent('tr').find('[number='+number+']').removeClass('hide');
    //     $(this).parent('tr').find('span.total-results').html(((number*5)+5));
    //     $(this).attr('number',number);
    // });

    $(document).ready(function () {
        viewFormGuideAway($('.formGuide-tabs-team2').find('#formguide-a-max-match').html())
        viewFormGuideHome($('.formGuide-tabs-team1').find('#formguide-h-max-match').html());
        viewHeadToHeadHome($('.h2h-tabs-team1').find('#h2h-h-max-match').html());
        viewHeadToHeadAway($('.h2h-tabs-team2').find('#h2h-a-max-match').html());

        $("#ul-liveScoreHead").find('li.match-slide').each(function (index) {
            $(this).css('width', ($('#liveScoreHead').width() / 4));
        });

        $(window).resize(function () {
            $("#ul-liveScoreHead").find('li.match-slide').each(function (index) {
                $(this).css('width', ($('#liveScoreHead').width() / 4));
            });
        });

        var $frame = $('#liveScoreHead');
        var startAt = $("#liveScoreHead").find("li.active").first().attr('data-key');
        var frame = new Sly('#liveScoreHead', {
//        slidee:$frame.find('#ul-liveScoreHead'),
            horizontal: 1,
            itemNav: 'basic',
            smart: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 0,
//            scrollBar: $wrap.find('.scrollbar'),
            scrollBy: 1,
            speed: 2000,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,

            // Cycling
            cycleBy: 'items',
            cycleInterval: 6000,
            pauseOnHover: 1,

            // Buttons
//        forward: $wrap.find('.btnforward'),
//        backward: $wrap.find('.btnbackward'),
//        prev: $wrap.find('.btnbackward'),
//        next: $wrap.find('.btnforward'),
//        prevPage: $wrap.find('.backward'),
//        nextPage: $wrap.find('.forward'),

//        draggedClass:  'dragged', // Class for dragged elements (like SLIDEE or scrollbar handle).
            activeClass: 'active-game',  // Class for active items and pages.
//        disabledClass: 'disabled' // Class for disabled navigation elements.
        });
        frame.init();

    });

    function userReviewForm() {
        $.get("/user-review-form?id=" + $('input#getMatchId').val(), function (data) {
            // console.log(data);
            if (data !== "false") {
                // console.log("IN");
                $('#userReviewForm').html(data);
                $('#addReview').modal('show');
            }else{
                alert("กรุณา Vote ทีมก่อน การแสดงทรรศนะ")
            }
        });
    }

    function showRank(status) {
        if (status == "home") {
            if ($('span#glyphicon-home').hasClass('glyphicon-triangle-bottom')) {
                $('span#glyphicon-home').removeClass('glyphicon-triangle-bottom');
                $('span#glyphicon-home').addClass('glyphicon-triangle-top');
                $('tr.rank-home').removeClass('hide');
            } else if ($('span#glyphicon-home').hasClass('glyphicon-triangle-top')) {
                $('span#glyphicon-home').removeClass('glyphicon-triangle-top');
                $('span#glyphicon-home').addClass('glyphicon-triangle-bottom');
                $('tr.rank-home').not('.view').addClass('hide');
            }
        } else if (status == "away") {
            if ($('span#glyphicon-away').hasClass('glyphicon-triangle-bottom')) {
                $('span#glyphicon-away').removeClass('glyphicon-triangle-bottom');
                $('span#glyphicon-away').addClass('glyphicon-triangle-top');
                $('tr.rank-away').removeClass('hide');
            } else if ($('span#glyphicon-away').hasClass('glyphicon-triangle-top')) {
                $('span#glyphicon-away').removeClass('glyphicon-triangle-top');
                $('span#glyphicon-away').addClass('glyphicon-triangle-bottom');
                $('tr.rank-away').not('.view').addClass('hide');
            }
        }
    }

    function viewFormGuideAway(total=null) {
//        console.log("viewFormGuideAway");
        if (total == null) {
            total = parseInt($('#formguide-a-max-match').html());
        }
//        console.log(total);
        var resultWin = 0, resultLose = 0, resultDraw = 0, resultPWin = 0, resultPLose = 0, resultPDraw = 0, oddWin = 0,
            oddLose = 0, oddDraw = 0, oddPWin = 0, oddPLose = 0, oddPDraw = 0;
        $('.formGuide-tabs-team2').find('tr.tr-h2h').addClass('hide');

        for (i = 1; i <= total; i++) {

            if ($('input.checkbox_formguide_team2_all').is(':checked')) {
                $('.formGuide-tabs-team2').find('tr[number=' + i + ']').removeClass('hide');
            } else {
//                console.log($('.formGuide-tabs-team2').find('tr[number=' + i + ']').attr('league_id'));
                if ($('input.checkbox_formguide_team2_' + $('.formGuide-tabs-team2').find('tr[number=' + i + ']').attr('league_id')).is(':checked')) {
                    $('.formGuide-tabs-team2').find('tr[number=' + i + ']').removeClass('hide');
                }
            }
//            console.log($('.h2h-tabs-team1').find('tr[number='+i+']').length);
        }
        setTimeout(function () {
//            console.log("Total: "+$('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").length);
            resultWin = $('.formGuide-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-win').length;
            resultLose = $('.formGuide-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-lose').length;
            resultDraw = $('.formGuide-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-draw').length;
            oddWin = $('.formGuide-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-win').length;
            oddLose = $('.formGuide-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-lose').length;
            oddDraw = $('.formGuide-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-draw').length;

//            console.log(resultWin + "::" + resultLose + "::" + resultDraw + "::" + oddWin + "::" + oddLose + "::" + oddDraw);

            resultPWin = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultWin) / (resultWin + resultLose + resultDraw)) * 100));
            resultPLose = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultLose) / (resultWin + resultLose + resultDraw)) * 100));
            resultPDraw = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultDraw) / (resultWin + resultLose + resultDraw)) * 100));

            console.log("resultPWin :: "+resultPWin+" resultPLose :: "+resultPLose+" resultPDraw: "+resultPDraw);

            if (oddWin !== 0 || oddLose !== 0 || oddDraw !== 0) {
                oddPWin = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddWin) / (oddWin + oddLose + oddDraw)) * 100));
                oddPLose = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddLose) / (oddWin + oddLose + oddDraw)) * 100));
                oddPDraw = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddDraw) / (oddWin + oddLose + oddDraw)) * 100));
            }

            $('.formGuide-tabs-team2').find('#resultPWin').html(resultWin);
            $('.formGuide-tabs-team2').find('#resultPLose').html(resultLose);
            $('.formGuide-tabs-team2').find('#resultPDraw').html(resultDraw);

            $('.formGuide-tabs-team2').find('#oddPWin').html(oddWin);
            $('.formGuide-tabs-team2').find('#oddPLose').html(oddLose);
            $('.formGuide-tabs-team2').find('#oddPDraw').html(oddDraw);

            google.charts.setOnLoadCallback(
                function () {
                    drawDonut('formguidedonut-A-result', [['result', 'value'], ['draw', resultPDraw], ['lose', resultPLose], ['win', resultPWin]]);
                    drawDonut('formguidedonut-A-oresult', [['result', 'value'], ['draw', oddPDraw], ['lose', oddPLose], ['win', oddPWin]]);
                }
            )

            $('.formGuide-tabs-team2').find('#formguide-a-max-match').html(total);
        }, 500);
    }
    function viewFormGuideHome(total=null) {
        if (total == null) {
            total = parseInt($('#formguide-h-max-match').html());
        }
//        console.log(total);
        var resultWin = 0, resultLose = 0, resultDraw = 0, resultPWin = 0, resultPLose = 0, resultPDraw = 0, oddWin = 0,
            oddLose = 0, oddDraw = 0, oddPWin = 0, oddPLose = 0, oddPDraw = 0;
        $('.formGuide-tabs-team1').find('tr.tr-h2h').addClass('hide');

        for (i = 1; i <= total; i++) {

            if ($('input.checkbox_formguide_team1_all').is(':checked')) {
                $('.formGuide-tabs-team1').find('tr[number=' + i + ']').removeClass('hide');
            } else {
                // console.log($('.formGuide-tabs-team1').find('tr[number=' + i + ']').attr('league_id'));
                if ($('input.checkbox_formguide_team1_' + $('.formGuide-tabs-team1').find('tr[number=' + i + ']').attr('league_id')).is(':checked')) {
                    $('.formGuide-tabs-team1').find('tr[number=' + i + ']').removeClass('hide');
                }
            }
//            console.log($('.h2h-tabs-team1').find('tr[number='+i+']').length);
        }
        setTimeout(function () {
//            console.log("Total: "+$('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").length);
            resultWin = $('.formGuide-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-win').length;
            resultLose = $('.formGuide-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-lose').length;
            resultDraw = $('.formGuide-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-draw').length;
            oddWin = $('.formGuide-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-win').length;
            oddLose = $('.formGuide-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-lose').length;
            oddDraw = $('.formGuide-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-draw').length;

            // console.log(resultWin + "::" + resultLose + "::" + resultDraw + "::" + oddWin + "::" + oddLose + "::" + oddDraw);

            resultPWin = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultWin) / (resultWin + resultLose + resultDraw)) * 100));
            resultPLose = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultLose) / (resultWin + resultLose + resultDraw)) * 100));
            resultPDraw = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultDraw) / (resultWin + resultLose + resultDraw)) * 100));

            if (oddWin !== 0 || oddLose !== 0 || oddDraw !== 0) {
                oddPWin = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddWin) / (oddWin + oddLose + oddDraw)) * 100));
                oddPLose = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddLose) / (oddWin + oddLose + oddDraw)) * 100));
                oddPDraw = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddDraw) / (oddWin + oddLose + oddDraw)) * 100));
            }

            $('.formGuide-tabs-team1').find('#resultPWin').html(resultWin);
            $('.formGuide-tabs-team1').find('#resultPLose').html(resultLose);
            $('.formGuide-tabs-team1').find('#resultPDraw').html(resultDraw);

            $('.formGuide-tabs-team1').find('#oddPWin').html(oddWin);
            $('.formGuide-tabs-team1').find('#oddPLose').html(oddLose);
            $('.formGuide-tabs-team1').find('#oddPDraw').html(oddDraw);

            google.charts.setOnLoadCallback(
                function () {
                    drawDonut('formguidedonut-H-result', [['result', 'value'], ['draw', resultPDraw], ['lose', resultPLose], ['win', resultPWin]]);
                    drawDonut('formguidedonut-H-oresult', [['result', 'value'], ['draw', oddPDraw], ['lose', oddPLose], ['win', oddPWin]]);
                }
            )

            $('.formGuide-tabs-team1').find('#formguide-h-max-match').html(total);
        }, 500);
    }
    function viewHeadToHeadAway(total=null) {
        if (total == null) {
            total = parseInt($('#h2h-a-max-match').html());
        }
//        console.log(total);
        var resultWin = 0, resultLose = 0, resultDraw = 0, resultPWin = 0, resultPLose = 0, resultPDraw = 0, oddWin = 0,
            oddLose = 0, oddDraw = 0, oddPWin = 0, oddPLose = 0, oddPDraw = 0;
        $('.h2h-tabs-team2').find('tr.tr-h2h').addClass('hide');

        for (i = 1; i <= total; i++) {

            if ($('input.checkbox_team2_all').is(':checked')) {
                $('.h2h-tabs-team2').find('tr[number=' + i + ']').removeClass('hide');
            } else {
//                console.log($('.h2h-tabs-team1').find('tr[number=' + i + ']').attr('league_id'));
                if ($('input.checkbox_team2_' + $('.h2h-tabs-team2').find('tr[number=' + i + ']').attr('league_id')).is(':checked')) {
                    $('.h2h-tabs-team2').find('tr[number=' + i + ']').removeClass('hide');
                }
            }
//            console.log($('.h2h-tabs-team1').find('tr[number='+i+']').length);
        }
        setTimeout(function () {
//            console.log("Total: "+$('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").length);
            resultWin = $('.h2h-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-win').length;
            resultLose = $('.h2h-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-lose').length;
            resultDraw = $('.h2h-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-draw').length;
            oddWin = $('.h2h-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-win').length;
            oddLose = $('.h2h-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-lose').length;
            oddDraw = $('.h2h-tabs-team2').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-draw').length;

//            console.log(resultWin + "::" + resultLose + "::" + resultDraw + "::" + oddWin + "::" + oddLose + "::" + oddDraw);

            resultPWin = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultWin) / (resultWin + resultLose + resultDraw)) * 100));
            resultPLose = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultLose) / (resultWin + resultLose + resultDraw)) * 100));
            resultPDraw = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultDraw) / (resultWin + resultLose + resultDraw)) * 100));

            if (oddWin !== 0 || oddLose !== 0 || oddDraw !== 0) {
                oddPWin = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddWin) / (oddWin + oddLose + oddDraw)) * 100));
                oddPLose = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddLose) / (oddWin + oddLose + oddDraw)) * 100));
                oddPDraw = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddDraw) / (oddWin + oddLose + oddDraw)) * 100));
            }

            $('.h2h-tabs-team2').find('#resultPWin').html(resultWin);
            $('.h2h-tabs-team2').find('#resultPLose').html(resultLose);
            $('.h2h-tabs-team2').find('#resultPDraw').html(resultDraw);

            $('.h2h-tabs-team2').find('#oddPWin').html(oddWin);
            $('.h2h-tabs-team2').find('#oddPLose').html(oddLose);
            $('.h2h-tabs-team2').find('#oddPDraw').html(oddDraw);

            google.charts.setOnLoadCallback(
                function () {
                    drawDonut('h2hdonut-a-result', [['result', 'value'], ['draw', resultPDraw], ['lose', resultPLose], ['win', resultPWin]]);
                    drawDonut('h2hdonut-a-oresult', [['result', 'value'], ['draw', oddPDraw], ['lose', oddPLose], ['win', oddPWin]]);
                }
            )

            $('.h2h-tabs-team2').find('#h2h-a-max-match').html(total);
        }, 500);
    }
    function viewHeadToHeadHome(total=null) {
        if (total == null) {
            total = parseInt($('#h2h-h-max-match').html());
        }
//        console.log(total);
        var resultWin = 0, resultLose = 0, resultDraw = 0, resultPWin = 0, resultPLose = 0, resultPDraw = 0, oddWin = 0,
            oddLose = 0, oddDraw = 0, oddPWin = 0, oddPLose = 0, oddPDraw = 0;
        $('.h2h-tabs-team1').find('tr.tr-h2h').addClass('hide');

        for (i = 1; i <= total; i++) {

            if ($('input.checkbox_team1_all').is(':checked')) {
                $('.h2h-tabs-team1').find('tr[number=' + i + ']').removeClass('hide');
            } else {
//                console.log($('.h2h-tabs-team1').find('tr[number=' + i + ']').attr('league_id'));
                if ($('input.checkbox_team1_' + $('.h2h-tabs-team1').find('tr[number=' + i + ']').attr('league_id')).is(':checked')) {
                    $('.h2h-tabs-team1').find('tr[number=' + i + ']').removeClass('hide');
                }
            }
//            console.log($('.h2h-tabs-team1').find('tr[number='+i+']').length);
        }
        setTimeout(function () {
//            console.log("Total: "+$('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").length);
            resultWin = $('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-win').length;
            resultLose = $('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-lose').length;
            resultDraw = $('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-result').find('img.img-draw').length;
            oddWin = $('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-win').length;
            oddLose = $('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-lose').length;
            oddDraw = $('.h2h-tabs-team1').find('div.tab-content').find('div.active').find('tr').not(".hide").find('td.tr-odd').find('img.img-draw').length;

            console.log(resultWin + "::" + resultLose + "::" + resultDraw + "::" + oddWin + "::" + oddLose + "::" + oddDraw);

            resultPWin = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultWin) / (resultWin + resultLose + resultDraw)) * 100));
            resultPLose = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultLose) / (resultWin + resultLose + resultDraw)) * 100));
            resultPDraw = parseInt(100 - ((((resultWin + resultLose + resultDraw) - resultDraw) / (resultWin + resultLose + resultDraw)) * 100));

            if (oddWin !== 0 || oddLose !== 0 || oddDraw !== 0) {
                oddPWin = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddWin) / (oddWin + oddLose + oddDraw)) * 100));
                oddPLose = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddLose) / (oddWin + oddLose + oddDraw)) * 100));
                oddPDraw = parseInt(100 - ((((oddWin + oddLose + oddDraw) - oddDraw) / (oddWin + oddLose + oddDraw)) * 100));
            }

            $('.h2h-tabs-team1').find('#resultPWin').html(resultWin);
            $('.h2h-tabs-team1').find('#resultPLose').html(resultLose);
            $('.h2h-tabs-team1').find('#resultPDraw').html(resultDraw);

            $('.h2h-tabs-team1').find('#oddPWin').html(oddWin);
            $('.h2h-tabs-team1').find('#oddPLose').html(oddLose);
            $('.h2h-tabs-team1').find('#oddPDraw').html(oddDraw);

            google.charts.setOnLoadCallback(
                function () {
                    drawDonut('h2hdonut-h-result', [['result', 'value'], ['draw', resultPDraw], ['lose', resultPLose], ['win', resultPWin]]);
                    drawDonut('h2hdonut-h-oresult', [['result', 'value'], ['draw', oddPDraw], ['lose', oddPLose], ['win', oddPWin]]);
                }
            )

            $('.h2h-tabs-team1').find('#h2h-h-max-match').html(total);
        }, 500);
    }
    function drawDonut(element, data) {
        var data = google.visualization.arrayToDataTable(data);
        var options = {
            backgroundColor: 'transparent',
            pieStartAngle: 0,
            chartArea: {top: 7, height: '90%', width: '100%'},
            height: '180',
            width: '203',
            colors: ['#888', '#e40520', '#0091c8'],
            pieHole: 0.5,
            legend: 'none',
        };

        var chart = new google.visualization.PieChart(document.getElementById(element));
        chart.draw(data, options);
    }
</script>
<script src="/js/play/game.js"></script>
<?php //include 'footer.php';
include '/view/index/footer.php';
?>
</body>
</html>
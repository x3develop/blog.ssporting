<?php
require_once '../model/SitemapInfo.php';
$sitemapInfoObject = new SitemapInfo();
$set=(isset($_GET['set'])?$_GET['set']:0);
$objResult2=$sitemapInfoObject->getByOffset(($set*20000));
$xmlstr="<urlset></urlset>";
$sxe = new SimpleXMLElement($xmlstr);
$sxe->addAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
$sxe->addAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
$sxe->addAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
foreach($objResult2 as $key=>$value){
    $movie = $sxe->addChild('url');
    $movie->addChild('loc', 'https://www.ngoal.com/search-result-play/?info=' . $value->url);
    $movie->addChild('lastmod', date('c', strtotime($value->created_at)));
    $movie->addChild('priority', 0.64);
}
$sxe->asXML("sitemap_info/infomap".$set.".xml");
header('Location: /manager/reportSiteMap_info.php');
exit();
?>
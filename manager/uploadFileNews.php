<?php
header('Access-Control-Allow-Origin: *');
?>
<?php include '_config.php';?>
<?php include '_database.php';?>
<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 2/15/2017
 * Time: 15:27
 */
//    $id = ($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
    $now = new DateTime();
    $dateNowText=date("YmdHis");
    if (!empty($_FILES)) {
        $webroot=$_SERVER['DOCUMENT_ROOT'];
        $targetPath = $webroot . '/images/news/'.date('Y-m-d').'/';
        for($i=0;$i<count($_FILES['Filedata']['tmp_name']);$i++){

            $tempFile = $_FILES['Filedata']['tmp_name'][$i];
            $nameFile = $_FILES['Filedata']['name'][$i];
            $typeFile= $_FILES['Filedata']['type'][$i];

            if (!is_dir($webroot . '/images/' . 'news')) {
                mkdir($webroot . '/images/' . 'news', 0777);
                chmod($webroot . '/images/' . 'news', 0777);
            }

            if (!is_dir($webroot . '/images/' . 'news/'.date('Y-m-d'))) {
                mkdir($webroot . '/images/' . 'news/'.date('Y-m-d'), 0777);
                chmod($webroot . '/images/' . 'news/'.date('Y-m-d'), 0777);
            }

//            Image Resize
            list($width, $height, $type, $attr) = getimagesize( $tempFile );
            $fn = $tempFile;
            $size = getimagesize( $fn );
            $ratio = $width/$height; // width/height
            $src = imagecreatefromstring(file_get_contents($fn));
            $dst = imagecreatetruecolor( $width/2, $height/2 );
            imagecopyresampled($dst, $src, 0, 0, 0, 0, ($width/2), ($height/2), $size[0], $size[1] );
            imagedestroy( $src );
            imagejpeg($dst, str_replace('//', '/', $targetPath) . $dateNowText.'_'.$size[0].'x'.$size[1]."-index-news".strrchr($nameFile, '.')); // adjust format as needed
            imagedestroy( $dst );

            $size=GetimageSize($tempFile);
            $file = str_replace('//', '/', $targetPath) .$dateNowText.'_'.$size[0].'x'.$size[1].strrchr($nameFile, '.');
            move_uploaded_file($tempFile, $file);
            $fileName = str_replace($webroot, '', $file);
            chmod($file, 0777);

        }
        echo json_encode($fileName);
    }
?>
<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 5/23/2018
 * Time: 15:47
 */
include '_database.php';
$webroot = $_SERVER['DOCUMENT_ROOT'];
$html="";
$channel=$_GET['channel'];
$sql = "INSERT INTO `channel_match` (`match_id`,`channel_id`,`link`,`created_at`,`updated_at`)";
$sql .= " VALUES (?,?,?,'" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
$status = $dbh->prepare($sql)->execute(array($channel['match_id'],$channel['channel_id'],$channel['link']));
if($status){
    $sth = $dbh->prepare("select channel_match.id,channel_match.link,channel_match.match_id,channel.channel_name,channel.channel_path from channel_match
inner join channel on channel.id=channel_match.channel_id
 WHERE match_id=".$channel['match_id']." order by channel_match.id DESC");
    $sth->execute();
    $channelMatch = $sth->fetchAll();
    $html="";
    if(!empty($channelMatch)){
        foreach ($channelMatch as $key=>$value){
            $html.="<tr><th scope='row'>".($key+1)."</th><td><img style='height: 50px;' src=".((!empty($value['channel_path']) && file_exists($webroot . $value['channel_path'])) ? $value['channel_path'] : '/images/image-not-found.png')."></td><td>".$value['channel_name']."</td><td>".$value['link']."</td><td><button class='btn btn-danger' onclick='removeChannelMatch(".$value['match_id'].",".$value['id'].")'>ลบ</button></td></tr>";
        }
    }
}
$result['status']=$status;
$result['data']=$html;
echo json_encode($result);
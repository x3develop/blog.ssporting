<?php
header('Access-Control-Allow-Origin: *');
?>
<?php include '_config.php';?>
<?php include '_database.php';?>
<?php
if (!isset($_SESSION)) {
    session_start();
}
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 2/15/2017
 * Time: 15:27
 */
    $id = ($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
    $now = new DateTime();
    if (!empty($_FILES)) {
        $webroot=$_SERVER['DOCUMENT_ROOT'];
        $targetPath = $webroot . '/images/users/'.$id.'/';
//        for($i=0;$i<count($_FILES['Filedata']['tmp_name']);$i++){

            $tempFile = $_FILES['Filedata']['tmp_name'];
            $nameFile = $_FILES['Filedata']['name'];
            $typeFile= $_FILES['Filedata']['type'];

            if (!is_dir($webroot . '/images/' . 'users')) {
                mkdir($webroot . '/images/' . 'users', 0777);
                chmod($webroot . '/images/' . 'users', 0777);
            }

            if (!is_dir($webroot . '/images/' . 'users/'.$id)) {
                mkdir($webroot . '/images/' . 'users/'.$id, 0777);
                chmod($webroot . '/images/' . 'users/'.$id, 0777);
            }

            $size=GetimageSize($tempFile);
            $file = str_replace('//', '/', $targetPath) . $id.'_'.$size[0].'x'.$size[1].strrchr($nameFile, '.');
            move_uploaded_file($tempFile, $file);
            $fileName = str_replace($webroot, '', $file);
            chmod($file, 0777);

//        }
        $sql = "UPDATE user set path='".$fileName."' WHERE id=".$id;
        $sth = $dbh->prepare($sql)->execute();
        $_SESSION['login']['path']=$fileName;
        echo json_encode($fileName);
    }
?>
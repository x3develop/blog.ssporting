<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 5/23/2018
 * Time: 12:31
 */
?>
<?php include '_database.php'; ?>
<?php
if (!isset($_SESSION)) {
    session_start();
}
if (isset($_GET['channel'])) {
    if (!empty($_GET['channel']['id'])) {
        $sql = "UPDATE channel set `channel_name`='" . $_GET['channel']['channel_name'] . "' ,`updated_at`='" . date("Y-m-d H:i:s") . "' WHERE id=" . $_GET['channel']['id'];
    } else {
        $sql = "INSERT INTO channel (`channel_name`,`created_at`,`updated_at`) VALUES ('" . $_GET['channel']['channel_name'] . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
    }
    $sth = $dbh->prepare($sql)->execute();
    header("Location: /manager/channel");
    exit;
}

if (isset($_GET['id'])) {
    $sth = $dbh->prepare("select * from channel where id=" . $_GET['id']);
    $sth->execute();
    $channel = $sth->fetch();
} elseif (isset($_GET['delete_id'])) {
    if(isset($_SESSION['login']) && !empty($_SESSION['login']['username']) && $_SESSION['login']['username']=="admin") {
        $sth = $dbh->prepare("delete from channel where id=" . $_GET['delete_id']);
        $sth->execute();
    }
    header("Location: /manager/channel");
    exit;
}

$sth = $dbh->prepare("select * from `channel`");
$sth->execute();
$channelList = $sth->fetchAll();
?>
<?php include 'layouts/header.php'; ?>
<?php
$webroot = $_SERVER['DOCUMENT_ROOT'];
?>
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col-md-6">
                <h3>Manage Channel</h3>
            </div>
            <div class="col-md-6">
                <form class="form-inline pull-right" style="margin-top: 20px;">
                    <label class="sr-only" for="inlineFormInput">Channel Name</label>
                    <div class="input-group">
                        <input class="form-control" name="search[channel_name]" placeholder="Channel Name">
                        <span class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="well well-sm">
                <form class="form-inline" method="get">
                    <input type="hidden" name="channel[id]" value="<?= ((!empty($channel['id'])) ? $channel['id'] : '') ?>">
                    <input style="width: 300px;" required name="channel[channel_name]"
                           value="<?= ((!empty($channel['channel_name'])) ? $channel['channel_name'] : '') ?>" type="text"
                           class="form-control" placeholder="Channel Name">
                    <button type="submit" class="btn btn-primary <?= ((!empty($channel['id'])) ? 'btn-outline-warning' : 'btn-outline-primary') ?>">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <?= ((!empty($channel['id'])) ? 'Edit' : 'Add') ?>

                    </button>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Path</th>
                    <th>Name</th>
                    <th>Manage</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($channelList as $key => $value) { ?>
                    <tr>
                        <td><?= ($key + 1) ?></td>
                        <td><img style="height: 100px;" id="channel-<?php echo $value['id']; ?>"
                                 src="<?php echo((!empty($value['channel_path']) && file_exists($webroot . $value['channel_path'])) ? $value['channel_path'] : '/images/image-not-found.png') ?>">
                        </td>
                        <td><?= $value['channel_name'] ?></td>
                        <td>
                            <div class="btn-group-manage" style="float: left;width: 100%;">
                                <a href="/manager/channel?id=<?php echo $value['id']; ?>" class="btn btn2 btn-default">Edit</a>
                                <a onclick="return confirm('ยืนยันการลบ')" href="/manager/channel?delete_id=<?php echo $value['id']; ?>" class="btn btn2 btn-default">Delete</a>
                                <label id="labelUpload" data-id="<?php echo $value['id']; ?>" for="inputUpload" class="btn btn3 btn-default">UpLoad</label>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
<form style="display: none;" id="formUpload" action="/manager/uploadFileLeague" method="post"
      enctype="multipart/form-data">
    <input type="text" name="id">
    <input id="inputUpload" type="file" name="Filedata[]" multiple="multiple" accept="image/*"><br>
</form>
</div>

<script>
    $("input#dateSelect").datepicker({dateFormat: 'yy-mm-dd'});
    $(document).on('click', "#labelUpload", function (e) {
        $('input[name=id]').val($(this).attr('data-id'));
    });

    $(document).on('change', "#inputUpload", function (e) {
        $("#formUpload").submit();
    });

    $(document).on('submit', "#formUpload", function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: "/manager/uploadFileChannel",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                response = jQuery.parseJSON(response);
                $('img#channel-' + $('#formUpload').find('input[name=id]').val()).attr('src', response + '#' + Math.floor((Math.random() * 1000) + 1));
            },
            error: function (data) {
                console.log("error");
                console.log(data);
            }
        });
    });
</script>

<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 1/17/2019
 * Time: 3:22 PM
 */
$arr = array();
$now = strtotime(date('Y-m-d', strtotime(' - 8 day')));
$last = strtotime(date('Y-m-d', strtotime(' + 1 day')));
$webroot=$_SERVER['DOCUMENT_ROOT'];
?>
<?php include 'layouts/header.php'; ?>
<div class="container">
    <div class="page-header title">
        <h3><a href="sitemap">SiteMap</a></h3>
        <h3><a href="reportSiteMap_info">SiteMapInfo</a></h3>
        <h3><a href="reportSiteMap_search">SiteMapSearch</a></h3>
        <h3 style="float: right;">SiteMap</h3>
    </div>
    <div class="row">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>link</th>
            </tr>
            </thead>
            <tbody>
            <?php while ($last >= $now) { ?>
                <tr class="<?php echo ((date('Y-m-d')==date('Y-m-d', $last))?'active':'');?>">
                    <th scope="row"><?php echo date('Y-m-d', $last)?></th>
                    <td><?php echo (file_exists($webroot."/sitemap/sitemap_".date('Ymd', $last).".xml")?'<a target="_blank" href="/sitemap/sitemap_'.date('Ymd', $last).'.xml">View</a>':'-')?></td>
                </tr>
                <?php $last = strtotime('-1 day', $last); } ?>
            </tbody>
        </table>
    </div>
</div>




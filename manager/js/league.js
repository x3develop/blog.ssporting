$(document).ready(function () {
  $(".update-priority").on("click", function (e) {
    var lid = $(e.currentTarget).attr("lid");
    var priority = $("#lid-" + lid).val();
    console.log(lid, priority);
    $.ajax({
      url: "/services/manager/league/updatepriority",
      data: {
        lid: lid,
        priority: priority
      },
      method: "GET",
      dataType: "JSON"
    }).done(function (response) {
        alert("Priority set to "+response.priority);
    });
  });
});
$(document).ready(function () {
    $(".mark-highlight").on("change", function (e) {
        var mid = $(e.currentTarget).attr("mid");
        var action = 'Y';
        if ($(e.currentTarget).is(":checked")) {

        } else {
            action = 'N';
        }

        // alert(mid+action);
        $.ajax({
            method: "POST",
            url: "/services/highlight/updatehighlight",
            data: { mid: mid, action: action },
            dataType: "JSON"
        }).done(function (response) {
            console.log(response);
            if(response.active=="Y"){
                $(".match-status[mid="+response.mid+"]").text("bet");
            }else{
                $(".match-status[mid="+response.mid+"]").text("created");
            }
        });

    });
    // reloadHMark();
    function reloadHMark() {
        var day = moment();
        $.ajax({
            method: "POST",
            url: "/services/highlight/gethighlightall",
            data: { day: day.format("YYYY-MM-DD") },
            dataType: "JSON"
        }).done(function (response) {
            // console.log(response);
            $.each(response, function (k, v) {
                // console.log(v.mid,v.active);
                if (v.active == "Y") {
                    recheck(v.mid, true);
                } else {
                    recheck(v.mid, false);
                }
            });
        });
    }
    findmark();
    function findmark() {
        var midlist = "0";
        // $.each($(".mark-highlight"), function (k, v) {

        // });

        $(".mark-highlight").each(function (k, v) {
            midlist += "," + $(v).attr("mid");
        });

        $(".mark-highlight").promise().done(function () {
            $.ajax({
                method: "POST",
                url: "/services/highlight/gethighlightlist",
                data: { list: midlist },
                dataType: "JSON"
            }).done(function (response) {
                // console.log(response);
                $.each(response, function (k, v) {
                    // console.log(v.mid,v.active);
                    if (v.active == "Y") {
                        recheck(v.mid, true);
                    } else {
                        recheck(v.mid, false);
                    }
                });
            });
        });
    }
    function recheck(mid, checked) {
        console.log(mid, checked);
        $(".mark-highlight[mid=" + mid + "]").prop("checked", checked);
    }
});
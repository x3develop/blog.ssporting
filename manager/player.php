<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/2/2017
 * Time: 22:46
 */
if (!isset($_SESSION)) {
    session_start();
}
include '_database.php';
if (isset($_GET['player'])) {
    if (!empty($_GET['player']['id'])) {
        $sql = "UPDATE `player` set `name`='" . $_GET['player']['name'] . "',`updated_at`='" . date("Y-m-d H:i:s") . "' WHERE id=" . $_GET['player']['id'];
    } else {
        $sql = "INSERT INTO `player` (`name`,`created_at`,`updated_at`) VALUES ('" . $_GET['player']['name'] . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
    }
    $sth = $dbh->prepare($sql)->execute();
    header("Location: /manager/player");
    exit;
}

if (isset($_GET['id'])) {
    $sth = $dbh->prepare("select * from player where id=" . $_GET['id']);
    $sth->execute();
    $player = $sth->fetch();
} elseif (isset($_GET['delete_id'])) {
    if (isset($_SESSION['login']) && !empty($_SESSION['login']['username']) && $_SESSION['login']['username'] == "admin") {
        $sth = $dbh->prepare("delete from player where id=" . $_GET['delete_id']);
        $sth->execute();
    }
    header("Location: /manager/player");
    exit;
}
?>
<?php include 'layouts/header.php';?>
<?php
if (isset($_GET['search'])) {
    $sth = $dbh->prepare("select p.*,t.name_en as teamname from player p LEFT JOIN team t ON p.tid=t.id7m WHERE `name` like '%" . $_GET['search']['name'] . "%' limit 30");
    $sth->execute();
    $userPlayerAll = $sth->fetchAll();
}else if(isset($_GET['tid'])){
    $sth = $dbh->prepare("select p.*,t.name_en as teamname from player p LEFT JOIN team t ON p.tid=t.id7m where p.tid=?");
    $sth->execute([$_GET['tid']]);
    $userPlayerAll = $sth->fetchAll();
}


else {
    $sth = $dbh->prepare("select p.*,t.name_en as teamname from player p LEFT JOIN team t ON p.tid=t.id7m limit 30");
    $sth->execute();
    $userPlayerAll = $sth->fetchAll();
}

$webroot = $_SERVER['DOCUMENT_ROOT'];
?>
<div class="container-fluid">
    <div class="page-header title">
        <h3>Player</h3>
    </div>

    <div class="row">
        <div class="col-md-6">
            <form class="form-inline">
                <div class="form-group">
                    <label>Player Name</label>
                    <input name="search[name]" type="text" class="form-control mb-2 mr-sm-2 mb-sm-0"
                           placeholder="Search Name">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i> Search
                    </button>
                </div>
            </form>
        </div>
        <div class="col-md-6">
            <form class="form-inline">
                <div class="form-group">
                    <input type="hidden" name="player[id]" value="<?=((!empty($player['id'])) ? $player['id'] : '')?>">
                    <label>League Name</label>
                    <input required name="player[name]" value="<?=((!empty($player['name'])) ? $player['name'] : '')?>"
                           type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" placeholder="Play Name">
                    <button type="submit"
                            class="btn <?=((!empty($player['id'])) ? 'btn-default' : 'btn-primary')?>"><?=((!empty($player['id'])) ? 'Edit' : 'Add')?></button>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
   
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Name 7M</th>
                    <th>Team</th>
                    <th class="text-center">Manage</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($userPlayerAll as $key => $value) {?>
                    <tr>
                        <td><?php echo $value['id']; ?></td>
                        <td>
                            <img style="height: 50px;" id="player-<?php echo $value['id']; ?>"
                                 src="<?php echo ((!empty($value['path']) && file_exists($webroot . $value['path'])) ? $value['path'] : '/images/avatar.png') ?>">
                        </td>
                        <td><input id="pid-<?php echo $value['id']; ?>" type="text" value="<?=$value['name']?>"><input class="update-pname" pid="<?php echo $value['id']; ?>" type="button" value="Update"></td>
                        <td><?=$value['name_7m']?></td>
                        <td><?=$value['teamname']?></td>
                        <td>
                            <div class="btn-group-manage" style="width: 180px;">
                            <a href="/manager/player?id=<?php echo $value['id']; ?>"
                               class="btn btn1 btn-default">Edit</a>
                            <a onclick="return confirm('ยืนยันการลบ')"
                               href="/manager/player?delete_id=<?php echo $value['id']; ?>"
                               class="btn btn2 btn-default">Delete</a>
                            <label id="labelUpload" data-id="<?php echo $value['id']; ?>" for="inputUpload"
                                   class="btn btn3 btn-default">UpLoad</label>
                            </div>
                        </td>
                    </tr>
                <?php }?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<form style="display: none;" id="formUpload" method="post" enctype="multipart/form-data">
    <input type="text" name="id">
    <input id="inputUpload" type="file" name="Filedata[]" multiple="multiple" accept="image/*"><br>
</form>

<script>
    $(document).on('click', "#labelUpload", function (e) {
        $('input[name=id]').val($(this).attr('data-id'));
    });

    $(document).on('change', "#inputUpload", function (e) {
        $("#formUpload").submit();
    });

    $(document).on('submit', "#formUpload", function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: "/manager/uploadFilePlayer",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                response = jQuery.parseJSON(response);
                $('img#player-' + $('#formUpload').find('input[name=id]').val()).attr('src', response + '#' + Math.floor((Math.random() * 1000) + 1));
            },
            error: function (data) {
                console.log("error");
                console.log(data);
            }
        });
    });
</script>

<?php include 'layouts/footer.php';?>

<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/3/2017
 * Time: 1:20
 */
if (!isset($_SESSION)) {
    session_start();
}
include '_database.php';
$webroot = $_SERVER['DOCUMENT_ROOT'];
$html="";
if(isset($_GET['id'])) {
    if(isset($_SESSION['login']) && !empty($_SESSION['login']['username']) && $_SESSION['login']['username']=="admin") {
        $sth = $dbh->prepare("delete from channel_match  WHERE id=" . $_GET['id']);
        $status = $sth->execute();
        if ($status) {
            $sth = $dbh->prepare("select channel_match.id,channel_match.link,channel_match.match_id,channel.channel_name,channel.channel_path from channel_match
inner join channel on channel.id=channel_match.channel_id
 WHERE match_id=" . $_GET['match_id'] . " order by channel_match.id DESC");
            $sth->execute();
            $channelMatch = $sth->fetchAll();
            $html = "";
            if (!empty($channelMatch)) {
                foreach ($channelMatch as $key => $value) {
                    $html .= "<tr><th scope='row'>" . ($key + 1) . "</th><td><img style='height: 50px;' src=" . ((!empty($value['channel_path']) && file_exists($webroot . $value['channel_path'])) ? $value['channel_path'] : '/images/image-not-found.png') . "></td><td>" . $value['channel_name'] . "</td><td>" . $value['link'] . "</td><td><button class='btn btn-danger' onclick='removeChannelMatch(" . $value['match_id'] . "," . $value['id'] . ")'>ลบ</button></td></tr>";
                }
            }
        }
    }
}
$result['status']=$status;
$result['data']=$html;
echo json_encode($result);
?>
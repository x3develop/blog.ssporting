<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 3/9/2018
 * Time: 15:34
 */
include '_database.php';

if (isset($_REQUEST['id'])) {
    $sth = $dbh->prepare("select * from media_store where media_store.remove='N' and media_store.gall_id=?");
    $sth->execute([$_REQUEST['id']]);
    $dataMediaGallery = $sth->fetchAll();
} else if (isset($_REQUEST['media_store'])) {
    $sql = "UPDATE media_store set remove='Y' WHERE id=" . $_REQUEST['media_store']['id'];
    $sth = $dbh->prepare($sql)->execute();

//    $sth = $dbh->prepare("select * from media_store where media_store.remove='N' and media_store.gall_id=?");
//    $sth->execute([$_REQUEST['media_store']['gall_id']]);
//    $dataMediaGallery = $sth->fetchAll();
    header("Location: /manager/sexy-upload?id=" . $_REQUEST['media_store']['gall_id']);
    exit;
}


?>
<?php include 'layouts/header.php'; ?>
<div class="container-fluid">
    <a href="/manager/sexy" class="btn btn-default">
        <span class="glyphicon glyphicon-chevron-left"
              aria-hidden="true"></span> Back</a>
    <div class="page-header">
        <table style="width: 100%;">
            <tr>
                <td><h3><span class="glyphicon glyphicon-picture" aria-hidden="true"></span> Manage Media Gallery File
                    </h3></td>
                <td class="text-right">
                    <label id="labelUpload" for="inputUpload" class="btn btn-primary"><i class="fa fa-upload"
                                                                                         aria-hidden="true"></i>
                        อัพรูปภาพ</label>
                </td>
            </tr>
        </table>

    </div>


    <div class="row">
        <div class="col-md-12" id="list-image">
            <?php foreach ($dataMediaGallery as $key => $value) { ?>
                <form style='float: left;width: 20%;padding: 3px;position: relative;'>
                    <input type="hidden" name="media_store[id]" value="<?= $value['id'] ?>">
                    <input type="hidden" name="media_store[gall_id]" value="<?= $value['gall_id'] ?>">
                    <img style="width: 100%;" src="<?= $value['thumbnail'] ?>">
                    <button style="position: absolute;top:0px;right:0px;" type="submit" class="btn btn-sm btn-danger">
                        X
                    </button>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
<form style="display: none;" id="formUpload" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?= ((isset($_REQUEST['id'])) ? $_REQUEST['id'] : '') ?>">
    <input id="inputUpload" type="file" name="Filedata[]" multiple="multiple" accept="image/*"><br>
</form>


<script>
    $(document).on('change', "#inputUpload", function (e) {
        $("#formUpload").submit();
    });
    $(document).on('submit', "#formUpload", function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: "/manager/uploadFileSexy",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                response = jQuery.parseJSON(response);
                console.log(response);
                $.each(response, function (key, value) {
                    $('div#list-image').append("<img style='width: 20%;padding: 5px;border: solid 1px;margin: 5px;' class='card-img-top' src='" + value + "#" + Math.floor((Math.random() * 1000) + 1) + "'>");
                });

//                $('input[name="news[imageLink]"]').val(response);
//                $('img#player').attr('src',response+'#'+Math.floor((Math.random() * 1000) + 1));
            },
            error: function (data) {
                console.log("error");
                console.log(data);
            }
        });
    });
</script>


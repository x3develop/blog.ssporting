<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/3/2017
 * Time: 1:20
 */
include '_database.php';
$html="";
if(isset($_GET['id'])) {
    $sth = $dbh->prepare("select comment_match.*,user.path from comment_match LEFT JOIN `user` on `user`.id=comment_match.user_id WHERE team='home' and match_id=".$_GET['id']);
    $sth->execute();
    $commentMatchHome = $sth->fetchAll();

    $sth = $dbh->prepare("select comment_match.*,user.path from comment_match LEFT JOIN `user` on `user`.id=comment_match.user_id WHERE team='away' and match_id=".$_GET['id']);
    $sth->execute();
    $commentMatchAway = $sth->fetchAll();

    if(!empty($commentMatchHome) and count($commentMatchHome)>=count($commentMatchAway)){
        foreach ($commentMatchHome as $key=>$value){
            $html.="<tr><td class='comment_match' data-id='".$value['id']."'><div style='width: 30%;float: left;'><img style='width: 100%' src=".((!empty($value['path']))?$value['path']:'/images/avatar.png')."></div><div style='width: 70%;float: left;padding-left: 5px;'>".$value['comment']."</div></td>";
//            $html.="<td>";
            if(!empty($commentMatchAway[$key]['comment'])) {
                $html .= "<td class='comment_match' data-id='".$commentMatchAway[$key]['id']."'><div style='width: 30%;float: left;'><img style='width: 100%' src=" . ((!empty($commentMatchAway[$key]['path'])) ? $commentMatchAway[$key]['path'] : '/images/avatar.png') . "></div>";
            }else{
                $html.="<td class='comment_match' data-id='".$commentMatchAway[$key]['id']."'>";
            }
            $html .= "<div style='width: 70%;float: left;padding-left: 5px;'>".((!empty($commentMatchAway[$key]['comment']))?$commentMatchAway[$key]['comment']:'')."</div></td></tr>";
        }
    }elseif (!empty($commentMatchAway) and count($commentMatchAway)>count($commentMatchHome)){
        foreach ($commentMatchAway as $key=>$value){
            $html.="<tr>";
            if(!empty($commentMatchHome[$key]['comment'])) {
                $html .= "<td class='comment_match' data-id='".$commentMatchHome[$key]['id']."'><div style='width: 30%;float: left;'><img style='width: 100%' src=" . ((!empty($commentMatchHome[$key]['path'])) ? $commentMatchHome[$key]['path'] : '/images/avatar.png') . "></div>";
            }else{
                $html.="<td>";
            }
            $html .= "<div style='width: 70%;float: left;padding-left: 5px;'>".((!empty($commentMatchHome[$key]['comment']))?$commentMatchHome[$key]['comment']:'')."</div></td>";
            $html.="<td class='comment_match' data-id='".$value['id']."'><div style='width: 30%;float: left;'><img style='width: 100%' src=".((!empty($value['path']))?$value['path']:'/images/avatar.png')."></div><div style='width: 70%;float: left;padding-left: 5px;'>".$value['comment']."</div></td>";
            $html.="</tr>";
        }
    }
}
$result['html']=$html;
echo json_encode($result);
?>
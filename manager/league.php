<?php include '_database.php'; ?>
<?php
if (!isset($_SESSION)) {
    session_start();
}
if (isset($_GET['league'])) {
    if (!empty($_GET['league']['id'])) {
        if (!empty($_GET['league']['date_open_season'])) {
            $sql = "UPDATE league set `group`='" . $_GET['league']['group'] . "',`country_id`='" . $_GET['league']['country'] . "',`date_open_season`='" . $_GET['league']['date_open_season'] . "',`bot`='" . $_GET['league']['bot'] . "', name='" . $_GET['league']['name'] . "',`updated_at`='" . date("Y-m-d H:i:s") . "' WHERE id=" . $_GET['league']['id'];
        }else{
            $sql = "UPDATE league set `group`='" . $_GET['league']['group'] . "',`country_id`='" . $_GET['league']['country'] . "',`bot`='" . $_GET['league']['bot'] . "', name='" . $_GET['league']['name'] . "',`updated_at`='" . date("Y-m-d H:i:s") . "' WHERE id=" . $_GET['league']['id'];
        }
    } else {
        if (!empty($_GET['league']['date_open_season'])) {
            $sql = "INSERT INTO league (`bot`,`group`,`country_id`,`name`,`date_open_season`,`created_at`,`updated_at`) VALUES ('" . $_GET['league']['bot'] . "','" . $_GET['league']['group'] . "'," . $_GET['league']['country'] . ",'" . $_GET['league']['name'] . "','" . $_GET['league']['date_open_season'] . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
        } else {
            $sql = "INSERT INTO league (`bot`,`group`,`country_id`,`name`,`created_at`,`updated_at`) VALUES ('" . $_GET['league']['bot'] . "','" . $_GET['league']['group'] . "'," . $_GET['league']['country'] . ",'" . $_GET['league']['name'] . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
        }
    }
    $sth = $dbh->prepare($sql)->execute();
    header("Location: /manager/league" . ((isset($_GET['league']['country'])) ? '?search[country]=' . $_GET['league']['country'] : ''));
    exit;
}
if (isset($_GET['search']) && $_GET['search']['country'] !== "") {
    $sth = $dbh->prepare("select * from league where `country_id` = '" . $_GET['search']['country'] . "' order by league.bot ASC LIMIT 50");
    $sth->execute();
    $leagueAll = $sth->fetchAll();
} else {
    $sth = $dbh->prepare("select * from league order by league.bot ASC LIMIT 50");
    $sth->execute();
    $leagueAll = $sth->fetchAll();
}

if (isset($_GET['id'])) {
    $sth = $dbh->prepare("select * from league where id=" . $_GET['id']);
    $sth->execute();
    $league = $sth->fetch();
} elseif (isset($_GET['delete_id'])) {
    if(isset($_SESSION['login']) && !empty($_SESSION['login']['username']) && $_SESSION['login']['username']=="admin") {
        $sth = $dbh->prepare("delete from league where id=" . $_GET['delete_id']);
        $sth->execute();
    }
    header("Location: /manager/league");
    exit;
}

$sth = $dbh->prepare("select * from `country`");
$sth->execute();
$country = $sth->fetchAll();
?>
<?php include 'layouts/header.php'; ?>
<?php
$webroot = $_SERVER['DOCUMENT_ROOT'];
?>
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6">
                    <h3>Manage League</h3>
                </div>
                <div class="col-md-6">
                    <form class="form-inline pull-right" style="margin-top: 20px;">
                        <label class="sr-only" for="inlineFormInput">League Name</label>
                        <div class="input-group">
                            <select required name="search[country]" class="form-control">
                                <option value="">Select Country</option>
                                <?php foreach ($country as $key => $value) { ?>
                                    <option <?= ((isset($_GET['search']['country']) and $_GET['search']['country'] == $value['id']) ? 'selected' : '') ?>
                                            value="<?php echo $value["id"] ?>"><?php echo $value["name"] ?></option>
                                <?php } ?>
                            </select>
                            <span class="input-group-btn">
                    <!--            <input name="search[name]" type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" placeholder="Search League">-->
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"
                                                                     aria-hidden="true"></i></button>
                    </span>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="well well-sm">
                    <form class="form-inline" method="get">
                        <!--            <input type="hidden" name="league[country]" value="-->
                        <?php //echo ((isset($_GET['search']['country']))?$_GET['search']['country']:'')?><!--">-->
                        <input type="hidden" name="league[id]"
                               value="<?= ((!empty($league['id'])) ? $league['id'] : '') ?>">
                        <label class="" for="inlineFormInput" style="padding-right: 5px;">BOT</label>
                        <select required name="league[bot]" class="form-control">
                            <option <?= ((!empty($league['bot']) and $league['bot'] == "N") ? 'selected' : '') ?>
                                    value="N">
                                ปิด
                            </option>
                            <option <?= ((!empty($league['bot']) and $league['bot'] == "Y") ? 'selected' : '') ?>
                                    value="Y">
                                เปิด
                            </option>
                        </select>
                        <label class="" for="inlineFormInput" style="padding-right: 5px;">Group</label>
                        <select required name="league[group]" class="form-control">
                            <option <?= ((!empty($league['group']) and $league['group'] == "N") ? 'selected' : '') ?>
                                    value="N">
                                ปิด
                            </option>
                            <option <?= ((!empty($league['group']) and $league['group'] == "Y") ? 'selected' : '') ?>
                                    value="Y">
                                เปิด
                            </option>
                        </select>
                        <select required name="league[country]" class="form-control">
                            <option value="">Select Country</option>
                            <?php foreach ($country as $key => $value) { ?>
                                <option <?= ((!empty($league['country_id']) and $league['country_id'] == $value['id']) ? 'selected' : '') ?>
                                        value="<?php echo $value["id"] ?>"><?php echo $value["name"] ?></option>
                            <?php } ?>
                        </select>
                        <input style="width: 300px;" required name="league[name]"
                               value="<?= ((!empty($league['name'])) ? $league['name'] : '') ?>" type="text"
                               class="form-control" placeholder="League Name">
                        <input style="width: 110px;" id="dateSelect" name="league[date_open_season]"
                               value="<?= ((!empty($league['date_open_season'])) ? $league['date_open_season'] : '') ?>"
                               type="text" class="form-control" placeholder="Date">
                        <button type="submit"
                                class="btn btn-primary <?= ((!empty($league['id'])) ? 'btn-outline-warning' : 'btn-outline-primary') ?>">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            <?= ((!empty($league['id'])) ? 'Edit' : 'Add') ?>

                        </button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Bot</th>
                        <th>Group</th>
                        <th>Priority</th>
                        <th>Manage</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($leagueAll as $key => $value) { ?>
                        <tr>
                            <td><?= ($key + 1) ?></td>
                            <td><img style="height: 100px;" id="league-<?php echo $value['id']; ?>"
                                     src="<?php echo((!empty($value['path']) && file_exists($webroot . $value['path'])) ? $value['path'] : '/images/image-not-found.png') ?>">
                            </td>
                            <td><?= $value['name'] ?></td>
                            <td><?= $value['bot'] ?></td>
                            <td><?= $value['group'] ?></td>
                            <td><input id="lid-<?php echo $value['id']; ?>" type="number" min=1 max=1000 value="<?= $value['priority'] ?>"><input class="update-priority" lid="<?php echo $value['id']; ?>" type="button" value="Update"></td>
                            <td>
                                <div class="btn-group-manage" style="width: 100%;">
                                <!--                <a href="/addMatch?league_id=-->
                                <?php //echo $value['id']; ?><!--" class="btn btn-outline-info">Add Match</a>-->
                                <button type="button" onclick='manageSnap(<?= json_encode($value); ?>)' class="btn btn1 btn-default"><span class="oi oi-aperture"></span></button>
                                <a href="/manager/team?search[league_id]=<?php echo $value['id']; ?>"
                                   class="btn btn2 btn-default">Team</a>
                                <a href="/manager/league?id=<?php echo $value['id']; ?><?php echo((isset($_GET['search']['country'])) ? '&search[country]=' . $_GET['search']['country'] : '') ?>"
                                   class="btn btn2 btn-default">Edit</a>
                                <a onclick="return confirm('ยืนยันการลบ')"
                                   href="/manager/league?delete_id=<?php echo $value['id']; ?>"
                                   class="btn btn2 btn-default">Delete</a>
                                <label id="labelUpload" data-id="<?php echo $value['id']; ?>" for="inputUpload"
                                       class="btn btn3 btn-default">UpLoad</label>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <form style="display: none;" id="formUpload" action="/manager/uploadFileLeague" method="post"
              enctype="multipart/form-data">
            <input type="text" name="id">
            <input id="inputUpload" type="file" name="Filedata[]" multiple="multiple" accept="image/*"><br>
        </form>
    </div>
    <!--    Snap-->
    <div class="modal fade" style="" id="ModalSnap" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Snap</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <form id="snap-match" class="form-inline">
                                <input type="hidden" name="snap[type]" value="league">
                                <input type="hidden" name="snap[id]" value="">
                                <div class="form-group">
                                    <label for="exampleInputName2">Date</label>
                                    <input style="width: 150px;" required type="date" name="snap[date]" class="form-control" id="exampleInputName2" placeholder="Date">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputName2">Width</label>
                                    <input style="width: 80px;" required type="number" name="snap[width]" class="form-control" id="exampleInputName2" placeholder="Width" value="600">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail2">Height</label>
                                    <input style="width: 80px;" required type="number" name="snap[height]" class="form-control" id="exampleInputEmail2" placeholder="Height" value="600">
                                </div>
                                <div class="form-group">
                                    <button data-loading-text="Loading..." autocomplete="off"  type="submit" class="btn btn-danger btn-lg"><span class="oi oi-camera-slr"></span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6" style="padding: 10%;">
                            <img onerror="this.src='/images/image-not-found.png';" id="image-snap-league-fixture" style="width: 100%;" src="/images/image-not-found.png">
                        </div>
                        <div class="col-sm-6" style="padding: 10%;">
                            <img onerror="this.src='/images/image-not-found.png';" id="image-snap-league-score" style="width: 100%;" src="/images/image-not-found.png">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a style="display: none;" onerror="this.disabled=true;" id="a-snap-league-fixture" href="" download class="btn btn-danger"><span class="oi oi-data-transfer-download"></span> Fixture</a>
                    <a style="display: none;" onerror="this.disabled=true;" id="a-snap-league-score" href="" download class="btn btn-danger"><span class="oi oi-data-transfer-download"></span> Score</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script>

        function manageSnap(data) {
            $('form#snap-match').find('input[name="snap[id]"]').val(data.id);
            $('#ModalSnap').modal('show');
        }

        $(document).on("submit", "form#snap-match", function (e) {
            e.preventDefault();
            var $btn = $(this).find('button').button('loading');
            $.ajax({
                url: '/manager/snapImage',
                type: 'GET',
                dataType: 'json',
                data: $(this).serialize(),
                success: function (data) {
                    if (data['status']) {
                        $('form#snap-match')[0].reset();
                        $('#image-snap-league-fixture').attr('src',data['fixture']+'#'+$.now());
                        $('#image-snap-league-score').attr('src',data['score']+'#'+$.now());
                        $('#a-snap-league-fixture').attr('href',data['fixture']);
                        $('#a-snap-league-score').attr('href',data['score'])
                        $('#a-snap-league-fixture').show();
                        $('#a-snap-league-score').show();
                        $btn.button('reset');
                    }
                }
            });
        });

        $("input#dateSelect").datepicker({dateFormat: 'yy-mm-dd'});
        $(document).on('click', "#labelUpload", function (e) {
//        console.log($(this).attr('data-id'));
            $('input[name=id]').val($(this).attr('data-id'));
        });

        $(document).on('change', "#inputUpload", function (e) {
            $("#formUpload").submit();
        });

        $(document).on('submit', "#formUpload", function (e) {
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                type: 'POST',
                url: "/manager/uploadFileLeague",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    response = jQuery.parseJSON(response);
//                console.log($('#formUpload').find('input[name=id]').val());
//                console.log(response);
                    $('img#league-' + $('#formUpload').find('input[name=id]').val()).attr('src', response + '#' + Math.floor((Math.random() * 1000) + 1));
                },
                error: function (data) {
                    console.log("error");
                    console.log(data);
                }
            });
        });
    </script>
<script src="/manager/js/league.js"></script>
<?php include 'layouts/footer.php'; ?>
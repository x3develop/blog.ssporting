<?php
header('Access-Control-Allow-Origin: *');
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playLeague.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playChannelMatch.php";
$playMatch = new playMatch();
$playLeague = new playLeague();
$playChannelMatch = new playChannelMatch();
$count=0;
$id=((isset($_GET['id']))?$_GET['id']:null);
$data=array();
$data=explode("_",$id);
if(!empty($data[0])) {
    $league=$playLeague->getLeagueById($data[0]);
}
if(!empty($data[0]) and !empty($data[1])) {
    $matchView = $playMatch->getMathByLeagueAndDate($data[0], $data[1]);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Fixture Small</title>
    <link href="css/reset.css" rel="stylesheet">
    <link href="css/fixture-small.css" rel="stylesheet">
</head>
<body>
<div class="content-fixture-small">
    <div class="content-fixture-small-topic">
        <table>
            <tr>
                <td>
                    <img src="<?=$league['path']?>">
                </td>
                <td class="title">
                    <h1>FIXTURE</h1>
                    <span><?=$league['name']?></span>
                </td>
                <td>
                    <h2><?= date("d M Y", strtotime($data[1])) ?></h2>
                    <p>WWW.NGOAL.COM</p>
                </td>
            </tr>
        </table>
    </div>
    <div class="content-fixture-small-list">
        <?php if(!empty($matchView)){ ?>
            <?php foreach ($matchView as $key=>$value){ ?>
                <?php if($value->status!="fullTime" and $count<=4){
                    $ChannelList=$playChannelMatch->getChannel($value->match_7m_id);
                    ?>
                    <table>
                        <tr>
                            <td style="width: 20%"><h2><?=$value->teamHomeEn?></h2></td>
                            <td class="logo"><img src="<?='http://ngoal.com/'.$value->teamHomePath?>"></td>
                            <td class="list-mid">
                                <span><?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?></span>
                                <div style="width: 100%;">
                                <?php if(!empty($ChannelList)){ ?>
                                    <?php foreach ($ChannelList as $Ckey=>$Cvalue){ ?>
                                        <?php if(!empty($Cvalue->channel_path)){ ?>
                                            <img style="float: left;padding: 0px 2px;" src="<?=$Cvalue->channel_path;?>">
                                        <?php }else{ ?>
                                            <div style="float: left;padding: 0px 2px;font-size: 14px;"><?=$Cvalue->channel_name?></div>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                                </div>
                            </td>
                            <td class="logo"><img src="<?='http://ngoal.com/'.$value->teamAwayPath?>"></td>
                            <td style="width: 20%"><h2><?=$value->teamAwayEn?></h2></td>
                        </tr>
                    </table>
                    <?php $count++ ?>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </div>
</div>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <title></title>
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.css">
    <!--    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.css">-->
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/manager/css/manage-style.css">
    <!--    <link rel="stylesheet" href="/manager/bootstrap-4.0.0/css/bootstrap.css">-->
    <link rel="stylesheet" href="/manager/jquery-ui/jquery-ui.css">
    <link rel="stylesheet" href="/manager/jquery-timepicker/jquery.timepicker.css">
    <link href="/manager/open-iconic/font/css/open-iconic-bootstrap.css" rel="stylesheet">
    <!--js-->
    <script src="/manager/jquery/jquery-3.2.1.min.js"></script>
    <script src="/manager/jquery-ui/jquery-ui.js"></script>
    <script src="/css/bootstrap/js/bootstrap.js"></script>
    <script src="/manager/jquery-timepicker/jquery.timepicker.js"></script>
    <script src="/manager/momentjs/moment.js"></script>

    <script>
        $(document).ready(function () {
            $('a[href="' + this.location.pathname + '"]').parent().addClass('active');
        });
    </script>
</head>

<body>
<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
?>
<?php //include '_config.php';?>
<?php include './_database.php'; ?>
<?php
if (!isset($_SESSION)) {
    session_start();
}
$webroot = $_SERVER['DOCUMENT_ROOT'];
if((!isset($_SESSION['login']) || (!empty($_SESSION['login']['username']) && $_SESSION['login']['username']!=="admin")) && !($_SERVER["REQUEST_URI"]=='/' || $_SERVER["REQUEST_URI"]=='/manager/' || $_SERVER["REQUEST_URI"]=='/manager/index.php')){
    header('Location: /manager/');
    exit();
}
?>
<div>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/manager"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    Management</a>
            </div>

            <div class="collapse navbar-collapse">
                <div class="menu-top" id="navbarSupportedContent">
                    <?php if (isset($_SESSION['login']) && $_SESSION['login']['username'] == "admin") { ?>
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="/manager/league">League</a>
                            </li>
                            <li>
                                <a href="/manager/team">Team</a>
                            </li>
                            <li>
                                <a href="/manager/match">Match</a>
                            </li>
                            <li>
                                <a href="/manager/review">Review</a>
                            </li>
                            <li>
                                <a href="/manager/player">Player</a>
                            </li>
                            <li>
                                <a href="/manager/news">News</a>
                            </li>
                            <li>
                                <a href="/manager/sexy">Sexy</a>
                            </li>
                            <li>
                                <a href="/manager/video">Video</a>
                            </li>
                            <li>
                                <a href="/manager/channel">Channel</a>
                            </li>
                            <li>
                                <a href="/manager/sitemap">SiteMap</a>
                            </li>
                        </ul>
                    <?php } ?>
                </div>
                <div class="navbar-management" id="navbarNavDropdown">
                    <ul class="nav navbar-nav navbar-right">
                        <?php if (!isset($_SESSION['login'])) { ?>
                            <li style="padding: 9px;">
                                <button data-toggle="modal" data-target="#myModalLogIn" type="button"
                                        class="btn btn-primary">Login
                                </button>
                            </li>
                        <?php } elseif (isset($_SESSION['login'])) { ?>
                            <li class="nav-item dropdown p-2 hide">
                            <span style="<?= (($_SESSION['login']['coupon'] >= 5) ? 'color: #bd0000;' : 'color:#fff') ?>"
                                  class="oi oi-medical-cross"></span>
                                <span style="<?= (($_SESSION['login']['coupon'] >= 4) ? 'color: #ae0000;' : 'color:#fff') ?>"
                                      class="oi oi-medical-cross"></span>
                                <span style="<?= (($_SESSION['login']['coupon'] >= 3) ? 'color: #9f0000;' : 'color:#fff') ?>"
                                      class="oi oi-medical-cross"></span>
                                <span style="<?= (($_SESSION['login']['coupon'] >= 2) ? 'color: #900000;' : 'color:#fff') ?>"
                                      class="oi oi-medical-cross"></span>
                                <span style="<?= (($_SESSION['login']['coupon'] >= 1) ? 'color: #810000;' : 'color:#fff') ?>"
                                      class="oi oi-medical-cross"></span>
                            </li>
                            <li>coin : <?= number_format($_SESSION['login']['coin']) ?></li>
                            <li>gold : <?= number_format($_SESSION['login']['gold']) ?></li>
                            <li class="dropdown dropdown-nav">
                                <div class="nav-image">
                                    <label class="m-0" for="inputUploadProfile">
                                        <img id="imageProfile"
                                             src="<?= ((!empty($_SESSION['login']['path'])) ? $_SESSION['login']['path'] : '/images/avatar.png') ?>"
                                             alt="...">
                                    </label>
                                    <form style="display: none;" id="formUploadProfile" method="post"
                                          enctype="multipart/form-data">
                                        <input type="text" name="id" value="<?= $_SESSION['login']['id'] ?>">
                                        <input id="inputUploadProfile" type="file" name="Filedata" accept="image/*"><br>
                                    </form>
                                </div>
                                <a class="dropdown-toggle" href=""
                                   id="navbarDropdownMenuLink"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?= $_SESSION['login']['username'] ?>
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a class="dropdown-item" data-toggle="modal" data-target="#modalGold">Check
                                            In</a></li>
                                    <li><a class="dropdown-item" data-toggle="modal" data-target="#result_bet">Result
                                            Bet</a></li>
                                    <li><a class="dropdown-item" href="/manager/logout">Logout</a></li>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <!--modal_gold-->
    <?php if (isset($_SESSION['login'])){ ?>
    <div class="modal fade" style="" tabindex="-1" role="dialog" id="result_bet">
        <div style="max-width: 100%;width: 60%;" class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Result Bet</h5>
                    <div> Lv. <?= $_SESSION['login']['lv']; ?>
                        <span class="oi oi-star <?= (($_SESSION['login']['star'] >= 1) ? 'text-warning' : '') ?>"></span>
                        <span class="oi oi-star <?= (($_SESSION['login']['star'] >= 2) ? 'text-warning' : '') ?>"></span>
                        <span class="oi oi-star <?= (($_SESSION['login']['star'] >= 3) ? 'text-warning' : '') ?>"></span>
                        <span class="oi oi-star <?= (($_SESSION['login']['star'] >= 4) ? 'text-warning' : '') ?>"></span>
                        <span class="oi oi-star <?= (($_SESSION['login']['star'] >= 5) ? 'text-warning' : '') ?>"></span>
                    </div>
                    <!--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
                    <!--                        <span aria-hidden="true">&times;</span>-->
                    <!--                    </button>-->
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-2" style="padding-right: 0px;">
                            <?php if (!empty($_SESSION['login']['bet'])) { ?>
                                <?php $dateSelect = ""; ?>
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                     aria-orientation="vertical">
                                    <?php foreach ($_SESSION['login']['bet'] as $key => $value) {
                                        $date = explode(' ', $value['created_at']);
                                        if ($dateSelect != $date[0]) { ?>
                                            <a href="#v-pills-<?= $key ?>"
                                               class="nav-link <?= (($key == 0) ? 'active' : '') ?>" data-toggle="pill"
                                               role="tab"><?= $date[0] ?></a>
                                            <?php $dateSelect = $date[0];
                                        } ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-10">
                            <div class="tab-content" id="v-pills-tabContent" style="border: solid 1px #cccccc;">
                                <?php if (!empty($_SESSION['login']['bet'])){ ?>
                                <?php $dateSelect = "";
                                $betWin = 0;
                                $betLose = 0;
                                $betEqual = 0; ?>
                                <?php foreach ($_SESSION['login']['bet'] as $key => $value) {
                                $date = explode(' ', $value['created_at']);
                                if ($dateSelect != $date[0]) {
                                if ($key !== 0){ ?>
                                <tr>
                                    <td colspan="2" style="text-align: center;">ถูก <span
                                                class="text-success"><?= $betWin ?></span> ผิด <span
                                                class="text-warning"><?= $betLose ?></span> เสมอ <span
                                                class="text-danger"><?= $betEqual ?></span></td>
                                    <td colspan="2" style="text-align: center;">
                                        ถูก <span
                                                class="text-success"><?= number_format((($betWin / ($betWin + $betLose + $betEqual)) * 100), 2, '.', '') ?>
                                            %</span>
                                        ผิด <span
                                                class="text-warning"><?= number_format((($betLose / ($betWin + $betLose + $betEqual)) * 100), 2, '.', '') ?>
                                            %</span>
                                        เสมอ <span
                                                class="text-danger"><?= number_format((($betEqual / ($betWin + $betLose + $betEqual)) * 100), 2, '.', '') ?>
                                            %</span>
                                    </td>
                                    <td>
                                        <?php if ($betWin > (($betWin + $betLose + $betEqual) / 2)) { ?>
                                            <span class="text-success">+1 Star</span>
                                        <?php } elseif ($betWin < (($betWin + $betLose + $betEqual) / 2)) { ?>
                                            <span class="text-danger">-1 Star</span>
                                        <?php } else { ?>
                                            <span class="text-warning">+0 Star</span>
                                        <?php } ?>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                            </div>
                            <?php }
                            $betWin = 0;
                            $betLose = 0;
                            $betEqual = 0;
                            ?>
                            <div class="tab-pane fade <?= (($key == 0) ? 'active show' : '') ?>"
                                 id="v-pills-<?= $key ?>" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                <div class="p-2">รายการผลการแทง</div>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col" style="text-align: center;">ผล</th>
                                        <th scope="col" style="text-align: center;">เจ้าบ้าน</th>
                                        <th scope="col" style="text-align: center;">ผล</th>
                                        <th scope="col" style="text-align: center;">ทีมเยือน</th>
                                        <th scope="col" style="text-align: center;"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $dateSelect = $date[0];
                                    } ?>
                                    <tr class="<?= (($value['status'] == 'cheap') ? 'table-success' : (($value['status'] == 'wrong') ? 'table-danger' : (($value['status'] == 'always') ? 'table-warning' : ''))) ?>">
                                        <td style="text-align: center;"><?= $value['status']; ?></td>
                                        <td style="text-align: right;">
                                            <span class="<?= (($value['team'] == "home") ? 'font-weight-bold' : '') ?>"><?= $value['home_name_en']; ?></span>
                                            <img src="<?php echo((!empty($value['team_home']) && file_exists($webroot . "/images/team/" . $value['team_home'] . "/" . $value['team_home'] . "_32x32.png")) ? "/images/team/" . $value['team_home'] . "/" . $value['team_home'] . "_32x32.png" : '/images/team/team_default_32x32.png') ?>">
                                        </td>
                                        <td style="text-align: center;">
                                            <div style="text-align: center;">
                                                (<?= ((!empty($value['home_water_bill'])) ? $value['home_water_bill'] : "-") ?>
                                                )-[ <?= ((!empty($value['handicap'])) ? $value['handicap'] : "-") ?>
                                                ]-(<?= ((!empty($value['away_water_bill'])) ? $value['away_water_bill'] : "-") ?>
                                                )
                                            </div>
                                            <div style="text-align: center;font-size: 16px;">
                                                <span class="text-info"><?= ((!empty($value['home_end_time_score'])) ? $value['home_end_time_score'] : "0") ?></span>
                                                -
                                                <span class="text-info"><?= ((!empty($value['away_end_time_score'])) ? $value['away_end_time_score'] : "0") ?></span>
                                            </div>
                                        </td>
                                        <td style="text-align: left;">
                                            <img src="<?php echo((!empty($value['team_away']) && file_exists($webroot . "/images/team/" . $value['team_away'] . "/" . $value['team_away'] . "_32x32.png")) ? "/images/team/" . $value['team_away'] . "/" . $value['team_away'] . "_32x32.png" : '/images/team/team_default_23x32.png') ?>">
                                            <span class="<?= (($value['team'] == "away") ? 'font-weight-bold' : '') ?>"><?= $value['away_name_en']; ?></span>
                                        </td>
                                        <td style="text-align: right;">
                                            <?php if ($value['status'] == 'cheap') { ?>
                                                <?php if ($value['team'] == "home") { ?>
                                                    <span class="text-warning">+<?= ($value['home_water_bill'] * $value['use_bet_coin']) ?>
                                                        G</span>
                                                <?php } else if ($value['team'] == "away") { ?>
                                                    <span class="text-warning">+<?= ($value['away_water_bill'] * $value['use_bet_coin']) ?>
                                                        G</span>
                                                <?php } ?>
                                            <?php } else if ($value['status'] == 'wrong') { ?>
                                                <span class="text-danger">+0 G</span>
                                            <?php } else { ?>
                                                -
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php (($value['status'] == "cheap") ? $betWin++ : (($value['status'] == "wrong") ? $betLose++ : (($value['status'] == "always") ? $betEqual++ : ''))); ?>
                                    <?php } ?>
                                    <tr>
                                        <td colspan="2" style="text-align: center;">ถูก <span
                                                    class="text-success"><?= $betWin ?></span> ผิด <span
                                                    class="text-warning"><?= $betLose ?></span> เสมอ <span
                                                    class="text-danger"><?= $betEqual ?></span></td>
                                        <td colspan="2" style="text-align: center;">
                                            ถูก <span
                                                    class="text-success"><?= number_format((($betWin / ($betWin + $betLose + $betEqual)) * 100), 2, '.', '') ?>
                                                %</span>
                                            ผิด <span
                                                    class="text-warning"><?= number_format((($betLose / ($betWin + $betLose + $betEqual)) * 100), 2, '.', '') ?>
                                                %</span>
                                            เสมอ <span
                                                    class="text-danger"><?= number_format((($betEqual / ($betWin + $betLose + $betEqual)) * 100), 2, '.', '') ?>
                                                %</span>
                                        </td>
                                        <td>
                                            <?php if (($betWin + $betLose + $betEqual) >= 3 && $betWin > (($betWin + $betLose + $betEqual) / 2)) { ?>
                                                <span class="text-success">+1 Star</span>
                                            <?php } elseif ($betWin < (($betWin + $betLose + $betEqual) / 2)) { ?>
                                                <span class="text-danger">-1 Star</span>
                                            <?php } else { ?>
                                                <span class="text-warning">+0 Star</span>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!--                    <button type="button" class="btn btn-primary">Save changes</button>-->
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php } ?>

<div class="modal fade" style="" id="myModalLogIn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="modal-title" id="exampleModalLabel">Login</h2>

            </div>
            <div class="modal-body">
                <form action="/manager/checkLogin" method="get">
                    <input type="hidden" name="login[link]" value="<?= $_SERVER['REQUEST_URI'] ?>">
                    <div class="form-group has-feedback">
                        <span class="glyphicon glyphicon-user form-control-feedback" aria-hidden="true"></span>
                        <input type="text" name="login[email]" class="form-control" id="inputEmail3"
                               placeholder="๊User">

                    </div>
                    <div class="form-group has-feedback">
                        <i class="fa fa-lock form-control-feedback" aria-hidden="true"></i>
                        <input type="password" name="login[password]" class="form-control" id="inputPassword3"
                               placeholder="Password">

                    </div>

                    <button type="submit" class="btn btn-primary btn-lg btn-block">Log in</button>
                    <button type="button" data-toggle="modal" data-target="#myModalRegister"
                            class="btn btn-default btn-lg btn-block">Register
                    </button>

                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalRegister" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <form id="formRegister">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title" id="exampleModalLabel">Register</h2>

                </div>
                <div class="modal-body">
                    <div class="form-group has-feedback">
                        <span class="glyphicon glyphicon-user form-control-feedback" aria-hidden="true"></span>
                        <input type="text" name="user[username]" class="form-control" id="inputEmail3"
                               placeholder="Username">
                        <small id="username_error" class="form-text text-danger"></small>
                    </div>
                    <div class="form-group has-feedback">
                        <span class="glyphicon glyphicon-envelope form-control-feedback" aria-hidden="true"></span>
                        <input type="email" name="user[email]" class="form-control" id="inputEmail3"
                               placeholder="Email">
                        <small id="email_error" class="form-text text-danger"></small>
                    </div>
                    <div class="form-group has-feedback">
                        <i class="fa fa-lock form-control-feedback" aria-hidden="true"></i>
                        <input type="password" name="user[password]" class="form-control" id="inputEmail3"
                               placeholder="Password">
                    </div>
                    <div class="form-group has-feedback">
                        <i class="fa fa-lock form-control-feedback" aria-hidden="true"></i>
                        <input type="password" name="user[cpassword]" class="form-control" id="inputEmail3"
                               placeholder="Confirm Password">
                        <small id="cpassword_error" class="form-text text-danger"></small>

                    </div>
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Register</button>
                </div>


            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="modelGeneratesSuccessVideo" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title">เตือนการสรา้งไฟล์</h4>
            </div>
            <div class="modal-body">
                <p>สรา้งไฟล์ สำเร็จ</p>
            </div>
            <div class="modal-footer">
                <a href="/manager/video" class="btn btn-outline-secondary">Close</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modelGeneratesSuccessSexy" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title">เตือนการสรา้งไฟล์</h4>
            </div>
            <div class="modal-body">
                <p>สรา้งไฟล์ สำเร็จ</p>
            </div>
            <div class="modal-footer">
                <a href="/manager/sexy" class="btn btn-outline-secondary">Close</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modelGeneratesSuccessNews" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title">เตือนการสรา้งไฟล์</h4>
            </div>
            <div class="modal-body">
                <p>สรา้งไฟล์ สำเร็จ</p>
            </div>
            <div class="modal-footer">
                <a href="/manager/news" class="btn btn-outline-secondary">Close</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modelGeneratesSuccessforeignNews" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">เตือนการสรา้งไฟล์</h4>
            </div>
            <div class="modal-body">
                <p>สรา้งไฟล์ สำเร็จ</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<!--                <a href="/manager/index" class="btn btn-outline-secondary">Close</a>-->
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<style>
    div.disabled {
        pointer-events: none;
        opacity: 0.4;
    }
</style>


<script>

    $(document).ready(function () {
        if (document.location.hash == "#Successvideo") {
            $('#modelGeneratesSuccessVideo').modal({backdrop: 'static', keyboard: false});
        } else if (document.location.hash == "#Successsexy") {
            $('#modelGeneratesSuccessSexy').modal({backdrop: 'static', keyboard: false});
        } else if (document.location.hash == "#Successnews") {
            $('#modelGeneratesSuccessNews').modal({backdrop: 'static', keyboard: false});
        }else if(document.location.hash =="#SuccessforeignNews" || document.location.hash =="#Successlivescore" || document.location.hash =="#Successgame"){
            $('#modelGeneratesSuccessforeignNews').modal({backdrop: 'static', keyboard: false});
        }
    });

    $('#v-pills-tab a').on('click', function (e) {
        $('.nav-link').removeClass('active');
        e.preventDefault()
        $(this).tab('show')
    });

    $(document).on('change', "#inputUploadProfile", function (e) {
        $("#formUploadProfile").submit();
    });

    $(document).on('submit', "#formUploadProfile", function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: "/manager/uploadFileProfile",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                response = jQuery.parseJSON(response);
//                console.log($('#formUpload').find('input[name=id]').val());
                console.log(response);
                $('img#imageProfile').attr('src', response + '#' + Math.floor((Math.random() * 1000) + 1));
            },
            error: function (data) {
                console.log("error");
                console.log(data);
            }
        });
    });

    $(document).on('keyup', 'form#formRegister input', function () {
        if ($(this).attr('name') == "user[username]") {
            var username = $(this).val();
            $.ajax({
                url: "/manager/checkUsername",
                data: {username: username},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                if (response['status']) {
                    $('#username_error').html("Username duplicate !!");
                } else {
                    $('#username_error').html("");
                }
            });
        } else if ($(this).attr('name') == "user[email]") {
            var email = $(this).val();
            $.ajax({
                url: "/manager/checkEmail",
                data: {email: email},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                if (response['status']) {
                    $('#email_error').html("Email duplicate !!");
                } else {
                    $('#email_error').html("");
                }
            });
        } else if ($(this).attr('name') == "user[cpassword]") {
            var password = $('form#formRegister').find('input[name="user[password]"]').first().val();
            var cpassword = $('form#formRegister').find('input[name="user[cpassword]"]').first().val();
            if (password != cpassword) {
                $('#cpassword_error').html("confirm password not math password");
            } else {
                $('#cpassword_error').html("");
            }
        }
    });
    $(document).on('focusout', 'form#formRegister input', function () {
        if ($(this).attr('name') == "user[username]") {
            var username = $(this).val();
            $.ajax({
                url: "/manager/checkUsername",
                data: {username: username},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                if (response['status']) {
                    $('#username_error').html("Username duplicate !!");
                } else {
                    $('#username_error').html("");
                }
            });
        } else if ($(this).attr('name') == "user[email]") {
            var email = $(this).val();
            $.ajax({
                url: "/manager/checkEmail",
                data: {email: email},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                if (response['status']) {
                    $('#email_error').html("Email duplicate !!");
                } else {
                    $('#email_error').html("");
                }
            });
        } else if ($(this).attr('name') == "user[cpassword]") {
            var password = $('form#formRegister').find('input[name="user[password]"]').first().val();
            var cpassword = $('form#formRegister').find('input[name="user[cpassword]"]').first().val();
            if (password != cpassword) {
                $('#cpassword_error').html("confirm password not math password");
            } else {
                $('#cpassword_error').html("");
            }
        }
    });
    $(document).on('submit', "#formRegister", function (e) {
        e.preventDefault();
        var email = $(this).find('input[name="user[email]"]').first().val();
        var username = $(this).find('input[name="user[username]"]').first().val();
        var password = $(this).find('input[name="user[password]"]').first().val();
        var cpassword = $(this).find('input[name="user[cpassword]"]').first().val();
        $.ajax({
            url: "/manager/checkUsername",
            data: {username: username},
            method: "GET",
            dataType: "json"
        }).done(function (response) {
            if (response['status']) {
                $('#username_error').html("Username Duplicate !!");
            } else {
                $.ajax({
                    url: "/manager/checkEmail",
                    data: {email: email},
                    method: "GET",
                    dataType: "json"
                }).done(function (response) {
                    if (response['status']) {
                        $('#email_error').html("Email Duplicate !!");
                    } else {
                        if (password != cpassword) {
                            $('#cpassword_error').html("confirm password not math password");
                        } else {
                            $.ajax({
                                url: "/manager/register",
                                data: $("form#formRegister").serialize(),
                                method: "GET",
                                dataType: "json"
                            }).done(function (response) {
                                if (response['status']) {
                                    alert("บันทึกข้อมูลสำเร็จ");
                                    window.location.reload();
                                }
                            })
                        }
                    }
                });
            }
        });
    })

</script>

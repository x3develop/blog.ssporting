<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 3/6/2018
 * Time: 21:44
 */
include '_database.php';
if (!isset($_SESSION)) {
    session_start();
}
if (isset($_REQUEST['news'])) {
    if (!empty($_REQUEST['news']['newsId'])) {
        $sql = "UPDATE `news` set `titleEn`=?,`titleTh`=?,`shortDescriptionEn`=?,`shortDescriptionTh`=?,`contentEn`=?,`contentTh`=?,`imageLink`='" . $_REQUEST['news']['imageLink'] . "',`category`='" . $_REQUEST['news']['category'] . "',`groupNews`='" . $_REQUEST['news']['groupNews'] . "',`typeVideo`='" . $_REQUEST['news']['typeVideo'] . "',`news_tag`='" . $_REQUEST['news']['news_tag'] . "',`linkVideo`='" . $_REQUEST['news']['linkVideo'] . "',`recommentNews`='" . $_REQUEST['news']['recommentNews'] . "',`updateDatetime`=" . date("U") . ",`updated_at`='" . date("Y-m-d H:i:s") . "' WHERE newsId=" . $_REQUEST['news']['newsId'];
    } else {
        $sql = "INSERT INTO `news` (`titleEn`,`titleTh`,`shortDescriptionEn`,`shortDescriptionTh`,`contentEn`,`contentTh`,`imageLink`,`category`,`groupNews`,`typeVideo`,`news_tag`,`linkVideo`,`recommentNews`,`created_at`,`updated_at`,`createDatetime`,`updateDatetime`)";
        $sql .= " VALUES (?,?,?,?,?,?,'" . $_REQUEST['news']['imageLink'] . "','" . $_REQUEST['news']['category'] . "','" . $_REQUEST['news']['groupNews'] . "','" . $_REQUEST['news']['typeVideo'] . "','" . $_REQUEST['news']['news_tag'] . "','" . $_REQUEST['news']['linkVideo'] . "','" . $_REQUEST['news']['recommentNews'] . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "'," . date("U") . "," . date("U") . ")";
    }
    $sth = $dbh->prepare($sql)->execute([$_REQUEST['news']['titleTh'], $_REQUEST['news']['titleTh'], $_REQUEST['news']['shortDescriptionTh'], $_REQUEST['news']['shortDescriptionTh'], $_REQUEST['news']['contentTh'], $_REQUEST['news']['contentTh']]);
    if ($sth) {
        header("Location: /manager/news");
    } else {
        echo "ไม่สามารถบันทึกข้อความนี้ได้";
        exit;
    }
    exit;
} elseif (isset($_REQUEST['id'])) {
    $sth = $dbh->prepare("select * from news where newsId=?");
    $sth->execute([$_REQUEST['id']]);
    $dataNewsEdit = $sth->fetch();
} elseif (isset($_REQUEST['delete_id'])) {
    if(isset($_SESSION['login']) && !empty($_SESSION['login']['username']) && $_SESSION['login']['username']=="admin") {
        $sth = $dbh->prepare("delete from news where newsId=" . $_REQUEST['delete_id']);
        $sth->execute();
    }
    header("Location: /manager/news");
    exit;
}
//------------------page------------------
$sth = $dbh->prepare("select count(*) as total from news");
$sth->execute();
$dataVideoHighlightList = $sth->fetch();
$pageTotal = ceil((int) $dataVideoHighlightList["total"] / (int) 30);
$pageTotalFor=$pageTotal;
$page=(isset($_REQUEST['page'])?$_REQUEST['page']:1);
if (isset($_GET['search'])) {
    $sth = $dbh->prepare("select * from news WHERE `titleEn` like '%" . $_GET['search']['name'] . "%' order by newsId DESC limit 30");
    $sth->execute();
    $dataNews = $sth->fetchAll();
} else {
    $sth = $dbh->prepare("select * from news order by newsId DESC limit 30 offset ".(($page-1)*30));
    $sth->execute();
    $dataNews = $sth->fetchAll();
}
if($pageTotal>100 and $page>=100){
    $pageTotalFor=$page+100;
}else{
    $pageTotalFor=100;
}
$webroot = $_SERVER['DOCUMENT_ROOT'];

?>
<?php include 'layouts/header.php'; ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/manager/froala/css/froala_editor.css">
<link rel="stylesheet" href="/manager/froala/css/froala_style.css">
<link rel="stylesheet" href="/manager/froala/css/plugins/code_view.css">
<link rel="stylesheet" href="/manager/froala/css/plugins/colors.css">
<link rel="stylesheet" href="/manager/froala/css/plugins/emoticons.css">
<link rel="stylesheet" href="/manager/froala/css/plugins/image_manager.css">
<link rel="stylesheet" href="/manager/froala/css/plugins/image.css">
<link rel="stylesheet" href="/manager/froala/css/plugins/line_breaker.css">
<link rel="stylesheet" href="/manager/froala/css/plugins/table.css">
<link rel="stylesheet" href="/manager/froala/css/plugins/char_counter.css">
<link rel="stylesheet" href="/manager/froala/css/plugins/video.css">
<link rel="stylesheet" href="/manager/froala/css/plugins/fullscreen.css">
<link rel="stylesheet" href="/manager/froala/css/plugins/file.css">
<link rel="stylesheet" href="/manager/froala/css/plugins/quick_insert.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">

<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>

<script type="text/javascript" src="/manager/froala/js/froala_editor.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/align.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/char_counter.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/code_beautifier.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/code_view.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/colors.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/draggable.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/emoticons.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/entities.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/file.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/font_size.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/font_family.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/fullscreen.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/image.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/image_manager.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/line_breaker.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/inline_style.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/link.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/lists.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/paragraph_format.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/paragraph_style.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/quick_insert.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/quote.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/table.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/save.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/url.min.js"></script>
<script type="text/javascript" src="/manager/froala/js/plugins/video.min.js"></script>

<div class="container-fluid">
    <div class="page-header title">

        <h3>Manage News</h3>
        <button type="button" class="btn btn-default" data-toggle="modal"
                data-target="#exampleModal">
            <i class="fa fa-plus" aria-hidden="true"></i> Add
        </button>
        <a href="/load/gameNew?type=news" class="btn btn-default"><i class="fa fa-check"
                                                                         aria-hidden="true"></i>
            Generates</a>
        <!--        <form class="form-inline">-->
        <!--            <input type="hidden" name="player[id]" value="-->
        <? //= ((!empty($player['id']))?$player['id']:'')?><!--">-->
        <!--            <label class="sr-only" for="inlineFormInput">League Name</label>-->
        <!--            <input required name="player[name]" value="-->
        <? //= ((!empty($player['name']))?$player['name']:'')?><!--" type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" placeholder="Play Name">-->
        <!--            <button type="submit" class="btn -->
        <? //= ((!empty($player['id']))?'btn-outline-warning':'btn-outline-primary')?><!--">-->
        <? //= ((!empty($player['id']))?'Edit':'Add')?><!--</button>-->
        <!--        </form>-->


        <form class="form-inline pull-right">
            <label class="sr-only" for="inlineFormInput">Player Name</label>
            <input name="search[name]" type="text" class="form-control mb-2 mr-sm-2 mb-sm-0"
                   placeholder="Search Name">
            <button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i> Search
            </button>
        </form>


    </div>

    <div class="row">
        <nav aria-label="Page navigation">
            <ul class="pagination pagination-sm" style="margin: 0px;">
                <?php if($page>1){ ?>
                    <li>
                        <a href="/manager/news?page=<?=$page-1?>" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                <?php } ?>
                <?php for ($i = 1; $i <= $pageTotalFor; $i++) { ?>
                    <?php if($i==$page){ ?>
                        <li class="active"><a href="/manager/news?page=<?=$i?>"><?=$i?></a></li>
                    <?php }else{ ?>
                        <li class=""><a href="/manager/news?page=<?=$i?>"><?=$i?></a></li>
                    <?php } ?>
                <?php } ?>
                <?php if($page!=$pageTotal){ ?>
                    <li>
                        <a href="/manager/news?page=<?=$pageTotal-1?>" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </nav>
    </div>

    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ชื่อหัวข้อ (ไทย)</th>
                    <th>หมวดหมู่</th>
                    <th>ป้ายกำกับ</th>
                    <th style="width: 12%;">เวลาที่แก้ไขล่าสุด</th>
                    <th class="text-center">ดู/แก้ไข</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($dataNews as $key => $value) { ?>
                    <tr>
                        <td><img src="<?= $value['imageLink']; ?>" width="100"></td>
                        <td><?= $value['titleTh']; ?></td>
                        <td><?= $value['category']; ?></td>
                        <td><?= $value['news_tag']; ?></td>
                        <td><?= date("Y-m-d H:i:s",strtotime($value['created_at']." +7 hours")); ?></td>
                        <td class="text-center">
                            <div class="btn-group-manage" style="width: 67px;" aria-label="...">
                                <a class="btn btn1 btn-default" href="/manager/news?id=<?= $value['newsId']; ?>"
                                   title="แก้ไข"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a class="btn btn3 btn-default"
                                   href="/manager/news?delete_id=<?= $value['newsId']; ?>" title="ลบ"><i
                                            class="fa fa-trash" aria-hidden="true"></i></a>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

</div>

<div class="modal fade <?php echo((!empty($dataNewsEdit["newsId"])) ? ' in show' : '') ?>"
     style="<?php echo((!empty($dataNewsEdit["newsId"])) ? 'display: block;' : '') ?>" id="exampleModal" tabindex="-1"
     role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="/manager/news" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h4 class="modal-title">Edit/Add News</h4>

                </div>
                <div class="modal-body">
                    <input type="hidden" name="news[newsId]"
                           value="<?php echo((!empty($dataNewsEdit["newsId"])) ? $dataNewsEdit["newsId"] : '') ?>">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">หัวข้อข่าว</label>
                            <div class="col-sm-10">
                                <input value="<?php echo((!empty($dataNewsEdit["titleTh"])) ? $dataNewsEdit["titleTh"] : '') ?>"
                                       name="news[titleTh]" type="text" class="form-control" id="inputPassword"
                                       placeholder="หัวข้อข่าว">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">เนื้อข่าวย่อ</label>
                            <div class="col-sm-10">
                            <textarea name="news[shortDescriptionTh]" class="form-control"
                                      placeholder="เนื้อข่าวย่อ"><?php echo((!empty($dataNewsEdit["shortDescriptionTh"])) ? $dataNewsEdit["shortDescriptionTh"] : '') ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">เนื้อข่าวเต็ม</label>
                            <div class="col-sm-10">
                            <textarea name="news[contentTh]"
                                      id="froala-editor"><?php echo((!empty($dataNewsEdit["contentTh"])) ? $dataNewsEdit["contentTh"] : '') ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Link รูปภาพ</label>
                            <div class="col-sm-10">
                                <input value="<?php echo((!empty($dataNewsEdit["imageLink"])) ? $dataNewsEdit["imageLink"] : '') ?>"
                                       name="news[imageLink]" type="text" class="form-control" id="inputPassword"
                                       placeholder="ลิงค์รูปภาพ">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Upload รูปภาพ</label>
                            <div class="col-sm-10">
                                <img id="player" style="height: 100px;"
                                     src="<?php echo((!empty($dataNewsEdit["imageLink"])) ? $dataNewsEdit["imageLink"] : '/manager/images/image-not-found.png') ?>">
                                <label id="labelUpload" for="inputUpload" class="btn btn-primary"><i
                                            class="fa fa-upload" aria-hidden="true"></i> Upload รูปภาพ</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <!--                    <label for="inputPassword" class="col-sm-2 col-form-label" style="text-align: right;">อื่น</label>-->

                                <div class="row">
                                    <div class="col-sm-2">
                                        <label>Category</label>
                                        <select name="news[category]" class="form-control">
                                            <option <?php echo((!empty($dataNewsEdit["category"]) and $dataNewsEdit["category"] == "other") ? 'selected' : '') ?>
                                                    value="other">other
                                            </option>
                                            <option <?php echo((!empty($dataNewsEdit["category"]) and $dataNewsEdit["category"] == "thai") ? 'selected' : '') ?>
                                                    value="thai">thai
                                            </option>
                                            <option <?php echo((!empty($dataNewsEdit["category"]) and $dataNewsEdit["category"] == "premier league") ? 'selected' : '') ?>
                                                    value="premier league">premier league
                                            </option>
                                            <option <?php echo((!empty($dataNewsEdit["category"]) and $dataNewsEdit["category"] == "la liga") ? 'selected' : '') ?>
                                                    value="la liga">la liga
                                            </option>
                                            <option <?php echo((!empty($dataNewsEdit["category"]) and $dataNewsEdit["category"] == "bundesliga") ? 'selected' : '') ?>
                                                    value="bundesliga">bundesliga
                                            </option>
                                            <option <?php echo((!empty($dataNewsEdit["category"]) and $dataNewsEdit["category"] == "serie a") ? 'selected' : '') ?>
                                                    value="serie a">serie a
                                            </option>
                                            <option <?php echo((!empty($dataNewsEdit["category"]) and $dataNewsEdit["category"] == "league 1") ? 'selected' : '') ?>
                                                    value="league 1">league 1
                                            </option>
                                            <option <?php echo((!empty($dataNewsEdit["category"]) and $dataNewsEdit["category"] == "ucl") ? 'selected' : '') ?>
                                                    value="ucl">ucl
                                            </option>
                                            <option <?php echo((!empty($dataNewsEdit["category"]) and $dataNewsEdit["category"] == "europa") ? 'selected' : '') ?>
                                                    value="europa">europa
                                            </option>
                                            <option <?php echo((!empty($dataNewsEdit["category"]) and $dataNewsEdit["category"] == "international") ? 'selected' : '') ?>
                                                    value="international">international
                                            </option>
                                            <option <?php echo((!empty($dataNewsEdit["category"]) and $dataNewsEdit["category"] == "other league") ? 'selected' : '') ?>
                                                    value="other league">other league
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>Group news</label>
                                        <select name="news[groupNews]" class="form-control">
                                            <option <?php echo((!empty($dataNewsEdit["groupNews"]) and $dataNewsEdit["groupNews"] == "common") ? 'selected' : '') ?>
                                                    value="common">Common
                                            </option>
                                            <option <?php echo((!empty($dataNewsEdit["groupNews"]) and $dataNewsEdit["groupNews"] == "highlight") ? 'selected' : '') ?>
                                                    value="highlight">Highlight
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>Type video</label>
                                        <select name="news[typeVideo]" class="form-control">
                                            <option <?php echo((!empty($dataNewsEdit["typeVideo"]) and $dataNewsEdit["typeVideo"] == "youtube") ? 'selected' : '') ?>
                                                    value="youtube">Youtube
                                            </option>
                                            <option <?php echo((!empty($dataNewsEdit["typeVideo"]) and $dataNewsEdit["typeVideo"] == "facebook") ? 'selected' : '') ?>
                                                    value="facebook">Facebook
                                            </option>
                                            <option <?php echo((!empty($dataNewsEdit["typeVideo"]) and $dataNewsEdit["typeVideo"] == "twitter") ? 'selected' : '') ?>
                                                    value="twitter">Twitter
                                            </option>
                                            <option <?php echo((!empty($dataNewsEdit["typeVideo"]) and $dataNewsEdit["typeVideo"] == "dailymotion") ? 'selected' : '') ?>
                                                    value="dailymotion">Dailymotion
                                            </option>
                                            <option <?php echo((!empty($dataNewsEdit["typeVideo"]) and $dataNewsEdit["typeVideo"] == "streamable") ? 'selected' : '') ?>
                                                    value="streamable">Streamable
                                            </option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">News tag</label>
                            <div class="col-sm-10">
                                <input value="<?php echo((!empty($dataNewsEdit["news_tag"])) ? $dataNewsEdit["news_tag"] : '') ?>"
                                       name="news[news_tag]" type="text" class="form-control" id="inputPassword"
                                       placeholder="news_tag">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Link video</label>
                            <div class="col-sm-6">
                                <input value="<?php echo((!empty($dataNewsEdit["linkVideo"])) ? $dataNewsEdit["linkVideo"] : '') ?>"
                                       name="news[linkVideo]" type="text" class="form-control" id="inputPassword"
                                       placeholder="http://www.example.com">
                            </div>
                            <label class="col-sm-2 control-label">RecommentNews</label>
                            <div class="col-sm-2">
                                <select name="news[recommentNews]" class="form-control">
                                    <option <?php echo((!empty($dataNewsEdit["recommentNews"]) and $dataNewsEdit["recommentNews"] == 0) ? 'selected' : '') ?>
                                            value="0">No Recomment
                                    </option>
                                    <option <?php echo((!empty($dataNewsEdit["recommentNews"]) and $dataNewsEdit["recommentNews"] == 1) ? 'selected' : '') ?>
                                            value="1">Recomment
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="/manager/news" class="btn btn-default">Close</a>
                    <?php if (!empty($dataNewsEdit['newsId'])) { ?>
                        <button type="submit" class="btn btn-warning">Changes</button>
                    <?php } else { ?>
                        <button type="submit" class="btn btn-primary">Save</button>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
</div>
<form style="display: none;" id="formUpload" method="post" enctype="multipart/form-data">
    <input id="inputUpload" type="file" name="Filedata[]" multiple="multiple" accept="image/*"><br>
</form>
<script>

    $(document).on('change', "#inputUpload", function (e) {
        $("#formUpload").submit();
    });
    $(document).on('submit', "#formUpload", function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: "/manager/uploadFileNews",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                response = jQuery.parseJSON(response);
                console.log(response);
//                            $('img#register-picture').attr('src',response+'#'+Math.floor((Math.random() * 1000) + 1));
                $('input[name="news[imageLink]"]').val(response);
                $('img#player').attr('src', response + '#' + Math.floor((Math.random() * 1000) + 1));
            },
            error: function (data) {
                console.log("error");
                console.log(data);
            }
        });
    });

    $(function () {
        // $.FroalaEditor.DefineIcon('imageInfo', {NAME: 'info'});
        // $.FroalaEditor.RegisterCommand('imageInfo', {
        //     title: 'Info',
        //     focus: false,
        //     undo: false,
        //     refreshAfterCallback: false,
        //     callback: function () {
        //         var $img = this.image.get();
        //         alert($img.attr('src'));
        //     }
        // });
        //
        // $('#froala-editor').froalaEditor({
        //     height: 200,
        //     width: '100%',
        //     imageEditButtons: ['imageDisplay', 'imageAlign', 'imageInfo', 'imageRemove']
        // });
        //
        // if ($('#froala-editor').val() !== "") {
        //     $('#froala-editor').froalaEditor('html.set', "" + $('#froala-editor').val() + "");
        // }
    });
</script>
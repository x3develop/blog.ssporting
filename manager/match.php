<?php include '_database.php'; ?>
<?php
if (!isset($_SESSION)) {
    session_start();
}
if (isset($_GET['comment'])) {
    if (!empty($_GET['comment']['id'])) {

    } else {
        $sql = "INSERT INTO `comment_match` (`user_id`,`match_id`,`team`,`comment`,`created_at`,`updated_at`)";
        $sql .= " VALUES ('" . $_GET['comment']['user_id'] . "','" . $_GET['comment']['match_id'] . "','" . $_GET['comment']['team'] . "','" . $_GET['comment']['review_text'] . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
    }
    $sth = $dbh->prepare($sql)->execute();
    header("Location: /manager/match");
    exit;
}

if (isset($_GET['review'])) {
    if (!empty($_GET['review']['id'])) {

    } else {
        $sql = "INSERT INTO `review_match` (`user_id`,`match_id`,`user_review_id`,`team`,`review_text`,`created_at`,`updated_at`)";
        $sql .= " VALUES ('" . $_GET['review']['user_id'] . "','" . $_GET['review']['match_id'] . "','" . $_GET['review']['user_review_id'] . "','" . $_GET['review']['team'] . "','" . $_GET['review']['review_text'] . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
    }
    $sth = $dbh->prepare($sql)->execute();
    header("Location: /manager/match");
    exit;
}

if (isset($_GET['match'])) {
    $date = new DateTime();
    $dateList = list($day, $month, $year) = explode('/', $_GET['match']['date']);
    $timeList = list($hour, $minute) = explode(':', $_GET['match']['time']);
    $date->setDate($year, $month, $day);
    $date->setTime($hour, $minute);

    $sth = $dbh->prepare("select * from `team` where name_en='" . $_GET['match']['team_home'] . "'");
    $sth->execute();
    $teamHome = $sth->fetch();

    $sth = $dbh->prepare("select * from `team` where name_en='" . $_GET['match']['team_away'] . "'");
    $sth->execute();
    $teamAway = $sth->fetch();

    if (!empty($teamHome) && !empty($teamAway)) {
        if (!empty($_GET['match']['id'])) {
            if (!empty($_GET['match']['match_7m_id']) && $_GET['match']['match_7m_id'] !== "") {
                $sql = "UPDATE `match` set `match_7m_id`=" . $_GET['match']['match_7m_id'] . ",`league_id`=" . $_GET['match']['league_id'] . ",`team_home`=" . $teamHome['id'] . ",`team_away`=" . $teamAway['id'] . ",`time_match`='" . $date->format('Y-m-d H:i:s') . "',`updated_at`='" . date("Y-m-d H:i:s") . "' WHERE id=" . $_GET['match']['id'];
            } else {
                $sql = "UPDATE `match` set `league_id`=" . $_GET['match']['league_id'] . ",`team_home`=" . $teamHome['id'] . ",`team_away`=" . $teamAway['id'] . ",`time_match`='" . $date->format('Y-m-d H:i:s') . "',`updated_at`='" . date("Y-m-d H:i:s") . "' WHERE id=" . $_GET['match']['id'];
            }
        } else {
            if (!empty($_GET['match']['match_7m_id'])) {
                $sql = "INSERT INTO `match` (`league_id`,`match_7m_id`,`team_home`,`team_away`,`status`,`time_match`,`created_at`,`updated_at`)";
                $sql .= " VALUES (" . $_GET['match']['league_id'] . "," . $_GET['match']['match_7m_id'] . "," . $teamHome['id'] . "," . $teamAway['id'] . ",'create','" . $date->format('Y-m-d H:i:s') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
            } else {
                $sql = "INSERT INTO `match` (`league_id`,`team_home`,`team_away`,`status`,`time_match`,`created_at`,`updated_at`)";
                $sql .= " VALUES (" . $_GET['match']['league_id'] . "," . $teamHome['id'] . "," . $teamAway['id'] . ",'create','" . $date->format('Y-m-d H:i:s') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
            }
        }
        $sth = $dbh->prepare($sql)->execute();
    }
    if(!empty($_GET['match']['link'])) {
        header("Location: ".$_GET['match']['link']);
    }else{
        header("Location: /manager/match");
    }
    exit;
}

if (isset($_GET['score'])) {
    $sql = "UPDATE `match` set `status`='" . ((!empty($_GET['score']['status'])) ? $_GET['score']['status'] : 'create') . "',`updated_at`='" . date("Y-m-d H:i:s") . "' where id=" . $_GET['score']['id'];
    $sth = $dbh->prepare($sql)->execute();
    header("Location: /manager/match");
    exit;
}

if (isset($_GET['gamble'])) {
    $sth = $dbh->prepare("select * from `gamble` where match_id=" . $_GET['gamble']['match_id']);
    $sth->execute();
    $gamble = $sth->fetch();
    if (!empty($gamble)) {
        $sql = "UPDATE `gamble` set `home_water_bill`=" . $_GET['gamble']['home_water_bill'] . ",`handicap`=" . $_GET['gamble']['handicap'] . ",`away_water_bill`=" . $_GET['gamble']['away_water_bill'] . ",`updated_at`='" . date("Y-m-d H:i:s") . "' where match_id=" . $_GET['gamble']['match_id'];
    } else {
        $sql = "INSERT INTO `gamble` (`match_id`,`home_water_bill`,`handicap`,`away_water_bill`,`created_at`,`updated_at`)";
        $sql .= " VALUES (" . $_GET['gamble']['match_id'] . "," . $_GET['gamble']['home_water_bill'] . "," . $_GET['gamble']['handicap'] . "," . $_GET['gamble']['away_water_bill'] . ",'" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
    }
    $sth = $dbh->prepare($sql)->execute();

    $sql = "UPDATE `match` set `status`='bet',`updated_at`='" . date("Y-m-d H:i:s") . "' where id=" . $_GET['gamble']['match_id'];
    $sth = $dbh->prepare($sql)->execute();

    header("Location: ".$_GET['gamble']['link']);
    exit;
}

if (isset($_GET['delete_id'])) {
    if(isset($_SESSION['login']) && !empty($_SESSION['login']['username']) && $_SESSION['login']['username']=="admin") {
        $sth = $dbh->prepare("delete from `match` where id=" . $_GET['delete_id']);
        $sth->execute();
    }
    header("Location: /manager/match");
    exit;
}

?>
<?php include 'layouts/header.php'; ?>
<?php

$sth = $dbh->prepare("select * from country ");
$sth->execute();
$countryAll = $sth->fetchAll();


$sth = $dbh->prepare("select * from league ");
$sth->execute();
$leagueAll = $sth->fetchAll();

$sth = $dbh->prepare("select * from channel ");
$sth->execute();
$channelAll = $sth->fetchAll();


$leagueTeam = array();
foreach ($leagueAll as $key => $value) {
    $sth = $dbh->prepare("select id,name_en from team where league_id=" . $value['id'] . " order by name_en ");
    $sth->execute();
    $teamAll = $sth->fetchAll(PDO::FETCH_ASSOC);
    $leagueTeam[$value['id']] = $teamAll;
}

$sth = $dbh->prepare("select id,`name` from `user_review`");
$sth->execute();
$selectReview = $sth->fetchAll();

$sth = $dbh->prepare("select id,`username` from `user` limit 30");
$sth->execute();
$selectUser = $sth->fetchAll();

$sth = $dbh->prepare("select id,name_en from `team`");
$sth->execute();
$selectTeam = $sth->fetchAll(PDO::FETCH_COLUMN | PDO::FETCH_GROUP);
if (isset($_GET['search']['league_id']) and isset($_GET['search']['team_name']) and $_GET['search']['team_name'] != "") {
    $sthTeam = $dbh->prepare("select * from `team` where team.name_en=?");
    $sthTeam->execute([$_GET['search']['team_name']]);
    $selectTeamSearch = $sthTeam->fetch();
    if (!empty($selectTeamSearch)) {
        $sth = $dbh->prepare("select `match`.*,`league`.name as league_name,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` left join `gamble` on `gamble`.match_id=`match`.id left join `league` on `league`.id=`match`.league_id WHERE (date(now())=date(time_match) or  date(now()+ INTERVAL 1 DAY)=date(time_match)) and  league_id=" . $_GET['search']['league_id'] . " and (team_home=" . $selectTeamSearch["id"] . " || team_away=" . $selectTeamSearch["id"] . ") order by time_match DESC limit 60 ");
    } else {
        $sth = $dbh->prepare("select `match`.*,`league`.name as league_name,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` left join `gamble` on `gamble`.match_id=`match`.id left join `league` on `league`.id=`match`.league_id WHERE (date(now())=date(time_match) or  date(now()+ INTERVAL 1 DAY)=date(time_match)) and  league_id=" . $_GET['search']['league_id'] . " order by time_match DESC limit 60 ");
    }
} else if (isset($_GET['search']['league_id'])) {
    $sth = $dbh->prepare("select `match`.*,`league`.name as league_name,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` left join `gamble` on `gamble`.match_id=`match`.id left join `league` on `league`.id=`match`.league_id WHERE (date(now())=date(time_match) or  date(now()+ INTERVAL 1 DAY)=date(time_match)) and league_id=" . $_GET['search']['league_id'] . " order by time_match DESC limit 60 ");
} else {
    $sth = $dbh->prepare("select `match`.*,`league`.name as league_name,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` left join `gamble` on `gamble`.match_id=`match`.id left join `league` on `league`.id=`match`.league_id where date(now())=date(time_match) or  date(now()+ INTERVAL 1 DAY)=date(time_match) order by time_match DESC limit 30 ");
}
$sth->execute();
$matchAll = $sth->fetchAll();

if (isset($_GET['search']['league_id'])) {
    $sth = $dbh->prepare("select id,name_en from `team` WHERE league_id=" . $_GET['search']['league_id'] . " order by name_en ASC");
    $sth->execute();
    $team = $sth->fetchAll();
}

?>
<style>
    .typeahead,
    .tt-query,
    .tt-hint {
        width: 396px;
        height: 25px;
        padding: 8px 12px;
        line-height: 30px;
        border: 2px solid #ccc;
        outline: none;
    }

    .typeahead {
        background-color: #fff;
    }

    .typeahead:focus {
        border: 2px solid #0097cf;
    }

    .tt-query {
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    }

    .tt-hint {
        color: #999;
    }

    .tt-menu {
        width: 100px;
        margin: 5px 0;
        padding: 8px 0;
        background-color: #fff;
        border: 1px solid #ccc;
        border: 1px solid rgba(0, 0, 0, 0.2);
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        border-radius: 8px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
    }

    .tt-suggestion {
        padding: 3px 20px;
        font-size: 14px;
        line-height: 20px;
        color: #000;
    }

    .tt-suggestion:hover {
        cursor: pointer;
        color: #fff;
        background-color: #084561;
    }

    .tt-suggestion.tt-cursor {
        color: #fff;
        background-color: #084561;

    }

    .tt-suggestion p {
        margin: 0;
    }

    .typeahead {
        height: auto !important;
    }

    .tt-menu {
        width: auto;
    !important;
    }
</style>
<div class="container-fluid">
    <div class="page-header title">
        <h3>Match</h3>
        <div class="pull-right" style="margin-top: 20px;">
            <button type="button" data-toggle="modal" data-target="#ModalAddMatch" onclick="resetForm()"
                    class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add Match
            </button>
        </div>
    </div>

    <div class="col-12">
        <div class="well well-sm">
            <form class="form-inline">
                <label class="sr-only" for="inlineFormInput">League Name</label>
                <select id="select-country" name="search[country]" class="form-control">
                    <option value="">Country Select</option>
                    <?php foreach ($countryAll as $key => $value) { ?>
                        <option value="<?= $value['id']; ?>"><?= $value['name']; ?></option>
                    <?php } ?>
                </select>
                <select id="select-league" name="search[league_id]" style="width: 300px;" class="form-control">
                    <option value="">League Select</option>
                    <?php foreach ($leagueAll as $key => $value) { ?>
                        <option class="option-country option-countryId-<?= $value['country_id'] ?>" <?= ((isset($_GET['search']['league_id']) and $_GET['search']['league_id'] == $value['id']) ? 'selected' : '') ?>
                                value="<?php echo $value['id'] ?>"><?php echo $value['name'] ?></option>
                    <?php } ?>
                </select>
                <input type="text" id="select-team_name" name="search[team_name]" class="form-control"
                       placeholder="Team Name">
                <button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i> Search
                </button>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>league name</th>
                    <th>Home</th>
                    <th class="text-center">Score</th>
                    <th>Away</th>
                    <th class="text-center">Gamble</th>
                    <th>Status</th>
                    <th>Highlight</th>
                    <th class="text-center">Tools</th>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($matchAll as $key => $value) { ?>
                    <tr>
                        <td><?= ($key + 1) ?></td>
                        <td><?php echo $value['league_name']; ?></td>
                        <td id="Team-Home-<?= $value['id'] ?>"><?= $selectTeam[$value['team_home']][0] ?></td>
                        <td>
                            <?php $date = new DateTime($value['time_match']); ?>
                            <div style="text-align: center;"><?php echo $date->modify("-1 hour")->format('M-d H:i'); ?></div>
                            <div style="text-align: center;">(<span
                                        class="text-danger"><?= ((!empty($value['home_over_time_score'])) ? $value['home_over_time_score'] : '-') ?></span>)
                                <span class="text-info"><?= (($value['home_end_time_score'] !== null) ? $value['home_end_time_score'] : "-") ?></span>
                                -
                                <span class="text-info"><?= (($value['away_end_time_score'] !== null) ? $value['away_end_time_score'] : "-") ?></span>
                                (<span class="text-danger"><?= ((!empty($value['away_over_time_score'])) ? $value['away_over_time_score'] : "-") ?></span>)
                            </div>
                        </td>
                        <td id="Team-Away-<?= $value['id'] ?>"><?= $selectTeam[$value['team_away']][0]?></td>
                        <td style="text-align: center;">
                            (<?= ((!empty($value['home_water_bill'])) ? $value['home_water_bill'] : "-")?>
                            )-<?= (($value['handicap']===null) ? "-" : ((!empty($value['handicap']))?$value['handicap']:"0"))?>
                            -(<?= ((!empty($value['away_water_bill'])) ? $value['away_water_bill'] : "-")?>)
                        </td>
                        <td class="match-status" mid="<?= $value['id'] ?>"><?= $value['status'] ?></td>
                        <td><input type="checkbox" class="mark-highlight" mid="<?php echo $value['id']; ?>"></td>
                        <td>
                            <div class="btn-group-manage" style="width: 100%;">
                                <button type="button" onclick='manageSnap(<?= json_encode($value); ?>)'
                                        class="btn btn1 btn-default"><span class="oi oi-aperture"></span></button>
                                <button type="button" onclick='manageChannel(<?= json_encode($value); ?>)'
                                        class="btn btn2 btn-default">Channel
                                </button>
                                <button type="button" onclick='manageScore(<?= json_encode($value); ?>)'
                                        class="btn btn-default btn2">Score
                                </button>
                                <button type="button" onclick='manageGamble(<?= json_encode($value); ?>)'
                                        class="btn btn-default btn2">Gamble
                                </button>
                                <button type="button" onclick='manageReview(<?= json_encode($value); ?>)'
                                        class="btn btn-default btn2">Review
                                </button>
                                <button type="button" onclick='manageComment(<?= json_encode($value); ?>)'
                                        class="btn btn-default btn2">Comment
                                </button>
                                <button type="button" onclick='manageEdit(<?= json_encode($value); ?>)'
                                        class="btn btn-default btn2">Edit
                                </button>
                                <a onclick="return confirm('ยืนยันการลบ')"
                                   href="/manager/match?delete_id=<?php echo $value['id']; ?>"
                                   class="btn btn-default btn3">Delete</a>
                                <!--                        <label id="labelUpload" data-id="-->
                                <?php //echo $value['id']; ?><!--" for="inputUpload" class="btn btn-outline-warning m-0">UpLoad</label>-->
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!--    Snap-->
<div class="modal fade" style="" id="ModalSnap" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel">Snap</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form id="snap-match" class="form-inline">
                            <input type="hidden" name="snap[type]" value="match">
                            <input type="hidden" name="snap[id]" value="">
                            <div class="form-group">
                                <label for="exampleInputName2">Width</label>
                                <input style="width: 150px;" required type="number" name="snap[width]" class="form-control" id="exampleInputName2" placeholder="Width" value="760">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail2">Height</label>
                                <input style="width: 150px;" required type="number" name="snap[height]" class="form-control" id="exampleInputEmail2" placeholder="Height" value="300">
                            </div>
                            <div class="form-group">
                                <button data-loading-text="Loading..." autocomplete="off"  type="submit" class="btn btn-danger btn-lg"><span class="oi oi-camera-slr"></span></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12" style="padding: 10%;">
                        <img onerror="this.src='/images/image-not-found.png';" id="image-snap-match" style="width: 100%;" src="/images/image-not-found.png">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a onerror="this.disabled=true;" id="a-snap-match" href="" download class="btn btn-danger"><span class="oi oi-data-transfer-download"></span> Download</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--    User Channel-->
<div class="modal fade" style="" id="ModalAddChannel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel">Channel</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="padding: 0px 5px;">
                    <div class="col">
                        <form class="form-inline" id="form-manageChannel" method="get">
                            <input type="hidden" name="channel[match_id]" value="">
                            <div class="form-group mb-2">
                                <label for="staticEmail2">Channel</label>
                                <select class="form-control" name="channel[channel_id]">
                                    <?php foreach ($channelAll as $key=>$value){ ?>
                                        <option value="<?=$value['id']?>"><?=$value['channel_name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group mb-2">
                                <label for="staticEmail2">Link</label>
                                <input style="width: 250px;" type="text" class="form-control" name="channel[link]" value="" placeholder="link">
                            </div>
                            <button type="submit" class="btn btn-primary mb-2">add</button>
                        </form>
                    </div>
                </div>
                <div class="row" style="margin-top: 10px;">
                    <div class="col">
                        <table class="table table-sm">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Path</th>
                                <th scope="col">Name</th>
                                <th scope="col">Link</th>
                                <th scope="col">Manage</th>
                            </tr>
                            </thead>
                            <tbody id="tbody-Channel">
                            <tr>
                                <th scope="row">1</th>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>@mdo</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!--                    <button type="submit" class="btn btn-primary">Save Change</button>-->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ModalAddComment" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Comment</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-manageComment">
                    <input type="hidden" name="comment[id]">
                    <input type="hidden" name="comment[match_id]">
                    <input type="hidden" name="match[link]" value="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
                    <!--                    <input type="hidden" name="comment[user_id]" value="-->
                    <? //= ((!empty($_SESSION['login']['id']))?$_SESSION['login']['id']:'')?><!--">-->

                    <div class="form-group">
                        <label class="col-sm-2 control-label">User Comment</label>
                        <div class="col-sm-10">
                            <select required name="comment[user_id]" class="form-control">
                                <?php foreach ($selectUser as $key => $value) { ?>
                                    <option value="<?= $value['id'] ?>"><?= $value['username'] ?></option>
                                <?php } ?>
                            </select>
                            <!--                        <input required name="review[user_review_id]" type="text" class="form-control" placeholder="Home Water Bill">-->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Team</label>
                        <div class="col-sm-10">
                            <select required name="comment[team]" class="form-control">
                                <option value="home">Home</option>
                                <option value="away">Away</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Text comment</label>
                        <div class="col-sm-10">
                            <textarea required name="comment[review_text]" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Save change</button>
                        </div>
                    </div>

                </form>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>Home</th>
                                <th>Away</th>
                            </tr>
                            </thead>
                            <tbody id="view-comment">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!--                <button type="button" class="btn btn-primary">Save changes</button>-->
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalAddReview" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Review</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-manageReview">
                    <input type="hidden" name="review[id]">
                    <input type="hidden" name="review[match_id]">
                    <input type="hidden" name="review[user_id]"
                           value="<?= ((!empty($_SESSION['login']['id'])) ? $_SESSION['login']['id'] : '') ?>">

                    <div class="form-group">
                        <label class="col-sm-3 control-label">User Review</label>
                        <div class="col-sm-9">
                            <select required name="review[user_review_id]" class="form-control">
                                <?php foreach ($selectReview as $key => $value) { ?>
                                    <option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                                <?php } ?>
                            </select>
                            <!--                        <input required name="review[user_review_id]" type="text" class="form-control" placeholder="Home Water Bill">-->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Team</label>
                        <div class="col-sm-9">
                            <select required name="review[team]" class="form-control">
                                <option value="home">Home</option>
                                <option value="away">Away</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Text Review</label>
                        <div class="col-sm-9">
                            <textarea required name="review[review_text]" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary">Save change</button>
                        </div>
                    </div>
                </form>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>Home</th>
                                <th>Away</th>
                            </tr>
                            </thead>
                            <tbody id="view-review">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!--                <button type="button" class="btn btn-primary">Save changes</button>-->
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--modal-inOpen-->
<div class="modal fade" style="" id="ModalAddGamble" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="form-manageGamble" method="get">
            <input value="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" name="gamble[link]" type="hidden" class="form-control" placeholder="link">
            <input type="hidden" name="search[league_id]"
                   value="<?= ((isset($_GET['search']['league_id'])) ? $_GET['search']['league_id'] : '') ?>">
            <input type="hidden" name="search[team_id]"
                   value="<?= ((isset($_GET['search']['team_id'])) ? $_GET['search']['team_id'] : '') ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Gamble</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="gamble[match_id]">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Home Water Bill</label>
                            <div class="col-sm-10">
                                <input required name="gamble[home_water_bill]" type="text" class="form-control"
                                       placeholder="Home Water Bill">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Handicap</label>
                            <div class="col-sm-10">
                                <input required name="gamble[handicap]" type="text" class="form-control"
                                       placeholder="Handicap">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Away Water Bill</label>
                            <div class="col-sm-10">
                                <input required name="gamble[away_water_bill]" type="text" class="form-control"
                                       placeholder="Away Water Bill">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save Change</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="ModalAddScore" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel">Score</h4>

            </div>
            <div class="modal-body">
                <form id="form-match-player" class="form-horizontal">
                    <!--                        <label class="sr-only" for="inlineFormInput">Player Name</label>-->

                    <input type="hidden" class="form-control" name="match_player[match_id]" value="">

                    <div class="form-group">
                        <!--                            <div class="form-check">-->

                        <label class="col-sm-2 control-label">เลือกฝั่ง</label>
                        <div class="col-sm-10">
                            <label class="radio-inline">
                                <input class="form-check-input" type="radio" name="match_player[team]"
                                       id="exampleRadios1" value="home" checked>
                                Home
                            </label>
                            <label class="radio-inline">
                                <input class="form-check-input" type="radio" name="match_player[team]"
                                       id="exampleRadios2" value="away">
                                Away
                            </label>
                            <!--                            </div>-->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ชื่อนักแตะ</label>
                        <div class="col-sm-10">
                            <input required class="form-control typehead pull-left"
                                   placeholder="ค้นหาชื่อนักแตะ" id="search-typehead" type="text"
                                   name="match_player[player_id]">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="row">
                                <div class="col-md-4">
                                    <input required min="0" style="width: 100%;" type="number" class="form-control"
                                           name="match_player[minute]" placeholder="นาที">
                                </div>
                                <div class="col-md-4">
                                    <input min="0" type="number" class="form-control"
                                           name="match_player[over_minute]" placeholder="ต่อ">
                                </div>
                                <div class="col-md-4">
                                    <select required required name="match_player[status]"
                                            class="form-control">
                                        <option value="goal">ทำประตู</option>
                                        <option value="freekick">ฟรีคิก(FreeKick)</option>
                                        <option value="penalty">จุดโทษ(Penalty)</option>
                                        <option value="Notpenalty">จุดโทษไม่เข้า(Penalty)</option>
                                        <option value="Owngoal">ทำเข้าประตูตัวเอง</option>
                                        <option value="overgoal">นอกเวลาทำประตู</option>
                                        <option value="overfreekick">นอกเวลาฟรีคิก</option>
                                        <option value="overpenalty">นอกเวลาจุดโทษ</option>
                                        <option value="Notoverpenalty">จุดโทษไม่เข้า</option>
                                        <option value="CYellow">ใบเหลือง</option>
                                        <option value="CRed">ใบแดง</option>
                                        <option value="CYellowRed">ใบเหลืองแดง</option>
                                        <option value="Out">เปลิ่ยนออก</option>
                                        <option value="In">เปลิ่ยนเข้า</option>
                                    </select>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i>
                                Add Score
                            </button>
                        </div>
                    </div>
                </form>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>ทีม</th>
                                <th>นาที</th>
                                <th>ชื่อ</th>
                                <th>ประเภท</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="match-player-list">

                            </tbody>
                        </table>
                    </div>
                </div>

                <!--                    <div class="row">-->
                <!--                        <div class="form-group col-6">-->
                <!--                            <label for="exampleInputEmail1">Home Full Time Score</label>-->
                <!--                            <input required name="score[home_end_time_score]" type="text" class="form-control" placeholder="Home Full Time Score">-->
                <!--                        </div>-->
                <!--                        <div class="form-group col-6">-->
                <!--                            <label for="exampleInputEmail1">Away Full Time Score</label>-->
                <!--                            <input required name="score[away_end_time_score]" type="text" class="form-control" placeholder="Away Full Time Score">-->
                <!--                        </div>-->
                <!--                    </div>-->
                <!--                    <div class="row">-->
                <!--                        <div class="form-group col-6">-->
                <!--                            <label for="exampleInputEmail1">Home Over Time Score</label>-->
                <!--                            <input name="score[home_over_time_score]" type="text" class="form-control" placeholder="Home Over Time Score">-->
                <!--                        </div>-->
                <!--                        <div class="form-group col-6">-->
                <!--                            <label for="exampleInputEmail1">Away Over Time Score</label>-->
                <!--                            <input name="score[away_over_time_score]" type="text" class="form-control" placeholder="Away Over Time Score">-->
                <!--                        </div>-->
                <!--                    </div>-->
            </div>
            <div class="modal-footer">
                <form id="form-manageScore" class="form-inline">
                    <input type="hidden" class="form-control" name="score[id]" value="">
                    <div class="col-md-6">
                        <div class="form-group pull-left">
                            <select required name="score[status]" class="form-control">
                                <option value="create">สร้าง</option>
                                <option value="fullTime">จบเกมส์</option>
                                <option value="penalty">จุดโทษ</option>
                                <option value="overTime">ต่อเวลา</option>
                                <option value="bet">เปิดราคา</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group pull-right">
                            <a href="<?= $_SERVER['REQUEST_URI'] ?>" type="button" class="btn btn-default">Close</a>
                            <button type="submit" class="btn btn-primary">Save Change</button>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalAddMatch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="form-manageMatch">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Add Match</h4>

                </div>
                <div class="modal-body">
                    <input type="hidden" name="match[id]">
                    <input type="hidden" name="match[link]" value="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Date Match</label>
                            <div class="col-md-9">
                                <input required id="dateSelect" name="match[date]" type="text" class="form-control"
                                       placeholder="Date">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Match 7m Id</label>
                            <div class="col-md-9">
                                <input name="match[match_7m_id]" id="input-match-7m-id" type="text" class="form-control"
                                       placeholder="Match 7m Id">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Time Match</label>
                            <div class="col-md-9">
                                <input required name="match[time]" type="text" class="form-control" placeholder="Time">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">League Name</label>
                            <div class="col-md-9">
                                <select required id="select-league" name="match[league_id]" class="form-control">
                                    <option value="">League Select</option>
                                    <?php foreach ($leagueAll as $key => $value) { ?>
                                        <option value="<?php echo $value['id'] ?>"><?php echo $value['name'] ?></option>
                                    <?php } ?>
                                </select>
                                <small id="emailHelp" class="form-text text-muted"></small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Team Home</label>
                            <div class="col-md-9">
                                <input id="input-Team-Home" class="form-control" placeholder="Team Home"
                                       name="match[team_home]">
                                <!--                        --><?php //foreach ($leagueTeam as $key=>$value){ ?>
                                <!--                            <select required disabled id="team-home--->
                                <? //= $key?><!--" name="match[team_home]" class="form-control team-home hide">-->
                                <!--                                <option value="">Team Select</option>-->
                                <!--                                --><?php //foreach ($value as $key=>$value){ ?>
                                <!--                                    <option value="--><? //= $value['id']?><!--">-->
                                <? //= $value['name_en']?><!--</option>-->
                                <!--                                --><?php //} ?>
                                <!--                            </select>-->
                                <!--                        --><?php //} ?>
                                <small id="error-team_home" class="form-text text-danger"></small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Team Away</label>
                            <div class="col-md-9">
                                <input id="input-Team-Away" class="form-control" placeholder="Team Away"
                                       name="match[team_away]">
                                <!--                        --><?php //foreach ($leagueTeam as $key=>$value){ ?>
                                <!--                            <select required disabled id="team-away--->
                                <? //= $key?><!--" name="match[team_away]" class="form-control team-away hide">-->
                                <!--                                <option value="">Team Select</option>-->
                                <!--                                --><?php //foreach ($value as $key=>$value){ ?>
                                <!--                                    <option value="--><? //= $value['id']?><!--">-->
                                <? //= $value['name_en']?><!--</option>-->
                                <!--                                --><?php //} ?>
                                <!--                            </select>-->
                                <!--                        --><?php //} ?>
                                <small id="error-team_away" class="form-text text-danger"></small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="addMatch" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add</button>
                </div>
            </div>
        </form>
    </div>
</div>

<style>
    .hide {
        display: none;
    }

    .ui-autocomplete {
        z-index: 1500;
    }
</style>
<script src="/typeahead/typeahead.jquery.js"></script>
<script>
    //    $(document).on("change","select#select-country", function (e) {
    //        $('option.option-country').hide();
    //        $('option.option-countryId-'+$(this).val()).show();
    //    });

    $(document).on("change", "select#select-country", function (e) {
        if ($(this).val() != "") {
            $.ajax({
                url: "/manager/getLeagueByCountry",
                data: {id: $(this).val()},
                method: "GET",
                dataType: "html"
            }).done(function (response) {
                $('#select-league').html(response);
            });
        } else {
//            $('#select-team_id').hide();
        }
    });

    $(document).on("change", "select#select-league", function (e) {
        if ($(this).val() != "") {
            $.ajax({
                url: "/manager/getTeamByLeague",
                data: {id: $(this).val()},
                method: "GET",
                dataType: "html"
            }).done(function (response) {
                $('#select-team_id').html(response);
            });
        } else {
            $('#select-team_id').hide();
        }
    });


    var cache = {};

    $("input#input-Team-Away").autocomplete({
        minLength: 1,
        source: function (request, response) {
            var term = request.term;
            if (term in cache) {
                response(cache[term]);
                return;
            }

            $.getJSON("/manager/readTeam", request, function (data, status, xhr) {
                cache[term] = data;
                response(data);
            });
        }
    });

    $("input#input-Team-Home").autocomplete({
        minLength: 1,
        source: function (request, response) {
            var term = request.term;
            if (term in cache) {
                response(cache[term]);
                return;
            }

            $.getJSON("/manager/readTeam", request, function (data, status, xhr) {
                cache[term] = data;
                response(data);
            });
        }
    });

    $("#search-typehead").autocomplete({
        minLength: 1,
        source: function (request, response) {
            var term = request.term;
            if (term in cache) {
                response(cache[term]);
                return;
            }

            $.getJSON("/manager/readplayer", request, function (data, status, xhr) {
                cache[term] = data;
                response(data);
            });
        }
    });

    function manageSnap(data) {
        $('form#snap-match').find('input[name="snap[id]"]').val(data.id);
        $('#image-snap-match').attr('src','/images/snap/match/'+data.id+'/snapmatch.png');
        $('#a-snap-match').attr('href','/images/snap/match/'+data.id+'/snapmatch.png');
        $('#ModalSnap').modal('show');
        setTimeout(function () {
            if($('#image-snap-match').attr('src')=="/images/image-not-found.png") {
                $('#a-snap-match').hide();
            }
        },500);
    }

    function removeMatchPlayer(id) {
        $.ajax({
            url: '/manager/addmathplayer?match_player_id=' + id,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                if (data['status'] == "true") {
                    $('tr#mathplayer-' + id).remove();
                }
            }
        });
    }

    function resetForm() {
        $('#form-manageMatch')[0].reset();
        $('#addMatch').removeClass("btn-warning").addClass("btn-primary").html("Add");
    }

    function manageComment(data) {
        console.log("manageComment");
        $('#form-manageComment').find('input[name="comment[match_id]"]').first().val(data.id);
        $.ajax({
            url: "/manager/getComment",
            data: {id: data.id},
            method: "GET",
            dataType: "json"
        }).done(function (response) {
            $('tbody#view-comment').html("");
            $('tbody#view-comment').html(response['html']);
            $('#ModalAddComment').modal('show');
        });
    }

    function manageReview(data) {
        $('#form-manageReview').find('input[name="review[match_id]"]').first().val(data.id);
        $.ajax({
            url: "/manager/getReview",
            data: {id: data.id},
            method: "GET",
            dataType: "json"
        }).done(function (response) {
            $('tbody#view-review').html("");
            $('tbody#view-review').html(response['html']);
            $('#ModalAddReview').modal('show');
        });
    }

    function manageEdit(data) {
        console.log(data);
        $('#form-manageMatch').find('input[name="match[match_7m_id]"]').first().val(data.match_7m_id);
        $('#form-manageMatch').find('input[name="match[id]"]').first().val(data.id);
        $('#form-manageMatch').find('input[name="match[date]"]').val(moment(data.time_match).format('DD/MM/YYYY'));
        $('#form-manageMatch').find('input[name="match[time]"]').val(moment(data.time_match).format('HH:mm'));
        $('#form-manageMatch').find('select[name="match[league_id]"]').first().val(data.league_id);

        $('select.team-home').attr('disabled', 'disabled');
        $('select.team-away').attr('disabled', 'disabled');
        $('select.team-home').addClass('hide');
        $('select.team-away').addClass('hide');

        if (data.league_id != "") {
            $('select#team-home-' + data.league_id).removeAttr('disabled');
            $('select#team-away-' + data.league_id).removeAttr('disabled');
            $('select#team-home-' + data.league_id).removeClass('hide');
            $('select#team-away-' + data.league_id).removeClass('hide');
        }

        $('#form-manageMatch').find('select#team-home-' + data.league_id).first().val(data.team_home);
        $('#form-manageMatch').find('select#team-away-' + data.league_id).first().val(data.team_away);
        console.log("td#Team-Home-" + data.id + " :: td#Team-Away-" + data.id);
        console.log($("td#Team-Home-" + data.id).html() + " :: " + $("td#Team-Away-" + data.id).html());
        $('#form-manageMatch').find('#input-Team-Home').first().val($("td#Team-Home-" + data.id).html());
        $('#form-manageMatch').find('#input-Team-Away').first().val($("td#Team-Away-" + data.id).html());

        $('#addMatch').removeClass("btn-primary").addClass("btn-warning").html("Edit");
        $('#ModalAddMatch').modal('show');
    }

    function manageGamble(data) {
//        console.log(data);
        $('#form-manageGamble').find('input[name="gamble[match_id]"]').first().val(data.id);
        $('#form-manageGamble').find('input[name="gamble[home_water_bill]"]').first().val(data.home_water_bill);
        $('#form-manageGamble').find('input[name="gamble[handicap]"]').first().val(data.handicap);
        $('#form-manageGamble').find('input[name="gamble[away_water_bill]"]').first().val(data.away_water_bill);
        $('#ModalAddGamble').modal('show');
    }

    function manageChannel(data) {
        $("input[name='channel[match_id]']").val(data.match_7m_id);
        $.ajax({
            url: '/manager/getChannel?match_id=' + data.match_7m_id,
            type: 'GET',
            dataType: 'html',
            success: function (data) {
                $('#tbody-Channel').html("");
                $('#tbody-Channel').html(data);
                $('#ModalAddChannel').modal('show');
            }
        });
    }

    function removeChannelMatch(match_id,id) {
        if(confirm('ยืนยันการลบ การถ่ายทอดสด')){
            $.ajax({
                url: '/manager/removeChannelMatch?id=' + id+'&match_id='+match_id,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    if (data['status']) {
                        $('#tbody-Channel').html(data['data']);
                    }
                }
            });
        }
    }

    function manageScore(data) {
        $('#form-match-player').find('input[name="match_player[match_id]"]').first().val(data.id);
        $('#form-manageScore').find('input[name="score[id]"]').first().val(data.id);
        $('#form-manageScore').find('input[name="score[home_end_time_score]"]').first().val(data.home_end_time_score);
        $('#form-manageScore').find('input[name="score[home_over_time_score]"]').first().val(data.home_over_time_score);
        $('#form-manageScore').find('input[name="score[away_end_time_score]"]').first().val(data.away_end_time_score);
        $('#form-manageScore').find('input[name="score[away_over_time_score]"]').first().val(data.away_over_time_score);
        $('#form-manageScore').find('select[name="score[status]"]').first().val(data.status);

        $.ajax({
            url: '/manager/getmathplayerbymath?id=' + data.id,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                if (data['status'] == "true") {
                    $('#match-player-list').html("");
                    $.each(data['data'], function (key, value) {
//                        console.log(typeof(value['over_minute']));
//                        console.log(value['over_minute']);
                        if (value['over_minute'] !== null && value['over_minute'] != 0) {
                            $('#match-player-list').append("<tr id='mathplayer-" + value['id'] + "'><td>" + value['team'] + "</td><td>" + value['minute'] + "(" + value['over_minute'] + ")</td><td>" + value['name'] + "</td><td>" + value['status'] + "</td><td><button onclick='removeMatchPlayer(" + value['id'] + ")' class='btn btn-outline-danger'><span class='oi oi-trash'></span></button></td></tr>");
                        } else {
                            $('#match-player-list').append("<tr id='mathplayer-" + value['id'] + "'><td>" + value['team'] + "</td><td>" + value['minute'] + "</td><td>" + value['name'] + "</td><td>" + value['status'] + "</td><td><button onclick='removeMatchPlayer(" + value['id'] + ")' class='btn btn-outline-danger'><span class='oi oi-trash'></span></button></td></tr>");
                        }
                    })
                    $('#ModalAddScore').modal('show');
                }
            }
        });
//        console.log(data);
    }

    $(document).on("submit", "form#snap-match", function (e) {
        e.preventDefault();
        var $btn = $(this).find('button').button('loading');
        $.ajax({
            url: '/manager/snapImage',
            type: 'GET',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data['status']) {
                    $('form#snap-match')[0].reset();
                    $('#image-snap-match').attr('src',data['data']);
                    $('#a-snap-match').show();
                    $btn.button('reset');
                }
            }
        });
    });

    $(document).on("submit", "form#form-manageChannel", function (e) {
        e.preventDefault();
        $.ajax({
            url: '/manager/addChannelMatch',
            type: 'GET',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data['status']) {
                    $('form#form-manageChannel')[0].reset();
                    $('#tbody-Channel').html(data['data']);
                }
            }
        });
    });

    $(document).on("submit", "form#form-match-player", function (e) {
        e.preventDefault();
        console.log($(this).serialize());
        $.ajax({
            url: '/manager/addmathplayer',
            type: 'GET',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data['status'] == "true") {
                    $('#match-player-list').html("");
                    $.each(data['data'], function (key, value) {
                        $('#match-player-list').append("<tr id='mathplayer-" + value['id'] + "'><td>" + value['team'] + "</td><td>" + value['minute'] + "</td><td>" + value['name'] + "</td><td>" + value['status'] + "</td><td><button onclick='removeMatchPlayer(" + value['id'] + ")' class='btn btn-outline-danger'><span class='oi oi-trash'></span></button></td></tr>")
                    });
                    $('#form-match-player').find('input[name="match_player[minute]"]').first().val("");
                    $('#form-match-player').find('input[name="match_player[player_id]"]').first().val("");
                }
            }
        });
    });

    $(document).on("click", "td.comment_match", function (e) {
        var id = $(this).attr('data-id');
        if (confirm("ยืนยันการ Comment นี้")) {
            $.ajax({
                url: "/manager/removeComment",
                data: {id: id},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                if (response['status']) {
                    $('tbody#view-comment').find("td[data-id='" + id + "']").remove();
                }
            });
        }
    });

    $(document).on("click", "td.review_match", function (e) {
        var id = $(this).attr('data-id');
        if (confirm("ยืนยันการ ลบ Review นี้")) {
            $.ajax({
                url: "/manager/removeReview",
                data: {id: id},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                if (response['status']) {
                    $('tbody#view-review').find("td[data-id='" + id + "']").remove();
                }
            });
        }
    });

    $(document).on("change", "select[name='match[team_away]']", function (e) {
//        console.log($('#form-manageMatch').find("select[name='match[team_away]']:not(:disabled)").val()+" :: "+$('#form-manageMatch').find("select[name='match[team_home]']:not(:disabled)").val());
        if ($("select[name='match[team_away]']:not(:disabled)").val() == $("select[name='match[team_home]']:not(:disabled)").val()) {
            $('#error-team_away').html('กรุณาเลือก ทีมใหม่');
            $('#addMatch').attr('disabled', 'disabled');
        } else {
            $('#error-team_away').html('');
            $('#error-team_home').html('');
            $('#addMatch').removeAttr('disabled');
        }
    });

    $(document).on("change", "select[name='match[team_home]']", function (e) {
//        console.log($('#form-manageMatch').find("select[name='match[team_away]']:not(:disabled)").val()+" :: "+$('#form-manageMatch').find("select[name='match[team_home]']:not(:disabled)").val());
        if ($("select[name='match[team_away]']:not(:disabled)").val() == $("select[name='match[team_home]']:not(:disabled)").val()) {
            $('#error-team_home').html('กรุณาเลือก ทีมใหม่');
            $('#addMatch').attr('disabled', 'disabled');
        } else {
            $('#error-team_away').html('');
            $('#error-team_home').html('');
            $('#addMatch').removeAttr('disabled');
        }
    });

    $(document).on("change", "select#select-league", function (e) {
        $('select.team-home').attr('disabled', 'disabled');
        $('select.team-away').attr('disabled', 'disabled');
        $('select.team-home').addClass('hide');
        $('select.team-away').addClass('hide');
        if ($(this).val() != "") {
            $('select#team-home-' + $(this).val()).removeAttr('disabled');
            $('select#team-away-' + $(this).val()).removeAttr('disabled');
            $('select#team-home-' + $(this).val()).removeClass('hide');
            $('select#team-away-' + $(this).val()).removeClass('hide');
        }
    });
    $("input#dateSelect").datepicker({dateFormat: 'dd/mm/yy'}).datepicker("setDate", new Date());
    $('input#timeSelect').timepicker({timeFormat: 'HH:mm', zindex: 1500});
</script>
<script src="/manager/js/highlightmark.js?<?php echo date("Y-m-d H:i:s");?>"></script>
<?php include 'layouts/footer.php'; ?>

<?php
/**
 * Created by PhpStorm.
 * User: EPOP
 * Date: 6/7/2017
 * Time: 12:22 PM
 */

header("content-type: application/json;charset=utf-8");
if (!empty($_FILES)) {
    $tempFile = $_FILES['file']['tmp_name'];
    $extension = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));
    $name_file = $_FILES['file']['name'];
    $change_name = "img_".rand(10000000,9999999999).".".$extension;
    $targetFile = dirname(__FILE__).'/img/'.$change_name; //rtrim($targetPath, '/') . '/libs/froala/img/'.$change_name;
    $dir = dirname(__FILE__) . '/img/';
    if (!is_dir($dir)) {
        mkdir($dir, 0777, true);
    }
    if(move_uploaded_file($tempFile, $targetFile)){
        echo json_encode(['link'=> '/libs/froala/img/'.$change_name]);
    }else {
        echo error_get_last();
    }

}else{
    echo 'not file';
}
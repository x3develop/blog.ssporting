<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 1/17/2019
 * Time: 9:53 AM
 */
require_once '../model/SitemapInfo.php';
$sitemapInfoObject = new SitemapInfo();
$objResult = $sitemapInfoObject->countPage();
$objResult = $objResult[0];
?>
<?php include 'layouts/header.php'; ?>

<div class="container">
    <div class="page-header title">
        <h3><a href="sitemap">SiteMap</a></h3>
        <h3><a href="reportSiteMap_info">SiteMapInfo</a></h3>
        <h3><a href="reportSiteMap_search">SiteMapSearch</a></h3>
        <h3 style="float: right;">SiteMapInfo</h3>
    </div>
    <div class="row">
        <table class="table table-hover">
            <tr>
                <td>name</td>
                <td>link</td>
                <td>generates</td>
            </tr>
            <tbody>
            <?php for ($i = ($objResult->maxPage - 1); $i >= 0; $i--) { ?>
                <tr>
                    <td>Set <?php echo $i ?></td>
                    <td><?php echo((file_exists("../sitemap/sitemap_info/infomap" . $i . ".xml") ? '<a target="_blank" href="../sitemap/sitemap_info/infomap' . $i . '.xml">view</a>' : '-')) ?></td>
                    <td><a href="../sitemap/loadSiteMap_info.php?set=<?php echo $i ?>">Set <?php echo $i ?></a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>


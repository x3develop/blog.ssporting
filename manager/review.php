<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/2/2017
 * Time: 22:46
 */
if (!isset($_SESSION)) {
    session_start();
}
include '_database.php';
if (isset($_GET['review'])) {
    if (!empty($_GET['review']['id'])) {
        $sql = "UPDATE `user_review` set `name`='" . $_GET['review']['name'] . "',`updated_at`='" . date("Y-m-d H:i:s") . "' WHERE id=" . $_GET['review']['id'];
    } else {
        $sql = "INSERT INTO `user_review` (`name`,`created_at`,`updated_at`) VALUES ('" . $_GET['review']['name'] . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
    }
    $sth = $dbh->prepare($sql)->execute();
    header("Location: /manager/review");
    exit;
}

if (isset($_GET['id'])) {
    $sth = $dbh->prepare("select * from user_review where id=" . $_GET['id']);
    $sth->execute();
    $review = $sth->fetch();
} elseif (isset($_GET['delete_id'])) {
    if(isset($_SESSION['login']) && !empty($_SESSION['login']['username']) && $_SESSION['login']['username']=="admin") {
        $sth = $dbh->prepare("delete from user_review where id=" . $_GET['delete_id']);
        $sth->execute();
    }
    header("Location: /manager/review");
    exit;
}
?>
<?php include 'layouts/header.php'; ?>
<?php
if (isset($_GET['search'])) {
    $sth = $dbh->prepare("select * from user_review WHERE `name` like '%" . $_GET['search']['name']) . "%'";
    $sth->execute();
    $userReviewAll = $sth->fetchAll();
} else {
    $sth = $dbh->prepare("select * from user_review");
    $sth->execute();
    $userReviewAll = $sth->fetchAll();
}

$webroot = $_SERVER['DOCUMENT_ROOT'];
?>
<div class="container-fluid">
    <div class="page-header title">
        <h3>Review</h3>
    </div>

    <div class="well well-sm">
        <form class="form-inline">
            <div class="form-group">
                <label>League Name</label>
                <input name="search[name]" type="text" class="form-control mb-2 mr-sm-2 mb-sm-0"
                       placeholder="Search Name">
                <button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
            </div>
            <div class="form-group">
                <input type="hidden" name="review[id]"
                       value="<?= ((!empty($review['id'])) ? $review['id'] : '') ?>">
                <label>League Name</label>
                <input required name="review[name]"
                       value="<?= ((!empty($review['name'])) ? $review['name'] : '') ?>"
                       type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" placeholder="Review Name TH">
                <button type="submit"
                        class="btn <?= ((!empty($review['id'])) ? 'btn-default' : 'btn-primary') ?>"><?= ((!empty($review['id'])) ? 'Edit' : 'Add') ?></button>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th class="text-center">Manage</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($userReviewAll as $key => $value) { ?>
                    <tr>
                        <td><?= ($key + 1) ?></td>
                        <td>
                            <img style="height: 50px;" id="review-<?php echo $value['id']; ?>"
                                 src="<?php echo((!empty($value['path'])) ? $value['path'] : '/images/avatar.png') ?>">
                        </td>
                        <td><?= $value['name'] ?></td>
                        <td>
                            <div class="btn-group-manage" style="width: 180px;">
                                <a href="/manager/review?id=<?php echo $value['id']; ?>"
                                   class="btn btn1 btn-default">Edit</a>
                                <a onclick="return confirm('ยืนยันการลบ')"
                                   href="/manager/review?delete_id=<?php echo $value['id']; ?>"
                                   class="btn btn2 btn-default">Delete</a>
                                <label id="labelUpload" data-id="<?php echo $value['id']; ?>" for="inputUpload"
                                       class="btn btn3 btn-default">UpLoad</label>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<form style="display: none;" id="formUpload" method="post" enctype="multipart/form-data">
    <input type="text" name="id">
    <input id="inputUpload" type="file" name="Filedata[]" multiple="multiple" accept="image/*"><br>
</form>

<script>
    $(document).on('click', "#labelUpload", function (e) {
        $('input[name=id]').val($(this).attr('data-id'));
    });

    $(document).on('change', "#inputUpload", function (e) {
        $("#formUpload").submit();
    });

    $(document).on('submit', "#formUpload", function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: "/manager/uploadFileReview",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                response = jQuery.parseJSON(response);
                $('img#review-' + $('#formUpload').find('input[name=id]').val()).attr('src', response + '#' + Math.floor((Math.random() * 1000) + 1));
            },
            error: function (data) {
                console.log("error");
                console.log(data);
            }
        });
    });
</script>

<?php include 'layouts/footer.php'; ?>

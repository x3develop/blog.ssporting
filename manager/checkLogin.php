<?php
/**
 * Created by PhpStorm.
 * User: E-POP
 * Date: 31/10/2560
 * Time: 2:00
 */
if (!isset($_SESSION)) {
    session_start();
}
include '_config.php';
include '_database.php';
if(isset($_GET['login'])) {

    $dateNow = new DateTime();
    $dateLast = new DateTime();
    $dateLastBet = new DateTime();

    $email = ((!empty($_GET['login']['email'])) ? $_GET['login']['email'] : null);
    $password = ((!empty($_GET['login']['password'])) ? $_GET['login']['password'] : null);
    $sth = $dbh->prepare("select * from user where email='" . $email . "' or username='".$email."'");
    $sth->execute();
    $user = $sth->fetch();

    if($user['password']==md5($password)){

        $sth = $dbh->prepare("select * from user where email='" . $email . "' or username='".$email."'");
        $sth->execute();
        $user = $sth->fetch();
        $_SESSION['login'] = [
            'id' => $user['id'],
            'username' => $user['username'],
            'email' => $user['email'],
            'coin' => $user['coin'],
            'gold' => $user['gold'],
            'path' => $user['path'],
            'coupon' => $user['coupon'],
            'combo_login' => $user['combo_login'],
            'lv' => $user['lv'],
            'star' => $user['star'],
            'modal_gold' =>false,
            'coin_daily' =>false,
            'bet'=>null
        ];

        //calculate Bet
        $sth = $dbh->prepare("select min(bet.created_at) as maxDate from bet where status='bet' and user_id=".$user['id']);
        $sth->execute();
        $betLast = $sth->fetch();
        if(!empty($betLast['maxDate'])) {
            $dateLastBet->setTimestamp(strtotime($betLast['maxDate']));
            $betDateStart = $dateLastBet->format('Y-m-d');
            $sth = $dbh->prepare("select `match`.`status` as match_status,`match`.`status` as match_status,bet.`status`,(`match`.home_end_time_score+gamble.handicap) as homeScore,`match`.away_end_time_score,
CASE
    WHEN (`match`.home_end_time_score+gamble.handicap) > `match`.away_end_time_score and bet.team='home' THEN 'win'
    WHEN (`match`.home_end_time_score+gamble.handicap) > `match`.away_end_time_score and bet.team='away' THEN 'lose'
    WHEN (`match`.home_end_time_score+gamble.handicap) < `match`.away_end_time_score and bet.team='away' THEN 'win'
    WHEN (`match`.home_end_time_score+gamble.handicap) < `match`.away_end_time_score and bet.team='home' THEN 'lose'
    ELSE 'equal'
END as results
,tameHome.name_en,tameHome.path,tameAway.name_en,tameAway.path,bet.team,bet.`status`,bet.use_bet_coin,gamble.home_water_bill,gamble.away_water_bill,bet.id,bet.created_at from bet 
left join `match` on `match`.id=bet.match_id
left join team as tameHome on tameHome.id=`match`.team_home
left join team as tameAway on tameAway.id=`match`.team_away
left join gamble on gamble.match_id=`match`.id
where bet.created_at>='" . $betDateStart . "' and bet.user_id=" . $user['id'] . " order by bet.created_at ASC");
            $sth->execute();
            $resultsBet = $sth->fetchAll();

            $betCount = 0;
            $betWin = 0;
            $betLose = 0;
            $betEqual = 0;
            $dateSelect = "";
            foreach ($resultsBet as $key => $value) {
                if($value['match_status']!="create") {
                    $date = explode(' ', $value['created_at']);
                    if ($dateSelect != $date[0]) {
                        if ($betCount != 0 && $betCount >= 3 && $betCount==($betWin+$betLose+$betEqual)) {
                            if ($betWin > ($betCount / 2)) {
                                if ($_SESSION['login']['star'] == 4) {
                                    $user_id = $_SESSION['login']['id'];
                                    $befor_gold = $_SESSION['login']['gold'];
                                    $befor_coin = $_SESSION['login']['coin'];
                                    $befor_coupon = $_SESSION['login']['coupon'];
                                    $befor_lv = $_SESSION['login']['lv'];
                                    $befor_star = $_SESSION['login']['star'];
                                    $income_derived = 'lv';
                                    $income_type = 'lv';
                                    $income_format = 'plus';
                                    $after_gold = $befor_gold;
                                    $after_coin = $befor_coin;
                                    $after_coupon = $befor_coupon;
                                    $after_lv = $user['lv'] + 1;
                                    $after_star = 0;
                                    $comment = "เพิ่ม Star จากทากทายผล ทั้งวัน ถูกถึง 50% เเละ ได้ อัพเลเวล จากสะสม star ครบ 5 ดวง";
                                    $sql = "INSERT INTO `log_income` (`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                                    $sql .= " VALUES (" . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                                    $sth = $dbh->prepare($sql)->execute();
                                    if ($sth) {
                                        $_SESSION['login']['star'] = 0;
                                        $_SESSION['login']['lv'] = $user['lv'] + 1;
                                    }
                                } else {
                                    $user_id = $_SESSION['login']['id'];
                                    $befor_gold = $_SESSION['login']['gold'];
                                    $befor_coin = $_SESSION['login']['coin'];
                                    $befor_coupon = $_SESSION['login']['coupon'];
                                    $befor_lv = $_SESSION['login']['lv'];
                                    $befor_star = $_SESSION['login']['star'];
                                    $income_derived = 'lv';
                                    $income_type = 'star';
                                    $income_format = 'plus';
                                    $after_gold = $befor_gold;
                                    $after_coin = $befor_coin;
                                    $after_coupon = $befor_coupon;
                                    $after_lv = $befor_lv;
                                    $after_star = $_SESSION['login']['star'] + 1;
                                    $comment = "เพิ่ม Star จากทากทายผล ทั้งวัน ถูกถึง 50% ";
                                    $sql = "INSERT INTO `log_income` (`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                                    $sql .= " VALUES (" . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                                    $sth = $dbh->prepare($sql)->execute();
                                    if ($sth) {
                                        $_SESSION['login']['star'] = $_SESSION['login']['star'] + 1;
                                    }
                                }
                            } elseif ($betWin < ($betCount / 2)) {
                                if ($_SESSION['login']['star'] > 0) {
                                    $user_id = $_SESSION['login']['id'];
                                    $befor_gold = $_SESSION['login']['gold'];
                                    $befor_coin = $_SESSION['login']['coin'];
                                    $befor_coupon = $_SESSION['login']['coupon'];
                                    $befor_lv = $_SESSION['login']['lv'];
                                    $befor_star = $_SESSION['login']['star'];
                                    $income_derived = 'lv';
                                    $income_type = 'star';
                                    $income_format = 'minus';
                                    $after_gold = $befor_gold;
                                    $after_coin = $befor_coin;
                                    $after_coupon = $befor_coupon;
                                    $after_lv = $befor_lv;
                                    $after_star = (intval($_SESSION['login']['star']) - 1);
                                    $comment = "ลด Star จากทากทายผล ทั้งวัน ถูกไม่ถึง 50% ของการทายผลทั้งหมด";
                                    $sql = "INSERT INTO `log_income` (`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                                    $sql .= " VALUES (" . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                                    $sth = $dbh->prepare($sql)->execute();
                                    if ($sth) {
                                        $_SESSION['login']['star'] = $_SESSION['login']['star'] - 1;
                                    }
                                }
                            } else {
//                        echo "<br>----------------equal-------------------<br>";
                            }
                        }
                        $dateSelect = $date[0];
                        $betCount = 0;
                        $betWin = 0;
                        $betLose = 0;
                        $betEqual = 0;
                    }

                    if ($value['results'] == "win") {
                        if($value['status']=="bet") {
                            $user_id = $_SESSION['login']['id'];
                            $befor_gold = $_SESSION['login']['gold'];
                            $befor_coin = $_SESSION['login']['coin'];
                            $befor_coupon = $_SESSION['login']['coupon'];
                            $befor_lv = $_SESSION['login']['lv'];
                            $befor_star = $_SESSION['login']['star'];
                            $income_derived = 'bet';
                            $income_type = 'gold';
                            $income_format = 'plus';
                            if ($value['team'] == "home") {
                                $after_gold = (intval($befor_gold) + intval($value['use_bet_coin'] * $value['home_water_bill']));
                            } else if ($value['team'] == "away") {
                                $after_gold = (intval($befor_gold) + intval($value['use_bet_coin'] * $value['away_water_bill']));
                            }
                            $after_coin = $befor_coin;
                            $after_coupon = $befor_coupon;
                            $after_lv = $befor_lv;
                            $after_star = $befor_star;
                            $comment = "กดรับ gold จากการ ทายผลฟุตบอล ถูก";
                            $sql = "INSERT INTO `log_income` (`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                            $sql .= " VALUES (" . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                            $sth = $dbh->prepare($sql)->execute();
                            if ($sth) {
                                $_SESSION['login']['gold'] = $after_gold;
                            }
                            $sql = "update `bet` set status='cheap' where id=" . $value['id'];
                            $sth = $dbh->prepare($sql)->execute();
                        }
                        $betWin++;
                    } else if ($value['results'] == "lose") {
                        if($value['status']=="bet") {
                            $sql = "update `bet` set status='wrong' where id=" . $value['id'];
                            $sth = $dbh->prepare($sql)->execute();
                        }
                        $betLose++;
                    } else if ($value['results'] == "equal") {
                        if($value['status']=="bet") {
                            $sql = "update `bet` set status='always' where id=" . $value['id'];
                            $sth = $dbh->prepare($sql)->execute();
                        }
                        $betEqual++;
                    }
                }
                $betCount++;
            }
            if ($betCount != 0 && $betCount >= 3 && $betCount==($betWin+$betLose+$betEqual)) {
                if ($betWin > ($betCount / 2)) {
                    if ($_SESSION['login']['star'] == 4) {
                        $user_id = $_SESSION['login']['id'];
                        $befor_gold = $_SESSION['login']['gold'];
                        $befor_coin = $_SESSION['login']['coin'];
                        $befor_coupon = $_SESSION['login']['coupon'];
                        $befor_lv = $_SESSION['login']['lv'];
                        $befor_star = $_SESSION['login']['star'];
                        $income_derived = 'lv';
                        $income_type = 'lv';
                        $income_format = 'plus';
                        $after_gold = $befor_gold;
                        $after_coin = $befor_coin;
                        $after_coupon = $befor_coupon;
                        $after_lv = $user['lv'] + 1;
                        $after_star = 0;
                        $comment = "เพิ่ม Star จากทากทายผล ทั้งวัน ถูกถึง 50% เเละ ได้ อัพเลเวล จากสะสม star ครบ 5 ดวง";
                        $sql = "INSERT INTO `log_income` (`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                        $sql .= " VALUES (" . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                        $sth = $dbh->prepare($sql)->execute();
                        if ($sth) {
                            $_SESSION['login']['star'] = 0;
                            $_SESSION['login']['lv'] = $user['lv'] + 1;
                        }
                    } else {
                        $user_id = $_SESSION['login']['id'];
                        $befor_gold = $_SESSION['login']['gold'];
                        $befor_coin = $_SESSION['login']['coin'];
                        $befor_coupon = $_SESSION['login']['coupon'];
                        $befor_lv = $_SESSION['login']['lv'];
                        $befor_star = $_SESSION['login']['star'];
                        $income_derived = 'lv';
                        $income_type = 'star';
                        $income_format = 'plus';
                        $after_gold = $befor_gold;
                        $after_coin = $befor_coin;
                        $after_coupon = $befor_coupon;
                        $after_lv = $befor_lv;
                        $after_star = $_SESSION['login']['star'] + 1;
                        $comment = "เพิ่ม Star จากทากทายผล ทั้งวัน ถูกถึง 50% ";
                        $sql = "INSERT INTO `log_income` (`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                        $sql .= " VALUES (" . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                        $sth = $dbh->prepare($sql)->execute();
                        if ($sth) {
                            $_SESSION['login']['star'] = $_SESSION['login']['star'] + 1;
                        }
                    }
                } elseif ($betWin < ($betCount / 2)) {
                    if ($_SESSION['login']['star'] > 0) {
                        $user_id = $_SESSION['login']['id'];
                        $befor_gold = $_SESSION['login']['gold'];
                        $befor_coin = $_SESSION['login']['coin'];
                        $befor_coupon = $_SESSION['login']['coupon'];
                        $befor_lv = $_SESSION['login']['lv'];
                        $befor_star = $_SESSION['login']['star'];
                        $income_derived = 'lv';
                        $income_type = 'star';
                        $income_format = 'minus';
                        $after_gold = $befor_gold;
                        $after_coin = $befor_coin;
                        $after_coupon = $befor_coupon;
                        $after_lv = $befor_lv;
                        $after_star = (intval($_SESSION['login']['star']) - 1);
                        $comment = "ลด Star จากทากทายผล ทั้งวัน ถูกไม่ถึง 50% ของการทายผลทั้งหมด";
                        $sql = "INSERT INTO `log_income` (`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                        $sql .= " VALUES (" . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                        $sth = $dbh->prepare($sql)->execute();
                        if ($sth) {
                            $_SESSION['login']['star'] = $_SESSION['login']['star'] - 1;
                        }
                    }
                } else {
//                        echo "<br>----------------equal-------------------<br>";
                }
            }
        }
//        var_dump($_SESSION['login']);
//        exit;
        //Bet
        $sth = $dbh->prepare("select MAX(bet.created_at) as maxDate from bet where user_id=".$user['id']);
        $sth->execute();
        $betMax = $sth->fetch();
        $dateLastBet->setTimestamp(strtotime($betMax['maxDate']));
        $betDateStart=$dateLastBet->format('Y-m-d');
        $dateLastBet->modify('-1 weeks');
        $betDateEnd=$dateLastBet->format('Y-m-d');
        $sth = $dbh->prepare("select tameHome.name_en as home_name_en,`match`.team_home,`match`.home_end_time_score,gamble.home_water_bill,gamble.handicap,tameAway.name_en as away_name_en,`match`.team_away,`match`.away_end_time_score,gamble.away_water_bill,bet.team,bet.`status`,bet.created_at,bet.use_bet_coin from bet 
left join `gamble` on `gamble`.match_id=bet.match_id
left join `match` on `match`.id=bet.match_id
left join team as tameHome on tameHome.id=`match`.team_home
left join team as tameAway on tameAway.id=`match`.team_away where bet.created_at > '".$betDateEnd."' and user_id=".$user['id']." order by bet.created_at DESC");

        $sth->execute();
        $bet = $sth->fetchAll();
        if(!empty($bet)){
            $_SESSION['login']['bet']=$bet;
        }

        $dateLast->setTimestamp(strtotime($user['last_login']));
        $dateNow->setTime(0,0,0);
        $dateLast->setTime(0,0,0);
        $dayLogin = $dateNow->diff($dateLast)->days;

        if($dayLogin==1){
            $sql="update `user` set coupon=5 where id=".$user['id'];
            $sth = $dbh->prepare($sql)->execute();
            $_SESSION['login']['coupon']=5;
            $sql="update `user` set combo_login=".($user['combo_login']+1)." where id=".$user['id'];
            $sth = $dbh->prepare($sql)->execute();
            $_SESSION['login']['combo_login']=($user['combo_login']+1);
//            $combo_login=$user['combo_login']+1;
//            $_SESSION['login']['modal_gold']=true;
        }else if($dayLogin>1){
            $sql="update `user` set coupon=5 where id=".$user['id'];
            $sth = $dbh->prepare($sql)->execute();
            $_SESSION['login']['coupon']=5;
            $sql="update `user` set combo_login=0 where id=".$user['id'];
            $sth = $dbh->prepare($sql)->execute();
            $_SESSION['login']['combo_login']=0;
            $combo_login=0;
//            $_SESSION['login']['modal_gold']=true;
        }

        $dateNow->format('Y-m-d');
        $sth = $dbh->prepare("select id from log_income where income_derived='daily' and user_id=".$user['id']." and date_record='".$dateNow->format('Y-m-d')."'");
        $sth->execute();
        $log_income = $sth->fetch();

        if(!empty($log_income)) {
            $_SESSION['login']['coin_daily'] = true;
            $_SESSION['login']['modal_gold']=false;
        }else{
            $_SESSION['login']['modal_gold']=true;
        }

        $sql="update `user` set star=".$_SESSION['login']['star'].",lv=".$_SESSION['login']['lv'].",gold=".$_SESSION['login']['gold'].",combo_login=".$_SESSION['login']['combo_login'].",last_login='".$dateNow->format('Y-m-d H:i:s')."' where id=".$user['id'];
        $sth = $dbh->prepare($sql)->execute();

//        echo $dateNow->format('Y-m-d H:i:s')."<br>";
//        echo $dateLast->format('Y-m-d H:i:s')."<br>";
//        var_dump($_SESSION['login']);
//        exit;
        if(isset($_GET['login']['link'])){
            header('Location: '.$_GET['login']['link']);
        }else {
            header('Location: /manager/index');
        }
    }else{
        header('Location: /manager/index#loginfalse');
    }
}
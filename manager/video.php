<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 3/13/2018
 * Time: 10:10
 */
if (!isset($_SESSION)) {
    session_start();
}
include '_database.php';
if (isset($_REQUEST['VideoHighlight'])) {
    if($_REQUEST['VideoHighlight']['video_id']){
        $sql="UPDATE `video_highlight` set `title`=?,`desc`=?,`content`=?,`videosource`='".$_REQUEST['VideoHighlight']['videosource']."',`videotype`='".$_REQUEST['VideoHighlight']['videotype']."',`videostyle`='".$_REQUEST['VideoHighlight']['videostyle']."',`video_tag`='".$_REQUEST['VideoHighlight']['video_tag']."',`category`='".$_REQUEST['VideoHighlight']['category']."',`pathFile`='".$_REQUEST['VideoHighlight']['pathFile']."',`pin`=".(!empty($_REQUEST['VideoHighlight']['pin'])?$_REQUEST['VideoHighlight']['pin']:'null')." WHERE video_id=".$_REQUEST['VideoHighlight']['video_id'];
    }else{
        $sql = "INSERT INTO `video_highlight` (`title`,`desc`,`content`,`mid`,`gid`,`hid`,`leagueId`,`competitionId`,`videosource`,`videotype`,`videostyle`,`video_tag`,`category`,`pathFile`,`create_datetime`,`pin`)";
        $sql.=" VALUES (?,?,?,0,0,0,0,0,'".$_REQUEST['VideoHighlight']['videosource']."','".$_REQUEST['VideoHighlight']['videotype']."','".$_REQUEST['VideoHighlight']['videostyle']."','".$_REQUEST['VideoHighlight']['video_tag']."','".$_REQUEST['VideoHighlight']['category']."','".$_REQUEST['VideoHighlight']['pathFile']."','" . date("Y-m-d H:i:s") . "',".(!empty($_REQUEST['VideoHighlight']['pin'])?$_REQUEST['VideoHighlight']['pin']:'null').")";
    }
    $sth = $dbh->prepare($sql)->execute([$_REQUEST['VideoHighlight']['title'],$_REQUEST['VideoHighlight']['desc'],$_REQUEST['VideoHighlight']['content']]);
    if(isset($_REQUEST['VideoHighlight']['link'])){
        header("Location: ".$_REQUEST['VideoHighlight']['link']);
    }else {
        header("Location: /manager/video");
    }
    exit;
} elseif ($_REQUEST['edit_id']) {
    $sth = $dbh->prepare("select * from video_highlight where video_id=?");
    $sth->execute([$_REQUEST['edit_id']]);
    $dataVideoHighlight = $sth->fetch();
//    header("Location: /manager/video");
//    exit;
} elseif ($_REQUEST['delete_id']) {
    if(isset($_SESSION['login']) && !empty($_SESSION['login']['username']) && $_SESSION['login']['username']=="admin") {
        $sth = $dbh->prepare("delete from video_highlight where video_id=" . $_REQUEST['delete_id']);
        $sth->execute();
    }
    header("Location: /manager/video");
    exit;
}

//------------------page------------------
$sth = $dbh->prepare("select count(*) as total from video_highlight");
$sth->execute();
$dataVideoHighlightList = $sth->fetch();
$pageTotal = ceil((int) $dataVideoHighlightList["total"] / (int) 30);

$page=(isset($_REQUEST['page'])?$_REQUEST['page']:1);
if (isset($_GET['search'])) {
    $sth = $dbh->prepare("select * from video_highlight WHERE `title` like '%" . $_GET['search']['name'] . "%' order by video_id DESC limit 30");
    $sth->execute();
    $dataVideoHighlightList = $sth->fetchAll();
} else {
    $sth = $dbh->prepare("select * from video_highlight order by video_id DESC limit 30 offset ".(($page-1)*30));
    $sth->execute();
    $dataVideoHighlightList = $sth->fetchAll();
}

//var_dump(count($dataVideoHighlight));
?>
<?php include 'layouts/header.php'; ?>
<div class="container-fluid">

    <div class="page-header title">
        <h3>Manage Video</h3>
        <button type="button" class="btn btn-default" onclick="addVideo();">
            <i class="fa fa-plus" aria-hidden="true"></i> Add
        </button>
        <a href="/load/gameNew?type=video" class="btn btn-default"><i class="fa fa-check" aria-hidden="true"></i> Generates</a>
        <form class="form-inline pull-right">
            <label class="sr-only" for="inlineFormInput">Title</label>
            <input name="search[name]" type="text" class="form-control mb-2 mr-sm-2 mb-sm-0"
                   placeholder="Search Title">
            <button type="submit" class="btn btn-default btn-outline-info"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
        </form>
    </div>

    <div class="row">
        <nav aria-label="Page navigation">
            <ul class="pagination pagination-sm" style="margin: 0px;">
                <?php if($page>1){ ?>
                <li>
                    <a href="/manager/video?page=<?=$page-1?>" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <?php } ?>
                <?php for ($i = 1; $i <= $pageTotal; $i++) { ?>
                    <?php if($i==$page){ ?>
                        <li class="active"><a href="/manager/video?page=<?=$i?>"><?=$i?></a></li>
                    <?php }else{ ?>
                        <li class=""><a href="/manager/video?page=<?=$i?>"><?=$i?></a></li>
                    <?php } ?>
                <?php } ?>
                <?php if($page!=$pageTotal){ ?>
                <li>
                    <a href="/manager/video?page=<?=$pageTotal-1?>" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
                <?php } ?>
            </ul>
        </nav>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Date</th>
                    <th style="text-align: center;">รูป</th>
                    <th style="text-align: center;">ลีก</th>
                    <th>หัวข้อ</th>
                    <th>ลิ้ง</th>
                    <!--                <th scope="col">รายละเอียด</th>-->
                    <th style="text-align: center;">ประเถท</th>
                    <th style="width: 6%;text-align: center;">หมวดหมู่</th>
                    <th style="text-align: center;">เเท็ค</th>
                    <th style="text-align: center;">ความสำคัญ</th>
                    <th style="text-align: center;">จัดการ</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($dataVideoHighlightList as $key => $value) { ?>
                    <tr>
                        <th><p style="font-size: 12px; width: 100px; margin: 0;"><?= date("Y-m-d H:i:s",strtotime($value['create_datetime']." +7 hours")); ?></p></th>
                        <td><img id="video_thumbnail_image_<?= $value['video_id'] ?>" style="width: 100px;"
                                 src="<?= ((!empty($value['thumbnail'])) ? $value['thumbnail'] : '/images/image-not-found.png') ?>">
                        </td>
                        <td style="text-align: center;"><?= $value['category'] ?></td>
                        <td><?= $value['title'] ?></td>
                        <td><?= $value['content'] ?></td>
                        <!--                <td>--><? //=$value['desc']?><!--</td>-->
                        <td><?= $value['videosource'] ?></td>
                        <td style="text-align: center;"><?= $value['videotype'] ?></td>
                        <td><?= $value['video_tag'] ?></td>
                        <td style="text-align: center;"><?= ((!empty($value['pin']))?(11-$value['pin']):'-'); ?></td>
                        <td>
                            <div class="btn-group-manage" aria-label="...">
                                <label id="labelUpload" data-videoId="<?= $value['video_id'] ?>" for="inputUpload"
                                       class="btn btn1 btn-default" title="อัพรูปภาพ">
                                    <i class="fa fa-upload" aria-hidden="true"></i>
                                </label>

                                <a href="/manager/video?edit_id=<?= $value['video_id'] ?>"
                                   class="btn btn2 btn-default" title="แก้ไข">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                                <a href="/manager/video?delete_id=<?= $value['video_id'] ?>"
                                   class="btn btn3 btn-default" title="ลบ">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade <?php echo((!empty($dataVideoHighlight["video_id"])) ? ' in show' : '') ?>"
         style="<?php echo((!empty($dataVideoHighlight["video_id"])) ? 'display: block;' : '') ?>" id="exampleModal"
         tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form>
                <input type="hidden" name="VideoHighlight[link]" value="<?php echo $_SERVER['HTTP_REFERER']; ?>">
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="/manager/video" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </a>
                        <h4 class="modal-title" id="exampleModalLabel">Video</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                        <input type="hidden" name="VideoHighlight[video_id]"
                               value="<?= ((!empty($dataVideoHighlight['video_id'])) ? $dataVideoHighlight['video_id'] : '') ?>">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">ชื่อ</label>
                            <div class="col-sm-9">
                                <input required
                                       value="<?= ((!empty($dataVideoHighlight['title'])) ? $dataVideoHighlight['title'] : '') ?>"
                                       name="VideoHighlight[title]" type="text" class="form-control"
                                       placeholder="Title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">ลิ้ง</label>
                            <div class="col-sm-9">
                                <input required
                                       value="<?= ((!empty($dataVideoHighlight['content'])) ? $dataVideoHighlight['content'] : '') ?>"
                                       name="VideoHighlight[content]" type="text" class="form-control"
                                       placeholder="Content">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">ประเถท</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="VideoHighlight[videosource]">
                                    <option <?php echo((!empty($dataVideoHighlight["videosource"]) and $dataVideoHighlight["videosource"] == "streamable") ? 'selected' : '') ?>
                                            Videovalue="archive-org">streamable
                                    </option>
                                    <option <?php echo((!empty($dataVideoHighlight["videosource"]) and $dataVideoHighlight["videosource"] == "ok-ru") ? 'selected' : '') ?>
                                            value="ok-ru">ok.ru
                                    </option>
                                    <option <?php echo((!empty($dataVideoHighlight["videosource"]) and $dataVideoHighlight["videosource"] == "archive-org") ? 'selected' : '') ?>
                                            value="archive-org">archive.org
                                    </option>
                                    <option <?php echo((!empty($dataVideoHighlight["videosource"]) and $dataVideoHighlight["videosource"] == "facebook") ? 'selected' : '') ?>
                                            value="facebook">facebook
                                    </option>
                                    <option <?php echo((!empty($dataVideoHighlight["videosource"]) and $dataVideoHighlight["videosource"] == "twitter") ? 'selected' : '') ?>
                                            value="twitter">twitter
                                    </option>
                                    <option <?php echo((!empty($dataVideoHighlight["videosource"]) and $dataVideoHighlight["videosource"] == "youtube") ? 'selected' : '') ?>
                                            value="youtube">youtube
                                    </option>
                                    <option <?php echo((!empty($dataVideoHighlight["videosource"]) and $dataVideoHighlight["videosource"] == "dailymotion") ? 'selected' : '') ?>
                                            value="dailymotion">dailymotion
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">หมวดหมู่</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="VideoHighlight[videotype]">
                                    <option <?php echo((!empty($dataVideoHighlight["videotype"]) and $dataVideoHighlight["videotype"] == "highlight") ? 'selected' : '') ?>
                                            value="highlight">highlight
                                    </option>
                                    <option <?php echo((!empty($dataVideoHighlight["videotype"]) and $dataVideoHighlight["videotype"] == "general") ? 'selected' : '') ?>
                                            value="general">general
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">สไตล์</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="VideoHighlight[videostyle]">
                                    <option <?php echo((!empty($dataVideoHighlight["videostyle"]) and $dataVideoHighlight["videostyle"] == "normal") ? 'selected' : '') ?>
                                            value="normal">normal
                                    </option>
                                    <option <?php echo((!empty($dataVideoHighlight["videostyle"]) and $dataVideoHighlight["videostyle"] == "goal") ? 'selected' : '') ?>
                                            value="goal">goal
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">ลีกส์</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="VideoHighlight[category]"
                                        id="VideoHighlight_category">
                                    <option <?php echo((!empty($dataVideoHighlight["category"]) and $dataVideoHighlight["category"] == "other") ? 'selected' : '') ?>
                                            value="other">other
                                    </option>
                                    <option <?php echo((!empty($dataVideoHighlight["category"]) and $dataVideoHighlight["category"] == "europa") ? 'selected' : '') ?>
                                            value="europa">europa
                                    </option>
                                    <option <?php echo((!empty($dataVideoHighlight["category"]) and $dataVideoHighlight["category"] == "ucl") ? 'selected' : '') ?>
                                            value="ucl">ucl
                                    </option>
                                    <option <?php echo((!empty($dataVideoHighlight["category"]) and $dataVideoHighlight["category"] == "league 1") ? 'selected' : '') ?>
                                            value="league 1">league 1
                                    </option>
                                    <option <?php echo((!empty($dataVideoHighlight["category"]) and $dataVideoHighlight["category"] == "serie a") ? 'selected' : '') ?>
                                            value="serie a">serie a
                                    </option>
                                    <option <?php echo((!empty($dataVideoHighlight["category"]) and $dataVideoHighlight["category"] == "bundesliga") ? 'selected' : '') ?>
                                            value="bundesliga">bundesliga
                                    </option>
                                    <option <?php echo((!empty($dataVideoHighlight["category"]) and $dataVideoHighlight["category"] == "la liga") ? 'selected' : '') ?>
                                            value="la liga">la liga
                                    </option>
                                    <option <?php echo((!empty($dataVideoHighlight["category"]) and $dataVideoHighlight["category"] == "premier league") ? 'selected' : '') ?>
                                            value="premier league">premier league
                                    </option>
                                    <option <?php echo((!empty($dataVideoHighlight["category"]) and $dataVideoHighlight["category"] == "thai") ? 'selected' : '') ?>
                                            value="thai">thai
                                    </option>
                                    <option <?php echo((!empty($dataVideoHighlight["category"]) and $dataVideoHighlight["category"] == "international") ? 'selected' : '') ?>
                                            value="international">international
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">เเท็ค</label>
                            <div class="col-sm-9">
                                <input required
                                       value="<?= ((!empty($dataVideoHighlight['video_tag'])) ? $dataVideoHighlight['video_tag'] : '') ?>"
                                       name="VideoHighlight[video_tag]" type="text" class="form-control" value=""
                                       placeholder="Video Tag">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">รายละเอียด</label>
                            <div class="col-sm-9">
                                <input value="<?= ((!empty($dataVideoHighlight['desc'])) ? $dataVideoHighlight['desc'] : '') ?>"
                                       name="VideoHighlight[desc]" type="text" class="form-control" value=""
                                       placeholder="Desc">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">พาร์ทไฟล์ Streamable</label>
                            <div class="col-sm-9">
                                <input value="<?= ((!empty($dataVideoHighlight['pathFile'])) ? $dataVideoHighlight['pathFile'] : '') ?>"
                                       name="VideoHighlight[pathFile]" type="text" class="form-control" value=""
                                       placeholder="pathFile Streamable">
                            </div>
                        </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">ปัดมุด</label>
                                <div class="col-sm-4">
                                    <select name="VideoHighlight[pin]" class="form-control">
                                        <option value="">ไม่ปัก</option>
                                        <?php for ($i=1;$i<11;$i++){ ?>
                                            <option <?= ((!empty($dataVideoHighlight['pin']) and (11-$dataVideoHighlight['pin'])==$i) ? 'selected' : '') ?> value="<?=(11-$i)?>">ความสำคัญ <?=$i?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="/manager/video" class="btn btn-default" data-dismiss="modal">Close</a>
                        <?php if (!empty($dataVideoHighlight['video_id'])) { ?>
                            <button type="submit" class="btn btn-warning">Changes</button>
                        <?php } else { ?>
                            <button type="submit" class="btn btn-primary">Save</button>
                        <?php } ?>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <form style="display: none;" id="formUpload" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="" id="input-video-id">
        <input id="inputUpload" type="file" name="Filedata[]" multiple="multiple" accept="image/*"><br>
    </form>

</div>
<script>
    function addVideo(){
        $("input[name='VideoHighlight[link]']").val(window.location.pathname);
        $("#exampleModal").modal('show');
    }
    $(document).on('click', "label#labelUpload", function (e) {
        $('#input-video-id').val($(this).attr('data-videoId'));
    });
    $(document).on('change', "#inputUpload", function (e) {
        $("#formUpload").submit();
    });
    $(document).on('submit', "#formUpload", function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: "/manager/uploadFileVideo",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                response = jQuery.parseJSON(response);
                $('#video_thumbnail_image_' + $('#input-video-id').val()).attr('src', response + "#" + Math.floor((Math.random() * 1000) + 1));
//                console.log(response);
//                $.each( response, function( key, value ) {
//                    $('div#list-image').append("<img style='width: 20%;padding: 5px;border: solid 1px;margin: 5px;' class='card-img-top' src='"+value+"#"+Math.floor((Math.random() * 1000) + 1)+"'>");
//                });
//                $('input[name="news[imageLink]"]').val(response);
//                $('img#player').attr('src',response+'#'+Math.floor((Math.random() * 1000) + 1));
            },
            error: function (data) {
                console.log("error");
                console.log(data);
            }
        });
    });
</script>
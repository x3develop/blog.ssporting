<?php
require_once __DIR__ . "/../bootstrap.php";
include '_database.php';
if (isset($_GET['comment'])) {
    if (!empty($_GET['comment']['id'])) {

    } else {
        $sql = "INSERT INTO `comment_match` (`user_id`,`match_id`,`team`,`comment`,`created_at`,`updated_at`)";
        $sql .= " VALUES ('" . $_GET['comment']['user_id'] . "','" . $_GET['comment']['match_id'] . "','" . $_GET['comment']['team'] . "','" . $_GET['comment']['review_text'] . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
    }
    $sth = $dbh->prepare($sql)->execute();
    header("Location: /manager");
    exit;
} elseif (isset($_REQUEST['review'])) {
    if (!empty($_REQUEST['review']['id'])) {
    } else {
        $sql = "INSERT INTO `review_match` (`user_id`,`match_id`,`user_review_id`,`team`,`review_text`,`created_at`,`updated_at`)";
        $sql .= " VALUES ('" . $_REQUEST['review']['user_id'] . "','" . $_REQUEST['review']['match_id'] . "','" . $_REQUEST['review']['user_review_id'] . "','" . $_REQUEST['review']['team'] . "','" . $_REQUEST['review']['review_text'] . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";

    }
    $sth = $dbh->prepare($sql)->execute();
    $matchcoment = new \model\MatchCommentElo();
    $matchcoment->mid = $_REQUEST['review']['match_id'];
    $matchcoment->user_id = $_REQUEST['review']['user_review_id'];
    $matchcoment->side = $_REQUEST['review']['team'];
    $matchcoment->user_type = "reviewer";
    $matchcoment->comment = $_REQUEST['review']['review_text'];
    $matchcoment->save();
    header("Location: /manager");
    exit;
} elseif (isset($_GET['score'])) {
    $sql = "UPDATE `match` set `status`='" . ((!empty($_GET['score']['status'])) ? $_GET['score']['status'] : 'create') . "',`updated_at`='" . date("Y-m-d H:i:s") . "' where id=" . $_GET['score']['id'];
    $sth = $dbh->prepare($sql)->execute();
    header("Location: /manager");
    exit;
} elseif (isset($_GET['gamble'])) {
    $sth = $dbh->prepare("select * from `gamble` where match_id=" . $_GET['gamble']['match_id']);
    $sth->execute();
    $gamble = $sth->fetch();
    if (!empty($gamble)) {
        $sql = "UPDATE `gamble` set `home_water_bill`=" . $_GET['gamble']['home_water_bill'] . ",`handicap`=" . $_GET['gamble']['handicap'] . ",`away_water_bill`=" . $_GET['gamble']['away_water_bill'] . ",`updated_at`='" . date("Y-m-d H:i:s") . "' where match_id=" . $_GET['gamble']['match_id'];
    } else {
        $sql = "INSERT INTO `gamble` (`match_id`,`home_water_bill`,`handicap`,`away_water_bill`,`created_at`,`updated_at`)";
        $sql .= " VALUES (" . $_GET['gamble']['match_id'] . "," . $_GET['gamble']['home_water_bill'] . "," . $_GET['gamble']['handicap'] . "," . $_GET['gamble']['away_water_bill'] . ",'" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
    }
    $sth = $dbh->prepare($sql)->execute();

//    if(empty($gamble)) {
    $sql = "UPDATE `match` set `status`='bet',`updated_at`='" . date("Y-m-d H:i:s") . "' where id=" . $_GET['gamble']['match_id'];
    $sth = $dbh->prepare($sql)->execute();

    header("Location: /manager");
    exit;
//    }
} elseif (isset($_GET['match'])) {
    $date = new DateTime();
    $dateList = list($day, $month, $year) = explode('/', $_GET['match']['date']);
    $timeList = list($hour, $minute) = explode(':', $_GET['match']['time']);
    $date->setDate($year, $month, $day);
    $date->setTime($hour, $minute);

    $sth = $dbh->prepare("select * from `team` where name_en='" . $_GET['match']['team_home'] . "'");
    $sth->execute();
    $teamHome = $sth->fetch();

    $sth = $dbh->prepare("select * from `team` where name_en='" . $_GET['match']['team_away'] . "'");
    $sth->execute();
    $teamAway = $sth->fetch();

    if (!empty($teamHome) && !empty($teamAway)) {
        if (!empty($_GET['match']['id'])) {
            if (!empty($_GET['match']['match_7m_id']) && $_GET['match']['match_7m_id'] !== "") {
                $sql = "UPDATE `match` set `match_7m_id`=" . $_GET['match']['match_7m_id'] . ",`league_id`=" . $_GET['match']['league_id'] . ",`team_home`=" . $teamHome['id'] . ",`team_away`=" . $teamAway['id'] . ",`time_match`='" . $date->format('Y-m-d H:i:s') . "',`updated_at`='" . date("Y-m-d H:i:s") . "' WHERE id=" . $_GET['match']['id'];
            } else {
                $sql = "UPDATE `match` set `league_id`=" . $_GET['match']['league_id'] . ",`team_home`=" . $teamHome['id'] . ",`team_away`=" . $teamAway['id'] . ",`time_match`='" . $date->format('Y-m-d H:i:s') . "',`updated_at`='" . date("Y-m-d H:i:s") . "' WHERE id=" . $_GET['match']['id'];
            }
        } else {
            if (!empty($_GET['match']['match_7m_id'])) {
                $sql = "INSERT INTO `match` (`league_id`,`match_7m_id`,`team_home`,`team_away`,`status`,`time_match`,`created_at`,`updated_at`)";
                $sql .= " VALUES (" . $_GET['match']['league_id'] . "," . $_GET['match']['match_7m_id'] . "," . $teamHome['id'] . "," . $teamAway['id'] . ",'create','" . $date->format('Y-m-d H:i:s') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
            } else {
                $sql = "INSERT INTO `match` (`league_id`,`team_home`,`team_away`,`status`,`time_match`,`created_at`,`updated_at`)";
                $sql .= " VALUES (" . $_GET['match']['league_id'] . "," . $teamHome['id'] . "," . $teamAway['id'] . ",'create','" . $date->format('Y-m-d H:i:s') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
            }
        }
        $sth = $dbh->prepare($sql)->execute();
    }
    header("Location: /manager");
    exit;
} elseif (isset($_GET['bet'])) {
    foreach ($_GET['bet'] as $key => $value) {
        $sth = $dbh->prepare("select user.name,user.coin,user.gold,user.coupon,user.lv,user.star,bet.* from bet inner join user on user.id=bet.user_id where bet.id=" . $key);
        $sth->execute();
        $bet = $sth->fetch();
        if (!empty($bet)) {
            $user_id = $bet['user_id'];
            $befor_gold = $bet['gold'];
            $befor_coin = $bet['coin'];
            $befor_coupon = $bet['coupon'];
            $befor_lv = $bet['lv'];
            $befor_star = $bet['star'];
            $income_derived = 'returnbet';
            $income_type = 'coin&gold';
            $income_format = 'minus';
            $after_gold = ($befor_gold + $bet['use_bet_gold']);
            $after_coin = ($befor_coin + $bet['use_bet_coin']);
            $after_coupon = $befor_coupon;
            $after_lv = $befor_lv;
            $after_star = $befor_star;
            $comment = 'ระบบได้ทำการ ยกเลิกการ ทายผล ของคู่ที่' . $bet['match_id'];
            $sql = "INSERT INTO `log_income` (`bet_id`,`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
            $sql .= " VALUES (" . $bet['id'] . "," . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . date('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
            $sth = $dbh->prepare($sql)->execute();
            if ($sth) {
                $sql = "update `user` set coin=" . $after_coin . ",gold=" . $after_gold . ",updated_at='" . date("Y-m-d H:i:s") . "' where id=" . $bet['user_id'];
                $sth = $dbh->prepare($sql)->execute();
                if ($sth) {
                    if(isset($_SESSION['login']) && !empty($_SESSION['login']['username']) && $_SESSION['login']['username']=="admin") {
                        $sql = "DELETE FROM `bet` WHERE `bet`.id=" . $bet['id'];
                        $sth = $dbh->prepare($sql)->execute();
                    }
                }
            }
        }
    }
    header("Location: /manager");
    exit;
} elseif (isset($_GET['logIncome'])) {
    $coin = 0;
    $gold = 0;
    $logIncomeFirst = 0;
    $logIncomeEnd = 0;
    foreach ($_GET['logIncome'] as $key => $value) {
        $sth = $dbh->prepare("select * from log_income where log_income.user_id=" . $value . " and log_income.id>=" . $key . " order by id ASC");
        $sth->execute();
        $logIncomeList = $sth->fetchAll();
        if (!empty($logIncomeList)) {
            $gold = $logIncomeList[0]['befor_gold'];
            $coin = $logIncomeList[0]['befor_coin'];
//            var_dump( "gold: ".$gold."::".$coin);
//            echo "<br><br>";
            foreach ($logIncomeList as $keyl => $valuel) {
                if ($logIncomeFirst == 0) {
                    $logIncomeFirst = $valuel['id'];
                }
                $logIncomeEnd = $valuel['id'];
//                var_dump($valuel['id']." :: ".$valuel['bet_id']." :: ".$valuel['befor_gold']." :: ".$valuel['befor_coin']." :: ".$valuel['comment']." :: ".$valuel['income_derived']." :: ".$valuel['income_type']);
//                echo "<br>";
                if (!empty($valuel['bet_id']) and $valuel['income_derived'] == "bet" and ($valuel['income_type'] == "gold" || $valuel['income_type'] == "coin&gold")) {
                    $sth = $dbh->prepare("select * from user where user.id=" . $valuel['user_id']);
                    $sth->execute();
                    $user = $sth->fetch();
                    $sth = $dbh->prepare("select bet.id as bet_id,`match`.`status` as match_status,`match`.`status` as match_status,bet.`status`,((`match`.home_end_time_score+gamble.handicap)-`match`.away_end_time_score) as homeScore,`match`.away_end_time_score,
CASE
    WHEN (`match`.home_end_time_score+gamble.handicap) > `match`.away_end_time_score and bet.team='home' THEN 'win'
    WHEN (`match`.home_end_time_score+gamble.handicap) > `match`.away_end_time_score and bet.team='away' THEN 'lose'
    WHEN (`match`.home_end_time_score+gamble.handicap) < `match`.away_end_time_score and bet.team='away' THEN 'win'
    WHEN (`match`.home_end_time_score+gamble.handicap) < `match`.away_end_time_score and bet.team='home' THEN 'lose'
    ELSE 'equal'
END as results
,tameHome.name_en,tameHome.path,tameAway.name_en,tameAway.path,bet.team,bet.`status`,bet.use_bet_coin,gamble.home_water_bill,gamble.away_water_bill,bet.id,bet.created_at from bet 
left join `match` on `match`.id=bet.match_id
left join team as tameHome on tameHome.id=`match`.team_home
left join team as tameAway on tameAway.id=`match`.team_away
left join gamble on gamble.match_id=`match`.id
where bet.id=" . $valuel['bet_id'] . " and `match`.away_end_time_score IS NOT NULL and `match`.home_end_time_score IS NOT NULL order by bet.created_at ASC");
                    $sth->execute();
                    $resultsBet = $sth->fetchAll();
                    foreach ($resultsBet as $key => $value) {
                        if ($value['match_status'] == "fullTime") {
                            if ($value['results'] == "win") {
                                if ($value['homeScore'] == 0.25 || $value['homeScore'] == (-0.25)) {
                                    $statusHalf = true;
                                }
                                $user_id = $user['id'];
                                $befor_gold = $gold;
                                $befor_coin = $coin;
                                $befor_coupon = $user['coupon'];
                                $befor_lv = $user['lv'];
                                $befor_star = $user['star'];
                                $income_derived = 'bet';
                                $income_type = 'gold';
                                $income_format = 'plus';
//                            สูตร คำนวน จ่าย  W คืน coin ที่เเทงมา G=(home_water_bill-1)*coin; * จ่ายคลึ้ง  G=((home_water_bill-1)/2)*coin;
                                if ($value['team'] == "home") {
                                    if ($statusHalf) {
                                        $after_gold = (intval($befor_gold) + (intval($value['use_bet_coin'] * (($value['home_water_bill']) - 1) / 2)));
                                        $after_coin = ($befor_coin + ($value['use_bet_coin'] / 2));
                                    } else {
                                        $after_gold = (intval($befor_gold) + (intval($value['use_bet_coin']) * (($value['home_water_bill']) - 1)));
                                        $after_coin = ($befor_coin + $value['use_bet_coin']);
                                    }
                                } else if ($value['team'] == "away") {
                                    if ($statusHalf) {
                                        $after_gold = (intval($befor_gold) + (intval($value['use_bet_coin'] * (($value['away_water_bill']) - 1) / 2)));
                                        $after_coin = ($befor_coin + ($value['use_bet_coin'] / 2));
                                    } else {
                                        $after_gold = (intval($befor_gold) + (intval($value['use_bet_coin']) * (($value['away_water_bill']) - 1)));
                                        $after_coin = ($befor_coin + $value['use_bet_coin']);
                                    }
                                }
                                $after_coupon = $befor_coupon;
                                $after_lv = $befor_lv;
                                $after_star = $befor_star;
                                $comment = "ได้รับ " . ($after_gold - $befor_gold) . "gold เเละ " . ($after_coin - $befor_coin) . "coin จากการ ทายผลฟุตบอล ถูก";
                                $sql = "INSERT INTO `log_income` (`bet_id`,`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                                $sql .= " VALUES (" . $value['bet_id'] . "," . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . date("Y-m-d") . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                                $sth = $dbh->prepare($sql)->execute();
//                                    var_dump($sql);
//                                    echo "<br><br>";
                                if ($sth) {
                                    $sql = "update `bet` set status='cheap' where id=" . $value['id'];
                                    $sth = $dbh->prepare($sql)->execute();
                                    if ($sth) {
                                        $gold = $after_gold;
                                        $coin = $after_coin;
                                    }
                                }
                                $betWin++;
                            } else if ($value['results'] == "lose") {
                                if ($value['homeScore'] == (-0.25) || $value['homeScore'] == (0.25)) {
                                    $user_id = $user['id'];
                                    $befor_gold = $gold;
                                    $befor_coin = $coin;
                                    $befor_coupon = $user['coupon'];
                                    $befor_lv = $user['lv'];
                                    $befor_star = $user['star'];
                                    $income_derived = 'bet';
                                    $income_type = 'coin&gold';
                                    $income_format = 'minus';
                                    if ($value['homeScore'] == (-0.25) || $value['homeScore'] == (0.25)) {
                                        $after_gold = (intval($befor_gold) - (intval($value['use_bet_coin'] / 2)));
                                        $after_coin = (intval($befor_coin) + (intval($value['use_bet_coin'] / 2)));
                                    }
                                    $after_coupon = $befor_coupon;
                                    $after_lv = $befor_lv;
                                    $after_star = $befor_star;
                                    $comment = "เสีย " . ($befor_gold - $after_gold) . "gold ได้รับ " . ($after_coin - $befor_coin) . "coin คืน จากการ ทายผลฟุตบอล ผ ิด";
                                    $sql = "INSERT INTO `log_income` (`bet_id`,`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                                    $sql .= " VALUES (" . $value['bet_id'] . "," . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . date("Y-m-d") . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
//                                    var_dump($sql);
//                                    echo "<br><br>";
                                    $sth = $dbh->prepare($sql)->execute();
                                    if ($sth) {
                                        $sql = "update `bet` set status='wrong' where id=" . $value['id'];
                                        $sth = $dbh->prepare($sql)->execute();
                                        if ($sth) {
                                            $gold = $after_gold;
                                            $coin = $after_coin;
                                        }
                                    }
                                } else {
                                    $user_id = $user['id'];
                                    $befor_gold = $gold;
                                    $befor_coin = $coin;
                                    $befor_coupon = $user['coupon'];
                                    $befor_lv = $user['lv'];
                                    $befor_star = $user['star'];
                                    $income_derived = 'bet';
                                    $income_type = 'gold';
                                    $income_format = 'minus';
                                    $after_gold = (intval($befor_gold) - (intval($value['use_bet_coin'])));
                                    $after_coin = $befor_coin;
                                    $after_coupon = $befor_coupon;
                                    $after_lv = $befor_lv;
                                    $after_star = $befor_star;
                                    $comment = "เสีย " . ($befor_gold - $after_gold) . "gold จากการ ทายผลฟุตบอล ผ ิด";
                                    $sql = "INSERT INTO `log_income` (`bet_id`,`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                                    $sql .= " VALUES (" . $value['bet_id'] . "," . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . date("Y-m-d") . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
//                                    var_dump($sql);
//                                    echo "<br><br>";
                                    $sth = $dbh->prepare($sql)->execute();
                                    if ($sth) {
                                        $sql = "update `bet` set status='wrong' where id=" . $value['id'];
                                        $sth = $dbh->prepare($sql)->execute();
                                        if ($sth) {
                                            $gold = $after_gold;
                                            $coin = $after_coin;
                                        }
                                    }
                                }
                                $betLose++;
                            } else if ($value['results'] == "equal") {
                                $user_id = $user['id'];
                                $befor_gold = $gold;
                                $befor_coin = $coin;
                                $befor_coupon = $user['coupon'];
                                $befor_lv = $user['lv'];
                                $befor_star = $user['star'];
                                $income_derived = 'bet';
                                $income_type = 'gold';
                                $income_format = 'plus';
                                $after_gold = $befor_gold;
                                $after_coin = ($befor_coin + $value['use_bet_coin']);
                                $after_coupon = $befor_coupon;
                                $after_lv = $befor_lv;
                                $after_star = $befor_star;
                                $comment = "ได้รับ " . ($after_coin - $befor_coin) . "coin คืน จากการ ทายผลฟุตบอล เสมอ";
                                $sql = "INSERT INTO `log_income` (`bet_id`,`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                                $sql .= " VALUES (" . $value['bet_id'] . "," . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . date('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
//                                    var_dump($sql);
//                                    echo "<br><br>";
                                $sth = $dbh->prepare($sql)->execute();
                                if ($sth) {
                                    $sql = "update `bet` set status='always' where id=" . $value['id'];
                                    $sth = $dbh->prepare($sql)->execute();
                                    if ($sth) {
                                        $gold = $after_gold;
                                        $coin = $after_coin;
                                    }
                                }
                                $betEqual++;
                            }
                        }
                        $betCount++;
                    }
                } else if (!empty($valuel['bet_id']) and $valuel['income_derived'] == "bet" and $valuel['income_type'] == "coin&coupon") {
//                    echo "<br><br>";
//                    var_dump( "*********gold: ".$gold."::".$coin);
//                    echo "<br><br>";
                    $user_id = $user['id'];
                    $befor_gold = $gold;
                    $befor_coin = $coin;
                    $befor_coupon = $user['coupon'];
                    $befor_lv = $user['lv'];
                    $befor_star = $user['star'];
                    $income_derived = 'bet';
                    $income_type = 'coin&coupon';
                    $income_format = 'minus';
                    $after_gold = ($befor_gold + ($valuel['after_gold'] - $valuel['befor_gold']));
                    $after_coin = ($befor_coin + ($valuel['after_coin'] - $valuel['befor_coin']));
                    $after_coupon = $befor_coupon;
                    $after_lv = $befor_lv;
                    $after_star = $befor_star;
                    $comment = $valuel["comment"];
                    $sql = "INSERT INTO `log_income` (`bet_id`,`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                    $sql .= " VALUES (" . $value['bet_id'] . "," . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . date('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
//                    var_dump($sql);
//                    echo "<br><br>";
                    $sth = $dbh->prepare($sql)->execute();
                    if ($sth) {
                        $gold = $after_gold;
                        $coin = $after_coin;
                    }
//                    var_dump( "------------------gold: ".$gold."::".$coin);
//                    echo "<br><br>";
                } else {
//                    var_dump( "------------------gold: ".$gold."::".$coin);
//                    echo "<br><br>";
//                    var_dump( ">>>>>>>>>".$valuel['comment']);
//                    echo "<br><br>";
                    $user_id = $user['id'];
                    $befor_gold = $gold;
                    $befor_coin = $coin;
                    $befor_coupon = $user['coupon'];
                    $befor_lv = $user['lv'];
                    $befor_star = $user['star'];
                    $income_derived = 'bet';
                    $income_type = 'coin&coupon';
                    $income_format = 'minus';
                    $after_gold = ($befor_gold + ($valuel['after_gold'] - $valuel['befor_gold']));
                    $after_coin = ($befor_coin + ($valuel['after_coin'] - $valuel['befor_coin']));
                    $after_coupon = $befor_coupon;
                    $after_lv = $befor_lv;
                    $after_star = $befor_star;
                    $comment = $valuel["comment"];
                    $sql = "INSERT INTO `log_income` (`bet_id`,`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                    $sql .= " VALUES (" . $value['bet_id'] . "," . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "'," . $after_gold . "," . $after_coin . "," . $after_coupon . ",'" . $comment . "','" . date('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
//                    var_dump($sql);
//                    echo "<br><br>";
                    $sth = $dbh->prepare($sql)->execute();
                    if ($sth) {
                        $gold = $after_gold;
                        $coin = $after_coin;
                    }
//                    var_dump( "------------------gold: ".$gold."::".$coin);
//                    echo "<br><br>";

                }

            }
            if ($logIncomeFirst != 0 and $logIncomeEnd != 0) {
                $sql = "DELETE FROM `log_income` WHERE `log_income`.id>=" . $logIncomeFirst . " and `log_income`.id<=" . $logIncomeEnd;
                $sth = $dbh->prepare($sql)->execute();
                if ($sth) {
                    $sql = "update `user` set coin=" . $coin . ",gold=" . $gold . ",updated_at='" . date("Y-m-d H:i:s") . "' where id=" . $value;
                    $sth = $dbh->prepare($sql)->execute();
                }
            }
        }
    }
    header("Location: /manager");
    exit;
}

?>
<?php include 'layouts/header.php'; ?>
<?php
$sql = "select review_highlight.active as highlight ,userReviewHome.name as userReviewHomeText,userReviewHome.path as userReviewHomePath,reviewHome.review_text as reviewHomeText,
userReviewAway.name as userReviewAwayText,userReviewAway.path as userReviewAwayPath,reviewAway.review_text as reviewAwayText,`match`.id,BetHome.countBet as countBetHome,BetAway.countBet as countBetAway,`match`.time_match,`match`.`status`,`match`.team_home,`match`.team_away,`match`.home_end_time_score,`match`.home_over_time_score,`match`.away_end_time_score,`match`.away_over_time_score,teamHome.name_en as teamHomeEn,teamHome.path as teamHomePath,teamAway.name_en as teamAwayEn,teamAway.path as teamAwayPath,`league`.id as league_id ,`league`.name,`league`.path,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
left join `gamble` on `gamble`.match_id=`match`.id 
left join `review_highlight` on `review_highlight`.mid=`match`.id 
left join `league` on `league`.id=`match`.league_id 
left join `review_match` as reviewHome on reviewHome.match_id=`match`.id and reviewHome.team='home'
left join `review_match` as reviewAway on reviewAway.match_id=`match`.id and reviewAway.team='away'
left join `user_review` as userReviewHome on userReviewHome.id=reviewHome.user_review_id
left join `user_review` as userReviewAway on userReviewAway.id=reviewAway.user_review_id
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='home' group by bet.match_id order by bet.match_id DESC limit 100 ) BetHome ON BetHome.match_id = `match`.id
left join (SELECT bet.match_id,COUNT(*) AS countBet FROM `bet` where `team`='away' group by bet.match_id order by bet.match_id DESC limit 100 ) BetAway ON BetAway.match_id = `match`.id
where `match`.time_match>DATE_ADD((NOW()- INTERVAL 1 DAY), INTERVAL 8 hour) and `match`.time_match <(DATE_ADD((NOW()+ INTERVAL 2 DAY), INTERVAL 8 hour))
group by match.id 
order by time_match DESC";
$sth = $dbh->prepare($sql);
$sth->execute();
$matchAll = $sth->fetchAll();

$sth = $dbh->prepare("select id,name_en from `team`");
$sth->execute();
$selectTeam = $sth->fetchAll(PDO::FETCH_COLUMN | PDO::FETCH_GROUP);

$sth = $dbh->prepare("select id,`name` from `user_review` order by `name` asc ");
$sth->execute();
$selectReview = $sth->fetchAll();

$sth = $dbh->prepare("select `match`.*,`teamHome`.path as pathHome,`teamAway`.path as pathAway,`league`.name,`league`.path,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from gamble
left join `match` on `match`.id=gamble.match_id
left join `league` on `league`.id=`match`.league_id 
left join `team` as teamHome on `teamHome`.id=`match`.team_home
left join `team` as teamAway on `teamAway`.id=`match`.team_away
where `match`.`status`='fullTime'
order by `match`.updated_at DESC limit 10");
$sth->execute();
$matchFullTime = $sth->fetchAll();

$sth = $dbh->prepare("select * from channel ");
$sth->execute();
$channelAll = $sth->fetchAll();

$betList = array();
$leagueIdList = array();
$leagueIdListText="";
foreach ($matchAll as $key => $value) {
    $sth = $dbh->prepare("select user.name,bet.* from bet INNER join user on user.id=bet.user_id where match_id=" . $value['id']);
    $sth->execute();
    $betAll = $sth->fetchAll(PDO::FETCH_ASSOC);
    $betList[$value['id']] = $betAll;
    if(!in_array($value['league_id'], $leagueIdList)){
        if($leagueIdListText==""){
            $leagueIdListText.=$value['league_id'];
        }else{
            $leagueIdListText.=",".$value['league_id'];
        }
        array_push($leagueIdList,$value['league_id']);
    }
}
$sth = $dbh->prepare("select * from league where league.id in (".$leagueIdListText.")");
$sth->execute();
$leagueAll = $sth->fetchAll();
$leagueTeam = array();
foreach ($leagueAll as $key => $value) {
    $sth = $dbh->prepare("select id,name_en from team where league_id=" . $value['id'] . " order by name_en ");
    $sth->execute();
    $teamAll = $sth->fetchAll(PDO::FETCH_ASSOC);
    $leagueTeam[$value['id']] = $teamAll;
}
$logIncomeMatch = array();
foreach ($matchFullTime as $key => $value) {
    $sth = $dbh->prepare("select user.name,log_income.* from bet 
INNER join user on user.id=bet.user_id
INNER join log_income on log_income.bet_id=bet.id and (log_income.income_type='gold' || log_income.income_type='coin&gold')
where match_id=" . $value['id'] . " group by log_income.user_id order by log_income.id DESC");
    $sth->execute();
    $logIncome = $sth->fetchAll(PDO::FETCH_ASSOC);
    $logIncomeMatch[$value['id']] = $logIncome;
}

$webroot = $_SERVER['DOCUMENT_ROOT'];
?>

    <!-- Include Editor style. -->
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.5/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.5/css/froala_style.min.css' rel='stylesheet' type='text/css' />

    <!-- Include JS file. -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.5/js/froala_editor.min.js'></script>

    <div class="container">
        <div class="col-md-8">

            <?php if(!empty($matchAll)){ ?>
                <?php if(isset($_SESSION['login']) && !empty($_SESSION['login']['username']) && $_SESSION['login']['username']=="admin"){ ?>
                    <button id="generates-livematch" style="margin-bottom: 10px;" onclick="generatesLiveMatch()" class="btn btn-block btn-default">
                        <i class="fa fa-check" aria-hidden="true"></i> Generates LiveMatch
                    </button>
                <?php } ?>
            <?php foreach ($matchAll as $key => $value) { ?>
                <div class="row">
                    <?php $date = new DateTime($value['time_match']); ?>
                    <div class="box-match-team" highlight="<?php echo $value['highlight'];?>"  status="<?php echo ((!empty($value['highlight']) and $value['highlight']=="Y")?0:(file_exists($webroot."/match/".$value['id'].".html")?1:0))?>" matchId="<?php echo $value['id'];?>">
                        <p class="text-center"><?php echo $date->format('M-d H:i'); ?> <?= $value['name'] ?></p>
                        <div class="row">
                            <div class="col-md-4 text-center">
                                <div><img style="width: 100px;"
                                          src="<?php echo((!empty($value['pathHome']) && file_exists($webroot . "/" . $value['pathHome'])) ? $value['pathHome'] : '/images/team/team_default_64x64.png') ?>">
                                </div>
                                <div id="match-<?= $value['id'] ?>-home"><?= $selectTeam[$value['team_home']][0] ?></div>
                            </div>
                            <div class="col-md-4 text-center">
                                <div>
                                    (<?= ((!empty($value['home_water_bill'])) ? $value['home_water_bill'] : "0") ?>
                                    )-[ <?= ((!empty($value['handicap'])) ? $value['handicap'] : "0") ?>
                                    ]-(<?= ((!empty($value['away_water_bill'])) ? $value['away_water_bill'] : "0") ?>
                                    )
                                </div>
                                <div style="font-size: 42px;">
                                    <span class="text-info"><?= ((!empty($value['home_end_time_score'])) ? $value['home_end_time_score'] : "0") ?></span>
                                    -
                                    <span class="text-info"><?= ((!empty($value['away_end_time_score'])) ? $value['away_end_time_score'] : "0") ?></span>
                                </div>

                            </div>
                            <div class="col-md-4 text-center">
                                <div><img style="width: 100px;"
                                          src="<?php echo((!empty($value['pathAway']) && file_exists($webroot . "/" . $value['pathAway'])) ? $value['pathAway'] : '/images/team/team_default_64x64.png') ?>">
                                </div>
                                <div style="text-align: center;"
                                     id="match-<?= $value['id'] ?>-away"><?= $selectTeam[$value['team_away']][0] ?></div>
                            </div>
                            <div class="col-md-12">
                                <div class="btn-group-manage" style="width:100%;">
                                    <?php if (isset($_SESSION['login']) && !empty($_SESSION['login']['username']) && $_SESSION['login']['username']=="admin") { ?>
                                        <button type="button" onclick='manageChannel(<?= json_encode($value); ?>)'
                                                class="btn btn1 btn-default">Channel
                                        </button>
                                        <button type="button" onclick='manageScore(<?= json_encode($value); ?>)'
                                                class="btn btn1 btn-default">Score
                                        </button>
                                        <button type="button" onclick='manageGamble(<?= json_encode($value); ?>)'
                                                class="btn btn2 btn-default">Gamble
                                        </button>
                                        <button type="button" onclick='manageReview(<?= json_encode($value); ?>)'
                                                class="btn btn2 btn-default">Review
                                        </button>
                                        <button type="button" onclick='manageComment(<?= json_encode($value); ?>)'
                                                class="btn btn2 btn-default">Comment
                                        </button>
                                        <button type="button" data-toggle="modal"
                                                data-target="#exampleModalBet-<?php echo $value['id']; ?>"
                                                class="btn btn2 btn-default">ManageBet
                                        </button>
                                        <a href="/load/gameNew?type=game&matchId=<?php echo $value['id']; ?>"
                                           class="btn btn2 btn-default"><span class="oi oi-terminal"></span></a>
                                        <button type="button" onclick='manageSnap(<?= json_encode($value); ?>)'
                                           class="btn btn2 btn-default"><span class="oi oi-aperture"></span></button>
                                        <button type="button" onclick='manageEdit(<?= json_encode($value); ?>)'
                                                class="btn btn2 btn-default"><span class="oi oi-pencil"></span>
                                        </button>
                                        <a onclick="return confirm('ยืนยันการลบ')"
                                           href="/manager/match?delete_id=<?php echo $value['id']; ?>"
                                           class="btn btn3 btn-default"><span class="oi oi-trash"></span></a>
                                    <?php } ?>

                                    <!--                                <form action="/bet.php" method="GET">-->
                                    <!--                                    <input type="hidden" name="match_id" value="-->
                                    <? //= $value['id'] ?><!--">-->
                                    <!--                                    <button class="btn btn-outline-primary" type="submit">แทง</button>-->
                                    <!--                                </form>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php } ?>
        </div>
        <div class="col-md-4 box-side-index" style="border-left: solid 1px #ddd;">
            <?php if(isset($_SESSION['login']) && !empty($_SESSION['login']['username']) && $_SESSION['login']['username']=="admin"){ ?>
            <div class="row">
                <div class="col-md-10">
                    <a style="margin-bottom: 10px;" href="/load/gameNew?type=foreignNews" class="btn btn-block btn-default">
                        <i class="fa fa-check" aria-hidden="true"></i> Generates
                    </a>
                </div>
                <div class="colmd-2">
                    <button type="button" data-toggle="modal" data-target="#ModalMatchSnap" class="btn btn-default"><span class="oi oi-aperture"></span></button>
                </div>
            </div>
            <?php } ?>
            <?php foreach ($matchFullTime as $key => $value) { ?>
                <div class="well well-sm">
                    <?php $date = new DateTime($value['time_match']); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <p><span class="glyphicon glyphicon-calendar"
                                     aria-hidden="true"></span> <?php echo $date->format('M-d H:i'); ?></p>
                        </div>

                        <div class="col-md-4">
                            <div style="text-align: center;"><img style="width: 100%;"
                                                                  src="<?php echo((!empty($value['pathHome']) && file_exists($webroot . "/" . $value['pathHome'])) ? $value['pathHome'] : '/images/team/team_default_64x64.png') ?>">
                            </div>
                            <div style="text-align: center;"><?= $selectTeam[$value['team_home']][0] ?></div>
                        </div>
                        <div class="col-md-4">
                            <div style="text-align: center;font-size: 12px;">
                                (<?= ((!empty($value['home_water_bill'])) ? $value['home_water_bill'] : "0") ?>
                                )-[ <?= ((!empty($value['handicap'])) ? $value['handicap'] : "0") ?>
                                ]-(<?= ((!empty($value['away_water_bill'])) ? $value['away_water_bill'] : "0") ?>)
                            </div>
                            <div class="text-center">
                                <h3><?= ((!empty($value['home_end_time_score'])) ? $value['home_end_time_score'] : "0") ?></h3>
                                -
                                <h3><?= ((!empty($value['away_end_time_score'])) ? $value['away_end_time_score'] : "0") ?></h3>
                            </div>

                        </div>
                        <div class="col-md-4">
                            <div style="text-align: center;"><img style="width: 100%;"
                                                                  src="<?php echo((!empty($value['pathAway']) && file_exists($webroot . "/" . $value['pathAway'])) ? $value['pathAway'] : '/images/team/team_default_64x64.png') ?>">
                            </div>
                            <div style="text-align: center;"><?= $selectTeam[$value['team_away']][0] ?></div>
                        </div>
                        <div class="col-md-12" style="text-align: center;">
                            <?php if (isset($_SESSION['login']) && !empty($_SESSION['login']['username']) && $_SESSION['login']['username']=="admin") { ?>
                                <div class="btn-group" role="group" aria-label="...">
                                    <button type="button" onclick='manageScore(<?= json_encode($value); ?>)'
                                            class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil"
                                                                                 aria-hidden="true"></span> Score
                                    </button>
                                    <button type="button" data-toggle="modal"
                                            data-target="#exampleModalLogIncome-<?php echo $value['id']; ?>"
                                            class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-refresh"
                                                                                aria-hidden="true"></span> Reset
                                    </button>
                                </div>
                            <?php } ?>

                            <!--                                <form action="/bet.php" method="GET">-->
                            <!--                                    <input type="hidden" name="match_id" value="-->
                            <? //= $value['id'] ?><!--">-->
                            <!--                                    <button class="btn btn-outline-primary" type="submit">แทง</button>-->
                            <!--                                </form>-->
                        </div>

                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <!--    Match Snap-->
    <div class="modal fade" style="" id="ModalMatchSnap" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Snap</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <form id="snap-matchAll" class="form-inline">
                                <input type="hidden" name="snap[type]" value="matchAll">
                                <div class="form-group">
                                    <label for="exampleInputName2">Date</label>
                                    <input style="width: 150px;" required type="date" name="snap[date]" class="form-control" id="exampleInputName2" placeholder="Date">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputName2">Width</label>
                                    <input style="width: 80px;" required type="number" name="snap[width]" class="form-control" id="exampleInputName2" placeholder="Width" value="600">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail2">Height</label>
                                    <input style="width: 80px;" required type="number" name="snap[height]" class="form-control" id="exampleInputEmail2" placeholder="Height" value="600">
                                </div>
                                <div class="form-group">
                                    <button data-loading-text="Loading..." autocomplete="off"  type="submit" class="btn btn-danger btn-lg"><span class="oi oi-camera-slr"></span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12" style="padding: 10%;">
                            <img onerror="this.src='/images/image-not-found.png';" id="image-snap-matchAll" style="width: 100%;" src="/images/image-not-found.png">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a onerror="this.disabled=true;" id="a-snap-matchAll" href="" download class="btn btn-danger"><span class="oi oi-data-transfer-download"></span> Download</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--    Snap-->
    <div class="modal fade" style="" id="ModalSnap" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Snap</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <form id="snap-match" class="form-inline">
                                <input type="hidden" name="snap[type]" value="match">
                                <input type="hidden" name="snap[id]" value="">
                                <div class="form-group">
                                    <label for="exampleInputName2">Width</label>
                                    <input style="width: 150px;" required type="number" name="snap[width]" class="form-control" id="exampleInputName2" placeholder="Width" value="760">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail2">Height</label>
                                    <input style="width: 150px;" required type="number" name="snap[height]" class="form-control" id="exampleInputEmail2" placeholder="Height" value="300">
                                </div>
                                <div class="form-group">
                                    <button data-loading-text="Loading..." autocomplete="off"  type="submit" class="btn btn-danger btn-lg"><span class="oi oi-camera-slr"></span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12" style="padding: 10%;">
                            <img onerror="this.src='/images/image-not-found.png';" id="image-snap-match" style="width: 100%;" src="/images/image-not-found.png">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a onerror="this.disabled=true;" id="a-snap-match" href="/images/snap/match/61061/snapMatch.png" download class="btn btn-danger"><span class="oi oi-data-transfer-download"></span> Download</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--    User Channel-->
    <div class="modal fade" style="" id="ModalAddChannel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Channel</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="padding: 0px 5px;">
                        <div class="col">
                            <form class="form-inline" id="form-manageChannel" method="get">
                                <input type="hidden" name="channel[match_id]" value="">
                                <div class="form-group mb-2">
                                    <label for="staticEmail2">Channel</label>
                                    <select class="form-control" name="channel[channel_id]">
                                        <?php foreach ($channelAll as $key=>$value){ ?>
                                            <option value="<?=$value['id']?>"><?=$value['channel_name']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group mb-2">
                                    <label for="staticEmail2">Link</label>
                                    <input style="width: 250px;" type="text" class="form-control" name="channel[link]" value="" placeholder="link">
                                </div>
                                <button type="submit" class="btn btn-primary mb-2">add</button>
                            </form>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col">
                            <table class="table table-sm">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Path</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Link</th>
                                    <th scope="col">Manage</th>
                                </tr>
                                </thead>
                                <tbody id="tbody-Channel">
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<!--                    <button type="submit" class="btn btn-primary">Save Change</button>-->
                </div>
            </div>
        </div>
    </div>
    <!--    User Comment-->
    <div class="modal fade" id="ModalAddComment" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">User Comment</h4>
                </div>
                <div class="modal-body">
                    <form id="form-manageComment">
                        <input type="hidden" name="comment[id]">
                        <input type="hidden" name="comment[match_id]">
                        <!--                    <input type="hidden" name="comment[user_id]" value="-->
                        <? //= ((!empty($_SESSION['login']['id']))?$_SESSION['login']['id']:'')?><!--">-->
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">User Comment</label>
                                <div class="col-sm-10">
                                    <select required name="comment[user_id]" class="form-control">
                                        <?php foreach ($selectUser as $key => $value) { ?>
                                            <option value="<?= $value['id'] ?>"><?= $value['username'] ?></option>
                                        <?php } ?>
                                    </select>
                                    <!--                        <input required name="review[user_review_id]" type="text" class="form-control" placeholder="Home Water Bill">-->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Team</label>
                                <div class="col-sm-10">
                                    <select required name="comment[team]" class="form-control">
                                        <option value="home">Home</option>
                                        <option value="away">Away</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Text comment</label>
                                <div class="col-sm-10">
                                    <textarea required name="comment[review_text]" class="form-control"
                                              rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary">Save change
                                    </button>
                                </div>
                            </div>
                        </div>


                    </form>
                    <div style="clear: both"></div>
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Home</th>
                                <th>Away</th>
                            </tr>
                            </thead>
                            <tbody id="view-comment">

                            </tbody>
                        </table>
                    </div>
                    <div style="clear: both"></div>
                </div>
                <!--                <div class="modal-footer">-->
                <!--                <button type="button" class="btn btn-primary">Save changes</button>-->
                <!--                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                <!--                </div>-->
            </div>
        </div>
    </div>
    <!--    Review-->
    <div class="modal fade" id="ModalAddReview" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Review</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="post" id="form-manageReview">
                        <input type="hidden" name="review[id]">
                        <input type="hidden" name="review[match_id]">
                        <input type="hidden" name="review[user_id]"
                               value="<?= ((!empty($_SESSION['login']['id'])) ? $_SESSION['login']['id'] : '') ?>">

                        <div class="form-group">
                            <label class="col-sm-3 control-label">User Review</label>
                            <div class="col-sm-9">
                                <select required name="review[user_review_id]" class="form-control">
                                    <?php foreach ($selectReview as $key => $value) { ?>
                                        <option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                                    <?php } ?>
                                </select>
                                <!--                        <input required name="review[user_review_id]" type="text" class="form-control" placeholder="Home Water Bill">-->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Team</label>
                            <div class="col-sm-9">
                                <select required name="review[team]" class="form-control">
                                    <option value="home">Home</option>
                                    <option value="away">Away</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Review</label>
                            <div class="col-sm-9">
                                <textarea id="froala-editor" required name="review[review_text]" class="form-control" rows="5"></textarea>
<!--                                    <textarea required name="review[review_text]" class="form-control"-->
<!--                                              rows="3"></textarea>-->
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-primary">Save change</button>
                            </div>
                        </div>
                    </form>
                    <div style="clear: both"></div>
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Home</th>
                                <th>Away</th>
                            </tr>
                            </thead>
                            <tbody id="view-review">

                            </tbody>
                        </table>
                    </div>
                    <div style="clear: both"></div>
                </div>
                <!--                <div class="modal-footer">-->
                <!--                                    <button type="button" class="btn btn-primary">Save changes</button>-->
                <!--                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                <!--                </div>-->
            </div>
        </div>
    </div>
    <!--    Gamble-->
    <div class="modal fade" style="" id="ModalAddGamble" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
               aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="form-manageGamble" method="get">
            <input type="hidden" name="search[league_id]"
                   value="<?= ((isset($_GET['search']['league_id'])) ? $_GET['search']['league_id'] : '') ?>">
            <input type="hidden" name="search[team_id]"
                   value="<?= ((isset($_GET['search']['team_id'])) ? $_GET['search']['team_id'] : '') ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Gamble</h4>

                </div>
                <div class="modal-body">
                    <input type="hidden" name="gamble[match_id]">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Home Water Bill</label>
                            <div class="col-sm-9">
                                <input required name="gamble[home_water_bill]" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Handicap</label>
                            <div class="col-sm-9">
                                <input required name="gamble[handicap]" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Away Water Bill</label>
                            <div class="col-sm-9">
                                <input required name="gamble[away_water_bill]" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save Change</button>
                </div>
            </div>
        </form>
    </div>
</div>

    <div class="modal fade" style="" id="ModalAddScore" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="exampleModalLabel">Score</h3>
                </div>
                <div class="modal-body">
                    <form id="form-match-player" class="form-horizontal">
                        <!--                        <label class="sr-only" for="inlineFormInput">Player Name</label>-->

                        <input type="hidden" class="form-control" name="match_player[match_id]" value="">

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="checkbox">
                                    <label>
                                        <input type="radio" name="match_player[team]"
                                               id="exampleRadios1" value="home" checked>
                                        Home
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="radio" name="match_player[team]"
                                               id="exampleRadios2" value="away">
                                        Away
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">ค้นหาชื่อนักแตะ</label>
                            <div class="col-sm-10">
                                <input required class="form-control"
                                       placeholder="ค้นหาชื่อนักแตะ" id="search-typehead" type="text"
                                       name="match_player[player_id]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">นาที</label>
                            <div class="col-sm-10">
                                <input required min="0" type="number" class="form-control"
                                       name="match_player[minute]" placeholder="นาที">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">ต่อ</label>
                            <div class="col-sm-10">
                                <input min="0" type="number" class="form-control"
                                       name="match_player[over_minute]" placeholder="ต่อ">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">ต่อ</label>
                            <div class="col-sm-10">
                                <select required required name="match_player[status]"
                                        class="form-control">
                                    <option value="goal">ทำประตู</option>
                                    <option value="freekick">ฟรีคิก(FreeKick)</option>
                                    <option value="penalty">จุดโทษ(Penalty)</option>
                                    <option value="Notpenalty">จุดโทษไม่เข้า(Penalty)</option>
                                    <option value="Owngoal">ทำเข้าประตูตัวเอง</option>
                                    <option value="overgoal">นอกเวลาทำประตู</option>
                                    <option value="overfreekick">นอกเวลาฟรีคิก</option>
                                    <option value="overpenalty">นอกเวลาจุดโทษ</option>
                                    <option value="Notoverpenalty">จุดโทษไม่เข้า</option>
                                    <option value="CYellow">ใบเหลือง</option>
                                    <option value="CRed">ใบแดง</option>
                                    <option value="CYellowRed">ใบเหลืองแดง</option>
                                    <option value="Out">เปลิ่ยนออก</option>
                                    <option value="In">เปลิ่ยนเข้า</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Add</button>
                            </div>
                        </div>

                    </form>

                    <div class="row" style="margin: 5px;">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ทีม</th>
                                <th>นาที</th>
                                <th>ชื่อ</th>
                                <th>ประเภท</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="match-player-list">

                            </tbody>
                        </table>
                    </div>

                    <!--                    <div class="row">-->
                    <!--                        <div class="form-group col-6">-->
                    <!--                            <label for="exampleInputEmail1">Home Full Time Score</label>-->
                    <!--                            <input required name="score[home_end_time_score]" type="text" class="form-control" placeholder="Home Full Time Score">-->
                    <!--                        </div>-->
                    <!--                        <div class="form-group col-6">-->
                    <!--                            <label for="exampleInputEmail1">Away Full Time Score</label>-->
                    <!--                            <input required name="score[away_end_time_score]" type="text" class="form-control" placeholder="Away Full Time Score">-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="row">-->
                    <!--                        <div class="form-group col-6">-->
                    <!--                            <label for="exampleInputEmail1">Home Over Time Score</label>-->
                    <!--                            <input name="score[home_over_time_score]" type="text" class="form-control" placeholder="Home Over Time Score">-->
                    <!--                        </div>-->
                    <!--                        <div class="form-group col-6">-->
                    <!--                            <label for="exampleInputEmail1">Away Over Time Score</label>-->
                    <!--                            <input name="score[away_over_time_score]" type="text" class="form-control" placeholder="Away Over Time Score">-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                </div>
                <div class="modal-footer">
                    <form style="width: 100%;" id="form-manageScore" class="form-inline">
                        <input type="hidden" class="form-control" name="score[id]" value="">
                        <div class="form-group col-7">
                            <select required name="score[status]" class="form-control">
                                <option value="create">สร้าง</option>
                                <option value="fullTime">จบเกมส์</option>
                                <option value="penalty">จุดโทษ</option>
                                <option value="overTime">ต่อเวลา</option>
                                <option value="bet">เปิดราคา</option>
                            </select>
                        </div>
                        <div class="form-group col-5" style="padding-left: 15%;">
                            <a href="<?= $_SERVER['REQUEST_URI'] ?>" type="button" class="btn btn-secondary">Close</a>
                            <button style="margin-left: 10px;" type="submit" class="btn btn-outline-primary">Save
                                Change
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" style="" id="ModalAddMatch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="form-manageMatch">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="exampleModalLabel">Add Match</h4>

                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="match[id]">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Date Match</label>
                                <div class="col-sm-9">
                                    <input required id="dateSelect" name="match[date]" type="text" class="form-control"
                                           placeholder="Date">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Match 7m Id</label>
                                <div class="col-sm-9">
                                    <input name="match[match_7m_id]" id="input-match-7m-id" type="text"
                                           class="form-control"
                                           placeholder="Match 7m Id">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Time Match</label>
                                <div class="col-sm-9">
                                    <input required id="timeSelect" name="match[time]" type="text" class="form-control"
                                           placeholder="Time">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">League Name</label>
                                <div class="col-sm-9">
                                    <select required id="select-league" name="match[league_id]" class="form-control">
                                        <option value="">League Select</option>
                                        <?php foreach ($leagueAll as $key => $value) { ?>
                                            <option value="<?php echo $value['id'] ?>"><?php echo $value['name'] ?></option>
                                        <?php } ?>
                                    </select>
                                    <small id="emailHelp" class="form-text text-muted"></small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Team Home</label>
                                <div class="col-sm-9">
                                    <input id="input-Team-Home" class="form-control" placeholder="Team Home"
                                           name="match[team_home]">
                                    <!--                        --><?php //foreach ($leagueTeam as $key=>$value){ ?>
                                    <!--                            <select required disabled id="team-home--->
                                    <? //= $key?><!--" name="match[team_home]" class="form-control team-home hide">-->
                                    <!--                                <option value="">Team Select</option>-->
                                    <!--                                --><?php //foreach ($value as $key=>$value){ ?>
                                    <!--                                    <option value="--><? //= $value['id']?><!--">-->
                                    <? //= $value['name_en']?><!--</option>-->
                                    <!--                                --><?php //} ?>
                                    <!--                            </select>-->
                                    <!--                        --><?php //} ?>
                                    <small id="error-team_home" class="form-text text-danger"></small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Team Away</label>
                                <div class="col-sm-9">
                                    <input id="input-Team-Away" class="form-control" placeholder="Team Away"
                                           name="match[team_away]">
                                    <!--                        --><?php //foreach ($leagueTeam as $key=>$value){ ?>
                                    <!--                            <select required disabled id="team-away--->
                                    <? //= $key?><!--" name="match[team_away]" class="form-control team-away hide">-->
                                    <!--                                <option value="">Team Select</option>-->
                                    <!--                                --><?php //foreach ($value as $key=>$value){ ?>
                                    <!--                                    <option value="--><? //= $value['id']?><!--">-->
                                    <? //= $value['name_en']?><!--</option>-->
                                    <!--                                --><?php //} ?>
                                    <!--                            </select>-->
                                    <!--                        --><?php //} ?>
                                    <small id="error-team_away" class="form-text text-danger"></small>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" id="addMatch" class="btn btn-primary">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

<?php foreach ($logIncomeMatch as $key => $value) { ?>
    <div class="modal fade" style="" id="exampleModalLogIncome-<?= $key ?>" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <form>
            <div class="modal-dialog" role="document" style="width: 80%;max-width: 100%;">
                <div class="modal-content">
                    <div class="modal-header">div>
                        <div class="modal-body" style="overflow-y: auto;height: 60%;">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Comment</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Befor Gold</th>
                                    <th scope="col">Befor Coin</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Format</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($value as $keyl => $valuel) { ?>
                                    <tr>
                                        <th scope="row">
                                            <input name="logIncome[<?= $valuel['id'] ?>]"
                                                   value="<?= $valuel['user_id'] ?>"
                                                   type="checkbox">
                                        </th>
                                        <td><?= $valuel['comment'] ?></td>
                                        <td><?= $valuel['name'] ?></td>
                                        <td><?= ($valuel['after_gold'] - $valuel['befor_gold']) ?></td>
                                        <td><?= ($valuel['after_coin'] - $valuel['befor_coin']) ?></td>
                                        <td><?= $valuel['income_type'] ?></td>
                                        <td><?= $valuel['income_format'] ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Clear</button>
                        </div>
                    </div>
                </div>
        </form>
    </div>
<?php } ?>
<?php foreach ($betList as $key => $value) { ?>
    <div class="modal fade" style="" id="exampleModalBet-<?= $key ?>" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <form>
            <div class="modal-dialog" role="document" style="width: 80%;max-width: 100%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal Bet(<?= count($value) ?>)</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="overflow-y: auto;height: 60%;">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">User</th>
                                <th scope="col">Coin</th>
                                <th scope="col">Status</th>
                                <th scope="col">Created_at</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($value as $keyl => $valuel) { ?>
                                <tr>
                                    <th scope="row">
                                        <input name="bet[<?= $valuel['id'] ?>]" value="true" type="checkbox">
                                    </th>
                                    <td><?= $valuel['name'] ?></td>
                                    <td><?= $valuel['use_bet_coin'] ?></td>
                                    <td><?= $valuel['status'] ?></td>
                                    <td><?= $valuel['created_at'] ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Clear</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
<?php } ?>
    <style>
        .hide {
            display: none;
        }

        .ui-autocomplete {
            z-index: 1500;
        }
    </style>
    <script>
        var cache = {};
        // $('textarea#froala-editor').froalaEditor();

        $(document).on("submit", "form#snap-matchAll", function (e) {
            e.preventDefault();
            var $btn = $(this).find('button').button('loading');
            $.ajax({
                url: '/manager/snapImage',
                type: 'GET',
                dataType: 'json',
                data: $(this).serialize(),
                success: function (data) {
                    if (data['status']) {
                        $('form#snap-matchAll')[0].reset();
                        $('#image-snap-matchAll').attr('src',data['data']+'#'+$.now());
                        $('#a-snap-matchAll').attr('href',data['data'])
                        $('#a-snap-matchAll').show();
                        $btn.button('reset');
                    }
                }
            });
        });

        $("input#input-Team-Away").autocomplete({
            minLength: 1,
            source: function (request, response) {
                var term = request.term;
                if (term in cache) {
                    response(cache[term]);
                    return;
                }

                $.getJSON("/manager/readTeam", request, function (data, status, xhr) {
                    cache[term] = data;
                    response(data);
                });
            }
        });

        $("input#input-Team-Home").autocomplete({
            minLength: 1,
            source: function (request, response) {
                var term = request.term;
                if (term in cache) {
                    response(cache[term]);
                    return;
                }

                $.getJSON("/manager/readTeam", request, function (data, status, xhr) {
                    cache[term] = data;
                    response(data);
                });
            }
        });

        $("#search-typehead").autocomplete({
            minLength: 1,
            source: function (request, response) {
                var term = request.term;
                if (term in cache) {
                    response(cache[term]);
                    return;
                }

                $.getJSON("/manager/readplayer", request, function (data, status, xhr) {
                    cache[term] = data;
                    response(data);
                });
            }
        });
        $(document).on("click", "td.comment_match", function (e) {
            var id = $(this).attr('data-id');
            if (confirm("ยืนยันการ Comment นี้")) {
                $.ajax({
                    url: "/manager/removeComment",
                    data: {id: id},
                    method: "GET",
                    dataType: "json"
                }).done(function (response) {
                    if (response['status']) {
                        $('tbody#view-comment').find("td[data-id='" + id + "']").remove();
                    }
                });
            }
        });
        $(document).on("click", "td.review_match", function (e) {
            var id = $(this).attr('data-id');
            if (confirm("ยืนยันการ ลบ Review นี้")) {
                $.ajax({
                    url: "/manager/removeReview",
                    data: {id: id},
                    method: "GET",
                    dataType: "json"
                }).done(function (response) {
                    if (response['status']) {
                        $('tbody#view-review').find("td[data-id='" + id + "']").remove();
                    }
                });
            }
        });
        
        function generatesLiveMatch() {
            $('#generates-livematch').attr('disabled','disabled');
            $('.box-match-team').each(function (index) {
                // console.log($(this).attr('status'));
                if($(this).attr('status')==0) {
                    $.ajax({
                        url: "/load/gameNew",
                        data: {method:'GET',type: 'game',matchId:$(this).attr('matchId')},
                        method: "GET",
                        dataType: "json"
                    }).done(function (response) {
                        if (response['status']) {
                            console.log(response);
                        }
                    });
                }
            });
            $.ajax({
                url: "/load/gameNew",
                data: {method:'GET',type: 'livescore'},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                if (response['status']) {
                    $('#generates-livematch').removeAttr('disabled');
                    $('#modelGeneratesSuccessforeignNews').modal("show");
                }
            });
        }

        function manageSnap(data) {
            $('form#snap-match').find('input[name="snap[id]"]').val(data.id)
            $('#image-snap-match').attr('src','/images/snap/match/'+data.id+'/snapmatch.png');
            $('#a-snap-match').attr('href','/images/snap/match/'+data.id+'/snapmatch.png');
            $('#ModalSnap').modal('show');
            setTimeout(function () {
                if($('#image-snap-match').attr('src')=="/images/image-not-found.png") {
                    $('#a-snap-match').hide();
                }
            },500);
        }

        function removeMatchPlayer(id) {
            $.ajax({
                url: '/manager/addmathplayer?match_player_id=' + id,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    if (data['status'] == "true") {
                        $('tr#mathplayer-' + id).remove();
                    }
                }
            });
        }

        function resetForm() {
            $('#form-manageMatch')[0].reset();
        }

        function manageComment(data) {
            console.log("manageComment");
            $('#form-manageComment').find('input[name="comment[match_id]"]').first().val(data.id);
            $.ajax({
                url: "/manager/getComment",
                data: {id: data.id},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                $('tbody#view-comment').html("");
                $('tbody#view-comment').html(response['html']);
                $('#ModalAddComment').modal('show');
            });
        }

        function manageReview(data) {
            $('#form-manageReview').find('input[name="review[match_id]"]').first().val(data.id);
            $.ajax({
                url: "/manager/getReview",
                data: {id: data.id},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                $('tbody#view-review').html("");
                $('tbody#view-review').html(response['html']);
                $('#ModalAddReview').modal('show');
            });
        }

        function manageEdit(data) {
            console.log(data);
            $('#form-manageMatch').find('input[name="match[match_7m_id]"]').first().val(data.match_7m_id);
            $('#form-manageMatch').find('input[name="match[id]"]').first().val(data.id);
            $('#form-manageMatch').find('input[name="match[date]"]').val(moment(data.time_match).format('DD/MM/YYYY'));
            $('#form-manageMatch').find('input[name="match[time]"]').val(moment(data.time_match).format('HH:mm'));
            $('#form-manageMatch').find('select[name="match[league_id]"]').first().val(data.league_id);

            $('select.team-home').attr('disabled', 'disabled');
            $('select.team-away').attr('disabled', 'disabled');
            $('select.team-home').addClass('hide');
            $('select.team-away').addClass('hide');

            if (data.league_id != "") {
                $('select#team-home-' + data.league_id).removeAttr('disabled');
                $('select#team-away-' + data.league_id).removeAttr('disabled');
                $('select#team-home-' + data.league_id).removeClass('hide');
                $('select#team-away-' + data.league_id).removeClass('hide');
            }
            $('#form-manageMatch').find('select#team-home-' + data.league_id).first().val(data.team_home);
            $('#form-manageMatch').find('select#team-away-' + data.league_id).first().val(data.team_away);
            $('#form-manageMatch').find('#input-Team-Home').first().val($("div#match-" + data.id + "-home").html());
            $('#form-manageMatch').find('#input-Team-Away').first().val($("div#match-" + data.id + "-away").html());
            $('#ModalAddMatch').modal('show');
        }

        function manageComment(data) {
            console.log("manageComment");
            $('#form-manageComment').find('input[name="comment[match_id]"]').first().val(data.id);
            $.ajax({
                url: "/manager/getComment",
                data: {id: data.id},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                $('tbody#view-comment').html("");
                $('tbody#view-comment').html(response['html']);
                $('#ModalAddComment').modal('show');
            });
        }

        function manageReview(data) {
            $('#form-manageReview').find('input[name="review[match_id]"]').first().val(data.id);
            $.ajax({
                url: "/manager/getReview",
                data: {id: data.id},
                method: "GET",
                dataType: "json"
            }).done(function (response) {
                $('tbody#view-review').html("");
                $('tbody#view-review').html(response['html']);
                $('#ModalAddReview').modal('show');
            });
        }

        function manageGamble(data) {
//        console.log(data);
            $('#form-manageGamble').find('input[name="gamble[match_id]"]').first().val(data.id);
            $('#form-manageGamble').find('input[name="gamble[home_water_bill]"]').first().val(data.home_water_bill);
            $('#form-manageGamble').find('input[name="gamble[handicap]"]').first().val(data.handicap);
            $('#form-manageGamble').find('input[name="gamble[away_water_bill]"]').first().val(data.away_water_bill);
            $('#ModalAddGamble').modal('show');
        }

        function manageChannel(data) {
            $("input[name='channel[match_id]']").val(data.match_7m_id);
            $.ajax({
                url: '/manager/getChannel?match_id=' + data.match_7m_id,
                type: 'GET',
                dataType: 'html',
                success: function (data) {
                    $('#tbody-Channel').html("");
                    $('#tbody-Channel').html(data);
                    $('#ModalAddChannel').modal('show');
                }
            });
        }

        function removeChannelMatch(match_id,id) {
            if(confirm('ยืนยันการลบ การถ่ายทอดสด')){
                $.ajax({
                    url: '/manager/removeChannelMatch?id=' + id+'&match_id='+match_id,
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        if (data['status']) {
                            $('#tbody-Channel').html(data['data']);
                        }
                    }
                });
            }
        }

        function manageScore(data) {
            $('#form-match-player').find('input[name="match_player[match_id]"]').first().val(data.id);
            $('#form-manageScore').find('input[name="score[id]"]').first().val(data.id);
            $('#form-manageScore').find('input[name="score[home_end_time_score]"]').first().val(data.home_end_time_score);
            $('#form-manageScore').find('input[name="score[home_over_time_score]"]').first().val(data.home_over_time_score);
            $('#form-manageScore').find('input[name="score[away_end_time_score]"]').first().val(data.away_end_time_score);
            $('#form-manageScore').find('input[name="score[away_over_time_score]"]').first().val(data.away_over_time_score);
            $('#form-manageScore').find('select[name="score[status]"]').first().val(data.status);

            $.ajax({
                url: '/manager/getmathplayerbymath?id=' + data.id,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    if (data['status'] == "true") {
                        $('#match-player-list').html("");
                        $.each(data['data'], function (key, value) {
                            //                        console.log(typeof(value['over_minute']));
                            //                        console.log(value['over_minute']);
                            if (value['over_minute'] !== null && value['over_minute'] != 0) {
                                $('#match-player-list').append("<tr id='mathplayer-" + value['id'] + "'><td>" + value['team'] + "</td><td>" + value['minute'] + "(" + value['over_minute'] + ")</td><td>" + value['name'] + "</td><td>" + value['status'] + "</td><td><button onclick='removeMatchPlayer(" + value['id'] + ")' class='btn btn-outline-danger'><span class='oi oi-trash'></span></button></td></tr>");
                            } else {
                                $('#match-player-list').append("<tr id='mathplayer-" + value['id'] + "'><td>" + value['team'] + "</td><td>" + value['minute'] + "</td><td>" + value['name'] + "</td><td>" + value['status'] + "</td><td><button onclick='removeMatchPlayer(" + value['id'] + ")' class='btn btn-outline-danger'><span class='oi oi-trash'></span></button></td></tr>");
                            }
                        })
                        $('#ModalAddScore').modal('show');
                    }
                }
            });
        }

        $(document).on("submit", "form#snap-match", function (e) {
            e.preventDefault();
            var $btn = $(this).find('button').button('loading');
            $.ajax({
                url: '/manager/snapImage',
                type: 'GET',
                dataType: 'json',
                data: $(this).serialize(),
                success: function (data) {
                    if (data['status']) {
                        $('form#snap-match')[0].reset();
                        $('#image-snap-match').attr('src',data['data']);
                        $('#a-snap-match').show();
                        $btn.button('reset');
                    }
                }
            });
        });

        $(document).on("submit", "form#form-manageChannel", function (e) {
            e.preventDefault();
            $.ajax({
                url: '/manager/addChannelMatch',
                type: 'GET',
                dataType: 'json',
                data: $(this).serialize(),
                success: function (data) {
                    if (data['status']) {
                        $('form#form-manageChannel')[0].reset();
                        $('#tbody-Channel').html(data['data']);
                    }
                }
            });
        });

        $(document).on("submit", "form#form-match-player", function (e) {
            e.preventDefault();
//        console.log($(this).serialize());
            $.ajax({
                url: '/manager/addmathplayer',
                type: 'GET',
                dataType: 'json',
                data: $(this).serialize(),
                success: function (data) {
                    if (data['status'] == "true") {
                        $('#match-player-list').html("");
                        $.each(data['data'], function (key, value) {
                            $('#match-player-list').append("<tr id='mathplayer-" + value['id'] + "'><td>" + value['team'] + "</td><td>" + value['minute'] + "</td><td>" + value['name'] + "</td><td>" + value['status'] + "</td><td><button onclick='removeMatchPlayer(" + value['id'] + ")' class='btn btn-outline-danger'><span class='oi oi-trash'></span></button></td></tr>")
                        });
                        $('#form-match-player').find('input[name="match_player[minute]"]').first().val("");
                        $('#form-match-player').find('input[name="match_player[player_id]"]').first().val("");
                    }
                }
            });
        });

        $(document).on("click", "td.comment_match", function (e) {
            var id = $(this).attr('data-id');
            if (confirm("ยืนยันการ Comment นี้")) {
                $.ajax({
                    url: "/manager/removeComment",
                    data: {id: id},
                    method: "GET",
                    dataType: "json"
                }).done(function (response) {
                    if (response['status']) {
                        $('tbody#view-comment').find("td[data-id='" + id + "']").remove();
                    }
                });
            }
        });

        $(document).on("click", "td.review_match", function (e) {
            var id = $(this).attr('data-id');
            if (confirm("ยืนยันการ ลบ Review นี้")) {
                $.ajax({
                    url: "/manager/removeReview",
                    data: {id: id},
                    method: "GET",
                    dataType: "json"
                }).done(function (response) {
                    if (response['status']) {
                        $('tbody#view-review').find("td[data-id='" + id + "']").remove();
                    }
                });
            }
        });

        $(document).on("change", "select[name='match[team_away]']", function (e) {
//        console.log($('#form-manageMatch').find("select[name='match[team_away]']:not(:disabled)").val()+" :: "+$('#form-manageMatch').find("select[name='match[team_home]']:not(:disabled)").val());
            if ($("select[name='match[team_away]']:not(:disabled)").val() == $("select[name='match[team_home]']:not(:disabled)").val()) {
                $('#error-team_away').html('กรุณาเลือก ทีมใหม่');
                $('#addMatch').attr('disabled', 'disabled');
            } else {
                $('#error-team_away').html('');
                $('#error-team_home').html('');
                $('#addMatch').removeAttr('disabled');
            }
        });

        $(document).on("change", "select[name='match[team_home]']", function (e) {
//        console.log($('#form-manageMatch').find("select[name='match[team_away]']:not(:disabled)").val()+" :: "+$('#form-manageMatch').find("select[name='match[team_home]']:not(:disabled)").val());
            if ($("select[name='match[team_away]']:not(:disabled)").val() == $("select[name='match[team_home]']:not(:disabled)").val()) {
                $('#error-team_home').html('กรุณาเลือก ทีมใหม่');
                $('#addMatch').attr('disabled', 'disabled');
            } else {
                $('#error-team_away').html('');
                $('#error-team_home').html('');
                $('#addMatch').removeAttr('disabled');
            }
        });

        $(document).on("change", "select#select-league", function (e) {
            $('select.team-home').attr('disabled', 'disabled');
            $('select.team-away').attr('disabled', 'disabled');
            $('select.team-home').addClass('hide');
            $('select.team-away').addClass('hide');
            if ($(this).val() != "") {
                $('select#team-home-' + $(this).val()).removeAttr('disabled');
                $('select#team-away-' + $(this).val()).removeAttr('disabled');
                $('select#team-home-' + $(this).val()).removeClass('hide');
                $('select#team-away-' + $(this).val()).removeClass('hide');
            }
        });
        $("input#dateSelect").datepicker({dateFormat: 'dd/mm/yy'}).datepicker("setDate", new Date());
        $('input#timeSelect').timepicker({timeFormat: 'HH:mm', zindex: 1500});
    </script>
<?php include 'layouts/footer.php'; ?>
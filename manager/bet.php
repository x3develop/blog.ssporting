<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/6/2017
 * Time: 21:24
 */
if (!isset($_SESSION)) {
    session_start();
}
?>
<?php include '_database.php';?>
<?php
if(isset($_POST['bet'])){
    if(isset($_SESSION['login']) && ($_SESSION['login']['coupon']>0) && ($_SESSION['login']['coin']>=$_POST['bet']['use_bet_coin']) && strtotime(date("Y-m-d H:i:s"))<strtotime($_POST['bet']['time_match'])) {
        $sql = "INSERT INTO `bet` (`user_id`,`match_id`,`team`,`use_bet_coin`,`use_bet_gold`,`use_bet_coupon`,`status`,`created_at`,`updated_at`)";
        $sql .= " VALUES ('" . $_POST['bet']['user_id'] . "','" . $_POST['bet']['match_id'] . "','" . $_POST['bet']['team'] . "','" . $_POST['bet']['use_bet_coin'] . "','" . $_POST['bet']['use_bet_gold'] . "','" . $_POST['bet']['use_bet_coupon'] . "','" . $_POST['bet']['status'] . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
        $sth = $dbh->prepare($sql)->execute();
        if ($sth) {
            $sql = "UPDATE `user` set `coin`=" . ($_SESSION['login']['coin']-$_POST['bet']['use_bet_coin']) . ",`coupon`=" . ($_SESSION['login']['coupon']-1) . ",`updated_at`='" . date("Y-m-d H:i:s") . "' where id=" . $_SESSION['login']['id'];
            $sth = $dbh->prepare($sql)->execute();
            if($sth) {
                $comment = "ได้ทำการ bet คู่ที่ " . $_POST['bet']['match_id'] . " เป็นจำนวน " . $_POST['bet']['use_bet_coin'] . " โดยใช้ " . $_POST['bet']['use_bet_coupon'] . " คูปอง";
                $sql = "INSERT INTO `log_income` (`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
                $sql .= " VALUES (" . $_POST['bet']['user_id'] . "," . $_SESSION['login']['gold'] . "," . $_SESSION['login']['coin'] . "," . $_SESSION['login']['coupon'] . ",'bet','coin&gold','minus'," . $_SESSION['login']['gold'] . "," . ($_SESSION['login']['coin']-$_POST['bet']['use_bet_coin']) . "," . ($_SESSION['login']['coupon']-1) . ",'" . $comment . "','" . date("Y-m-d") . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                $sth = $dbh->prepare($sql)->execute();
                if ($sth) {
                    $_SESSION['login']['coin']=($_SESSION['login']['coin']-$_POST['bet']['use_bet_coin']);
                    $_SESSION['login']['coupon']=($_SESSION['login']['coupon']-1);
                    header('Location: /bet?match_id=' . $_POST['bet']['match_id']);
                    exit;
                } else {
                    header('Location: /bet?match_id=' . $_POST['bet']['match_id'] . '#betUpdateLogIncome');
                    exit;
                }
            } else {
                header('Location: /bet?match_id='.$_POST['bet']['match_id'].'#betUpdateUser');
                exit;
            }
        }else{
            header('Location: /bet?match_id='.$_POST['bet']['match_id'].'#betFlase');
            exit;
        }
    }else{
        if(($_SESSION['login']['coupon']<=0)) {
            header('Location: /bet?match_id=' . $_POST['bet']['match_id'] . '#betBoupon');
            exit;
        }elseif(($_SESSION['login']['coin']<=$_POST['bet']['use_bet_coin'])){
            header('Location: /bet?match_id=' . $_POST['bet']['match_id'] . '#betCoinMin');
            exit;
        }elseif(strtotime(date("Y-m-d H:i:s"))>=strtotime($_POST['bet']['time_match'])){
            header('Location: /bet?match_id=' . $_POST['bet']['match_id'] . '#betTimeMatch');
            exit;
        }
    }
}
if(isset($_POST['comment'])){
    $sql = "INSERT INTO `comment_match` (`user_id`,`match_id`,`team`,`comment`,`created_at`,`updated_at`)";
    $sql.=" VALUES ('" . $_POST['comment']['user_id'] . "','" . $_POST['comment']['match_id'] . "','".$_POST['comment']['team']."','".$_POST['comment']['comment']."','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
    $sth = $dbh->prepare($sql)->execute();
    header('Location: /bet?match_id='.$_POST['comment']['match_id'].'#betBoupon');
    exit;
}
if(isset($_GET['match_id'])){
    $sth = $dbh->prepare("select `match`.*,`league`.name,`league`.path,`gamble`.home_water_bill,`gamble`.handicap,`gamble`.away_water_bill from `match` left join `gamble` on `gamble`.match_id=`match`.id left join `league` on `league`.id=`match`.league_id where `match`.id=".$_GET['match_id']);
    $sth->execute();
    $match=$sth->fetch();

    $sth = $dbh->prepare("select id,name_en from `team` where id in (".$match['team_home'].",".$match['team_away'].")");
    $sth->execute();
    $selectTeam = $sth->fetchAll(PDO::FETCH_COLUMN|PDO::FETCH_GROUP);
    $webroot=$_SERVER['DOCUMENT_ROOT'];

    $sth = $dbh->prepare("select review_match.*,user_review.path,user_review.name from review_match LEFT JOIN user_review on user_review.id=review_match.user_review_id WHERE team='home' and match_id=".$_GET['match_id']);
    $sth->execute();
    $leagueReviewMatchHome = $sth->fetchAll();

    $sth = $dbh->prepare("select review_match.*,user_review.path,user_review.name from review_match LEFT JOIN user_review on user_review.id=review_match.user_review_id WHERE team='away' and match_id=".$_GET['match_id']);
    $sth->execute();
    $leagueReviewMatchAway = $sth->fetchAll();

    $sth = $dbh->prepare("select comment_match.*,user.path,user.username from comment_match LEFT JOIN `user` on `user`.id=comment_match.user_id WHERE match_id=".$_GET['match_id']);
    $sth->execute();
    $commentMatch = $sth->fetchAll();

    $sth = $dbh->prepare("select user.path from `bet` inner join `user` on `user`.id=`bet`.user_id where `bet`.team='home' and match_id=".$_GET['match_id']." group by user.id limit 20");
    $sth->execute();
    $betUserHome = $sth->fetchAll();

    $sth = $dbh->prepare("select user.path from `bet` inner join `user` on `user`.id=`bet`.user_id where `bet`.team='away' and match_id=".$_GET['match_id']." group by user.id limit 20");
    $sth->execute();
    $betUserAway = $sth->fetchAll();

    if(isset($_SESSION['login'])) {
        $sth = $dbh->prepare("select * from `bet` where user_id=? and match_id=?");
        $sth->execute([$_SESSION['login']['id'], $_GET['match_id']]);
        $bet = $sth->fetch();
    }
}else{
    header("Location: /index.php");
    exit;
}
?>
<?php include 'layouts/header';?>
<div class="row p-3">
    <div class="col" style="border: solid 1px;">
        <div class="row mb-1">
            <?php $date = new DateTime($match['time_match']); ?>
            <div class="col">
                <div class="col p-2" style="text-align: center;"><?php echo $date->format('M-d H:i'); ?> <?= $match['name']?></div>
                <div class="row">
                    <div class="col-5">
                        <div style="text-align: center;"><img style="background-image:none;" src="<?php echo ((!empty($match['team_home'])&& file_exists($webroot."/images/team/".$match['team_home']."/".$match['team_home']."_64x64.png"))?"/images/team/".$match['team_home']."/".$match['team_home']."_64x64.png":'/images/team/team_default_64x64.png')?>"></div>
                        <div style="text-align: center;"><?= $selectTeam[$match['team_home']][0] ?></div>
                        <div style="text-align: center">
                            <?php if(empty($bet) and $match['status']=="bet" and (strtotime(date("Y-m-d H:i:s"))<strtotime($match['time_match']))){ ?>
                            <form class="p-1" style="float: left;width: 20%;" method="post">
                                <input name="bet[time_match]" type="hidden" value="<?= $match['time_match']; ?>">
                                <input name="bet[user_id]" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
                                <input name="bet[match_id]" type="hidden" value="<?= $_GET['match_id']?>">
                                <input name="bet[team]" type="hidden" value="home">
                                <input name="bet[use_bet_coin]" type="hidden" value="100">
                                <input name="bet[use_bet_gold]" type="hidden" value="0">
                                <input name="bet[use_bet_coupon]" type="hidden" value="1">
                                <input name="bet[status]" type="hidden" value="bet">
                                <button style="width: 100%;" onclick="return confirm('ยืนยันการแทง ทีมเจ้าบ้าน 100 Coin')" class="btn btn-outline-info">100c</button>
                            </form>
                            <form class="p-1" style="float: left;width: 20%;" method="post">
                                <input name="bet[time_match]" type="hidden" value="<?= $match['time_match']; ?>">
                                <input name="bet[user_id]" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
                                <input name="bet[match_id]" type="hidden" value="<?= $_GET['match_id']?>">
                                <input name="bet[team]" type="hidden" value="home">
                                <input name="bet[use_bet_coin]" type="hidden" value="200">
                                <input name="bet[use_bet_gold]" type="hidden" value="0">
                                <input name="bet[use_bet_coupon]" type="hidden" value="1">
                                <input name="bet[status]" type="hidden" value="bet">
                                <button style="width: 100%;" onclick="return confirm('ยืนยันการแทง ทีมเจ้าบ้าน 200 Coin')" class="btn btn-outline-info">200c</button>
                            </form>
                            <form class="p-1" style="float: left;width: 20%;" method="post">
                                <input name="bet[time_match]" type="hidden" value="<?= $match['time_match']; ?>">
                                <input name="bet[user_id]" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
                                <input name="bet[match_id]" type="hidden" value="<?= $_GET['match_id']?>">
                                <input name="bet[team]" type="hidden" value="home">
                                <input name="bet[use_bet_coin]" type="hidden" value="400">
                                <input name="bet[use_bet_gold]" type="hidden" value="0">
                                <input name="bet[use_bet_coupon]" type="hidden" value="1">
                                <input name="bet[status]" type="hidden" value="bet">
                                <button style="width: 100%;" onclick="return confirm('ยืนยันการแทง ทีมเจ้าบ้าน 400 Coin')" class="btn btn-outline-warning">400c</button>
                            </form>
                            <form class="p-1" style="float: left;width: 20%;" method="post">
                                <input name="bet[time_match]" type="hidden" value="<?= $match['time_match']; ?>">
                                <input name="bet[user_id]" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
                                <input name="bet[match_id]" type="hidden" value="<?= $_GET['match_id']?>">
                                <input name="bet[team]" type="hidden" value="home">
                                <input name="bet[use_bet_coin]" type="hidden" value="750">
                                <input name="bet[use_bet_gold]" type="hidden" value="0">
                                <input name="bet[use_bet_coupon]" type="hidden" value="1">
                                <input name="bet[status]" type="hidden" value="bet">
                                <button style="width: 100%;" onclick="return confirm('ยืนยันการแทง ทีมเจ้าบ้าน 750 Coin')" class="btn btn-outline-danger">750c</button>
                            </form>
                            <form class="p-1" style="float: left;width: 20%;" method="post">
                                <input name="bet[time_match]" type="hidden" value="<?= $match['time_match']; ?>">
                                <input name="bet[user_id]" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
                                <input name="bet[match_id]" type="hidden" value="<?= $_GET['match_id']?>">
                                <input name="bet[team]" type="hidden" value="home">
                                <input name="bet[use_bet_coin]" type="hidden" value="1200">
                                <input name="bet[use_bet_gold]" type="hidden" value="0">
                                <input name="bet[use_bet_coupon]" type="hidden" value="1">
                                <input name="bet[status]" type="hidden" value="bet">
                                <button style="width: 100%;" onclick="return confirm('ยืนยันการแทง ทีมเจ้าบ้าน 1200 Coin')" class="btn btn-outline-danger">1200c</button>
                            </form>
                            <?php } ?>
                        </div>
                        <div style="">
                            <?php if(!empty($betUserHome)){
                                foreach ($betUserHome as $key=>$value){ ?>
                                    <img style="height: 25px;" src="<?= ((!empty($value['path']))?$value['path']:'/images/avatar.png')?>" alt="..." class="img-thumbnail">
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-2">
                        <div style="text-align: center;">
                            (<?= ((!empty($match['home_water_bill']))?$match['home_water_bill']:"-") ?>)-[ <?= ((!empty($match['handicap']))?$match['handicap']:"-") ?> ]-(<?= ((!empty($match['away_water_bill']))?$match['away_water_bill']:"-") ?>)
                        </div>
                        <div style="text-align: center;font-size: 36px;">
                            <span class="text-info"><?= ((!empty($match['home_end_time_score']))?$match['home_end_time_score']:"0") ?></span>
                            -
                            <span class="text-info"><?= ((!empty($match['away_end_time_score']))?$match['away_end_time_score']:"0") ?></span>
                        </div>
                        <?php if(!empty($bet)){ ?>
                        <div style="text-align: center;">
                            <div><?= $bet['team'] ?></div>
                            <div><?= number_format($bet['use_bet_coin']) ?></div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="col-5">
                        <div style="text-align: center;"><img src="<?php echo ((!empty($match['team_away'])&& file_exists($webroot."/images/team/".$match['team_away']."/".$match['team_away']."_64x64.png"))?"/images/team/".$match['team_away']."/".$match['team_away']."_64x64.png":'/images/team/team_default_64x64.png')?>"></div>
                        <div style="text-align: center;"><?= $selectTeam[$match['team_away']][0] ?></div>
                        <div style="text-align: center">
                            <?php if(empty($bet) and $match['status']=="bet" and (strtotime(date("Y-m-d H:i:s"))<strtotime($match['time_match']))){ ?>
                            <form class="p-1" style="float: left;width: 20%;" method="post">
                                <input name="bet[time_match]" type="hidden" value="<?= $match['time_match']; ?>">
                                <input name="bet[user_id]" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
                                <input name="bet[match_id]" type="hidden" value="<?= $_GET['match_id']?>">
                                <input name="bet[team]" type="hidden" value="away">
                                <input name="bet[use_bet_coin]" type="hidden" value="100">
                                <input name="bet[use_bet_gold]" type="hidden" value="0">
                                <input name="bet[use_bet_coupon]" type="hidden" value="1">
                                <input name="bet[status]" type="hidden" value="bet">
                                <button style="width: 100%;" onclick="return confirm('ยืนยันการแทง ทีมเยือน 100 Coin')" class="btn btn-outline-info">100c</button>
                            </form>
                            <form class="p-1" style="float: left;width: 20%;" method="post">
                                <input name="bet[time_match]" type="hidden" value="<?= $match['time_match']; ?>">
                                <input name="bet[user_id]" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
                                <input name="bet[match_id]" type="hidden" value="<?= $_GET['match_id']?>">
                                <input name="bet[team]" type="hidden" value="away">
                                <input name="bet[use_bet_coin]" type="hidden" value="200">
                                <input name="bet[use_bet_gold]" type="hidden" value="0">
                                <input name="bet[use_bet_coupon]" type="hidden" value="1">
                                <input name="bet[status]" type="hidden" value="bet">
                                <button style="width: 100%;" onclick="return confirm('ยืนยันการแทง ทีมเยือน 200 Coin')" class="btn btn-outline-info">200c</button>
                            </form>
                            <form class="p-1" style="float: left;width: 20%;" method="post">
                                <input name="bet[time_match]" type="hidden" value="<?= $match['time_match']; ?>">
                                <input name="bet[user_id]" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
                                <input name="bet[match_id]" type="hidden" value="<?= $_GET['match_id']?>">
                                <input name="bet[team]" type="hidden" value="away">
                                <input name="bet[use_bet_coin]" type="hidden" value="400">
                                <input name="bet[use_bet_gold]" type="hidden" value="0">
                                <input name="bet[use_bet_coupon]" type="hidden" value="1">
                                <input name="bet[status]" type="hidden" value="bet">
                                <button style="width: 100%;" onclick="return confirm('ยืนยันการแทง ทีมเยือน 400 Coin')" class="btn btn-outline-warning">400c</button>
                            </form>
                            <form class="p-1" style="float: left;width: 20%;" method="post">
                                <input name="bet[time_match]" type="hidden" value="<?= $match['time_match']; ?>">
                                <input name="bet[user_id]" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
                                <input name="bet[match_id]" type="hidden" value="<?= $_GET['match_id']?>">
                                <input name="bet[team]" type="hidden" value="away">
                                <input name="bet[use_bet_coin]" type="hidden" value="750">
                                <input name="bet[use_bet_gold]" type="hidden" value="0">
                                <input name="bet[use_bet_coupon]" type="hidden" value="1">
                                <input name="bet[status]" type="hidden" value="bet">
                                <button style="width: 100%;" onclick="return confirm('ยืนยันการแทง ทีมเยือน 750 Coin')" class="btn btn-outline-danger">750c</button>
                            </form>
                            <form class="p-1" style="float: left;width: 20%;" method="post">
                                <input name="bet[time_match]" type="hidden" value="<?= $match['time_match']; ?>">
                                <input name="bet[user_id]" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
                                <input name="bet[match_id]" type="hidden" value="<?= $_GET['match_id']?>">
                                <input name="bet[team]" type="hidden" value="away">
                                <input name="bet[use_bet_coin]" type="hidden" value="1200">
                                <input name="bet[use_bet_gold]" type="hidden" value="0">
                                <input name="bet[use_bet_coupon]" type="hidden" value="1">
                                <input name="bet[status]" type="hidden" value="bet">
                                <button style="width: 100%;" onclick="return confirm('ยืนยันการแทง ทีมเยือน 1/00 Coin')" class="btn btn-outline-danger">1200c</button>
                            </form>
                            <?php } ?>
                        </div>
                        <div style="">
                            <?php if(!empty($betUserAway)){
                                foreach ($betUserAway as $key=>$value){ ?>
                                    <img style="height: 25px;" src="<?= ((!empty($value['path']))?$value['path']:'/images/avatar.png')?>" alt="..." class="img-thumbnail">
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row p-3">
    <div class="col" style="border: solid 1px;padding-top: 5px;">
        <?php foreach ($leagueReviewMatchHome as $key=>$value){ ?>
            <div style="margin-bottom:5px;height: 50px;padding: 5px;border: solid 1px; border-radius: 5px;">
                <div style="float:left;width:10%;height: 100%;">
                    <img style="width: 100%;" src="<?= ((!empty($value['path']))?$value['path']:'/images/avatar.png') ?>">
                </div>
                <div style="float:left;width:90%;padding-left: 5px;">
                    <div style="font-size: 10px;line-height: 90%;color: #777;"><?= date("M d H:i",strtotime($value['updated_at']))?> By <?= $value['name']?></div>
                    <div><?= $value['review_text']?></div>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="col" style="border: solid 1px;padding-top: 5px;">
        <?php foreach ($leagueReviewMatchAway as $key=>$value){ ?>
            <div style="margin-bottom:5px;height: 50px;padding: 5px;border: solid 1px; border-radius: 5px;">
                <div style="float:left;width:10%;height: 100%;">
                    <img style="width: 100%;" src="<?= ((!empty($value['path']))?$value['path']:'/images/avatar.png') ?>">
                </div>
                <div style="float:left;width:90%;padding-left: 5px;">
                    <div style="font-size: 10px;line-height: 90%;color: #777;"><?= date("M d H:i",strtotime($value['updated_at']))?> By <?= $value['name']?></div>
                    <div><?= $value['review_text']?></div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row p-3">
    <div class="col" style="border: solid 1px;padding-bottom: 5px;">
        <?php foreach ($commentMatch as $key=>$value){ ?>
            <?php if($value['team']=='home'){ ?>
                <div style="width: 100%;margin-top: 5px;">
                    <div style="width: 50%;float: left;">
                        <div style="height: 50px;width:10%;float: left;"><img style="height: 100%;" src="<?=((!empty($value['path'])) ? $value['path'] : '/images/avatar.png')?>"></div>
                        <div style="padding: 5px;width:90%;float: left;border: solid 1px #7babc3;border-radius: 5px;background-color: #d9edf7;">
                            <div style="word-wrap:break-word;"><?= $value['comment']; ?></div>
                            <div style="text-align: right;font-size: 10px;color: #777;line-height: 90%;"><?= date("M d H:i",strtotime($value['updated_at']))?> By <?= $value['username']?></div>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            <?php }else if($value['team']=='away'){ ?>
                <div style="width: 100%;margin-top: 5px;">
                    <div style="width: 50%;float: right;">
                        <div style="padding: 5px;width:90%;float: left;border: solid 1px #7babc3;border-radius: 5px;background-color: #d9edf7;">
                            <div style="word-wrap:break-word;"><?= $value['comment']; ?></div>
                            <div style="text-align: right;font-size: 10px;color: #777;line-height: 90%;"><?= date("M d H:i",strtotime($value['updated_at']))?> By <?= $value['username']?></div>
                        </div>
                        <div style="height: 50px;width:10%;float: left;"><img style="height: 100%;float: right;" src="<?=((!empty($value['path'])) ? $value['path'] : '/images/avatar.png')?>"></div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            <?php } ?>
        <?php } ?>
        <?php if(isset($_SESSION['login']) && isset($_GET['match_id'])){ ?>
        <div style="width: 100%;margin-top: 10px;">
            <div style="width:50%;float: left;">
                <form method="post" class="form-inline" style="100%;">
                    <input name="comment[user_id]" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
                    <input name="comment[match_id]" type="hidden" value="<?= $_GET['match_id']?>">
                    <input name="comment[team]" type="hidden" value="home">
                    <input style="width: 90%;" name="comment[comment]" type="text" class="form-control" placeholder="comment...">
<!--                    <button type="submit" class="btn btn-primary">Submit</button>-->
                    <img style="width: 8%;margin: 0px 1%;" src="<?= ((!empty($_SESSION['login']['path']))?$_SESSION['login']['path']:'/images/avatar.png')?>" alt="..." class="img-thumbnail">
                </form>
            </div>
            <div style="width:50%;float: left;">
                <form method="post" class="form-inline" style="100%;">
                    <input name="comment[user_id]" type="hidden" value="<?= $_SESSION['login']['id']; ?>">
                    <input name="comment[match_id]" type="hidden" value="<?= $_GET['match_id']?>">
                    <input name="comment[team]" type="hidden" value="away">
                    <input style="width: 90%;" name="comment[comment]" type="text" class="form-control" placeholder="comment...">
                    <!--                    <button type="submit" class="btn btn-primary">Submit</button>-->
                    <img style="width: 8%;margin: 0px 1%;" src="<?= ((!empty($_SESSION['login']['path']))?$_SESSION['login']['path']:'/images/avatar.png')?>" alt="..." class="img-thumbnail">
                </form>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<div class="modal fade in" tabindex="-1" role="dialog" id="modelAlert">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Alert Box</h4>
            </div>
            <div class="modal-body">
                <p id="TextAlertBox"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!--                <button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    $(document).ready(function () {
        console.log(document.location.hash);
        if (document.location.hash == "#betTimeMatch") {
            $('#TextAlertBox').html("ขออถัย คู่บอลคู่นี้ได้ หมดเวลาในการทายผลแล้ว.");
            $('#modelAlert').modal('show');
        } else if (document.location.hash == "#betUpdateLogIncome") {
            $('#TextAlertBox').html("ขออถัย ไม่สามารถบันทึก ข้อมูลการเข้าออกข้อมูลได่.");
            $('#modelAlert').modal('show');
        } else if (document.location.hash == "#betUpdateUser") {
            $('#TextAlertBox').html("ขออถัย ไม่สามารถบันทึกข้อมูล สมาชิกได้.");
            $('#modelAlert').modal('show');
        } else if (document.location.hash == "#betFlase") {
            $('#TextAlertBox').html("ขออถัย ไม่สามารถบันทึกการแทงได้.");
            $('#modelAlert').modal('show');
        } else if (document.location.hash == "#betBoupon") {
            $('#TextAlertBox').html("ขออถัย คูปองคุณเพียงพอ.");
            $('#modelAlert').modal('show');
        } else if (document.location.hash == "#betCoinMin") {
            $('#TextAlertBox').html("ขออถัย เหรียญ คุณไม่เพียงพอ.");
            $('#modelAlert').modal('show');
        }
    });
</script>

<?php include 'layouts/footer.php';?>

<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/3/2017
 * Time: 1:20
 */
include '_database.php';
$html="";
if(isset($_GET['id'])) {
    $sth = $dbh->prepare("select review_match.*,user_review.path from review_match LEFT JOIN user_review on user_review.id=review_match.user_review_id WHERE team='home' and match_id=".$_GET['id']);
    $sth->execute();
    $leagueReviewMatchHome = $sth->fetchAll();

    $sth = $dbh->prepare("select review_match.*,user_review.path from review_match LEFT JOIN user_review on user_review.id=review_match.user_review_id WHERE team='away' and match_id=".$_GET['id']);
    $sth->execute();
    $leagueReviewMatchAway = $sth->fetchAll();

    if(!empty($leagueReviewMatchHome) and count($leagueReviewMatchHome)>=count($leagueReviewMatchAway)){
        foreach ($leagueReviewMatchHome as $key=>$value){
            $html.="<tr><td class='review_match' data-id='".$value['id']."'><div style='width: 30%;float: left;'><img style='width: 100%' src=".((!empty($value['path']))?$value['path']:'/images/avatar.png')."></div><div style='width: 70%;float: left;padding-left: 5px;'>".$value['review_text']."</div></td>";
//            $html.="<td>";
            if(!empty($leagueReviewMatchAway[$key]['review_text'])) {
                $html .= "<td class='review_match' data-id='".$leagueReviewMatchAway[$key]['id']."'><div style='width: 30%;float: left;'><img style='width: 100%' src=" . ((!empty($leagueReviewMatchAway[$key]['path'])) ? $leagueReviewMatchAway[$key]['path'] : '/images/avatar.png') . "></div>";
            }else{
                $html.="<td class='review_match' data-id='".$leagueReviewMatchAway[$key]['id']."'>";
            }
            $html .= "<div style='width: 70%;float: left;padding-left: 5px;'>".((!empty($leagueReviewMatchAway[$key]['review_text']))?$leagueReviewMatchAway[$key]['review_text']:'')."</div></td></tr>";
        }
    }elseif (!empty($leagueReviewMatchAway) and count($leagueReviewMatchAway)>count($leagueReviewMatchHome)){
        foreach ($leagueReviewMatchAway as $key=>$value){
            $html.="<tr>";
            if(!empty($leagueReviewMatchHome[$key]['review_text'])) {
                $html .= "<td class='review_match' data-id='".$leagueReviewMatchHome[$key]['id']."'><div style='width: 30%;float: left;'><img style='width: 100%' src=" . ((!empty($leagueReviewMatchHome[$key]['path'])) ? $leagueReviewMatchHome[$key]['path'] : '/images/avatar.png') . "></div>";
            }else{
                $html.="<td>";
            }
            $html .= "<div style='width: 70%;float: left;padding-left: 5px;'>".((!empty($leagueReviewMatchHome[$key]['review_text']))?$leagueReviewMatchHome[$key]['review_text']:'')."</div></td>";
            $html.="<td class='review_match' data-id='".$value['id']."'><div style='width: 30%;float: left;'><img style='width: 100%' src=".((!empty($value['path']))?$value['path']:'/images/avatar.png')."></div><div style='width: 70%;float: left;padding-left: 5px;'>".$value['review_text']."</div></td>";
            $html.="</tr>";
        }
    }
}
$result['html']=$html;
echo json_encode($result);
?>
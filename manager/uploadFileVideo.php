<?php
header('Access-Control-Allow-Origin: *');
?>
<?php include '_config.php';?>
<?php include '_database.php';?>
<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 2/15/2017
 * Time: 15:27
 */
    $id = ($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
    $now = new DateTime();
    if (!empty($_FILES)) {
        $webroot=$_SERVER['DOCUMENT_ROOT'];
        $targetPath = $webroot . '/images/video/'.$id.'/';
        for($i=0;$i<count($_FILES['Filedata']['tmp_name']);$i++){

            $tempFile = $_FILES['Filedata']['tmp_name'][$i];
            $nameFile = $_FILES['Filedata']['name'][$i];
            $typeFile= $_FILES['Filedata']['type'][$i];

            if (!is_dir($webroot . '/images/' . 'video')) {
                mkdir($webroot . '/images/' . 'video', 0777);
                chmod($webroot . '/images/' . 'video', 0777);
            }

            if (!is_dir($webroot . '/images/' . 'video/'.$id)) {
                mkdir($webroot . '/images/' . 'video/'.$id, 0777);
                chmod($webroot . '/images/' . 'video/'.$id, 0777);
            }

//            Resize Image
            $maxDimW = 275;
            $maxDimH = 155;
            list($width, $height, $type, $attr) = getimagesize( $tempFile );
            if ( $width > $maxDimW || $height > $maxDimH ) {
//                $target_filename = $tempFile;
                $fn = $tempFile;
                $size = getimagesize( $fn );
                $ratio = $width/$height; // width/height
                if( $ratio > 1) {
                    $width = $maxDimW;
                    $height = $maxDimW/$ratio;
                }else{
                    $width = $maxDimH*$ratio;
                    $height = $maxDimH;
                }
                $src = imagecreatefromstring(file_get_contents($fn));
                $dst = imagecreatetruecolor( $width, $height );
                imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
                imagedestroy( $src );
                imagejpeg($dst, str_replace('//', '/', $targetPath) . $id."-index-video".strrchr($nameFile, '.')); // adjust format as needed
                imagedestroy( $dst );
            }

            $size=GetimageSize($tempFile);
            $file = str_replace('//', '/', $targetPath) . $id.'_'.$size[0].'x'.$size[1].strrchr($nameFile, '.');
            move_uploaded_file($tempFile, $file);
            $fileName = str_replace($webroot, '', $file);
            chmod($file, 0777);

        }
        $sql = "UPDATE video_highlight set thumbnail='".$fileName."' WHERE video_id=".$id;
        $sth = $dbh->prepare($sql)->execute();
        echo json_encode($fileName);
    }
?>
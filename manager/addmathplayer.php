<?php include '_config.php';?>
<?php include '_database.php';?>
<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/27/2017
 * Time: 10:41
 */
$MatchPlayer=$_GET['match_player'];
$MatchPlayerId=$_GET['match_player_id'];
$dataReturn=array();
$matchPlayer=array();
if(!empty($MatchPlayerId)){
    $sth = $dbh->prepare("select * from `match_player` where id='".$MatchPlayerId."'");
    $sth->execute();
    $MatchPlayer=$sth->fetch();
    $sth = $dbh->prepare("Delete from `match_player` where id=".$MatchPlayerId);
    if($sth->execute()){

        $sth = $dbh->prepare("select sum(CASE WHEN team='home' && (status='goal'||status='freekick'||status='penalty'||status='Owngoal') then 1 else 0 end) as Fullhome,
sum(CASE WHEN team='away' && (status='goal'||status='freekick'||status='penalty'||status='Owngoal') then 1 else 0 end) as Fullaway,
sum(CASE WHEN team='home' && (status='overgoal'||status='overfreekick'||status='overpenalty') then 1 else 0 end) as Overhome,
sum(CASE WHEN team='away' && (status='overgoal'||status='overfreekick'||status='overpenalty') then 1 else 0 end) as Overaway 
from match_player where match_player.match_id=".$MatchPlayer['match_id']);
        $sth->execute();
        $score = $sth->fetch();
        if(!empty($score)) {
            $sql = "UPDATE `match` set `home_end_time_score`=" .$score['Fullhome']. ",`home_over_time_score`=" .$score['Overhome']. ",`away_end_time_score`=" .$score['Fullaway']. ",`away_over_time_score`=" .$score['Overaway']. ",`updated_at`='" . date("Y-m-d H:i:s") . "' where id=" . $MatchPlayer['match_id'];
            $sth = $dbh->prepare($sql)->execute();
        }

        $dataReturn=array('status' => 'true','write' => "ok", 'message' => 'Get data Success.', 'data' => $matchPlayer);
    }else{
        $dataReturn=array('status' => 'false','write' => "ok", 'message' => 'Get data Success.', 'data' => $matchPlayer);
    }
}elseif(!empty($MatchPlayer)){
    if(!empty($MatchPlayer['player_id'])) {
        $sth = $dbh->prepare("select id from `player` where name='".$MatchPlayer['player_id']."'");
        $sth->execute();
        $player = $sth->fetch();
        $sth = $dbh->prepare("select league_id from `match` where id='".$MatchPlayer['match_id']."'");
        $sth->execute();
        $match = $sth->fetch();
        if(!empty($player['id'])) {
            $sqlAddMatchPlayer = "INSERT INTO `match_player` (`league_id`,`match_id`, `player_id`, `minute`,`over_minute`,`team`, `status`,`created_at`,`updated_at`) VALUES (:league_id,:match_id,:player_id,:minute,:over_minute,:team,:status,:created_at,:updated_at)";
            $stmt = $dbh->prepare($sqlAddMatchPlayer);
            $stmt->bindParam(':league_id',$match['league_id']);
            $stmt->bindParam(':match_id', $MatchPlayer['match_id']);
            $stmt->bindParam(':player_id', $player['id']);
            $stmt->bindParam(':minute', $MatchPlayer['minute']);
            $stmt->bindParam(':over_minute', $MatchPlayer['over_minute']);
            $stmt->bindParam(':team', $MatchPlayer['team']);
            $stmt->bindParam(':status', $MatchPlayer['status']);
            $stmt->bindParam(':created_at', date("Y-m-d H:i:s"));
            $stmt->bindParam(':updated_at', date("Y-m-d H:i:s"));
            if ($stmt->execute()) {
                $sth = $dbh->prepare("select sum(CASE WHEN team='home' && (status='goal'||status='freekick'||status='penalty'||status='Owngoal') then 1 else 0 end) as Fullhome,
sum(CASE WHEN team='away' && (status='goal'||status='freekick'||status='penalty'||status='Owngoal') then 1 else 0 end) as Fullaway,
sum(CASE WHEN team='home' && (status='overgoal'||status='overfreekick'||status='overpenalty') then 1 else 0 end) as Overhome,
sum(CASE WHEN team='away' && (status='overgoal'||status='overfreekick'||status='overpenalty') then 1 else 0 end) as Overaway 
from match_player where match_player.match_id=".$MatchPlayer['match_id']);
                $sth->execute();
                $score = $sth->fetch();
                if(!empty($score)) {
                    $sql = "UPDATE `match` set `home_end_time_score`=" .$score['Fullhome']. ",`home_over_time_score`=" .$score['Overhome']. ",`away_end_time_score`=" .$score['Fullaway']. ",`away_over_time_score`=" .$score['Overaway']. ",`updated_at`='" . date("Y-m-d H:i:s") . "' where id=" . $MatchPlayer['match_id'];
                    $sth = $dbh->prepare($sql)->execute();
                }
                $sth = $dbh->prepare("select match_player.*,player.name,player.path from match_player 
inner join player on player.id=match_player.player_id
where match_player.match_id=".$MatchPlayer['match_id']." order by match_player.minute ASC");
                $sth->execute();
                $matchPlayer = $sth->fetchAll();
                $dataReturn=array('status' => 'true','write' => "ok", 'message' => 'Get data Success.', 'data' => $matchPlayer);
            } else {
                $dataReturn=array('status' => 'false','write' => "ok", 'message' => 'Get data Success.', 'data' => $matchPlayer);
            }
        }else{
            $dataReturn=array('status' => 'false','write' => "ok", 'message' => 'Get data Success.', 'data' => $matchPlayer);
        }
    }else{
        $dataReturn=array('status' => 'false','write' => "ok", 'message' => 'Get data Success.', 'data' => $matchPlayer);
    }
}
echo json_encode($dataReturn);
?>

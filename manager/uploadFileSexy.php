<?php
header('Access-Control-Allow-Origin: *');
?>
<?php include '_config.php';?>
<?php include '_database.php';?>
<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 2/15/2017
 * Time: 15:27
 */
    $id = ($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
    $now = new DateTime();
    $dateReturn=array();
    if (!empty($_FILES)) {
        $webroot=$_SERVER['DOCUMENT_ROOT'];
        $targetPath = $webroot . '/images/gallery/'.$id.'/';
        for($i=0;$i<count($_FILES['Filedata']['tmp_name']);$i++){
            $tempFile = $_FILES['Filedata']['tmp_name'][$i];
            $nameFile = $_FILES['Filedata']['name'][$i];
            $typeFile= $_FILES['Filedata']['type'][$i];

            if (!is_dir($webroot . '/images/' . 'gallery')) {
                mkdir($webroot . '/images/' . 'gallery', 0777);
                chmod($webroot . '/images/' . 'gallery', 0777);
            }

            if (!is_dir($webroot . '/images/' . 'gallery/'.$id)) {
                mkdir($webroot . '/images/' . 'gallery/'.$id, 0777);
                chmod($webroot . '/images/' . 'gallery/'.$id, 0777);
            }

            $size=GetimageSize($tempFile);
            $file = str_replace('//', '/', $targetPath) .date("YmdHis").'_'.$i."_".$size[0].'x'.$size[1].strrchr($nameFile, '.');
            move_uploaded_file($tempFile, $file);
            $fileName = str_replace($webroot, '', $file);
            chmod($file, 0777);
            array_push($dateReturn,$fileName);

            $sql = "INSERT INTO `media_store` (`gall_id`,`path`,`thumbnail`,`created_at`,`remove`) VALUE (".$id.",'".$fileName."','".$fileName."','".date('Y-m-d')."','N');";
            $sth = $dbh->prepare($sql)->execute();

        }
        echo json_encode($dateReturn);
    }
?>
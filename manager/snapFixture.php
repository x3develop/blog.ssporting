<?php
header('Access-Control-Allow-Origin: *');
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
$playMatch = new playMatch();
$date=((isset($_GET['date']))?$_GET['date']:null);
$matchView = $playMatch->getMathByMainLeagueAndDate($date);
$matchList=array();
foreach ($matchView as $key=>$value){
    if(empty($matchList[$value->league_id])){
        $matchList[$value->league_id]=array();
        array_push($matchList[$value->league_id],$value);
    }else{
        array_push($matchList[$value->league_id],$value);
    }
}
$league=array();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Fixture</title>
    <link href="css/reset.css" rel="stylesheet">
    <link href="css/fixture.css" rel="stylesheet">
</head>
<body>
<div class="content-fixture">
    <div class="logo-ngoal">
        <img src="img/logo-ngoal-white.png">
    </div>
    <div class="content-fixture-topic">
        <table>
            <tr>
                <td><h1>FIXTURE</h1></td>
                <td>
                    <h2><?= date("d M Y", strtotime($date)) ?></h2>
                    <p>WWW.NGOAL.COM</p>
                </td>
            </tr>
        </table>
    </div>
    <div class="box-half">
        <?php foreach ($matchList as $keyM=>$valueM){ ?>
            <?php if(count($league)<4 and count($valueM)<6){ ?>
                <div class="box-fixture">
                    <div class="box-fixture-title">
                        <img src="<?=$valueM[0]->path;?>">
                        <h2><?=$valueM[0]->name?> </h2>
                    </div>
                    <div class="box-fixture-match">
                        <table>
                            <?php foreach ($valueM as $key=>$value){?>
                                <tr>
                                    <td><h2><?=strtoupper(substr($value->teamHomeEn,0,3))?></h2></td>
                                    <td><img src="<?=$value->teamHomePath?>"></td>
                                    <td class="box-vs">
                                        VS
                                    </td>
                                    <td><img src="<?=$value->teamAwayPath?>"></td>
                                    <td><h2><?=strtoupper(substr($value->teamAwayEn,0,3))?></h2></td>
                                    <td class="box-fixture-date">
                                        <p><?= date("d.m.Y", strtotime($value->time_match) - (60 * 60)) ?></p>
                                        <span><?= date("H:s", strtotime($value->time_match) - (60 * 60)) ?></span>
                                    </td>
                                    <td class="tv-live" style="text-align: center">
                                        <?php if(!empty($value->channel_match_id)){ ?>
                                            <?php if(!empty($value->channel_path)){ ?>
                                                <img src="<?=$value->channel_path;?>">
                                            <?php }else { ?>
                                                <?=$value->channel_name;?>
                                            <?php } ?>
                                        <?php }else{ ?>
                                            -
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
                <?php array_push($league,$keyM) ?>
            <?php } ?>
       <?php } ?>
    </div>
    <div class="box-half">
        <?php $league2=0; ?>
        <?php foreach ($matchList as $keyM=>$valueM){ ?>
            <?php if(count($valueM)>=6 and $league2<=2 and (count($matchList)>count($league) and !in_array($key,$league))){ ?>
                <div class="box-fixture">
                    <div class="box-fixture-title">
                        <img src="<?=$valueM[0]->path;?>">
                        <h2><?=$valueM[0]->name?></h2>
                    </div>
                    <div class="box-fixture-match">
                        <table>
                            <?php foreach ($valueM as $key=>$value){?>
                                <tr>
                                    <td><h2><?=substr($value->teamHomeEn,0,3)?></h2></td>
                                    <td><img src="<?=$value->teamHomePath?>"></td>
                                    <td class="box-vs">
                                        VS
                                    </td>
                                    <td><img src="<?=$value->teamAwayPath?>"></td>
                                    <td><h2><?=substr($value->teamAwayEn,0,3)?></h2></td>
                                    <td class="box-fixture-date">
                                        <p><?= date("d.m.Y", strtotime($value->time_match) - (60 * 60)) ?></p>
                                        <span><?= date("H:s", strtotime($value->time_match) - (60 * 60)) ?></span>
                                    </td>
                                    <td class="tv-live" style="text-align: center">
                                        <?php if(!empty($value->channel_match_id)){ ?>
                                            <?php if(!empty($value->channel_path)){ ?>
                                                <img src="<?=$value->channel_path;?>">
                                            <?php }else { ?>
                                                <?=$value->channel_name;?>
                                            <?php } ?>
                                        <?php }else{ ?>
                                            -
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            <?php } ?>
            <?php $league2++; ?>
        <?php } ?>
    </div>
</div>
</body>
</html>
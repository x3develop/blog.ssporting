<?php include '_database.php'; ?>
<?php
if (!isset($_SESSION)) {
    session_start();
}
if (isset($_GET['team'])) {
    if (!empty($_GET['team']['id'])) {
        $sql = "UPDATE team set league_id='" . $_GET['team']['league_id'] . "',name_en='" . $_GET['team']['name_en'] . "',name_th='" . $_GET['team']['name_th'] . "',`updated_at`='" . date("Y-m-d H:i:s") . "' WHERE id=" . $_GET['team']['id'];
    } else {
        $sql = "INSERT INTO team (`league_id`,`name_th`,`name_en`,`created_at`,`updated_at`) VALUES ('" . $_GET['team']['league_id'] . "','" . $_GET['team']['name_th'] . "','" . $_GET['team']['name_en'] . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
    }
    $sth = $dbh->prepare($sql)->execute();
    if (isset($_GET['team']['league_id'])) {
        header("Location: /manager/team?search[league_id]=" . $_GET['team']['league_id']);
    } else {
        header("Location: /manager/team");
    }
    exit;
}
if (isset($_GET['id'])) {
    $sth = $dbh->prepare("select * from team where id=" . $_GET['id']);
    $sth->execute();
    $team = $sth->fetch();
} elseif (isset($_GET['delete_id'])) {
    if(isset($_SESSION['login']) && !empty($_SESSION['login']['username']) && $_SESSION['login']['username']=="admin") {
        $sth = $dbh->prepare("delete from team where id=" . $_GET['delete_id']);
        $sth->execute();
    }
    header("Location: /manager/team");
    exit;
}

?>
<?php include 'layouts/header.php'; ?>
<?php
$sth = $dbh->prepare("select * from league ");
$sth->execute();
$leagueAll = $sth->fetchAll();

if (isset($_GET['search'])) {
    if (!empty($_GET['search']['name_en'])) {
        $sth = $dbh->prepare("select `team`.id,`team`.league_id,`team`.name_th,`team`.name_en,`team`.path,league.name as league from team left join league on league.id=team.league_id WHERE team.name_en='" . $_GET['search']['name_en'] . "'");
        $sth->execute();
        $teamAll = $sth->fetchAll();
    } else {
        $sth = $dbh->prepare("select `team`.id,`team`.league_id,`team`.name_th,`team`.name_en,`team`.path,league.name as league from team left join league on league.id=team.league_id WHERE team.league_id=" . $_GET['search']['league_id']);
        $sth->execute();
        $teamAll = $sth->fetchAll();
    }
} else {
    $sth = $dbh->prepare("select `team`.id,`team`.league_id,`team`.name_th,`team`.name_en,`team`.path,league.name as league from team left join league on league.id=team.league_id");
    $sth->execute();
    $teamAll = $sth->fetchAll();
}

$webroot = $_SERVER['DOCUMENT_ROOT'];
?>
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col-md-6">
                <h3>Manage Team</h3>
            </div>
            <div class="col-md-6">
                <form class="form-inline pull-right" style="margin-top: 20px;">
                    <div class="input-group">

                        <input name="search[name_en]" id="input-Team" type="text" class="form-control"
                               placeholder="ค้นหาทีม EN">
                        <!--                    <select name="search[league_id]" class="form-control">-->
                        <!--                        <option value="">League Select</option>-->
                        <!--                        --><?php //foreach ($leagueAll as $key=>$value){ ?>
                        <!--                            <option value="--><?php //echo $value['id'] ?><!--">-->
                        <?php //echo $value['name'] ?><!--</option>-->
                        <!--                        --><?php //} ?>
                        <!--                    </select>-->
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"
                                                                             aria-hidden="true"></i></button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
                <form class="form-horizontal">
                    <input type="hidden" name="team[id]" value="<?= ((!empty($team['id'])) ? $team['id'] : '') ?>">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">เลือกลีก</label>
                        <div class="col-sm-10">
                            <select required name="team[league_id]" class="form-control">
                                <option value="">League Select</option>
                                <?php foreach ($leagueAll as $key => $value) { ?>
                                    <option <?= ((!empty($team['league_id']) && $team['league_id'] == $value['id']) ? 'selected' : '') ?>
                                            value="<?php echo $value['id'] ?>"><?php echo $value['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ชื่อ[ภาษาไทย]</label>
                        <div class="col-sm-10">
                            <input required name="team[name_th]"
                                   value="<?= ((!empty($team['name_th'])) ? $team['name_th'] : '') ?>" type="text"
                                   class="form-control mb-2 mr-sm-2 mb-sm-0" placeholder="League Name TH">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ชื่อ[ภาษาอังกฤษ]</label>
                        <div class="col-sm-10">
                            <input required name="team[name_en]"
                                   value="<?= ((!empty($team['name_en'])) ? $team['name_en'] : '') ?>" type="text"
                                   class="form-control mb-2 mr-sm-2 mb-sm-0" placeholder="League Name EN">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit"
                                    class="btn btn-default <?= ((!empty($team['id'])) ? 'btn-default' : 'btn-primary') ?>"><?= ((!empty($team['id'])) ? 'Edit' : 'Add') ?></button>
                        </div>
                    </div>

                </form>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Image</th>
                    <th>League</th>
                    <th>Name TH</th>
                    <th>Name EN</th>
                    <th>Manage</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($teamAll as $key => $value) { ?>
                    <tr>
                        <td><?= ($key + 1) ?></td>
                        <td>
                            <img style="height: 100px;" id="team-<?php echo $value['id']; ?>"
                                 src="<?php echo((!empty($value['path']) && file_exists($webroot . $value['path'])) ? $value['path'] : '/images/image-not-found.png') ?>">
                        </td>
                        <td><?= $value['league'] ?></td>
                        <td><?= $value['name_th'] ?></td>
                        <td><?= $value['name_en'] ?></td>
                        <td>
                            <div class="btn-group-manage" >
                                <a href="/manager/team?id=<?php echo $value['id']; ?>"
                                   class="btn btn1 btn-default" title="Edit"><i class="fa fa-pencil"
                                                                                aria-hidden="true"></i></a>
<!--                                <a onclick="return confirm('ยืนยันการลบ')"-->
<!--                                   href="/manager/team?delete_id=--><?php //echo $value['id']; ?><!--"-->
<!--                                   class="btn btn2 btn-default" title="Delete"><i class="fa fa-trash"-->
<!--                                                                                  aria-hidden="true"></i></a>-->
                                <label id="labelUpload" data-id="<?php echo $value['id']; ?>" for="inputUpload"
                                       class="btn btn3 btn-default" title="UpLoad"><i class="fa fa-upload"
                                                                                      aria-hidden="true"></i></label>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

    <form style="display: none;" id="formUpload" method="post" enctype="multipart/form-data">
        <input type="text" name="id">
        <input id="inputUpload" type="file" name="Filedata[]" multiple="multiple" accept="image/*"><br>
    </form>
</div>
<script>

    var cache = {};

    $("input#input-Team").autocomplete({
        minLength: 1,
        source: function (request, response) {
            var term = request.term;
            if (term in cache) {
                response(cache[term]);
                return;
            }

            $.getJSON("/manager/readTeam", request, function (data, status, xhr) {
                cache[term] = data;
                response(data);
            });
        }
    });

    $(document).on('click', "#labelUpload", function (e) {
        $('input[name=id]').val($(this).attr('data-id'));
    });

    $(document).on('change', "#inputUpload", function (e) {
        $("#formUpload").submit();
    });

    $(document).on('submit', "#formUpload", function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: "/manager/uploadFileTeam",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                response = jQuery.parseJSON(response);
//                console.log($('#formUpload').find('input[name=id]').val());
                console.log(response);
                $('img#team-' + $('#formUpload').find('input[name=id]').val()).attr('src', response + '#' + Math.floor((Math.random() * 1000) + 1));
            },
            error: function (data) {
                console.log("error");
                console.log(data);
            }
        });
    });
</script>

<?php include 'layouts/footer.php'; ?>

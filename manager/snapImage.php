<?php
header('Access-Control-Allow-Origin: *');
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 5/23/2018
 * Time: 22:10
 */
require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
use GuzzleHttp\Client;
$snap=(isset($_GET['snap'])?$_GET['snap']:null);
$webroot=$_SERVER['DOCUMENT_ROOT'];
$return=array();
$return['status']=false;
if(!empty($snap)) {
    if($snap['type']=="matchAll"){
        $dtm = new DateTime($snap['date']);
        $dateText = $dtm->format('Y-m-d');

        $dir = $webroot . '/images/snap/matchAll/'.$dateText;
        if (!is_dir($webroot . '/images/' . 'snap')) {
            mkdir($webroot . '/images/' . 'snap', 0777);
            chmod($webroot . '/images/' . 'snap', 0777);
        }

        if (!is_dir($webroot . '/images/snap/' . 'matchAll')) {
            mkdir($webroot . '/images/snap/' . 'matchAll', 0777);
            chmod($webroot . '/images/snap/' . 'matchAll', 0777);
        }

        if (!is_dir($webroot . '/images/snap/matchAll/'.$dateText)) {
            mkdir($webroot . '/images/snap/matchAll/'.$dateText, 0777);
            chmod($webroot . '/images/snap/matchAll/'.$dateText, 0777);
        }

        $client = new Client();

        $url = "https://ngoal.com/manager/snapFixture?date=" . $dateText;
        $return['url']=$url;
        $response = $client->get('http://61.91.124.164:51801/?url=' . $url . '&width='.$snap['width'].'&height='.$snap['height'].'&delay=3000');
        $return['client']='http://61.91.124.164:51801/?url=' . $url . '&width='.$snap['width'].'&height='.$snap['height'].'&delay=3000';
        $data = $response->getBody()->getContents();
        if (strlen($data) > 3000) {
            @file_put_contents($dir . '/snapMatchAll.png', $data);
            $return['status']=true;
            $return['data']= str_replace($webroot,'' ,$dir. '/snapMatchAll.png');
        } else {
            $this->info(strlen($data) . " skip!");
        }
    }else if($snap['type']=="match") {
        $dir = $webroot . '/images/snap/match/'.$snap['id'];

        if (!is_dir($webroot . '/images/' . 'snap')) {
            mkdir($webroot . '/images/' . 'snap', 0777);
            chmod($webroot . '/images/' . 'snap', 0777);
        }

        if (!is_dir($webroot . '/images/snap/' . 'match')) {
            mkdir($webroot . '/images/snap/' . 'match', 0777);
            chmod($webroot . '/images/snap/' . 'match', 0777);
        }

        if (!is_dir($webroot . '/images/snap/match/'.$snap['id'])) {
            mkdir($webroot . '/images/snap/match/'.$snap['id'], 0777);
            chmod($webroot . '/images/snap/match/'.$snap['id'], 0777);
        }

        $client = new Client();

        $url = "https://ngoal.com/manager/snapMatch?id=" . $snap['id'];

        $response = $client->get('http://61.91.124.164:51801/?url=' . $url . '&width='.$snap['width'].'&height='.$snap['height'].'&delay=3000');
        $data = $response->getBody()->getContents();
        if (strlen($data) > 3000) {
            @file_put_contents($dir . '/snapMatch.png', $data);
            $return['status']=true;
            $return['data']= str_replace($webroot,'' ,$dir. '/snapMatch.png');
        } else {
            $this->info(strlen($data) . " skip!");
        }
    }else if($snap['type']=="league") {
        $dtm = new DateTime($snap['date']);
        $dateText = $dtm->format('Y-m-d');
        $dir = $webroot . '/images/snap/league/' . $snap['id'] . '/' . $dateText;

        if (!is_dir($webroot . '/images/' . 'snap')) {
            mkdir($webroot . '/images/' . 'snap', 0777);
            chmod($webroot . '/images/' . 'snap', 0777);
        }

        if (!is_dir($webroot . '/images/snap/' . 'league')) {
            mkdir($webroot . '/images/snap/' . 'league', 0777);
            chmod($webroot . '/images/snap/' . 'league', 0777);
        }

        if (!is_dir($webroot . '/images/snap/league/' . $snap['id'])) {
            mkdir($webroot . '/images/snap/league/' . $snap['id'], 0777);
            chmod($webroot . '/images/snap/league/' . $snap['id'], 0777);
        }

        if (!is_dir($webroot . '/images/snap/league/' . $snap['id'] . '/' . $dateText)) {
            mkdir($webroot . '/images/snap/league/' . $snap['id'] . '/' . $dateText, 0777);
            chmod($webroot . '/images/snap/league/' . $snap['id'] . '/' . $dateText, 0777);
        }

        $client = new Client();

        $urlFixture = "https://ngoal.com/manager/snapFixture-small?id=" . $snap['id'] . "_".$dateText;
        $urlScore = "https://ngoal.com/manager/snapScore-small?id=" . $snap['id'] . "_".$dateText;
        $return['urlFixture']=$urlFixture;
        $return['urlScore']=$urlScore;
        $response = $client->get('http://61.91.124.164:51801/?url=' . $urlFixture . '&width=' . $snap['width'] . '&height=' . $snap['height'] . '&delay=3000');
        $return['clientFixture']='http://61.91.124.164:51801/?url=' . $urlFixture . '&width=' . $snap['width'] . '&height=' . $snap['height'] . '&delay=3000';
        $data = $response->getBody()->getContents();
        if (strlen($data) > 3000) {
            @file_put_contents($dir . '/snapLeagueFixture.png', $data);
            $return['status'] = true;
            $return['fixture'] = str_replace($webroot, '', $dir . '/snapLeagueFixture.png');

            $response = $client->get('http://61.91.124.164:51801/?url=' . $urlScore . '&width=' . $snap['width'] . '&height=' . $snap['height'] . '&delay=3000');
            $return['clientScore']='http://61.91.124.164:51801/?url=' . $urlScore . '&width=' . $snap['width'] . '&height=' . $snap['height'] . '&delay=3000';
            $data = $response->getBody()->getContents();
            if (strlen($data) > 3000) {
                @file_put_contents($dir . '/snapLeagueScore.png', $data);
                $return['status'] = true;
                $return['score'] = str_replace($webroot, '', $dir . '/snapLeagueScore.png');
            } else {
                $this->info(strlen($data) . " skip!");
            }
        } else {
            $this->info(strlen($data) . " skip!");
        }
    }
}
echo json_encode($return);


<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 3/12/2018
 * Time: 21:50
 */
include '_database.php';
if (isset($_REQUEST['MediaStore'])) {

    if (!empty($_REQUEST['MediaStore']['id'])) {
        $sql = "update `media_store` set `gall_id`=" . $_REQUEST['MediaStore']['gall_id'] . ",`path`='" . $_REQUEST['MediaStore']['path'] . "',`thumbnail`='" . $_REQUEST['MediaStore']['thumbnail'] . "',`desc`='" . $_REQUEST['MediaStore']['desc'] . "',`typeVideo`='" . $_REQUEST['MediaStore']['typeVideo'] . "' where media_store.id=" . $_REQUEST['MediaStore']['id'];
    } else {
        $sql = "INSERT INTO `media_store` (`gall_id`,`path`,`thumbnail`,`created_at`,`remove`,`desc`,`typeVideo`)";
        $sql .= " VALUE (" . $_REQUEST['MediaStore']['gall_id'] . ",'" . $_REQUEST['MediaStore']['path'] . "','" . $_REQUEST['MediaStore']['thumbnail'] . "','" . date('Y-m-d') . "','N','" . $_REQUEST['MediaStore']['desc'] . "','" . $_REQUEST['MediaStore']['typeVideo'] . "');";
    }

    $sth = $dbh->prepare($sql)->execute();
    header("Location: /manager/sexy-video?id=" . $_REQUEST['MediaStore']['gall_id']);
    exit;
} elseif (isset($_REQUEST['edit_id'])) {
    $sth = $dbh->prepare("select * from media_store where media_store.remove='N' and media_store.id=?");
    $sth->execute([$_REQUEST['edit_id']]);
    $mediaStore = $sth->fetch();
} elseif (isset($_REQUEST['delete_id'])) {
    $sql = "UPDATE media_store set remove='Y' WHERE id=" . $_REQUEST['delete_id'];
    $sth = $dbh->prepare($sql)->execute();
}

$sth = $dbh->prepare("select * from media_store where media_store.remove='N' and media_store.gall_id=?");
$sth->execute([$_REQUEST['id']]);
$dataMediaStore = $sth->fetchAll();

?>
<?php include 'layouts/header.php'; ?>
<div class="container-fluid">
    <a href="/manager/sexy" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"
                                                              aria-hidden="true"></span> Back</a>

    <div class="page-header">
        <h3><i class="fa fa-play-circle" aria-hidden="true"></i> Manage Media Gallery Video</h3>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
                <form>
                    <input type="hidden" name="MediaStore[gall_id]"
                           value="<?php echo((isset($_REQUEST['id'])) ? $_REQUEST['id'] : '') ?>">
                    <input type="hidden" name="MediaStore[id]"
                           value="<?php echo((isset($_REQUEST['edit_id'])) ? $_REQUEST['edit_id'] : '') ?>">
                    <div class="form-group">
                        <input type="text" value="<?= ((!empty($mediaStore['path'])) ? $mediaStore['path'] : '') ?>"
                               name="MediaStore[path]" class="form-control" id="inputPassword2" placeholder="link">
                    </div>
                    <div class="form-group">
                        <input type="text"
                               value="<?= ((!empty($mediaStore['thumbnail'])) ? $mediaStore['thumbnail'] : ''); ?>"
                               name="MediaStore[thumbnail]" class="form-control" id="inputPassword2"
                               placeholder="thumbnail">
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="MediaStore[typeVideo]">
                            <option <?= ((!empty($mediaStore['typeVideo']) and $mediaStore['typeVideo'] == "youtube") ? 'selected' : '') ?>
                                    value="youtube">youtube
                            </option>
                            <option <?= ((!empty($mediaStore['typeVideo']) and $mediaStore['typeVideo'] == "facebook") ? 'selected' : '') ?>
                                    value="facebook">facebook
                            </option>
                            <option <?= ((!empty($mediaStore['typeVideo']) and $mediaStore['typeVideo'] == "twitter") ? 'selected' : '') ?>
                                    value="twitter">twitter
                            </option>
                            <option <?= ((!empty($mediaStore['typeVideo']) and $mediaStore['typeVideo'] == "dailymotion") ? 'selected' : '') ?>
                                    value="dailymotion">dailymotion
                            </option>
                            <option <?= ((!empty($mediaStore['typeVideo']) and $mediaStore['typeVideo'] == "streamable") ? 'selected' : '') ?>
                                    value="streamable">streamable
                            </option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" value="<?= ((!empty($mediaStore['desc'])) ? $mediaStore['desc'] : ''); ?>"
                               name="MediaStore[desc]" class="form-control" id="inputPassword2" placeholder="desc">
                    </div>
                    <div class="pull-right">
                        <?php if (!empty($mediaStore['id'])) { ?>
                            <button type="submit" class="btn btn-default"><i class="fa fa-pencil-square-o"
                                                                             aria-hidden="true"></i> Edit Video
                            </button>
                        <?php } else { ?>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i>
                                Add Video
                            </button>
                        <?php } ?>
                    </div>

                </form>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Thumbnail</th>
                    <th>Url</th>
                    <th>Type</th>
                    <th>Desc</th>
                    <th class="text-center">Tools</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($dataMediaStore as $key => $value) { ?>
                    <tr>
                        <td>
                            <?php echo $value['created_at']; ?>
                            <a class="btn btn-default btn-sm" href="<?php echo $value['path']; ?>"
                               target="_blank"><i class="fa fa-eye" aria-hidden="true"></i> view</a>
                        </td>
                        <td><img id="ImageThumbnail_<?php echo $value['id']; ?>" style="height: 100px;"
                                 src="<?php echo $value['thumbnail']; ?>"></td>
                        <td><?php echo $value['path']; ?></td>
                        <td><?php echo $value['typeVideo']; ?></td>
                        <td><?php echo $value['desc']; ?></td>
                        <td>
                            <div class="btn-group-manage" style="width: 195px;" aria-label="...">
                            <a href="/manager/sexy-video?id=<?= ((isset($_REQUEST['id'])) ? $_REQUEST['id'] : '') ?>&delete_id=<?= $value['id'] ?>"
                               class="btn btn1 btn-default">Delete</a>
                            <a href="/manager/sexy-video?id=<?= ((isset($_REQUEST['id'])) ? $_REQUEST['id'] : '') ?>&edit_id=<?= $value['id'] ?>"
                               class="btn btn2 btn-default">Edit</a>

                            <form inImage="<?php echo $value['id']; ?>" id="imageUploadForm-1"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="gallery"
                                       value="<?= ((isset($_REQUEST['id'])) ? $_REQUEST['id'] : '') ?>">
                                <input type="hidden" name="store" value="<?php echo $value['id']; ?>">
                                <label for="upload-file-transfer-1"
                                       class="custom-file-upload btn3 btn btn-default">
                                    Select File
                                </label>
                                <!--                                                    <div class="bt-normal bt-upload">-->
                                <input multiple style="display: none;" type="file" name="Filedata"
                                       id="upload-file-transfer-1">
                                <!--                                                    </div>-->
                            </form>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).on('change', "#upload-file-transfer-1", function (e) {
        $("#imageUploadForm-1").submit();
    });

    $(document).on('submit', "#imageUploadForm-1", function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: "uploadFileSexyThumbnail",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (response) {
//                response=jQuery.parseJSON(response);
                $('#ImageThumbnail_' + response['id']).attr('src', (response['thumbnail'] + '?' + Math.random()));
            },
            error: function (data) {
                console.log("error");
                console.log(data);
            }
        });
    })
</script>
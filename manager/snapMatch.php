<?php
header('Access-Control-Allow-Origin: *');
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
$playMatch = new playMatch();
$matchView = $playMatch->getFirstMathById($_GET['id'])[0];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>March</title>
    <link href="css/reset.css" rel="stylesheet">
    <link href="css/march.css" rel="stylesheet">
</head>
<body>
<div class="content-march">
    <div class="march-top">
        <div class="march-top-logo">
            <img src="<?=$matchView->teamHomePath?>">
        </div>
        <div class="march-top-name" style="overflow: hidden;">
            <?=$matchView->teamHomeEn;?>
        </div>
    </div>
    <div class="march-mid">
        <div class="box-vs">
            <div class="box-vs-text">
                VS
            </div>
        </div>
        <div class="box-hdp">
            <div class="box-hdp-content">
                <span>HDP</span>
                <p><?=$matchView->handicap;?></p>
            </div>
        </div>
        <div class="box-time">
            <?= date("d.m.Y", (strtotime($matchView->time_match) - (60 * 60))) ?> | <?= date("H:i", (strtotime($value->time_match) - (60 * 60))) ?>
        </div>
    </div>
    <div class="march-button">
        <div class="march-button-name" style="overflow: hidden;">
            <?=$matchView->teamAwayEn;?>
        </div>
        <div class="march-button-logo">
            <img src="<?=$matchView->teamAwayPath?>">
        </div>
    </div>
</div>
</body>
</html>
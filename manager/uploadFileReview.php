<?php
header('Access-Control-Allow-Origin: *');
?>
<?php include '_config.php';?>
<?php include '_database.php';?>
<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 2/15/2017
 * Time: 15:27
 */
    $id = ($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
    $now = new DateTime();
    if (!empty($_FILES)) {
        $webroot=$_SERVER['DOCUMENT_ROOT'];
        $targetPath = $webroot . '/images/review/'.$id.'/';
        for($i=0;$i<count($_FILES['Filedata']['tmp_name']);$i++){

            $tempFile = $_FILES['Filedata']['tmp_name'][$i];
            $nameFile = $_FILES['Filedata']['name'][$i];
            $typeFile= $_FILES['Filedata']['type'][$i];

            if (!is_dir($webroot . '/images/' . 'review')) {
                mkdir($webroot . '/images/' . 'review', 0777);
                chmod($webroot . '/images/' . 'review', 0777);
            }

            if (!is_dir($webroot . '/images/' . 'review/'.$id)) {
                mkdir($webroot . '/images/' . 'review/'.$id, 0777);
                chmod($webroot . '/images/' . 'review/'.$id, 0777);
            }

            $size=GetimageSize($tempFile);
            $file = str_replace('//', '/', $targetPath) . $id.'_'.$size[0].'x'.$size[1].strrchr($nameFile, '.');
            move_uploaded_file($tempFile, $file);
            $fileName = str_replace($webroot, '', $file);
            chmod($file, 0777);

        }

        $sql = "UPDATE user_review set path='".$fileName."' WHERE id=".$id;
        $sth = $dbh->prepare($sql)->execute();
        echo json_encode($fileName);
    }
?>
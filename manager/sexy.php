<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 3/9/2018
 * Time: 13:58
 */
include '_database.php';
if (!isset($_SESSION)) {
    session_start();
}
if (isset($_REQUEST['sexy'])) {
    if (!empty($_REQUEST['sexy']['id'])) {
        $sql = "UPDATE `media_gallery` set `title`=?,`tag`=?,`desc`=?,`gall_type`='" . $_REQUEST['sexy']['gall_type'] . "',`category`='" . $_REQUEST['sexy']['category'] . "',`update_at`='" . date("Y-m-d H:i:s") . "' WHERE id=" . $_REQUEST['sexy']['id'];
    } else {
        $sql = "INSERT INTO `media_gallery` (`title`,`tag`,`desc`,`fb_uid`,`gall_type`,`group`,`category`,`created_at`,`update_at`)";
        $sql .= " VALUES (?,?,?,'9999','" . $_REQUEST['sexy']['gall_type'] . "','sexy','" . $_REQUEST['sexy']['category'] . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
    }
    $sth = $dbh->prepare($sql)->execute([$_REQUEST['sexy']['title'],$_REQUEST['sexy']['tag'],$_REQUEST['sexy']['desc']]);
    if(isset($_REQUEST['sexy']['page'])){
        header("Location: /manager/sexy?page=".$_REQUEST['sexy']['page']);
    }else {
        header("Location: /manager/sexy");
    }
    exit;
} elseif (isset($_REQUEST['id'])) {
    $sth = $dbh->prepare("select * from media_gallery where id=?");
    $sth->execute([$_REQUEST['id']]);
    $dataMediaGallery = $sth->fetch();
} elseif (isset($_REQUEST['delete_id'])) {
    if(isset($_SESSION['login']) && !empty($_SESSION['login']['username']) && $_SESSION['login']['username']=="admin") {
        $sth = $dbh->prepare("delete from media_gallery where id=" . $_REQUEST['delete_id']);
        $sth->execute();
    }
    if(isset($_REQUEST['sexy']['page'])){
        header("Location: /manager/sexy?page=".$_REQUEST['sexy']['page']);
    }else {
        header("Location: /manager/sexy");
    }
    exit;
}

//------------------page------------------
$sth = $dbh->prepare("select count(*) as total from media_gallery");
$sth->execute();
$dataVideoHighlightList = $sth->fetch();
$pageTotal = ceil((int) $dataVideoHighlightList["total"] / (int) 30);
$pageTotalFor=$pageTotal;
$page=(isset($_REQUEST['page'])?$_REQUEST['page']:1);
if (isset($_GET['search'])) {
    $sth = $dbh->prepare("select * from media_gallery WHERE `title` like '%" . $_GET['search']['name'] . "%' order by id DESC limit 30");
    $sth->execute();
    $dataMediaGalleryList = $sth->fetchAll();
} else {
    $sth = $dbh->prepare("select * from media_gallery order by id DESC limit 30 offset ".(($page-1)*30));
    $sth->execute();
    $dataMediaGalleryList = $sth->fetchAll();
}
if($pageTotal>100 and $page>=100){
    $pageTotalFor=$page+100;
}else{
    $pageTotalFor=$pageTotal;
}
?>
<?php include 'layouts/header.php'; ?>
<div class="container-fluid">
    <div class="page-header title">
        <h3>Sexy</h3>
        <button type="button" class="btn btn-default" data-toggle="modal"
                data-target="#exampleModal">
            <i class="fa fa-plus" aria-hidden="true"></i> Add
        </button>
        <a href="/load/gameNew?type=sexy" class="btn btn-default"><i class="fa fa-check" aria-hidden="true"></i>
            Generates</a>
        <form class="form-inline pull-right">
            <label class="sr-only" for="inlineFormInput">Title</label>
            <input name="search[name]" type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" placeholder="Search Title">
            <button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i> Search
            </button>
        </form>
    </div>


    <div class="row">
        <nav aria-label="Page navigation">
            <ul class="pagination pagination-sm" style="margin: 0px;">
                <?php if($page>1){ ?>
                    <li>
                        <a href="/manager/sexy?page=<?=$page-1?>" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                <?php } ?>
                <?php for ($i = 1; $i <= $pageTotalFor; $i++) { ?>
                    <?php if($i==$page){ ?>
                        <li class="active"><a href="/manager/sexy?page=<?=$i?>"><?=$i?></a></li>
                    <?php }else{ ?>
                        <li class=""><a href="/manager/sexy?page=<?=$i?>"><?=$i?></a></li>
                    <?php } ?>
                <?php } ?>
                <?php if($page!=$pageTotalFor){ ?>
                    <li>
                        <a href="/manager/sexy?page=<?=$pageTotalFor-1?>" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </nav>
    </div>

    <div class="col-md-12">
        <div class="row">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>วันที่สร้าง</th>
                    <th>หัวข้อ</th>
                    <th>รายละเอียด</th>
                    <th>ประเภท</th>
                    <th>ป้ายกำกับ</th>
                    <th style="width: 12%;">เวลาที่แก้ไขล่าสุด</th>
                    <th class="text-center">จัดการ</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($dataMediaGalleryList as $key => $value) { ?>
                    <tr>
                        <td><?= date("Y-m-d H:i:s",strtotime($value['created_at']." +7 hours")); ?></td>
                        <td><?= $value['title']; ?></td>
                        <td><?= $value['desc']; ?></td>
                        <td><?= $value['category']; ?></td>
                        <td><?= $value['tag']; ?></td>
                        <td><?= $value['update_at']; ?></td>
                        <td class="text-center">
                            <div class="btn-group-manage" aria-label="...">
                                <?php if ($value['gall_type'] == "picture") { ?>
                                    <a class="btn btn1 btn-default"
                                       href="/manager/sexy-upload?id=<?= $value['id']; ?>" title="picture"><span
                                                class="glyphicon glyphicon-picture" aria-hidden="true"></span></a>
                                <?php } else if ($value['gall_type'] == "video") { ?>
                                    <a class="btn btn1 btn-default"
                                       href="/manager/sexy-video?id=<?= $value['id']; ?>" title="Upload Video">
                                        <i class="fa fa-upload" aria-hidden="true"></i>
                                    </a>
                                <?php } ?>
                                <a class="btn btn2 btn-default" href="/manager/sexy?page=<?php echo $page;?>&id=<?= $value['id']; ?>"
                                   title="แก้ไข">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                                <a class="btn btn3 btn-default"
                                   href="/manager/sexy?page=<?php echo $page;?>&delete_id=<?= $value['id']; ?>" title="ลบ">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>

<div class="modal fade<?php echo((!empty($dataMediaGallery["id"])) ? ' in show' : '') ?>"
     style="<?php echo((!empty($dataMediaGallery["id"])) ? 'display: block;' : '') ?>" id="exampleModal" tabindex="-1"
     role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="/manager/sexy" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h4 class="modal-title" id="exampleModalLabel">Edit / Add</h4>

                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                    <input type="hidden" name="sexy[page]"
                               value="<?= ((!empty($page)) ? $page : 0) ?>">
                    <input type="hidden" name="sexy[id]"
                           value="<?= ((!empty($dataMediaGallery['id'])) ? $dataMediaGallery['id'] : '') ?>">

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Title</label>
                            <div class="col-sm-10">
                                <input value="<?= ((!empty($dataMediaGallery['title'])) ? $dataMediaGallery['title'] : '') ?>"
                                       name="sexy[title]" type="text" class="form-control" id="exampleInputEmail1"
                                       aria-describedby="emailHelp" placeholder="Title">
                                <!--                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tag</label>
                            <div class="col-sm-10">
                                <input value="<?= ((!empty($dataMediaGallery['tag'])) ? $dataMediaGallery['tag'] : '') ?>"
                                       name="sexy[tag]" type="text" class="form-control" id="exampleInputPassword1"
                                       placeholder="Tag">
                            </div>
                        </div>
                        <div class="form-group form-check">
                            <label class="col-sm-2 control-label">Type</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="sexy[gall_type]" style="margin: 0px;">
                                    <option <?php echo((!empty($dataMediaGallery["gall_type"]) and $dataMediaGallery["gall_type"] == "picture") ? 'selected' : '') ?>
                                            value="picture">picture
                                    </option>
                                    <option <?php echo((!empty($dataMediaGallery["gall_type"]) and $dataMediaGallery["gall_type"] == "video") ? 'selected' : '') ?>
                                            value="video">video
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-check">
                            <label class="col-sm-2 control-label">Category</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="sexy[category]" style="margin: 0px;">
                                    <option <?php echo((!empty($dataMediaGallery["category"]) and $dataMediaGallery["category"] == "ฝรั่ง") ? 'selected' : '') ?>
                                            value="ฝรั่ง">ฝรั่ง
                                    </option>
                                    <option <?php echo((!empty($dataMediaGallery["category"]) and $dataMediaGallery["category"] == "เอเชีย") ? 'selected' : '') ?>
                                            value="เอเชีย">เอเชีย
                                    </option>
                                    <option <?php echo((!empty($dataMediaGallery["category"]) and $dataMediaGallery["category"] == "ไทย") ? 'selected' : '') ?>
                                            value="ไทย">ไทย
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-check">
                            <label class="col-sm-2 control-label">Desc</label>
                            <div class="col-sm-10">
                            <textarea name="sexy[desc]" class="form-control" id="exampleFormControlTextarea1"
                                      rows="3"><?= ((!empty($dataMediaGallery['desc'])) ? $dataMediaGallery['desc'] : '') ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="/manager/sexy" class="btn btn-secondary" data-dismiss="modal">Close</a>
                    <?php if (!empty($dataMediaGallery["id"])) { ?>
                        <button type="submit" class="btn btn-warning">changes</button>
                    <?php } else { ?>
                        <button type="submit" class="btn btn-primary">Save</button>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/6/2017
 * Time: 16:39
 */
include '_database.php';
if (!isset($_SESSION)) {
    session_start();
}
if(isset($_POST['coin'])) {
    $dateNow = new DateTime();

    $sth = $dbh->prepare("select * from `log_income` where date_record='".$dateNow->format('Y-m-d')."' and user_id=" . $_SESSION['login']['id'] . " and income_derived='daily'");
    $sth->execute();
    $log_income = $sth->fetch();

    if(empty($log_income)){
        $user_id=$_SESSION['login']['id'];
        $befor_gold=$_SESSION['login']['gold'];
        $befor_coin=$_SESSION['login']['coin'];
        $befor_coupon=$_SESSION['login']['coupon'];
        $befor_lv=$_SESSION['login']['lv'];
        $befor_star=$_SESSION['login']['star'];
        $income_derived='daily';
        $income_type='coin';
        $income_format='plus';
        $after_gold=$befor_gold;
        $after_coin=(intval($befor_coin)+intval($_POST['coin']));
        $after_coupon=$befor_coupon;
        $after_lv=$_SESSION['login']['lv'];
        $after_star=$_SESSION['login']['star'];
        $comment="กดรับ coin วันที่ ".($_SESSION['login']['combo_login']+1);
        $sql = "INSERT INTO `log_income` (`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
        $sql.=" VALUES (" . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "',".$after_gold.",".$after_coin.",".$after_coupon.",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
        $sth = $dbh->prepare($sql)->execute();
        if($sth){
            $_SESSION['login']['coin']=$after_coin;
            $sql="update `user` set coin=".$after_coin." where id=".$user_id;
            $sth = $dbh->prepare($sql)->execute();
            if($sth){
                $_SESSION['login']['modal_gold']=false;
            }

            $sth = $dbh->prepare("select id from log_income where income_derived='daily' and user_id=".$user_id." and date_record='".$dateNow->format('Y-m-d')."'");
            $sth->execute();
            $log_income = $sth->fetch();

            if(!empty($log_income)){
                $_SESSION['login']['coin_daily']=true;
            }

        }
    }

    header('Location: /');
    exit();
}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>อันดับผู้เล่นที่ ทำคะแนนสูงสุด เกมทายผลฟุตบอล Ngoal</title>
    <meta name="keywords" content="เกมทายผลบอล, ทายผลบอล, แทงบอล, ทายผลบอลเมื่อคืน, เกมทายผล">
    <meta name="description" content="Ngoal เกมทายผลฟุตบอลด้วยข้อมูลการแข่งขัน  โดยจะจัดอันดับของผู้เล่นออกเป็น Ranking แต่ละประเภท และจะมีการแจกรางวัลทุกสัปดาห์สำหรับผู้เล่นที่ติดอันดับตามที่กำหนด">
    <link rel="shortcut icon" type="images/x-icon" href="/images/favicon.ico.png">
    <meta content="width=device-width,initial-scale=1" name="viewport">

    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="nanoscrollbar/nanoscrollbar.js"></script>
    <script src="js/utils.js"></script>
    <script src="/js/ranking.js"></script>
    <?php include 'view/index/googletagmanager.php'; ?>
</head>
<?php
//require_once $_SERVER['DOCUMENT_ROOT'] . "/model/RankM.php";
//require_once $_SERVER['DOCUMENT_ROOT'] . '/utility/LogoManager.php';
//require_once $_SERVER['DOCUMENT_ROOT'] . '/utility/DataMap.php';
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playUser.php";

$playUser=new playUser();

$topRankingAll=$playUser->accuracyUserAll();
$topRankingNow=$playUser->accuracyUserNow();
$topRankingLastChamp=$playUser->accuracyUserLastChamp();
$topGoldAll=$playUser->goldUserAll();
$topGoldNow=$playUser->goldUserNow();
$topGoldLastChamp=$playUser->goldUserLastChamp();

?>
<body class="mobile-ranking">
<?php //include 'header.php'; ?>
<!--index menu-->
<?php include $_SERVER['DOCUMENT_ROOT'] . '/view/index/menu-all.php'; ?>
<!--<nav class="navbar navbar-menu-v2">-->
<!--    --><?php //include $_SERVER['DOCUMENT_ROOT'] . '/view/menu/navbar-menu-v2.php'; ?>
<!--</nav>-->
<div class="wrapper-news-articles">
    <div class="containter-fluid">
        <div class="bx-head-fixtures" style="height: 580px;">
            <div class="hilight-cover" style="opacity: 0.75;"></div>
            <div class="articles-title">
                <div class="container">
                        <div class="col-sm-6">
                            <div class="col-sm-8"><img src="/images/thebest-img.png" style="width: 100%;"></div>
                        </div>
                        <div class="col-sm-6 text-right" style="color: #fff;">
                            <div class="text-right">
                                <h2>RANKING</h2>
                                <h1>อันดับผู้เล่นที่ทำคะแนนสูงสุด</h1>
                                <div class="cols-info">Ngoal เกมทายผลฟุตบอลด้วยข้อมูลการแข่งขัน สถิติการแข่งขัน<br>
                                    อัตราการต่อรอง จากข้อมูลจริง
                                    โดยจะจัดอันดับของผู้เล่นออกเป็น Ranking แต่ละประเภท
                                    และจะมีการแจกรางวัลทุกเดือนสำหรับผู้เล่นที่ติดอันดับในแต่ละ Ranking
                                    <u>เริ่มต้นเล่นเกมฟรีที่นี่</u>
                                </div>
                                <div class="bt-games">

                                    เกมทายผลบอล
                                </div>
                            </div>
                        </div>
                        <div style="clear: both;"></div>

                </div>
            </div>
            <div class="background-image_image4"></div>
        </div>
    </div>
    <div class="container container-ranking">

        <div class="rows" style="margin-top: 20px;">

            <div class="col-sm-4 mg-bt15">
                <div class="bx-white-border mg-bt15">
                    <div class="tab-h-game h-rank">
                        <img src="/images/trophy/300150.png"> Top Ranking
                    </div>
                    <div class="wrap-rank">
                        <div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-3sec" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#all-rank" aria-controls="home" role="tab" data-toggle="tab">All Time</a></li>
                                <li role="presentation">
                                    <a href="#now-rank" aria-controls="profile" role="tab" data-toggle="tab">Now</a></li>
                                <li role="presentation">
                                    <a href="#last-rank" aria-controls="messages" role="tab" data-toggle="tab">Last Champ</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="all-rank">
                                    <div class="box-ranking-top">
                                        <!--                                        <div id="about" class="nano bx2">-->
                                        <!--                                            <div class="content">-->
                                        <table id="gp-alltime-tab" class="table-striped">
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <!--                                                <th>P</th>-->
                                                <th>PTA</th>
                                            </tr>
                                            <tr class="hide" id="rank-row-original">
                                                <td>
                                                    <span class="rank-no"></span>
                                                </td>
                                                <td>
                                                    <div class="img-user-ranking"><img class="user-img-rank"
                                                                                       src="">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div
                                                        class="username-ranking-top"></div>
                                                </td>
                                                <td>
                                                    <div class="txt-3em">
                                                        <strong class="rank-indicator"></strong>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php foreach ($topRankingAll as $key => $val) { ?>
                                                <tr>
                                                    <td>
                                                        <span><?php echo $key+1 ?></span>
                                                    </td>
                                                    <td style="width: 35px;">
                                                        <div class="img-user-ranking">
                                                            <img src="https://graph.facebook.com/v2.8/<?php echo $val["fb_uid"]; ?>/picture?type=large">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="username-ranking-top">
                                                            <a style="color:#000000;" href="/profile.php?user_id=<?php echo $val['id']; ?>"><?php echo $val['name']; ?></a>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="txt-3em"><?php echo number_format($val['accuracy'], 2); ?></div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                        <div class="bt-seemore hide">ดูอันดับทั้งหมด</div>
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="now-rank">
                                    <div class="box-ranking-top">
                                        <!--                                        <div id="about" class="nano bx2">-->
                                        <!--                                            <div class="content">-->
                                        <table id="gp-now-tab" class="table-striped">
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <!--                                                <th>P</th>-->
                                                <th>PTA</th>
                                            </tr>
                                            <?php foreach ($topRankingNow as $key => $val) { ?>
                                                <tr>
                                                    <td>
                                                        <span><?php echo $key+1 ?></span>
                                                    </td>
                                                    <td style="width: 35px;">
                                                        <div class="img-user-ranking">
                                                            <img src="https://graph.facebook.com/v2.8/<?php echo $val["fb_uid"]; ?>/picture?type=large">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="username-ranking-top">
                                                            <a style="color:#000000;" href="/profile.php?user_id=<?php echo $val['id']; ?>"><?php echo $val['name']; ?></a>
                                                        </div>

                                                    </td>
                                                    <td>
                                                        <div class="txt-3em"><?php echo number_format($val['accuracy_half'], 2); ?></div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                        <div class="bt-seemore hide">ดูอันดับทั้งหมด</div>
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="last-rank">
                                    <div class="box-ranking-top">
                                        <!--                                        <div id="about" class="nano bx2">-->
                                        <!--                                            <div class="content">-->
                                        <table id="gp-period-tab" class="table-striped">
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <!--                                                <th>P</th>-->
                                                <th>PTA</th>
                                            </tr>
                                            <?php foreach ($topRankingLastChamp as $key => $val) { ?>
                                                <tr>
                                                    <td>
                                                        <span><?php echo $key+1 ?></span>
                                                    </td>
                                                    <td style="width: 35px;">
                                                        <div class="img-user-ranking">
                                                            <img src="https://graph.facebook.com/v2.8/<?php echo $val["fb_uid"]; ?>/picture?type=large">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="username-ranking-top">
                                                            <a style="color:#000000;" href="/profile.php?user_id=<?php echo $val['id']; ?>"><?php echo $val['name']; ?></a>
                                                        </div>

                                                    </td>
                                                    <td>
                                                        <div class="txt-3em"><?php echo number_format($val['accuracy_month'], 2); ?></div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                        <div class="bt-seemore hide">ดูอันดับทั้งหมด</div>
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mg-bt15">
                <div class="bx-white-border mg-bt15">
                    <div class="tab-h-game h-rank">
                        <img src="/images/trophy/000150.png"> Top Sgold
                    </div>
                    <div class="wrap-rank">
                        <div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-half nav-3sec" role="tablist">
                                <li role="presentation" class="active"><a href="#now-rank-sgold" aria-controls="profile"
                                                                          role="tab" data-toggle="tab">All Time</a>
                                </li>
                                <li role="presentation"><a href="#last-rank-sgold" aria-controls="messages"
                                                           role="tab" data-toggle="tab">Now</a></li>
                                <li role="presentation"><a href="#last-champ-sgold" aria-controls="messages"
                                                           role="tab" data-toggle="tab">Last Champ</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="now-rank-sgold">
                                    <div class="box-ranking-top">
                                        <!--                                        <div id="about" class="nano bx2">-->
                                        <!--                                            <div class="content">-->
                                        <table id="gold-alltime-tab" class="table-striped">
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <!--                                                <th>P</th>-->
                                                <th>GOLD</th>
                                            </tr>
                                            <?php foreach ($topGoldAll as $key => $val) { ?>
                                                <tr>
                                                    <td>
                                                        <span><?php echo $key+1 ?></span>
                                                    </td>
                                                    <td>
                                                        <div class="img-user-ranking">
                                                            <img src="https://graph.facebook.com/v2.8/<?php echo $val["fb_uid"]; ?>/picture?type=large">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="username-ranking-top">
                                                            <a style="color:#000000;" href="/profile.php?user_id=<?php echo $val['id']; ?>"><?php echo $val['name']; ?></a>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="txt-3em">
                                                            <strong><?php echo number_format($val['gold'], 2); ?></strong>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                        <div class="bt-seemore hide">ดูอันดับทั้งหมด</div>
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="last-rank-sgold">
                                    <div class="box-ranking-top">
                                        <!--                                        <div id="about" class="nano bx2">-->
                                        <!--                                            <div class="content">-->
                                        <table id="gold-now-tab" class="table-striped">
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <!--                                                <th>P</th>-->
                                                <th>GOLD</th>
                                            </tr>
                                            <?php foreach ($topGoldNow as $key => $val) { ?>
                                                <tr>
                                                    <td>
                                                        <span><?php echo $key+1 ?></span>
                                                    </td>
                                                    <td>
                                                        <div class="img-user-ranking">
                                                            <img src="https://graph.facebook.com/v2.8/<?php echo $val["fb_uid"]; ?>/picture?type=large">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="username-ranking-top">
                                                            <a style="color:#000000;" href="/profile.php?user_id=<?php echo $val['id']; ?>"><?php echo $val['name']; ?></a>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="txt-3em">
                                                            <strong><?php echo number_format($val['gold_half'], 2); ?></strong>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                        <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="last-champ-sgold">
                                    <div class="box-ranking-top">
                                        <!--                                        <div id="about" class="nano bx2">-->
                                        <!--                                            <div class="content">-->
                                        <table id="gold-period-tab" class="table-striped">
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <!--                                                <th>P</th>-->
                                                <th>GOLD</th>
                                            </tr>
                                            <?php foreach ($topGoldLastChamp as $key => $val) { ?>
                                                <tr>
                                                    <td>
                                                        <span><?php echo $key+1 ?></span>
                                                    </td>
                                                    <td>
                                                        <div class="img-user-ranking">
                                                            <img src="https://graph.facebook.com/v2.8/<?php echo $val["fb_uid"]; ?>/picture?type=large">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="username-ranking-top">
                                                            <a style="color:#000000;" href="/profile.php?user_id=<?php echo $val['id']; ?>"><?php echo $val['name']; ?></a>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="txt-3em">
                                                            <strong><?php echo number_format($val['gold_month'], 2); ?></strong>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                        <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mg-bt15">
                <div class="bx-white-border mg-bt15">
                    <div class="tab-h-game h-rank">
                        <img src="/images/trophy/ranking-ufa.png"> Top 5 League
                    </div>
                    <div class="wrap-rank">
                        <div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-5leage" role="tablist">
                                <li role="presentation" class="active"><a href="#premier-tab" aria-controls="profile"
                                                                          role="tab" data-toggle="tab"><img
                                            src="/images/five-leagues/English-Premier-League.png" style="width: 31px;"></a>
                                </li>
                                <li role="presentation"><a href="#laliga-tab" aria-controls="messages"
                                                           role="tab" data-toggle="tab"><img
                                            src="/images/five-leagues/La-Liga-Logo-1.png" style="width: 30px;"></a></li>
                                <li role="presentation"><a href="#uefa-tab" aria-controls="messages"
                                                           role="tab" data-toggle="tab"><img
                                            src="/images/five-leagues/UEFA-Champions-League-1.png" style="width: 32px;"></a>
                                </li>
                                <li role="presentation"><a href="#uropa-tab" aria-controls="messages"
                                                           role="tab" data-toggle="tab"><img
                                            src="/images/five-leagues/uropa.png" style="width: 31px;"></a></li>
                                <li role="presentation"><a href="#tpl-tab" aria-controls="messages"
                                                           role="tab" data-toggle="tab"><img
                                            src="/images/five-leagues/tpl.png" style="width: 31px;"></a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="premier-tab">
                                    <div class="box-ranking-top">
                                        <!--                                        <div id="about" class="nano bx2">-->
                                        <!--                                            <div class="content">-->
                                        <table id="englist-premierleague-tab" class="table-striped">
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <!--                                                <th>P</th>-->
                                                <th>PTA</th>
                                            </tr>
                                            <?php if(!empty($ranklist['league']['english_premier_league'])){ ?>
                                                <?php foreach ($ranklist['league']['english_premier_league'] as $key => $val) { ?>
                                                <?php
                                                $rank = $datamap->mapByKey($ranklist['keylist']['league'], $val)
                                                ?>
                                                <tr>
                                                    <td>
                                                        <span><?php echo $rank['rank']; ?></span>
                                                    </td>
                                                    <td>
                                                        <div class="img-user-ranking"><img
                                                                src="<?php echo $logoman->getFbProfilePix($rank['fb_uid'], 'normal'); ?>">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div
                                                            class="username-ranking-top"><a style="color:#000000;" href="/profile.php?fbuid=<?php echo $rank['fb_uid']; ?>"><?php echo $rank['display_name']; ?></a></div>
                                                    </td>
                                                    <!--                                                    <td>-->
                                                    <!--                                                        <div class="txt-3em" style="color: #888;">-->
                                                    <!--                                                            <strong>-->
                                                    <?php //echo $rank['play']; ?><!--</strong>-->
                                                    <!--                                                        </div>-->
                                                    <!--                                                    </td>-->
                                                    <td>
                                                        <div class="txt-3em">
                                                            <strong><?php echo number_format($rank['overall_gp'], 2); ?></strong>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        </table>
                                        <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="laliga-tab">
                                    <div class="box-ranking-top">
                                        <!--                                        <div id="about" class="nano bx2">-->
                                        <!--                                            <div class="content">-->
                                        <table id="spain-laliga-tab" class="table-striped">
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <!--                                                <th>P</th>-->
                                                <th>PTA</th>
                                            </tr>
                                            <?php if(!empty($ranklist['league']['english_premier_league'])){ ?>
                                                <?php foreach ($ranklist['league']['spain_laliga'] as $key => $val) { ?>
                                                    <?php
                                                    $rank = $datamap->mapByKey($ranklist['keylist']['league'], $val)
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <span><?php echo $rank['rank']; ?></span>
                                                        </td>
                                                        <td>
                                                            <div class="img-user-ranking"><img
                                                                    src="<?php echo $logoman->getFbProfilePix($rank['fb_uid'], 'normal'); ?>">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div
                                                                class="username-ranking-top"><a style="color:#000000;" href="/profile.php?fbuid=<?php echo $rank['fb_uid']; ?>"><?php echo $rank['display_name']; ?></a></div>
                                                        </td>
                                                        <!--                                                    <td>-->
                                                        <!--                                                        <div class="txt-3em" style="color: #888;">-->
                                                        <!--                                                            <strong>-->
                                                        <?php //echo $rank['play']; ?><!--</strong>-->
                                                        <!--                                                        </div>-->
                                                        <!--                                                    </td>-->
                                                        <td>
                                                            <div class="txt-3em">
                                                                <strong><?php echo number_format($rank['overall_gp'], 2); ?></strong>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        </table>
                                        <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="uefa-tab">
                                    <div class="box-ranking-top">
                                        <!--                                        <div id="about" class="nano bx2">-->
                                        <!--                                            <div class="content">-->
                                        <table id="euro-uefa-tab" class="table-striped">
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <!--                                                <th>P</th>-->
                                                <th>PTA</th>
                                            </tr>
                                            <?php if(!empty($ranklist['league']['uefa_champion_league'])){ ?>
                                                <?php foreach ($ranklist['league']['uefa_champion_league'] as $key => $val) { ?>
                                                    <?php
                                                    $rank = $datamap->mapByKey($ranklist['keylist']['league'], $val)
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <span><?php echo $rank['rank']; ?></span>
                                                        </td>
                                                        <td>
                                                            <div class="img-user-ranking"><img
                                                                    src="<?php echo $logoman->getFbProfilePix($rank['fb_uid'], 'normal'); ?>">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div
                                                                class="username-ranking-top"><a style="color:#000000;" href="/profile.php?fbuid=<?php echo $rank['fb_uid']; ?>"><?php echo $rank['display_name']; ?></a></div>
                                                        </td>
                                                        <!--                                                    <td>-->
                                                        <!--                                                        <div class="txt-3em" style="color: #888;">-->
                                                        <!--                                                            <strong>-->
                                                        <?php //echo $rank['play']; ?><!--</strong>-->
                                                        <!--                                                        </div>-->
                                                        <!--                                                    </td>-->
                                                        <td>
                                                            <div class="txt-3em">
                                                                <strong><?php echo number_format($rank['overall_gp'], 2); ?></strong>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        </table>
                                        <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="uropa-tab">
                                    <div class="box-ranking-top">
                                        <!--                                        <div id="about" class="nano bx2">-->
                                        <!--                                            <div class="content">-->
                                        <table id="euro-uropa-tab" class="table-striped">
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <!--                                                <th>P</th>-->
                                                <th>PTA</th>
                                            </tr>
                                            <?php if(!empty($ranklist['league']['europa_league'])){ ?>
                                                <?php foreach ($ranklist['league']['europa_league'] as $key => $val) { ?>
                                                    <?php
                                                    $rank = $datamap->mapByKey($ranklist['keylist']['league'], $val)
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <span><?php echo $rank['rank']; ?></span>
                                                        </td>
                                                        <td>
                                                            <div class="img-user-ranking"><img
                                                                    src="<?php echo $logoman->getFbProfilePix($rank['fb_uid'], 'normal'); ?>">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div
                                                                class="username-ranking-top"><a style="color:#000000;" href="/profile.php?fbuid=<?php echo $rank['fb_uid']; ?>"><?php echo $rank['display_name']; ?></a></div>
                                                        </td>
                                                        <!--                                                    <td>-->
                                                        <!--                                                        <div class="txt-3em" style="color: #888;">-->
                                                        <!--                                                            <strong>-->
                                                        <?php //echo $rank['play']; ?><!--</strong>-->
                                                        <!--                                                        </div>-->
                                                        <!--                                                    </td>-->
                                                        <td>
                                                            <div class="txt-3em">
                                                                <strong><?php echo number_format($rank['overall_gp'], 2); ?></strong>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        </table>
                                        <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tpl-tab">
                                    <div class="box-ranking-top">
                                        <!--                                        <div id="about" class="nano bx2">-->
                                        <!--                                            <div class="content">-->
                                        <table id="thai-premierleague-tap" class="table-striped">
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <!--                                                <th>P</th>-->
                                                <th>PTA</th>
                                            </tr>
                                            <?php if(!empty($ranklist['league']['thai_premier_league'])){ ?>
                                                <?php foreach ($ranklist['league']['thai_premier_league'] as $key => $val) { ?>
                                                    <?php
                                                    $rank = $datamap->mapByKey($ranklist['keylist']['league'], $val)
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <span><?php echo $rank['rank']; ?></span>
                                                        </td>
                                                        <td>
                                                            <div class="img-user-ranking"><img
                                                                    src="<?php echo $logoman->getFbProfilePix($rank['fb_uid'], 'normal'); ?>">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div
                                                                class="username-ranking-top"><a style="color:#000000;" href="/profile.php?fbuid=<?php echo $rank['fb_uid']; ?>"><?php echo $rank['display_name']; ?></a></div>
                                                        </td>
                                                        <!--                                                    <td>-->
                                                        <!--                                                        <div class="txt-3em" style="color: #888;">-->
                                                        <!--                                                            <strong>-->
                                                        <?php //echo $rank['play']; ?><!--</strong>-->
                                                        <!--                                                        </div>-->
                                                        <!--                                                    </td>-->
                                                        <td>
                                                            <div class="txt-3em">
                                                                <strong><?php echo number_format($rank['overall_gp'], 2); ?></strong>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        </table>
                                        <div class="bt-seemore pull-right">ดูอันดับทั้งหมด</div>
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
</div>
<?php //include 'footer.php';
include 'view/index/footer.php';
?>

<script>
    $(".nano").nanoScroller();
</script>

</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>บ้านผลบอล ทรรศนะบอลวันนี้ แหล่งรวมเซียนบอลทรรศนะบอลจากเว็บดัง ที่ Ngoal.com</title>
    <meta name="keywords" content="บ้านผลบอล, วิเคราะห์บอล, ทรรศนะบอล, ผลบอล, ผลบอลวันนี้, ผลบอลเมื่อคืน, วิเคราะห์บอลเมื่อคืน, ราคาไหล, รวมทีเด็ด, รวมทีเด็ดฟุตบอล">
    <meta name="description" content="บ้านผลบอล ทรรศนะบอลวันนี้ แหล่งรวมเซียนบอลทรรศนะบอลจากเว็บดัง รวมวิเคราะห์บอลวันนี้ วิเคราะห์บอลแม่นๆ ราคาไหล  วิเคราะห์สถิติการเจอกันย้อนหลัง ราคาบอลไหล ทีเด็ดสเต็ป ทีเด็ดบอลชุด รวมทีเด็ด จากเว็บต่างๆ
เว็บไหนแม่นไม่แม่น มาดูกัน">
    <link rel="shortcut icon" type="images/x-icon" href="/images/favicon.ico.png">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/doce.css">
    <link rel="stylesheet" href="/css/livescore.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <link rel="stylesheet" href="/css/game-redesign.css">
    <link rel="stylesheet" href="/css/review-new-style.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.js"></script>
    <script src="/js/readmore.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
<!--    <script src="js/jquery-timeago/jquery.timeago.js"></script>-->
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="/js/moment.js"></script>
<style type="text/css">
    .content-menu-index.affix{
        position: relative;
    }
</style>
    <?php include 'view/index/googletagmanager.php'; ?>
</head>
<body style="background-color: #f6f6f6;">
<!--index menu-->
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="wrap-review">
    <?php include_once $_SERVER["DOCUMENT_ROOT"].'/mockup/sidebar-feed.php' ?>
    <div class="container-fluid" id="main">
        <div class="sidebar-left-ranking">
            <div class="sidebar-ranking-review">
                <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/mockup/review-sidebar-left.php' ?>
            </div>
        </div>
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/mockup/content-review.php' ?>
        <div class="open-btn hide" onclick="openNav()">
            <ul>
                <li><i class="fa fa-rss" aria-hidden="true"></i></li>
                <li>Feed</li>
                <li><i class="fa fa-chevron-up pull-right" aria-hidden="true"></i></li>
            </ul>
        </div>
    </div>
</div>
<script>
    function toggleSidenav() {
        document.body.classList.toggle('sidenav-active');
    }
</script>
</body>
</html>
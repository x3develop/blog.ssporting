<?php
require_once 'model/NewsM.php';
require_once 'model/VideoM.php';
require_once 'model/GalleryM.php';
include_once './utility/Carbon/Carbon.php';
use Carbon\Carbon;

$videoMObject = new VideoM();
$newsMObject = new NewsM();
$GalleryObject = new GalleryM();
$Page = 21;
$maxPage = $newsMObject->getAllNewsCountPage($_GET['type'], 'common', date('Y-m-d H:i:s'), $Page);
$dataHotNewsCommon=array();
if (isset($_GET['type']) && isset($_GET['Page'])) {
    $dataHotNewsCommon = $newsMObject->getAllNewsByPara($_GET['type'], 'common', date('Y-m-d H:i:s'), $Page, (($_GET['Page'] - 1) * $Page));
} else {
    if (isset($_GET['type'])) {
        $dataHotNewsCommon = $newsMObject->getAllNewsByPara($_GET['type'], 'common', date('Y-m-d H:i:s'), $Page, 1);
    } else {
        header("Location: /");
    }
}
$dataHotNewsHighlight = $newsMObject->getAllNewsByParaWeek($_GET['type'], 'common', 7);
//$dataHotNewsHead = $newsMObject->getAllNewsByPara($_GET['type'], 'common', date('Y-m-d H:i:s'), 1);
$dataVideoHighlight = $videoMObject->getSlideVideosCategory($_GET['type'], '', 5, '');
$videoSexy = $GalleryObject->getMediaGalleryVideoRandom(4);
$otherVideo = $videoMObject->getSlideVideosRandom($_GET['type'], 4);
//var_dump($otherVideo);
//exit;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="shortcut icon" type="images/x-icon" href="/images/favicon.ico.png">

    <title>ข่าวฟุตบอล ข่าวฟุตบอล<?php echo ((isset($_GET['type'])) ? (($_GET['type']=="thai")?'ไทย' : $_GET['type']):$_GET['type']) ?> วิเคราะห์บอล ข่าวฟุตบอล<?php echo ((isset($_GET['type'])) ? (($_GET['type']=="thai")?'ไทย' : $_GET['type']):$_GET['type']); ?>เมื่อคืน </title>
    <meta name="keywords" content="ข่าวฟุตบอล, ข่าวฟุตบอล<?php echo ((isset($_GET['type'])) ? (($_GET['type']=="thai")?'ไทย' : $_GET['type']):$_GET['type']) ?>, บ้านผลบอล, ไฮไลท์ฟุตบอล, ไฮไลท์บอลเมื่อคืน, บอลเมื่อคืน, บอลล่าสุด, เร็วที่สุด, คมชัดที่สุด">
    <meta name="description" content="ข่าวฟุตบอลด่วน ฟุตบอล<?php echo ((isset($_GET['type'])) ? (($_GET['type']=="thai")?'ไทย' : $_GET['type']):$_GET['type']) ?> ข่าวลือ ข่าวซื้อขาย ข่าวฟุตบอล<?php echo ((isset($_GET['type'])) ? (($_GET['type']=="thai")?'ไทย' : $_GET['type']):$_GET['type']) ?>ใหม่ล่าสุดได้ก่อนใคร รวมถึง รายงานผลบอลแบบสด ๆ, ดูตารางคะแนน ฟุตบอลtype<?php echo ((isset($_GET['type'])) ? (($_GET['type']=="thai")?'ไทย' : $_GET['type']):$_GET['type']) ?>, ไฮไลท์, สถิติ, คลิปวิดีโอ และอื่น ๆ อีกมากมาย">

    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/news.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="js/jquery-timeago/jquery.timeago.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="/js/moment.js"></script>

    <?php include 'view/index/googletagmanager.php'; ?>

</head>
<body>
<!--index menu-->
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<!--<nav class="navbar navbar-menu-v2">-->
<!--    --><?php //include $_SERVER['DOCUMENT_ROOT'].'/view/menu/navbar-menu-v2.php'; ?>
<!--</nav>-->
<div class="containter-fluid">
    <div class="bx-head-articles">
        <div class="hilight-cover"></div>
        <div class="articles-title">
            <div class="container">
                <div class="head-banner-news">
                    <a href="/articles?newsid=<?php echo $dataHotNewsCommon[0]->newsid; ?>">
                        <h1><?php echo $dataHotNewsCommon[0]->titleTh; ?></h1></a>
                    <div class="by-line no-line">
                        <div><label>by </label><label style="background-color: transparent;padding-left: 5px;"
                                                      class="timeMoreRecentedGallery"
                                                      date="<?php echo $dataHotNewsCommon[0]->createDatetime; ?>"></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="background-image_image2"
             style="background-image: url('<?php echo $dataHotNewsCommon[0]->imageLink ?>');"></div>
    </div>
</div>
<div class="container">

    <div class="row">
        <div class="col-sm-3">
            <div class="bx-sidebar-left" style="margin-top: -85px;">
                <div class="title">
                    <table>
                        <tr>
                            <td>
                                <?php if ($_GET['type'] == "premier league") { ?>
                                    <img src="/images/five-leagues/English-Premier-League.png">
                                <?php } else if ($_GET['type'] == "la liga") { ?>
                                    <img src="/images/five-leagues/La-Liga-Logo-1.png">
                                <?php } else if ($_GET['type'] == "bundesliga") { ?>
                                    <img src="/images/five-leagues/logo_bl.gif">
                                <?php } else if ($_GET['type'] == "serie a") { ?>
                                    <img src="/images/five-leagues/seriea1.png">
                                <?php } else if ($_GET['type'] == "league 1") { ?>
                                    <img src="/images/five-leagues/Ligue-1-logo-france.png">
                                <?php } else if ($_GET['type'] == "thai") { ?>
                                    <img src="/images/five-leagues/tpl.png">
                                <?php } else if ($_GET['type'] == "ucl") { ?>
                                    <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                <?php } else if ($_GET['type'] == "other") { ?>
                                    <img src="images/teams_clean/team_default_256x256.png">
                                <?php } else if ($_GET['type'] == "europa") { ?>
                                    <img src="/images/five-leagues/uropa.png">
                                <?php } ?>
                            </td>
                            <td>
                                <h3>ข่าวฟุตบอล</h3>
                                <h3><?php echo $_GET['type']; ?></h3>
                            </td>
                        </tr>
                    </table>


                </div>

                <div class="section-news-hot hotNew-side" style="">
                    <div class="tabs-head"><i class="fa fa-newspaper-o"></i> ข่าวฮอตประจำสัปดาห์</div>
                    <?php if (!empty($dataHotNewsHighlight)) { ?>
                        <?php foreach ($dataHotNewsHighlight as $key => $value) { ?>
                            <a href="/articles?newsid=<?php echo $value->newsid; ?>">
                                <div class="bx-popular-news">
                                    <div class="times-content timeMoreRecentedGallery"
                                         date="<?php echo $value->createDatetime; ?>"></div>
                                    <div class="box-popular-title"><?php echo $value->titleTh ?></div>
                                    <div><img src="<?php echo $value->imageLink ?>"></div>
                                </div>
                            </a>
                        <?php }
                    } ?>
                </div>

            </div>
            <div class="bx-sidebar-left" style="margin-top: 20px;">
                <div class="title" style="background-color: #fff;">
                    <table>
                        <tr>
                            <td>
                                <?php if ($_GET['type'] == "premier league") { ?>
                                    <img src="/images/five-leagues/English-Premier-League.png">
                                <?php } else if ($_GET['type'] == "la liga") { ?>
                                    <img src="/images/five-leagues/La-Liga-Logo-1.png">
                                <?php } else if ($_GET['type'] == "bundesliga") { ?>
                                    <img src="/images/five-leagues/logo_bl.gif">
                                <?php } else if ($_GET['type'] == "serie a") { ?>
                                    <img src="/images/five-leagues/seriea1.png">
                                <?php } else if ($_GET['type'] == "league 1") { ?>
                                    <img src="/images/five-leagues/Ligue-1-logo-france.png">
                                <?php } else if ($_GET['type'] == "thai") { ?>
                                    <img src="/images/five-leagues/tpl.png">
                                <?php } else if ($_GET['type'] == "ucl") { ?>
                                    <img src="/images/five-leagues/UEFA-Champions-League-1.png">
                                <?php } else if ($_GET['type'] == "other") { ?>
                                    <img src="images/teams_clean/team_default_256x256.png">
                                <?php } else if ($_GET['type'] == "europa") { ?>
                                    <img src="/images/five-leagues/uropa.png">
                                <?php } ?>
                            </td>
                            <td style="color: #0d497d;">
                                <h3>ไฮไลท์ฟุตบอล</h3>
                                <h3><?php echo $_GET['type']; ?></h3>
                            </td>
                        </tr>
                    </table>

                </div>
                <div class="section-news-hot"  style="background-color: #0d497d;">
                    <div class="tabs-head"><i class="fa fa-video-camera"></i> วีดีโอฮอตประจำสัปดาห์</div>
                    <?php foreach ($dataVideoHighlight as $key => $value) { ?>
                        <a href="/hothit-videos?videoId=<?php echo $value->video_id; ?>">
                            <div class="bx-popular-news">
                                <div class="times-content timeMoreRecentedGallery"
                                        date="<?php echo $value->create_datetime; ?>">
                                </div>
                                <div class="box-popular-title title-video-sidebar">
                                    <?php echo $value->title . ":" . $value->videosource ?>
                                </div>
                                <div>
                                    <?php if ($value->videosource == "dailymotion") { ?>
                                        <iframe id="large-videos-autoPlay" width="100%" height="150"
                                                data-autoplay="true"
                                                src="http://www.dailymotion.com/embed/video/<?php echo $value->videokey ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } elseif ($value->videosource == "twitter") { ?>
                                        <iframe id="large-videos-autoPlay" width="100%" height="150"
                                                data-autoplay="true"
                                                src="https://twitter.com/i/cards/tfw/v1/<?php echo $value->videokey ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } elseif ($value->videosource == "facebook") { ?>
                                        <iframe id="large-videos-autoPlay" width="100%" height="150"
                                                data-autoplay="true"
                                                src="https://www.facebook.com/v2.3/plugins/video.php?allowfullscreen=true&container_width=800&href=<?php echo $value->content ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } elseif ($value->videosource == "youtube") { ?>
                                        <iframe id="large-videos-autoPlay" width="100%" height="150"
                                                data-autoplay="true"
                                                src="http://www.youtube.com/embed/<?php echo $value->videokey ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } else if ($value->videosource == "streamable") { ?>
                                        <iframe id="large-videos-autoPlay" loop="0" width="100%" height="150"
                                                data-autoplay="true"
                                                src="https://streamable.com/s/<?php echo $value->videokey ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } ?>
                                </div>
                            </div>
                        </a>
                    <?php } ?>

                    <!--                     <div class="bx-popular-news">-->
                    <!--                         <div class="times-content">Nov 23, 2016</div>-->
                    <!--                         <div class="box-popular-title"><strong>เสือคืนป่า! ฟัลเกายิงประตู UCL รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี</strong></div>-->
                    <!--                         <div><img src="/images/6.jpg"></div>-->
                    <!--                     </div>-->
                    <!--                     <div class="bx-popular-news">-->
                    <!--                         <div class="times-content">Nov 23, 2016</div>-->
                    <!--                         <div class="box-popular-title"><strong>เสือคืนป่า! ฟัลเกายิงประตู UCL รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี</strong></div>-->
                    <!--                     </div>-->
                    <!--                     <div class="bx-popular-news">-->
                    <!--                         <div class="times-content">Nov 23, 2016</div>-->
                    <!--                         <div class="box-popular-title"><strong>เสือคืนป่า! ฟัลเกายิงประตู UCL รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี</strong></div>-->
                    <!--                     </div>-->
                    <!--                     <div class="bx-popular-news">-->
                    <!--                         <div class="times-content">Nov 23, 2016</div>-->
                    <!--                         <div class="box-popular-title"><strong>เสือคืนป่า! ฟัลเกายิงประตู UCL รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี</strong></div>-->
                    <!--                     </div>-->
                </div>
            </div>
        </div>

        <div class="col-sm-9">
            <div class="bx-content-type-news">
                <?php foreach ($dataHotNewsCommon as $key => $value) { ?>
                    <!--                     <div class="rows mg-bt15">-->
                    <?php if (($key + 1) % 3 !== 0) { ?>
                        <div class="col-sm-6" style="margin-bottom: 20px">
                            <a href="/articles?newsid=<?php echo $value->newsid; ?>">
                                <div class="bx-news-info">
                                    <div class="title-hover-news bg-transparent">
                                        <div class="story-image-frame__headline-and-byline">
                                           <p><?php echo $value->titleTh ?></p>
                                            <div class="by-line">
                                                <div><label><i class="fa fa-clock-o"></i></label>
                                                    <span style="padding-left: 5px;" class="timeMoreRecentedGallery"
                                                          date="<?php echo $value->createDatetime; ?>"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="crop-team imgTest2"
                                         style="background-image: url('<?php echo $value->imageLink ?>');"></div>
                                </div>
                            </a>
                        </div>
                        <!--                         <div class="col-sm-6" style="margin-bottom: 20px">-->
                        <!--                             <div class="bx-news-info">-->
                        <!--                                 <div class="title-hover-news bg-transparent">-->
                        <!--                                     <div class="story-image-frame__headline-and-byline">-->
                        <!--                                         Ex-BB&T CEO Allison Said to Be in Running for Treasury Chief-->
                        <!--                                         <div class="by-line">-->
                        <!--                                             <div><label>by</label> Jennifer Jacobs And David Carey</div>-->
                        <!--                                         </div>-->
                        <!--                                     </div>-->
                        <!--                                 </div>-->
                        <!--                                 <div class="crop-team"></div>-->
                        <!--                             </div>-->
                        <!--                         </div>-->
                        <!--                         <div style="clear: both;"></div>-->
                        <!--                     </div>-->

                        <!--                 <div class="rows mg-bt15">-->
                    <?php } elseif (($key + 1) % 3 == 0) { ?>
                        <div class="col-sm-12" style="margin-bottom: 20px">
                            <a href="/articles?newsid=<?php echo $value->newsid; ?>">
                                <div class="bx-news-info bx-news-info-full">
                                    <div class="title-hover-news">
                                        <div class="story-image-frame__headline-and-byline">
                                            <?php echo $value->titleTh ?>
                                            <div class="by-line no-line">
                                                <div><label><i class="fa fa-clock-o"></i></label>
                                                    <span style="padding-left: 5px;" class="timeMoreRecentedGallery"
                                                          date="<?php echo $value->createDatetime; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hilight-cover"></div>
                                    <div class="crop-team imgTest3"
                                         style="background-image: url('<?php echo $value->imageLink ?>');"></div>
                                </div>
                            </a>
                        </div>
                        <div style="clear: both;"></div>
                    <?php } ?>
                    <!--                 </div>-->
                    <!---->
                <?php } ?>
            </div>
            <div style="clear: both;"></div>
            <div class="text-center">
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <?php if ((isset($_GET['Page']) && $_GET['Page'] > 1)) { ?>
                            <li>
                                <a href="/news?type=<?php echo $_GET['type'] ?>&Page=<?php echo $_GET['Page'] - 1; ?>"
                                   aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                        <?php } ?>
                        <?php for ($i = 1; $i <= $maxPage[0]->maxPage; $i++) { ?>
                            <?php if ((($_GET['Page'] + 4 > $maxPage[0]->maxPage) and $i < 4) || (($i >= ($_GET['Page'] - 3) && $i <= ($_GET['Page'] + 3)) || $i >= ($maxPage[0]->maxPage - 3))) { ?>
                                <li class="<?php echo((isset($_GET['Page']) && $_GET['Page'] == $i) ? 'active' : '') ?>">
                                    <a href="/news?type=<?php echo $_GET['type'] ?>&Page=<?php echo $i; ?>"><?php echo $i ?></a>
                                </li>
                            <?php } else { ?>
                                <?php if ((($_GET['Page'] + 4 > $maxPage[0]->maxPage) and $i == 4) || (($i == $_GET['Page'] + 4) and ($_GET['Page'] + 4 < $maxPage[0]->maxPage))) { ?>
                                    <li class="disabled"><a>...</a></li>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                        <?php if ((isset($_GET['Page']) && $_GET['Page'] < $maxPage[0]->maxPage)) { ?>
                            <li>
                                <a href="/news?type=<?php echo $_GET['type'] ?>&Page=<?php echo $_GET['Page'] + 1; ?>"
                                   aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <?php if(!empty($videoSexy) or !empty($otherVideo)){ ?>
    <div class="row row-recent-news" style="margin-top: 30px;">
        <div class="double-underline bx-tabs-related relate25">Related</div>
        <?php if(!empty($videoSexy)){ ?>
        <div class="wrap-content-cols">
            <div class="label-clip">Clip</div>
            <div class="wrap-video-sexy">
                <?php foreach ($videoSexy as $key => $value) { ?>
                    <div class="col-sm-3">
                        <div>
                            <a href="/other-videos-sexy?galleryId=<?= $value->id ?>">
                                <div class="box-videos box-video-sexy" style="">
                                    <div class="crop"
                                         style="background-image: url(<?= $value->thumbnail; ?>);"></div>
                                    <div class="icon-play"><i class="fa fa-play"></i></div>
                                </div>
                            </a>

                            <div class="bx-detail-gallery">
                                <a style="color: #000000;"
                                   href="/other-videos-sexy?galleryId=<?= $value->id ?>"><?= $value->title; ?></a>
                            </div>
                            <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                        class="how-long"><?= $value->update_at ?></span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div class="clearfix"></div>
            </div>
        </div>
        <?php } ?>
    </div>

    <?php if(!empty($otherVideo)){ ?>
        <div class="row  row-recent-news">
            <div class="wrap-content-cols">
                <div class="label-clip" style="color: #888;">Other</div>
                <div class="wrap-video-sexy">
                    <?php foreach ($otherVideo as $key => $value) { ?>
                        <div class="col-sm-3">
                            <div>
                                <a href="/<?php echo(($value->videotype == 'highlight') ? 'highlight' : 'hothit-videos') ?>?videoId=<?php echo $value->video_id ?>">
                                    <div class="box-videos box-video-sexy" style="">
                                        <div class="crop" style="background-image: url(<?= $value->urlImg; ?>);"></div>
                                        <div class="icon-play"><i class="fa fa-play"></i></div>
                                    </div>
                                </a>
                                <div class="bx-detail-gallery">
                                    <a style="color: #000000;"
                                       href="/<?php echo(($value->videotype == 'highlight') ? 'highlight' : 'hothit-videos') ?>?videoId=<?php echo $value->video_id ?>"><?= $value->title; ?></a>
                                </div>
                                <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                            class="how-long"><?= $value->create_datetime; ?></span></div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php } ?>

</div>
<script>
    loadMoment();
    function loadMoment() {
        $('.timeMoreRecentedGallery').each(function (index) {
            if ($.isNumeric($(this).attr('date'))) {
                $(this).html(moment($(this).attr('date'), "X").fromNow())
            } else {
                $(this).html(moment($(this).attr('date'), "YYYY-MM-DD HH:mm:ss").fromNow())
            }
        })
    }
</script>

<!--<script>-->
<!--    $('#myAffix').affix({-->
<!--        offset: {-->
<!--            top: 100,-->
<!--            bottom: function () {-->
<!--                return (this.bottom = $('.footer').outerHeight(true))-->
<!--            }-->
<!--        }-->
<!--    })-->
<!--</script>-->

</body>
</html>
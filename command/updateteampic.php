<?php
require_once __DIR__ . "/../bootstrap.php";
$teams = \model\TeamElo::all();
$host = "ngoal.com";
$servdir = __DIR__ . "/..";
$dir = "/images/team/";
foreach ($teams as $team) {
    $path = $dir . $team->id . "/";
    echo $team->id . ":" . $path;
    if (is_dir($servdir . $path)) {
        $filename = "nofile";
        $filesize = 0;
        foreach (scandir($servdir . $path, 1) as $file) {
            if (is_file($servdir . $path . $file)) {
                if($filesize==0){
                    $filename = $file;
                    $filesize = filesize($servdir . $path . $file);
                }
                echo "\t" . $file . "(" . filesize($servdir . $path . $file) . ")";
                if ($filesize < filesize($servdir . $path . $file)) {
                    $filename = $file;
                    $filesize = filesize($servdir . $path . $file);
                }

                $team->path = $path . $filename;
                $team->save();
            }
        }
        echo "\t ==>" . $filename;
    } else {
        echo " used default";
        $team->path = "/images/team/team_default_256x256.png";
        $team->save();
    }
    echo "\n";

}

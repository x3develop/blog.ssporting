<?php
require_once __DIR__ . "/../bootstrap.php";
$mcon = new \controller\MatchController();
$reviewer = \model\Reviewer::all();

foreach ($reviewer as $rv) {
    echo $rv->name . "\n";
    $predicts = \model\ReviewMatch::where("user_review_id", $rv->id)->orderBy("created_at", "DESC")->limit(40)->get();
    $rssum = 0;
    $currect = 0;
    foreach ($predicts as $pre) {
        if ($rssum < 30) {
            $rs = $mcon->winner($pre->match_id);
            if (!empty($rs)) {
                $rssum++;

                if ($rs == $pre->team) {
                    $currect++;
                }

            }
        } else {
            break;
        }
    }

    if ($rssum > 0) {
        echo $currect . "/" . $rssum . "=" . ($currect / $rssum * 100) . "\n";
        $rv->accurate = $currect / $rssum * 100;
        $rv->save();
    }else{
        $rv->accurate = 0;
        $rv->save();
    }

}

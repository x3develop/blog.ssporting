<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 6/25/2018
 * Time: 9:02 PM
 */
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playReviewMatch.php";
$playReviewMatch=new playReviewMatch();
$listUserReview=$playReviewMatch->getListUserReview();
$dataReturn=array();
foreach ($listUserReview as $key=>$value){
    $winRate=$playReviewMatch->getWinRate($key,30);
    $dataReturn[$key]=intval(100 - ((($winRate["total"] - $winRate["win"]) / $winRate["total"]) * 100));
}
echo json_encode($dataReturn);
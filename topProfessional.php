<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 6/26/2018
 * Time: 2:39 PM
 */
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playReviewMatch.php";
$playReviewMatch=new playReviewMatch();
$listUserReview=$playReviewMatch->getListUserReview();
$dataReturn=array();
foreach ($listUserReview as $key=>$value){
    $winRate=$playReviewMatch->getWinRate($key,30);
    $dataReturn[$key]['winRate']=$winRate['winRate'];
    $dataReturn[$key]['path']=$value['path'];
    $dataReturn[$key]['name']=$value['name'];
}
$playReviewMatch->array_sort_by_column($dataReturn, 'winRate',SORT_DESC);
$count=0;
foreach ($dataReturn as $key=>$value){
    if($count<10){?>
    <tr>
        <td><?php echo ($key+1);?></td>
        <td><img src="<?php echo $value['path'];?>"></td>
        <td><h4><?php echo $value['name'];?></h4></td>
        <td><?php echo number_format($value['winRate'], 2, '.', '');;?>%</td>
    </tr>
<?php }$count++; } ?>


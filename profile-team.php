<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/profile.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="/js/flogin/src/flogin.js"></script>
    <script src="nanoscrollbar/nanoscrollbar.js"></script>
</head>
<body>
<div id="fb-root"></div>
<div id="fb-root"></div>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<?php //include 'header.php'; ?>
<!--index menu-->
<?php include '/view/index/menu.php'; ?>
<nav class="navbar navbar-menu-v2">
    <div class="container">
        <div class="nav-menutop">
            <ul class="nav nav-pills">
                <li><a href=""><strong><i class="fa fa-futbol-o" aria-hidden="true"></i> SPORTBALL</a></strong></li>
                <li><a href="">หน้าหลัก</a></li>
                <li><a href="">ไฮไลท์บอล</a></li>
                <li><a href="">ข่าวบอลต่างประเทศ</a></li>
                <li><a href="">ข่าวบอลไทย</a></li>
                <li><a href="">ข่าวกีฬาอื่นๆ</a></li>
            </ul>
        </div>
    </div>
</nav>


<div class="wrapper-news-articles">
    <div class="containter-fluid">
        <div class="bx-head-profile" style="padding:50px 0px 50px 0px;">
            <div class="articles-title">
                <div class="container">
                    <div class="head-title-profile bx-profile-team">
                        <div class="pull-left"><img src="/images/logo-team/Chelsea.png"></div>
                        <div class="pull-left">
                            <h3>Chelsea</h3>

                            <div>
                                <table>
                                    <tr>
                                        <td>ประเทศ</td>
                                        <td>อังกฤษ</td>
                                    </tr>
                                    <tr>
                                        <td>เมือง</td>
                                        <td>แมนเชสเตอร์</td>
                                    </tr>
                                    <tr>
                                        <td>ก่อตั้ง</td>
                                        <td>1880</td>
                                    </tr>
                                    <tr>
                                        <td>สนาม</td>
                                        <td>Eithad Stadium</td>
                                    </tr>
                                    <tr>
                                        <td>Website:</td>
                                        <td>www.chelsea.com</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="background-image_image3"></div>
        </div>
    </div>


    <div class="container">

        <div class="rows" style="margin-top: 20px;">
            <div class="col-sm-3">
                <div class="section-menu-left">
                    <div>
                        <h3>ถ้วยรางวัล</h3>
                        <table class="table-list table-striped">
                            <thead>
                            <tr>
                                <th>การแข่งขัน</th>
                                <th>พื่นที่</th>
                                <th>ไปยัง</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>พรีเมียร์ลีก</td>
                                <td><img src="/images/countries/59.png"> อังกฤษ</td>
                                <td>4</td>
                            </tr>
                            <tr>
                                <td>แชมเปี้ยนชิฟ</td>
                                <td><img src="/images/countries/59.png"> อังกฤษ</td>
                                <td>7</td>
                            </tr>
                            <tr>
                                <td>UEFA Cup Winner Cup</td>
                                <td><img src="/images/countries/59.png"> อังกฤษ</td>
                                <td>7</td>
                            </tr>
                            <tr>
                                <td>FA Cup</td>
                                <td><img src="/images/countries/59.png"> อังกฤษ</td>
                                <td>7</td>
                            </tr>
                            <tr>
                                <td>League Cup</td>
                                <td><img src="/images/countries/59.png"> อังกฤษ</td>
                                <td>7</td>
                            </tr>
                            <tr>
                                <td class="text-right" colspan="2"><strong>ไปยัง</strong></td>
                                <td><strong>25</strong></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <h3>ตารางคะแนน</h3>
                        <table class="table-point-left">
                            <tbody>
                            <tr>
                                <th>Pos</th>
                                <th>Club</th>
                                <th>GD</th>
                                <th>Pts</th>
                            </tr>
                            <tr class="bg-pos1">
                                <td>1 <span class="down"><i class="fa fa-caret-down"></i></span></td>
                                <td><img src="http://api.ssporting.com/teams_clean/6716_32x32.png"> Muang Thong United
                                </td>
                                <td>+34</td>
                                <td>5</td>
                            </tr>
                            <tr class="bg-pos2">
                                <td>2 <span class="up"><i class="fa fa-caret-up"></i></span></td>
                                <td><img src="http://api.ssporting.com/teams_clean/5076_32x32.png"> Bangkok Glass</td>
                                <td>+24</td>
                                <td>6</td>
                            </tr>
                            <tr>
                                <td>3 <span class="static"><i class="fa fa-circle"></i></span></td>
                                <td><img src="http://api.ssporting.com/teams_clean/6718_32x32.png"> Buriram United</td>
                                <td>+23</td>
                                <td>1</td>
                            </tr>
                            <tr>
                                <td>4 <span class="down"><i class="fa fa-caret-down"></i></span></td>
                                <td><img src="http://api.ssporting.com/teams_clean/9488_32x32.png"> Suphanburi FC</td>
                                <td>+21</td>
                                <td>1</td>
                            </tr>
                            <tr>
                                <td>5 <span class="down"><i class="fa fa-caret-down"></i></span></td>
                                <td><img src="http://api.ssporting.com/teams_clean/3637_32x32.png"> Bangkok United</td>
                                <td>+12</td>
                                <td>4</td>
                            </tr>
                            <tr>
                                <td>6 <span class="up"><i class="fa fa-caret-up"></i></span></td>
                                <td><img src="http://api.ssporting.com/teams_clean/3637_32x32.png"> Bangkok United</td>
                                <td>+12</td>
                                <td>4</td>
                            </tr>
                            <tr>
                                <td>7 <span class="up"><i class="fa fa-caret-up"></i></span></td>
                                <td><img src="http://api.ssporting.com/teams_clean/3637_32x32.png"> Bangkok United</td>
                                <td>+12</td>
                                <td>4</td>
                            </tr>
                            <tr>
                                <td>8 <span class="up"><i class="fa fa-caret-up"></i></span></td>
                                <td><img src="http://api.ssporting.com/teams_clean/3637_32x32.png"> Bangkok United</td>
                                <td>+12</td>
                                <td>4</td>
                            </tr>
                            <tr>
                                <td>9 <span class="up"><i class="fa fa-caret-up"></i></span></td>
                                <td><img src="http://api.ssporting.com/teams_clean/3637_32x32.png"> Bangkok United</td>
                                <td>+12</td>
                                <td>4</td>
                            </tr>
                            <tr>
                                <td>10 <span class="up"><i class="fa fa-caret-up"></i></span></td>
                                <td><img src="http://api.ssporting.com/teams_clean/3637_32x32.png"> Bangkok United</td>
                                <td>+12</td>
                                <td>4</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <div>
                        <h3>ผู้จัดการทีม</h3>

                        <div class="h-line">ผู้รักษาประตู</div>
                        <table class="table-list-player table-striped">
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="h-line">กองหลัง</div>
                        <table class="table-list-player table-striped">
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="h-line">กองกลาง</div>
                        <table class="table-list-player table-striped">
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="img-player"><img src="/images/img-user.jpg"></div>
                                </td>
                                <td style="width: 30px;"><img src="/images/countries/48.png" style="width: 20px;"></td>
                                <td>
                                    <div>ซี บราโว</div>
                                    <div>33 ปี</div>
                                    <div class="tab-record"><img src="/images/yellow-card.png"> 5 <img
                                            src="/images/red-card.png"> 2 <img src="/images/football.png"> 0
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#all" aria-controls="home" role="tab"
                                                                  data-toggle="tab">ภาพรวมทั้งหมด</a></li>
                        <li role="presentation"><a href="#statistics" aria-controls="profile" role="tab" data-toggle="tab">สถิติ</a>
                        </li>
                        <li role="presentation"><a href="#table-played" aria-controls="messages" role="tab"
                                                   data-toggle="tab">ตารางแข่งขัน</a></li>
                        <li role="presentation"><a href="#buysell" aria-controls="settings" role="tab"
                                                   data-toggle="tab">ซื้อ/ขาย</a></li>
                        <li role="presentation"><a href="#related" aria-controls="settings" role="tab"
                                                   data-toggle="tab">ข่าวที่เกี่ยวข้อง</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="all">
                            <div class="bx-white-border" style="margin-top: 20px;">
                                <div class="title-articles">สถิติการลงเล่น</div>
                                <div>
                                    <table class="table-statistics">
                                        <tr>
                                            <td><strong>2,490</strong>

                                                <div>แข่งทั้งหมด</div>
                                            </td>
                                            <td><strong>490</strong>

                                                <div>ชนะ</div>
                                            </td>
                                            <td><strong>2,490</strong>

                                                <div>แพ้</div>
                                            </td>
                                            <td><strong>290</strong>

                                                <div>เสมอ</div>
                                            </td>
                                            <td><strong>90</strong>

                                                <div>ยิงประตู</div>
                                            </td>
                                            <td><strong>50</strong>

                                                <div>เสียประตู</div>
                                            </td>
                                            <td><strong>100</strong>

                                                <div>ไม่เสียประตู</div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                            <div class="bx-white-border" style="margin-top: 20px;">
                                <div class="title-articles">สถิติการแข่งขัน</div>
                                <div class="bx-h2h bx-h2h-stat">
                                    <div class="col-sm-4">
                                        <div>
                                            <div class="pull-left">
                                                <img src="/images/graph/circle-graph2.jpg" style="width: 100px;">
                                            </div>
                                            <div class="pull-left">
                                                <div>
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <div class="bx-percent">50%</div>
                                                                <span style="color: #0091c8;"><i
                                                                        class="fa fa-circle"></i></span>
                                                                <label>Win</label></td>
                                                            <td>
                                                                <div class="bx-percent">30%</div>
                                                                <span style="color: #e40520;"><i
                                                                        class="fa fa-circle"></i></span>
                                                                <label>Lose</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <div class="bx-percent">20%</div>
                                                                <span style="color: #888;"><i class="fa fa-
circle"></i></span>
                                                                <label>Draw</label>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div style="clear:both;"></div>
                                        </div>
                                        <div class="tab-txt text-center">สถิติการแข่งขันในลีก</div>
                                        <div style="clear:both;"></div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div>
                                            <div class="pull-left">
                                                <img src="/images/graph/circle-graph2.jpg" style="width: 100px;">
                                            </div>
                                            <div class="pull-left">
                                                <div>
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <div class="bx-percent">50%</div>
                                                                <span style="color: #0091c8;"><i
                                                                        class="fa fa-circle"></i></span>
                                                                <label>Win</label></td>
                                                            <td>
                                                                <div class="bx-percent">30%</div>
                                                                <span style="color: #e40520;"><i
                                                                        class="fa fa-circle"></i></span>
                                                                <label>Lose</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <div class="bx-percent">20%</div>
                                                                <span style="color: #888;"><i class="fa fa-
circle"></i></span>
                                                                <label>Draw</label>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div style="clear:both;"></div>
                                        </div>
                                        <div class="tab-txt text-center">สถิติการแข่งขันทั้งหมด</div>
                                        <div style="clear:both;"></div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div>
                                            <div class="pull-left">
                                                <img src="/images/graph/circle-graph2.jpg" style="width: 100px;">
                                            </div>
                                            <div class="pull-left">
                                                <div>
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <div class="bx-percent">50%</div>
                                                                <span style="color: #0091c8;"><i
                                                                        class="fa fa-circle"></i></span>
                                                                <label>Win</label></td>
                                                            <td>
                                                                <div class="bx-percent">30%</div>
                                                                <span style="color: #e40520;"><i
                                                                        class="fa fa-circle"></i></span>
                                                                <label>Lose</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <div class="bx-percent">20%</div>
                                                                <span style="color: #888;"><i class="fa fa-
circle"></i></span>
                                                                <label>Draw</label>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div style="clear:both;"></div>
                                        </div>
                                        <div class="tab-txt text-center">สถิติการชนะ Odds</div>
                                        <div style="clear:both;"></div>
                                    </div>
                                    <div style="clear: both;"></div>
                                </div>
                                <table class="table-h2h">
                                    <thead>
                                    <tr>
                                        <th>League</th>
                                        <th>Date</th>
                                        <th colspan="3">
                                            <div class="tab-filter text-center">
                                                <div>
                                                    <div class="times-content text-center">Filter by Home &amp; Away</div>
                                                    <div class="dropdown"
                                                         style="display:inline-block;margin-right: 10px; border-bottom: solid 1px #0091c8;">
                                                        <a href="">
                                                            All Matches
                                                            <span class="caret"></span>
                                                        </a>
                                                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                            <li><a href="">Premier League</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div style="clear:both;"></div>
                                            </div>
                                        </th>
                                        <th>HDP</th>
                                        <th>Odds</th>
                                        <th>Result</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>FA Cup</td>
                                        <td>28 December 2016</td>
                                        <td class="text-right"><span>Chelsea</span> <img
                                                src="/images/logo-team/Chelsea.png"></td>
                                        <td class="text-center">
                                            <div class="bx-label-hdp">-0.75</div>
                                        </td>
                                        <td class="text-left"><img src="/images/logo-team/manchester.png">
                                            <span>Manchester United</span></td>
                                        <td>-0.75</td>
                                        <td><img src="/images/icon-stat/win.png"></td>
                                        <td><img src="/images/icon-stat/win.png"></td>
                                    </tr>
                                    <tr>
                                        <td>FA Cup</td>
                                        <td>28 December 2016</td>
                                        <td class="text-right"><span>Chelsea</span> <img
                                                src="/images/logo-team/Chelsea.png"></td>
                                        <td class="text-center">
                                            <div class="bx-label-hdp">-0.75</div>
                                        </td>
                                        <td class="text-left"><img src="/images/logo-team/manchester.png">
                                            <span>Manchester United</span></td>
                                        <td>-0.75</td>
                                        <td><img src="/images/icon-stat/win.png"></td>
                                        <td><img src="/images/icon-stat/win.png"></td>
                                    </tr>
                                    <tr>
                                        <td>FA Cup</td>
                                        <td>28 December 2016</td>
                                        <td class="text-right"><span>Chelsea</span> <img
                                                src="/images/logo-team/Chelsea.png"></td>
                                        <td class="text-center">
                                            <div class="bx-label-hdp">-0.75</div>
                                        </td>
                                        <td class="text-left"><img src="/images/logo-team/manchester.png">
                                            <span>Manchester United</span></td>
                                        <td>-0.75</td>
                                        <td><img src="/images/icon-stat/win.png"></td>
                                        <td><img src="/images/icon-stat/win.png"></td>
                                    </tr>
                                    <tr>
                                        <td>FA Cup</td>
                                        <td>28 December 2016</td>
                                        <td class="text-right"><span>Chelsea</span> <img
                                                src="/images/logo-team/Chelsea.png"></td>
                                        <td class="text-center">
                                            <div class="bx-label-hdp">-0.75</div>
                                        </td>
                                        <td class="text-left"><img src="/images/logo-team/manchester.png">
                                            <span>Manchester United</span></td>
                                        <td>-0.75</td>
                                        <td><img src="/images/icon-stat/win.png"></td>
                                        <td><img src="/images/icon-stat/win.png"></td>
                                    </tr>
                                    <tr>
                                        <td>FA Cup</td>
                                        <td>28 December 2016</td>
                                        <td class="text-right"><span>Chelsea</span> <img
                                                src="/images/logo-team/Chelsea.png"></td>
                                        <td class="text-center">
                                            <div class="bx-label-hdp">-0.75</div>
                                        </td>
                                        <td class="text-left"><img src="/images/logo-team/manchester.png">
                                            <span>Manchester United</span></td>
                                        <td>-0.75</td>
                                        <td><img src="/images/icon-stat/win.png"></td>
                                        <td><img src="/images/icon-stat/win.png"></td>
                                    </tr>
                                    <tr>
                                        <td>FA Cup</td>
                                        <td>28 December 2016</td>
                                        <td class="text-right"><span>Chelsea</span> <img
                                                src="/images/logo-team/Chelsea.png"></td>
                                        <td class="text-center">
                                            <div class="bx-label-hdp">-0.75</div>
                                        </td>
                                        <td class="text-left"><img src="/images/logo-team/manchester.png">
                                            <span>Manchester United</span></td>
                                        <td>-0.75</td>
                                        <td><img src="/images/icon-stat/win.png"></td>
                                        <td><img src="/images/icon-stat/win.png"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="rows">
                                <div class="wrap-table-rows">
                                    <div class="col-sm-6">
                                        <div class="title-articles"><img src="images/logo-team/manchester.png" style="width: 30px;"> ตารางแข่งขัน</div>
                                        <table class="table-striped">
                                            <tr>
                                                <td class="">Chelsea</td>
                                                <td class="text-center">
                                                    <div class="bx-label-hdp">22:00</div>
                                                    <div class="times-content">EPL League 20/11/2016</div>
                                                </td>
                                                <td class="text-right">Manchester United</td>
                                            </tr>
                                            <tr>
                                                <td class="">Manchester United</td>
                                                <td class="text-center">
                                                    <div class="bx-label-hdp">22:00</div>
                                                    <div class="times-content">EPL League 20/11/2016</div>
                                                </td>
                                                <td class="text-right">Chelsea</td>
                                            </tr>
                                            <tr>
                                                <td class="">Chelsea</td>
                                                <td class="text-center">
                                                    <div class="bx-label-hdp">22:00</div>
                                                    <div class="times-content">EPL League 20/11/2016</div>
                                                </td>
                                                <td class="text-right">Manchester United</td>
                                            </tr>
                                            <tr>
                                                <td class="">Manchester United</td>
                                                <td class="text-center">
                                                    <div class="bx-label-hdp">22:00</div>
                                                    <div class="times-content">EPL League 20/11/2016</div>
                                                </td>
                                                <td class="text-right">Chelsea</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="title-articles">ซื้อ/ขายนักเตะ</div>
                                        <table class="table-market">
                                            <tr>
                                                <td>25/08/2559</td>
                                                <td><img src="images/countries/64.png"> ซี บราโว</td>
                                                <td>Bacelona</td>
                                                <td>$345</td>
                                            </tr>
                                            <tr>
                                                <td>25/08/2559</td>
                                                <td><img src="images/countries/65.png"> ซี บราโว</td>
                                                <td>Pablo Bali</td>
                                                <td>N/A</td>
                                            </tr>
                                            <tr>
                                                <td>25/08/2559</td>
                                                <td><img src="images/countries/64.png"> ซี บราโว</td>
                                                <td>Bacelona</td>
                                                <td>$345</td>
                                            </tr>
                                            <tr>
                                                <td>25/08/2559</td>
                                                <td><img src="images/countries/65.png"> ซี บราโว</td>
                                                <td>Pablo Bali</td>
                                                <td>N/A</td>
                                            </tr>
                                            <tr>
                                                <td>25/08/2559</td>
                                                <td><img src="images/countries/64.png"> ซี บราโว</td>
                                                <td>Bacelona</td>
                                                <td>$345</td>
                                            </tr>
                                            <tr>
                                                <td>25/08/2559</td>
                                                <td><img src="images/countries/65.png"> ซี บราโว</td>
                                                <td>Pablo Bali</td>
                                                <td>N/A</td>
                                            </tr>
                                            <tr>
                                                <td>25/08/2559</td>
                                                <td><img src="images/countries/64.png"> ซี บราโว</td>
                                                <td>Bacelona</td>
                                                <td>$345</td>
                                            </tr>
                                            <tr>
                                                <td>25/08/2559</td>
                                                <td><img src="images/countries/65.png"> ซี บราโว</td>
                                                <td>Pablo Bali</td>
                                                <td>N/A</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="clear: both;"></div>
                                </div>
                            </div>
                            <div class="rows">
                                <div class="title-articles">ข่าวที่เกี่ยวข้อง</div>
                                <div class="col-sm-4">
                                    <div class="bx-story-small">
                                        <div class="hilight-cover"></div>
                                        <div class="crop" style="background-image: url(http://www.gudoball.com/wp-content/uploads/2016/11/hd-radamel-falcao-monaco_12j2awfbt2uvy16xckijm0c338.jpg)"></div>
                                        <div class="bx-story-content">
                                            <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long"> 2 hour ago</span>
                                            </div>
                                            <a href="" class="h-content">เสือคืนป่า! ฟัลเกายิงประตู UCL รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="bx-story-small">
                                        <div class="hilight-cover"></div>
                                        <div class="crop" style="background-image: url(http://www.gudoball.com/wp-content/uploads/2016/11/hd-radamel-falcao-monaco_12j2awfbt2uvy16xckijm0c338.jpg)"></div>
                                        <div class="bx-story-content">
                                            <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long"> 2 hour ago</span>
                                            </div>
                                            <a href="" class="h-content">เสือคืนป่า! ฟัลเกายิงประตู UCL รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="bx-story-small">
                                        <div class="hilight-cover"></div>
                                        <div class="crop" style="background-image: url(http://www.gudoball.com/wp-content/uploads/2016/11/hd-radamel-falcao-monaco_12j2awfbt2uvy16xckijm0c338.jpg)"></div>
                                        <div class="bx-story-content">
                                            <div class="times-content"><i class="fa fa-clock-o"></i> <span class="how-long"> 2 hour ago</span>
                                            </div>
                                            <a href="" class="h-content">เสือคืนป่า! ฟัลเกายิงประตู UCL รอบแบ่งกลุ่มครั้งแรกในรอบ 6 ปี</a>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div role="tabpanel" class="tab-pane" id="statistics">สถิติ...</div>
                        <div role="tabpanel" class="tab-pane" id="table-played">ตารางแข่งขัน...</div>
                        <div role="tabpanel" class="tab-pane" id="buysell">ซื้อ/ขาย...</div>
                        <div role="tabpanel" class="tab-pane" id="related">ข่าวที่เกี่ยวข้อง...</div>
                    </div>

                </div>
            </div>
            <div style="clear: both;"></div>
        </div>


    </div>
</div>
<?php //include 'footer.php';
include 'view/index/footer.php';
?>

<script>
    $(".nano").nanoScroller();
</script>

</body>
</html>
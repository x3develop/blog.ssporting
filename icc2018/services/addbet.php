<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 7/26/2018
 * Time: 17:08
 */

require_once __DIR__ . "/../bootstrap.php";


$fb_id = $_REQUEST['fb_id'];
$minute = $_REQUEST['minute'];

$dt = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", "2018-07-28 18:00:00");
$dt->subHours(7);
$nt = \Carbon\Carbon::now();

$bet = null;

if ($nt <= $dt) {
    $bcon = new \controller\BetController();
    $bet = $bcon->add($fb_id, $minute);
}

header("ContentType:application/json");
echo json_encode($bet);
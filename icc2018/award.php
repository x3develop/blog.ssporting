<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link href="/icc2018/css/icc2018-style.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style-new.css">
    <!--    <link rel="stylesheet" href="/css/reset.css">-->

    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.js"></script>

</head>
<?php
require_once __DIR__ . "/bootstrap.php";

$isMobile = (bool)preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet' .
    '|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]' .
    '|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', $_SERVER['HTTP_USER_AGENT']);

if ($isMobile) {
    header('Location: /icc2018/mobile-award.php');
}
?>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="container-icc2018">
    <div class="bg-content-icc2018">
        <img src="/icc2018/img/icc-award.png">
    </div>
</div>
</body>
</html>
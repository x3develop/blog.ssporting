<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="กิจกรรมตามหาลูกเเรก"/>
    <meta property="og:description" content="ทายว่าประตูแรก ยิงนาทีเท่าไหร่ แจกรางวัลเงินสด"/>
    <meta property="og:image" content="https://ngoal.com/images/icc2018.jpg"/>

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link href="/icc2018/css/icc2018-style.css" rel="stylesheet">
    <link href="/icc2018/css/icc2018-style-mobile.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style-new.css">
    <!--    <link rel="stylesheet" href="/css/reset.css">-->
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <style>
        input[type='number'] {
            -moz-appearance: textfield;
        }

        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        .topmenu ul {
            margin: 0;
            padding: 0;
        }

        @media only screen and (max-width: 750px) {
            .content-profile-menu ul li {
                padding: 0;
            }

            .content-menu-index {
                z-index: 11;
            }
        }
    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.js"></script>

</head>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="container-icc2018">
    <div class="container container-mobile" style="">
        <div class="pull-left"><img src="/icc2018/img/logo1.jpg"></div>
        <div class="pull-right text-right">
            <p style="margin: 0">Sun 29 Jul</p>
            <p>04.05</p>
        </div>
        <div style="clear: both;"></div>
        <div class="col-lg-12" style="margin: 15px 0;">
            <h4>กิจกรรมตามหาลูกแรก</h4>
        </div>


        <div class="row-logo">
            <div class="col-xs-6" style="background-color: #fff; padding: 20px 0;">
                <img src="/icc2018/img/man-u.png">
            </div>
            <div class="col-xs-6" style="background-color: #820d0d; padding: 20px 0;">
                <img src="/icc2018/img/liverpool.png">
            </div>
        </div>

        <h4 style="font-size: 150%; line-height: 50px;">ทายว่า <u>ประตูแรก</u> ยิงนาทีที่เท่าไหร่</h4>
        <div class="box-select-num">
            <form id="add-form">
                <div class="form-inline text-center">
                    <ul>
                        <li>เลือกนาที</li>
                        <li><input style="width: 85px;" name="at-minute" id="at-minute" type="number" required min="0"
                                   max="99" class="form-control" placeholder="0-99"></li>
                        <li>
                            <div class="pull-left">นาที</div>
                        </li>
                    </ul>
                </div>
                <button id="save-minute" type="submit" class="btn btn-lg btn-default"><span
                            class="glyphicon glyphicon-send"
                            aria-hidden="true"></span> ส่งคำตอบ
                </button>
            </form>
        </div>

        <p>***กรณีที่จบเสมอ 0-0 ให้ตอบนาทีที่ 0</p>
        <h5>แจกรางวัลเงินสดร่วม 1,500 บาท 6 รางวัล สำหรับผู้ทายเวลาถูกหรือใกล้เคียงที่สุด</h5>

    </div>
</div>
</body>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://connect.facebook.net/en_US/sdk.js"></script>
<script type='text/javascript'>
    var hassession = <?php echo (isset($_SESSION['login'])) ? 1 : 0; ?>;
</script>

<script>
    FB.init({
        // appId      : '1630409860331473',
        appId: '188797391962899',
        // appId: '1761102130633089',
        cookie: true,  // enable cookies to allow the server to access
                       // the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.8
    });

</script>
<script src="/icc2018/js/icc.js?<?php echo date("Ymdhis") ?>"></script>
</html>
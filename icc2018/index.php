<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="กิจกรรมตามหาลูกเเรก"/>
    <meta property="og:description" content="ทายว่าประตูแรก ยิงนาทีเท่าไหร่ แจกรางวัลเงินสด"/>
    <meta property="og:image" content="https://ngoal.com/images/icc2018.jpg"/>

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link href="/icc2018/css/icc2018-style.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style-new.css">
    <!--    <link rel="stylesheet" href="/css/reset.css">-->
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">
    <style>
        input[type='number'] {
        -moz-appearance:textfield;
        }

        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
        }

        .topmenu ul {
            margin: 0;
            padding: 0;
        }

        @media only screen and (max-width: 750px) {
            .content-profile-menu ul li {
                padding: 0;
            }

            .content-menu-index {
                z-index: 11;
            }
        }

    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.js"></script>

</head>
<?php
require_once __DIR__ . "/bootstrap.php";

$isMobile = (bool)preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet' .
    '|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]' .
    '|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', $_SERVER['HTTP_USER_AGENT']);

if ($isMobile) {
    header('Location: /icc2018/mobile.php');
}
?>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="container-icc2018">
    <div class="box-select-num">
        <form id="add-form">
            <div class="form-inline">
                <label>เลือกนาที :</label>
                <input name="at-minute" id="at-minute" type="number" required min="0" max="99" style="width: 100px;"
                       class="form-control"
                       placeholder="0-99">
                <label>น.</label>
                <button id="save-minute" type="submit" class="btn btn-default"><span class="glyphicon glyphicon-send"
                                                                                     aria-hidden="true"></span> ส่งคำตอบ
                </button>
            </div>
        </form>
        <!--        <div>เลือกนาที</div>-->
        <!--        <div class="form-inline">-->
        <!--            <div class="form-group">-->
        <!--                <input type="text" class="form-control"  placeholder="15">-->
        <!--            </div>-->
        <!---->
        <!--        </div>-->
        <!--        <div class="dropdown hide">-->
        <!--            <a href="#" class="dropdown-toggle" data-toggle="dropdown">-->
        <!--                0-99-->
        <!--                <span class="caret"></span>-->
        <!--            </a>-->
        <!--            <ul class="dropdown-menu" id="menu2" aria-labelledby="drop5">-->
        <!--                <li><a href="#">0</a></li>-->
        <!--                <li><a href="#">1</a></li>-->
        <!--                <li><a href="#">99</a></li>-->
        <!--            </ul>-->
        <!--        </div>-->
        <!---->
        <!--        <div>ส่งคำตอบ</div>-->
    </div>
    <div class="bg-content-icc2018">
        <img src="/icc2018/img/content-icc.jpg">
    </div>
</div>
</body>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://connect.facebook.net/en_US/sdk.js"></script>

<script type='text/javascript'>
    var hassession = <?php echo (isset($_SESSION['login'])) ? 1 : 0; ?>;
</script>

<script>
    FB.init({
        // appId      : '1630409860331473',
        appId: '188797391962899',
        // appId: '1761102130633089',
        cookie: true,  // enable cookies to allow the server to access
                       // the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.8
    });

</script>
<script src="/icc2018/js/icc.js?<?php echo date("Ymdhis")?>"></script>

</body>
</html>
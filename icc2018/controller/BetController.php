<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 7/26/2018
 * Time: 17:10
 */

namespace controller;

use model\Bet;

require_once __DIR__ . "/../bootstrap.php";

class BetController
{
    public function add($fb_uid, $minute)
    {
        $bet = Bet::where("fb_id", $fb_uid)->first();
        if (empty($bet)) {
            $bet = new Bet();
            $bet->fb_id = $fb_uid;
            $bet->at_minute = $minute;
            $bet->save();
        }
        return $bet;
    }

    public function getByFB($fb_uid)
    {
        return $bet = Bet::where("fb_id", $fb_uid)->first();
    }
}
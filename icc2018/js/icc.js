$(document).ready(function () {
    var fbid = "0";
    checkLoginState();
    // checkplay();
    $("#add-form").on("submit", function (e) {
        e.preventDefault();
        if (fbid !== "0") {
            var minite = $("#at-minute").val();
            $.ajax({
                url: "/icc2018/services/addbet.php",
                method: "GET",
                data: {fb_id: fbid, minute: minite},
                dataType: "JSON"
            }).done(function (res) {
                console.log(res);
                if (res) {
                    swal("เข้าร่วมกิจกรรมเรียบร้อย", {
                        icon: "success",
                    });

                    $("#at-minute").attr("disabled", "disabled");
                    $("#save-minute").attr("disabled", "disabled");
                }else{
                    swal("หมดเวลาทายผลแล้ว");
                }
            })
        } else {
            FBlogin();
        }

    });

    function checkLoginState() {
        FB.getLoginStatus(function (response) {
            // console.log(response);
            if (response.status === 'connected' && isLogin()) {
                updateUser(response.authResponse.accessToken);

            } else {
                // $(".btn-sign-FB").removeClass("hide");
                if (hassession == 1) {
                    FBlogin();
                    // fbLogin();
                }
            }

        });
    }

    function updateUser(token) {
        FB.api('/me', function (response) {
            fbid = response.id;
            checkplay();
            $.ajax({
                url: "/icc2018/services/updateuser.php",
                method: "POST",
                data: {fb_id: response.id, name: response.name, access_token: token},
                dataType: "JSON"
            }).done(function (res) {

            });

        });
    }

    function fbLogin() {
        FB.login(function (response) {
            location.reload();
        });

    }

    function isLogin() {
        // console.log(fbid);
        if (hassession == 0) {
            return false;
        } else {
            return true;
        }
    }

    function checkplay() {
        $.ajax({
            url: "/icc2018/services/getbet.php",
            method: "POST",
            data: {fb_id: fbid},
            dataType: "JSON"
        }).done(function (res) {
            if (res) {
                $("#at-minute").val(res.at_minute);
                $("#at-minute").attr("disabled", "disabled");
                $("#save-minute").attr("disabled", "disabled");
            }

        });
    }

});
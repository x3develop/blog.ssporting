<?php
/**
 * Created by PhpStorm.
 * User: pongs
 * Date: 6/26/2018
 * Time: 4:06 PM
 */

namespace model;
require_once __DIR__."/../bootstrap.php";

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = "answer";
    protected $fillable=['eid', 'rid','fb_id',',answer','created_at', 'updated_at'];

    public function event(){
        return $this->belongsTo(Event::class,"id","eid");
    }

    public function owner()
    {
        return $this->belongsTo(User::class, "fb_id", "fb_id");
    }

    public function reward()
    {
        return $this->hasOne(Reward::class, "id", "rid");
    }

}
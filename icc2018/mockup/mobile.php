<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link href="/icc2018/css/icc2018-style.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.css">
    <!--    <link rel="stylesheet" href="/css/reset.css">-->

    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.js"></script>

</head>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="container-icc2018">
    <div class="container container-mobile" style="">
        <div class="pull-left"><img src="/icc2018/img/logo1.jpg"></div>
        <div class="pull-right text-right">
            <p style="margin: 0">Sun 29 Jul</p>
            <p>04.05</p>
        </div>
<div style="clear: both;"></div>
<div class="col-lg-12" style="margin: 15px 0;">
    <h4>กิจกรรมตามหาลูกแรก</h4>
</div>


        <div class="row-logo">
            <div class="col-xs-6" style="background-color: #fff; padding: 20px 0;">
                <img src="/icc2018/img/man-u.png">
            </div>
            <div class="col-xs-6" style="background-color: #820d0d; padding: 20px 0;">
                <img src="/icc2018/img/liverpool.png">
            </div>
        </div>

        <h4 style="font-size: 150%; line-height: 50px;">ทายว่า <u>ประตูแรก</u> ยิงนาทีที่เท่าไหร่</h4>
        <div class="box-select-num">
            <div class="form-inline text-center">


                    <ul>
                        <li>เลือกนาที</li>
                        <li><input style="width: 85px;" type="text" class="form-control" placeholder="0-99"></li>
                        <li><div class="pull-left">นาที</div></li>
                    </ul>



            </div>
            <button type="submit" class="btn btn-lg btn-default"><span class="glyphicon glyphicon-send"
                                                                       aria-hidden="true"></span> ส่งคำตอบ
            </button>
        </div>

        <p>***กรณีที่จบเสมอ 0-0 ให้ตอบนาทีที่ 0</p>
        <h5>แจกรางวัลเงินสดร่วม 1,500 บาท 6 รางวัล สำหรับผู้ทายเวลาถูกหรือใกล้เคียงที่สุด</h5>

    </div>
</div>
</body>
</html>
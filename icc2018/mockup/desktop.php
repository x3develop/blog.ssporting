<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link href="/icc2018/css/icc2018-style.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style-new.css">
    <!--    <link rel="stylesheet" href="/css/reset.css">-->

    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.js"></script>

</head>
<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="container-icc2018">
    <div class="box-select-num">
        <div class="form-inline">

                <label>เลือกนาที :</label>
                <input style="width: 100px;" type="text" class="form-control"  placeholder="0-99">
            <label>น.</label>
            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-send" aria-hidden="true"></span> ส่งคำตอบ</button>
        </div>
<!--        <div>เลือกนาที</div>-->
<!--        <div class="form-inline">-->
<!--            <div class="form-group">-->
<!--                <input type="text" class="form-control"  placeholder="15">-->
<!--            </div>-->
<!---->
<!--        </div>-->
<!--        <div class="dropdown hide">-->
<!--            <a href="#" class="dropdown-toggle" data-toggle="dropdown">-->
<!--                0-99-->
<!--                <span class="caret"></span>-->
<!--            </a>-->
<!--            <ul class="dropdown-menu" id="menu2" aria-labelledby="drop5">-->
<!--                <li><a href="#">0</a></li>-->
<!--                <li><a href="#">1</a></li>-->
<!--                <li><a href="#">99</a></li>-->
<!--            </ul>-->
<!--        </div>-->
<!---->
<!--        <div>ส่งคำตอบ</div>-->
    </div>
    <div class="bg-content-icc2018">
        <img src="/icc2018/img/content-icc.jpg">
    </div>
</div>
</body>
</html>
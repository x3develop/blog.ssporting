<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 6/25/2018
 * Time: 2:36 PM
 */
$id=((isset($_GET['id']))?$_GET['id']:null);
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playReviewMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";

$playMatch = new playMatch();
$playReviewMatch = new playReviewMatch();
$reviewMatch = $playReviewMatch->getReviewMatchByMatchId($id);
$match = $playMatch->getFirstMathById($id)[0];
$reviewHome=0;
$reviewAway=0;
foreach ($reviewMatch as $key => $value) {
    if($value['team']=="home"){
        $reviewHome++;
    }else if($value['team']=="away"){
        $reviewAway++;
    }
    $winRate = $playReviewMatch->getWinRate($value['user_review_id'], 30);
    if ($winRate["total"] != 0) {
        $resultsReviewMatch[$value['user_review_id']] = intval(100 - ((($winRate["total"] - $winRate["win"]) / $winRate["total"]) * 100));
    } else {
        $resultsReviewMatch[$value['user_review_id']] = 0;
    }
    $playReviewMatchList[$value['user_review_id']]=$playReviewMatch->getResultsReviewMatchByUserReview($value['user_review_id'],15);
}

?>


<div class="modal-header" style="padding: 0px;border-bottom: transparent;">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a style="padding: 15px 20px;" href="#home-<?php echo $id;?>" aria-controls="home" role="tab" data-toggle="tab">
                <div style="font-size: 20px;">ทั้งหมด(<?php echo count($reviewMatch); ?>)</div></a></li>
        <li role="presentation"><a style="padding: 15px 20px;font-size: 20px;" href="#profile-<?php echo $id;?>" aria-controls="profile" role="tab" data-toggle="tab">
                <img style="width: 26px;" src="<?php echo $match->teamHomePath ?>"> <?php echo $match->teamHomeEn;?>(<?php echo $reviewHome;?>)</a></li>
        <li role="presentation"><a style="padding: 15px 20px;font-size: 20px;" href="#messages-<?php echo $id;?>" aria-controls="messages" role="tab" data-toggle="tab">
                <img style="width: 26px;" src="<?php echo $match->teamAwayPath ?>"> <?php echo $match->teamAwayEn;?>(<?php echo $reviewAway;?>)</a></li>
    </ul>
</div>
<div class="modal-body">
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home-<?php echo $id;?>">
            <?php if(!empty($reviewMatch)){ ?>
                <?php foreach ($reviewMatch as $key => $value) { ?>
                    <div class="box-preview" style="padding: 15px; <?php echo (($key%2==0)?'background-color: #e4e4e4':'');?>">
                        <div class="top-box-preview">
                            <table>
                                <tr>
                                    <td>
                                        <div class="box-guru-img pull-left">
                                            <img style="height: 40px;" src="<?= $value['path'] ?>">
                                        </div>
                                        <div class="pull-left">
                                            <h5><?= $value['name'] ?></h5>
                                            <div class="box-player-preview">
                                                <?php if ($value['team'] == "home") { ?>
                                                    <img src="<?= $value['homepath'] ?>">
                                                    <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                <?php } elseif ($value['team'] == "away") { ?>
                                                    <img src="<?= $value['awaypath'] ?>">
                                                    <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="pull-left">
                                            <ul class="box-betlist">
                                                <?php if (!empty($playReviewMatchList[$value['user_review_id']])) { ?>
                                                    <?php foreach ($playReviewMatchList[$value['user_review_id']] as $keyr => $valuer) { ?>
                                                        <?php if ($valuer['results'] == "win") { ?>
                                                            <li><img title="<?= $valuer['updated_at'] ?>"
                                                                     src="/images/icon-stat/betlist_result/win.png"></li>
                                                        <?php } else if ($valuer['results'] == "lose") { ?>
                                                            <li><img title="<?= $valuer['updated_at'] ?>"
                                                                     src="/images/icon-stat/betlist_result/lose.png"></li>
                                                        <?php } else if ($valuer['results'] == "equal") { ?>
                                                            <li><img title="<?= $valuer['updated_at'] ?>"
                                                                     src="/images/icon-stat/betlist_result/draw.png"></li>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="pull-right">
                                            <h3><?= $resultsReviewMatch[$value['user_review_id']]; ?>%</h3>
                                            <p>Win rate</p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="button-box-preview">
                            <div>
                                <?= $value['review_text'] ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="profile-<?php echo $id;?>">
            <?php if(!empty($reviewMatch)){ ?>
                <?php foreach ($reviewMatch as $key => $value) { ?>
                    <?php if($value['team']=="home"){ ?>
                        <div class="box-preview" style="padding: 15px; <?php echo (($key%2==0)?'background-color: #e4e4e4':'');?>">
                            <div class="top-box-preview">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="box-guru-img pull-left">
                                                <img style="height: 40px;" src="<?= $value['path'] ?>">
                                            </div>
                                            <div class="pull-left">
                                                <h5><?= $value['name'] ?></h5>
                                                <div class="box-player-preview">
                                                    <?php if ($value['team'] == "home") { ?>
                                                        <img src="<?= $value['homepath'] ?>">
                                                        <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                    <?php } elseif ($value['team'] == "away") { ?>
                                                        <img src="<?= $value['awaypath'] ?>">
                                                        <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="pull-left">
                                                <ul class="box-betlist">
                                                    <?php if (!empty($playReviewMatchList[$value['user_review_id']])) { ?>
                                                        <?php foreach ($playReviewMatchList[$value['user_review_id']] as $keyr => $valuer) { ?>
                                                            <?php if ($valuer['results'] == "win") { ?>
                                                                <li><img title="<?= $valuer['updated_at'] ?>"
                                                                         src="/images/icon-stat/betlist_result/win.png"></li>
                                                            <?php } else if ($valuer['results'] == "lose") { ?>
                                                                <li><img title="<?= $valuer['updated_at'] ?>"
                                                                         src="/images/icon-stat/betlist_result/lose.png"></li>
                                                            <?php } else if ($valuer['results'] == "equal") { ?>
                                                                <li><img title="<?= $valuer['updated_at'] ?>"
                                                                         src="/images/icon-stat/betlist_result/draw.png"></li>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="pull-right">
                                                <h3><?= $resultsReviewMatch[$value['user_review_id']]; ?>%</h3>
                                                <p>Win rate</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="button-box-preview">
                                <div>
                                    <?= $value['review_text'] ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="messages-<?php echo $id;?>">
            <?php if(!empty($reviewMatch)){ ?>
                <?php foreach ($reviewMatch as $key => $value) { ?>
                    <?php if($value['team']=="away"){ ?>
                        <div class="box-preview" style="padding: 15px; <?php echo (($key%2==0)?'background-color: #e4e4e4':'');?>">
                            <div class="top-box-preview">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="box-guru-img pull-left">
                                                <img style="height: 40px;" src="<?= $value['path'] ?>">
                                            </div>
                                            <div class="pull-left">
                                                <h5><?= $value['name'] ?></h5>
                                                <div class="box-player-preview">
                                                    <?php if ($value['team'] == "home") { ?>
                                                        <img src="<?= $value['homepath'] ?>">
                                                        <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                    <?php } elseif ($value['team'] == "away") { ?>
                                                        <img src="<?= $value['awaypath'] ?>">
                                                        <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="pull-left">
                                                <ul class="box-betlist">
                                                    <?php if (!empty($playReviewMatchList[$value['user_review_id']])) { ?>
                                                        <?php foreach ($playReviewMatchList[$value['user_review_id']] as $keyr => $valuer) { ?>
                                                            <?php if ($valuer['results'] == "win") { ?>
                                                                <li><img title="<?= $valuer['updated_at'] ?>"
                                                                         src="/images/icon-stat/betlist_result/win.png"></li>
                                                            <?php } else if ($valuer['results'] == "lose") { ?>
                                                                <li><img title="<?= $valuer['updated_at'] ?>"
                                                                         src="/images/icon-stat/betlist_result/lose.png"></li>
                                                            <?php } else if ($valuer['results'] == "equal") { ?>
                                                                <li><img title="<?= $valuer['updated_at'] ?>"
                                                                         src="/images/icon-stat/betlist_result/draw.png"></li>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="pull-right">
                                                <h3><?= $resultsReviewMatch[$value['user_review_id']]; ?>%</h3>
                                                <p>Win rate</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="button-box-preview">
                                <div>
                                    <?= $value['review_text'] ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>

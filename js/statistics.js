/**
 * Created by mrsyrop on 1/2/2560.
 */
$(document).ready(function () {
        google.charts.load("current", {packages: ["corechart"]});
        var mid = getQueryString("mid");
        //loadGraphMathHeadToHead('GameHistory');
        //LoadGraphOddsHeadToHead('GameHistory');
        var teamguide = null;
        var teamh2h = null;
        var guiderow_original = null;
        var rank_original = null;
        var leagueranking = null;
        var currentside = 'all';
        var currenth2hside = 'all';
        var currentlimit = 0;
        var currenth2hlimit = 0;

        //setTimeout(getEvent(mid), 1000);

        getH2H(mid);
        function getH2H(mid) {
            $.ajax({
                url: "/services/geth2hbymid.php",
                data: {mid: mid},
                method: "GET",
                dataType: "JSON"
            }).done(function (response) {
                teamh2h = response;
                showH2H(response, 'all', 10);
                $('#h2h-h-max-match').text("(" + response['h2h'].length + ")");
                $('#h2h-g-max-match').text("(" + response['h2h'].length + ")");
                genMaxView('#h2h-h-select-limit', response['h2h'].length, 'h2h');
                genMaxView('#h2h-g-select-limit', response['h2h'].length, 'h2h');
            });
        }

        function showH2H(data, side, limit) {
            console.log(side, limit);
            if (guiderow_original == null) {
                guiderow_original = $("#guide-row-original").clone();
            }
            $("#teamhome-h2h-list").empty();
            $("#teamaway-h2h-list").empty();
            var donuthresult = [['result', 'value'], ['draw', 0], ['lose', 0], ['win', 0]];
            var donuthoresult = [['result', 'value'], ['draw', 0], ['lose', 0], ['win', 0]];
            var donutgresult = [['result', 'value'], ['draw', 0], ['lose', 0], ['win', 0]];
            var donutgoresult = [['result', 'value'], ['draw', 0], ['lose', 0], ['win', 0]];
            var hrssum = 0;
            var horssum = 0;
            var grssum = 0;
            var gorssum = 0;
            var hround = 0;
            var ground = 0;
            var hlimit = limit;
            var glimit = limit;
            var passcon = false;
            var hid = $('.h2h-select[tab=team1]').attr('tid');
            var gid = $('.h2h-select[tab=team2]').attr('tid');

            if (limit == 0) {
                hlimit = data['h2h'].length;
                glimit = data['h2h'].length;
            }

            $.each(data['h2h'], function (k, v) {
                    var mapdata = mapKV(data['keylist']['h2h'], v);
                    passcon = false;
                    if (hround < hlimit) {

                        if (side == 'away') {
                            if (mapdata['gid'] == hid) {
                                passcon = true;
                            }
                        } else if (side == 'home') {
                            if (mapdata['hid'] == hid) {
                                passcon = true;
                            }
                        } else {
                            passcon = true;
                        }

                        if (passcon) {
                            var row = $(guiderow_original).clone();
                            $(row).find(".guide-league").first().text(v[17]);
                            var dt = moment(v[11], "YYYY-MM-DD");
                            $(row).find(".guide-date").first().text(dt.format("DD MMMM YYYY"));
                            $(row).find(".guide-home-name").first().text(v[19]);
                            var himgpath = getLogo(v[14], 0);
                            $(row).find(".guide-home-img").first().attr('src', himgpath);
                            $(row).find(".guide-away-name").first().text(v[21]);
                            var gimgpath = getLogo(v[15], 0);
                            $(row).find(".guide-away-img").first().attr('src', gimgpath);
                            $(row).find(".guide-odds").first().text(v[8]);
                            $(row).find(".guide-score").first().text(v[6] + "-" + v[7]);

                            var reverselogo = v[9];
                            if (mapdata['hid'] == hid) {
                                if (mapdata['home_score'] > mapdata['away_score']) {
                                    reverselogo = 0
                                } else if (mapdata['home_score'] < mapdata['away_score']) {
                                    reverselogo = 2;
                                } else {
                                    reverselogo = 1;
                                }
                            } else {
                                if (mapdata['home_score'] < mapdata['away_score']) {
                                    reverselogo = 0
                                } else if (mapdata['home_score'] > mapdata['away_score']) {
                                    reverselogo = 2;
                                } else {
                                    reverselogo = 1;
                                }
                            }
                            var rsimg = getResultImg(reverselogo);
                            $(row).find(".guide-result").first().attr('src', rsimg);
                            if (reverselogo == 0) {
                                donuthresult[3][1]++;
                            } else if (reverselogo == 1) {
                                donuthresult[1][1]++;
                            } else {
                                donuthresult[2][1]++;
                            }
                            console.log(donuthresult, reverselogo);
                            hrssum++;
                            var reverselogoodd = v[10];
                            if (reverselogoodd != -1) {
                                var scoreresult = parseFloat(mapdata['home_score']) + parseFloat(mapdata['odds']) - parseFloat(mapdata['away_score']);
                                console.log(mapdata['home_score'], mapdata['odds'], mapdata['away_score'], scoreresult);

                                if (Math.abs(scoreresult) == 0) {
                                    reverselogoodd = 2;
                                } else if (scoreresult > 0 && scoreresult <= 0.25) {
                                    if (mapdata['hid'] == hid) {
                                        reverselogoodd = 1;
                                    } else {
                                        reverselogoodd = 4;
                                    }
                                } else if (scoreresult >= 0.5) {
                                    if (mapdata['hid'] == hid) {
                                        reverselogoodd = 0;
                                    } else {
                                        reverselogoodd = 3;
                                    }
                                } else if (scoreresult < 0 && scoreresult >= -0.25) {
                                    if (mapdata['hid'] == hid) {
                                        reverselogoodd = 4;
                                    } else {
                                        reverselogoodd = 1;
                                    }
                                } else if (scoreresult <= -0.5) {
                                    if (mapdata['hid'] == hid) {
                                        reverselogoodd = 3;
                                    } else {
                                        reverselogoodd = 0;
                                    }
                                }

                                var orsimg = getOddsresultImg(reverselogoodd);
                                $(row).find(".guide-oddsresult").first().attr('src', orsimg);
                                horssum++;
                            } else {
                                $(row).find(".guide-oddsresult").first().addClass('hide');
                            }

                            if (reverselogoodd == 0 || reverselogoodd == 1) {
                                donuthoresult[3][1]++;
                            } else if (reverselogoodd == 2) {
                                donuthoresult[1][1]++;
                            } else if (reverselogoodd == [3] || reverselogoodd == 4) {
                                donuthoresult[2][1]++;
                            }
                            $("#teamhome-h2h-list").append($(row));
                        }
                    }
                    hround++;
                }
            )
            ;

            $.each(data['h2h'], function (k, v) {
                var mapdata = mapKV(data['keylist']['h2h'], v);
                passcon = false;
                if (ground < glimit) {
                    if (side == 'away') {
                        if (v[15] == gid) {
                            passcon = true;
                        }
                    } else if (side == 'home') {
                        if (v[14] == gid) {
                            passcon = true;
                        }
                    } else {
                        passcon = true;
                    }

                    if (passcon) {


                        var row = $(guiderow_original).clone();
                        $(row).find(".guide-league").first().text(v[17]);
                        var dt = moment(v[11], "YYYY-MM-DD");
                        $(row).find(".guide-date").first().text(dt.format("DD MMMM YYYY"));
                        $(row).find(".guide-home-name").first().text(v[19]);
                        var himgpath = getLogo(v[14], 0);
                        $(row).find(".guide-home-img").first().attr('src', himgpath);
                        $(row).find(".guide-away-name").first().text(v[21]);
                        var gimgpath = getLogo(v[15], 0);
                        $(row).find(".guide-away-img").first().attr('src', gimgpath);
                        $(row).find(".guide-odds").first().text(v[8]);
                        $(row).find(".guide-score").first().text(v[6] + "-" + v[7]);

                        var reverselogo = v[9];
                        if (mapdata['hid'] == gid) {
                            if (mapdata['home_score'] > mapdata['away_score']) {
                                reverselogo = 0
                            } else if (mapdata['home_score'] < mapdata['away_score']) {
                                reverselogo = 2;
                            } else {
                                reverselogo = 1;
                            }
                        } else {
                            if (mapdata['home_score'] < mapdata['away_score']) {
                                reverselogo = 0
                            } else if (mapdata['home_score'] > mapdata['away_score']) {
                                reverselogo = 2;
                            } else {
                                reverselogo = 1;
                            }
                        }
                        var rsimg = getResultImg(reverselogo);
                        $(row).find(".guide-result").first().attr('src', rsimg);
                        if (reverselogo == 0) {
                            donutgresult[3][1]++;
                        } else if (reverselogo == 1) {
                            donutgresult[1][1]++;
                        } else {
                            donutgresult[2][1]++;
                        }
                        grssum++;

                        var reverselogoodd = v[10];
                        if (reverselogoodd != -1) {
                            var scoreresult = parseFloat(mapdata['home_score']) + parseFloat(mapdata['odds']) - parseFloat(mapdata['away_score']);
                            console.log(mapdata['home_score'], mapdata['odds'], mapdata['away_score'], scoreresult);

                            if (scoreresult == 0) {
                                reverselogoodd = 2;
                            } else if (scoreresult > 0 && scoreresult <= 0.25) {
                                if (mapdata['hid'] == gid) {
                                    reverselogoodd = 1;
                                } else {
                                    reverselogoodd = 4;
                                }
                            } else if (scoreresult >= 0.5) {
                                if (mapdata['hid'] == gid) {
                                    reverselogoodd = 0;
                                } else {
                                    reverselogoodd = 3;
                                }
                            } else if (scoreresult < 0 && scoreresult >= -0.25) {
                                if (mapdata['hid'] == gid) {
                                    reverselogoodd = 4;
                                } else {
                                    reverselogoodd = 1;
                                }
                            } else if (scoreresult <= -0.5) {
                                if (mapdata['hid'] == gid) {
                                    reverselogoodd = 3;
                                } else {
                                    reverselogoodd = 0;
                                }
                            }
                            var orsimg = getOddsresultImg(reverselogoodd);
                            $(row).find(".guide-oddsresult").first().attr('src', orsimg);
                            gorssum++;
                        } else {
                            $(row).find(".guide-oddsresult").first().addClass('hide');
                        }
                        if (reverselogoodd == 0 || reverselogoodd == 1) {
                            donutgoresult[3][1]++;
                        } else if (reverselogoodd == 2) {
                            donutgoresult[1][1]++;
                        } else if (reverselogoodd == [3] || reverselogoodd == 4) {
                            donutgoresult[2][1]++;
                        }

                        $("#teamaway-h2h-list").append($(row));
                    }
                }
                ground++;
            });

            google.charts.setOnLoadCallback(
                function () {
                    drawDonut('h2hdonut-h-result', donuthresult);
                    drawDonut('h2hdonut-h-oresult', donuthoresult);
                    drawDonut('h2hdonut-g-result', donutgresult);
                    drawDonut('h2hdonut-g-oresult', donutgoresult);
                });
            $(".h2hh-result-winrate").text((donuthresult[3][1] / hrssum * 100).toFixed(2) + '%');
            $(".h2hh-result-drawrate").text((donuthresult[1][1] / hrssum * 100).toFixed(2) + '%');
            $(".h2hh-result-loserate").text((donuthresult[2][1] / hrssum * 100).toFixed(2) + '%');

            $(".h2hh-oresult-winrate").text((donuthoresult[3][1] / horssum * 100).toFixed(2) + '%');
            $(".h2hh-oresult-drawrate").text((donuthoresult[1][1] / horssum * 100).toFixed(2) + '%');
            $(".h2hh-oresult-loserate").text((donuthoresult[2][1] / horssum * 100).toFixed(2) + '%');

            $(".h2hg-result-winrate").text((donutgresult[3][1] / grssum * 100).toFixed(2) + '%');
            $(".h2hg-result-drawrate").text((donutgresult[1][1] / grssum * 100).toFixed(2) + '%');
            $(".h2hg-result-loserate").text((donutgresult[2][1] / grssum * 100).toFixed(2) + '%');

            $(".h2hg-oresult-winrate").text((donutgoresult[3][1] / gorssum * 100).toFixed(2) + '%');
            $(".h2hg-oresult-drawrate").text((donutgoresult[1][1] / gorssum * 100).toFixed(2) + '%');
            $(".h2hg-oresult-loserate").text((donutgoresult[2][1] / gorssum * 100).toFixed(2) + '%');
            console.log(donuthresult);
            console.log(donuthoresult);
            console.log(donutgresult);
            console.log(donutgoresult);

        }


        getTeamGuide(mid);
        function getTeamGuide(mid) {
            $.ajax({
                url: "/services/getteamguidebymid.php",
                data: {mid: mid},
                method: "GET",
                dataType: "JSON"
            }).done(function (response) {
                teamguide = response;
                showGuideline(response, 'all', 10);

                $('#gt-h-max-match').text("(" + response['guide']['home'].length + ")");
                $('#gt-g-max-match').text("(" + response['guide']['away'].length + ")");
                genMaxView('#tg-h-select-limit', response['guide']['home'].length, 'guideline');
                genMaxView('#tg-g-select-limit', response['guide']['away'].length, 'guideline');
            });
        }

        function genMaxView(elm, limit, section) {
            var lnk = jQuery.parseHTML("<li><a class='tg-at-limit' limit='0'></a></li>");
            if (section == 'h2h') {
                lnk = jQuery.parseHTML("<li><a class='h2h-at-limit' limit='0'></a></li>");
            }
            $(elm).empty();
            for (var i = 1; i <= limit; i++) {
                var e = $(lnk).clone();
                if (section == 'h2h') {
                    $(e).find('.h2h-at-limit').first().text(i);
                    $(e).find('.h2h-at-limit').first().attr('limit', i);
                } else {
                    $(e).find('.tg-at-limit').first().text(i);
                    $(e).find('.tg-at-limit').first().attr('limit', i);
                }
                $(elm).append(e);
            }
        }

        function showGuideline(data, side, limit) {
            console.log(side, limit);
            if (guiderow_original == null) {
                guiderow_original = $("#guide-row-original").clone();
            }
            $("#teamhome-guide-list").empty();
            $("#teamaway-guide-list").empty();
            var donuthresult = [['result', 'value'], ['draw', 0], ['lose', 0], ['win', 0]];
            var donuthoresult = [['result', 'value'], ['draw', 0], ['lose', 0], ['win', 0]];
            var donutgresult = [['result', 'value'], ['draw', 0], ['lose', 0], ['win', 0]];
            var donutgoresult = [['result', 'value'], ['draw', 0], ['lose', 0], ['win', 0]];
            var hrssum = 0;
            var horssum = 0;
            var grssum = 0;
            var gorssum = 0;
            var hround = 0;
            var ground = 0;
            var hlimit = limit;
            var glimit = limit;
            var passcon = false;
            var hid = $('.guide-select[tab=team1]').attr('tid');
            var gid = $('.guide-select[tab=team2]').attr('tid');

            if (limit == 0) {
                hlimit = data['guide']['home'].length;
                glimit = data['guide']['away'].length;
            }

            $.each(data['guide']['home'], function (k, v) {

                passcon = false;
                if (hround < hlimit) {

                    if (side == 'away') {
                        if (v[15] == hid) {
                            passcon = true;
                        }
                    } else if (side == 'home') {
                        if (v[14] == hid) {
                            passcon = true;
                        }
                    } else {
                        passcon = true;
                    }

                    if (passcon) {
                        var row = $(guiderow_original).clone();
                        $(row).find(".guide-league").first().text(v[17]);
                        var dt = moment(v[11], "YYYY-MM-DD");
                        $(row).find(".guide-date").first().text(dt.format("DD MMMM YYYY"));
                        $(row).find(".guide-home-name").first().text(v[19]);
                        var himgpath = getLogo(v[14], 0);
                        $(row).find(".guide-home-img").first().attr('src', himgpath);
                        $(row).find(".guide-away-name").first().text(v[21]);
                        var gimgpath = getLogo(v[15], 0);
                        $(row).find(".guide-away-img").first().attr('src', gimgpath);
                        $(row).find(".guide-odds").first().text(v[8]);
                        $(row).find(".guide-score").first().text(v[6] + "-" + v[7]);

                        var rsimg = getResultImg(v[9]);
                        $(row).find(".guide-result").first().attr('src', rsimg);
                        if (v[9] == 0) {
                            donuthresult[3][1]++;
                        } else if (v[9] == 1) {
                            donuthresult[1][1]++;
                        } else {
                            donuthresult[2][1]++;
                        }
                        hrssum++;
                        if (v[10] != -1) {
                            var orsimg = getOddsresultImg(v[10]);
                            $(row).find(".guide-oddsresult").first().attr('src', orsimg);
                            horssum++;
                        } else {
                            $(row).find(".guide-oddsresult").first().addClass('hide');
                        }

                        if (v[10] == 0 || v[10] == 1) {
                            donuthoresult[3][1]++;
                        } else if (v[10] == 2) {
                            donuthoresult[1][1]++;
                        } else if (v[10] == [3] || v[10] == 4) {
                            donuthoresult[2][1]++;
                        }

                        $("#teamhome-guide-list").append($(row));
                    }
                }
                hround++;
            });

            $.each(data['guide']["away"], function (k, v) {
                passcon = false;
                if (ground < glimit) {
                    if (side == 'away') {
                        if (v[15] == gid) {
                            passcon = true;
                        }
                    } else if (side == 'home') {
                        if (v[14] == gid) {
                            passcon = true;
                        }
                    } else {
                        passcon = true;
                    }

                    if (passcon) {


                        var row = $(guiderow_original).clone();
                        $(row).find(".guide-league").first().text(v[17]);
                        var dt = moment(v[11], "YYYY-MM-DD");
                        $(row).find(".guide-date").first().text(dt.format("DD MMMM YYYY"));
                        $(row).find(".guide-home-name").first().text(v[19]);
                        var himgpath = getLogo(v[14], 0);
                        $(row).find(".guide-home-img").first().attr('src', himgpath);
                        $(row).find(".guide-away-name").first().text(v[21]);
                        var gimgpath = getLogo(v[15], 0);
                        $(row).find(".guide-away-img").first().attr('src', gimgpath);
                        $(row).find(".guide-odds").first().text(v[8]);
                        $(row).find(".guide-score").first().text(v[6] + "-" + v[7]);

                        var rsimg = getResultImg(v[9]);
                        $(row).find(".guide-result").first().attr('src', rsimg);
                        if (v[9] == 0) {
                            donutgresult[3][1]++;
                        } else if (v[9] == 1) {
                            donutgresult[1][1]++;
                        } else {
                            donutgresult[2][1]++;
                        }
                        grssum++;

                        if (v[10] != -1) {
                            var orsimg = getOddsresultImg(v[10]);
                            $(row).find(".guide-oddsresult").first().attr('src', orsimg);
                            gorssum++;
                        } else {
                            $(row).find(".guide-oddsresult").first().addClass('hide');
                        }
                        if (v[10] == 0 || v[10] == 1) {
                            donutgoresult[3][1]++;
                        } else if (v[10] == 2) {
                            donutgoresult[1][1]++;
                        } else if (v[10] == [3] || v[10] == 4) {
                            donutgoresult[2][1]++;
                        }

                        $("#teamaway-guide-list").append($(row));
                    }
                }
                ground++;
            });

            google.charts.setOnLoadCallback(
                function () {
                    drawDonut('donut-h-result', donuthresult);
                    drawDonut('donut-h-oresult', donuthoresult);
                    drawDonut('donut-g-result', donutgresult);
                    drawDonut('donut-g-oresult', donutgoresult);
                });
            $(".tgh-result-winrate").text((donuthresult[3][1] / hrssum * 100).toFixed(2) + '%');
            $(".tgh-result-drawrate").text((donuthresult[1][1] / hrssum * 100).toFixed(2) + '%');
            $(".tgh-result-loserate").text((donuthresult[2][1] / hrssum * 100).toFixed(2) + '%');

            $(".tgh-oresult-winrate").text((donuthoresult[3][1] / horssum * 100).toFixed(2) + '%');
            $(".tgh-oresult-drawrate").text((donuthoresult[1][1] / horssum * 100).toFixed(2) + '%');
            $(".tgh-oresult-loserate").text((donuthoresult[2][1] / horssum * 100).toFixed(2) + '%');

            $(".tgg-result-winrate").text((donutgresult[3][1] / grssum * 100).toFixed(2) + '%');
            $(".tgg-result-drawrate").text((donutgresult[1][1] / grssum * 100).toFixed(2) + '%');
            $(".tgg-result-loserate").text((donutgresult[2][1] / grssum * 100).toFixed(2) + '%');

            $(".tgg-oresult-winrate").text((donutgoresult[3][1] / gorssum * 100).toFixed(2) + '%');
            $(".tgg-oresult-drawrate").text((donutgoresult[1][1] / gorssum * 100).toFixed(2) + '%');
            $(".tgg-oresult-loserate").text((donutgoresult[2][1] / gorssum * 100).toFixed(2) + '%');

        }

        function getLogo(tid, size) {
            var dir = "images/teams_clean";
            var sizelist = ["32x32", "64x64", "256x256"];
            var df_name = "team_default";
            var fullpath = dir + "/" + df_name + "_" + sizelist[size] + ".png";
            var genpath = dir + "/" + tid + "_" + sizelist[size] + ".png";
            fullpath = genpath;
            return fullpath;
        }

        function getResultImg(result) {
            var dir = "images/icon-stat/result";
            return dir + "/" + result + ".png";
        }

        function getOddsresultImg(result) {
            var dir = "images/icon-stat/oddsresult";
            return dir + "/" + result + ".png";
        }

        $(".guide-select").on('click', function (e) {
            var tab = $(e.currentTarget).attr('tab');
            $('.guide-select').removeClass('active');
            $(e.currentTarget).addClass('active');
            $('.guide-tabs').addClass('hide');
            $('.guide-tabs[tab=' + tab + ']').removeClass('hide');
        });

        $(".h2h-select").on('click', function (e) {
            var tab = $(e.currentTarget).attr('tab');
            $('.h2h-select').removeClass('active');
            $(e.currentTarget).addClass('active');
            $('.h2h-tabs').addClass('hide');
            $('.h2h-tabs[tab=' + tab + ']').removeClass('hide');
        });

        $('.tg-viewside').on('click', function (e) {
            var side = $(e.currentTarget).attr('side');
            showGuideline(teamguide, side, currentlimit);
            currentside = side;
            $('.tg-currentside-view').text($(e.currentTarget).text());
            //console.log(currentside, currentlimit);
        });
        $('.h2h-viewside').on('click', function (e) {
            var side = $(e.currentTarget).attr('side');
            showH2H(teamh2h, side, currenth2hlimit);
            currenth2hside = side;
            $('.h2h-currentside-view').text($(e.currentTarget).text());
            //console.log(currentside, currentlimit);
        });
        $(document).on('click', '.tg-at-limit', function (e) {
            var limit = $(e.currentTarget).attr('limit');
            showGuideline(teamguide, currentside, limit);
            currentlimit = limit;
            $('#gt-h-max-match').text("(" + limit + ")");
            $('#gt-g-max-match').text("(" + limit + ")");
            //console.log(currentside, currentlimit);
        });

        $(document).on('click', '.h2h-at-limit', function (e) {
            var limit = $(e.currentTarget).attr('limit');
            showH2H(teamh2h, currenth2hside, limit);
            currenth2hlimit = limit;
            $('#h2h-h-max-match').text("(" + limit + ")");
            $('#h2h-g-max-match').text("(" + limit + ")");
            //console.log(currentside, currentlimit);
        });


        getLeagueRanking();
        function getLeagueRanking() {
            var slid = $('#game-data').attr('slid');
            var lid = $('#game-data').attr('lid');
            $.ajax({
                url: "/services/getleagueranking.php?lid=43893&slid=43893",
                data: {slid: slid, lid: lid},
                dataType: "JSON",
                method: "GET"
            }).done(function (response) {
                leagueranking = response;
                showRanklist(leagueranking, 'match');

            });
        }

        function showRanklist(datapack, view) {
            var keylist = datapack['keylist']['rank'];
            var data = datapack['rank'];
            var passcon = false;
            var hid = $('#game-data').attr('hid');
            var gid = $('#game-data').attr('gid');
            if (rank_original == null) {
                rank_original = $('#rank-original-row').clone();
            }
            $('#rank-list').empty();
            $.each(data, function (k, v) {
                var rs = mapKV(keylist, v);
                passcon = false;
                if (view == 'all') {
                    passcon = true;
                } else {
                    if (rs['tid'] == hid || rs['tid'] == gid) {
                        passcon = true;
                    }
                }
                if (passcon) {
                    var row = $(rank_original).clone();
                    $(row).find('.rank-no').first().text(rs['no']);
                    $(row).find('.rank-logo').first().attr('src', getLogo(rs['tid'], 0));
                    $(row).find('.rank-team').first().text(' ' + rs['teamNameTh']);
                    $(row).find('.rank-play').first().text(rs['gp']);
                    $(row).find('.rank-win').first().text(rs['w']);
                    $(row).find('.rank-lose').first().text(rs['l']);
                    $(row).find('.rank-draw').first().text(rs['d']);
                    $(row).find('.rank-gf').first().text(rs['gf']);
                    $(row).find('.rank-ga').first().text(rs['ga']);
                    $(row).find('.rank-gd').first().text(rs['gf'] - rs['ga']);
                    $(row).find('.rank-pts').first().text(rs['pts']);
                    $('#rank-list').append(row);
                }
            })
        }

        $('.rank-select-view').on('click', function (e) {
            var view = $(e.currentTarget).attr('view');
            showRanklist(leagueranking, view);
        });

        refreshTime();
        function refreshTime() {
            console.log("refreshTime");
            $.each($(".how-long"), function (k, v) {
                if ($.isNumeric($(this).text())) {
                    $(this).html(moment($(this).text(), "X").fromNow())
                } else {
                    $(this).html(moment($(this).text(), "YYYY-MM-DD HH:mm:ss").fromNow())
                }
            })

        }

    }
)
;

$(document).on('click', ".selelctDate", function (e) {
    loadGameHistory($(this).attr('type'), $(this).attr('date'), 'AllMath');
});
var selectTeam = 'home';
$(document).on('click', ".btnSelectTeam", function (e) {
    selectTeam = $(this).attr('team');
    var type = $(this).attr('type');
    var dateSelect = $('#spanselelctDate' + type).attr('date');
    $('.btnSelectTeam').removeClass('active');
    $(this).addClass('active');
    loadGameHistory(type, dateSelect, 'AllMath');
})

$(document).on('click', ".btnAllMath,.btnHome,.btnAway", function (e) {
    var type = $(this).attr('type');
    var dateSelect = $('#spanselelctDate' + type).attr('date');
    if ($(this).hasClass("btnAllMath")) {
        loadGameHistory(type, dateSelect, 'AllMath');
    } else if ($(this).hasClass("btnHome")) {
        loadGameHistory(type, dateSelect, 'Home');
    } else if ($(this).hasClass("btnAway")) {
        loadGameHistory(type, dateSelect, 'Away');
    }
})

function loadGameHistory(name, dateSelectAll, type) {
    var trCount = 0;
    var showCount = 0;
    var winCount = 0;
    var drawCount = 0;
    var loseCount = 0;
    var oddWinCount = 0;
    var oddDrawCount = 0;
    var oddLoseCount = 0;
    // console.log("selectTeam:: "+selectTeam);
    $('.tr' + name).show();
    $('.tr' + name).each(function (index) {
        if ((moment($(this).attr('date'), 'YYYY-MM-DD') <= moment(dateSelectAll, 'YYYY-MM-DD')) && (type != "AllMath" && ((selectTeam == 'away' && ((type == "Home" && $(this).attr('type') == "away") || (type == "Away" && $(this).attr('type') == "home"))) || (selectTeam == 'home' && ((type == "Home" && $(this).attr('type') == "home") || (type == "Away" && $(this).attr('type') == "away")))))) {
            $(this).hide();
        } else {
            if (selectTeam == 'home') {
                if ($(this).attr('result') == 0) {
                    winCount++;
                } else if ($(this).attr('result') == 1) {
                    drawCount++;
                } else if ($(this).attr('result') == 2) {
                    loseCount++;
                }
            } else {
                if ($(this).attr('result') == 0) {
                    loseCount++;
                } else if ($(this).attr('result') == 1) {
                    drawCount++;
                } else if ($(this).attr('result') == 2) {
                    winCount++;
                }
            }

            if (selectTeam == 'home') {
                if ($(this).attr('oddsresult') == 0 || $(this).attr('oddsresult') == 1) {
                    oddWinCount++;
                } else if ($(this).attr('oddsresult') == 2) {
                    oddDrawCount++;
                } else if ($(this).attr('oddsresult') == 3 || $(this).attr('oddsresult') == 4) {
                    oddLoseCount++;
                }
            } else {
                if ($(this).attr('oddsresult') == 0 || $(this).attr('oddsresult') == 1) {
                    oddLoseCount++;
                } else if ($(this).attr('oddsresult') == 2) {
                    oddDrawCount++;
                } else if ($(this).attr('oddsresult') == 3 || $(this).attr('oddsresult') == 4) {
                    oddWinCount++;
                }
            }
            showCount++;
        }
        trCount++;
    });
    $('#labelTotal' + name).html("(" + (showCount) + ")");
    $('#percentWin' + name).html(Math.round(100 - ((((showCount - winCount) / showCount) * 100))) + "%");
    $('#percentLoss' + name).html(Math.round(100 - ((((showCount - drawCount) / showCount) * 100))) + "%");
    $('#percentDraw' + name).html(Math.round(100 - ((((showCount - loseCount) / showCount) * 100))) + "%");
    loadGraphMathHeadToHead(name);
    $('#percentOddsWin' + name).html(Math.round(100 - ((((showCount - oddWinCount) / showCount) * 100))) + "%");
    $('#percentOddsLoss' + name).html(Math.round(100 - ((((showCount - oddDrawCount) / showCount) * 100))) + "%");
    $('#percentOddsDraw' + name).html(Math.round(100 - ((((showCount - oddLoseCount) / showCount) * 100))) + "%");
    LoadGraphOddsHeadToHead(name);
    $('#spanselelctDate' + name).attr('date', dateSelectAll);
    $('#spanselelctDate' + name).html(moment(dateSelectAll, 'YYYY-MM-DD').format('DD MMMM YYYY'));
}


function getEvent(mid) {
    $(".team1").empty();
    $(".team2").empty();
    $.ajax({
        url: "/services/getliveeventbymatch.php",
        data: {mid: mid},
        method: "GET",
        dataType: "JSON"
    }).done(function (response) {
        console.log(response);
        var ori = $("#event-circle").children().first().clone();

        $.each(response.event, function (k, v) {
            var circle = $(ori).clone();

        });

    });
}

function getQueryString(field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
}

function LoadGraphOddsHeadToHead(name) {
    google.charts.load("current", {packages: ["corechart"]});
    google.charts.setOnLoadCallback(
        function () {
            drawChartGraphOddsHeadToHead(name)
        });
}

function drawChartGraphOddsHeadToHead(name) {
    // console.log('#percentOddsDraw' + name);
    var Draw = parseInt($('#percentOddsDraw' + name).text());
    var Lose = parseInt($('#percentOddsLoss' + name).text());
    var Win = parseInt($('#percentOddsWin' + name).text());
    var data = google.visualization.arrayToDataTable([
        ['Type', 'Percent'],
        ['Draw', Draw],
        ['Lose', Lose],
        ['Win', Win],
    ]);
    var options = {
        pieStartAngle: 0,
        chartArea: {top: 7, height: '90%', width: '100%'},
        height: '100%',
        colors: ['#888', '#e40520', '#0091c8'],
        pieHole: 0.5,
        legend: 'none',
    };

    var chart = new google.visualization.PieChart(document.getElementById('graphOddsHeadToHead'));
    chart.draw(data, options);
}

function loadGraphMathHeadToHead(name) {
    //google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(
        function () {
            drawChartGraphMathHeadToHead(name)
        });
}

function drawChartGraphMathHeadToHead(name) {
    var Draw = parseInt($('#percentDraw' + name).text());
    var Lose = parseInt($('#percentLoss' + name).text());
    var Win = parseInt($('#percentWin' + name).text());
    var data = google.visualization.arrayToDataTable([
        ['Type', 'Percent'],
        ['Draw', Draw],
        ['Lose', Lose],
        ['Win', Win],
    ]);
    var options = {
        pieStartAngle: 0,
        chartArea: {top: 7, height: '90%', width: '100%'},
        height: '100%',
        colors: ['#888', '#e40520', '#0091c8'],
        pieHole: 0.5,
        legend: 'none',
    };

    var chart = new google.visualization.PieChart(document.getElementById('graphMathHeadToHead'));
    chart.draw(data, options);
}

/**
 * Created by mrsyrop on 30/11/2559.
 */
$(document).ready(function () {

    var matchlist_tmp = null;
    var dateselect = "";
    if (matchlist_tmp == null) {
        var day = $.datepicker.formatDate("yy-mm-dd", new Date());
        getDayMath(day, true);
    }

    initDateSlider();
    function initDateSlider() {
        var dataNow = moment();
        var ul = $(".date-slider").first().children().first().clone();
        $(".date-slider").empty();
        var d = new Date();
        d.setDate(d.getDate() - 2);
        $.each($(ul).children('.match-date'), function (k, v) {
            //console.log(k);
            d.setDate(d.getDate() + 1);
            $(v).attr('date', moment($.datepicker.formatDate("yy-mm-dd", d), 'YYYY-MM-DD').format("YYYY-MM-DD"));
            if (moment($.datepicker.formatDate("yy-mm-dd", d), 'YYYY-MM-DD').format("YYYY-MM-DD") == dataNow.format("YYYY-MM-DD")) {
                $(v).text("Today");
            } else {
                $(v).text(moment($.datepicker.formatDate("yy-mm-dd", d), 'YYYY-MM-DD').format("DD MMMM"));
            }
            if (k == 1) {
                dateselect = $.datepicker.formatDate("yy-mm-dd", d);
                $(v).addClass("active");
            }
        });
        $(".date-slider").append($(ul));
    }

    $(document).on('click', ".li-back,.li-next", function (e) {
        var dataNow = moment();
        var ul = $(".date-slider").first().children().first().clone();
        if ($(this).hasClass('li-back')) {
            var dateMoment = moment(dateselect, 'YYYY-MM-DD').add(-1, 'days');
            dateMoment.add(-2, 'days');
            $.each($(ul).children('.match-date'), function (k, v) {
                //console.log(k);
                dateMoment.add(1, 'days');
                $(v).attr('date', dateMoment.format("YYYY-MM-DD"));
                if (dateMoment.format("YYYY-MM-DD") == dataNow.format("YYYY-MM-DD")) {
                    $(v).text("Today");
                } else {
                    $(v).text(dateMoment.format("DD MMMM"));
                }
                if (k == 1) {
                    dateselect = dateMoment.format("YYYY-MM-DD");
                    getDayMath(dateselect, false);
                    $(v).addClass("active");
                }
            });
            $(".date-slider").html($(ul));
        } else if ($(this).hasClass('li-next')) {
            var dataNow = moment();
            var dateMoment = moment(dateselect, 'YYYY-MM-DD').add(1, 'days');
            dateMoment.add(-2, 'days');
            $.each($(ul).children('.match-date'), function (k, v) {
                //console.log(k);
                dateMoment.add(1, 'days');
                $(v).attr('date', dateMoment.format("YYYY-MM-DD"));
                if (dateMoment.format("YYYY-MM-DD") == dataNow.format("YYYY-MM-DD")) {
                    $(v).text("Today");
                } else {
                    $(v).text(dateMoment.format("DD MMMM"));
                }
                // $(v).text(dateMoment.format("DD MMMM"));
                if (k == 1) {
                    dateselect = dateMoment.format("YYYY-MM-DD");
                    getDayMath(dateselect, false);
                    $(v).addClass("active");
                }
            });
            $(".date-slider").html($(ul));
        }
    });

    $(document).on('click', ".match-date", function (e) {
        // var day = $(e.currentTarget).attr('date');
        // $(".match-date").removeClass('active');
        // $(".match-date[date=" + day + "]").addClass('active');
        // getDayMath(day, false);
        var dataNow = moment();
        var ul = $(".date-slider").first().children().first().clone();
        var dateMoment = moment($(e.currentTarget).attr('date'), 'YYYY-MM-DD');
        dateMoment.add(-2, 'days');
        $.each($(ul).children('.match-date'), function (k, v) {
            //console.log(k);
            dateMoment.add(1, 'days');
            $(v).attr('date', dateMoment.format("YYYY-MM-DD"));
            if (dateMoment.format("YYYY-MM-DD") == dataNow.format("YYYY-MM-DD")) {
                $(v).text("Today");
            } else {
                $(v).text(dateMoment.format("DD MMMM"));
            }
            // $(v).text(dateMoment.format("DD MMMM"));
            if (k == 1) {
                dateselect = dateMoment.format("YYYY-MM-DD");
                getDayMath(dateselect, false);
                $(v).addClass("active");
            }
        });
        $(".date-slider").html($(ul));
    });

    function getDayMath(day, getonly) {
        dateselect = day;
        $('#div-refresh-loading').removeClass('hide');
        $('.date-slider').css('pointer-events', 'none');
        $.ajax({
            url: "/services/getMatchByDate.php",
            data: {date: day},
            dataType: "JSON",
            method: "GET"
        }).done(function (response) {
            //console.log(response);
            matchlist_tmp = response;
            if (!getonly) {
                generateMatchTable(response, true, []);
            }
            $('#div-refresh-loading').addClass('hide');
            $('.date-slider').removeAttr('style');
        });
    }

    function generateMatchTable(response, showall, leagelist) {
        // console.log("generateMatchTable");
        // console.log(response, showall, leagelist);
        if ($('#div-refresh-loading').hasClass('hide')) {
            $('#div-refresh-loading').removeClass('hide');
            $('.date-slider').css('pointer-events', 'none');
        }
        var leaguebox = $(".league-box").first().clone();
        var allmatchcount = 0;
        var livematchcount = 0;
        var betmatchcount = 0;
        var resultmatchcount = 0
        var betalbegame = {};
        var h2h = "/statistics.php?mid=";
        var gameplay = "/game.php?mid=";

        $(".all-match-count").text(response.gamecount);
        $(".live-match-count").text(response.livecount);
        $(".bet-match-count").text(response.betcount);
        $(".result-match-count").text(response.resultcount);

        $(".allgame-box").empty();
        $.each(response.allmatch, function (key, val) {
            if (showall || leagelist.indexOf(key) != -1) {
                var box = $(leaguebox).clone();
                if (box.hasClass('hide')) {
                    box.removeClass('hide');
                }
                var matchbox = $(box).find(".live-matche").first().clone();
                $(box).find(".table-fixtures").first().empty();
                $.each(val, function (k, v) {
                    var tr = $(matchbox).clone();
                    $(box).find(".league-name").first().text(response.all[key][v][15]);
                    $(box).find(".league-name").first().siblings().first().attr("src", "images/countries/" + response.all[key][v][22] + ".png");
                    $(tr).attr('mid', v);
                    $(tr).find(".home-name").first().text(response.all[key][v][16]);
                    $(tr).find(".guest-name").first().text(response.all[key][v][18]);
                    $(tr).find(".home-logo").first().attr('src', response.all[key][v][20]);
                    $(tr).find(".guest-logo").first().attr('src', response.all[key][v][21]);
                    $(tr).find(".h2h").first().attr('href', h2h + response.all[key][v][0]);
                    $(tr).find(".game-play").first().attr('href', gameplay + v);
                    $(tr).find(".txt-blue").last().html("<a href='/statistics.php?mid=" + v + "'>" + response.all[key][v][11] + "</a>");

                    if (response.all[key][v][3] >= 5) {
                        $(tr).find(".showdate").first().text('FT');
                    } else {
                        $(tr).find(".showdate").first().text(moment(response.all[key][v][13]).startOf('hour').add(7, 'hours').format("HH:mm"));
                    }

                    $(tr).find(".home-name").first().removeClass('odds');
                    $(tr).find(".guest-name").first().removeClass('odds');

                    if (typeof(response.odds[v]) !== "undefined") {
                        if (response.odds[v][2] == "") {
                            $(tr).find(".bx-label-hdp").first().text("-");
                        } else if (parseFloat(response.odds[v][2]) < 0) {
                            $(tr).find(".home-name").first().addClass('odds');
                            $(tr).find(".bx-label-hdp").first().text(response.odds[v][2]);
                            $(tr).find(".bx-label-hdp").first().attr('href', gameplay + v);
                        } else if (parseFloat(response.odds[v][2]) > 0) {
                            $(tr).find(".guest-name").first().addClass('odds');
                            $(tr).find(".bx-label-hdp").first().text(response.odds[v][2]);
                            $(tr).find(".bx-label-hdp").first().attr('href', gameplay + v);
                        }
                    } else {
                        $(tr).find(".bx-label-hdp").first().text("-");
                    }
                    $(box).find(".table-fixtures").first().append($(tr));
                    allmatchcount++;
                })
                $(".allgame-box").append($(box));
            }
        });
        $(".live-box").empty();
        $.each(response.live, function (key, val) {
            if (showall || leagelist.indexOf(key) != -1) {
                var box = $(leaguebox).clone();
                if (box.hasClass('hide')) {
                    box.removeClass('hide');
                }
                var matchbox = $(box).find(".live-matche").first().clone();
                $(box).find(".table-fixtures").first().empty();
                $.each(val, function (k, v) {
                    var tr = $(matchbox).clone();
                    $(box).find(".league-name").first().text(response.all[key][v][15]);
                    $(box).find(".league-name").first().siblings().first().attr("src", "images/countries/" + response.all[key][v][22] + ".png");
                    $(tr).attr('mid', response.all[key][v][0]);
                    $(tr).find(".home-name").first().text(response.all[key][v][16]);
                    $(tr).find(".guest-name").first().text(response.all[key][v][18]);
                    $(tr).find(".home-logo").first().attr('src', response.all[key][v][20]);
                    $(tr).find(".guest-logo").first().attr('src', response.all[key][v][21]);
                    $(tr).find(".h2h").first().attr('href', h2h + response.all[key][v][0]);
                    $(tr).find(".game-play").first().attr('href', gameplay + response.all[key][v][0]);
                    $(tr).find(".txt-blue").last().html("<a href='/statistics.php?mid=" + response.all[key][v][0] + "'>" + response.all[key][v][11] + "</a>");

                    if (response.all[key][v][3] >= 5) {
                        $(tr).find(".showdate").first().text('FT');
                    } else {
                        $(tr).find(".showdate").first().text(moment(response.all[key][v][13]).startOf('hour').add(7, 'hours').format("HH:mm"));
                    }

                    $(tr).find(".home-name").first().removeClass('odds');
                    $(tr).find(".guest-name").first().removeClass('odds');

                    if (typeof(response.odds[v]) !== "undefined") {
                        if (response.odds[v][2] == "") {
                            $(tr).find(".bx-label-hdp").first().text("-");
                        } else if (parseFloat(response.odds[v][2]) < 0) {
                            $(tr).find(".home-name").first().addClass('odds');
                            $(tr).find(".bx-label-hdp").first().text(response.odds[v][2]);
                            $(tr).find(".bx-label-hdp").first().attr('href', gameplay + v);
                        } else if (parseFloat(response.odds[v][2]) > 0) {
                            $(tr).find(".guest-name").first().addClass('odds');
                            $(tr).find(".bx-label-hdp").first().text(response.odds[v][2]);
                            $(tr).find(".bx-label-hdp").first().attr('href', gameplay + v);
                        }
                    } else {
                        $(tr).find(".bx-label-hdp").first().text("-");
                    }

                    $(box).find(".table-fixtures").first().append($(tr));
                    livematchcount++;
                })
                $(".live-box").append($(box));
            }
        });
        $(".bet-box").empty();
        $.each(response.bet, function (key, val) {
            if (showall || leagelist.indexOf(key) != -1) {
                var box = $(leaguebox).clone();
                if (box.hasClass('hide')) {
                    box.removeClass('hide');
                }
                var matchbox = $(box).find(".live-matche").first().clone();
                $(box).find(".table-fixtures").first().empty();
                $.each(val, function (k, v) {
                    var tr = $(matchbox).clone();
                    $(box).find(".league-name").first().text(response.all[key][v][15]);
                    $(box).find(".league-name").first().siblings().first().attr("src", "images/countries/" + response.all[key][v][22] + ".png");
                    $(tr).attr('mid', response.all[key][v][0]);
                    $(tr).find(".home-name").first().text(response.all[key][v][16]);
                    $(tr).find(".guest-name").first().text(response.all[key][v][18]);
                    $(tr).find(".home-logo").first().attr('src', response.all[key][v][20]);
                    $(tr).find(".guest-logo").first().attr('src', response.all[key][v][21]);
                    $(tr).find(".h2h").first().attr('href', h2h + response.all[key][v][0]);
                    $(tr).find(".game-play").first().attr('href', gameplay + response.all[key][v][0]);

                    $(tr).find(".txt-blue").last().html("<a href='/statistics.php?mid=" + response.all[key][v][0] + "'>" + response.all[key][v][11] + "</a>");

                    if (response.all[key][v][3] >= 5) {
                        $(tr).find(".showdate").first().text('FT');
                    } else {
                        $(tr).find(".showdate").first().text(moment(response.all[key][v][13]).startOf('hour').add(7, 'hours').format("HH:mm"));
                    }

                    $(tr).find(".home-name").first().removeClass('odds');
                    $(tr).find(".guest-name").first().removeClass('odds');

                    if (typeof(response.odds[v]) !== "undefined") {
                        if (response.odds[v][2] == "") {
                            $(tr).find(".bx-label-hdp").first().text("-");
                        } else if (parseFloat(response.odds[v][2]) < 0) {
                            $(tr).find(".home-name").first().addClass('odds');
                            $(tr).find(".bx-label-hdp").first().text(response.odds[v][2]);
                            $(tr).find(".bx-label-hdp").first().attr('href', gameplay + response.all[key][v][0]);
                            $(tr).find(".bx-label-hdp").first().attr('href', gameplay + v);
                        } else if (parseFloat(response.odds[v][2]) > 0) {
                            $(tr).find(".guest-name").first().addClass('odds');
                            $(tr).find(".bx-label-hdp").first().text(response.odds[v][2]);
                            $(tr).find(".bx-label-hdp").first().attr('href', gameplay + response.all[key][v][0]);
                            $(tr).find(".bx-label-hdp").first().attr('href', gameplay + v);
                        }
                    } else {
                        $(tr).find(".bx-label-hdp").first().text("-");
                    }
                    $(box).find(".table-fixtures").first().append($(tr));
                    allmatchcount++;
                })
                $(".bet-box").append($(box));
            }
        });
        $(".result-box").empty();
        $.each(response.result, function (key, val) {
            if (showall || leagelist.indexOf(key) != -1) {
                var box = $(leaguebox).clone();
                if (box.hasClass('hide')) {
                    box.removeClass('hide');
                }
                var matchbox = $(box).find(".live-matche").first().clone();
                $(box).find(".table-fixtures").first().empty();
                $.each(val, function (k, v) {
                    var tr = $(matchbox).clone();
                    $(box).find(".league-name").first().text(response.all[key][v][15]);
                    $(box).find(".league-name").first().siblings().first().attr("src", "images/countries/" + response.all[key][v][22] + ".png");
                    $(tr).attr('mid', response.all[key][v][0]);
                    $(tr).find(".home-name").first().text(response.all[key][v][16]);
                    $(tr).find(".guest-name").first().text(response.all[key][v][18]);
                    $(tr).find(".home-logo").first().attr('src', response.all[key][v][20]);
                    $(tr).find(".guest-logo").first().attr('src', response.all[key][v][21]);
                    $(tr).find(".h2h").first().attr('href', h2h + response.all[key][v][0]);
                    $(tr).find(".game-play").first().attr('href', gameplay + response.all[key][v][0]);
                    $(tr).find(".txt-blue").last().html("<a href='/statistics.php?mid=" + response.all[key][v][0] + "'>" + response.all[key][v][11] + "</a>");

                    if (response.all[key][v][3] >= 5) {
                        $(tr).find(".showdate").first().text('FT');
                    } else {
                        $(tr).find(".showdate").first().text(moment(response.all[key][v][13]).startOf('hour').add(7, 'hours').format("HH:mm"));
                    }

                    $(tr).find(".home-name").first().removeClass('odds');
                    $(tr).find(".guest-name").first().removeClass('odds');

                    if (typeof(response.odds[response.all[key][v][0]]) !== "undefined") {
                        if (response.odds[response.all[key][v][0]][2] == "") {
                            $(tr).find(".bx-label-hdp").first().text("-");
                        } else if (parseFloat(response.odds[response.all[key][v][0]][2]) < 0) {
                            $(tr).find(".home-name").first().addClass('odds');
                            $(tr).find(".bx-label-hdp").first().text(response.odds[response.all[key][v][0]][2]);
                            $(tr).find(".bx-label-hdp").first().attr('href', gameplay + response.all[key][v][0]);
                        } else if (parseFloat(response.odds[response.all[key][v][0]][2]) > 0) {
                            $(tr).find(".guest-name").first().addClass('odds');
                            $(tr).find(".bx-label-hdp").first().text(response.odds[response.all[key][v][0]][2]);
                            $(tr).find(".bx-label-hdp").first().attr('href', gameplay + response.all[key][v][0]);
                        }
                    } else {
                        $(tr).find(".bx-label-hdp").first().text("-");
                    }
                    $(box).find(".table-fixtures").first().append($(tr));
                    resultmatchcount++;
                })
                $(".result-box").append($(box));
            }
        });


        if (showall) {
            var sl = $(".league-select").first().children().first();
            $(".league-select").empty();
            $(".league-select").append($(sl));
            $.each(response['leaguelist'], function (k, v) {
                var op = $(sl).clone();
                $(op).val(k);
                $(op).text(v);
                $(".league-select").append($(op));
            });
            $(".league-select").children().first().prop("selected", true);
        }

        $('#div-refresh-loading').addClass('hide');
        $('.date-slider').removeAttr('style');
    }

    $(".league-select").on("change", function (e) {
        var lid = $(e.currentTarget).val();
        generateMatchTable(matchlist_tmp, false, [lid]);
    });

    $(".label-reset").on("click", function () {
        generateMatchTable(matchlist_tmp, true, []);
    });

    //setInterval(getMatchUpdate, 5000);
    getMatchUpdate();
    function getMatchUpdate() {
        $.ajax({
            url: "/services/getmatchupdate.php",
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
            var timeboxs = $(document).find('.nowshowing');
            $.each(timeboxs, function (k, v) {
                console.log($(v).attr('mid'));
            });
        });
    }
});
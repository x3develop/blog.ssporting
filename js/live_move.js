$(document).ready(function () {
    var waitlist = {};
    var livelist = {};
    setInterval(movetolivebox, 1000 * 10);

    function movetolivebox() {
        var atmark = $(document).find(".at-minute-mark");
        $.each(atmark, function (k, v) {
            var state = $(v).attr("state");
            var mid = $(v).attr("mid");
            var lid = $(v).attr("lid");
            var atmin = $(v).text();

            if (state == 1 || state == 2 || state == 3) {
                if (mid in livelist) {

                } else {
                    if ($("#liveleague" + lid).length > 0) {
                        $("#liveleague" + lid).removeClass("hide");
                    } else {
                        cloneleague(lid);
                    }
                    $(".color-tape[mid=" + mid + "]").removeClass("livescore-full-time");
                    $(".color-tape[mid=" + mid + "]").addClass("livescore-live");
                    $(".color-tape[mid=" + mid + "]").detach().appendTo(".league-matches-live[lid=" + lid + "]");

                    livelist[mid] = lid;
                }
            }
            
            if (atmin == 'FT') {
                console.log("state:", state);
                if ($(".color-tape[mid=" + mid + "]").hasClass("livescore-live")) {
                    console.log("has class livescore-live");
                    $(".color-tape[mid=" + mid + "]").addClass("livescore-full-time");
                    $(".color-tape[mid=" + mid + "]").removeClass("livescore-live");
                    $(".color-tape[mid=" + mid + "]").detach().appendTo(".league-matches[lid=" + lid + "]");
                }
            }
        });

        $(".league-box").each(function (k, v) {
            var lid = $(v).attr("lid");
            var matchesrow = $(v).find(".matches-row").first();
            if ($(matchesrow).html().trim().length==0) {
                $(v).addClass("hide");
            }else{
                $(v).removeClass("hide");
            }
        });

        console.log(livelist);

    }


    // cloneleague(20);

    function cloneleague(lid) {
        var leaguedom = $(".league-box[lid=" + lid + "]").clone();
        $(leaguedom).find("#accordion" + lid).first().empty();
        $(leaguedom).find("#accordion" + lid).first().addClass("league-matches-live");
        $(leaguedom).find("#accordion" + lid).first().removeClass("league-matches");
        $(leaguedom).attr("id", "liveleague" + lid);
        $(leaguedom).addClass("container-live_all");
        $("#live-box").append($(leaguedom));
    }
});
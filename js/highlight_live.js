$(document).ready(function () {
    moment.tz.add("Asia/Bangkok|LMT BMT +07|-6G.4 -6G.4 -70|012|-3D8SG.4 1C000|15e6");
    moment.tz.add("Asia/Singapore|LMT SMT +07 +0720 +0730 +09 +08|-6T.p -6T.p -70 -7k -7u -90 -80|01234546|-2M0ST.p aIM0 17anT.p l5XE 17bO 8Fyu 1so1u|56e5");
    var uncollapsed = [];
    var livelist = [];
    var endlist = [];
    var expandedmid = 0;

    $(document).find(".panel-collapse-side").first().collapse("show");

    initdate();
    // getHLUpdate();
    timestate();

    function initdate() {
        moment.locale();
        var mn = moment();
        mn.tz("Asia/Bangkok");
        // console.log(mn.locale('th').format('LLL'));
        if (mn.hour() < 12) {
            mn.subtract(1, "day");
        }
        $("#hl-datemark").text(mn.locale("th").format("dddd Do MMMM YYYY"));
    }


    // setInterval(timestate, 60000);
    function timestate() {
        moment.locale();
        var mn = moment();
        mn.tz("Asia/Bangkok");

        // console.log("TimeState");
        // console.log(mn);
        $(".hdp-float").addClass("hide");
        $.each($(".at-minute-mark"), function (k, v) {
            // console.log(k, $(v).attr("at"));
            var stateTime = moment.tz($(v).attr("at"), "Asia/Singapore");
            console.log($(v).attr("mid"), mn.diff(stateTime, "minute"));
            var state = $(v).attr("state");
            var mid = $(v).attr("mid");

            // $(".color-tape[mid=" + mid + "]").removeClass("livescore-panel");
            // $(".color-tape[mid=" + mid + "]").removeClass("livescore-full-time");
            $(".color-tape[mid=" + mid + "]").addClass("livescore-live");

            if (state == 17 || state == 0) {
                $(v).removeClass("label-live");
                $(v).removeClass("label-ft");
                $(v).addClass("ct-time");
                $(".wait-float[mid=" + $(v).attr("mid") + "]").removeClass("hide");
                $(".event-dp-button[mid=" + mid + "]").text("WAIT");
                $(".event-dp-button[mid=" + mid + "]").removeClass("real-time-match");
                $(".event-dp-button[mid=" + mid + "]").addClass("end-match");
                // $(".color-tape[mid=" + mid + "]").addClass("livescore-panel");
                $(".color-tape[mid=" + mid + "]").addClass("livescore-full-time");
                $(".color-tape[mid=" + mid + "]").removeClass("livescore-live");
            } else if (state == 4) {
                $(v).removeClass("label-live");
                $(v).addClass("label-ft");
                $(v).removeClass("ct-time");
                $(v).text("FT");
                $(".FT-float[mid=" + $(v).attr("mid") + "]").removeClass("hide");
                $(".event-dp-button[mid=" + mid + "]").text("END");
                $(".event-dp-button[mid=" + mid + "]").removeClass("real-time-match");
                $(".event-dp-button[mid=" + mid + "]").addClass("end-match");
            } else if (state == 1 || state == 2 || state == 3 || state == 8) {
                $(v).addClass("label-live");
                $(v).removeClass("label-ft");
                $(v).removeClass("ct-time");
                $(v).text("LIVE");
                $(".live-float[mid=" + $(v).attr("mid") + "]").removeClass("hide");
                $(".event-dp-button[mid=" + mid + "]").text("LIVE");
                $(".event-dp-button[mid=" + mid + "]").addClass("real-time-match");
                $(".event-dp-button[mid=" + mid + "]").removeClass("end-match");
            }

            if (state == 2) {
                $(".live-minute[mid=" + $(v).attr("mid") + "]").text("HT");
            }

            if (state == 8) {
                $(".live-minute[mid=" + $(v).attr("mid") + "]").text("AET");
            }

            if (state == 3) {
                var htstart = moment.tz($(v).attr("ht"), "Asia/Bangkok");
                var aminute = 45 + mn.diff(htstart, "minute");
                $(".live-minute[mid=" + $(v).attr("mid") + "]").text(aminute + "'");
                if (aminute > 90) {
                    aminute = 90;
                    $(".live-minute[mid=" + $(v).attr("mid") + "]").text("90+");
                }
                var percent = Math.round(aminute / 90 * 100);
                $(".minute-progress[mid=" + $(v).attr("mid") + "]").attr("data-percentage", percent - percent % 10);
            }

            if (state == 1) {
                var aminute = mn.diff(stateTime, "minute");
                $(".live-minute[mid=" + $(v).attr("mid") + "]").text(aminute + "'");
                if (aminute > 45) {
                    aminute = 45;
                    $(".live-minute[mid=" + $(v).attr("mid") + "]").text("45+");
                }
                var percent = Math.round(aminute / 90 * 100);
                console.log(percent - percent % 10);
                $(".minute-progress[mid=" + $(v).attr("mid") + "]").attr("data-percentage", percent - percent % 10);
            }
        });
    }

    setInterval(getHLUpdate, 60000);

    function getHLUpdate() {
        var midlist = "0";
        $.each($(".at-minute-mark"), function (k, v) {
            var state = $(v).attr("state");
            var mid = $(v).attr("mid");
            if (state != 4) {
                if (midlist == "0") {
                    midlist = mid;
                } else {
                    midlist += "," + mid;
                }
                if (state > 0 && state < 4) {
                    livelist.push(mid);
                }
            } else {
                endlist.push(mid);
            }
        });

        if (midlist != "0") {
            $.ajax({
                url: "/services/highlight/gethlupdate",
                data: {
                    midlist: midlist
                },
                dataType: "JSON",
                method: "GET"
            }).done(function (response) {
                // console.log(response);
                $.each(response, function (k, v) {
                    console.log(v);
                    $(".at-minute-mark[mid=" + v.id + "]").attr("state", v.state);
                    $(".at-minute-mark[mid=" + v.id + "]").attr("ht", v.ht_start);
                    $(".live-h-score[mid=" + v.id + "]").text(v.live_score_home);
                    $(".live-a-score[mid=" + v.id + "]").text(v.live_score_away);
                    // console.log($(".at-minute-mark[mid=" + v.id + "]")[0].outerHTML);
                    if (v.hdp != null) {
                        $(".live-h-hdp[mid=" + v.id + "]").text(numeral(v.hdp.home_water_bill).format("0.00"));
                        $(".live-hdp[mid=" + v.id + "]").text(v.hdp.handicap);
                        $(".live-a-hdp[mid=" + v.id + "]").text(numeral(v.hdp.away_water_bill).format("0.00"));
                    }
                });
                timestate();
            });
        }
    }

    $(document).on("click", ".review-dropdown", function (e) {
        var mid = $(e.currentTarget).attr("mid");
        $(".panel-collapse ").collapse("hide");
        // $("#collapse_endmatch" + mid).collapse("show");
        if (mid != expandedmid) {
            expandedmid = mid;
            getLiveEvent();
            getStatUpdate();

        }
    });

    setInterval(getLiveEvent, 60000 * 2);

    function getLiveEvent() {
        var midlist = expandedmid;
        var icon = {
            goal: "goal-score",
            penalty: "penalty-score",
            "own goal": "own-goal",
            "yellow card": "yellow-card",
            "red card": "red-card",
            "yellow to red": "group-yellow-card",
            substutitions: "substitute-player",
            "penalty miss": "penalty-none-score"
        };
        var homeboxori = '<div class="full-width line-content-all">';
        homeboxori += '    <div class="col-home-live">';
        homeboxori += '    <div class="box-football-icon">';
        homeboxori += '                <div class="icon-mark"></div>';
        homeboxori += "    </div>";
        homeboxori += '            <div class="min-live"></div>';
        homeboxori += '            <div class="score-live"></div>';
        homeboxori += '            <div class="name-live name-normal hide"> </div>';
        homeboxori += '            <div class="name-live name-sub-out hide">';
        homeboxori += '                <span class="substitute-player-out"></span>';
        homeboxori += '                <p class="out-mark"></p>';
        homeboxori += "            </div>";
        homeboxori += '            <div class="name-live name-sub-in hide">';
        homeboxori += '                <span class="substitute-player-in"></span>';
        homeboxori += '                <p class="in-mark"></p>';
        homeboxori += "            </div>";
        homeboxori += "        </div>";
        homeboxori += "    </div>";

        var awayboxori = '<div class="full-width line-content-all">';
        awayboxori += '        <div class="col-away-live pull-right">';
        awayboxori += '            <div class="name-live name-normal hide"></div>';
        awayboxori += '            <div class="name-live name-sub-in hide">';
        awayboxori += '                <span class="substitute-player-in"></span>';
        awayboxori += '                <p class="in-mark"></p>';
        awayboxori += "            </div>";
        awayboxori += '            <div class="name-live name-sub-out hide">';
        awayboxori += '                <span class="substitute-player-out"></span>';
        awayboxori += '                <p class="out-mark"></p>';
        awayboxori += "            </div>";
        awayboxori += '            <div class="score-live"></div>';
        awayboxori += '            <div class="min-live"></div>';
        awayboxori += '            <div class="box-football-icon">';
        awayboxori += '                <div class="icon-mark"></div>';
        awayboxori += "            </div>";
        awayboxori += "        </div>";
        awayboxori += "    </div>";

        var homeboxdom = $.parseHTML(homeboxori);
        var awayboxdom = $.parseHTML(awayboxori);


        if (midlist != "0") {
            $.ajax({
                url: "/services/matchevents/geteventsinlist",
                data: {
                    midlist: midlist
                },
                dataType: "JSON",
                method: "GET"
            }).done(function (response) {
                console.log(response);
                // $.each(response, function (k, v) {

                // });

                $.each(response, function (midk, matcevent) {
                    $(".content-event-match[mid=" + midk + "]").empty();
                    $.each(matcevent, function (k, v) {
                        var eventbox = $(homeboxdom).clone();
                        if (v.side == "away") {
                            eventbox = $(awayboxdom).clone();
                        }
                        $(eventbox).find(".icon-mark").first().addClass(icon[v.event]);
                        $(eventbox).find(".min-live").first().text(v.at_minute + "'");
                        $(eventbox).find(".score-live").first().text(v.score);
                        if (v.event == "substutitions") {
                            $(eventbox).find(".name-sub-in").first().removeClass("hide");
                            $(eventbox).find(".name-sub-out").first().removeClass("hide");
                            $(eventbox).find(".out-mark").first().text(v.player);
                            $(eventbox).find(".in-mark").first().text(v.assist);
                        } else {
                            var etxt = v.player;
                            if (v.assist != "") {
                                etxt = v.player + "(" + v.assist + ")";
                            }
                            if (v.side == "away") {
                                etxt = v.player;
                                if (v.assist != "") {
                                    etxt = "(" + v.assist + ")" + v.player;
                                }
                            }

                            $(eventbox).find(".name-normal").first().text(etxt);
                            $(eventbox).find(".name-normal").removeClass("hide");
                        }
                        $(".content-event-match[mid=" + midk + "]").append($(eventbox));
                    });
                });
            });
        }
    }

    setInterval(getStatUpdate, 60000 * 2);

    function getStatUpdate() {
        var midlist = expandedmid;
        var rowdom = "<tr>";
        rowdom += "<td>";
        rowdom += '    <div class="num-progress num-progress-home">0</div>';
        rowdom += '    <div class="progress">';
        rowdom += '        <div class="progress-bar progress-bar-danger pull-right progress-bar-home" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="1" style="width: 0%;"></div>';
        rowdom += "    </div>";
        rowdom += "</td>";
        rowdom += '<td class="event-type">shots</td>';
        rowdom += "<td>";
        rowdom += '    <div class="progress">';
        rowdom += '        <div class="progress-bar progress-bar-away" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="1" style="width: 100%;"></div>';
        rowdom += "    </div>";
        rowdom += '    <div class="num-progress num-progress-away">1</div>';
        rowdom += "</td>";
        rowdom += "</tr>";

        if (midlist != "0") {
            $.ajax({
                url: "/services/matchevents/getstatinlist",
                data: {
                    midlist: midlist
                },
                dataType: "JSON",
                method: "GET"
            }).done(function (response) {
                // console.log(response);

                $.each(response, function (mid, val) {
                    // console.log(mid, val);
                    $(".statistics-mark[mid=" + mid + "]").empty();
                    $.each(val, function (k, v) {
                        var row = $.parseHTML(rowdom);
                        var max = v.hval + v.aval;
                        $(row).find(".event-type").first().text(v.type);
                        $(row).find(".num-progress-home").first().text(v.hval);
                        $(row).find(".num-progress-away").first().text(v.aval);
                        if (v.type == "ball possession") {
                            $(row).find(".num-progress-home").first().text(v.hval + "%");
                            $(row).find(".num-progress-away").first().text(v.aval + "%");
                        }
                        if (v.hval > v.aval) {
                            $(row).find(".num-progress-home").first().addClass("active");
                        } else if (v.hval < v.aval) {
                            $(row).find(".num-progress-away").first().addClass("active");
                        }
                        $(row).find(".progress-bar-home").attr("style", "width: " + v.hval / max * 100 + "%;");
                        $(row).find(".progress-bar-away").attr("style", "width: " + v.aval / max * 100 + "%;");
                        $(".statistics-mark[mid=" + mid + "]").append($(row));
                    });
                });
            });
        }
    }

    betonCount("all");
    setInterval(function () {
        betonCount("wait");
    }, 60000 * 3);

    function betonCount(getstate) {
        var midlist = "";
        // console.log(getstate);
        $.each($(".at-minute-mark"), function (k, v) {
            var state = $(v).attr("state");
            switch (state) {
                case "all":
                    midlist += $(v).attr("mid") + ",";
                    break;
                case "wait":
                    if (state == 17 || state == 0) {
                        midlist += $(v).attr("mid") + ",";
                    }
                    break;
                case "start":
                    if (state == 1 || state == 2 || state == 3 || state == 4) {
                        midlist += $(v).attr("mid") + ",";
                    }
                    break;
                default:
                    midlist += $(v).attr("mid") + ",";
                    break;
            }
        });
        if (midlist != "") {
            $.ajax({
                url: "/services/bet/getbetcount",
                data: {
                    midlist: midlist
                },
                dataType: "JSON",
                method: "GET"
            }).done(function (response) {
                console.log(response);
                $.each(response, function (k, v) {
                    var max = v.home + v.away;
                    if (max > 0) {
                        var hp = v.home / max * 100;
                        var ap = v.away / max * 100;
                        $(".beton-homep[mid=" + k + "]").text(numeral(hp).format("0.00") + "%");
                        $(".beton-awayp[mid=" + k + "]").text(numeral(ap).format("0.00") + "%");
                        $(".beton-home[mid=" + k + "]").attr("style", "width: " + hp + "%");
                        $(".beton-away[mid=" + k + "]").attr("style", "width: " + ap + "%");
                    }
                });
            });
        }
    }

    $(".filter-option").on("click", function (e) {
        var opt = $(e.currentTarget).attr("option");
        $(".color-tape").addClass("hide");
        switch (opt) {
            case "all":
                $(".color-tape").removeClass("hide");
                $("#filter-mark").text("ALL");
                break;

            case "live":
                $.each(livelist, function (k, v) {
                    $(".color-tape[mid=" + v + "]").removeClass("hide");
                });
                $("#filter-mark").text("LIVE");
                break;
            case "end":
                $.each(endlist, function (k, v) {
                    $(".color-tape[mid=" + v + "]").removeClass("hide");
                });
                $("#filter-mark").text("END");
                break;

            default:
                $(".color-tape").removeClass("hide");
                $("#filter-mark").text("ALL");
                break;
        }
    });
});
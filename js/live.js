/**
 * Created by mrsyrop on 2/3/2560.
 */
$(document).ready(function () {
    // $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
    //     FB.init({
    //         appId: '1677798712517610',
    //         status: true,
    //         xfbml: true,
    //         cookie: true,
    //         version: 'v2.8' // or v2.1, v2.2, v2.3, ...
    //     });
    //     if (FB.getUserID()) {
    //         getMatchNoti();
    //     }
    // });
    var zid = 0;
    var noti_list = [];
    var live_list = [];
    $('.league-select').on('change', function (e) {
        var lid = $(e.currentTarget).val();
        if(lid=="all"){
            $(".league-box").removeClass('hide');
        }else {
            $('.league-box').addClass('hide');
            $(".league-box[lid=" + lid + "]").removeClass('hide');
        }
        $('.league-select').val(lid);
    });

    $('.label-reset').on('click', function (e) {
        // $(".league-box").removeClass('hide');
        // console.log(".label-reset");
        if(!$(this).hasClass('disabled')) {
            getMatchUpdate();
        }
    });
    getMatchUpdate();
    setInterval(getMatchUpdate, 40000);
    function getMatchUpdate() {
        $('span.label-reset').html("Loading...");
        $('span.label-reset').addClass('disabled');
        if (zid == 0) {
            zid = $('#page-info').attr('zid');
        }
        $.ajax({
            url: "http://Ngoal.com/services/getmatchupdate.php",
            method: "GET",
            dataType: "JSON",
            data: {zid: zid}
        }).done(function (response) {
            $.each(response.data, function (k, v) {
                if (v['s1'].length > 0 && v['s1'] != "0-0" && v['s1'] != $(".now-score[mid=" + v['mid'] + "]").text().replace(' - ', '-')) {

                    if (noti_list.indexOf(v['mid']) != -1) {
                        var audio = new Audio('/system_config/sound/whistle.mp3');
                        audio.play();
                    }
                    $(".now-score[mid=" + v['mid'] + "]").text(v['s1'].replace("-", " - "));
                    $(".now-score[mid=" + v['mid'] + "]").addClass("blinking");
                    $(".now-score[mid=" + v['mid'] + "]").addClass("blink-color");
                    setTimeout(function () {
                        $(".now-score[mid=" + v['mid'] + "]").removeClass("blinking");
                        $(".now-score[mid=" + v['mid'] + "]").removeClass("blink-color");
                    }, 15000);
                }

                if (v['sid'].length > 0) {
                    $(".nowshowing[mid=" + v['mid'] + "]").attr('sid', v['sid']);
                    if (parseInt(v['sid']) == 2) {
                        var match = $(".set-noti[mid=" + v['mid'] + "]").first();
                        if ($(match).length !== 0) {
                            var mid = $(match).attr('mid');
                            var lid = $(match).attr('lid');
                            if (live_list.indexOf(mid) == -1) {
                                var livecount = parseInt($('.live-match-count').text());
                                copyMatch(lid, mid, '.live-box');
                                var matchchange = $(document).find(".live-matche[mid=" + mid + "]");
                                $.each(matchchange, function (k, v) {
                                    $(v).find('.showdate').first().addClass('nowshowing');
                                });
                                livecount++;
                                $('.live-match-count').text(livecount);
                                live_list.push(mid);
                            }
                        }
                    }
                }
            });
            zid = response.zid;

            var timeboxs = $(document).find('.nowshowing');
            $.each(timeboxs, function (k, v) {
                var mid = $(v).attr('mid');
                var sid = parseInt($(v).attr('sid'));
                var c0 = parseInt($(v).attr('c0'));
                var c1 = parseInt($(v).attr('c1'));
                var c2 = parseInt($(v).attr('c2'));
                var cx = parseInt($(v).attr('cx'));
                $(v).text(calTime(sid, c0, c1, c2, cx, response.c3));
            });
            $('span.label-reset').html("Refresh <i class='fa fa-refresh'></i>");
            $('span.label-reset').removeClass('disabled');
        }).error(function () {
            $('span.label-reset').html("Refresh <i class='fa fa-refresh'></i>");
            $('span.label-reset').removeClass('disabled');
        })
    }

    setInterval(blinkstart, 1500);
    function blinkstart() {
        $('.blinking').fadeOut(500);
        $('.blinking').fadeIn(500);
    }

    function blinkstop() {
        clearInterval(blinke);
    }

    $(document).on('click', '.set-noti', function (e) {
        var lid = $(e.currentTarget).attr('lid');
        var mid = $(e.currentTarget).attr('mid');
        if ($(e.currentTarget).hasClass('active-select-noti')) {
            $.ajax({
                url: "/services/unsetmatchnoti.php",
                data: {fb_uid: FB.getUserID(), mid: mid},
                method: "POST",
                dataType: "JSON"
            }).done(function (response) {
                console.log(response);
                var match = $(document).find(".set-noti[mid=" + mid + "]");
                $.each(match, function (k, v) {
                    $(v).removeClass('active-select-noti');
                    $(v).find(".white-bell").first().removeClass("hide");
                    $(v).find(".blue-bell").first().addClass("hide");

                });
                if (noti_list.indexOf(mid) != -1) {
                    noti_list.splice(noti_list.indexOf(mid), 1);
                }
                getMatchNoti();
            });
        } else {
            if (FB.getUserID()) {
                $.ajax({
                    url: "/services/setmatchnoti.php",
                    data: {fb_uid: FB.getUserID(), mid: mid},
                    method: "POST",
                    dataType: "JSON"
                }).done(function (response) {
                    getMatchNoti();
                });
            } else {
                alert('Please login!');
            }
        }
    });

    function copyMatch(lid, mid, destination) {
        var leaguebox = $('.allgame-box').find(".league-box[lid=" + lid + "]").first().clone();
        var matchbox = $(leaguebox).find(".live-matche[mid=" + mid + "]").first().clone();
        $(leaguebox).find(".table-fixtures").first().empty();
        $(leaguebox).find(".table-fixtures").first().append($(matchbox));

        var notileaguebox = $(destination).find(".league-box[lid=" + lid + "]");
        console.log(notileaguebox.length);
        if (notileaguebox.length == 0) {
            $(destination).append($(leaguebox));
        } else {
            notileaguebox = $(destination).find(".league-box[lid=" + lid + "]").first();
            $(notileaguebox).find(".table-fixtures").first().append($(matchbox));
        }
    }


    function getMatchNoti() {
        $.ajax({
            url: "/services/getmatchnoti.php",
            data: {fb_uid: FB.getUserID()},
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
            var mcount = 0;
            var endedmatch = [];
            $('.noti-box').empty();
            $.each(response.data, function (k, v) {
                if (v == 1) {
                    var match = $(".set-noti[mid=" + k + "]").first();
                    if ($(match).length !== 0) {
                        var matchs = $(".set-noti[mid=" + k + "]");
                        $.each(matchs, function (k, v) {
                            $(v).addClass('active-select-noti');
                            $(v).find(".white-bell").first().addClass("hide");
                            $(v).find(".blue-bell").first().removeClass("hide");
                        });
                        var mid = $(match).attr('mid');
                        var lid = $(match).attr('lid');
                        copyMatch(lid, mid, '.noti-box');
                        mcount++;
                        noti_list.push(mid);
                    } else {
                        endedmatch.push(k);
                    }
                }
            });
            $('#select-noti-count').text(mcount);
            clearNoti(endedmatch);
        });
    }

    function clearNoti(endedmatch) {
        $.ajax({
            type: "POST",
            url: "/services/clearmatchnoti.php",
            data: {fb_uid: FB.getUserID(), list: JSON.stringify(endedmatch)}
        }).done(function (reponse) {
            console.log(reponse);
        });
    }

    $('.ended-noti').on('click', function () {
            alert('This game has been ended.');
    });
});
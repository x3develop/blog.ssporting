/**
 * Created by mrsyrop on 14/2/2560.
 */
$(document).ready(function () {
    refreshTime();
    function refreshTime() {
        $.each($(".pass-long"), function (k, v) {
            if ($.isNumeric($(this).text())) {
                $(this).html(moment($(this).text(), "X").fromNow())
            } else {
                $(this).html(moment($(this).text(), "YYYY-MM-DD HH:mm:ss").fromNow())
            }
        })

    }
})

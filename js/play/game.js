$(document).ready(function () {
    // moment.locale('th');
    loadMatchAll();
    timeago();

    $(document).on('click','.a-vote-team-home,.close-tab-right', function () {
        console.log("addLeftShow");
        if($('#fade-in-index').hasClass('show') && $(".wrap-left-team").hasClass('show')) {
            $('#fade-in-index').removeClass('show');
            $(".wrap-left-team").removeClass('show');
        }else{
            console.log('addLeftShow');
            $('#fade-in-index').addClass('show');
            $(".wrap-left-team").addClass('show');
        }
        if($('li.active').length) {
            $('h1#h1-TeamHomeEn').html($('#ul-liveScoreHead').find('li.active').find('.name-team-slide-top').eq(0).html());
            $('h1#h1-TeamAwayEn').html($('#ul-liveScoreHead').find('li.active').find('.name-team-slide-top').eq(1).html());
            $('img#img-TeamHomePath').attr('src', $('#ul-liveScoreHead').find('li.active').find('img').eq(0).attr('src'));
            $('img#img-TeamAwayPath').attr('src', $('#ul-liveScoreHead').find('li.active').find('img').eq(1).attr('src'));
            $('form#bet-home-match').find('input[name="bet[time_match]"]').val($('#ul-liveScoreHead').find('li.active').attr('data-timematch'));
            $('form#bet-home-match').find('input[name="bet[match_id]"]').val($('#ul-liveScoreHead').find('li.active').attr('data-matchid'));
            // console.log($('li.active').attr('data-timeMatch'));
            // console.log($('li.active').attr('data-matchid'));
        }else{
             $('h1#h1-TeamHomeEn').html($('#ul-liveScoreHead').find('li.active').find('.name-team-slide-top').eq(0).html());
            $('h1#h1-TeamAwayEn').html($('#ul-liveScoreHead').find('li.active').find('.name-team-slide-top').eq(1).html());
            $('img#img-TeamHomePath').attr('src', $('#ul-liveScoreHead').find('li.active').find('img').eq(0).attr('src'));
            $('img#img-TeamAwayPath').attr('src', $('#ul-liveScoreHead').find('li.active').find('img').eq(1).attr('src'));
            $('form#bet-home-match').find('input[name="bet[time_match]"]').val($('#ul-liveScoreHead').find('li.active').attr('data-timematch'));
            $('form#bet-home-match').find('input[name="bet[match_id]"]').val($('#ul-liveScoreHead').find('li.active').attr('data-matchid'));
            // console.log($('.active').attr('data-timeMatch'));
            // console.log($('.active').attr('data-matchid'));
        }
        // console.log($('.active-playing').find('tr').last().find('img').attr('src'));
        // console.log($('li.active').find('tr').last().find('label').html());
    });

    $(document).on('click','.a-vote-team-away,.close-tab-left', function () {
        console.log('addRightShow');
        if($('#fade-in-right-index').hasClass('show') && $(".wrap-right-team").hasClass('show')) {
            $('#fade-in-right-index').removeClass('show');
            $(".wrap-right-team").removeClass('show');
        }else {
            $('#fade-in-right-index').addClass('show');
            $(".wrap-right-team").addClass('show');
        }
        if($('li.active').length) {
            $('h1#h1-TeamHomeEn').html($('#ul-liveScoreHead').find('li.active').find('.name-team-slide-top').eq(0).html());
            $('h1#h1-TeamAwayEn').html($('#ul-liveScoreHead').find('li.active').find('.name-team-slide-top').eq(1).html());
            $('img#img-TeamHomePath').attr('src', $('#ul-liveScoreHead').find('li.active').find('img').eq(0).attr('src'));
            $('img#img-TeamAwayPath').attr('src', $('#ul-liveScoreHead').find('li.active').find('img').eq(1).attr('src'));
            $('form#bet-away-match').find('input[name="bet[time_match]"]').val($('#ul-liveScoreHead').find('li.active').attr('data-timematch'));
            $('form#bet-away-match').find('input[name="bet[match_id]"]').val($('#ul-liveScoreHead').find('li.active').attr('data-matchid'));
        }else{
             $('h1#h1-TeamHomeEn').html($('#ul-liveScoreHead').find('li.active').find('.name-team-slide-top').eq(0).html());
            $('h1#h1-TeamAwayEn').html($('#ul-liveScoreHead').find('li.active').find('.name-team-slide-top').eq(1).html());
            $('img#img-TeamHomePath').attr('src', $('#ul-liveScoreHead').find('li.active').find('img').eq(0).attr('src'));
            $('img#img-TeamAwayPath').attr('src', $('#ul-liveScoreHead').find('li.active').find('img').eq(1).attr('src'));
            $('form#bet-away-match').find('input[name="bet[time_match]"]').val($('#ul-liveScoreHead').find('li.active').attr('data-timematch'));
            $('form#bet-away-match').find('input[name="bet[match_id]"]').val($('#ul-liveScoreHead').find('li.active').attr('data-matchid'));
        }
    });

    $(document).on('submit', '#bet-home-match,#bet-away-match', function (e){
        e.preventDefault();
        var $btn = $(this).find('button').first().button('loading');
        $.ajax({
            url: "/play/bet",
            data: $(this).serialize(),
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
            if(response['alert']=="alert-success"){
                if(response['comment']) {
                    var boxComment = $('#main-Comments').clone();
                    boxComment.attr('id', 'box-comment-' + response['comment']['id'])
                    boxComment.removeClass('hide');
                    boxComment.find('#comment-profile').attr('src', "https://graph.facebook.com/v2.8/" + response['comment']['fb_uid'] + "/picture");
                    boxComment.find('#comment-name').text(response['comment']['name'])
                    boxComment.find('#comment-comment').text(response['comment']['comment'])
                    boxComment.find('.comment-updated_at').attr('data-updated_at', response['comment']['updated_at']);
                    boxComment.find('i').first().attr('onclick', "addHeart(" + response['comment']['id'] + "," + response['comment']['match_id'] + ")");
                    boxComment.find('i').attr('id', 'commemt-heart-' + response['comment']['id']);
                    boxComment.find('.heart-count').attr('id', 'commemt-comment_match-' + response['comment']['id']);

                    boxComment.find('#form-comment').find('input[name="comment_match[team]"]').val(response['comment']['team']);
                    boxComment.find('#form-comment').find('input[name="comment_match[comment_match_id]"]').val(response['comment']['id']);
                    boxComment.find('#form-comment').find('input[name="comment_match[match_id]"]').val(response['comment']['match_id']);

                    $('tbody#box-comments-' + response['comment']['team'] + '-' + response['comment']['match_id'] + ' tr').last().before(boxComment);
                    console.log('#form-comment-' + response['comment']['team'] + '-' + response['comment']['match_id']);
                    $('#form-comment-' + response['comment']['team'] + '-' + response['comment']['match_id']).find('input[name="comment_match[comment]"]').val("");
                    timeago();
                    $('#bet-home-match').find('textarea').first().val("");
                    $('#bet-away-match').find('textarea').first().val("");
                    $('#bet-home-match').find('select').first().val(1200);
                    $('#bet-away-match').find('select').first().val(1200);
                }

                $('.vote-already').html("");

                if(response['betData']['team']=="home"){
                    $('#match-team-'+response['betData']['match_id']).find('.a-vote-team-away').html("");
                    $('#col-sm-match-'+response['betData']['match_id']).find('.vote-already').html("VOTE");
                    $('#fade-in-index').removeClass('show');
                    $(".wrap-left-team").removeClass('show');
                    $('#userBetMatchHome-'+response['betData']['match_id']).append("<img src='"+$('img#fb-picture').attr('src')+"/picture'>");
                }else if(response['betData']['team']=="away"){
                    $('#match-team-'+response['betData']['match_id']).find('.a-vote-team-home').html("");
                    $('#col-sm-match-'+response['betData']['match_id']).find('.vote-away').html("VOTE");
                    $('#fade-in-right-index').removeClass('show');
                    $(".wrap-right-team").removeClass('show');
                    $('#userBetMatchAway-'+response['betData']['match_id']).append("<img src='"+$('img#fb-picture').attr('src')+"/picture'>")
                }
                if($(location).attr('pathname')=="/index.php" || $(location).attr('pathname')=="/") {
                    $.ajax({
                        url: "/services-index",
                        method: "POST",
                        dataType: "JSON"
                    }).done(function (response) {
                        servicesIndex = response;
                        insertIndex();
                    });
                }

            }else{
                alert(response['message']);
            }
            $('#vote-left-'+response['betData']['match_id']).hide();
            $('#vote-right-'+response['betData']['match_id']).hide();
            $btn.button('reset');

            if($(location).attr('pathname')=="/games.php" || $(location).attr('pathname').indexOf("/match/")==0){
                window.location.reload();
            }
            // console.log($(location).attr('pathname'));
            console.log(response);
        });
    });

    $(document).on('submit', '.form-comment-home,.form-comment-away', function (e) {
        e.preventDefault();
        $.ajax({
            url: "/play/addComment",
            data: $(this).serialize(),
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
            var boxComment=$('#main-Comments').clone();
            boxComment.attr('id','box-comment-'+response['id'])
            boxComment.removeClass('hide');
            boxComment.find('#comment-profile').attr('src',"https://graph.facebook.com/v2.8/"+response['fb_uid']+"/picture");
            boxComment.find('#comment-name').text(response['name'])
            boxComment.find('#comment-comment').text(response['comment'])
            boxComment.find('.comment-updated_at').attr('data-updated_at',response['updated_at']);
            boxComment.find('i').first().attr('onclick',"addHeart("+response['id']+","+response['match_id']+")");
            boxComment.find('i').attr('id','commemt-heart-'+response['id']);
            boxComment.find('.heart-count').attr('id','commemt-comment_match-'+response['id']);

            boxComment.find('#form-comment').find('input[name="comment_match[team]"]').val(response['team']);
            boxComment.find('#form-comment').find('input[name="comment_match[comment_match_id]"]').val(response['id']);
            boxComment.find('#form-comment').find('input[name="comment_match[match_id]"]').val(response['match_id']);

            if($(location).attr('pathname')=="/games.php"){
                $('tbody#box-comments-' + response['team'] + '-' + response['match_id'] + ' tr').first().before(boxComment);
            }else {
                $('tbody#box-comments-' + response['team'] + '-' + response['match_id'] + ' tr').first().after(boxComment);
            }

            console.log('#form-comment-'+response['team']+'-'+response['match_id']);
            $('#form-comment-'+response['team']+'-'+response['match_id']).find('input[name="comment_match[comment]"]').val("");
            timeago();
        });
    });

    $(document).on('submit', '.form-comment', function (e) {
        e.preventDefault();
        $.ajax({
            url: "/play/addComment",
            data: $(this).serialize(),
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
            var boxComment=$('#main-miniComments').clone();
            boxComment.attr('id','box-comment-'+response['id'])
            boxComment.removeClass('hide');
            boxComment.find('#comment-profile').attr('src',"https://graph.facebook.com/v2.8/"+response['fb_uid']+"/picture");
            boxComment.find('#comment-name').html(response['name']);
            boxComment.find('#comment-comment').html(response['comment']);
            boxComment.find('.comment-updated_at').attr('data-updated_at',response['updated_at']);
            boxComment.find('i').attr('id','commemt-heart-'+response['id']);
            boxComment.find('.heart-count').attr('id','commemt-comment_match-'+response['id']);
            boxComment.find('i').first().attr('onclick',"addHeart("+response['id']+","+response['match_id']+")");

            $('tbody#comment-bx_reply-'+response['comment_match_id']+' tr').last().before(boxComment);
            $('.form-comment-'+response['comment_match_id']).find('input[name="comment_match[comment]"]').val("");
            timeago();
        });
    });

    $(document).on('click', '.list-match-all', function (e) {
        $('.list-match-all').removeClass('active-playing');
        $('#list-match-all-'+$(this).attr('data-matchId')).addClass('active-playing');
        $('.col-sm-match').addClass('hide');
        $('#col-sm-match-'+$(this).attr('data-matchId')).removeClass('hide');

        $('#fade-in-index').removeClass('show');
        $(".wrap-left-team").removeClass('show');
        $('#fade-in-right-index').removeClass('show');
        $(".wrap-right-team").removeClass('show');
    });

    $(document).on('click', '.show-bx-reply', function (e) {
        if($(this).parents('tr').find('.bx-reply').hasClass('hide')){
            $(this).parents('tr').find('.bx-reply').removeClass('hide');
        }else {
            $(this).parents('tr').find('.bx-reply').addClass('hide');
        }
    });
});

function loadMatchAll() {
    $('.list-match-all').each(function( index ) {
        if(index>0) {
            $.ajax({
                url: "/play/loadMatchGame",
                data: {id: $(this).attr('data-matchId')},
                method: "GET",
                dataType: "html"
            }).done(function (response) {
                $('#list-match-view').prepend(response);
                timeago();
            })
        }
    })
}

function viewMiniComment(id) {
    console.log("viewMiniComment");
    if($('#box-comment-'+id).find('.bx-reply').hasClass('hide')) {
        $("#comment-bx_reply-"+id).find('.miniBoxCommemt').remove();
        $.ajax({
            url: "/play/getMiniComment",
            data: {id: id},
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
            var box=$('#main-miniComments').clone();
            box.removeClass('hide');
            $.each( response, function( key, value ) {
                box.find('#comment-profile').attr('src',"https://graph.facebook.com/v2.8/"+value['fb_uid']+"/picture");
                box.find('#comment-name').html(value['name']);
                box.find('#comment-comment').html(value['comment']);
                box.find('#comment-comment').html(value['comment']);
                console.log(value);
            });
        });
    }else {
        $(this).parents('tr').find('.bx-reply').addClass('hide');
    }
}

function timeago() {
    $('.comment-updated_at').each(function( index ) {
        if($(this).attr('data-updated_at')!=="") {
            $(this).html(moment($(this).attr('data-updated_at'), "YYYY-MM-DD HH:mm:ss").add(1, 'hours').fromNow());
        }
    })
}

function addHeart(id,match_id) {
    $.ajax({
        url: "/play/addHeart",
        data: {id:id,match_id:match_id},
        method: "GET",
        dataType: "JSON"
    }).done(function (response) {
        $('#box-comment-'+id).find('.heart-count').text(response['count']);
        $('#box-comment-'+id).find('i').first().addClass('heart-active');
        $('#box-comment-'+id).find('i').first().attr('id','commemt-heart-'+id);
        $('#box-comment-'+id).find('i').first().attr('onclick',"removeHeart("+response['lastInsertId']+")");
        timeago();
    });
}

function removeHeart(id) {
    $.ajax({
        url: "/play/removeHeart",
        data: {id:id},
        method: "GET",
        dataType: "JSON"
    }).done(function (response) {
        $('#box-comment-'+response['commentHeart']['comment_match_id']).find('i').removeClass('heart-active');
        $('#box-comment-'+response['commentHeart']['comment_match_id']).find('.heart-count').text(response['count']);
        $('#box-comment-'+response['commentHeart']['comment_match_id']).find('i').first().attr('onclick',"addHeart("+response['commentHeart']['comment_match_id']+","+response['commentHeart']['match_id']+")");
        timeago();
    });
}
/**
 * Created by mrsyrop on 23/11/2559.
 */
$(document).ready(function () {
    var teamlogopath = "http://api.ssporting.com/teams_clean/";
    initHeaderGame();
    function initHeaderGame() {
        $.ajax({
            url: "/services/gettenmatch.php",
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
            //console.log(response)
            var game_original = $(".header-game").children().first().clone();
            $(".header-game").empty();
            $.each(response, function (k, v) {
                var game = $(game_original).clone();
                $(game).attr('mid', v[5]);
                //$(game).removeClass('active');
                $(game).find(".header-home-logo").first().attr('src', teamlogopath + v[0] + "_32x32.png");
                $(game).find(".header-home-name").first().text(v[2]);
                $(game).find(".header-guest-logo").first().attr('src', teamlogopath + v[1] + "_32x32.png");
                $(game).find(".header-guest-name").first().text(v[3]);
                $(game).find(".header-game-time").first().attr('data-datetime', v[4]);
                $(game).find(".header-game-time").first().formatDateTime('hh:ii');
                $(".header-game").append($(game));
            });
            $(".header-game").css('width', "2000px");

            var options = {
                horizontal: 1,
                itemNav: 'basic',
                smart: 1,
                activateOn: 'click',
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1,
                startAt: 0,
//            scrollBar: $wrap.find('.scrollbar'),
                scrollBy: 1,
                speed: 1000,
                elasticBounds: 1,
                easing: 'easeOutExpo',
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,

                // Cycling
                cycleBy: 'items',
                cycleInterval: 2000,
                pauseOnHover: 1,
            }

            $('#liveScoreHead').sly(options);
        })
    }

    getHotNEWS();
    function getHotNEWS() {
        $.ajax({
            url: "/services/gethotnews.php",
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
            $("#hotnews-header").text(response);
        });
    }

    getReccommendNews();
    function getReccommendNews() {
        $.ajax({
            url: "/services/getrecommentnews.php",
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
            //console.log(response);
            $.each(response, function (k, v) {
                var element = $(".recommend-news[recommend=" + v[4] + "]");
                $(element).attr('newsid', v[0]);
                $(element).find(".h-content").text(v[1]);
                $(element).find(".crop").css('background-image', "url(" + v[3] + ")");
                var d = new Date(parseInt(v[2]) * 1000);
                $(element).find(".how-long").text(jQuery.timeago(d));
            });

        });
    }

    getLastestNews()
    function getLastestNews() {
        $.ajax({
            url: "/services/getlastestnews.php",
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
            //console.log(response);
            $.each(response, function (k, v) {
                var element = $(".lastest-news")[k];
                $(element).attr('newsid', v[0]);
                $(element).find(".h-content").text(v[1]);
                $(element).find(".crop").css('background-image', "url(" + v[3] + ")");
                var d = new Date(parseInt(v[2]) * 1000);
                $(element).find(".how-long").text(jQuery.timeago(d));
            });

        });
    }

    getPopularNews();
    function getPopularNews() {
        $.ajax({
            url: "/services/getmostpopularnews.php",
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
            //console.log(response);
            $.each(response, function (k, v) {
                var element = $(".box-popular")[k];
                $(element).attr('newsid', v[0]);
                $(element).find("img").attr('src', v[3]);
                var d = new Date(parseInt(v[2]) * 1000);
                $(element).find(".how-long").text(jQuery.timeago(d));
            });

        });
    }

    function getNewVideo() {
        $.ajax({
            url: "/services/getmostpopularnews.php",
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
            //console.log(response);
            $.each(response, function (k, v) {
                var element = $(".box-popular")[k];
                $(element).attr('newsid', v[0]);
                $(element).find("img").attr('src', v[3]);
                var d = new Date(parseInt(v[2]) * 1000);
                $(element).find(".how-long").text(jQuery.timeago(d));
            });

        });
    }
})
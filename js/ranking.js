/**
 * Created by mrsyrop on 23/2/2560.
 */
$(document).ready(function () {
    var offset = 0;
    var limit = 10;
    var row_ori = $('#rank-row-original').clone();
    $('.bt-games').on('click', function (e) {
        window.location = '/fixtures.php';
    });
    $('.bt-seemore').on('click', function () {
        $.ajax({
            url: '/services/getuserranking.php',
            data: {offset: (++offset) * limit, limit: limit},
            method: 'GET',
            dataType: 'JSON'
        }).done(function (response) {
            console.log(response);
            addRowToTab($('#gp-alltime-tab'), response['gp']['alltime'], response['keylist']['gp']);
            addRowToTab($('#gp-now-tab'), response['gp']['now'], response['keylist']['gp']);
            addRowToTab($('#gp-period-tab'), response['gp']['period'], response['keylist']['gp']);
            addRowToTab($('#gold-alltime-tab'), response['gold']['alltime'], response['keylist']['gold']);
            addRowToTab($('#gold-now-tab'), response['gold']['now'], response['keylist']['gold']);
            addRowToTab($('#gold-period-tab'), response['gold']['period'], response['keylist']['gold']);
            addRowToTab($('#englist-premierleague-tab'), response['league']['english_premier_league'], response['keylist']['league']);
            addRowToTab($('#spain-laliga-tab'), response['league']['spain_laliga'], response['keylist']['league']);
            addRowToTab($('#euro-uefa-tab'), response['league']['uefa_champion_league'], response['keylist']['league']);
            addRowToTab($('#euro-uropa-tab'), response['league']['europa_league'], response['keylist']['league']);
            addRowToTab($('#thai-premierleague-tap'), response['league']['thai_premier_league'], response['keylist']['league']);
        });
    });

    function addRowToTab(tab, data, key) {

        $.each(data, function (k, v) {
            var row = $(row_ori).clone();
            $(row).removeClass('hide');
            var rdata = mapKV(key, v);
            //console.log(rdata);
            FB.api("/" + rdata['fb_uid'] + "/picture?type=normal", function (response) {
                $(row).find('.rank-no').first().text(rdata['current_rank']);
                $(row).find('.user-img-rank').attr('src', response.data.url);
                $(row).find('.username-ranking-top').first().text(rdata['display_name']);
                $(row).find('.rank-indicator').first().text(rdata['indicator']);
                $(tab).append(row);
            });

        })

    }
});
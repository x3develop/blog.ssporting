/**
 * Created by mrsyrop on 14/2/2560.
 */
$(document).ready(function () {
    //$.ajaxSetup({cache: true});
    // $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
    //     FB.init({
    //         appId: '1677798712517610',
    //         status: true,
    //         xfbml: true,
    //         cookie: true,
    //         version: 'v2.8' // or v2.1, v2.2, v2.3, ...
    //     });
    //     getComment();
    // });
    //localStorage.setItem('fb_uid', "1258778534171742");
    var sideselect = 'home';
    var fullsize_cm_original = $('.fullsize-cm-original').clone();
    var fullsize_rp_original = $('.fullsize-rp-original').clone();
    var reply_smgbox_orginal = $('.reply-smgbox-orginal').clone();
    var likelist = [];
    var showplayedimg = "<div class='box-img-user'><img src='' data-pin-nopin='true'></div>";

    $('.bt-play').on('click', function (e) {
        //console.log(e.currentTarget);
        var side = $(e.currentTarget).attr('side');
        sideselect = side;
        if (side == 'home') {
            $('.select-away').addClass('hide');
            $('.select-home').removeClass('hide');
        } else {
            $('.select-away').removeClass('hide');
            $('.select-home').addClass('hide');
        }
        $('#myModal').modal('show');
    });

    $('.button-play').on('click', function (e) {
        if ($(e.currentTarget).hasClass('select-away')) {
            $('.select-away').addClass('hide');
            $('.select-home').removeClass('hide');
            sideselect = 'home';
        } else {
            $('.select-away').removeClass('hide');
            $('.select-home').addClass('hide');
            sideselect = 'away';
        }
    });


    $('.btn-bet').on('click', function (e) {
        var side = sideselect;
        console.log('side', side);
        var mid = $('#game-info').attr('mid');
        var lid = $('#game-info').attr('lid');
        var hid = $('#game-info').attr('hid');
        var gid = $('#game-info').attr('gid');
        var c0 = $('#game-info').attr('c0');
        var sid = $('#game-info').attr('sid');
        var hdp = parseFloat($('#hdp_val').text());
        var betval = parseFloat($('#hdp_home_val').text());
        var oddstype = $('#hdp_val').attr('oddstype');
        var amount = $('#bet-amount').val();
        var msg = $('#bet-comment').val();
        if (side == "away") {
            betval = parseFloat($('#hdp_away_val').text());
        }
        //$('#myModal').modal('hide');
        if (FB.getUserID()) {
            var fb_uid = FB.getUserID();
            $(e.currentTarget).addClass('hide');
            $('#bet-wait-img').removeClass('hide');
            $.ajax({
                url: "/services/bet.php",
                data: {
                    side: side,
                    mid: mid,
                    lid: lid,
                    hid: hid,
                    gid: gid,
                    c0: c0,
                    sid: sid,
                    hdp: hdp,
                    betval: betval,
                    oddstype: oddstype,
                    fb_uid: fb_uid,
                    msg: msg,
                    amount: amount
                },
                method: "POST",
                dataType: "JSON"
            }).done(function (response) {
                $(e.currentTarget).removeClass('hide');
                $('#bet-wait-img').addClass('hide');
                $('#myModal').modal('hide');
                console.log(response);
                alert(response.desc);
                if (response.success) {
                    getComment()
                    if (side == 'home') {
                        if ($('.earlybet-home-user').first().children().length < 10) {
                            var playerpix = jQuery.parseHTML(showplayedimg);
                            $(playerpix).find('img').first().attr('src', getFbProfilePix(fb_uid, 'normal'));
                            $('.earlybet-home-user').first().append($(playerpix));
                        }
                    } else if (side == 'away') {
                        if ($('.earlybet-away-user').first().children().length < 10) {
                            var playerpix = jQuery.parseHTML(showplayedimg);
                            $(playerpix).find('img').first().attr('src', getFbProfilePix(fb_uid, 'normal'));
                            $('.earlybet-away-user').first().prepend($(playerpix, 'normal'));
                        }
                    }
                }
            });
        } else {
            alert("Please login");
        }
    });

    $('#post-comment').on('click', function (e) {
        var mid = $('#game-info').attr('mid');
        var choose = $('#game-info').attr('choose');
        var msg = $('#comment-inputbox').val();
        var parent = 0;
        var displayname = $('#game-info').attr('displayname');
        if (choose) {
            if (FB.getUserID()) {
                $(e.currentTarget).prop('disabled', true);
                $.ajax({
                    url: "/services/commentonmatch.php",
                    data: {
                        side: choose,
                        mid: mid,
                        fb_uid: FB.getUserID(),
                        msg: msg,
                        parent: parent
                    },
                    method: "POST",
                    dataType: "JSON"
                }).done(function (response) {
                    /*
                     var commentrow = $(fullsize_cm_original).clone();
                     $(commentrow).find('.reply-to').first().attr('parent', response.message_id);
                     $(commentrow).find('.reply-row').first().attr('parent', response.message_id);
                     $(commentrow).find('.reply-row').first().empty();
                     var comment = {
                     message: msg,
                     id: response.message_id,
                     fb_uid: FB.getUserID(),
                     like: 0,
                     comment_at: moment().unix(),
                     display_name: displayname,
                     parent_id: parent,
                     choose: choose
                     }


                     makeCmBox(commentrow, comment);

                     if (FB.getUserID()) {
                     var rpbox = $(reply_smgbox_orginal).clone();
                     $(rpbox).find('.reply-msg-val').first().attr('parent', comment['id']);
                     $(rpbox).attr('parent', comment['id']);
                     $(rpbox).find('.active-user-pix').first().attr('src', "https://graph.facebook.com/v2.8/" + FB.getUserID() + "/picture?type=normal");
                     $(commentrow).find('.reply-row').first().append(rpbox);
                     }
                     if (comment['choose'] == 'home') {
                     $('#cm-home-side').append(commentrow);
                     } else {
                     $('#cm-away-side').append(commentrow);
                     }
                     $(e.currentTarget).prop('disabled', false);
                     */
                    getComment();
                });
            } else {
                alert("Please login");
            }
        } else {
            alert('Please play a game before comment.');
        }

    });

    $(document).on('keypress', '.reply-msg-val', function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
            $(e.currentTarget).prop('readonly', true);
            var parent = $(e.currentTarget).attr('parent');
            var msg = $(e.currentTarget).val();
            var mid = $('#game-info').attr('mid');
            var choose = $('#game-info').attr('choose');
            var displayname = $('#game-info').attr('displayname');
            if (choose) {
                if (FB.getUserID()) {
                    $.ajax({
                        url: "/services/commentonmatch.php",
                        data: {
                            side: choose,
                            mid: mid,
                            fb_uid: FB.getUserID(),
                            msg: msg,
                            parent: parent
                        },
                        method: "POST",
                        dataType: "JSON"
                    }).done(function (response) {
                        if (response.success) {
                            var rprow = $(fullsize_rp_original).clone();
                            var reply = {
                                message: msg,
                                id: response.message_id,
                                fb_uid: FB.getUserID(),
                                like: 0,
                                comment_at: moment().unix(),
                                display_name: displayname,
                                parent_id: parent
                            }
                            makeRpBox(rprow, reply);
                            var rpbox = $(document).find(".reply-smgbox-orginal[parent=" + parent + "]").first();
                            $(rprow).insertBefore($(rpbox));
                        } else {
                            alert(response.desc);
                        }

                        $(e.currentTarget).prop('readonly', false);
                        $(e.currentTarget).val('');
                    });
                } else {
                    alert("Please login");
                }
            } else {
                alert('Please play a game before comment.');
            }
        }
    });


    function getComment() {
        var mid = $('#game-info').attr('mid');
        $.ajax({
            url: "/services/getcommentbymid.php",
            data: {
                mid: mid
            },
            method: "POST",
            dataType: "JSON"
        }).done(function (response) {
            likelist = response['likelist'];
            $('#cm-home-side').empty();
            $('#cm-away-side').empty();
            $('#cm-home-side').removeClass('hide');
            $('#cm-away-side').removeClass('hide');
            var homesize = 0;
            var awaysize = 0;
            console.log(Object.keys(response.commentlist).length);
            var keyarr = Object.keys(response.commentlist);
            for (var i = keyarr.length - 1; i >= 0; i--) {
                var val = response.commentlist[keyarr[i]];
                console.log(val);
                var comment = mapKV(response['keylist']['comment'], val['comment']);
                var commentrow = $(fullsize_cm_original).clone();
                $(commentrow).find('.reply-to').first().attr('parent', comment['id']);
                $(commentrow).find('.reply-row').first().attr('parent', comment['id']);
                $(commentrow).find('.reply-row').first().empty();
                makeCmBox(commentrow, comment);
                //$.each(val['reply'], function (k, v) {
                //    var reply = mapKV(response['keylist']['comment'], v);
                //    var rprow = $(fullsize_rp_original).clone();
                //    makeRpBox(rprow, reply);
                //    $(commentrow).find('.reply-row').first().append(rprow);
                //});

                if (FB.getUserID()) {
                    var rpbox = $(reply_smgbox_orginal).clone();
                    $(rpbox).find('.reply-msg-val').first().attr('parent', comment['id']);
                    $(rpbox).attr('parent', comment['id']);
                    $(rpbox).find('.active-user-pix').first().attr('src', "https://graph.facebook.com/v2.8/" + FB.getUserID() + "/picture?type=normal");
                    $(commentrow).find('.reply-row').first().append(rpbox);
                }
                if (comment['choose'] == 'home') {
                    if (homesize < 2) {
                        $('#cm-home-side').append(commentrow);
                    }
                    homesize++;
                } else {
                    if (awaysize < 2) {
                        $('#cm-away-side').append(commentrow);
                    }
                    awaysize++;
                }
                if (homesize == 2 && awaysize == 2) {
                    break;
                }
            }

            //console.log(likelist);
        });
    }

    function makeRpBox(rprow, reply) {
        $(rprow).find('.cm-profile-pix').first().attr('src', "https://graph.facebook.com/v2.8/" + reply['fb_uid'] + "/picture?type=normal");
        $(rprow).find('.cm-name-user').first().text(reply['display_name']);
        $(rprow).find('.rp-msg').first().text(reply['message']);
        $(rprow).find('.how-long').first().text(moment(reply['comment_at'], 'X').fromNow());
        $(rprow).find('.like-count').first().text(reply['like']);
        $(rprow).find('.like-count').first().attr('commentid', reply['id']);
        $(rprow).find('.reply-to').first().attr('parent', reply['parent_id']);
        $(rprow).find('.like-comment').first().attr('commentid', reply['id']);
        if (reply['id'] in likelist) {
            if ($.inArray(FB.getUserID(), likelist[reply['id']]) >= 0) {
                $(rprow).find('.like-comment').first().addClass('red-heart');
            } else {
                $(rprow).find('.like-comment').first().removeClass('red-heart');
            }
        }
    }

    function makeCmBox(commentrow, comment) {
        $(commentrow).find('.cm-profile-pix').first().attr('src', "https://graph.facebook.com/v2.8/" + comment['fb_uid'] + "/picture?type=normal");
        $(commentrow).find('.cm-name-user').first().text(comment['display_name']);
        $(commentrow).find('.cm-msg').first().text(comment['message']);
        $(commentrow).find('.how-long').first().text(moment(comment['comment_at'], 'X').fromNow());
        $(commentrow).find('.like-count').first().text(comment['like']);
        $(commentrow).find('.like-count').first().attr('commentid', comment['id']);
        $(commentrow).find('.like-comment').first().attr('commentid', comment['id']);
        if (comment['id'] in likelist) {
            if ($.inArray(FB.getUserID(), likelist[comment['id']]) >= 0) {
                $(commentrow).find('.like-comment').first().addClass('red-heart');
            } else {
                $(commentrow).find('.like-comment').first().removeClass('red-heart');
            }
        }
    }

    $('.lnktogame').on('click', function (e) {
        var mid = $(e.currentTarget).attr('mid');
        window.location = 'game.php?mid=' + mid;
    });

    $(document).on('click', '.like-comment', function (e) {
        var id = $(e.currentTarget).attr('commentid');
        var mid = $('#game-info').attr('mid');
        var status = 'like';

        console.log(likelist);
        if (FB.getUserID()) {

            if (id in likelist) {
                if ($.inArray(FB.getUserID(), likelist[id]) >= 0) {
                    status = 'unlike';
                }
            } else {
                likelist[id] = [];
            }

            $.ajax({
                url: '/services/likematchcomment.php',
                data: {mid: mid, comment_id: id, fb_uid: FB.getUserID(), status: status},
                method: 'POST',
                dataType: 'JSON'
            }).done(function (response) {
                if (response.success) {
                    var olike = 0;
                    if (status == 'like') {
                        $(".like-comment[commentid=" + id + "]").addClass('red-heart');
                        olike = parseInt($(".like-count[commentid=" + id + "]").text());
                        $(".like-count[commentid=" + id + "]").text(++olike);
                        likelist[id].push(FB.getUserID());
                    } else {
                        $(".like-comment[commentid=" + id + "]").removeClass('red-heart');
                        olike = parseInt($(".like-count[commentid=" + id + "]").text());
                        $(".like-count[commentid=" + id + "]").text(--olike);
                        likelist[id].splice($.inArray(FB.getUserID(), likelist[id]), 1);
                    }
                    console.log(olike);
                    console.log(likelist);
                }
            });
        } else {
            alert("Please login");
        }
    });

    //getCountDown();

    $(document).on('click', '.reply-to', function (e) {
        var mid = $('#game-info').attr('mid');
        window.location = 'game.php?mid=' + mid;
    });

});
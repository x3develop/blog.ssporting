/**
 * Created by mrsyrop on 17/2/2560.
 */
$(document).ready(function () {
    var fbuid = getQueryString("fbuid");
    google.charts.load("current", {packages: ["corechart"]});
    $('#betview-filter').on('change', function (e) {
        var viewtype = $(e.currentTarget).val();
        //console.log(viewtype);

        if (viewtype == "view-all") {
            $(".view-bet").removeClass('hide');
        } else {
            $(".view-bet").addClass('hide');
            $(".view-bet[viewtype=" + viewtype + "]").removeClass('hide');
        }
    });

    getUser();
    function getUser() {
        $.ajax({
            url: "/services/getuserbyfbuid.php",
            data: {fbuid: fbuid},
            method: "GET",
            dataType: "JSON"
        }).done(function (response) {
            data = mapKV(response.keylist.user, response.user);
            //console.log(response);
            var rsdata = [["result", "value"], ["draw", parseInt(data['d'])], ["lose", parseInt(data['l'])], ["win", parseInt(data['w'])]];
            var acc =0;
            if(data['w']!=0) {
                acc = parseFloat((parseInt(data['w']) / (parseInt(data['w']) + parseInt(data['l'])) * 100).toFixed(2));
            }
            google.charts.setOnLoadCallback(
                function () {
                    //console.log('draw donut', rsdata);
                    drawDonut('result-circle', rsdata);
                });

            // console.log('acc', acc);
            circleProgressCustom('#accuracy-rate', acc, '#ccc', '#0091c8', 4, 4, acc + '%');

        });
    }

    function getQueryString(field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    }

    $('.btn-combo-step').on('click', function (e) {
        if (FB.getUserID()) {
            $.ajax({
                url: "/services/exchangestep.php",
                data: {fb_uid: FB.getUserID()},
                method: "POST",
                dataType: "JSON"
            }).done(function (response) {
                alert(response.desc);
            });
        } else {
            alert("Please login!");
        }
    });


    $('.btn-combo-day').on('click', function (e) {
        if (FB.getUserID()) {
            $.ajax({
                url: "/services/exchangecomboday.php",
                data: {fb_uid: FB.getUserID()},
                method: "POST",
                dataType: "JSON"
            }).done(function (response) {
                alert(response.desc);
            });
        } else {
            alert("Please login!");
        }
    });

    $('.btn-reset-gold').on('click', function (e) {
        var agreed = confirm("การรีเซ็ตจะทำให้เหรียญทองของคุณเป็น 0 แต่จะไม่มีผลกับการจัดลำดับด้วยเหรียญทอง คุณต้องการรีเซ็ตหรือไม่");
        if (agreed) {
            if (FB.getUserID()) {
                $.ajax({
                    url: "/services/resetgold.php",
                    data: {fb_uid: FB.getUserID()},
                    method: "POST",
                    dataType: "JSON"
                }).done(function (response) {
                    alert(response.desc);
                });
            } else {
                alert("Please login!");
            }
        }
    });

    $('.show-exchange-box').on('click', function (e) {
        if ($('.section-exchange-coins').hasClass('hide')) {
            $('.section-exchange-coins').removeClass('hide');
        } else {
            $('.section-exchange-coins').addClass('hide');
        }
    });

    $('#gold-select-diamond').on('change', function (e) {
        var gold = $(e.currentTarget).val();
        $('#diamond-received').text(gold / 1000);
    });
    $('#gold-select-coin').on('change', function (e) {
        var gold = $(e.currentTarget).val();
        $('#coin-received').text(gold * 100);
    });

    $('#gold-for-diamond').on('click', function (e) {
        var gold = parseInt($('#gold-select-diamond').val());
        if (gold > 0) {
            var agreed = confirm('ยืนยันการแลกเปลี่ยน');
            if (agreed) {
                if (FB.getUserID()) {
                    $.ajax({
                        url: "/services/exchangegoldfordiamond.php",
                        data: {fb_uid: FB.getUserID(), gold: gold},
                        method: "POST",
                        dataType: "JSON"
                    }).done(function (response) {
                        alert(response.desc);
                    });
                } else {
                    alert("Please login!");
                }
            }
        }
    });

    $('#gold-for-coin').on('click', function (e) {
        var gold = parseInt($('#gold-select-coin').val());
        if (gold > 0) {
            var agreed = confirm('ยืนยันการแลกเปลี่ยน');
            if (agreed) {
                if (FB.getUserID()) {
                    $.ajax({
                        url: "/services/exchangegoldforcoin.php",
                        data: {fb_uid: FB.getUserID(), gold: gold},
                        method: "POST",
                        dataType: "JSON"
                    }).done(function (response) {
                        alert(response.desc);
                    });
                } else {
                    alert("Please login!");
                }
            }
        }
    });
    getCountDown();
})
;
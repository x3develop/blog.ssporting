function drawDonut(element, data) {
    var data = google.visualization.arrayToDataTable(data);

    var options = {
        pieStartAngle: 0,
        chartArea: {top: 7, height: '90%', width: '100%'},
        height: '180',
        width: '203',
        colors: ['#888', '#e40520', '#0091c8'],
        pieHole: 0.5,
        legend: 'none',
    };

    var chart = new google.visualization.PieChart(document.getElementById(element));
    chart.draw(data, options);
}
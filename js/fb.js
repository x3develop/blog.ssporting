/**
 * Created by mrsyrop on 14/2/2560.
 */
$(document).ready(function () {
    $.ajaxSetup({cache: true});

    // $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
    //     FB.init({
    //         appId: '1630409860331473',
    //         status: true,
    //         xfbml: true,
    //         cookie: true,
    //         version: 'v2.8' // or v2.1, v2.2, v2.3, ...
    //     });
    //     //$('#loginbutton,#feedbutton').removeAttr('disabled');
    //     //FB.getLoginStatus(updateStatusCallback);
    //     checkLogin();
    // });

    function fblogin() {
        FB.login(function (response) {
            if (response.authResponse) {
                //console.log('Welcome!  Fetching your information.... ');
                FB.api('/me', 'get', {
                    fields: 'id,email,name,first_name,middle_name,last_name',
                    edges: 'friends,friendlists'
                }, function (response) {
                    //console.log('Good to see you, ' + response.name + '.');
                    console.log(response);
                    showFbUser(response);
                    updateUser(response);
                    //localStorage.setItem('fb_uid', response.id);

                });
                console.log(response);
            } else {
                console.log('User cancelled login or did not fully authorize.');
            }
        }, {scope: 'public_profile,email,user_friends'});
    }

    function fblogout() {
        FB.logout(function (response) {
            // user is now logged out
        });
        // user is now logged out
        //$('#fb-online').addClass('hide');
        //$('#facebook-loginbutton').removeClass('hide');
        //localStorage.removeItem('fb_uid');
    }

    $('#facebook-loginbutton').on('click', function () {
        fblogin();
    });
    $('#facebook-logoutbutton').on('click', function () {
        fblogout();
    });

    function showFbUser(response) {
        $('#fb-name').text(response.name);
        $('#fb-picture').attr('src', "https://graph.facebook.com/v2.8/" + response.id + "/picture");
        $('#fb-online').removeClass('hide');
        $('#facebook-loginbutton').addClass('hide');
        $('#own-profile-link').attr('href', "/profile.php?fbuid=" + response.id);
    }

    function checkLogin() {
        console.log('checkLogin');
        FB.getLoginStatus(function (response) {
            console.log('check login', response)
            if (response.status == 'connected') {
                FB.api('/me', {
                    fields: 'id,email,name,first_name,middle_name,last_name',
                    edges: 'friends,friendlists'
                }, function (response) {
                    console.log('check login', response)
                    showFbUser(response);
                    updateUser(response);
                    //getFbProfilePix(response.id, 'normal',$('#profile-pix'));
                });
            } else {
                $('#facebook-loginbutton').removeClass('hide');
            }
        });
    }

    function updateUser(response) {

        var email = "";
        var middlename = "";
        if ("email" in response) {
            email = response.email;
        }
        if ("middle_name" in response) {
            middlename = response.middle_name;
        }
        $.ajax({
            url: "/services/updateuser.php",
            data: {
                id: response.id,
                email: email,
                name: response.name,
                first_name: response.first_name,
                middle_name: middlename,
                last_name: response.last_name
            },
            method: "POST",
            dataType: "JSON"
        }).done(function (response) {
            console.log(response);
        })
    }

    function getFbProfilePix(id, type, img_element) {
        FB.api(
            "/" + id + "/picture", {type: type},
            function (response) {
                console.log('picture', response);
                if (response) {
                    console.log($(img_element));
                    $(img_element).attr("src", response.data.url);
                }
            }
        );
    }

});
/**
 * Created by mrsyrop on 9/2/2560.
 */
function getByKey(key, keylist, data) {
    return data[keylist[key]];
}
function mapKV(keylist, data) {
    var rs = [];
    $.each(keylist, function (k, v) {
        rs[k] = data[v];
    });

    return rs;
}

function getFbProfilePix(fbuid, type) {
    if (!$.isNumeric(fbuid)) {
        fbuid = 0;
    }
    return "https://graph.facebook.com/v2.8/" + fbuid + "/picture?type=" + type;
}

function getCountDown() {
    $.ajax({
        url: "/services/getcountdown",
        method: "GET",
        dataType: "JSON"
    }).done(function (response) {
        var eventTime = response.timestamp;
        var currentTime = response.today_timestamp;
        var diffTime = eventTime - currentTime;
        var duration = moment.duration(diffTime * 1000, 'milliseconds');
        var interval = 1000;

        setInterval(function () {
            duration = moment.duration(duration - interval, 'milliseconds');
            //$('.countdown').text(duration.hours() + ":" + duration.minutes() + ":" + duration.seconds())
            //console.log(duration.days(), duration.hours() + ":" + duration.minutes() + ":" + duration.seconds());
            $('.countdown-sec').text(duration.seconds());
            $('.countdown-min').text(duration.minutes());
            $('.countdown-h').text(duration.hours());
            $('.countdown-d').text(duration.days());
        }, interval);
    });
}

function calTime(sid, c0, c1, c2, cx, c3) {
    var minuteat = '';
    if (sid == 2) {
        var ap = calHatftime(cx);
        var ao = parseInt(((c3 - c0) - c1) / 60);
        if (ao < 1) {
            minuteat = "1'";
        } else if (ao > ap) {
            minuteat = $ap + '+';
        } else {
            minuteat = ao + "'";
        }
    } else if (sid == 4) {
        var ap = calHatftime(0);
        var ao = parseInt(((c3 - c0) - c2) / 60 + ap);
        if (ao <= ap) {
            minuteat = (ap + 1) + "'";
        } else if (ao > ap) {
            ap = ap * 2;
            if (ao > ap) {
                minuteat = ap + "'";
            } else {
                minuteat = ao + "'";
            }
        }
    } else if (sid == 3) {
        minuteat = 'HT';
    } else if (sid >= 5) {
        minuteat = 'FT';
    }
    return minuteat;
}
function calHatftime(cx) {
    var rs = cx;
    if (cx <= 0) {
        rs = 45;
    }
    return rs;
}



<?php
include_once './model/NewsM.php';
include_once './model/VideoM.php';
include_once './utility/Carbon/Carbon.php';
use Carbon\Carbon;

?>
<?php
$newsid = ($_REQUEST['newsid']) ? (int)$_REQUEST['newsid'] : 0;
if ($newsid == 0) {
    header("Location: /");
}
$newsm = new NewsM();
$videom = new VideoM();
$newslist = array();
$newslist = $newsm->getNewsById($newsid);
$news = array();
if (array_key_exists(0, $newslist)) {
    $news = $newslist[0];
} else {
    header("Location: /");
}

$relatednews = $newsm->getRelatedNews($news->news_tag, 4);
$recommendnews = $newsm->getRecommendNews($news->category, 5);
$cb = Carbon::createFromTimestamp($news->createDatetime);
$up = Carbon::createFromTimestamp($news->updateDatetime);
$dataRelatedVideos = $videom->getRelatedVideo($news->news_tag, 4);

?>

<div class="wrapper-news-articles news-articles-list" style="padding-top: 50px;">
    <div class="containter-fluid">
        <div class="bx-head-articles">
            <div class="hilight-cover" style="opacity: 0.8;"></div>
            <div class="articles-title">
                <div class="container">
                    <h1 class="leads-headline"><span><?php echo $news->titleTh; ?></span>
                    </h1>
                </div>
            </div>
            <div class="background-image_image2" style="background-image:url(<?php echo $news->imageLink; ?>)"></div>
        </div>
        <div style="clear: both"></div>
    </div>
    <div style="clear: both"></div>
    <div class="container">
        <div class="wrap-content-cols">
            <div class="rows">
                <div class="col-sm-9 main_articles" newsid="<?php echo $newsid ?>"
                     id="main_articles_<?php echo $newsid ?>">
                    <div class="bx-publish-info">
                        <span class="pull-left" style="padding-top: 5px;"><?php echo $cb->format('F d, Y - h:i A'); ?>
                            Updated on <?php echo $up->format('F d, Y - h:i A'); ?></span>
                        <span class="bx-social pull-right">
<!--                        <span style="color: #3B579D;"><i class="fa fa-facebook"></i> </span>-->
                            <!--                        <span style="color: #4AC7F9;"><i class="fa fa-twitter"></i></span>-->
                            <!--                        <span> <i class="fa fa-mail-forward"></i> </span>-->
                    </span>

                        <div style="clear: both;"></div>
                    </div>
                    <div class="content-articles">
                        <?php echo $news->shortDescriptionTh; ?>
                        <div style="clear: both;"></div>
                        <?php if (!empty($news->linkVideo)) { ?>
                            <div id="Main_Box_Video_Articles_<?php echo $newsid ?>"
                                 style="width: 100%;height: 635px;padding: 2%;">
                                <?php if ($news->typeVideo == "dailymotion") { ?>
                                    <iframe id="Video_Articles_<?php echo $newsid ?>" style="position: absolute"
                                            width="95%" height="600" data-autoplay="true"
                                            src="http://www.dailymotion.com/embed/video/<?php echo $news->videokey ?>"
                                            StopSrc="http://www.dailymotion.com/embed/video/<?php echo $news->videokey ?>"
                                            StartSrc="<?php echo $news->urlIframe ?>"
                                            frameborder="0"
                                            allowfullscreen></iframe>
                                <?php } elseif ($news->typeVideo == "twitter") { ?>
                                    <iframe id="Video_Articles_<?php echo $newsid ?>" style="position: absolute"
                                            width="95%" height="600" data-autoplay="true"
                                            src="https://twitter.com/i/cards/tfw/v1/<?php echo $news->videokey ?>"
                                            StopSrc="https://twitter.com/i/cards/tfw/v1/<?php echo $news->videokey ?>"
                                            StartSrc="<?php echo $news->urlIframe ?>"
                                            frameborder="0"
                                            allowfullscreen></iframe>
                                <?php } elseif ($news->typeVideo == "facebook") { ?>
                                    <iframe id="Video_Articles_<?php echo $newsid ?>" style="position: absolute"
                                            width="95%" height="600" data-autoplay="true"
                                            src="https://www.facebook.com/v2.3/plugins/video?allowfullscreen=true&container_width=800&href=<?php echo $news->linkVideo ?>"
                                            StopSrc="https://www.facebook.com/v2.3/plugins/video?allowfullscreen=true&container_width=800&href=<?php echo $news->linkVideo ?>"
                                            StartSrc="<?php echo $news->urlIframe ?>"
                                            frameborder="0"
                                            allowfullscreen></iframe>
                                <?php } else { ?>
                                    <iframe id="Video_Articles_<?php echo $newsid ?>" style="position: absolute"
                                            id="large-videos-autoPlay" width="94%" height="600" data-autoplay="true"
                                            src="http://www.youtube.com/embed/<?php echo $news->videokey ?>"
                                            StopSrc="http://www.youtube.com/embed/<?php echo $news->videokey ?>"
                                            StartSrc="<?php echo $news->urlIframe ?>"
                                            frameborder="0"
                                            allowfullscreen></iframe>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <div style="clear: both;"></div>
                        <img src="<?php echo $news->imageLink; ?>" style="width: 100%;">
                        <?php echo $news->contentTh; ?>
                    </div>
                </div>
                <div class="col-sm-3 menu_articles" id="menu_articles_<?php echo $newsid ?>"
                     style="position: relative;">
                    <div id="menu_articles_highlight_<?php echo $newsid ?>" class="bx-publish-recommend"
                         style="position: absolute;top: 0;">
                        <div class="title">
                            <h3>Most Recented</h3>
                        </div>

                        <table>
                            <?php foreach ($recommendnews as $key => $recommend) { ?>
                                <tr>
                                    <td>
                                        <a href="<?php echo '/articles?newsid=' . $recommend->newsid; ?>">
                                        <div class="news-image">
                                            <div class="crop">
                                                <img src="<?php echo $recommend->imageLink; ?>">
                                            </div>
                                        </div>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?php echo '/articles?newsid=' . $recommend->newsid; ?>">
                                            <?php echo $recommend->titleTh; ?>
                                        </a>
                                        <div class="times-content">
                                            <span class="timeMoreRecentedGallery"
                                                  date="<?php echo $recommend->createDatetime ?>">3 hours ago</span>
                                        </div>

                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                        <div style="border: solid 0px;width: 100%;height: 200px;"
                             id="Box_Mini_Video_Articles_<?php echo $newsid ?>"></div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
        <div class="wrap-content-cols content-related-article">
            <div class="double-underline bx-tabs-related relate25">Related</div>
            <div class="rows">
                <?php if (!empty($relatednews)) { ?>
                    <div class="label-news-vertical">News</div>
                    <?php foreach ($relatednews as $key => $relate) { ?>
                        <div class="col-sm-3 related-news" newsid="<?php echo $relate->newsid; ?>">
                            <div class="box-videos box-related box-related-sexy">
                                <div class="bx-type-gallery">
                                    <i class="fa fa-picture-o"></i>
                                </div>
                                <div class="crop">
                                    <img src="<?php echo $relate->imageLink; ?>" data-pin-nopin="true">
                                </div>
                            </div>
                            <div class="title-owl-carousel">
                                <a href="<?php echo '/articles?newsid=' . $relate->newsid; ?>"><?php echo $relate->titleTh; ?></a>
                            </div>
                            <div class="times-content">
                                <span class="timeMoreRecentedGallery" date="<?php echo $relate->createDatetime ?>">23 minutes ago</span>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
            <div style="clear: both;"></div>
        </div>
        <div class="wrap-content-cols content-related-article">
            <div class="double-underline bx-tabs-related">&nbsp;</div>
            <div class="rows">
                <?php if (!empty($dataRelatedVideos)) { ?>
                    <div class="label-news-vertical">Video</div>
                    <?php foreach ($dataRelatedVideos as $key => $value) { ?>
                        <div class="col-sm-3">
                            <a href="/<?php echo(($value->videotype == 'highlight') ? 'videos' : 'hothit-videos') ?>?videoId=<?php echo $value->video_id ?>">
                                <div class="box-videos box-related box-related-sexy">
                                    <div class="bx-type-gallery">
                                        <i class="fa fa-play"></i>
                                    </div>
                                    <div class="crop">
                                        <img src="<?php echo $value->urlImg; ?>" data-pin-nopin="true">
                                    </div>
                                </div>
                                <div class="title-owl-carousel"><?php echo $value->title ?></div>
                                <div class="times-content"><span
                                            class="timeMoreRecentedGallery"
                                            date="<?php echo $value->create_datetime ?>">23 minutes ago</span></div>
                            </a>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
            <div style="clear: both;"></div>
        </div>
        <div style="clear: both"></div>
    </div>
    <div style="clear: both"></div>
</div>
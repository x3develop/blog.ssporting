<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 6/25/2018
 * Time: 9:56 PM
 */
$id=((isset($_GET['id']))?$_GET['id']:null);
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
$playMatch = new playMatch();
$matchResultsAway = $playMatch->getListMatchResults($id, 20);
?>
<td>Last <span class="total-results">5</span> Match</td>
<?php if (!empty($matchResultsAway)) { ?>
    <?php foreach ($matchResultsAway as $keyResults => $valueResults) { ?>
        <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
            <td scoreAway="<?=$valueResults->scoreAway?>" scoreHome="<?=$valueResults->scoreHome?>" number="<?=floor($keyResults/5);?>" class="<?=((floor($keyResults/5)==0)?'':'hide')?> li-results"><img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/draw.png"></td>
        <?php } elseif ($valueResults->team_home == $matchView->team_away && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
            <td scoreAway="<?=$valueResults->scoreAway?>" scoreHome="<?=$valueResults->scoreHome?>" number="<?=floor($keyResults/5);?>" class="<?=((floor($keyResults/5)==0)?'':'hide')?> li-results"><img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/win.png"></td>
        <?php } elseif ($valueResults->team_away == $matchView->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
            <td scoreAway="<?=$valueResults->scoreAway?>" scoreHome="<?=$valueResults->scoreHome?>" number="<?=floor($keyResults/5);?>" class="<?=((floor($keyResults/5)==0)?'':'hide')?> li-results"><img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/win.png"></td>
        <?php } else { ?>
            <td scoreAway="<?=$valueResults->scoreAway?>" scoreHome="<?=$valueResults->scoreHome?>" number="<?=floor($keyResults/5);?>" class="<?=((floor($keyResults/5)==0)?'':'hide')?> li-results"><img title="<?= $valueResults->time_match ?>" src="/images/icon-stat/betlist_result/lose.png"</td>
        <?php } ?>
    <?php } ?>
<?php } ?>
<td class="btn-results-right" number="0"><i class="fa fa-caret-right" aria-hidden="true"></i></td>

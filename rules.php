<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>กติกาและเงื่อนไขการเล่นเกม ทายผลบอล</title>
    <meta name="keywords" content="เกมทายผลบอล, ทายผลบอล, ผลบอล, ผลบอลสด, ผลบอลเมื่อคืน, ผลบอลย้อนหลัง">
    <meta name="description" content="กติกาและเงื่อนไขของการเล่นเกม ทายผลบอล วีธีการเล่นทายผลบอล Ngoal ข้อมูล SGoal และ Scoin รวมทั้งเงื่อนไขการรับรางวัล จากเกมทายผลบอล">
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/doce.css">
    <link rel="stylesheet" href="/css/livescore.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="js/jquery/dist/jquery.min.js"></script>
    <script src="js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="nanoscrollbar/nanoscrollbar.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/timeview.js"></script>
    <script src="js/game.js"></script>
    <script src="js/sly/sly.js"></script>
</head>
<script>

</script>
<body style="position: relative;" data-spy="scroll" data-target="#myScrollspy">

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="container">




        <div class="container-rules-wrap">
            <div class="tab-h-content sexy-football-topic">
                <div class="pull-left"><span>กติกาและเงื่อนไขการเล่นเกม</span></div>
                <a href="/gallery.php" class="pull-right more-btn">เล่นเกม</a>
            </div>
            <div class="row">
                <div class="col-sm-3 ">
                    <nav class="box-left-menu" id="myScrollspy">
                        <ul class="nav nav-pills nav-stacked" data-spy="affix" data-offset-top="100">
                            <li class="active"><a href="#section1">กติกาและเงื่อนไข</a></li>
                            <li class=""><a href="#section2">Sgoal และ Scoin คืออะไร</a></li>
                            <li class=""><a href="#section3">การเล่นเกมทายผล</a></li>
                            <li class=""><a href="#section4">คอมโบรายวัน คืออะไร</a></li>
                            <li class=""><a href="#section5">คอมโบสะสม คืออะไร</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-sm-9">
                    <div class="container-rules-detail">
                        <div class="bs-docs-section container-rule">
                            <h1 class="page-header" id="section1">กติกาและเงื่อนไข</h1>
                            <ul>
                                <li>ผู้เล่นต้องสมัครเล่นเกมผ่าน ID Facebook</li>
                                <li>กรณีที่สมัครเล่นแบบปกติ ผู้เล่นต้องทำการยืนยันตัวตนผ่าน E-mail
                                    ก่อนจึงจะสามารถเล่นเกมทายผลได้
                                </li>
                                <li>ผู้เล่นที่ใช้ ID Facebook สมัครเล่นเกม จะต้องเป็น ID จริงของตัวเองเท่านั้น
                                    หากตรวจพบว่ามีการสร้าง ID ปลอมจะทำการตัดสิทธิ์
                                </li>
                                <li>ผู้เล่นที่สามารถรับรางวัลได้ ID Facebook ต้องมีอายุเกิน 3 เดือนขึ้นไป</li>
                                <li> ทางเว็บไซต์จะทำการสรุปผู้ที่ได้รับรางวัลทุกวันที่ 16 และ 1 ของทุกเดือน</li>
                                <li> คะแนนสะสมแต่ละประเภทจะปรับเหลือ 0 เพื่อเริ่มต้นใหม่ทุกวันที่ 16 และ 1 ของเดือน</li>
                                <li> ผู้ที่ได้รับรางวัลทุกรางวัลต้องทำการยืนยันการขอรับรางวัลภายในวันที่ 2-3</li>
                                <li> เกมทายผลสามารถเล่นได้ทุกคู่ที่มีการเปิดอัตราต่อรอง</li>
                                <li>คำตัดสินของทางเว็บ ถือเป็นที่สิ้นสุด</li>
                            </ul>
                        </div>
                        <div class="bs-docs-section container-rule">
                            <h1 class="page-header" id="section2">Sgoal และ Scoin คืออะไร</h1>
                            <h3>Sgoal </h3>
                            <ul>
                                <li>เหรียญที่ได้มาจากการเล่นเกมทายผล</li>
                                <li>การแลกคอมโบรายวัน</li>
                                <li>การแลกคอมโบสะสม</li>
                                <li>Sgoal ที่ได้มาจะใช้จัดอับดับแข่งขันแต่ละเดือน</li>
                                <li>Sgoal สามารถใช้แลกรางวัลของแต่ละเดือน</li>
                                <li>Sgoal จะเริ่มต้นที่ 0</li>
                                <li>Sgoal จะติดลบเมื่อผู้เล่นทายผลผิด</li>
                                <li>Sgoal ติดลบสามารถรีเช็ตให้เป็น 0 ได้ ทุกๆ 15 วัน</li>
                            </ul>
                            <h3>Scoin</h3>
                            <ul>
                                <li>เหรียญที่ใช้เล่นเกมทายผล</li>
                                <li>ได้จากการ login ในแต่ละวัน</li>
                                <li>สมัครสมาชิกเล่นเกมครั้งแรกจะได้รับ 5,000 Scoin</li>
                                <li>Scoin สามารถใช้ Sgoal แลกได้ โดย 1 Sgoal = 50 Scoin</li>
                            </ul>
                        </div>
                        <div class="bs-docs-section container-rule">
                            <h1 class="page-header" id="section3">การเล่นเกมทายผล</h1>
                            <div class="alert alert-info" role="alert"><strong>Heads up!</strong>
                                ** ใช้ Scoin ในการเล่นเกมทายผล โดยจำนวน Scoin ที่ใช้แบ่งเป็น 5 ช่วงดังนี้ 200 400 600
                                800
                                1200
                                ตามลำดับ **
                            </div>
                            <h3>การจัดอันดับคะแนนมี 2 แบบ</h3>
                            <ul>
                                <li>W = win, w = winhaif, L = lose, l = losehaif, D = draw</li>
                                <li>
                                    **ไม่สามารถแก้ไขเปลี่ยนทีมที่เลือกเล่นในแต่ละวันได้ควรตรวจสอบให้มั่นใจก่อนยืนยันการเล่นทุกครั้ง**
                                </li>
                                <li>Ranking และ Sgoal</li>
                                <li>ันดับแต่ละแบบประกอบด้วย อันดับทั้งหมด, อันดับ Now (15 วัน), อันดับ Last champ (30
                                    วัน)
                                </li>
                            </ul>
                            <h3>1. อันดับความแม่นยำ Ranking</h3>
                            <ul>
                                <li>ผลชนะเต็ม (W) 100% ความแม่น</li>
                                <li>ผลชนะครึ่ง (w) 100% ความแม่น</li>
                                <li>ผลเสมอ (D) 0% ความแม่น</li>
                                <li>ผลแพ้ครึ่ง (l) -100% ความแม่น</li>
                                <li>ผลแพ้เต็ม (L) -100% ความแม่น</li>
                                <li>ตัวอย่างการนับความแม่น เล่น 3 คู่ ชนะ 2 แพ้ 1 เท่ากับ 66.66 ความแม่นยำ</li>
                                <li>หากคะแนนความแม่นยำเท่ากันจะให้ความสำคัญกับจำนวนคู่เล่นที่มากกว่า</li>
                            </ul>
                            <h3>อันดับ Sgoal</h3>
                            <ul>
                                <li>คำนวน Sgoal จาก Sgoal = Scoin x ค่าน้ำ</li>
                                <li>**กรณี win (ตัวอย่าง เล่น 800 Scoin ค่าน้ำ 0.90 จะได้รับ Sgoal 720)</li>
                                <li>**กรณี lose ค่า Sgoal จะติดลบ (ตัวอย่าง เล่น 800 Scoin ค่าน้ำ 0.90 เสีย Sgoal 720)
                                </li>
                                <li>ผลชนะเต็ม (W) ตัวคูณค่าน้ำจะคิดเต็ม (0.90)</li>
                                <li>ผลชนะครึ่ง (w) ตัวคูณค่าน้ำจะคิดครึ่ง (0.45)</li>
                                <li> ผลเสมอ (D) ไม่นำมาคิด</li>
                                <li>ผลแพ้ครึ่ง (l) ตัวคูณค่าน้ำจะคิดครึ่ง (-0.45)</li>
                                <li> ผลแพ้เต็ม (L) ตัวคูณค่าน้ำจะคิดเต็ม (-0.90)</li>
                                <li> หากคะแนนความแม่นยำเท่ากันจะให้ความสำคัญกับจำนวนคู่เล่นที่มากกว่า</li>
                            </ul>
                        </div>
                        <div class="bs-docs-section container-rule">
                            <h1 class="page-header" id="section4">คอมโบรายวัน คืออะไร</h1>
                            <ul>
                                <li>คอมโบรายวันหรือ Step มีสองแบบคือ win ทั้งหมด และ lose
                                    ทั้งหมดของคู่บอลที่เล่นในวันนั้น
                                    โดยเริ่มต้น 3 คู่ขึ้นไป
                                </li>
                                <li>รางวัลที่ได้จากคอมโบรายวันคือ Sgoal</li>
                                <li> โดยจะคิดรางวัลให้เฉพาะคู่ที่ win และ lose เท่านั้น กรณี draw
                                    จะไม่คิดรางวัลให้แต่ยังอยู่ในเงื่อนไขการได้คอมโบ
                                </li>
                                <li> ตัวอย่าง เล่น 3 คู่ win 2 draw 1 คู่ที่นำมาคิดคอมโบเฉพาะคู่ที่ win (3 คู่ เล่นคู่ละ
                                    400
                                    win
                                    2
                                    ได้รับ Sgoal 800 ตามค่าน้ำที่เล่น)
                                </li>
                                <li> คอมโบจะตัดรอบเวลา 12.00 น. ของทุกวัน</li>
                                <li> รางวัลที่ได้จะส่งเข้า statement ทันที</li>
                            </ul>
                        </div>
                        <div class="bs-docs-section container-rule">
                            <h1 class="page-header" id="section5">คอมโบสะสม คืออะไร</h1>
                            <ul>
                                <li>รางวัลใหญ่มีโอกาศได้รับ Sgoal สูงสุดถึง 300,000 Sgoal</li>
                                <li> คอมโบสะสมหรือ Step ต่อเนื่อง มีสองแบบคือ win ทั้งหมด และ lose
                                    ทั้งหมดของคู่บอลที่เล่น
                                </li>
                                <li> รางวัลที่ได้จากคอมโบสะสมคือ Sgoal</li>
                                <li> ช่วงของคอมโบสะสมที่แจก Sgoal</li>
                                <li>
                                    <ul>
                                        <li> 4 คู่ต่อเนื่อง 5000 Sgoal</li>
                                        <li> 7 คู่ต่อเนื่อง 10,000 Sgoal</li>
                                        <li> 10 คู่ต่อเนื่อง 50,000 Sgoal</li>
                                        <li> 15 คู่ต่อเนื่อง 100,000 Sgoal</li>
                                        <li> 20 คู่ต่อเนื่อง 300,000 Sgoal</li>
                                    </ul>
                                </li>
                                <li> โดยจะคิดรางวัลให้เฉพาะคู่ที่ win และ lose เท่านั้น กรณี draw
                                    จะไม่คิดรางวัลให้แต่ยังอยู่ในเงื่อนไขการได้คอมโบสะสม
                                </li>
                                <li> ตัวอย่าง เล่น 3 คู่ win 2 draw 1 คู่ที่นำมาคิดคอมโบสะสมเฉพาะคู่ที่ win</li>
                                <li> คอมโบสะสมจะตัดรอบเวลา 12.00 น. ของทุกวัน</li>
                                <li> ช่วงที่กดแลก คอมโบสะสมได่คือ 12.00 - 15.00 ของทุกวัน</li>
                                <li> เมื่อกดแลกคอมโบแล้ว จะเริ่มนับหนึ่งใหม่เพื่อสะสมคอมโบ</li>

                            </ul>
                        </div>

                    </div>
                </div>

            </div>
        </div>

</div>
<?php //include 'footer.php';
include 'view/index/footer.php';
?>

</body>
</html>
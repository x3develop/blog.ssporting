<?php
$match_id = (isset($_REQUEST['match_id']) ? $_REQUEST['match_id'] : null);
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playBet.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playChannelMatch.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playReviewMatch.php";
$playBet = new playBet();
$playMatch = new playMatch();
$playReviewMatch = new playReviewMatch();
$playChannelMatch = new playChannelMatch();
$match = $playMatch->getFirstMathById($match_id);
$match = $match[0];
$matchResultsHome = $playMatch->getListMatchResults($match->team_home, 5);
$matchResultsAway = $playMatch->getListMatchResults($match->team_away, 5);
$reviewMatchAwayByMatchId = $playReviewMatch->getReviewAndUserMatchAwayByMatchId($match_id);
$reviewMatchHomeByMatchId = $playReviewMatch->getReviewAndUserMatchHomeByMatchId($match_id);
$matchYesterdayTodayTomorrow = $playMatch->getByYesterdayTodayTomorrow();

if (intval(date("j")) == intval(date("j", (strtotime('-12 hours', strtotime($match->time_match)))))) {
    $selelct = "Today";
} else if (intval(date("j")) > intval(date("j", (strtotime('-12 hours', strtotime($match->time_match)))))) {
    $selelct = "Yesterday";
} else if (intval(date("j")) < intval(date("j", (strtotime('-12 hours', strtotime($match->time_match)))))) {
    $selelct = "Tomorrow";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ngoal | ทีเด็ด ทรรศนะ</title>
    <link rel="shortcut icon" type="images/x-icon" href="/images/favicon.ico.png">
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="/css/style-new.css">
    <link rel="stylesheet" href="/css/doce.css">
    <link rel="stylesheet" href="/css/livescore.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/fixtures.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/view-all.css">
    <link rel="stylesheet" href="/css/game-redesign.css">
    <link rel="stylesheet" href="/css/review-new-style.css">
    <link rel="stylesheet" href="nanoscrollbar/nanoscrollbar.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/js/readmore.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="js/jquery-timeago/jquery.timeago.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="/js/moment.js"></script>

    <?php include 'view/index/googletagmanager.php'; ?>
</head>
<body style="background-color: #f6f6f6;">
<a style="opacity: 0.7;" id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button"><span
            class="glyphicon glyphicon-chevron-up"></span></a>
<!--index menu-->
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/view/index/menu-all.php'; ?>
<div class="wrap-review">
    <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/mockup/sidebar-feed.php' ?>
    <div class="container-fluid" id="main">

        <div class="sidebar-left-ranking">
            <div class="sidebar-ranking-review">
                <div class="box-match-sidebar">
                    <div class="text-title">
                        <h4 style="font-size: 16px;">คู่บอล</h4>
                        <div class="dropdown pull-right">
                            <button class="btn btn-default btn-xs dropdown-toggle" type="button" id="dropdownMenu1"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <?php echo $selelct; ?>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="#" onclick="changeContentMatch('Tomorrow')">Tomorrow</a></li>
                                <li><a href="#" onclick="changeContentMatch('Today')">Today</a></li>
                                <li><a href="#" onclick="changeContentMatch('Yesterday')">Yesterday</a></li>
                            </ul>
                        </div>
                    </div>
                    <?php foreach ($matchYesterdayTodayTomorrow as $key => $value) {
                        if (intval(date("j")) == intval(date("j", (strtotime('-12 hours', strtotime($value->time_match)))))) {
                            $status = "Today";
                        } else if (intval(date("j")) > intval(date("j", (strtotime('-12 hours', strtotime($value->time_match)))))) {
                            $status = "Yesterday";
                        } else if (intval(date("j")) < intval(date("j", (strtotime('-12 hours', strtotime($value->time_match)))))) {
                            $status = "Tomorrow";
                        }
                        ?>
                        <a href="/review-infomation.php?match_id=<?php echo $value->match_id ?>" style="color: #000000;"
                           class="<?php echo $status; ?> <?php echo(($status == $selelct) ? '' : 'hide'); ?> content-match <?php echo(($value->match_id == $match_id) ? 'active' : '') ?>">
                            <div class="content-match-home">
                                <img src="<?php echo $value->teamHomePath; ?>">
                                <span><?php echo $value->teamHomeEn; ?></span>
                            </div>
                            <div class="content-match-hdp">
                                <strong><?php echo $value->handicap; ?></strong>
                                <div class="box-water-text">
                                    <div><?php echo((!empty($value->home_water_bill)) ? number_format($value->home_water_bill, 2) : 0); ?></div>
                                    <div>|</div>
                                    <div><?php echo((!empty($value->away_water_bill)) ? number_format($value->away_water_bill, 2) : 0); ?></div>
                                </div>
                                <span class="time-match"><?php echo date("H:i", (strtotime($value->time_match))) ?></span>
                            </div>
                            <div class="content-match-away">
                                <img src="<?php echo $value->teamAwayPath; ?>">
                                <span><?php echo $value->teamAwayEn; ?></span>
                            </div>
                        </a>
                    <?php } ?>
                </div>
                <?php include '../mockup/review-sidebar-left.php' ?>
            </div>
        </div>
        <div class="main-review">
            <div class="content-review-information">
                <div class="container-review">
                    <div class="text-title">
                        <h4>ทีเด็ด ทรรศนะ</h4>
                        <div class="pull-right">
                            <div class="inline-box" style="margin-top: 5px;">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                <?php echo date("j M Y", (strtotime($match->time_match) - (60 * 60))) ?>
                            </div>
                            <div class="hamburger-review inline-box inline-box-middle" id="hamburger"
                                 onclick="toggleSidenav();"></div>
                        </div>

                    </div>
                    <div class="content-review">
                        <div class="top-section">
                            <a class="link_wrap" href="/games?mid=<?php echo $match_id; ?>"></a>
                            <div class="home-section">
                                <div class="vote-home-section">
                                    <?php if ($match->status == "fullTime") { ?>
                                        <?php echo $match->home_end_time_score ?>
                                    <?php } else { ?>
                                        <a class="link_wrap" href="/games?mid=<?php echo $match_id; ?>"></a>
                                        VOTE
                                    <?php } ?>
                                </div>
                                <div class="content-home-section text-right">
                                    <div class="topic-event"><?php echo $match->name; ?></div>
                                    <div class="detail-team-home">
                                        <div class="detail-team-home-state">
                                            <h4><?php echo $match->teamHomeEn; ?></h4>
                                            <div class="box-progress">
                                                <span>Vote <?= number_format($percentBet['pHome']); ?>%</span>
                                                <div class="progress">
                                                    <div class="progress-bar pull-right progress-bar-striped active"
                                                         role="progressbar"
                                                         aria-valuenow="<?= number_format($percentBet['pHome'], 2); ?>"
                                                         aria-valuemin="0" aria-valuemax="100"
                                                         style="width: <?= number_format($percentBet['pHome'], 2); ?>%">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="detail-team-home-logo">
                                            <img src="<?= (!empty($match->teamHomePath)?$match->teamHomePath:'/images/team/team_default_64x64.png'); ?>">
                                        </div>
                                    </div>
                                    <ul class="list-state-match pull-right">
                                        <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                        <?php if (!empty($matchResultsHome)) { ?>
                                            <?php foreach (array_reverse($matchResultsHome) as $keyResults => $valueResults) { ?>
                                                <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                    <li>
                                                        <div title="<?= $valueResults->time_match ?>" class="draw-st"
                                                             aria-hidden="true"></div>
                                                    </li>
                                                <?php } elseif ($valueResults->team_home == $value->team_home && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                    <li>
                                                        <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                             aria-hidden="true"></div>
                                                    </li>
                                                <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                    <li>
                                                        <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                             aria-hidden="true"></div>
                                                    </li>
                                                <?php } else { ?>
                                                    <li>
                                                        <div title="<?= $valueResults->time_match ?>" class="loss-st"
                                                             aria-hidden="true"></div>
                                                    </li>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                        <li class="text-match">Last 5 matches</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="hdp-section text-center">
                                <a class="link_wrap" href="/games?mid=<?php echo $match_id; ?>"></a>
                                <strong>HDP</strong>
                                <h2><?php echo $match->handicap; ?></h2>
                                <div class="box-water-text">
                                    <div><?php echo number_format($match->home_water_bill, 2); ?></div>
                                    <div>|</div>
                                    <div><?php echo number_format($match->away_water_bill, 2); ?></div>
                                </div>
                                <span class="time-match"><?php echo date("H:i", (strtotime($match->time_match) - (60 * 60))) ?></span>
                            </div>
                            <div class="away-section">
                                <div class="content-home-section content-away-section pull-left">
                                    <div class="topic-live-on">
                                        <?php if (!empty($matchPlayChannelMatch)) { ?>
                                            <?php foreach ($matchPlayChannelMatch as $keyC => $valueC) { ?>
                                                <img src="<?php echo $valueC->channel_path; ?>">
                                                <?php if ($keyC != count($matchPlayChannelMatch)) { ?>
                                                    <div>|</div>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                    <div class="detail-team-home detail-team-away">
                                        <div class="detail-team-home-logo">
                                            <img src="<?php echo (!empty($match->teamAwayPath)?$match->teamAwayPath:'/images/team/team_default_64x64.png'); ?>">
                                        </div>
                                        <div class="detail-team-home-state detail-team-away-state">
                                            <h4><?php echo $match->teamAwayEn; ?></h4>
                                            <div class="box-progress">
                                                <div class="progress">
                                                    <div class="progress-bar pull-left progress-bar-danger active"
                                                         role="progressbar"
                                                         aria-valuenow="<?= number_format($percentBet['pAway'], 2); ?>"
                                                         aria-valuemin="0" aria-valuemax="100"
                                                         style="width: <?= number_format($percentBet['pAway'], 2); ?>%">

                                                    </div>
                                                </div>
                                                <span>Vote <?= number_format($percentBet['pAway']); ?>%</span>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="list-state-match pull-left">
                                        <li class="text-match">Last 5 matches</li>
                                        <li><i class="fa fa-caret-left" aria-hidden="true"></i></li>
                                        <?php if (!empty($matchResultsAway)) { ?>
                                            <?php foreach ($matchResultsAway as $keyResults => $valueResults) { ?>
                                                <?php if ($valueResults->scoreHome == $valueResults->scoreAway) { ?>
                                                    <li>
                                                        <div title="<?= $valueResults->time_match ?>" class="draw-st"
                                                             aria-hidden="true"></div>
                                                    </li>
                                                <?php } elseif ($valueResults->team_home == $value->team_away && $valueResults->scoreHome > $valueResults->scoreAway) { ?>
                                                    <li>
                                                        <div title="<?= $valueResults->time_match ?>" class="win-ste"
                                                             aria-hidden="true"></div>
                                                    </li>
                                                <?php } elseif ($valueResults->team_away == $value->team_away && $valueResults->scoreAway > $valueResults->scoreHome) { ?>
                                                    <li>
                                                        <div title="<?= $valueResults->time_match ?>" class="win-st"
                                                             aria-hidden="true"></div>
                                                    </li>
                                                <?php } else { ?>
                                                    <li>
                                                        <div title="<?= $valueResults->time_match ?>" class="loss-st"
                                                             aria-hidden="true"></div>
                                                    </li>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <div class="vote-away-section">
                                    <?php if ($match->status == "fullTime") { ?>
                                        <?php echo $match->away_end_time_score; ?>
                                    <?php } else { ?>
                                        <a class="link_wrap" href="/games?mid=<?php echo $match_id; ?>"></a>
                                        VOTE
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="review-section">
                            <div class="box-review-section">
                                <div class="home-review-section">
                                    <?php foreach ($reviewMatchHomeByMatchId as $key => $value) { ?>
                                        <div class="box-content-review">
                                            <div class="top-content-review pull-left">
                                                <div>
                                                    <img style="<?php echo((!empty($value['path'])) ? '' : 'width: 40px;'); ?>"
                                                         src="<?= ((!empty($value['path'])) ? $value['path'] : 'https://graph.facebook.com/v2.8/' . $value['user_fb_uid'] . '/picture'); ?>">
                                                </div>
                                                <div>
                                                    <h5><?= ((!empty($value['name'])) ? $value['name'] : $value['user_name']); ?></h5>
                                                    <?php if ($value['team'] == "home") { ?>
                                                        <img src="<?= $value['homepath'] ?>">
                                                        <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                    <?php } elseif ($value['team'] == "away") { ?>
                                                        <img src="<?= $value['awaypath'] ?>">
                                                        <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="winRate-box">
                                                    <div class="bg-winRate-box">
                                                        <h3 class="user-review-<?php echo $value['user_review_id']; ?>">
                                                            %</h3>
                                                        <p>Win Rate</p>
                                                    </div>
                                                    <div class="bg-day-box">
                                                        <h3 class="user-review-day-<?php echo $value['user_review_id']; ?>"><?php echo date("t"); ?></h3>
                                                        <p>Days</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="full-review-caption-information">
                                                <p><?= $value['review_text'] ?></p>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="away-review-section">
                                    <?php foreach ($reviewMatchAwayByMatchId as $key => $value) { ?>
                                        <div class="box-content-review">
                                            <div class="top-content-review pull-left">
                                                <div>
                                                    <img style="<?php echo((!empty($value['path'])) ? '' : 'width: 40px;'); ?>"
                                                         src="<?= ((!empty($value['path'])) ? $value['path'] : 'https://graph.facebook.com/v2.8/' . $value['user_fb_uid'] . '/picture'); ?>">
                                                </div>
                                                <div>
                                                    <h5><?= ((!empty($value['name'])) ? $value['name'] : $value['user_name']); ?></h5>
                                                    <?php if ($value['team'] == "home") { ?>
                                                        <img src="<?= $value['homepath'] ?>">
                                                        <span class="name-team-slide-top">@ <?= $value['homename'] ?></span>
                                                    <?php } elseif ($value['team'] == "away") { ?>
                                                        <img src="<?= $value['awaypath'] ?>">
                                                        <span class="name-team-slide-top">@ <?= $value['awayname'] ?></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="winRate-box">
                                                    <div class="bg-winRate-box">
                                                        <h3 class="user-review-<?php echo $value['user_review_id']; ?>">
                                                            %</h3>
                                                        <p>Win Rate</p>
                                                    </div>
                                                    <div class="bg-day-box">
                                                        <h3 class="user-review-day-<?php echo $value['user_review_id']; ?>"><?php echo date("t"); ?></h3>
                                                        <p>Days</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="full-review-caption-information">
                                                <p><?= $value['review_text'] ?></p>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function changeContentMatch(status) {
        $('.content-match').addClass('hide');
        $('.' + status).removeClass('hide');
        $("button#dropdownMenu1").html(status + "<span class='caret'></span>")
    }
    function openNav() {
        document.getElementById("mySidenav").style.marginRight = "0";
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main").style.marginRight = "250px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.margin = "0 auto";
    }
</script>
<script>
    $(document).ready(function () {
        $.ajax({
            url: "/json/checkWinRateReview.json",
            method: "POST",
            dataType: "JSON"
        }).done(function (response) {
            $.each(response, function (key, val) {
                $("h3.user-review-" + val.id).html(parseInt(val.winRate) + "%");
                // console.log(key+" :: "+val)
            })
        });
    });
    function myFunction() {
        var dots = document.getElementById("dots");
        var moreText = document.getElementById("more");
        var btnText = document.getElementById("myBtn");

        if (dots.style.display === "none") {
            dots.style.display = "inline";
            btnText.innerHTML = "Read more";
            moreText.style.display = "none";
        } else {
            dots.style.display = "none";
            btnText.innerHTML = "Read less";
            moreText.style.display = "inline";
        }
    }
</script>
<!--scroll body to 0px on click-->
<script>
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 1200);
        return false;
    });

    $('#back-to-top').tooltip('show');
</script>

<script>
    function toggleSidenav() {
        document.body.classList.toggle('sidenav-active');
    }
</script>
</body>
</html>
<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/29/2017
 * Time: 15:17
 */
session_start();
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playComment.php";
$playComment=new playComment();
$comment_match=$_GET['comment_match'];
$comment_match['fb_uid']=$_SESSION['login']['fb_uid'];
$comment_match['name']=$_SESSION['login']['name'];
$comment_match['updated_at']=date('Y-m-d H:i:s');
$comment_match['id']=$playComment->addComment($comment_match['match_id'],$comment_match['team'],$comment_match['comment'],$_SESSION['login']['fb_uid'],$comment_match['comment_match_id']);
echo json_encode($comment_match);
?>
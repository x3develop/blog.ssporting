<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/6/2017
 * Time: 16:39
 */
if (!isset($_SESSION)) {
    session_start();
}
//require_once $_SERVER["DOCUMENT_ROOT"].'/model/DBPDO.php';
require_once $_SERVER["DOCUMENT_ROOT"]."/model/playUser.php";
$playUser = new playUser();
$dateNow = new DateTime();
$dateLast = new DateTime();
$coinDaily=500;
$db = new DBPDO();
$dbh = $db->connect2();
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if(isset($_SESSION['login'])) {
    $user=$playUser->checkUserPlay($_SESSION['login']['id']);
    if(!empty($user['last_login'])) {
        $dateLast->setTimestamp(strtotime($user['last_login']));
        $dateNow->setTime(0, 0, 0);
        $dateLast->setTime(0, 0, 0);
        $dayLogin = $dateNow->diff($dateLast)->days;
    }else{
        $dateLast->setTimestamp(strtotime($user['created_at']));
        $dateNow->setTime(0, 0, 0);
        $dateLast->setTime(0, 0, 0);
        $dayLogin = $dateNow->diff($dateLast)->days;
    }

    if($dayLogin==1){
        $sql="update `user` set coupon=5 where id=".$user['id'];
        $sth = $dbh->prepare($sql)->execute();
//        $_SESSION['login']['coupon']=5;
        $sql="update `user` set last_login='".$dateNow->format('Y-m-d')."',combo_login=".($user['combo_login']+1)." where id=".$user['id'];
        $sth = $dbh->prepare($sql)->execute();
        $_SESSION['login']['combo_login']=($user['combo_login']+1);
    }else if($dayLogin>1){
        $sql="update `user` set coupon=5 where id=".$user['id'];
        $sth = $dbh->prepare($sql)->execute();
//        $_SESSION['login']['coupon']=5;
        $sql="update `user` set last_login='".$dateNow->format('Y-m-d')."',combo_login=1 where id=".$user['id'];
        $sth = $dbh->prepare($sql)->execute();
        $_SESSION['login']['combo_login']=1;
        $combo_login=0;
    }

    $sth = $dbh->prepare("select * from `log_income` where date_record='".$dateNow->format('Y-m-d')."' and user_id=" . $_SESSION['login']['id'] . " and income_derived='daily'");
    $sth->execute();
    $log_income = $sth->fetch();

    if(empty($log_income)){
        $user_id=$_SESSION['login']['id'];
        $befor_gold=$_SESSION['login']['gold'];
        $befor_coin=$_SESSION['login']['coin'];
        $befor_coupon=$_SESSION['login']['coupon'];
        $befor_lv=$_SESSION['login']['lv'];
        $befor_star=$_SESSION['login']['star'];
        $income_derived='daily';
        $income_type='coin';
        $income_format='plus';
        $after_gold=$befor_gold;

        if(($_SESSION['login']['combo_login']%7)==0 and $_SESSION['login']['combo_login']>=7){
            $after_coin = (intval($befor_coin) + intval(2*$coinDaily));
        }else {
            $after_coin = (intval($befor_coin) + intval($coinDaily));
        }

        $after_coupon=$befor_coupon;
        $after_lv=$_SESSION['login']['lv'];
        $after_star=$_SESSION['login']['star'];
        $comment="กดรับ ".($after_coin-$befor_coin)."coin วันที่ ".($_SESSION['login']['combo_login']);
        $sql = "INSERT INTO `log_income` (`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
        $sql.=" VALUES (" . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "',".$after_gold.",".$after_coin.",".$after_coupon.",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
        $sth = $dbh->prepare($sql)->execute();
        if($sth){
            $_SESSION['login']['coin']=$after_coin;
            $sql="update `user` set coin=".$after_coin." where id=".$user_id;
            $sth = $dbh->prepare($sql)->execute();
            if($sth){
                $_SESSION['login']['modal_gold']=false;
            }

            $sth = $dbh->prepare("select id from log_income where income_derived='daily' and user_id=".$user_id." and date_record='".$dateNow->format('Y-m-d')."'");
            $sth->execute();
            $log_income = $sth->fetch();

            if(!empty($log_income)){
                $_SESSION['login']['coin_daily']=true;
            }

        }
    }
    $db->close();

    $pathname = $_REQUEST['pathname'];

    if(isset($_REQUEST['pathname'])){
        header('Location: '.$_REQUEST['pathname']);
    }else {
        header('Location: /index.php');
    }
    exit();
}
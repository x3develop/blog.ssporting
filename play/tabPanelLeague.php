<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 12/4/2017
 * Time: 22:29
 */
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/HomeM.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playLeague.php";
$homem = new HomeM();
$id=(isset($_GET['id'])?$_GET['id']:null);
if(!empty($id) && $id==1) {
    $foreignNewsPlData = $homem->getNewsByCategory('premier league', 7);
}elseif(!empty($id) && $id==18) {
    $foreignNewsPlData = $homem->getNewsByCategory('la liga', 7);
}elseif(!empty($id) && $id==44) {
    $foreignNewsPlData = $homem->getNewsByCategory('bundesliga', 7);
}elseif(!empty($id) && $id==27) {
    $foreignNewsPlData = $homem->getNewsByCategory('serie a', 7);
}elseif(!empty($id) && $id==36) {
    $foreignNewsPlData = $homem->getNewsByCategory('league 1', 7);
}elseif(!empty($id) && $id==105) {
    $foreignNewsPlData = $homem->getNewsByCategory('ucl', 7);
}elseif(!empty($id) && $id==608) {
    $foreignNewsPlData = $homem->getNewsByCategory('europa', 7);
}

$playLeague=new playLeague();
$league=$playLeague->checkTeamByLeague($id);
$goalScorer=$playLeague->checkGoalScorerByLeague($id);
$matchLast=$playLeague->checkMatchLastByLeague($id);
?>

<!--<script>-->
<!--    $(document).ready(function () {-->
<!--        function refreshTime() {-->
<!--            console.log("refreshTime");-->
<!--            $.each($(".how-long"), function (k, v) {-->
<!--                if ($.isNumeric($(this).text())) {-->
<!--                    $(this).html(moment($(this).text(), "X").fromNow())-->
<!--                } else {-->
<!--                    $(this).html(moment($(this).text(), "YYYY-MM-DD HH:mm:ss").fromNow())-->
<!--                }-->
<!--            })-->
<!---->
<!--        }-->
<!--    }-->
<!--</script>-->

<div class="wrap-content-cols">
    <div class="wrap-box-results">
        <!--        ตารางคะแนน-->
        <div class="col-sm-4">
            <div class="box-results">
                <span class="title-box-results">ตารางคะแนน</span>
                <div class="wrap-height">
                    <div id="about" class="nano  bx2">
                        <div class="content">
                            <table class="table-point">
                                <tr>
                                    <th>Pos</th>
                                    <th>Club</th>
                                    <th>Pl</th>
                                    <th>GD</th>
                                    <th>Pts</th>
                                </tr>
                                <?php if(!empty($league)){ ?>
                                    <?php foreach ($league as $key => $rank) { ?>
                                        <tr>
                                            <td><?php echo $key+1; ?></td>
                                            <td>
                                                <img src="<?=((!empty($rank["path"]))?"".$rank["path"]:'/images/teams_clean/team_default_32x32.png') ; ?>">
                                                <?php echo $rank["name_en"]; ?>
                                            </td>
                                            <td><?php echo $rank["PI"]; ?></td>
                                            <td><?php echo $rank["GD"]; ?></td>
                                            <td><?php echo ((!empty($rank["totalPts"])?$rank["totalPts"]:'0')); ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--        ดาวซัลโว-->
        <div class="col-sm-3">
            <div class="box-results">
                <span class="title-box-results">ดาวซัลโว</span>
                <div class="wrap-height">
                    <div id="about" class="nano bx2">
                        <div class="content">
                            <div class="bx-player-records">
                                <!--                                                    <div class="h-records">Goals</div>-->
                                <div class="bx-top-records">
                                    <table>
                                        <?php if (!empty($goalScorer)) { ?>
                                            <tr>
<td></td>
                                                <td class="text-right image-name-topScore">
                                                    <img src="<?= ((!empty($goalScorer[0]['path']))?'' .$goalScorer[0]['path']:'/images/p1.png')?>">
                                                </td>
                                                <td class="image-logo-topScore">
                                                    <img src="<?=$playLeague->checkPathTeamPlayer($goalScorer[0]['id'],$id)?>">
                                                </td>
                                                <td><div class="name-top-records"><?= $goalScorer[0]['name']; ?></div></td>
                                                <td>
                                                    <div class="txt-point"><?= $goalScorer[0]['goal']; ?></div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                                <table class="table-list-othertop">
                                    <?php foreach ($goalScorer as $key=>$value) { ?>
                                        <?php if ($key!=0) { ?>
                                            <tr>
                                                <td><?php echo $key+1; ?></td>
                                                <td><img src="<?=$playLeague->checkPathTeamPlayer($value['id'],$id)?>"></td>
                                                <td>
                                                    <div class="name-list-othertop"><?php echo $value['name']; ?></div>
                                                </td>
                                                <td>
                                                    <span><?php echo $value['goal']; ?></span>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </table>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--        ตารางแข่งขัน-->
        <div class="col-sm-5">
            <div class="box-results">
                <span class="title-box-results">ตารางแข่งขัน</span>
                <div class="wrap-height">
                    <div id="about" class="nano bx2">
                        <div class="content">
                            <div>
                                <?php $data=""; ?>
                                <?php foreach ($matchLast as $okey => $fixtures) { ?>
                                <?php if($data!=date('d-M-Y', strtotime($fixtures['time_match']))){ ?>
                                <?php if($okey!=0){?>
                                    </table>
                                <?php } ?>
                                <div class="tab-date-played">
                                    <i class="fa fa-calendar"></i> <?php echo date('d-M-Y', strtotime($fixtures['time_match'])); ?>
                                </div>
                                <table class="table-live">
                                    <?php $data=date('d-M-Y', (strtotime($fixtures['time_match'])-(60*60))); ?>
                                    <?php } ?>
                                    <tr>
                                        <td>
                                            <?php echo $fixtures['nameTeamHome']; ?>
                                            <img src="<?=((!empty($fixtures['pathTeamHome']))?"".$fixtures['pathTeamHome']:'/images/teams_clean/team_default_32x32.png') ; ?>">
                                        </td>
                                        <td>
                                            <?php if($fixtures['status']=="fullTime"){ ?>
                                                <strong><?php echo $fixtures['home_end_time_score']?>-<?php echo $fixtures['away_end_time_score']?></strong>
                                            <?php }else { ?>
                                                <strong><?php echo date('H:i', (strtotime($fixtures['time_match'])-(60*60))); ?></strong>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <img src="<?=((!empty($fixtures['pathTeamAway']))?"".$fixtures['pathTeamAway']:'/images/teams_clean/team_default_32x32.png') ; ?>">
                                            <?php echo $fixtures['nameTeamAway']; ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div style="clear:both;"></div>
    </div>
</div>
<div class="wrap-box-content-news">
    <div class="box-other-news">
        <div class="rows">
            <?php for ($i = 0; $i < 3; $i++) { ?>
                <?php if (array_key_exists($i, $foreignNewsPlData)) { ?>
                    <div class="col-sm-4" style="border: 0px;">
                        <div class="thumbnail">
                            <div class="tab-color"></div>
                            <div class="times-content"><i class="fa fa-clock-o"></i>
                                <time
                                    class="how-long-<?=$id?>"><?php echo $foreignNewsPlData[$i]->createDatetime; ?></time>
                            </div>
                            <div
                                class="title-owl-carousel"><?php echo $foreignNewsPlData[$i]->titleTh; ?></div>
                            <div class="box-videos img-other-news">
                                <div class="hilight-cover"></div>
                                <a href="/articles?newsid=<?php echo $foreignNewsPlData[$i]->newsid; ?>">
                                    <img src="<?php echo $foreignNewsPlData[$i]->imageLink; ?>">
                                </a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <div class="box-other-news">
        <div class="rows">
            <?php for ($i = 3; $i < count($foreignNewsPlData); $i++) { ?>
                <?php if (array_key_exists($i, $foreignNewsPlData)) { ?>
                    <div class="col-sm-3">
                        <div class="thumbnail">
                            <div class="tab-color"></div>
                            <div class="times-content"><i class="fa fa-clock-o"></i>
                                <time class="how-long-<?=$id?>"><?php echo $foreignNewsPlData[$i]->createDatetime; ?></time>
                            </div>
                            <div
                                class="title-owl-carousel"><?php echo $foreignNewsPlData[$i]->titleTh; ?></div>
                            <div class="box-videos">
                                <div class="hilight-cover"></div>
                                <a href="/articles?newsid=<?php echo $foreignNewsPlData[$i]->newsid; ?>">
                                    <img src="<?php echo $foreignNewsPlData[$i]->imageLink; ?>">
                                </a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <div style="clear: both;"></div>
</div>



<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/6/2017
 * Time: 16:39
 */
if (!isset($_SESSION)) {
    session_start();
}
//require_once $_SERVER["DOCUMENT_ROOT"].'/model/DBPDO.php';
require_once $_SERVER["DOCUMENT_ROOT"]."/model/playUser.php";
$playUser = new playUser();
$dateNow = new DateTime();
$dateLast = new DateTime();

$db = new DBPDO();
$dbh = $db->connect2();
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sumBetStep=0;

if(isset($_SESSION['login'])) {
    $sth = $dbh->prepare("select bet.team,(bet.use_bet_coin*(gamble.home_water_bill-1)) as sumHomeBet,(bet.use_bet_coin*(gamble.away_water_bill-1)) as sumAwayBet from `match` 
inner join bet on bet.match_id=`match`.id
left join gamble on gamble.match_id=`match`.id
where DATE(DATE_ADD(`match`.time_match, INTERVAL -12 HOUR))=(CURDATE()-INTERVAL 1 DAY) and bet.user_id=" . $_SESSION['login']['id'] . " and bet.`status`='cheap'
order by `match`.time_match asc");
    $sth->execute();
    $listBetStep = $sth->fetchAll();

    if(!empty($listBetStep)){
        foreach ($listBetStep as $key=>$value){
            if($value['team']=="away"){
                $sumBetStep=$sumBetStep+$value['sumAwayBet'];
            }else if($value['team']=="home"){
                $sumBetStep=$sumBetStep+$value['sumHomeBet'];
            }
        }
    }

    $sth = $dbh->prepare("select * from `log_income` where date_record='".$dateNow->format('Y-m-d')."' and user_id=" . $_SESSION['login']['id'] . " and income_derived='step'");
    $sth->execute();
    $log_income = $sth->fetch();

    if(empty($log_income) and $sumBetStep>0){
        $user_id=$_SESSION['login']['id'];
        $befor_gold=$_SESSION['login']['gold'];
        $befor_coin=$_SESSION['login']['coin'];
        $befor_coupon=$_SESSION['login']['coupon'];
        $befor_lv=$_SESSION['login']['lv'];
        $befor_star=$_SESSION['login']['star'];
        $income_derived='step';
        $income_type='gold';
        $income_format='plus';
        $after_gold=($befor_gold+$sumBetStep);
        $after_coin=$befor_coin;
        $after_coupon=$befor_coupon;
        $after_lv=$_SESSION['login']['lv'];
        $after_star=$_SESSION['login']['star'];
        $comment="ได้กดรับ ".($after_gold-$befor_gold)."gold จากการ แทง Step ".count($listBetStep)."คู่";
        $sql = "INSERT INTO `log_income` (`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
        $sql.=" VALUES (" . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "',".$after_gold.",".$after_coin.",".$after_coupon.",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
        $sth = $dbh->prepare($sql)->execute();
        if($sth){
            $_SESSION['login']['gold']=$after_gold;
            $sql="update `user` set gold=".$after_gold." where id=".$user_id;
            $sth = $dbh->prepare($sql)->execute();
        }
    }
    $db->close();
    header('Location: /profile.php');
    exit();
}
<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/28/2017
 * Time: 15:51
 */
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playUser.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/UserFeed.php";
$playUser = new playUser();
$userFeed=new UserFeed();
$fb_uid = $_REQUEST['id'];
$email = $_REQUEST['email'];
$name = $_REQUEST['name'];
$pathname = $_REQUEST['pathname'];

if (!isset($_SESSION)) {
    session_start();
}

$user=$playUser->checkUserPlayFb($fb_uid);
if(!empty($user)){
    //addFeed
    $userFeed->addUserFeed($user['id'],'login','Login '.$user['name'].' By Ip:'.$userFeed->getUserIpAddr());
    $user=$playUser->updateUserPlay($fb_uid,$name,$email);
}else{
    //addFeed
    $userFeed->addUserFeed($user['id'],'new','Create By Ip:'.$userFeed->getUserIpAddr());
    $user=$playUser->addUserPlay($fb_uid,$name,$email);
}
$user=$playUser->checkUserPlayFb($fb_uid);
$_SESSION['login'] = [
    'fb_uid' => $fb_uid,
    'name' => $user['name'],
    'id' => $user['id'],
    'username' => $user['username'],
    'email' => $user['email'],
    'coin' => $user['coin'],
    'gold' => $user['gold'],
    'path' => $user['path'],
    'coupon' => $user['coupon'],
    'combo_login' => $user['combo_login'],
    'accuracy_at' => $user['accuracy_at'],
    'lv' => $user['lv'],
    'star' => $user['star'],
    'modal_gold' =>false,
    'coin_daily' =>false,
    'bet'=>null
];

//exit();

if(isset($_REQUEST['pathname'])){
    header('Location: '.$_REQUEST['pathname']);
}else {
    header('Location: /index.php');
}
    exit();
?>
<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 12/13/2017
 * Time: 17:14
 */
session_start();
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playBet.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playComment.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playUser.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playLogIncome.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/UserFeed.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playMatch.php";
$playBet=new playBet();
$playUser=new playUser();
$userFeed=new UserFeed();
$playMatch=new playMatch();
$playComment=new playComment();
$playLogIncome=new playLogIncome();
$betId="";$commentId="";$alert="";$message="";$comment=array();
//not Use coupon
if(isset($_SESSION['login']['fb_uid'])){
    $dataPlayUser=$playUser->checkUserPlayFb($_SESSION['login']['fb_uid']);
    $_SESSION['login']['fb_uid']=$dataPlayUser['fb_uid'];
    $_SESSION['login']['name']=$dataPlayUser['name'];
    $_SESSION['login']['id']=$dataPlayUser['id'];
    $_SESSION['login']['username']=$dataPlayUser['username'];
    $_SESSION['login']['email']=$dataPlayUser['email'];
    $_SESSION['login']['coin']=$dataPlayUser['coin'];
    $_SESSION['login']['gold']=$dataPlayUser['gold'];
    $_SESSION['login']['path']=$dataPlayUser['path'];
    $_SESSION['login']['coupon']=$dataPlayUser['coupon'];
    $_SESSION['login']['combo_login']=$dataPlayUser['combo_login'];
    $_SESSION['login']['lv']=$dataPlayUser['lv'];
    $_SESSION['login']['star']=$dataPlayUser['star'];
    $_SESSION['login']['coupon']=5;
}
$betData=((isset($_GET['bet']))?$_GET['bet']:null);
$dataNow=$playBet->getNow();
$checkBet=$playBet->getBetMatchById($_SESSION['login']['id'],$betData['match_id']);
//var_dump(date("Y-m-d H:i:s",strtotime('+7 hour'))." :: ".$betData['time_match']);
//var_dump($dataNow['dateTime']." :: ".$betData['time_match']);
//var_dump(strtotime($dataNow['dateTime'])." :: ".(strtotime($betData['time_match'])-(60*60)));
//var_dump(strtotime($dataNow['dateTime'])<=(strtotime($betData['time_match'])-(60*60)));
//exit;
if(empty($checkBet)) {
    if (!empty($betData)) {
        if (isset($_SESSION['login']) && ($_SESSION['login']['coupon'] > 0) && ($_SESSION['login']['coin'] >= $betData['use_bet_coin']) && strtotime(date("Y-m-d H:i:s", strtotime('+6 hour'))) <= (strtotime($betData['time_match']) - (60 * 60))) {
            $betId = $playBet->addBet($betData['user_id'], $betData['match_id'], $betData['team'], $betData['use_bet_coin'], $betData['use_bet_gold'], $betData['use_bet_coupon'], $betData['status']);
            if (!empty($betData['comment'])) {
                $commentId = $playComment->addComment($betData['match_id'], $betData['team'], $betData['comment'], $_SESSION['login']['fb_uid']);
                $comment = $playComment->getCommentMatchById($commentId);
            }
            $playUserObject = $playUser->updateCoinAndCoupon($_SESSION['login']['fb_uid'], ($_SESSION['login']['coin'] - $betData['use_bet_coin']), ($_SESSION['login']['coupon'] - 1));
            $commentLogIncome = "ได้ทำการ bet คู่ที่ " . $betData['match_id'] . " เป็นจำนวน " . $betData['use_bet_coin'] . " โดยใช้ " . $betData['use_bet_coupon'] . " คูปอง";
            $logIncome = $playLogIncome->addLogInCome($betData['user_id'], $betId, $_SESSION['login']['gold'], $_SESSION['login']['coin'], $_SESSION['login']['coupon'], $_SESSION['login']['lv'], $_SESSION['login']['star'], 'bet', 'coin&coupon', 'minus', $_SESSION['login']['gold'], ($_SESSION['login']['coin'] - $betData['use_bet_coin']), ($_SESSION['login']['coupon'] - 1), $_SESSION['login']['lv'], $_SESSION['login']['star'], $commentLogIncome);
            $dataMatch=$playMatch->getFirstMathById($betData['match_id'])[0];
            if(!empty($dataMatch) && $betData['team']=="home"){
                //addFeed
                $userFeed->addUserFeedMatch($betData['user_id'], $betData['match_id'], 'bet', '<img style="width: 18px;height: 18px;" src="' . $dataMatch->teamHomePath . '"> กำลังทายผล');
            }else {
                //addFeed
                $userFeed->addUserFeedMatch($betData['user_id'], $betData['match_id'], 'bet', '<img style="width: 18px;height: 18px;" src="' . $dataMatch->teamAwayPath . '"> กำลังทายผล');
            }
            $_SESSION['login']['coin'] = ($_SESSION['login']['coin'] - $betData['use_bet_coin']);
            $_SESSION['login']['coupon'] = ($_SESSION['login']['coupon'] - 1);
            $alert = "alert-success";
            $message = "Add Bet Success";
        } else {
            if (!isset($_SESSION['login'])) {
                $alert = "alert-warning";
                $message = "กรุณา Login";
            } else if (($_SESSION['login']['coupon'] <= 0)) {
//                header('Location: /bet.php?match_id=' . $betData['match_id'] . '#betBoupon');
//                exit;
                $alert = "alert-warning";
                $message = "betCoupon";
            } elseif (($_SESSION['login']['coin'] <= $betData['use_bet_coin'])) {
//                header('Location: /bet.php?match_id=' . $betData['match_id'] . '#betCoinMin');
//                exit;
                $alert = "alert-warning";
                $message = "betCoinMin";
            } elseif (strtotime(date("Y-m-d H:i:s", strtotime('+6 hour'))) >= (strtotime($betData['time_match']) - (60 * 60))) {
//                header('Location: /bet.php?match_id=' . $betData['match_id'] . '#betTimeMatch');
//                exit;
                $alert = "alert-warning";
                $message = "หมดเวลาให้การทายผล";
            }
        }
    } else {
        $alert = "alert-warning";
        $message = "betNull";
    }
}else{
    $alert = "alert-warning";
    $message = "คุณได้ Vote คู่นี้เเล้ว";
}

//$webroot=$_SERVER['DOCUMENT_ROOT'];
//$strFileName = $webroot."/load/livescore-main-index.html";
//$strFileHtml = $webroot."/view/index/livescore-main-index.php";
//if(file_exists($strFileName) and file_exists($strFileHtml)) {
//    $objFopen = fopen($strFileName, 'w');
//    $stringData = getRenderedHTML($webroot . "/view/index/livescore-main-index.php");
//    fwrite($objFopen, $stringData);
//    fclose($objFopen);
//}

//$playMatch = new playMatch();
//$match = $playMatch->getFirstMathStatusBet(30);
//foreach ($match as $key=>$value){
//    if($value->id==$betData['match_id']){
//        $strFileName=$webroot.'/match/'.$betData['match_id'].'.html';
//        $objFopen = fopen($strFileName, 'w');
//        $stringData = getRenderedHTML($webroot . "/games-redesign-generates-".$key.".php");
//        fwrite($objFopen, $stringData);
//        fclose($objFopen);
//    }
//}

function getRenderedHTML($path)
{
    ob_start();
    include($path);
    $var=ob_get_contents();
    ob_end_clean();
    return $var;
}

echo json_encode(['alert'=>$alert,'message'=>$message,'betId'=>$betId,'comment'=>$comment,'playUserObject'=>$playUserObject,'logIncome'=>$logIncome,'betData'=>$betData]);


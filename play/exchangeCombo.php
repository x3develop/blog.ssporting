<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/6/2017
 * Time: 16:39
 */
if (!isset($_SESSION)) {
    session_start();
}
//require_once $_SERVER["DOCUMENT_ROOT"].'/model/DBPDO.php';
require_once $_SERVER["DOCUMENT_ROOT"]."/model/playUser.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playBet.php";
$playUser = new playUser();
$playBet=new playBet();
$dateNow = new DateTime();
$dateLast = new DateTime();

$db = new DBPDO();
$dbh = $db->connect2();
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sumBetStep=0;
//$listGold=[0,0,200,400,800,1000,2000,3000,5000,8000,10000];
$listGold=[0,0,200,400,5000,5000,5000,10000,10000,10000,50000,50000,50000,50000,50000,100000,100000,100000,100000,100000,300000,300000,300000,300000,300000,500000,500000,500000,500000,500000,1000000];
if(isset($_SESSION['login'])) {
    $dataBetComboContinuous = $playBet->checkBetComboContinuous($_SESSION['login']['id']);
    $sth = $dbh->prepare("select * from `log_income` where date_record='".$dateNow->format('Y-m-d')."' and user_id=" . $_SESSION['login']['id'] . " and income_derived='combo'");
    $sth->execute();
    $log_income = $sth->fetch();
    if(empty($log_income) and !empty($dataBetComboContinuous) and $dataBetComboContinuous['countCombo']>=2){
        $user_id=$_SESSION['login']['id'];
        $befor_gold=$_SESSION['login']['gold'];
        $befor_coin=$_SESSION['login']['coin'];
        $befor_coupon=$_SESSION['login']['coupon'];
        $befor_lv=$_SESSION['login']['lv'];
        $befor_star=$_SESSION['login']['star'];
        $income_derived='combo';
        $income_type='gold';
        $income_format='plus';
        $after_gold=($befor_gold+$listGold[$dataBetComboContinuous['countCombo']]);
        $after_coin=$befor_coin;
        $after_coupon=$befor_coupon;
        $after_lv=$_SESSION['login']['lv'];
        $after_star=$_SESSION['login']['star'];
        $comment="ได้กดรับ ".($after_gold-$befor_gold)."gold จากการ combo ต่อเนื่อง ".$dataBetComboContinuous['countCombo']."คู่";
        $sql = "INSERT INTO `log_income` (`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
        $sql.=" VALUES (" . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "',".$after_gold.",".$after_coin.",".$after_coupon.",'" . $comment . "','" . $dataBetComboContinuous['date'] . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
        $sth = $dbh->prepare($sql)->execute();
        if($sth){
            $_SESSION['login']['gold']=$after_gold;
            $sql="update `user` set gold=".$after_gold." where id=".$user_id;
            $sth = $dbh->prepare($sql)->execute();
        }
    }
    $db->close();
    header('Location: /profile.php');
    exit();
}
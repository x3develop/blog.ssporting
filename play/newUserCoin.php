<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 11/6/2017
 * Time: 16:39
 */
if (!isset($_SESSION)) {
    session_start();
}
//require_once $_SERVER["DOCUMENT_ROOT"].'/model/DBPDO.php';
require_once $_SERVER["DOCUMENT_ROOT"]."/model/playUser.php";
$playUser = new playUser();
$dateNow = new DateTime();
$dateLast = new DateTime();

$db = new DBPDO();
$dbh = $db->connect2();
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if(isset($_SESSION['login'])) {
    $user=$playUser->checkUserPlayFb($_SESSION['login']['fb_uid']);

    var_dump(date("Y-m-d", strtotime($user['created_at']))." :: ".date("Y-m-d"));

    if(date("Y-m-d", strtotime($user['created_at']))<=date("Y-m-d")){
        $sth = $dbh->prepare("select * from `log_income` where user_id=" . $_SESSION['login']['id'] . " and income_derived='newuser'");
        $sth->execute();
        $log_income = $sth->fetch();
        if(empty($log_income)){
            $user_id=$_SESSION['login']['id'];
            $befor_gold=$_SESSION['login']['gold'];
            $befor_coin=$_SESSION['login']['coin'];
            $befor_coupon=$_SESSION['login']['coupon'];
            $befor_lv=$_SESSION['login']['lv'];
            $befor_star=$_SESSION['login']['star'];
            $income_derived='newuser';
            $income_type='coin';
            $income_format='plus';
            $after_gold=$befor_gold;
            $after_coin = ($befor_coin+5000);
            $after_coupon=$befor_coupon;
            $after_lv=$_SESSION['login']['lv'];
            $after_star=$_SESSION['login']['star'];
            $comment="กดรับ ".($after_coin-$befor_coin)."coin จากการสมัครสมาชิกใหม่";
            $sql = "INSERT INTO `log_income` (`befor_lv`,`befor_star`,`after_lv`,`after_star`,`user_id`,`befor_gold`,`befor_coin`,`befor_coupon`,`income_derived`,`income_type`,`income_format`,`after_gold`,`after_coin`,`after_coupon`,`comment`,`date_record`,`created_at`,`updated_at`)";
            $sql.=" VALUES (" . $befor_lv . "," . $befor_star . "," . $after_lv . "," . $after_star . "," . $user_id . "," . $befor_gold . "," . $befor_coin . "," . $befor_coupon . ",'" . $income_derived . "','" . $income_type . "','" . $income_format . "',".$after_gold.",".$after_coin.",".$after_coupon.",'" . $comment . "','" . $dateNow->format('Y-m-d') . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
            $sth = $dbh->prepare($sql)->execute();
            if($sth){
                $_SESSION['login']['coin']=$after_coin;
                $sql="update `user` set coin=".$after_coin." where id=".$user_id;
                $sth = $dbh->prepare($sql)->execute();
                if($sth){
                    $_SESSION['login']['modal_gold']=false;
                }
            }
        }
        $db->close();
        if(isset($_REQUEST['pathname'])){
            header('Location: '.$_REQUEST['pathname']);
        }else {
            header('Location: /index.php');
        }
        exit();
    }else{
        $db->close();
        if(isset($_REQUEST['pathname'])){
            header('Location: '.$_REQUEST['pathname']);
        }else {
            header('Location: /index.php');
        }
        exit();
    }
}
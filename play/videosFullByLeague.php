<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 1/15/2018
 * Time: 10:20
 */
?>
<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/VideoM.php";
//require_once 'model/VideoM.php';
$VideoMObj = new VideoM();
//$dataVideoMMain = $VideoMObj->getSlideVideos("", 1,((isset($_GET['tag']))?$_GET['tag']:''));

if (isset($_GET['videoId'])) {
    $dataVideo=array();
    $dataVideo = $VideoMObj->getVideosById($_GET['videoId']);
//    $dataVideoMAll = $VideoMObj->getSlideVideosMore($dataVideo[0]->video_id, "highlight", 21, $dataVideo[0]->video_tag);
    $dataVideoMAllByNormal = $VideoMObj->getSlideVideosMoreByStyle($dataVideo[0]->video_id, "highlight", 21, $dataVideo[0]->video_tag,'normal');
    $dataVideoMAllByGoal= $VideoMObj->getSlideVideosMoreByStyle($dataVideo[0]->video_id, "highlight", 21, $dataVideo[0]->video_tag,'goal');

    if(!empty($dataVideoMAllByGoal)) {
        $dataVideoMHighlight = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByGoal[0]->video_id, "highlight", 7);
        $dataVideoMOthers = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByGoal[0]->video_id, "general", 7);
        $dataVideoMRelated = $VideoMObj->getRelatedVideoMore($dataVideoMAllByGoal[0]->video_id, '', 6);
    }else {
        $dataVideoMHighlight = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideo[0]->video_id, "highlight", 7);
        $dataVideoMOthers = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideo[0]->video_id, "general", 7);
        $dataVideoMRelated = $VideoMObj->getRelatedVideoMore($dataVideo[0]->video_id, '', 6);
    }
} else {

//    $dataVideoMAll = $VideoMObj->getSlideVideosCategory(((isset($_GET['category'])) ? $_GET['category'] : ''), "highlight", 21, ((isset($_GET['tag'])) ? $_GET['tag'] : ''));
    $dataVideoMAllByNormal = $VideoMObj->getSlideVideosCategoryByStyle(((isset($_GET['category'])) ? $_GET['category'] : ''), "highlight", 21, ((isset($_GET['tag'])) ? $_GET['tag'] : ''),'normal');
    $dataVideoMAllByGoal = $VideoMObj->getSlideVideosCategoryByStyle(((isset($_GET['category'])) ? $_GET['category'] : ''), "highlight", 21, ((isset($_GET['tag'])) ? $_GET['tag'] : ''),'goal');

    if(!empty($dataVideoMAllByGoal)) {
        $dataVideoMHighlight = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByGoal[0]->video_id, "highlight", 7);
        $dataVideoMOthers = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByGoal[0]->video_id, "general", 7);
        $dataVideoMRelated = $VideoMObj->getRelatedVideoMore($dataVideoMAllByGoal[0]->video_id, '', 6);
    }else {
        $dataVideoMHighlight = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByNormal[0]->video_id, "highlight", 7);
        $dataVideoMOthers = $VideoMObj->getRelatedVideoMoreByVideoType($dataVideoMAllByNormal[0]->video_id, "general", 7);
        $dataVideoMRelated = $VideoMObj->getRelatedVideoMore($dataVideoMAllByNormal[0]->video_id, '', 6);
    }
}
?>
<div class="box-list-videos">
    <table>
        <?php foreach ($dataVideoMAllByNormal as $key=>$value){ ?>
            <tr onclick="setIndexVideosAutoPlay('<?php echo $value->urlIframe ?>','<?php echo $value->video_tag ?>','<?php echo $value->title ?>','','<?php echo $value->create_datetime ?>','<?php echo $value->video_id ?>')" >
                <td>
                    <span><i class="fa fa-caret-right"></i> <?= date('d-M-Y', strtotime($value->create_datetime)); ?></span>
                    <div class="box-content-list">
                        <?=$value->title ?>
                    </div>
                </td>
                <td>
                    <div>
                        <img width="100%" height="80" src="<?= $value->urlImg;?>">
                    </div>
                </td>
            </tr>
        <?php } ?>
    </table>
</div>

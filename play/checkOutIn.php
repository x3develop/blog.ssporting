<?php
/**
 * Created by PhpStorm.
 * User: NanoK
 * Date: 3/7/2018
 * Time: 17:24
 */
if (!isset($_SESSION)) {
    session_start();
}
require_once $_SERVER["DOCUMENT_ROOT"] . "/model/playUser.php";
$playUser = new playUser();
$dataReturn=array();
if(isset($_REQUEST['register'])){
    if($_REQUEST['register']['id']){
        $playUser->editUser($_REQUEST['register']['id'],$_REQUEST['register']['email'], $_REQUEST['register']['name'], $_REQUEST['register']['username'], $_REQUEST['register']['password'], $_REQUEST['register']['path']);
    }else {
        $playUser->addUserEmail($_REQUEST['register']['email'], $_REQUEST['register']['name'], $_REQUEST['register']['username'], $_REQUEST['register']['password'], $_REQUEST['register']['path']);
    }
    $user=$playUser->checkUserByUserName($_REQUEST['register']['username']);
    $_SESSION['login'] = [
        'fb_uid' => $user['fb_uid'],
        'name' => $user['name'],
        'id' => $user['id'],
        'username' => $user['username'],
        'email' => $user['email'],
        'coin' => $user['coin'],
        'gold' => $user['gold'],
        'path' => $user['path'],
        'coupon' => $user['coupon'],
        'combo_login' => $user['combo_login'],
        'lv' => $user['lv'],
        'star' => $user['star'],
        'modal_gold' =>false,
        'coin_daily' =>false,
        'bet'=>null
    ];
    $dataReturn['data']=$user;
    $dataReturn['status']="success";
}else if(isset($_REQUEST['login'])){
    $user=$playUser->checkUserByUserName($_REQUEST['login']['username']);
    if(!empty($user)){
        if($user['password']==md5($_REQUEST['login']['passage'])){
            $_SESSION['login'] = [
                'fb_uid' => $user['fb_uid'],
                'name' => $user['name'],
                'id' => $user['id'],
                'username' => $user['username'],
                'email' => $user['email'],
                'coin' => $user['coin'],
                'gold' => $user['gold'],
                'path' => $user['path'],
                'coupon' => $user['coupon'],
                'combo_login' => $user['combo_login'],
                'lv' => $user['lv'],
                'star' => $user['star'],
                'modal_gold' =>false,
                'coin_daily' =>false,
                'bet'=>null
            ];
            $dataReturn['data']=$user;
            $dataReturn['status']="success";
//            $dataReturn['message']="Password ไม่ถูกต้อง";
        }else{
            $dataReturn['status']="errorPassword";
            $dataReturn['message']="Password ไม่ถูกต้อง";
        }
    }else{
        $dataReturn['status']="errorUsername";
        $dataReturn['message']="ไม่ข้อมูล UserName หรือ Email";
    }
}
echo json_encode($dataReturn);
<?php
include_once './model/NewsM.php';
include_once './model/VideoM.php';
include_once './utility/Carbon/Carbon.php';
use Carbon\Carbon;

?>
<?php
$newsid = ($_REQUEST['newsid']) ? (int)$_REQUEST['newsid'] : 0;
if ($newsid == 0) {
    header("Location: /");
}
$newsm = new NewsM();
$videom = new VideoM();
$newslist = array();
$newslist = $newsm->getNewsById($newsid);

$newslistLoading = $newsm->getListByIdNewsByDay($newsid, 7);

$news = array();
if (array_key_exists(0, $newslist)) {
    $news = $newslist[0];
    $newsm->updateView($newsid);
} else {
    header("Location: /");
}
$dataRelatedVideos = $videom->getRelatedVideo($news->news_tag, 4);
$relatednews = $newsm->getRelatedNewsByIdNews($newsid, $news->news_tag, 4);
$recommendnews = $newsm->getRecommendNews($news->category, 5);
$cb = Carbon::createFromTimestamp($news->createDatetime);
$cb->addHours(7);
$up = Carbon::createFromTimestamp($news->updateDatetime);
$up->addHours(7);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="shortcut icon" type="images/x-icon" href="images/favicon.ico.png">

    <title>ข่าว<?php echo ((!empty($newslist)) ? (($newslist[0]->category=="thai")?'ไทย':$newslist[0]->category) : '-') ?> ข่าว<?php echo  ((!empty($newslist)) ? $newslist[0]->titleTh : '') ?> วันที่ <?php echo $cb->format("Y-m-d"); ?></title>
    <meta name="keywords" content="<?php echo $news->news_tag; ?> ,ข่าวฟุตบอล, ข่าวฟุตบอล<?php echo ((!empty($newslist)) ? (($newslist[0]->category=="thai")?'ไทย':$newslist[0]->category) : '-') ?>, บ้านผลบอล, ไฮไลท์ฟุตบอล, ไฮไลท์บอลเมื่อคืน, บอลเมื่อคืน, บอลล่าสุด, เร็วที่สุด, คมชัดที่สุด">
    <meta name="description" content="ข่าวฟุตบอลด่วน ข่าว<?php echo  ((!empty($newslist)) ? $newslist[0]->titleTh : '') ?>  วันที่ <?php echo $cb->format("Y-m-d"); ?> ข่าวลือ ข่าวซื้อขาย ข่าวฟุตบอล<?php echo ((!empty($newslist)) ? (($newslist[0]->category=="thai")?'ไทย':$newslist[0]->category) : '-') ?> ใหม่ล่าสุดได้ก่อนใคร รวมถึง รายงานผลบอลแบบสด ๆ, ดูตารางคะแนน ฟุตบอล<?php echo ((!empty($newslist)) ? (($newslist[0]->category=="thai")?'ไทย':$newslist[0]->category) : '-') ?>, ไฮไลท์, สถิติ, คลิปวิดีโอ และอื่น ๆ อีกมากมาย">

    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <!--    <link rel="stylesheet" href="/css/style.css">-->
    <link rel="stylesheet" href="css/style-new.css">
    <link rel="stylesheet" href="/css/articles.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/css/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Pridi" rel="stylesheet">

    <!--js-->
    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="/css/OwlCarousel/dist/owl.carousel.min.js"></script>
    <!--    <script src="/js/flogin/src/flogin.js"></script>-->
    <script src="/js/moment.js"></script>
    <meta property="fb:app_id" content="1630409860331473"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url"
          content="<?= (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>"/>
    <meta property="og:title" content="<?php echo $news->titleTh; ?>"/>
    <meta property="og:description" content="<?php echo $news->shortDescriptionTh; ?>"/>
    <meta property="og:image"
          content="<?php echo((strpos($news->imageLink, "http://") === 0) ? $news->imageLink : "http://" . $_SERVER['SERVER_NAME'] . $news->imageLink); ?>"/>
    <?php include 'view/index/googletagmanager.php'; ?>
</head>
<body>
<!--index menu-->
<?php include $_SERVER['DOCUMENT_ROOT'] . '/view/index/menu-all.php'; ?>
<?php //include $_SERVER['DOCUMENT_ROOT'].'/view/menu/navbar-menu-v2-fixed.php'; ?>

<div id="Main-articles">
    <div class="wrapper-news-articles news-articles-list" newsId="<?php echo $newsid ?>">
        <div class="containter-fluid">
            <div class="bx-head-articles">
                <div class="hilight-cover"></div>
                <div class="articles-title">
                    <div class="container">
                        <h1 class="leads-headline"><span><?php echo $news->titleTh; ?></span>
                        </h1>
                    </div>
                </div>
                <div class="background-image_image2"
                     style="background-image:url(<?php echo $news->imageLink; ?>)"></div>
            </div>
            <div style="clear: both"></div>
        </div>
        <div style="clear: both"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12" style="margin-top: 20px;">

                    <div class="col-sm-9 main_articles" newsid="<?php echo $newsid ?>"
                         id="main_articles_<?php echo $newsid ?>">
                        <div class="bx-publish-info">
                        <span class="pull-left" style="padding-top: 5px;">
                            <?php echo $cb->format('F d, Y - h:i A'); ?>
                            Updated on <?php echo $up->format('F d, Y - h:i A'); ?></span>
                            <span class="bx-social pull-right">
                        <?php include 'view/social/bx-social.php'; ?>
                    </span>

                            <div style="clear: both;"></div>
                        </div>
                        <div class="content-articles">
                            <?php echo $news->shortDescriptionTh; ?>
                            <div style="clear: both;"></div>

                            <?php if (!empty($news->linkVideo)) { ?>
                                <div id="Main_Box_Video_Articles_<?php echo $newsid ?>"
                                     style="width: 100%;height: 635px;padding: 2%;">
                                    <?php if ($news->typeVideo == "dailymotion") { ?>
                                        <iframe id="Video_Articles_<?php echo $newsid ?>" style="position: absolute"
                                                width="95%" height="600" data-autoplay="true"
                                                src="<?php echo $news->urlIframe ?>"
                                                StopSrc="http://www.dailymotion.com/embed/video/<?php echo $news->videokey ?>"
                                                StartSrc="<?php echo $news->urlIframe ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } elseif ($news->typeVideo == "twitter") { ?>
                                        <iframe id="Video_Articles_<?php echo $newsid ?>" style="position: absolute"
                                                width="95%" height="600" data-autoplay="true"
                                                src="<?php echo $news->urlIframe ?>"
                                                StopSrc="https://twitter.com/i/cards/tfw/v1/<?php echo $news->videokey ?>"
                                                StartSrc="<?php echo $news->urlIframe ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } elseif ($news->typeVideo == "facebook") { ?>
                                        <iframe id="Video_Articles_<?php echo $newsid ?>" style="position: absolute"
                                                width="95%" height="600" data-autoplay="true"
                                                src="<?php echo $news->urlIframe ?>"
                                                StopSrc="https://www.facebook.com/v2.3/plugins/video?allowfullscreen=true&container_width=800&href=<?php echo $news->linkVideo ?>"
                                                StartSrc="<?php echo $news->urlIframe ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } elseif ($news->typeVideo == "streamable") { ?>
                                        <iframe id="Video_Articles_<?php echo $newsid ?>" style="position: absolute"
                                                width="95%" height="600" data-autoplay="true"
                                                src="<?php echo $news->urlIframe ?>'?autoplay=1"
                                                StopSrc="https://www.facebook.com/v2.3/plugins/video?allowfullscreen=true&container_width=800&href=<?php echo $news->linkVideo ?>"
                                                StartSrc="<?php echo $news->urlIframe ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } else { ?>
                                        <iframe id="Video_Articles_<?php echo $newsid ?>" style="position: absolute"
                                                width="94%" height="600" data-autoplay="true"
                                                src="<?php echo $news->urlIframe ?>"
                                                StopSrc="http://www.youtube.com/embed/<?php echo $news->videokey ?>"
                                                StartSrc="<?php echo $news->urlIframe ?>"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    <?php } ?>
                                </div>
                            <?php } ?>

                            <div style="clear: both;"></div>
                            <img src="<?php echo $news->imageLink; ?>" style="width: 100%;">

                            <?php echo $news->contentTh; ?>
                        </div>
                    </div>
                    <div class="col-sm-3 menu_articles" id="menu_articles_<?php echo $newsid ?>"
                         style="position: relative;">
                        <div id="menu_articles_highlight_<?php echo $newsid ?>" class="bx-publish-recommend"
                             style="position: absolute;top: 0;">
                            <div class="title">
                            <h3>Most Recent</h3>
                            </div>
                            <table>
                                <?php foreach ($recommendnews as $key => $recommend) { ?>
                                    <tr>
                                        <td>
                                            <div class="news-image">
                                                <a href="<?php echo '/articles?newsid=' . $recommend->newsid; ?>">
                                                 <div class="crop">
                                                    <img src="<?php echo $recommend->imageLink; ?>">
                                                 </div>
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="<?php echo '/articles?newsid=' . $recommend->newsid; ?>"><?php echo $recommend->titleTh; ?></a>
                                            <div class="times-content"><i class="fa fa-clock-o"></i> <span
                                                        class="timeMoreRecentedGallery"
                                                        date="<?php echo $recommend->createDatetime ?>">3 hours ago</span>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                            <div style="border: solid 0px;width: 100%;height: 200px;"
                                 id="Box_Mini_Video_Articles_<?php echo $newsid ?>">

                            </div>
                        </div>

                    </div>

                </div>
                <div style="clear: both;"></div>
                <!--            wrap-content-cols-->
                <div class="col-sm-12 content-related-article">
                    <div class="double-underline bx-tabs-related text-center relate25">Related</div>

                    <?php if (!empty($relatednews)) { ?>
                        <div class="label-news-vertical">News</div>
                        <?php foreach ($relatednews as $key => $relate) { ?>
                            <a href="<?php echo '/articles?newsid=' . $relate->newsid; ?>">
                                <div class="col-sm-3 related-news" newsid="<?php echo $relate->newsid; ?>">
                                    <div class="box-videos box-related box-related-sexy">
                                        <div class="bx-type-gallery">
                                            <i class="fa fa-picture-o"></i>
                                        </div>
                                        <div class="crop">
                                            <img src="<?php echo $relate->imageLink; ?>" data-pin-nopin="true">
                                        </div>
                                    </div>
                                    <div class="title-owl-carousel title-h-videos">
                                        <a href="<?php echo '/articles?newsid=' . $relate->newsid; ?>"><?php echo $relate->titleTh; ?></a>
                                    </div>
                                    <div class="times-content">
                                        <span class="timeMoreRecentedGallery"
                                              date="<?php echo $relate->createDatetime ?>">23 minutes ago</span>
                                    </div>
                                </div>
                            </a>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div style="clear: both"></div>
                <div class="col-sm-12 content-related-article">
                    <!--            <div class="double-underline bx-tabs-related">&nbsp;</div>-->
                    <?php if (!empty($dataRelatedVideos)) { ?>
                        <div class="label-news-vertical">Video</div>
                        <?php foreach ($dataRelatedVideos as $key => $value) { ?>
                            <div class="col-sm-3 related-news">
                                <a class="link_wrap"
                                   href="/<?php echo(($value->videotype == 'highlight') ? 'videos' : 'hothit-videos') ?>?videoId=<?php echo $value->video_id ?>"></a>
                                <div class="box-videos box-related box-related-sexy">
                                    <div class="bx-type-gallery">
                                        <i class="fa fa-play"></i>
                                    </div>
                                    <div class="crop">
                                        <img src="<?php echo $value->urlImg; ?>" data-pin-nopin="true">
                                    </div>
                                </div>
                                <div class="title-owl-carousel"><?php echo $value->title ?></div>
                                <div class="times-content">
                                        <span class="timeMoreRecentedGallery"
                                              date="<?php echo $value->create_datetime ?>">23 minutes ago</span>
                                </div>

                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div style="clear: both"></div>

                <div style="clear: both"></div>
            </div>
        </div>
    </div>
</div>
<?php //include 'footer.php';
include 'view/index/footer.php';
?>

<div id="Articles-BlackUp">
    <?php foreach ($newslistLoading as $key => $value) { ?>
        <div newsId="<?php echo $value->newsid ?>" class="dataBlackUp" id="dataBlackUp_<?php echo $value->newsid ?>"
             style="display: none;"></div>
    <?php } ?>
</div>

<script>
    $(document).ready(function () {
        var stickyNavTop = $('#navbar').offset().top;
        var countPage = 0;
        var arrayWidth = [];
        var arrayPosition = [];
        var arrayMenuBox = [];
        var arrayIdNews = [];
        var arrayMainMenuBox = [];
        var arrayMimiBoxVideo = [];
        loadMoment();
        function loadMoment() {
            $('.timeMoreRecentedGallery').each(function (index) {
                if ($.isNumeric($(this).attr('date'))) {
                    $(this).html(moment($(this).attr('date'), "X").fromNow())
                } else {
                    $(this).html(moment($(this).attr('date'), "YYYY-MM-DD HH:mm:ss").fromNow())
                }
            })
        }

        function stickyHighlight() {
            var scrollTop = $(window).scrollTop();
            scrollTop += 48;
            if (scrollTop > arrayPosition[countPage] && scrollTop < ((arrayPosition[countPage] + arrayMainMenuBox[countPage]) - arrayMenuBox[countPage])) {
                $('#menu_articles_highlight_' + arrayIdNews[countPage]).css('top', ((scrollTop) - arrayPosition[countPage]));
            }
        }

        $('.main_articles').each(function (index) {
            $('#menu_articles_' + $(this).attr('newsid')).css('height', $(this).height());
            arrayMainMenuBox.push($(this).height());
            arrayMenuBox.push($('#menu_articles_highlight_' + $(this).attr('newsId')).height());
            arrayPosition.push($(this).offset().top);
            arrayMimiBoxVideo.push($('#Box_Mini_Video_Articles_' + $(this).attr('newsId')).offset().top);
            arrayIdNews.push($(this).attr('newsId'));
        })

        $(".dataBlackUp").each(function (index) {
            if (index < 3) {
                var object = $(this);
                $.ajax({
                    url: "articles-list",
                    data: {newsid: $(this).attr('newsId')},
                    method: "GET",
                    dataType: "html"
                }).done(function (response) {
                    object.html(response);
                    loadMoment();
                });
            }
        })
        stickyNav();
//        stickyHighlight();
        function stickyNav() {
            var maxPage = 0;
            var windowHeight = jQuery(window).height();
            var windowWidth = jQuery(window).width();
            var scrollTop = $(window).scrollTop();
            if (scrollTop > stickyNavTop) {
                $('#navbarTopBar').css('top', 0);
            } else {
                $('#navbarTopBar').removeAttr('style');
            }
//            console.log(windowHeight);
//            scrollTop+=windowHeight;

            $(".news-articles-list").each(function (index) {
                if (index <= countPage) {
                    var heightArticles = ($(this).height() + 50);
                    maxPage += heightArticles;
                    arrayWidth[index] = heightArticles;
                }

            });

            var reng = (maxPage - scrollTop) - windowHeight;
//            console.log(reng);
//            console.log(+" :: "+);
            if ($('#Main_Box_Video_Articles_' + arrayIdNews[countPage]).length) {
//                console.log(((arrayWidth[countPage] - windowHeight) - reng)+" :: "+(($('#Main_Box_Video_Articles_' + arrayIdNews[countPage]).height())+400));
                if (scrollTop > arrayPosition[countPage] && scrollTop < ((arrayPosition[countPage] + arrayMainMenuBox[countPage]) - arrayMenuBox[countPage])) {
                    if (((arrayWidth[countPage] - windowHeight) - reng) > (($('#Main_Box_Video_Articles_' + arrayIdNews[countPage]).height()) + 400)) {
//                if(($('#Video_Articles_'+arrayIdNews[countPage]).offset().top+$('#Video_Articles_'+arrayIdNews[countPage]).height())>reng){
                        $('#Video_Articles_' + arrayIdNews[countPage]).css('width', 286);
                        $('#Video_Articles_' + arrayIdNews[countPage]).css('height', 200);
                        $('#Video_Articles_' + arrayIdNews[countPage]).css('top', (((scrollTop) - (arrayPosition[countPage])) + (arrayMenuBox[countPage] - 150)));
                        $('#Video_Articles_' + arrayIdNews[countPage]).css('left', $('#Box_Mini_Video_Articles_' + arrayIdNews[countPage]).offset().left - ((windowWidth - 1180) / 2));
//                }
//                console.log(reng+" :: "+($('#Video_Articles_'+arrayIdNews[countPage]).offset().top+$('#Video_Articles_'+arrayIdNews[countPage]).height()));
//                console.log($('#Video_Articles_'+arrayIdNews[countPage]).position().top);
//                console.log($('#Video_Articles_'+arrayIdNews[countPage]).offset().top);
//                console.log($('#Video_Articles_'+arrayIdNews[countPage]).height());
                    } else {
//                    console.log('false '+arrayIdNews[countPage]);
                        $('#Video_Articles_' + arrayIdNews[countPage]).removeAttr('style');
                        $('#Video_Articles_' + arrayIdNews[countPage]).css('position', "absolute");
                    }
                }
            }

            if (reng < 100) {
                $(".dataBlackUp").each(function (index) {
                    if ($(this).html() !== "" && index == countPage) {
                        $('#Main-articles').append($(this).html());
                        $(this).html("");
                        arrayMenuBox = [];
                        arrayPosition = [];
                        arrayIdNews = [];
                        arrayMainMenuBox = [];
                        $('#Main-articles').find('.main_articles').each(function (index) {
                            $('#menu_articles_' + $(this).attr('newsid')).css('height', $(this).height());
                            arrayMenuBox.push($('#menu_articles_highlight_' + $(this).attr('newsId')).height());
                            arrayPosition.push($(this).offset().top);
                            arrayIdNews.push($(this).attr('newsId'));
                            arrayMainMenuBox.push($(this).height());
                        })
                        $(".dataBlackUp").each(function (index) {
                            if (index == (countPage + 3)) {
                                var object = $(this);
                                $.ajax({
                                    url: "articles-list",
                                    data: {newsid: $(this).attr('newsId')},
                                    method: "GET",
                                    dataType: "html"
                                }).done(function (response) {
                                    object.html(response);
                                    loadMoment();
                                });
                            }
                        })
                    }
                });
            }

            if (maxPage > 0 && scrollTop > maxPage) {
                if ($('#Video_Articles_' + arrayIdNews[countPage]).length) {
                    $('#Video_Articles_' + arrayIdNews[countPage]).attr('src', $('#Video_Articles_' + arrayIdNews[countPage]).attr('stopsrc'))
                }
                countPage += 1;
                if ($('#Video_Articles_' + arrayIdNews[countPage]).length) {
                    $('#Video_Articles_' + arrayIdNews[countPage]).attr('src', $('#Video_Articles_' + arrayIdNews[countPage]).attr('startsrc'))
                }
            } else if (maxPage > 0 && scrollTop > 0 && scrollTop < (maxPage - arrayWidth[countPage])) {
                if ($('#Video_Articles_' + arrayIdNews[countPage]).length) {
                    $('#Video_Articles_' + arrayIdNews[countPage]).attr('src', $('#Video_Articles_' + arrayIdNews[countPage]).attr('stopsrc'))
                }
                countPage -= 1;
                if ($('#Video_Articles_' + arrayIdNews[countPage]).length) {
                    $('#Video_Articles_' + arrayIdNews[countPage]).attr('src', $('#Video_Articles_' + arrayIdNews[countPage]).attr('startsrc'))
                }
            }
            $('#loadingBar').css('width', (((((arrayWidth[countPage] - (maxPage - scrollTop)) - arrayWidth[countPage]) / arrayWidth[countPage]) * 100) + 100) + '%');
        };

        $(window).scroll(function () {
            stickyNav();
            stickyHighlight();
        });

    });
</script>

</body>
</html>